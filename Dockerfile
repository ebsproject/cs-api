FROM gradle:7.4.2-jdk11 AS gradleBuild
WORKDIR /home/gradle/project

COPY --chown=gradle:gradle . /home/gradle/project

RUN gradle build --no-daemon --stacktrace

#FROM maven:3.6.3-ibmjava-8-alpine as buildDataflow
FROM maven:3.6.3-openjdk-17-slim as buildDataflow

COPY dataflows dataflows
RUN mvn clean -f dataflows/import_files -Dmaven.test.skip=true install
RUN mvn -f dataflows/import_files/import_filesCompositeExporter -Dmaven.test.skip=true clean package
RUN mvn clean -f dataflows/notifyMessaging -Dmaven.test.skip=true install
RUN mvn -f dataflows/notifyMessaging/notifyMessagingCompositeExporter -Dmaven.test.skip=true clean package
RUN mvn clean -f dataflows/workflowEngine -Dmaven.test.skip=true install
RUN mvn -f dataflows/workflowEngine/workflowEngineCompositeExporter -Dmaven.test.skip=true clean package

# Change to ubuntu image
# FROM wso2/wso2mi:4.1.0
FROM wso2/wso2mi:4.0.1-SNAPSHOT-ubuntu

# Return to default user 
USER wso2carbon

WORKDIR /home/wso2carbon

ENV spring_profiles_active=default
ENV spring_datasource_url=jdbc:postgresql://cs-db:5433/cs_db
ENV spring_datasource_username=postgres
ENV spring_datasource_password=postgres

#Env variables to authenticate
ENV ebs_cs_auth_client_id=some_client_id
ENV ebs_cs_auth_client_secret=some_client_secret
ENV ebs_cs_auth_username=some_username
ENV ebs_cs_auth_password=some_password
ENV ebs_cs_auth_endpoint=https://sg.SOME_SERVER:PORT/oauth2/token
ENV ebs_cs_auth_grant_type=password
ENV ebs_cs_auth_claims=username
ENV ebs_cs_auth_scope=openid

#Env variables to enable the synchronizator
ENV ebs_cs_sync_enabled=false
ENV ebs_cs_sync_delay_time_seconds=30

#Env variables to bring external data
ENV ebs_cb_api_endpoint=https://cbapi.SOME_SERVER/v3
ENV ebs_cb_graphql_endpoint=https://cbgraphqlapi.SERVER/graphql
ENV ebs_cb_serverData=cbapi-dev.ebsproject.org

#Env variables to reach local data
ENV ebs_cs_serverData=http://localhost:8080/graphql
ENV ebs_cs_server=http://localhost:8080
# TODO: Remove on next commit
ENV ebs_cs_server_port=8080
# ENV ebs_file_api_endpoint=http://fileapi-SERVER:8080 Delete because is not use on project
ENV ebs_sm_graphql_endpoint=http://smapi-SERVER:8080/graphql
ENV ebs_email_endpoint=https://csps-dev.ebsproject.org:443/api/email/send

#Env variables to set gigwa properties
ENV ebs_gigwa_loader_enabled=true
ENV ebs_gigwa_loader_delay_time_seconds=15
ENV ebs_gigwa_username=gigwadmin
ENV ebs_gigwa_password=nimda
ENV ebs_gigwa_api_endpoint=http://gigwa-SERVER:8080/gigwa/rest

#Env variables to set rabbit server properties
ENV ebs_cs_rabbit_host=localhost
ENV ebs_cs_rabbit_port=5672
ENV ebs_cs_rabbit_username=admin
ENV ebs_cs_rabbit_password=Rabbit123

#Enviroment to configure notification service
ENV ebs_enable_email_notification=true

#ENV JAVA_TOOL_OPTIONS "-Xms512M -Xmx1536M"

COPY --from=buildDataflow dataflows/import_files/import_filesCompositeExporter/target ./wso2mi-4.0.1-SNAPSHOT/repository/deployment/server/carbonapps
COPY --from=buildDataflow dataflows/notifyMessaging/notifyMessagingCompositeExporter/target ./wso2mi-4.0.1-SNAPSHOT/repository/deployment/server/carbonapps
COPY --from=buildDataflow dataflows/workflowEngine/workflowEngineCompositeExporter/target ./wso2mi-4.0.1-SNAPSHOT/repository/deployment/server/carbonapps
COPY dataflows/dependencies/DBDrives ./wso2mi-4.0.1-SNAPSHOT/lib
COPY dataflows/dependencies/UsedFiles ./wso2mi-4.0.1-SNAPSHOT/lib
COPY dataflows/deploy ./wso2mi-4.0.1-SNAPSHOT/conf
COPY --from=gradleBuild /home/gradle/project/build/libs/cs-api.jar ./app.jar
COPY entrypoint.sh .

ENTRYPOINT ["./entrypoint.sh"]
