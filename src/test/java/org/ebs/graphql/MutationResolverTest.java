package org.ebs.graphql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.ebs.services.AddressService;
import org.ebs.services.CFTypeService;
import org.ebs.services.CFValueService;
import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactInfoTypeService;
import org.ebs.services.ContactService;
import org.ebs.services.ContactWorkflowService;
// import org.ebs.services.CoreHierarchyService;
import org.ebs.services.CustomerService;
import org.ebs.services.DelegationService;
import org.ebs.services.DomainInstanceService;
import org.ebs.services.EmailTemplateService;
import org.ebs.services.EventService;
import org.ebs.services.FileObjectService;
import org.ebs.services.FileTypeService;
import org.ebs.services.FilterService;
import org.ebs.services.FunctionalUnitService;
// import org.ebs.services.HierarchyDesignAttributesService;
// import org.ebs.services.HierarchyDesignService;
import org.ebs.services.HierarchyService;
// import org.ebs.services.HierarchyTreeAttributesValuesService;
// import org.ebs.services.HierarchyTreeService;
// import org.ebs.services.HierarchyTypeService;
import org.ebs.services.InstanceService;
import org.ebs.services.JobLogService;
import org.ebs.services.JobWorkflowService;
import org.ebs.services.NodeCFService;
import org.ebs.services.NodeService;
import org.ebs.services.NodeTypeService;
import org.ebs.services.OccurrenceShipmentService;
import org.ebs.services.OrganizationService;
import org.ebs.services.OrganizationalUnitService;
import org.ebs.services.PhaseService;
import org.ebs.services.PrintoutTemplateService;
import org.ebs.services.ProcessService;
import org.ebs.services.ProductFunctionService;
import org.ebs.services.ProductService;
import org.ebs.services.ProgramService;
import org.ebs.services.PurposeService;
import org.ebs.services.RoleContactHierarchyService;
import org.ebs.services.RoleProductService;
import org.ebs.services.RoleService;
import org.ebs.services.SecurityRuleRoleService;
import org.ebs.services.SecurityRuleService;
import org.ebs.services.ServiceFileService;
import org.ebs.services.ServiceItemService;
import org.ebs.services.ServiceService;
import org.ebs.services.ShipmentFileService;
import org.ebs.services.ShipmentItemService;
import org.ebs.services.ShipmentService;
import org.ebs.services.StageService;
import org.ebs.services.StatusService;
import org.ebs.services.StatusTypeService;
import org.ebs.services.TenantService;
import org.ebs.services.UserService;
import org.ebs.services.WorkflowInstanceService;
import org.ebs.services.WorkflowService;
import org.ebs.services.WorkflowViewTypeService;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.Input.ContactInfoInput;
import org.ebs.services.to.Input.ContactInfoTypeInput;
import org.ebs.services.to.Input.ContactInput;
import org.ebs.services.to.Input.CustomerInput;
import org.ebs.services.to.Input.DelegationInput;
import org.ebs.services.to.Input.HierarchyInput;
import org.ebs.services.to.Input.PrintoutTemplateInput;
import org.ebs.services.to.Input.ProductFunctionInput;
import org.ebs.services.to.Input.RoleInput;
import org.ebs.services.to.Input.RoleProductInput;
import org.ebs.services.to.Input.SecurityRuleRoleInput;
import org.ebs.services.to.Input.UserInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import reactor.core.publisher.Sinks.Many;

@ExtendWith(MockitoExtension.class)
public class MutationResolverTest {

    @Mock()
    ContactInput contactInputMock;
    @Mock()
    PrintoutTemplateInput printoutTemplateInputMock;
    @Mock()
    CustomerInput customerInputMock;
    @Mock()
    HierarchyInput hierarchyInputMock;
    @Mock()
    ContactInfoTypeInput contactInfoTypeInputMock;
    @Mock()
    ContactInfoInput contactInfoInputMock;
    @Mock()
    RoleProductInput roleProductInputMock;
    @Mock()
    SecurityRuleRoleInput securityRuleRoleInputMock;
    @Mock()
    DelegationInput delegationInputMock;
    @Mock()
    ProductFunctionInput productFunctionInputMock;
    @Mock()
    RoleInput roleInputMock;
    @Mock()
    UserInput userInputMock;
    @Mock
    private ContactService contactServiceMock;
    @Mock
    private PrintoutTemplateService printoutTemplateServiceMock;
    @Mock
    private CustomerService customerServiceMock;
    @Mock
    private HierarchyService hierarchyServiceMock;
    @Mock
    private PurposeService purposeServiceMock;
    @Mock
    private OrganizationService organizationServiceMock;
    @Mock
    private ContactInfoTypeService contactInfoTypeServiceMock;
    @Mock
    private ContactInfoService contactInfoServiceMock;
    @Mock
    private RoleProductService roleProductServiceMock;
    @Mock
    private SecurityRuleRoleService securityRuleRoleServiceMock;
    @Mock
    private SecurityRuleService securityRuleServiceMock;
    @Mock
    private ProductFunctionService productFunctionServiceMock;
    @Mock
    private DomainInstanceService domainInstanceServiceMock;
    @Mock
    private ProductService productServiceMock;
    @Mock
    private RoleService roleServiceMock;
    @Mock
    private UserService userServiceMock;
    @Mock
    private DelegationService delegationServiceMock;
    @Mock
    private FunctionalUnitService functionalUnitService;

    // @Mock
    // private CoreHierarchyService coreHierarchyServiceMock;
    // @Mock
    // private HierarchyTypeService hierarchyTypeServiceMock;
    // @Mock
    // private HierarchyDesignService hierarchyDesignServiceMock;
    // @Mock
    // private HierarchyTreeService hierarchyTreeServiceMock;
    @Mock
    private FilterService filterServiceMock;
    @Mock
    private InstanceService instanceServiceMock;
    @Mock
    private TenantService tenantServiceMock;
    @Mock
    private JobWorkflowService jobWorkflowServiceMock;
    @Mock
    private JobLogService jobLogServiceMock;
    @Mock
    private WorkflowInstanceService workflowInstanceServiceMock;
    @Mock
    private EventService eventServiceMock;
    @Mock
    private ShipmentService shipmentServiceMock;
    @Mock
    private ShipmentItemService shipmentItemServiceMock;
    @Mock
    private ServiceItemService serviceItemServiceMock;
    @Mock
    private ShipmentFileService shipmentFileServiceMock;
    @Mock
    private ServiceFileService filesServiceMock;
    @Mock
    private FileObjectService fileObjectServiceMock;
    @Mock
    private EmailTemplateService emailTemplateServiceMock;
    @Mock
    private CFTypeService cfTypeServiceMock;
    @Mock
    private WorkflowViewTypeService workflowViewTypeServiceMock;
    @Mock
    private NodeTypeService nodeTypeServiceMock;
    @Mock
    private NodeService nodeServiceMock;
    @Mock
    private WorkflowService workflowServiceMock;
    @Mock
    private NodeCFService nodeCFServiceMock;
    @Mock
    private CFValueService cfValueServiceMock;
    @Mock
    private StatusTypeService statusTypeServiceMock;
    @Mock
    private StatusService statusServiceMock;
    @Mock
    private StageService stageServiceMock;
    @Mock
    private PhaseService phaseServiceMock;
    @Mock
    private ServiceService serviceServiceMock;
    @Mock
    private ProgramService programServiceMock;
    // @Mock
    // private HierarchyDesignAttributesService hierarchyDesignAttributesServiceMock;
    // @Mock
    // private HierarchyTreeAttributesValuesService hierarchyTreeAttributesValuesServiceMock;
    @Mock
    private ProcessService processServiceMock;
    @Mock
    private FileTypeService fileTypeServiceMock;
    @Mock
    private ContactWorkflowService contactWorkflowServiceMock;
    @Mock
    private OrganizationalUnitService organizationalUnitServiceMock;
    @Mock
    private RoleContactHierarchyService roleContactHierarchyServiceMock;
    @Mock
    private AddressService addressServiceMock;
    @Mock
    private OccurrenceShipmentService occurrenceShipmentServiceMock;

    @Mock
    private Many<JobLogTo> jobLogSinkMock;

    private MutationResolver subject;

    @BeforeEach
    public void init() {
        subject = new MutationResolver(
            contactServiceMock, 
            printoutTemplateServiceMock, 
            customerServiceMock,
            domainInstanceServiceMock,
            purposeServiceMock,
            hierarchyServiceMock, 
            organizationServiceMock, 
            contactInfoTypeServiceMock,
            roleProductServiceMock,
            contactInfoServiceMock, 
            productFunctionServiceMock,
            securityRuleRoleServiceMock,
            securityRuleServiceMock,
            productServiceMock,
            roleServiceMock,
            userServiceMock,
            delegationServiceMock,
            functionalUnitService,
            // coreHierarchyServiceMock,
            // hierarchyTypeServiceMock,
            // hierarchyDesignServiceMock,
            // hierarchyTreeServiceMock,
            filterServiceMock,
            instanceServiceMock,
            tenantServiceMock,
            jobWorkflowServiceMock,
            jobLogServiceMock,
            workflowInstanceServiceMock,
            eventServiceMock,
            shipmentServiceMock,
            shipmentItemServiceMock,
            serviceItemServiceMock,
            shipmentFileServiceMock,
            filesServiceMock,
            fileObjectServiceMock,
            emailTemplateServiceMock,
            cfTypeServiceMock,
            workflowViewTypeServiceMock,
            nodeServiceMock,
            workflowServiceMock,
            nodeCFServiceMock,
            cfValueServiceMock,
            statusTypeServiceMock,
            statusServiceMock,
            stageServiceMock,
            phaseServiceMock,
            serviceServiceMock,
            programServiceMock, 
            // hierarchyDesignAttributesServiceMock,
            // hierarchyTreeAttributesValuesServiceMock,
            nodeTypeServiceMock,
            processServiceMock,
            fileTypeServiceMock,
            contactWorkflowServiceMock,
            organizationalUnitServiceMock,
            roleContactHierarchyServiceMock,
            addressServiceMock,
            occurrenceShipmentServiceMock,
            jobLogSinkMock         
        );
    }

    @Test
    public void givenValidContactInput_whenCreateContact_thenPersistContact() {

        subject.createContact(contactInputMock);

        verify(contactServiceMock, times(1)).createContact(contactInputMock);
    }

    @Test
    public void givenValidContactInput_whenModifyContact_thenPersistContact() {

        subject.modifyContact(contactInputMock);

        verify(contactServiceMock, times(1)).modifyContact(contactInputMock);
    }

    @Test
    public void givenContactExists_whenDeleteContact_thenContactDeleted() {

        subject.deleteContact(anyInt());

        verify(contactServiceMock, times(1)).deleteContact(anyInt());
    }

    @Test
    public void givenValidPrintoutTemplateInput_whenCreatePrintoutTemplate_thenPersistPrintoutTemplate() {

        subject.createPrintoutTemplate(printoutTemplateInputMock);

        verify(printoutTemplateServiceMock, times(1))
                .createPrintoutTemplate(printoutTemplateInputMock);
    }

    @Test
    public void givenValidPrintoutTemplateInput_whenModifyPrintoutTemplate_thenPersistPrintoutTemplate() {

        subject.modifyPrintoutTemplate(printoutTemplateInputMock);

        verify(printoutTemplateServiceMock, times(1))
                .modifyPrintoutTemplate(printoutTemplateInputMock);
    }

    @Test
    public void givenPrintoutTemplateExists_whenDeletePrintoutTemplate_thenPrintoutTemplateDeleted() {

        subject.deletePrintoutTemplate(anyInt());

        verify(printoutTemplateServiceMock, times(1)).deletePrintoutTemplate(anyInt());
    }

    @Test
    public void givenPrintoutTemplateExists_whenAddPrintoutTemplateToPrograms_thenPrintoutTemplateAdded() {

        subject.addPrintoutTemplateToPrograms(anyInt(), anySet());

        verify(printoutTemplateServiceMock, times(1)).addPrintoutTemplateToPrograms(anyInt(),
                anySet());
    }

    @Test
    public void givenPrintoutTemplateExists_whenRemovePrintoutTemplateFromPrograms_thenPrintoutTemplateRemoved() {

        subject.removePrintoutTemplateFromPrograms(anyInt(), anySet());

        verify(printoutTemplateServiceMock, times(1)).removePrintoutTemplateFromPrograms(anyInt(),
                anySet());
    }

    @Test
    public void givenPrintoutTemplateExists_whenAddPrintoutTemplateToProducts_thenPrintoutTemplateAdded() {

        subject.addPrintoutTemplateToProducts(anyInt(), anySet());

        verify(printoutTemplateServiceMock, times(1)).addPrintoutTemplateToProducts(anyInt(),
                anySet());
    }

    @Test
    public void givenPrintoutTemplateExists_whenRemovePrintoutTemplateFromProducts_thenPrintoutTemplateRemoved() {

        subject.removePrintoutTemplateFromProducts(anyInt(), anySet());

        verify(printoutTemplateServiceMock, times(1)).removePrintoutTemplateFromProducts(anyInt(),
                anySet());
    }

    @Test
    public void givenCustomerInput_whenCreateCustomer_thenPersistCustomer() {

        subject.createCustomer(customerInputMock);

        verify(customerServiceMock, times(1)).createCustomer(customerInputMock);
    }

    @Test
    public void givenCustomerInput_whenModifyCustomer_thenPersistCustomer() {

        subject.modifyCustomer(customerInputMock);

        verify(customerServiceMock, times(1)).modifyCustomer(customerInputMock);
    }

    @Test
    public void givenCustomerExists_whenDeleteCustomer_thenCustomerDeleted() {

        subject.deleteCustomer(anyInt());

        verify(customerServiceMock, times(1)).deleteCustomer(anyInt());
    }

    public void givenAddressExists_whenRemoveAddress_thenAddressRemoved() {

        subject.removeAddress(anyInt(), anyInt());

        verify(contactServiceMock, times(1)).removeAddress(anyInt(), anyInt());
    }

    @Test
    public void givenHierarchyInput_whenCreateHierarchy_thenPersistHierarchy() {

        subject.createHierarchy(hierarchyInputMock);

        verify(hierarchyServiceMock, times(1)).createHierarchy(hierarchyInputMock);

    }

    @Test
    public void givenHierarchyInput_whenModifyHierarchy_thenPersistHierarchy() {

        subject.modifyHierarchy(hierarchyInputMock);

        verify(hierarchyServiceMock, times(1)).modifyHierarchy(hierarchyInputMock, false);

    }

    @Test
    public void givenHierarchyExists_whenDeleteHierarchy_thenHierarchyDeleted() {

        subject.deleteHierarchy(anyInt(), anyInt());

        verify(hierarchyServiceMock, times(1)).deleteHierarchy(anyInt(), anyInt());

    }

    @Test
    public void givenContactInfoExists_whenRemoveContactInfo_thenContactInfoRemoved() {

        subject.removeContactInfo(anyInt());

        verify(contactServiceMock, times(1)).removeContactInfo(anyInt());
    }

    @Test
    public void givenContactInfoTypeInput_whenCreateContactInfoType_thenPersistContactInfoTYpe() {

        subject.createContactInfoType(contactInfoTypeInputMock);

        verify(contactInfoTypeServiceMock, times(1)).createContactInfoType(contactInfoTypeInputMock);

    }

    @Test
    public void givenContactInfoTypeInput_whenModifyContactInfoType_thenPersistContactInfoTYpe() {

        subject.modifyContactInfoType(contactInfoTypeInputMock);

        verify(contactInfoTypeServiceMock, times(1)).modifyContactInfoType(contactInfoTypeInputMock);

    }

    @Test
    public void givenContactInfoTypeExists_whenDeleteContactInfoType_thenContactInfoTypeDeleted() {

        subject.deleteContactInfoType(anyInt());

        verify(contactInfoTypeServiceMock, times(1)).deleteContactInfoType(anyInt());

    }

    @Test
    public void givenContactInfoInput_whenCreateContactInfo_thenPersistContactInfo() {

        subject.createContactInfo(contactInfoInputMock);

        verify(contactInfoServiceMock, times(1)).createContactInfo(contactInfoInputMock);

    }

    @Test
    public void givenContactInfoInput_whenModifyContactInfo_thenPersistContactInfo() {

        subject.modifyContactInfo(contactInfoInputMock);

        verify(contactInfoServiceMock, times(1)).modifyContactInfo(contactInfoInputMock);

    }

    @Test
    public void givenContactInfoExists_whenDeleteContactInfo_thenContactInfoDeleted() {

        subject.deleteContactInfo(anyInt());

        verify(contactInfoServiceMock, times(1)).deleteContactInfo(anyInt());

    }

    @Test
    public void givenRoleProductInput_whenCreateRoleProduct_thenPersistRoleProduct() {

        subject.createRoleProduct(roleProductInputMock);

        verify(roleProductServiceMock, times(1)).createRoleProduct(roleProductInputMock);

    }

    @Test
    public void givenProductFunctionInput_whenCreateProductFunction_thenPersistProductFunction() {

        subject.createProductFunction(productFunctionInputMock);

        verify(productFunctionServiceMock, times(1)).createProductFunction(productFunctionInputMock);

    }

    @Test
    public void givenRoleInput_whenCreateRole_thenPersistRole() {

        subject.createRole(roleInputMock);

        verify(roleServiceMock, times(1)).createRole(roleInputMock);

    }

    @Test
    public void givenRoleInput_whenModifyRole_thenPersistRole() {

        subject.modifyRole(roleInputMock);

        verify(roleServiceMock, times(1)).modifyRole(roleInputMock);

    }

    @Test
    public void givenRoleExists_whenDeleteRole_thenRoleDeleted() {

        subject.deleteRole(anyInt());

        verify(roleServiceMock, times(1)).deleteRole(anyInt());

    }

    // @Test
    // public void givenValidInputs_whenAddProductsToTenant_thenProductsAdded() {
    // when(tenantServiceMock.addProducts(anyInt(), any())).thenReturn(new
    // TenantTo());

    // TenantTo result = subject.addProductsToTenant(1, Arrays.asList(1, 2, 3));

    // assertNotNull(result);
    // verify(tenantServiceMock, times(1)).addProducts(anyInt(), any());
    // }

    // @Test
    // public void
    // givenValidInputs_whenRemoveProductsFromTenant_thenProductsRemoved() {
    // when(tenantServiceMock.removeProducts(anyInt(), any())).thenReturn(new
    // TenantTo());

    // TenantTo result = subject.removeProductsFromTenant(1, Arrays.asList(1, 2,
    // 3));

    // assertNotNull(result);
    // verify(tenantServiceMock, times(1)).removeProducts(anyInt(), any());
    // }

    // @Test
    // public void
    // givenValidFunctionalUnit_whenCreateFunctionalUnit_thenPersistFunctionalUnit()
    // {
    // when(functionalUnitServiceMock.createFunctionalUnit(any())).thenReturn(new
    // FunctionalUnitTo());

    // FunctionalUnitTo result = subject.createFunctionalUnit(new
    // FunctionalUnitInput());

    // assertNotNull(result);
    // verify(functionalUnitServiceMock, times(1)).createFunctionalUnit(any());
    // }

    // @Test
    // public void
    // givenValidFunctionalUnit_whenModifyFunctionalUnit_thenPersistFunctionalUnit()
    // {
    // when(functionalUnitServiceMock.modifyFunctionalUnit(any())).thenReturn(new
    // FunctionalUnitTo());

    // FunctionalUnitTo result = subject.modifyFunctionalUnit(new
    // FunctionalUnitInput());

    // assertNotNull(result);
    // verify(functionalUnitServiceMock, times(1)).modifyFunctionalUnit(any());
    // }

    // @Test
    // public void
    // givenFunctionalUnitExists_whenDeleteFunctionalUnit_thenFunctionalUnitDeleted()
    // {
    // when(functionalUnitServiceMock.removeFunctionalUnit(anyInt())).thenReturn(1);

    // int result = subject.deleteFunctionalUnit(1);

    // assertNotNull(result);
    // verify(functionalUnitServiceMock, times(1)).removeFunctionalUnit(anyInt());
    // }

    // @Test
    // public void givenFunctionalUnitExists_whenAddUsersToUnit_thenUsersAdded() {
    // when(functionalUnitServiceMock.addUsers(anyInt(), any())).thenReturn(new
    // FunctionalUnitTo());

    // FunctionalUnitTo result = subject.addUsersToUnit(1, Arrays.asList(1, 2, 3));

    // assertNotNull(result);
    // verify(functionalUnitServiceMock, times(1)).addUsers(anyInt(), any());
    // }

    // @Test
    // public void
    // givenFunctionalUnitExists_whenRemoveUsersFromUnit_thenUsersRemoved() {
    // when(functionalUnitServiceMock.removeUsers(anyInt(), any())).thenReturn(new
    // FunctionalUnitTo());

    // FunctionalUnitTo result = subject.removeUsersFromUnit(1, Arrays.asList(1, 2,
    // 3));

    // assertNotNull(result);
    // verify(functionalUnitServiceMock, times(1)).removeUsers(anyInt(), any());
    // }

    // @Test
    // public void
    // givenFunctionalUnitExists_whenAddProductsToUnit_thenProductsAdded() {
    // when(functionalUnitServiceMock.addProductsToFunctionalUnit(anyInt(),
    // any())).thenReturn(new FunctionalUnitTo());

    // FunctionalUnitTo result = subject.addProductsToUnit(1, emptySet());

    // assertNotNull(result);
    // verify(functionalUnitServiceMock,
    // times(1)).addProductsToFunctionalUnit(anyInt(), any());
    // }

    // @Test
    // public void
    // givenFunctionalUnitExists_whenRemoveProductsFromUnit_thenProductsRemoved() {
    // when(functionalUnitServiceMock.removeProductsFromFunctionalUnit(anyInt(),
    // any()))
    // .thenReturn(new FunctionalUnitTo());

    // FunctionalUnitTo result = subject.removeProductsFromUnit(1, emptySet());

    // assertNotNull(result);
    // verify(functionalUnitServiceMock,
    // times(1)).removeProductsFromFunctionalUnit(anyInt(), any());
    // }

    @Test
    public void givenValidUserInput_whenCreateUser_thenPersistUser() {

        subject.createUser(userInputMock);

        verify(userServiceMock, times(1)).createUser(userInputMock);

    }

    @Test
    public void givenValidUserInput_whenModifyUser_thenPersistUser() {

        subject.modifyUser(userInputMock);

        verify(userServiceMock, times(1)).modifyUser(userInputMock);

    }

    @Test
    public void givenExistingUserId_whenDeleteUser_thenDeleteUser() {

        when(userServiceMock.deleteUser(anyInt())).thenReturn(1);

        int result = subject.deleteUser(1);

        assertEquals(1, result);

        verify(userServiceMock, times(1)).deleteUser(anyInt());

    }

    @Test
    public void givenDelegationInput_whenCreateDelegation_thenPersistDelegation() {

        subject.createDelegation(delegationInputMock);

        verify(delegationServiceMock, times(1)).createDelegation(delegationInputMock);

    }

    @Test
    void givenDelegationInput_whenModifyDelegation_thenPersistDelegation() {

        subject.modifyDelegation(delegationInputMock);

        verify(delegationServiceMock, times(1)).modifyDelegation(delegationInputMock);

    }

    @Test
    void givenDelegationExists_whenDeleteDelegation_thenDelegationDeleted() {

        when(subject.deleteDelegation(anyInt())).thenReturn(1);

        int result = subject.deleteDelegation(1);

        assertEquals(1, result);

        verify(delegationServiceMock, times(1)).deleteDelegation(anyInt());

    }

}