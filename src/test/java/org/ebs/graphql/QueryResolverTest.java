package org.ebs.graphql;

import static java.util.Collections.emptyList;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.ebs.services.AddressService;
import org.ebs.services.CFTypeService;
import org.ebs.services.CFValueService;
import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactInfoTypeService;
import org.ebs.services.ContactService;
import org.ebs.services.ContactTypeService;
// import org.ebs.services.CoreHierarchyService;
import org.ebs.services.CountryService;
import org.ebs.services.CropService;
import org.ebs.services.CustomerService;
import org.ebs.services.DelegationService;
import org.ebs.services.DomainService;
import org.ebs.services.EmailTemplateService;
import org.ebs.services.EntityReferenceService;
import org.ebs.services.EventService;
import org.ebs.services.FileTypeService;
import org.ebs.services.FilterService;
import org.ebs.services.FunctionalUnitService;
// import org.ebs.services.HierarchyDesignAttributesService;
// import org.ebs.services.HierarchyDesignService;
import org.ebs.services.HierarchyService;
// import org.ebs.services.HierarchyTreeAttributesValuesService;
// import org.ebs.services.HierarchyTreeService;
// import org.ebs.services.HierarchyTypeService;
import org.ebs.services.InstanceService;
import org.ebs.services.InstitutionService;
import org.ebs.services.IwinOccurrenceService;
import org.ebs.services.JobLogService;
import org.ebs.services.JobWorkflowService;
import org.ebs.services.NodeCFService;
import org.ebs.services.NodeService;
import org.ebs.services.NodeTypeService;
import org.ebs.services.OccurrenceShipmentService;
import org.ebs.services.OrganizationService;
import org.ebs.services.OrganizationalUnitService;
import org.ebs.services.PhaseService;
import org.ebs.services.PrintoutTemplateService;
import org.ebs.services.ProcessService;
import org.ebs.services.ProductFunctionService;
import org.ebs.services.ProductService;
import org.ebs.services.ProgramService;
import org.ebs.services.PurposeService;
import org.ebs.services.RoleService;
import org.ebs.services.SecurityRuleService;
import org.ebs.services.ServiceFileService;
import org.ebs.services.ServiceItemService;
import org.ebs.services.ServiceService;
import org.ebs.services.ShipmentFileService;
import org.ebs.services.ShipmentItemService;
import org.ebs.services.ShipmentService;
import org.ebs.services.StageService;
import org.ebs.services.StatusService;
import org.ebs.services.StatusTypeService;
import org.ebs.services.SynchronizeProgramService;
import org.ebs.services.SynchronizeUserService;
import org.ebs.services.TenantService;
import org.ebs.services.UserService;
import org.ebs.services.WorkflowInstanceService;
import org.ebs.services.WorkflowService;
import org.ebs.services.WorkflowViewTypeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class QueryResolverTest {

    @Mock
    private InstanceService instanceServiceMock;
    @Mock
    private UserService userServiceMock;
    @Mock
    private ContactService contactServiceMock;
    @Mock
    private ProductService productServiceMock;
    @Mock
    private TenantService tenantServiceMock;
    @Mock
    private EntityReferenceService entityReferenceServiceMock;
    @Mock
    private DomainService domainServiceMock;
    @Mock
    private CropService cropServiceMock;
    @Mock
    private ProgramService programServiceMock;
    @Mock
    private RoleService roleServiceMock;
    @Mock
    private SecurityRuleService securityRuleServiceMock;
    @Mock
    private PrintoutTemplateService printoutTemplateServiceMock;
    @Mock
    private CountryService countryService;
    @Mock
    private ContactTypeService contactTypeServiceMock;
    @Mock
    private CustomerService customerServiceMock;
    @Mock
    private OrganizationService organizationServiceMock;
    @Mock
    private PurposeService purposeServiceMock;
    @Mock
    private HierarchyService hierarchyServiceMock;
    @Mock
    private ContactInfoTypeService contactInfoTypeServiceMock;
    @Mock
    private ContactInfoService contactInfoServiceMock;
    @Mock
    private DelegationService delegationServiceMock;
    @Mock
    private ProductFunctionService productFunctionServiceMock;
    @Mock
    private FunctionalUnitService functionalUnitServiceMock;
    // @Mock
    // private CoreHierarchyService coreHierarchyServiceMock;
    // @Mock
    // private HierarchyTypeService hierarchyTypeServiceMock;
    // @Mock
    // private HierarchyDesignService hierarchyDesignServiceMock;
    // @Mock
    // private HierarchyTreeService hierarchyTreeServiceMock;
    @Mock
    private FilterService filterServiceMock;
    @Mock
    private JobWorkflowService jobWorkflowServiceMock;
    @Mock
    private JobLogService jobLogServiceMock;
    @Mock
    private WorkflowInstanceService workflowInstanceServiceMock;
    @Mock
    private EventService eventServiceMock;
    @Mock
    private StageService stageServiceMock;
    @Mock
    private ShipmentService shipmentServiceMock;
    @Mock
    private ShipmentItemService shipmentItemServiceMock;
    @Mock
    private ServiceItemService serviceItemServiceMock;
    @Mock
    private ShipmentFileService shipmentFileServiceMock;
    @Mock
    private ServiceFileService filesServiceMock;
    @Mock
    private EmailTemplateService emailTemplateServiceMock;
    @Mock
    private CFTypeService cfTypeServiceMock;
    @Mock
    private WorkflowViewTypeService workflowViewTypeServiceMock;
    @Mock
    private NodeTypeService nodeTypeServiceMock;
    @Mock
    private NodeService nodeServiceMock;
    @Mock
    private WorkflowService workflowServiceMock;
    @Mock
    private NodeCFService nodeCFServiceMock;
    @Mock
    private CFValueService cfValueServiceMock;
    @Mock
    private StatusTypeService statusTypeServiceMock;
    @Mock
    private StatusService statusServiceMock;
    @Mock
    private PhaseService phaseServiceMock;
    @Mock
    private ServiceService serviceServiceMock;
    // @Mock
    // private HierarchyDesignAttributesService hierarchyDesignAttributesServiceMock;
    // @Mock
    // private HierarchyTreeAttributesValuesService hierarchyTreeAttributesValuesServiceMock;
    @Mock
    private ProcessService processServiceMock;
    @Mock
    private OrganizationalUnitService organizationalUnitServiceMock;
    @Mock
    private FileTypeService fileTypeServiceMock;
    @Mock
    private AddressService addressServiceMock;
    @Mock
    private InstitutionService institutionServiceMock;
    @Mock
    private SynchronizeUserService synchronizeUserServiceMock;
    @Mock
    private SynchronizeProgramService synchronizeProgramServiceMock;
    @Mock
    private OccurrenceShipmentService occurrenceShipmentServiceMock;
    @Mock
    private IwinOccurrenceService iwinOccurrenceServiceMock;

    QueryResolver subject;

    @BeforeEach
    public void init() {
        subject = new QueryResolver(instanceServiceMock, userServiceMock, contactServiceMock,
                productServiceMock, tenantServiceMock, entityReferenceServiceMock,
                domainServiceMock, cropServiceMock, programServiceMock, roleServiceMock,securityRuleServiceMock,
                printoutTemplateServiceMock, countryService, contactTypeServiceMock,
                customerServiceMock, organizationServiceMock,purposeServiceMock, hierarchyServiceMock, 
                contactInfoTypeServiceMock, contactInfoServiceMock, delegationServiceMock,
                productFunctionServiceMock, functionalUnitServiceMock,
                //  coreHierarchyServiceMock,
                // hierarchyTypeServiceMock, hierarchyDesignServiceMock, hierarchyTreeServiceMock,
                filterServiceMock, jobWorkflowServiceMock, jobLogServiceMock,
                workflowInstanceServiceMock, eventServiceMock, stageServiceMock, shipmentServiceMock,
                shipmentItemServiceMock,serviceItemServiceMock, shipmentFileServiceMock,filesServiceMock, emailTemplateServiceMock,
                cfTypeServiceMock, workflowViewTypeServiceMock,nodeServiceMock, workflowServiceMock,
                nodeCFServiceMock, nodeTypeServiceMock, cfValueServiceMock, statusTypeServiceMock, statusServiceMock,
                phaseServiceMock, serviceServiceMock,
                //  hierarchyDesignAttributesServiceMock,
                // hierarchyTreeAttributesValuesServiceMock,
                 processServiceMock, organizationalUnitServiceMock,
                fileTypeServiceMock, addressServiceMock, institutionServiceMock, synchronizeUserServiceMock,
                synchronizeProgramServiceMock, occurrenceShipmentServiceMock, iwinOccurrenceServiceMock);
    }

    // CROP
    @Test
    public void givenCropExists_whenFindCrop_thenReturnCrop() {

        subject.findCrop(1);

        verify(cropServiceMock, times(1)).findCrop(anyInt());
    }

    @Test
    public void givenCropExists_whenFindCropList_thenReturnCrops() {

        subject.findCropList(null, emptyList(), emptyList(), false);

        verify(cropServiceMock, times(1)).findCrops(isNull(), anyList(), anyList(), anyBoolean());
    }

    // DOMAIN
    @Test
    public void givenDomainExists_whenFindDomain_thenReturnDomain() {

        subject.findDomain(1);

        verify(domainServiceMock, times(1)).findDomain(anyInt());
    }

    @Test
    public void givenDomainExists_whenFindDomainList_thenReturnDomains() {

        subject.findDomainList(null, emptyList(), emptyList(), false);

        verify(domainServiceMock, times(1)).findDomains(isNull(), anyList(), anyList(),
                anyBoolean());
    }

    // ENTITY REFERENCE
    @Test
    public void givenEntityReferenceExists_whenFindEntityReference_thenReturnEntityReference() {

        subject.findEntityReference(1);

        verify(entityReferenceServiceMock, times(1)).findEntityReference(anyInt());
    }

    @Test
    public void givenEntityReferenceExists_whenFindEntityReferenceList_thenReturnEntityReferences() {

        subject.findEntityReferenceList(null, emptyList(), emptyList(), false);

        verify(entityReferenceServiceMock, times(1)).findEntityReferences(isNull(), anyList(),
                anyList(), anyBoolean());
    }

    // INSTANCE
    @Test
    public void givenInstanceExists_whenFindInstance_thenReturnInstance() {

        subject.findInstance(1);

        verify(instanceServiceMock, times(1)).findInstance(anyInt());
    }

    @Test
    public void givenInstanceExists_whenFindInstanceList_thenReturnInstances() {

        subject.findInstanceList(null, emptyList(), emptyList(), false);

        verify(instanceServiceMock, times(1)).findInstances(isNull(), anyList(), anyList(),
                anyBoolean());
    }

    // PRODUCT
    @Test
    public void givenProductExists_whenFindProduct_thenReturnProduct() {

        subject.findProduct(1);

        verify(productServiceMock, times(1)).findProduct(anyInt());
    }

    @Test
    public void givenProductExists_whenFindProductList_thenReturnProducts() {

        subject.findProductList(null, emptyList(), emptyList(), false);

        verify(productServiceMock, times(1)).findProducts(isNull(), anyList(), anyList(),
                anyBoolean());
    }

    // PROGRAM
    @Test
    public void givenProgramExists_whenFindProgram_thenReturnProgram() {

        subject.findProgram(1);

        verify(programServiceMock, times(1)).findProgram(anyInt());
    }

    @Test
    public void givenProgramExists_whenFindProgramList_thenReturnPrograms() {

        subject.findProgramList(null, emptyList(), emptyList(), false);

        verify(programServiceMock, times(1)).findPrograms(isNull(), anyList(), anyList(),
                anyBoolean());
    }

    // ROLE
    @Test
    public void givenRoleExists_whenFindRole_thenReturnRole() {

        subject.findRole(1);

        verify(roleServiceMock, times(1)).findRole(anyInt());
    }

    @Test
    public void givenRoleExists_whenFindRoleList_thenReturnRoles() {

        subject.findRoleList(null, emptyList(), emptyList(), false);

        verify(roleServiceMock, times(1)).findRoles(isNull(), anyList(), anyList(), anyBoolean());
    }

    // TENANT
    @Test
    public void givenTenantExists_whenFindTenant_thenReturnTenant() {

        subject.findTenant(1);

        verify(tenantServiceMock, times(1)).findTenant(anyInt());
    }

    @Test
    public void givenTenantExists_whenFindTenantList_thenReturnTenants() {

        subject.findTenantList(null, emptyList(), emptyList(), false);

        verify(tenantServiceMock, times(1)).findTenants(isNull(), anyList(), anyList(),
                anyBoolean());
    }

    // USER
    @Test
    public void givenUserExists_whenFindUser_thenReturnUser() {

        subject.findUser(1);

        verify(userServiceMock, times(1)).findUser(anyInt());
    }

    @Test
    public void givenUserExists_whenFindUserList_thenReturnUsers() {

        subject.findUserList(null, emptyList(), emptyList(), false);

        verify(userServiceMock, times(1)).findUsers(isNull(), anyList(), anyList(), anyBoolean());
    }

    // CONTACT
    @Test
    public void givenContactExists_whenFindContact_thenReturnContact() {

        subject.findContact(1);

        verify(contactServiceMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenContactExists_whenFindContactList_thenReturnContacts() {

        subject.findContactList(null, emptyList(), emptyList(), false);

        verify(contactServiceMock, times(1)).findContacts(isNull(), anyList(), anyList(),
                anyBoolean());
    }

    // PRINTOUT TEMPLATE
    @Test
    public void givenPrintoutTemplateExists_whenFindPrintoutTemplate_thenReturnPrintoutTemplate() {

        subject.findPrintoutTemplate(1);

        verify(printoutTemplateServiceMock, times(1)).findPrintoutTemplate(anyInt());
    }

    @Test
    public void givenPrintoutTemplateExists_whenFindPrintoutTemplateList_thenReturnPrintoutTemplates() {

        subject.findPrintoutTemplateList(null, emptyList(), emptyList(), false);

        verify(printoutTemplateServiceMock, times(1)).findPrintoutTemplates(isNull(), anyList(),
                anyList(), anyBoolean());
    }

    @Test
    public void givenContactInfoTypeExists_whenFindContactInfoType_thenReturnContactInfoType() {
        subject.findContactInfoType(anyInt());

        verify(contactInfoTypeServiceMock, times(1)).findContactInfoType(anyInt());
    }

    @Test
    public void givenContactInfoTypeExists_whenFindContactInfoTypeList_thenReturnContactInfoTypes() {
        subject.findContactInfoTypeList(null, emptyList(), emptyList(), false);

        verify(contactInfoTypeServiceMock, times(1)).findContactInfoTypes(isNull(), anyList(), anyList(), anyBoolean());
    }

    @Test
    public void givenContactInfoExists_whenFindContactInfo_thenReturnContactInfo() {
        subject.findContactInfo(anyInt());

        verify(contactInfoServiceMock, times(1)).findContactInfo(anyInt());
    }

    @Test
    public void givenContactInfoExists_whenFindContactInfoList_thenReturnContactInfos() {
        subject.findContactInfoList(null, emptyList(), emptyList(), false);

        verify(contactInfoServiceMock, times(1)).findContactInfos(isNull(), anyList(), anyList(), anyBoolean());
    }

    @Test void givenDelegationExists_whenFindDelegation_thenReturnDelegation() {

        subject.findDelegation(anyInt());

        verify(delegationServiceMock, times(1)).findDelegation(anyInt());

    }

    @Test void givenDelegationExists_whenFindDelegationList_thenReturnDelegations() {

        subject.findDelegationList(null, emptyList(), emptyList(), false);

        verify(delegationServiceMock, times(1)).findDelegations(isNull(), anyList(), anyList(), anyBoolean());

    }

}