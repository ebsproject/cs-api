package org.ebs.graphql.resolvers;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.TenantService;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.OrganizationTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.TenantTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TenantResolverTest {

    @Mock private TenantService tenantServiceMock;
    @Mock private TenantTo tenantMock;

    private TenantResolver subject;

    @BeforeEach
    public void init() {
        subject = new TenantResolver(tenantServiceMock);
    }

    @Test
    public void givenTenantHasCustomer_whenGetCustomer_thenReturnCustomer() {
        when(tenantServiceMock.findCustomer(anyInt()))
            .thenReturn(Optional.of(new CustomerTo()));

        CustomerTo result = subject.getCustomer(tenantMock);

        assertNotNull(result);
        verify(tenantServiceMock, times(1)).findCustomer(anyInt());
    }

    @Test
    public void givenTenantHasNoCustomer_whenGetCustomer_thenReturnNull() {

        CustomerTo result = subject.getCustomer(tenantMock);

        assertNull(result);
        verify(tenantServiceMock, times(1)).findCustomer(anyInt());
    }

    @Test
    public void givenTenantHasOrganization_whenGetOrganization_thenReturnOrganization() {
        when(tenantServiceMock.findOrganization(anyInt()))
            .thenReturn(Optional.of(new OrganizationTo()));

        OrganizationTo result = subject.getOrganization(tenantMock);

        assertNotNull(result);
        verify(tenantServiceMock, times(1)).findOrganization(anyInt());
    }

    @Test
    public void givenTenantHasNoOrganization_whenGetOrganization_thenReturnNull() {

        OrganizationTo result = subject.getOrganization(tenantMock);

        assertNull(result);
        verify(tenantServiceMock, times(1)).findOrganization(anyInt());
    }

    @Test
    public void givenTenantHasProducts_whenGetProducts_thenReturnProducts() {
        when(tenantServiceMock.findProducts(anyInt()))
            .thenReturn(new HashSet<>(asList(new ProductTo(1), new ProductTo(2))));

        Set<ProductTo> result = subject.getProducts(tenantMock);

        assertThat(result).hasSize(2);
        verify(tenantServiceMock, times(1)).findProducts(anyInt());
    }

    @Test
    public void givenTenantHasNoProducts_whenGetProducts_thenReturnEmpty() {

        Set<ProductTo> result = subject.getProducts(tenantMock);

        assertNotNull(result);
        assertThat(result).isEmpty();
        verify(tenantServiceMock, times(1)).findProducts(anyInt());
    }


}