package org.ebs.graphql.resolvers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactInfoTypeService;
import org.ebs.services.to.ContactInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ContactInfoResolverTest {

    @Mock
    private ContactInfoTypeService contactInfoTypeServiceMock;
    @Mock
    private ContactInfoService contactInfoServiceMock;
    @Mock
    private ContactInfo contactInfoMock;

    private ContactInfoResolver subject;

    @BeforeEach
    public void init() {
        subject = new ContactInfoResolver(contactInfoTypeServiceMock, contactInfoServiceMock);
    }

    @Test
    public void givenContactInfoHasInfoType_whenGetContactInfoType_thenReturnContactInfoType() {

        subject.getContactInfoType(contactInfoMock);

        verify(contactInfoTypeServiceMock, times(1)).findByContactInfo(anyInt());
    }

    @Test
    public void givenContactInfoHasContact_whenGetContact_thenReturnContact() {
        subject.getContact(contactInfoMock);

        verify(contactInfoServiceMock, times(1)).findContactInfoContact(anyInt());
    }

}