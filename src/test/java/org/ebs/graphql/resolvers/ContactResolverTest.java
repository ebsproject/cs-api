package org.ebs.graphql.resolvers;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.ebs.services.AddressService;
import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactService;
import org.ebs.services.ContactTypeService;
import org.ebs.services.HierarchyService;
import org.ebs.services.TenantService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.Institution;
import org.ebs.services.to.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ContactResolverTest {

    @Mock
    private ContactService contactServiceMock;
    @Mock
    private ContactTypeService contactTypeServiceMock;
    @Mock
    private AddressService addressServiceMock;
    @Mock
    private ContactInfoService contactInfoServiceMock;
    @Mock
    private HierarchyService hierarchyServiceMock;
    @Mock
    private TenantService tenantServiceMock;
    @Mock
    Contact contactMock;
    private ContactResolver subject;

    @BeforeEach
    public void init() {
        subject = new ContactResolver(contactServiceMock, contactTypeServiceMock, addressServiceMock,
                contactInfoServiceMock, tenantServiceMock);
    }

    @Test
    public void givenContactHasContactType_whenGetContactTypes_thenReturnContactTypes() {

        subject.getContactTypes(contactMock);

        verify(contactTypeServiceMock, times(1)).findByContact(anyInt());
    }

    @Test
    public void givenContactHasAddress_whenGetAddresss_thenReturnAddresss() {

        subject.getAddresses(contactMock);

        verify(addressServiceMock, times(1)).findByContact(anyInt());
    }

    @Test
    public void givenContactHasContactInfo_whenGetContactInfos_thenReturnContactInfos() {

        subject.getContactInfos(contactMock);

        verify(contactInfoServiceMock, times(1)).findByContact(anyInt());
    }

    @Test
    public void givenContactHasPerson_whenGetPerson_thenReturnPerson() {

        subject.getPerson(contactMock);

        verify(contactServiceMock, times(1)).findById(anyInt());

    }

    @Test
    public void givenContactHasNoPerson_whenGetPerson_thenReturnNull() {
        when(contactServiceMock.findById(anyInt())).thenReturn(contactMock);

        Person result = subject.getPerson(contactMock);

        assertNull(result);
        verify(contactServiceMock, times(1)).findById(anyInt());

    }

    @Test
    public void givenContactHasInstitution_whenGetInstitution_thenReturnInstitution() {

        subject.getInstitution(contactMock);

        verify(contactServiceMock, times(1)).findById(anyInt());

    }

    @Test
    public void givenContactHasNoInstitution_whenGetInstitution_thenReturnNull() {
        when(contactServiceMock.findById(anyInt())).thenReturn(contactMock);

        Institution result = subject.getInstitution(contactMock);

        assertNull(result);
        verify(contactServiceMock, times(1)).findById(anyInt());

    }

}