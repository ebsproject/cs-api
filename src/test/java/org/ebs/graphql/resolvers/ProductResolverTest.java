package org.ebs.graphql.resolvers;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.ebs.services.ProductService;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.ProductTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductResolverTest {

    @Mock
    private ProductService productService;
    @Mock
    private ProductTo productMock;

    private ProductResolver subject;

    @BeforeEach
    public void init() {
        subject = new ProductResolver(productService);
    }

    @Test
    public void givenProductHasDomain_whenGetDomain_thenReturnDomain() {
        when(productService.findDomain(anyInt())).thenReturn(Optional.of(new DomainTo()));

        DomainTo result = subject.getDomain(productMock);

        assertNotNull(result);
        verify(productService, times(1)).findDomain(anyInt());
    }

    @Test
    public void givenProductHasNoDomain_whenGetDomain_thenReturnNull() {

        DomainTo result = subject.getDomain(productMock);

        assertNull(result);
        verify(productService, times(1)).findDomain(anyInt());
    }

    @Test
    public void givenProductHasHtmltag_whenGetHtmltag_thenReturnHtmltag() {
        when(productService.findHtmlTag(anyInt())).thenReturn(Optional.of(new HtmlTagTo()));

        HtmlTagTo result = subject.getHtmltag(productMock);

        assertNotNull(result);
        verify(productService, times(1)).findHtmlTag(anyInt());
    }

    @Test
    public void givenProductHasNoHtmltag_whenGetHtmltag_thenReturnNull() {

        HtmlTagTo result = subject.getHtmltag(productMock);

        assertNull(result);
        verify(productService, times(1)).findHtmlTag(anyInt());
    }
}