package org.ebs.graphql.resolvers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.ebs.services.ContactService;
import org.ebs.services.TenantService;
import org.ebs.services.UserService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserResolverTest {

    @Mock
    private ContactService contactServiceMock;
    @Mock
    private TenantService tenantServiceMock;
    @Mock
    private UserTo userMock;
    @Mock
    private UserService userServiceMock;

    private UserResolver subject;

    @BeforeEach
    public void init() {
        subject = new UserResolver(contactServiceMock, tenantServiceMock, userServiceMock);
    }

    @Test
    public void givenUserHasNoContact_whenGetContact_thenReturnNull() {

        Contact result = subject.getContact(userMock);

        assertNull(result);
        verify(contactServiceMock, times(1)).findByUser(anyInt());
    }

    @Test
    public void givenUserHasContact_whenGetContact_thenReturnContact() {
        when(contactServiceMock.findByUser(anyInt())).thenReturn(new Contact());

        Contact result = subject.getContact(userMock);

        assertNotNull(result);
        verify(contactServiceMock, times(1)).findByUser(anyInt());
    }

    @Test
    public void givenUserHasNoTenant_whenGetTenants_thenReturnNull() {

        List<TenantTo> result = subject.getTenants(userMock);

        assertThat(result).isEmpty();
        verify(tenantServiceMock, times(1)).findByUser(anyInt());
    }

    @Test
    public void givenUserHasTenant_whenGetTenants_thenReturnTenants() {

        when(tenantServiceMock.findByUser(anyInt())).thenReturn(Arrays.asList(new TenantTo()));
        List<TenantTo> result = subject.getTenants(userMock);

        assertThat(result).hasSize(1);
        verify(tenantServiceMock, times(1)).findByUser(anyInt());
    }

}