package org.ebs.graphql.resolvers;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.ebs.services.AddressService;
import org.ebs.services.CountryService;
import org.ebs.services.to.Address;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AddressResolverTest {

    @Mock
    private CountryService countryServiceMock;
    @Mock
    private AddressService addressServiceMock;
    @Mock
    private Address AddressMock;

    private AddressResolver subject;

    @BeforeEach
    public void init() {
        subject = new AddressResolver(countryServiceMock,
                    addressServiceMock);
    }

    @Test
    public void givenAddressHasCountry_whenGetCountry_thenReturnCountry() {

        subject.getCountry(AddressMock);

        verify(countryServiceMock, times(1)).findByAddress(anyInt());
    }

}