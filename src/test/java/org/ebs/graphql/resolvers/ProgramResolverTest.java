package org.ebs.graphql.resolvers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.Set;

import org.ebs.services.ProgramService;
import org.ebs.services.to.CropProgramTo;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.ProjectTo;
import org.ebs.services.to.TenantTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProgramResolverTest {

    /* @Mock
    private ProgramService programServiceMock;
    @Mock
    private ProgramTo programMock;

    private ProgramResolver subject;

    @BeforeEach
    public void init() {
        subject = new ProgramResolver(programServiceMock);
    }

    @Test
    public void givenProgramHasCropProgram_whenGetCropProgram_thenReturnCropProgram() {
        when(programServiceMock.findCropProgram(anyInt()))
                .thenReturn(Optional.of(new CropProgramTo()));

        CropProgramTo result = subject.getCropProgram(programMock);

        assertNotNull(result);
        verify(programServiceMock, times(1)).findCropProgram(anyInt());
    }

    @Test
    public void givenProgramHasNoCropProgram_whenGetCropProgram_thenReturnNull() {

        CropProgramTo result = subject.getCropProgram(programMock);

        assertNull(result);
        verify(programServiceMock, times(1)).findCropProgram(anyInt());
    }

    @Test
    public void givenProgramHasNoProject_whenGetProjects_thenReturnEmpty() {

        Set<ProjectTo> result = subject.getProjects(programMock);

        assertThat(result).isEmpty();
        verify(programServiceMock, times(1)).findProjects(anyInt());
    }

    @Test
    public void givenProgramHasTenant_whenGetTenant_thenReturnTenant() {
        when(programServiceMock.findTenant(anyInt())).thenReturn(Optional.of(new TenantTo()));

        TenantTo result = subject.getTenant(programMock);

        assertNotNull(result);
        verify(programServiceMock, times(1)).findTenant(anyInt());
    }

    @Test
    public void givenProgramHasNoTenant_whenGetTenant_thenReturnNull() {

        TenantTo result = subject.getTenant(programMock);

        assertNull(result);
        verify(programServiceMock, times(1)).findTenant(anyInt());
    } */

}