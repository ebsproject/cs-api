package org.ebs.graphql.resolvers;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.ebs.services.FunctionalUnitService;
import org.ebs.services.ProductService;
import org.ebs.services.UserService;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.UserTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FunctionalUnitResolverTest {

    @Mock private FunctionalUnitService functionalUnitServiceMock;
    @Mock private UserService userServiceMock;
    @Mock private ProductService productServiceMock;

    @Mock private FunctionalUnitTo functionalUnitMock;

    private FunctionalUnitResolver subject;

    @BeforeEach
    public void init() {
        subject = new FunctionalUnitResolver(functionalUnitServiceMock, userServiceMock, productServiceMock);
    }

    @Test
    public void givenFunctionalUnitHasSubUnits_whenGetSubUnits_thenReturnSubUnits() {
        when(functionalUnitServiceMock.findByParent(anyInt()))
            .thenReturn(new HashSet<>(asList(new FunctionalUnitTo(1), new FunctionalUnitTo(2))));
        Set<FunctionalUnitTo> result = subject.getSubUnits(functionalUnitMock);

        assertThat(result).hasSize(2);
        verify(functionalUnitServiceMock, times(1)).findByParent(anyInt());
    }

    @Test
    public void givenFunctionalUnitHasNoSubUnits_whenGetSubUnits_thenReturnEmpty() {

        Set<FunctionalUnitTo> result = subject.getSubUnits(functionalUnitMock);

        assertNotNull(result);
        assertThat(result).isEmpty();
        verify(functionalUnitServiceMock, times(1)).findByParent(anyInt());
    }


    @Test
    public void givenFunctionalUnitHasUser_whenGetUsers_thenReturnUser() {
        when(userServiceMock.findByFunctionalUnit(anyInt()))
            .thenReturn(new HashSet<>(Arrays.asList(new UserTo(1), new UserTo(2))));

        Set<UserTo> result = subject.getUsers(functionalUnitMock);

        assertThat(result).hasSize(2);
        verify(userServiceMock, times(1)).findByFunctionalUnit(anyInt());
    }

    @Test
    public void givenFunctionalUnitHasNoUser_whenGetUsers_thenReturnEmpty() {

        Set<UserTo> result = subject.getUsers(functionalUnitMock);

        assertNotNull(result);
        assertThat(result).isEmpty();
        verify(userServiceMock, times(1)).findByFunctionalUnit(anyInt());
    }


    @Test
    public void givenFunctionalUnitHasProduct_whenGetProducts_thenReturnProduct() {
        when(productServiceMock.findProductsByFunctionalUnit(anyInt()))
            .thenReturn(new HashSet<>(Arrays.asList(new ProductTo(1), new ProductTo(2))));

        Set<ProductTo> result = subject.getProducts(functionalUnitMock);

        assertThat(result).hasSize(2);
        verify(productServiceMock, times(1)).findProductsByFunctionalUnit(anyInt());
    }

    @Test
    public void givenFunctionalUnitHasNoProduct_whenGetProducts_thenReturnEmpty() {

        Set<ProductTo> result = subject.getProducts(functionalUnitMock);

        assertNotNull(result);
        assertThat(result).isEmpty();
        verify(productServiceMock, times(1)).findProductsByFunctionalUnit(anyInt());
    }
}