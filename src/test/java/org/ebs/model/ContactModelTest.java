package org.ebs.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContactModelTest {

    private ContactModel subject;

    @BeforeEach
    public void init() {
        subject = new ContactModel();
        List<AddressModel> addresses = List.of(new AddressModel(), new AddressModel(),
                new AddressModel(), new AddressModel());
        subject.setAddresses(addresses);

        List<ContactInfoModel> contactInfos = List.of(new ContactInfoModel(),
                new ContactInfoModel(), new ContactInfoModel());
        subject.setContactInfos(contactInfos);

    }

    @Test
    public void givenDeletedAddresses_whenGetAddresses_thenFilteredDeleted() {

        subject.getAddresses().get(2).setDeleted(true);

        assertEquals(3, subject.getAddresses().size());

    }

    @Test
    public void givenNonDeletedAddresses_whenGetAddresses_thenReturnAll() {

        assertEquals(4, subject.getAddresses().size());

    }

    @Test
    public void givenDeletedContactInfos_whenGetContactInfos_thenFilteredDeleted() {

        subject.getContactInfos().get(1).setDeleted(true);

        assertEquals(2, subject.getContactInfos().stream().filter(ci -> !ci.isDeleted()).collect(Collectors.toList()).size());

    }

    @Test
    public void givenNonDeletedContactInfoes_whenGetContactInfoes_thenReturnAll() {

        assertEquals(3, subject.getContactInfos().size());

    }

    @Test
    public void givenExistingAddresses_whenSetAddresses_thenNewAddressesAdded() {
        subject.setAddresses(List.of(new AddressModel(), new AddressModel()));

        assertEquals(6, subject.getAddresses().size());
    }

    @Test
    public void givenExistingContactInfos_whenSetContactInfos_thenNewContactInfosAdded() {
        subject.setContactInfos(List.of(new ContactInfoModel(), new ContactInfoModel()));

        assertEquals(5, subject.getContactInfos().size());
    }
}
