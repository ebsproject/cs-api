package org.ebs.services;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactModel;
import org.ebs.model.ContactWorkflowModel;
import org.ebs.model.CropModel;
import org.ebs.model.DomainInstanceModel;
import org.ebs.model.DomainModel;
import org.ebs.model.HierarchyModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.PersonModel;
import org.ebs.model.ProductFunctionModel;
import org.ebs.model.ProductModel;
import org.ebs.model.RoleModel;
import org.ebs.model.RoleProductModel;
import org.ebs.model.UnitTypeModel;
import org.ebs.model.UserModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.ContactWorkflowRepository;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class UserProfileServiceImplTest {

    private @Mock UserRepository userRepositoryMock;
    private @Mock DomainRepository domainRepositoryMock;
    private @Mock ProductRepository productRepositoryMock;
    private @Mock InstitutionRepository institutionRepositoryMock;
    private @Mock ContactRepository contactRepositoryMock;
    private @Mock CropRepository cropRepositoryMock;
    private @Mock ContactWorkflowRepository contactWorkflowRepositoryMock;
    
    private UserProfileServiceImpl subject;

    @BeforeEach
    public void init() {
        subject = new UserProfileServiceImpl(userRepositoryMock, domainRepositoryMock, productRepositoryMock, institutionRepositoryMock, contactRepositoryMock, cropRepositoryMock, contactWorkflowRepositoryMock);
    }

    /**
     * Test that verifies not null result on the most basic user profile
     * with just a user and contact records created
     */
    @Test
    public void givenUserExistsWithoutNotNullInformation_whenBuildUserProfile_thenReturnNotNull() {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        user.setUserName("mock@test.com");
        user.setContact(contact);
        List<HierarchyModel> parents = new ArrayList<>();
        contact.setParents(parents);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        
        String object = subject.buildUserProfile(1, null, null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on a more elaborate user profile
     * Non admin user with program, role assignations
     */
    @Test
    public void givenUserExists_whenBuildUserProfile_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("CS");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        unitParent.setContactWorkflows(new ArrayList<>());
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        String object = subject.buildUserProfile(1, null, null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on a more elaborate user profile
     * Non admin user with program, role assignations
     * User can request genotyping services via a program
     */
    @Test
    public void givenUserExistsWithServiceProvidersViaProgram_whenBuildUserProfile_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("CS");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        unitParent.setContactWorkflows(new ArrayList<>());
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        JsonNode securityDefinition = om.readTree("{\"programs\":[{\"id\":2}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        String object = subject.buildUserProfile(1, null, null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on a more elaborate user profile
     * Non admin user with program, role assignations
     * User can request genotyping services via a role
     */
    @Test
    public void givenUserExistsWithServiceProvidersViaRole_whenBuildUserProfile_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("CS");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        unitParent.setContactWorkflows(new ArrayList<>());
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        JsonNode securityDefinition = om.readTree("{\"roles\":[{\"name\":\"User\"}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        String object = subject.buildUserProfile(1, null, null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on a more elaborate user profile
     * Non admin user with program, role assignations
     * User can request genotyping services via its id
     */
    @Test
    public void givenUserExistsWithServiceProviders_whenBuildUserProfile_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("CS");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        wf.setId(583);
        JsonNode securityDefinition = om.readTree("{\"users\":[{\"id\":1}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        unitParent.setContactWorkflows(new ArrayList<>(Arrays.asList(cw)));
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        String object = subject.buildUserProfile(1, null, null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on a more elaborate user profile with domain prefix filter
     * Non admin user with program, role assignations
     */
    @Test
    public void givenUserExists_whenBuildUserProfileWithDomainFilter_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("cs");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        wf.setId(583);
        JsonNode securityDefinition = om.readTree("{\"users\":[{\"id\":1}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        unitParent.setContactWorkflows(new ArrayList<>(Arrays.asList(cw)));
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        String object = subject.buildUserProfile(1, "cs", null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on a more elaborate user profile
     * Non admin user with program, role assignations with programDbIdFilter
     */
    @Test
    public void givenUserExists_whenBuildUserProfileWithProgramDbIdFilter_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("CS");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setExternalCode(101);
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        wf.setId(583);
        JsonNode securityDefinition = om.readTree("{\"users\":[{\"id\":1}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        unitParent.setContactWorkflows(new ArrayList<>(Arrays.asList(cw)));
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        String object = subject.buildUserProfile(1, null, 101);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on an admin user profile
     */
    @Test
    public void givenAdminUserExists_whenBuildUserProfile_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setAdmin(true);
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("CS");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        product.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        List<ContactModel> allUnits = new ArrayList<>(Arrays.asList(programParent, unitParent));
        when(contactRepositoryMock.findByCategoryNameAndDeletedIsFalseAndInstitutionUnitTypeNotNullAndInstitutionDeletedIsFalse(anyString())).thenReturn(allUnits);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        wf.setId(583);
        JsonNode securityDefinition = om.readTree("{\"users\":[{\"id\":1}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        unitParent.setContactWorkflows(new ArrayList<>(Arrays.asList(cw)));
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        when(domainRepositoryMock.findAll()).thenReturn(List.of(domain));
        String object = subject.buildUserProfile(1, null, null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on an admin user profile with domain prefix filter
     */
    @Test
    public void givenAdminUserExists_whenBuildUserProfileWithDomainFilter_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setAdmin(true);
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("cs");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        product.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        List<ContactModel> allUnits = new ArrayList<>(Arrays.asList(programParent, unitParent));
        when(contactRepositoryMock.findByCategoryNameAndDeletedIsFalseAndInstitutionUnitTypeNotNullAndInstitutionDeletedIsFalse(anyString())).thenReturn(allUnits);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        wf.setId(583);
        JsonNode securityDefinition = om.readTree("{\"users\":[{\"id\":1}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        unitParent.setContactWorkflows(new ArrayList<>(Arrays.asList(cw)));
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        when(domainRepositoryMock.findAll()).thenReturn(List.of(domain));
        String object = subject.buildUserProfile(1, "cs", null);
        assertNotNull(object);
    }

    /**
     * Test that verifies not null result on an admin user profile with programDbId filter
     */
    @Test
    public void givenAdminUserExists_whenBuildUserProfileWithProgramDbIdFilter_thenReturnNotNull() throws JsonMappingException, JsonProcessingException {
        UserModel user = new UserModel(1);
        ContactModel contact = new ContactModel(1);
        PersonModel person = new PersonModel();
        ObjectMapper om = new ObjectMapper();
        JsonNode preference = om.readTree("{\"tenant\":1}");
        user.setAdmin(true);
        user.setPreference(preference);
        person.setFullName("Doe, John");
        contact.setPerson(person);
        user.setUserName("mock@test.com");
        user.setExternalId(10);
        List<HierarchyModel> parents = new ArrayList<>();
        ContactModel programParent = new ContactModel(2);
        ContactModel unitParent = new ContactModel(3);
        UnitTypeModel programType = new UnitTypeModel(1, "Program");
        UnitTypeModel serviceUnitType = new UnitTypeModel(1, "Service Unit");
        InstitutionModel programInstitution = new InstitutionModel();
        InstitutionModel serviceUnitInstitution = new InstitutionModel();
        CropModel crop = new CropModel();
        DomainInstanceModel domainInstance = new DomainInstanceModel();
        DomainModel domain = new DomainModel(1, "Core System");
        domain.setDomaininstances(Arrays.asList(domainInstance));
        domain.setPrefix("CS");
        domain.setId(1);
        domain.setName("Core System");
        ProductModel product = new ProductModel();
        product.setId(1);
        product.setName("Contact Relationship Management");
        product.setAbbreviation("CRM");
        product.setMenuOrder(1);
        product.setIsRedirect(false);
        product.setExternalId(10);
        product.setPath("/path");
        product.setDomain(domain);   
        product.setDescription("crm");  
        product.setHasWorkflow(false);   
        domain.setProducts(Arrays.asList(product));
        ProductFunctionModel pf = new ProductFunctionModel();
        pf.setId(1);
        pf.setAction("Create");
        pf.setDataAction(true);
        pf.setProduct(product);
        ProductFunctionModel pfAction = new ProductFunctionModel();
        pfAction.setId(1);
        pfAction.setAction("View");
        pfAction.setDataAction(false);
        pfAction.setProduct(product);
        RoleModel role = new RoleModel();
        role.setId(1);
        role.setName("User");
        pf.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pf))));
        pfAction.setRoleProductFunctions(new HashSet<>(Arrays.asList(new RoleProductModel(1, role, pfAction))));
        HashSet<ProductFunctionModel> pfs = new HashSet<>();
        pfs.add(pfAction);
        pfs.add(pf);
        role.setProductfunctions(pfs);
        product.setProductfunctions(pfs);
        crop.setId(1);
        crop.setName("Crop");
        programInstitution.setCommonName("PU");
        programInstitution.setLegalName("Program Unit");
        programInstitution.setUnitType(programType);
        programInstitution.setCrops(Arrays.asList(crop));
        programInstitution.setExternalCode(101);
        programInstitution.setContact(programParent);
        programParent.setInstitution(programInstitution);
        programParent.setContactWorkflows(new ArrayList<>());
        serviceUnitInstitution.setUnitType(serviceUnitType);
        serviceUnitInstitution.setCrops(Arrays.asList(crop));
        serviceUnitInstitution.setCommonName("SU");
        serviceUnitInstitution.setLegalName("Service Unit");
        serviceUnitInstitution.setContact(unitParent);
        unitParent.setInstitution(serviceUnitInstitution);
        parents.add(new HierarchyModel(0, false, contact, unitParent, new HashSet<>(Arrays.asList(role))));
        parents.add(new HierarchyModel(1, false, contact, programParent, new HashSet<>(Arrays.asList(role))));
        contact.setParents(parents);
        user.setContact(contact);
        List<ContactModel> allUnits = new ArrayList<>(Arrays.asList(programParent, unitParent));
        when(contactRepositoryMock.findByCategoryNameAndInstitutionExternalCodeAndDeletedIsFalseAndInstitutionUnitTypeNotNullAndInstitutionDeletedIsFalse(anyString(), eq(101))).thenReturn(allUnits);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(user));
        WorkflowModel wf = new WorkflowModel();
        wf.setId(583);
        JsonNode securityDefinition = om.readTree("{\"users\":[{\"id\":1}]}");
        wf.setSecurityDefinition(securityDefinition);
        ContactWorkflowModel cw = new ContactWorkflowModel(unitParent, wf, true, 583);
        unitParent.setContactWorkflows(new ArrayList<>(Arrays.asList(cw)));
        when(contactWorkflowRepositoryMock.findByWorkflowIdAndProvideIsTrue(anyInt())).thenReturn(List.of(cw));
        when(domainRepositoryMock.findAll()).thenReturn(List.of(domain));
        String object = subject.buildUserProfile(1, null, 101);
        assertNotNull(object);
    }
    
}
