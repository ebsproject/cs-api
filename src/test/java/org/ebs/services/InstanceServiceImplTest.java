package org.ebs.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.ebs.model.CustomerModel;
import org.ebs.model.InstanceModel;
import org.ebs.model.OrganizationModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.AuditLogsRepository;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.DomainInstanceRepository;
import org.ebs.model.repos.InstanceRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.InstanceTo;
import org.ebs.services.to.Input.InstanceInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;

@ExtendWith(MockitoExtension.class)
public class InstanceServiceImplTest {

    private @Mock
    DomainInstanceRepository domainInstanceRepositoryMock;
    private @Mock
    TenantRepository tenantRepositoryMock;
    private @Mock
    AuditLogsRepository auditLogsRepositoryMock;
    private @Mock
    ConversionService converterMock;
    private @Mock
    InstanceRepository instanceRepositoryMock;
    private @Mock
    CropRepository cropRepositoryMock;
    private @Mock
    AddressRepository addressRepositoryMock;

    private InstanceServiceImpl subject;

    @BeforeEach
    public void init() {
        subject = new InstanceServiceImpl(instanceRepositoryMock, converterMock,
                auditLogsRepositoryMock, tenantRepositoryMock, domainInstanceRepositoryMock,
                addressRepositoryMock);
    }

    @Test
    public void givenValidTenantInput_whenCreateTenant_thenPersistTenant() {
        InstanceModel model = new InstanceModel();
        TenantModel tenantModel = new TenantModel(1, null, null, false, false, null, null,
                new OrganizationModel(), null, new CustomerModel(), null, null, null, null, null);

        tenantModel.getOrganization().setCode("code");
        tenantModel.getCustomer().setName("name");
        tenantModel.setInstances(Collections.emptySet());
        model.setTenant(tenantModel);

        when(converterMock.convert(any(), eq(InstanceModel.class))).thenReturn(model);
        when(converterMock.convert(any(), eq(InstanceTo.class))).thenReturn(new InstanceTo());
        when(instanceRepositoryMock.save(eq(model))).thenReturn(model);
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(tenantModel));

        InstanceTo result = subject.createInstance(new InstanceInput());

        assertNotNull(result);
        verify(instanceRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenTenantExists_whenGenerateInstanceName_thenReturnUniqueName() {
        TenantModel tenantModel = new TenantModel(1, null, null, false, false, null, null,
                new OrganizationModel(), null, new CustomerModel(), null, null, null, null, null);
        tenantModel.getOrganization().setCode("code");
        tenantModel.getCustomer().setName("name");
        tenantModel.setInstances(Collections.emptySet());

        String result = subject.generateInstanceName(tenantModel);
        assertEquals("CODE-NAME-01", result);
    }

    @Test
    public void givenInstanceExists_whenDeleteInstance_thenSuccess() {
        when(instanceRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new InstanceModel()));

        int result = subject.deleteInstance(1);

        assertEquals(1, result);
        verify(instanceRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenInstanceExists_whenVerifyInstance_thenSuccess() {
        when(instanceRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new InstanceModel()));

        subject.verifyInstance(1);
        verify(instanceRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenInstanceNotExists_whenVerifyInstance_thenFail() {

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyInstance(1));
        assertEquals("Instance not found", e.getMessage());
    }

}