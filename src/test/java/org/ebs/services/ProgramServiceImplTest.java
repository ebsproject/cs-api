package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.CropProgramModel;
import org.ebs.model.ProgramModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.CropProgramRepository;
import org.ebs.model.repos.ProgramRepository;
import org.ebs.model.repos.ProjectRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.Input.ProgramInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class ProgramServiceImplTest {

	private @Mock ProgramRepository programRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock TenantRepository tenantRepositoryMock;
	private @Mock ProjectRepository projectRepositoryMock;
	private @Mock CropProgramRepository cropProgramRepositoryMock;

    private ProgramServiceImpl subject;
    private ProgramInput inputStub;
    private ProgramModel modelStub;

    /*@ BeforeEach
    public void init() {
        subject = new ProgramServiceImpl(programRepositoryMock,converterMock,tenantRepositoryMock,projectRepositoryMock,cropProgramRepositoryMock);
        inputStub = new ProgramInput(1, "a code", "a name", "a type", "a status", "a description", "a notes", 11, 1, 11, 123);
        modelStub = new ProgramModel(1, "", "", "", "", "", "", 1, null, null, null, null);
    }

    @Test
    public void givenProgramIdLessThan1_whenFindProgram_thenReturnEmpty() {
        Optional<ProgramTo> object = subject.findProgram(0);
        assertFalse(object.isPresent());
    }

    @Test
    public void givenProgramExistsAndDeleted_whenFindProgram_thenReturnEmpty() {
        ProgramModel t = new ProgramModel();
        t.setDeleted(true);
        when(programRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<ProgramTo> object = subject.findProgram(1);

        assertFalse(object.isPresent());
        verify(programRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenProgramNotExists_whenFindProgram_thenReturnEmpty() {

        Optional<ProgramTo> object = subject.findProgram(1);

        assertFalse(object.isPresent());
        verify(programRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenProgramExists_whenFindProgram_thenReturnNotEmpty() {
        when(programRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new ProgramModel()));
        when(converterMock.convert(any(), eq(ProgramTo.class))).thenReturn(new ProgramTo());

        Optional<ProgramTo> object = subject.findProgram(1);

        assertTrue(object.isPresent());
        verify(programRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
    }


    @Test
    @SuppressWarnings("unchecked")
    public void givenProgramListExist_whenFindPrograms_thenReturnNotEmpty() {
        List<ProgramModel> ProgramList = Arrays.asList(new ProgramModel(), new ProgramModel());
        when(programRepositoryMock.findByCriteria(eq(ProgramModel.class), isNull(),(List<SortInput>)isNull(), (PageInput)isNull(), anyBoolean() ))
        .thenReturn(new Connection<ProgramModel>(ProgramList,PageRequest.of(0, 10) ,2));



        Page<ProgramTo> object = subject.findPrograms(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(ProgramList);
        verify(programRepositoryMock,times(1)).findByCriteria(eq(ProgramModel.class), any(),(List<SortInput>)isNull(), (PageInput)isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenProgramListNotExists_whenFindProgram_thenReturnEmpty() {
        when(programRepositoryMock.findByCriteria(eq(ProgramModel.class), isNull(),(List<SortInput>)isNull(), (PageInput)isNull(), anyBoolean() ))
            .thenReturn(new Connection<ProgramModel>(emptyList(),PageRequest.of(0, 10),0));

        Page<ProgramTo> object = subject.findPrograms(null,null,null, false);

        assertThat(object.getContent()).isEmpty();
        verify(programRepositoryMock,times(1)).findByCriteria(eq(ProgramModel.class), any(),(List<SortInput>)isNull(), (PageInput)isNull(), anyBoolean());
    }

    @Test
    public void givenValidProgramInput_whenCreateProgram_thenPersistProgram() {
        when(converterMock.convert(any(),eq(ProgramModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(ProgramTo.class))).thenReturn(new ProgramTo());
        when(programRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(cropProgramRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new CropProgramModel()));
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new TenantModel()));

        ProgramTo result = subject.createProgram(inputStub);

        assertNotNull(result);
        verify(programRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidProgramInput_whenModifyProgram_thenPersistProgram() {
        ProgramModel model = new ProgramModel();

        when(converterMock.convert(any(),eq(ProgramModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(ProgramTo.class))).thenReturn(new ProgramTo());
        when(programRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));
        when(cropProgramRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new CropProgramModel()));
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new TenantModel()));

        ProgramTo result = subject.modifyProgram(inputStub);

        assertNotNull(result);
        verify(programRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenProgramExists_whenDeleteProgram_thenSuccess() {
        ProgramModel model = new ProgramModel();

        when(programRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteProgram(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(programRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(programRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenProgramExists_whenVerifyProgramModel_thenReturnProgramModel() {
        when(programRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new ProgramModel()));

        ProgramModel result = subject.verifyProgramModel(1);

        assertNotNull(result);
        verify(programRepositoryMock, times(1)).findByIdAndDeletedIsFalse(eq(1));
    }

    @Test
    public void givenProgramNotExists_whenVerifyProgramModel_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyProgramModel(1));
        assertThat(e.getMessage()).isEqualTo("Program not found");
    }

    @Test
    public void givenProgramHasNoCropProgram_whenVerifyDependencies_thenFail() {

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyDependencies(new ProgramModel(), inputStub));
        assertThat(e.getMessage()).isEqualTo("CropProgram not found");
    }

    @Test
    public void givenProgramHasNoTenant_whenVerifyDependencies_thenFail() {

        when(cropProgramRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new CropProgramModel()));

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyDependencies(new ProgramModel(), inputStub));
        assertThat(e.getMessage()).isEqualTo("Tenant not found");
    }

    @Test
    public void givenProgramHasAllDependencies_whenVerifyDependencies_thenSuccess() {

        when(cropProgramRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new CropProgramModel()));
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new TenantModel()));

        subject.verifyDependencies(modelStub, inputStub);

        assertThat(modelStub).extracting("cropProgram","tenant").noneMatch(v -> v == null);
    } */

}