package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.DomainModel;
import org.ebs.model.HtmlTagModel;
import org.ebs.model.repos.DomainInstanceRepository;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.Input.DomainInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class DomainServiceImplTest {

    private @Mock DomainRepository domainRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock HtmlTagRepository htmltagRepositoryMock;
	private @Mock ProductRepository productRepositoryMock;
    private @Mock DomainInstanceRepository domainInstanceRepositoryMock;
    private @Mock UserRepository userRepositoryMock;
    
    private DomainServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private DomainInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private DomainModel modelStub;

    @BeforeEach
    public void init() {
        subject = new DomainServiceImpl(domainRepositoryMock, converterMock, htmltagRepositoryMock, productRepositoryMock, domainInstanceRepositoryMock, userRepositoryMock);
    }

    @Test
    public void givenDomainExists_whenFindDomain_thenReturnNotEmpty() {
        when(domainRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new DomainModel()));
        when(converterMock.convert(any(), eq(DomainTo.class))).thenReturn(new DomainTo());

        Optional<DomainTo> object = subject.findDomain(1);

        assertTrue(object.isPresent());
        verify(domainRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenDomainExistsAndDeleted_whenFindDomain_thenReturnEmpty() {
        DomainModel t = new DomainModel();
        t.setDeleted(true);
        when(domainRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<DomainTo> object = subject.findDomain(1);

        assertFalse(object.isPresent());
        verify(domainRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenDomainIdLessThan1_whenFindDomain_thenReturnEmpty() {
        Optional<DomainTo> object = subject.findDomain(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenDomainListExist_whenFindDomains_thenReturnNotEmpty() {
        List<DomainModel> domainList = Arrays.asList(new DomainModel(), new DomainModel());
        when(domainRepositoryMock.findByCriteria(eq(DomainModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<DomainModel>(domainList, PageRequest.of(0, 10), 2));

        Page<DomainTo> object = subject.findDomains(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(domainList);
        verify(domainRepositoryMock, times(1)).findByCriteria(eq(DomainModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenDomainListNotExists_whenFindDomain_thenReturnEmpty() {
        when(domainRepositoryMock.findByCriteria(eq(DomainModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<DomainModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<DomainTo> object = subject.findDomains(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(domainRepositoryMock, times(1)).findByCriteria(eq(DomainModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidDomainInput_whenCreateDomain_thenPersistDomain() {
        when(converterMock.convert(any(),eq(DomainModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(DomainTo.class))).thenReturn(new DomainTo());
        when(domainRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(htmltagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new HtmlTagModel()));

        DomainTo result = subject.createDomain(inputStub);

        assertNotNull(result);
        verify(domainRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidDomainInput_whenModifyDomain_thenPersistDomain() {
        DomainModel model = new DomainModel();

        when(converterMock.convert(any(),eq(DomainModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(DomainTo.class))).thenReturn(new DomainTo());
        when(domainRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));
        when(htmltagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new HtmlTagModel()));
        DomainTo result = subject.modifyDomain(inputStub);

        assertNotNull(result);
        verify(domainRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenDomainNotExists_whenModifyDomain_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyDomain(inputStub));
        assertThat(e.getMessage()).isEqualTo("Domain not found");
    }
 
    @Test
    public void givenDomainExists_whenDeleteDomain_thenSuccess() {
        DomainModel model = new DomainModel();

        when(domainRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteDomain(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(domainRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(domainRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenDomainNotExists_whenDeleteDomain_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteDomain(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Domain not found");
    }
    
}
