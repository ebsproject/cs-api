package org.ebs.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.ebs.model.FileObjectModel;
import org.ebs.model.repos.FileObjectRepository;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.Input.FileObjectInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;

@ExtendWith(MockitoExtension.class)
public class FileObjectServiceImplTest {

    private @Mock FileObjectRepository fileObjectRepositoryMock;
	private @Mock ConversionService converterMock;
    
    private FileObjectServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private FileObjectInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private FileObjectModel modelStub;

    @BeforeEach
    public void init() {
        subject = new FileObjectServiceImpl(converterMock, fileObjectRepositoryMock);
    }

    @Test
    public void givenValidFileObjectInput_whenModifyFileObject_thenPersistFileObject() {
        FileObjectModel model = new FileObjectModel();

        when(converterMock.convert(any(),eq(FileObjectTo.class))).thenReturn(new FileObjectTo());
        when(fileObjectRepositoryMock.findById(inputStub.getId())).thenReturn(Optional.of(model));

        FileObjectTo result = subject.modifyFileObject(inputStub);

        assertNotNull(result);
        verify(fileObjectRepositoryMock, times(1)).save(any());
        verify(converterMock, times(1)).convert(any(), any());
    }

    @Test
    public void givenFileObjectNotExists_whenModifyFileObject_thenFail() {
        inputStub.setId(UUID.randomUUID());
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyFileObject(inputStub));
        assertThat(e.getMessage()).isEqualTo("File Object relationship id " + inputStub.getId() + " not found");
    }
    
}
