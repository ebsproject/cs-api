package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AttributesModel;
import org.ebs.model.EntityReferenceModel;
import org.ebs.model.HtmlTagModel;
import org.ebs.model.repos.AttributesRepository;
import org.ebs.model.repos.EntityReferenceRepository;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.services.to.AttributesTo;
import org.ebs.services.to.Input.AttributesInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class AttributesServiceImplTest {

    private @Mock AttributesRepository attributesRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock HtmlTagRepository htmltagRepositoryMock;
	private @Mock EntityReferenceRepository entityReferenceRepositoryMock;
    
    private AttributesServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private AttributesInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private AttributesModel modelStub;

    @BeforeEach
    public void init() {
        subject = new AttributesServiceImpl(htmltagRepositoryMock, entityReferenceRepositoryMock, converterMock, attributesRepositoryMock);
    }

    @Test
    public void givenAttributesExists_whenFindAttributes_thenReturnNotEmpty() {
        when(attributesRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new AttributesModel()));
        when(converterMock.convert(any(), eq(AttributesTo.class))).thenReturn(new AttributesTo());

        Optional<AttributesTo> object = subject.findAttributes(1);

        assertTrue(object.isPresent());
        verify(attributesRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAttributesExistsAndDeleted_whenFindAttributes_thenReturnEmpty() {
        AttributesModel t = new AttributesModel();
        t.setDeleted(true);
        when(attributesRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<AttributesTo> object = subject.findAttributes(1);

        assertFalse(object.isPresent());
        verify(attributesRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAttributesIdLessThan1_whenFindAttributes_thenReturnEmpty() {
        Optional<AttributesTo> object = subject.findAttributes(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAttributesListExist_whenFindAttributess_thenReturnNotEmpty() {
        List<AttributesModel> attributesList = Arrays.asList(new AttributesModel(), new AttributesModel());
        when(attributesRepositoryMock.findByCriteria(eq(AttributesModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AttributesModel>(attributesList, PageRequest.of(0, 10), 2));

        Page<AttributesTo> object = subject.findAttributess(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(attributesList);
        verify(attributesRepositoryMock, times(1)).findByCriteria(eq(AttributesModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAttributesListNotExists_whenFindAttributes_thenReturnEmpty() {
        when(attributesRepositoryMock.findByCriteria(eq(AttributesModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AttributesModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<AttributesTo> object = subject.findAttributess(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(attributesRepositoryMock, times(1)).findByCriteria(eq(AttributesModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidAttributesInput_whenCreateAttributes_thenPersistAttributes() {
        when(converterMock.convert(any(),eq(AttributesModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(AttributesTo.class))).thenReturn(new AttributesTo());
        when(attributesRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(htmltagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new HtmlTagModel()));
        when(entityReferenceRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new EntityReferenceModel()));
        AttributesTo result = subject.createAttributes(inputStub);

        assertNotNull(result);
        verify(attributesRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidAttributesInput_whenModifyAttributes_thenPersistAttributes() {
        AttributesModel model = new AttributesModel();

        when(converterMock.convert(any(),eq(AttributesModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(AttributesTo.class))).thenReturn(new AttributesTo());
        when(attributesRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        AttributesTo result = subject.modifyAttributes(inputStub);

        assertNotNull(result);
        verify(attributesRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenAttributesNotExists_whenModifyAttributes_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyAttributes(inputStub));
        assertThat(e.getMessage()).isEqualTo("Attributes not found");
    }
 
    @Test
    public void givenAttributesExists_whenDeleteAttributes_thenSuccess() {
        AttributesModel model = new AttributesModel();

        when(attributesRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteAttributes(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(attributesRepositoryMock,times(1)).findById(anyInt());
        verify(attributesRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenAttributesNotExists_whenDeleteAttributes_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteAttributes(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Attributes not found");
    }
    
}
