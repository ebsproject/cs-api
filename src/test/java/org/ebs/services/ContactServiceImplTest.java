package org.ebs.services;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.ebs.model.ContactInfoTypeModel.INFO_TYPE_TELEPHONE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.ebs.model.AddressModel;
import org.ebs.model.CategoryModel;
import org.ebs.model.ContactAddressModel;
import org.ebs.model.ContactInfoModel;
import org.ebs.model.ContactInfoTypeModel;
import org.ebs.model.ContactModel;
import org.ebs.model.ContactTypeModel;
import org.ebs.model.HierarchyModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.PurposeModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.ContactAddressRepository;
import org.ebs.model.repos.ContactInfoRepository;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.ContactTypeRepository;
import org.ebs.model.repos.ContactWorkflowRepository;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.HierarchyRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.model.repos.PersonRepository;
import org.ebs.model.repos.PurposeRepository;
import org.ebs.model.repos.RoleContactHierarchyRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UnitTypeRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.model.rule.repos.SequenceRuleRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.Input.AddressInput;
import org.ebs.services.to.Input.ContactInfoInput;
import org.ebs.services.to.Input.ContactInput;
import org.ebs.services.to.Input.InstitutionInput;
import org.ebs.services.to.Input.PersonInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class ContactServiceImplTest {

    @Mock
    private ConversionService converterMock;
    @Mock
    private ContactRepository contactRepositoryMock;
    @Mock
    private UserRepository userRepositoryMock;
    @Mock
    private ContactTypeRepository contactTypeRepositoryMock;
    @Mock
    private PersonRepository personRepositoryMock;
    @Mock
    private InstitutionRepository institutionRepositoryMock;
    @Mock
    private AddressRepository addressRepositoryMock;
    @Mock
    private ContactInfoRepository contactInfoRepositoryMock;

    private ContactServiceImpl subject;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private ContactModel contactModelMock;
    @Mock
    private ContactTypeModel contactTypeModelMock;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private ContactInfoModel contactInfoModelMock;
    @Mock
    private InstitutionModel institutionModelMock;
    @Mock
    private PurposeModel purposeModelMock;
    @Mock
    private HierarchyModel hierarchyModelMock;
    @Mock
    private ContactInfoInput contactInfoInputMock;
    @Mock
    private AddressInput addressInputMock;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private AddressModel addressModelMock;
    @Mock
    private List<AddressModel> addressesToSaveMock;
    @Mock
    private Contact contactMock;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private ContactInput contactInputMock;
    @Mock
    private PersonInput personInputMock;
    @Mock
    private InstitutionInput institutionInputMock;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private UserModel userModelMock;
    @Mock
    private Page<Contact> contactPageMock;
    @Mock
    private HierarchyRepository hierarchyRepositoryMock;
    @Mock
    private PurposeRepository purposeRepositoryMock;
    @Mock
    private TenantRepository tenantRepositoryMock;
    @Mock
    private ContactAddressRepository contactAddressRepositoryMock;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private ContactAddressModel contactAddressModelMock;
    @Mock
    private SequenceRuleRepository sequenceRuleRepositoryMock;
    @Mock
    private CropRepository cropRepositoryMock;
    @Mock
    private UnitTypeRepository unitTypeRepositoryMock;
    @Mock
    private RoleContactHierarchyRepository roleContactHierarchyRepositoryMock;
    @Mock
    private ContactWorkflowRepository contactWorkflowRepositoryMock;
    @Mock
    private SynchronizeUserService synchronizeUserServiceMock;
    //@Mock
    //private SequenceRuleService sequenceRuleServiceMock;

    private Connection<ContactModel> contactConnection = new Connection<ContactModel>(
            singletonList(contactModelMock), PageRequest.of(1, 1), 1);

    @BeforeEach
    public void init() {
        subject = new ContactServiceImpl(converterMock, contactRepositoryMock, userRepositoryMock,
                contactTypeRepositoryMock, personRepositoryMock, institutionRepositoryMock,
                addressRepositoryMock, contactInfoRepositoryMock,hierarchyRepositoryMock, 
                purposeRepositoryMock, tenantRepositoryMock, contactAddressRepositoryMock,
                sequenceRuleRepositoryMock, cropRepositoryMock, unitTypeRepositoryMock,
                roleContactHierarchyRepositoryMock, contactWorkflowRepositoryMock,
                synchronizeUserServiceMock
                //, sequenceRuleServiceMock
                );
    }

    @Test
    public void givenUserExists_whenFindByUser_thenReturnContact() {
        when(userRepositoryMock.findById(anyInt())).thenReturn(of(userModelMock));
        when(converterMock.convert(any(), eq(Contact.class))).thenReturn(contactMock);
        Contact result = subject.findByUser(1);
        verify(userRepositoryMock, times(1)).findById(anyInt());
        assertNotNull(result);
    }

    @Test
    public void givenUserNotExists_whenFindByUser_thenReturnNull() {

        Contact result = subject.findByUser(1);

        verify(userRepositoryMock, times(1)).findById(anyInt());
        assertNull(result);
    }

    @Test
    public void givenContactExists_whenFindById_thenReturnContact() {
        when(contactRepositoryMock.findById(anyInt())).thenReturn(of(contactModelMock));
        when(converterMock.convert(any(), eq(Contact.class))).thenReturn(contactMock);

        Contact result = subject.findById(1);

        verify(contactRepositoryMock, times(1)).findById(anyInt());
        assertNotNull(result);
    }

    @Test
    public void givenContactNotExists_whenFindById_thenReturnNull() {

        Contact result = subject.findById(1);

        verify(contactRepositoryMock, times(1)).findById(anyInt());
        assertNull(result);
    }

    @Test
    public void givenContacExists_whenFindContacts_thenReturnContacts() {
        when(contactRepositoryMock.findByCriteria(eq(ContactModel.class), anyList(), anyList(),
                (PageInput) isNull(), anyBoolean())).thenReturn(contactConnection);

        subject.findContacts(null, emptyList(), emptyList(), false);
        verify(contactRepositoryMock, times(1)).findByCriteria(eq(ContactModel.class), anyList(),
                anyList(), (PageInput) isNull(), anyBoolean());
        verify(converterMock, times(1)).convert(any(), eq(Contact.class));
    }

    @Test
    public void givenContactIsPerson_whenCreateContact_thenCreatePersonContact() {

        when(contactModelMock.getCategory().getName()).thenReturn("Person");
        when(converterMock.convert(any(), eq(ContactModel.class))).thenReturn(new ContactModel());
        when(contactRepositoryMock.save(any())).thenReturn(contactModelMock);
        List<ContactAddressModel> contactAddressModels = new ArrayList<>();
        ContactAddressModel contactAddressModel = new ContactAddressModel(new ContactModel(), new AddressModel(), true);
        contactAddressModels.add(contactAddressModel);
        when(contactAddressRepositoryMock.findByContact(any())).thenReturn(List.of(contactAddressModel));

        subject.createContact(new ContactInput());

        verify(contactRepositoryMock, times(1)).save(any());
        verify(personRepositoryMock, times(1)).save(any());
    }

    /*@Test
    public void givenContactIsInstitution_whenCreateContact_thenCreateInstitutionContact() {

        when(contactModelMock.getCategory().getName()).thenReturn("Institution");
        when(converterMock.convert(any(), eq(ContactModel.class))).thenReturn(new ContactModel());
        when(contactRepositoryMock.save(any())).thenReturn(contactModelMock);
        List<AddressInput> addresses = new ArrayList<>();
        AddressInput newAddress = new AddressInput(0, "loc", "R", "123", "street", true, 10, null);
        addresses.add(newAddress);
        //when(contactInputMock.getAddresses()).thenReturn(List.of(newAddress));
        //List<ContactAddressModel> contactAddressModels = new ArrayList<>();
        //ContactAddressModel contactAddressModel = new ContactAddressModel(new ContactModel(), new AddressModel(), true);
        //contactAddressModels.add(contactAddressModel);
        //when(contactAddressRepositoryMock.findByContact(any())).thenReturn(List.of(contactAddressModel));
        ContactInput input = new ContactInput();
        input.setAddresses(addresses);
        subject.createContact(contactInputMock);

        verify(contactRepositoryMock, times(1)).save(any());
        verify(institutionRepositoryMock, times(1)).save(any());
    }*/

    @Test
    public void givenInvalidContactType_whenVerifyContactTypes_thenThrowException() {
        when(contactTypeRepositoryMock.findByIds(any()))
                .thenReturn(List.of(contactTypeModelMock, contactTypeModelMock));
        when(contactTypeModelMock.getId()).thenReturn(11, 22);

        Exception e = assertThrows(RuntimeException.class,
                () -> subject.verifyContactTypes(List.of(22, 11, 33), contactModelMock));

        assertEquals("Invalid contact type: 33", e.getMessage());
    }

    @Test
    public void givenValidContactType_whenVerifyContactTypes_thenSetContactTypes() {
        when(contactTypeRepositoryMock.findByIds(any()))
                .thenReturn(List.of(contactTypeModelMock, contactTypeModelMock));
        when(contactTypeModelMock.getId()).thenReturn(11, 22);

        subject.verifyContactTypes(List.of(22, 11), contactModelMock);

        verify(contactModelMock, times(1)).setContactTypes(anyList());
    }

    @Test
    public void givenValidAddresses_whenVerifyAddresses_thenSetAddresses() {
        when(converterMock.convert(any(), eq(AddressModel.class))).thenReturn(addressModelMock);
        subject.verifyAddresses(List.of(addressInputMock, addressInputMock), contactModelMock);


        verify(addressInputMock, times(2)).setId(eq(0));
        verify(converterMock, times(2)).convert(any(), eq(AddressModel.class));
    }

    @Test
    public void givenValidContactInfos_whenVerifyContactInfos_thenSetContactInfos() {
        when(contactModelMock.getContactInfos()).thenReturn(emptyList());
        when(converterMock.convert(any(), eq(ContactInfoModel.class)))
                .thenReturn(contactInfoModelMock);

        subject.verifyContactInfos(List.of(contactInfoInputMock, contactInfoInputMock),
                contactModelMock);

        verify(contactInfoInputMock, times(2)).setId(eq(0));
        verify(contactModelMock, times(1)).setContactInfos(anyList());
        verify(converterMock, times(2)).convert(any(), eq(ContactInfoModel.class));
        verify(contactInfoModelMock, times(2)).setContact(contactModelMock);
    }

    @Test
    public void givenExistingPersonContact_whenModifyContact_thenSuccess() {
        when(contactModelMock.getContactInfos()).thenReturn(emptyList());
        when(contactRepositoryMock.findById(anyInt())).thenReturn(of(contactModelMock));
        when(contactModelMock.getCategory().getName()).thenReturn("Person");
        List<ContactAddressModel> contactAddressModels = new ArrayList<>();
        ContactAddressModel contactAddressModel = new ContactAddressModel(new ContactModel(), new AddressModel(), true);
        contactAddressModels.add(contactAddressModel);
        when(contactAddressRepositoryMock.findByContact(any())).thenReturn(List.of(contactAddressModel));

        subject.modifyContact(contactInputMock);

        verify(personRepositoryMock, times(1)).save(any());
        verify(contactRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenExistingInstitutionContact_whenModifyContact_thenSuccess() {
        when(contactModelMock.getContactInfos()).thenReturn(emptyList());
        when(contactRepositoryMock.findById(anyInt())).thenReturn(of(contactModelMock));
        when(contactModelMock.getCategory().getName()).thenReturn("Institution");
        List<ContactAddressModel> contactAddressModels = new ArrayList<>();
        ContactAddressModel contactAddressModel = new ContactAddressModel(new ContactModel(), new AddressModel(), true);
        contactAddressModels.add(contactAddressModel);
        when(contactAddressRepositoryMock.findByContact(any())).thenReturn(List.of(contactAddressModel));

        subject.modifyContact(contactInputMock);

        verify(institutionRepositoryMock, times(1)).save(any());
        verify(contactRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenContactNotExists_whenVerifyContact_thenThrowException() {

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyContact(1));

        assertEquals("Contact 1 not found", e.getMessage());
    }

    @Test
    public void givenNoAddress_whenSaveAddresses_thenDoNothing() {
        subject.saveAddresses(null, contactModelMock, List.of(contactAddressModelMock));

        verify(contactMock, times(0)).getAddresses();
    }

    @Test
    public void givenExistingAddress_whenUpdateExistingAddresses_thenUpdate() {
        when(addressInputMock.getId()).thenReturn(99, 100);
        AddressModel model = new AddressModel(99, null, null, null, null, null, null,null, null, null, null,null,null, null, null);
        when(converterMock.convert(any(), eq(AddressModel.class))).thenReturn(new AddressModel(99,
                "a loc", "a region", "a zipCode", "a streetAddress", null, null,null, null, null, null, null,null, null, null));

        subject.updateExistingAddresses(List.of(addressInputMock, addressInputMock),
                List.of(model), List.of(contactAddressModelMock, contactAddressModelMock));

        assertThat(model)
                .extracting("id", "location", "region", "zipCode", "streetAddress")
                .containsExactly(99, "a loc", "a region", "a zipCode", "a streetAddress");

    }

    @Test
    public void givenNoContactInfor_whenSaveContactInfos_thenDoNothing() {

        subject.saveContactInfos(null, contactModelMock);

        verify(contactMock, times(0)).getContactInfos();
    }

    @Test
    public void givenExistingContactInfo_whenUpdateExistingContactInfos_thenUpdate() {
        when(contactInfoInputMock.getId()).thenReturn(99, 100);
        ContactInfoModel model = new ContactInfoModel(99, "", false, null, null);
        when(converterMock.convert(any(), eq(ContactInfoModel.class)))
                .thenReturn(new ContactInfoModel(99, "a value", true, null, null));

        subject.updateExistingContactInfos(List.of(contactInfoInputMock, contactInfoInputMock),
                List.of(model));

        assertThat(model).extracting("id", "value", "Default").containsExactly(99, "a value", true);

    }


   @Test
    public void givenInstitutionWithContacts_whenFindContactsByInstitution_thenReturnContacts() {
        when(contactRepositoryMock.findById(anyInt())).thenReturn(of(contactModelMock));   
        List<HierarchyModel> hierarchyModelListMock = new ArrayList<HierarchyModel>();
        hierarchyModelListMock.add(hierarchyModelMock);
        when(hierarchyRepositoryMock.findByInstitution(contactModelMock)).thenReturn(hierarchyModelListMock);
        when(converterMock.convert(any(), eq(Contact.class))).thenReturn(contactMock);
        List<Contact> result = subject.findContactsByInstitution(anyInt());
        verify(contactRepositoryMock, times(1)).findById(anyInt());
        verify(converterMock).convert(any(), eq(Contact.class));
        assertNotNull(result);
    }

    @Test
    public void givenInstitutionWithNoContact_whenFindByInstitution_thenReturnNull() {
        when(contactRepositoryMock.findById(anyInt())).thenReturn(empty());

        List<Contact> result = subject.findContactsByInstitution(anyInt());
        verify(contactRepositoryMock, times(1)).findById(anyInt());
        verify(converterMock, times(0)).convert(any(), eq(Contact.class));
        assertNull(result);

    }

    @Test
    public void givenExistingContact_whenDeleteContact_thenSuccess() {
        when(contactModelMock.getAddresses())
                .thenReturn(List.of(addressModelMock, addressModelMock, addressModelMock));
        when(contactModelMock.getContactInfos()).thenReturn(
                List.of(contactInfoModelMock, contactInfoModelMock, contactInfoModelMock));
        when(contactRepositoryMock.findById(anyInt())).thenReturn(of(contactModelMock));
        when(contactModelMock.getCategory().getName()).thenReturn(anyString());
        subject.deleteContact(55);

        verify(contactModelMock, times(1)).setDeleted(eq(true));
        //verify(addressModelMock, times(3)).setDeleted(eq(true));
        verify(contactInfoModelMock, times(3)).setDeleted(eq(true));
        verify(contactRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenExistingContactInfo_whenRemoveContactInfo_thenContactInfoRemoved() {
        ContactInfoTypeModel type = new ContactInfoTypeModel();
        type.setId(INFO_TYPE_TELEPHONE);
        when(contactInfoModelMock.getContact().getContactInfos())
                .thenReturn(List.of(contactInfoModelMock));
        when(contactInfoModelMock.getContactInfoType()).thenReturn(type);
        when(contactInfoRepositoryMock.findById(anyInt())).thenReturn(of(contactInfoModelMock));
        when(contactInfoRepositoryMock.save(any())).thenReturn(contactInfoModelMock);

        int result = subject.removeContactInfo(99);

        assertEquals(99, result);
        verify(contactInfoRepositoryMock, times(1)).save(eq(contactInfoModelMock));

    }

    @Test
    public void givenInvalidContactInfo_whenRemoveContactInfo_thenThrowException() {

        Exception e = assertThrows(RuntimeException.class, () -> subject.removeContactInfo(99));

        assertEquals("Contact Info not found", e.getMessage());

    }

/*     @Test
    public void givenExistingAddress_whenRemoveAddress_thenAddressRemoved() {
        when(addressRepositoryMock.findById(anyInt())).thenReturn(of(addressModelMock));
        when(addressRepositoryMock.save(any())).thenReturn(addressModelMock);
        when(addressModelMock.getContacts().iterator().next().getAddresses()).thenReturn(List.of(addressModelMock));

        int result = subject.removeAddress(99);

        assertEquals(99, result);
        verify(addressRepositoryMock, times(1)).save(eq(addressModelMock));

    } */

/*     @Test
    public void givenInvalidAddress_whenRemoveAddress_thenThrowException() {

        Exception e = assertThrows(RuntimeException.class, () -> subject.removeAddress(99));

        assertEquals("Address not found", e.getMessage());

    } */

/*     @Test
    public void givenOneAddress_whenRemoveAddress_thenThrowException() {
        when(addressModelMock.getContacts().iterator().next().getAddresses()).thenReturn(emptyList());
        when(addressRepositoryMock.findById(anyInt())).thenReturn(of(addressModelMock));
        when(addressRepositoryMock.save(any())).thenReturn(addressModelMock);

        Exception e = assertThrows(RuntimeException.class, () -> subject.removeAddress(99));

        assertEquals("At least one address must remain", e.getMessage());

    } */

/*     @Test
    public void givenTwoDefaultAddresses_whenCheckDefaultAddress_thenThrowException() {
        AddressModel address1 = new AddressModel(), address2 = new AddressModel();
        address1.setDefault(true);
        address2.setDefault(true);
        when(contactModelMock.getAddresses()).thenReturn(List.of(address1, address2));

        Exception e = assertThrows(RuntimeException.class,
                () -> subject.checkDefaultAddress(contactModelMock));

        assertEquals("Only one address can be set as default", e.getMessage());

    } */

    @Test
    public void givenTwoDefaultInfos_whenCheckDefaultContactInfo_thenThrowException() {
        ContactInfoTypeModel typePhone = new ContactInfoTypeModel();
        typePhone.setName("web page");
        ContactInfoModel phone1 = new ContactInfoModel(), phone2 = new ContactInfoModel();
        phone1.setDefault(true);
        phone1.setContactInfoType(typePhone);
        phone2.setDefault(true);
        phone2.setContactInfoType(typePhone);
        when(contactModelMock.getContactInfos()).thenReturn(List.of(phone1, phone2));

        Exception e = assertThrows(RuntimeException.class,
                () -> subject.checkDefaultContactInfo(contactModelMock));

        assertEquals("Only one web page can be set as default", e.getMessage());

    }

/*     @Test
    public void givenOneTelephone_whenRemoveContactInfo_thenThrowException() {
        when(contactInfoModelMock.getContact().getContactInfos()).thenReturn(emptyList());
        when(contactInfoRepositoryMock.findById(anyInt())).thenReturn(of(contactInfoModelMock));
        when(contactInfoRepositoryMock.save(any())).thenReturn(contactInfoModelMock);

        Exception e = assertThrows(RuntimeException.class, () -> subject.removeContactInfo(99));

        assertEquals("At least one Phone number must remain", e.getMessage());

    } */

}