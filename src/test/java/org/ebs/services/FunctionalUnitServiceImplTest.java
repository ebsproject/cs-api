package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.ebs.model.FunctionalUnitModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.FunctionalUnitRepository;
import org.ebs.model.repos.ProductAuthorizationRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.services.to.Input.FunctionalUnitInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class FunctionalUnitServiceImplTest {

    @Mock private ConversionService converterMock;
    @Mock private FunctionalUnitRepository functionalUnitRepositoryMock;
    @Mock private UserRepository userRepoMock;
    @Mock private ProductAuthorizationRepository productAuthorizationRepoMock;
    @Mock private ProductRepository productRepositoryMock;
    @Mock private TenantRepository tenantRepositoryMock;
    @Mock private ContactRepository contactRepoMock;



    private FunctionalUnitServiceImpl subject;

    @BeforeEach
    public void init() {
        subject = new FunctionalUnitServiceImpl(converterMock, functionalUnitRepositoryMock, contactRepoMock);
    }

    @Test
    public void givenFunctionalUnitExists_whenFindFunctionalUnit_thenReturnNotEmpty() {
        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new FunctionalUnitModel()));
        when(converterMock.convert(any(), eq(FunctionalUnitTo.class))).thenReturn(new FunctionalUnitTo());

        Optional<FunctionalUnitTo> object = subject.findFunctionalUnit(1);

        assertTrue(object.isPresent());
        verify(functionalUnitRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenFunctionalUnitExistsAndDeleted_whenFindFunctionalUnit_thenReturnEmpty() {
        FunctionalUnitModel t = new FunctionalUnitModel();
        t.setDeleted(true);
        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<FunctionalUnitTo> object = subject.findFunctionalUnit(1);

        assertFalse(object.isPresent());
        verify(functionalUnitRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenFunctionalUnitNotExists_whenFindFunctionalUnit_thenReturnEmpty() {
        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.empty());

        Optional<FunctionalUnitTo> object = subject.findFunctionalUnit(1);

        assertFalse(object.isPresent());
        verify(functionalUnitRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenFunctionalUnitIdLessThan1_whenFindFunctionalUnit_thenReturnEmpty() {
        Optional<FunctionalUnitTo> object = subject.findFunctionalUnit(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenFunctionalUnitListExist_whenFindFunctionalUnitList_thenReturnNotEmpty() {
        List<FunctionalUnitModel> tenantList = Arrays.asList(new FunctionalUnitModel(), new FunctionalUnitModel());
        when(functionalUnitRepositoryMock.findByCriteria(eq(FunctionalUnitModel.class), any(),(List<SortInput>)isNull(), (PageInput)isNull(), anyBoolean() ))
            .thenReturn(new Connection<FunctionalUnitModel>(tenantList,PageRequest.of(0, 10) ,2));

        Page<FunctionalUnitTo> object = subject.findFunctionalUnitList(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(tenantList);
        verify(functionalUnitRepositoryMock,times(1)).findByCriteria(eq(FunctionalUnitModel.class), any(),(List<SortInput>)isNull(),(PageInput)isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenFunctionalUnitListNotExists_whenFindFunctionalUnitList_thenReturnEmpty() {
        when(functionalUnitRepositoryMock.findByCriteria(eq(FunctionalUnitModel.class), any(),(List<SortInput>)isNull(), (PageInput)isNull(), anyBoolean() ))
            .thenReturn(new Connection<FunctionalUnitModel>(emptyList(),PageRequest.of(0, 10),0));

        Page<FunctionalUnitTo> object = subject.findFunctionalUnitList(null,null,null, false);

        assertThat(object.getContent()).isEmpty();
        verify(functionalUnitRepositoryMock,times(1)).findByCriteria(eq(FunctionalUnitModel.class), any(),(List<SortInput>)isNull(),(PageInput)isNull(), anyBoolean());
    }

    @Test
    public void givenValidFunctionalUnitInput_whenCreateFunctionalUnit_thenPersistFunctionalUnit() {
        when(converterMock.convert(any(), eq(FunctionalUnitModel.class))).thenReturn(new FunctionalUnitModel());
        when(converterMock.convert(any(), eq(FunctionalUnitTo.class))).thenReturn(new FunctionalUnitTo());
        FunctionalUnitTo result = subject.createFunctionalUnit(new FunctionalUnitInput());

        assertNotNull(result);
        verify(converterMock, times(2)).convert(any(), any());
        verify(functionalUnitRepositoryMock, times(1)).save(any());

    }

    @Test
    public void givenValidFunctionalUnitInput_whenModifyFunctionalUnit_thenPersistFunctionalUnit() {
        when(converterMock.convert(any(), eq(FunctionalUnitModel.class))).thenReturn(new FunctionalUnitModel());
        when(converterMock.convert(any(), eq(FunctionalUnitTo.class))).thenReturn(new FunctionalUnitTo());
        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new FunctionalUnitModel()));

        when(functionalUnitRepositoryMock.save(any())).thenReturn(new FunctionalUnitModel());

        FunctionalUnitTo result = subject.modifyFunctionalUnit(new FunctionalUnitInput(1, null, null, null, null, null, null, null));

        assertNotNull(result);
        verify(converterMock, times(2)).convert(any(), any());
        verify(functionalUnitRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenExistindFunctionalUnitId_whenRemoveFunctionalUnit_thenFunctionalUnitRemoved() {
        FunctionalUnitModel object = new FunctionalUnitModel();
        object.setChildren(Collections.emptySet());

        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(object));
        when(functionalUnitRepositoryMock.save(any())).thenReturn(new FunctionalUnitModel());

        int result = subject.removeFunctionalUnit(1);

        assertEquals(1, result);
        verify(functionalUnitRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenFunctionalUnitExists_whenVerifyFuncUnitModel_thenReturnFunctionalUnitModel() {
        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new FunctionalUnitModel()));

        FunctionalUnitModel result = subject.verifyFuncUnitModel(1);

        assertNotNull(result);
        verify(functionalUnitRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenFunctionalUnitNotExists_whenVerifyFuncUnitModel_thenFail() {

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyFuncUnitModel(1));
        assertEquals("FunctionalUnit not found", e.getMessage());
    }

    @Test
    public void givenFuncUnitIsValidAndNoParent_whenSetFunctionalUnitDependencies_thenSuccess() {

        FunctionalUnitModel object = new FunctionalUnitModel();
        subject.setFunctionalUnitDependencies(object, new FunctionalUnitInput(0, null, null, null, null, null, null, null));

        assertNull(object.getParent());

    }

/*     @Test
    public void givenFuncUnitIsValid_whenAddUsers_thenUsersAdded() {
        FunctionalUnitModel funcUnit = new FunctionalUnitModel();
        funcUnit.setContacts(new HashSet<>());
        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(funcUnit));
        when(userRepoMock.findAllById(anyIterable())).thenReturn(new ArrayList<>());
        when(converterMock.convert(any(), eq(FunctionalUnitTo.class))).thenReturn(new FunctionalUnitTo());

        FunctionalUnitTo result = subject.addContacts(1, Arrays.asList(1,2,3));

        assertNotNull(result);
        verify(functionalUnitRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(functionalUnitRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenFuncUnitIsRoot_whenDefineUsersToAdd_thenAddAll() {
        FunctionalUnitModel object = new FunctionalUnitModel();
        object.setContacts(new HashSet<>());
        Set<ContactModel> newUsers = new HashSet<>(Arrays.asList(new ContactModel(),new ContactModel(),new ContactModel()));

        subject.defineContactsToAdd(object, newUsers);

        assertThat(object.getContacts()).hasSize(3);
    }

    @Test
    public void givenParentHasAllUsers_whenDefineUsersToAdd_thenAddAll() {
        Set<ContactModel> parentUsers = new HashSet<>(Arrays.asList(new ContactModel(1),new ContactModel(2),new ContactModel(3)));
        Set<ContactModel> newUsers = new HashSet<>(parentUsers);
        newUsers.removeIf(u -> u.getId() == 3);
        FunctionalUnitModel parent = new FunctionalUnitModel();
        parent.setContacts(parentUsers);
        FunctionalUnitModel object = new FunctionalUnitModel();
        object.setContacts(new HashSet<>());
        object.setParent(parent);

        subject.defineContactsToAdd(object, newUsers);

        assertThat(object.getContacts()).hasSize(2);
    }

    @Test
    public void givenParentHasNotAllUsers_whenDefineUsersToAdd_thenFail() {
        Set<ContactModel> parentUsers = new HashSet<>(Arrays.asList(new ContactModel(),new ContactModel(),new ContactModel()));
        Set<ContactModel> newUsers = new HashSet<>(parentUsers);
        parentUsers.removeIf(u -> u.getId() == 3);
        FunctionalUnitModel parent = new FunctionalUnitModel();
        parent.setContacts(parentUsers);
        FunctionalUnitModel object = new FunctionalUnitModel();
        object.setContacts(new HashSet<>());
        object.setParent(parent);

        Exception e = assertThrows(RuntimeException.class, () -> subject.defineContactsToAdd(object, newUsers));
        assertThat(e.getMessage()).contains("cannot be added because they don't belong to parent");
    } */

/*     @Test
    public void givenFuncUnitIsValid_whenRemoveUsers_thenUsersRemoved() {
        FunctionalUnitModel fu = new FunctionalUnitModel();
        fu.setContacts(emptySet());
        fu.setChildren(emptySet());
        when(functionalUnitRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn( Optional.of(fu) );
        when(functionalUnitRepositoryMock.save(any())).thenReturn(fu);
        when(userRepoMock.findAllById(anyIterable())).thenReturn( emptyList() );
        when(converterMock.convert(any(), eq(FunctionalUnitTo.class))).thenReturn(new FunctionalUnitTo());

        FunctionalUnitTo result = subject.removeContacts(1, Arrays.asList(1,2,3));

        assertNotNull(result);
        verify(converterMock, times(1)).convert(any(), eq(FunctionalUnitTo.class));

    }

    @Test
    public void givenFunctionalUnitHasChildren_whenCascadeRemoveUsers_thenRemoveFromChildren() {
        // structure:
        // fu (1,2,3,4,5)
        //   - fuA (1,2,3)
        //     - fuC (2,3)
        //   - fuB (3,4,5)
        List<ContactModel> users = Arrays.asList(new ContactModel(1), new ContactModel(2), new ContactModel(3),
        new ContactModel(4), new ContactModel(5));

        FunctionalUnitModel fu = new FunctionalUnitModel()
            ,fuA = new FunctionalUnitModel()
            ,fuB = new FunctionalUnitModel()
            ,fuC = new FunctionalUnitModel();
        fu.setChildren(new HashSet<>(Arrays.asList(fuA, fuB)));
        fuA.setChildren(new HashSet<>(Arrays.asList(fuC)));
        fuB.setChildren(emptySet());
        fuC.setChildren(emptySet());

        fu.setContacts(new HashSet<>(users));
        fuA.setContacts(new HashSet<>(users.subList(0, 3)));  // 1,2,3
        fuB.setContacts(new HashSet<>(users.subList(2, 5)));  // 3,4,5
        fuC.setContacts(new HashSet<>(users.subList(1, 3)));  // 2,3

        when(functionalUnitRepositoryMock.save(any())).thenAnswer(i -> i.getArguments()[0]);

        subject.cascadeRemoveContacts(fu, new HashSet<>(users.subList(1, 4))); // 2,3,4

        assertThat(fu.getContacts()).hasSize(2);
        assertThat(fuA.getContacts()).hasSize(1);
        assertThat(fuB.getContacts()).hasSize(1);
        assertThat(fuC.getContacts()).hasSize(0);
    } */

}