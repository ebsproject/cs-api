package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.EntityReferenceModel;
import org.ebs.model.repos.AttributesRepository;
import org.ebs.model.repos.EntityReferenceRepository;
import org.ebs.services.to.EntityReferenceTo;
import org.ebs.services.to.Input.EntityReferenceInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class EntityReferenceServiceImplTest {

    private @Mock EntityReferenceRepository entityReferenceRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock AttributesRepository attributesRepositoryMock;
    
    private EntityReferenceServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private EntityReferenceInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private EntityReferenceModel modelStub;

    @BeforeEach
    public void init() {
        subject = new EntityReferenceServiceImpl(entityReferenceRepositoryMock, converterMock, attributesRepositoryMock);
    }

    @Test
    public void givenEntityReferenceExists_whenFindEntityReference_thenReturnNotEmpty() {
        when(entityReferenceRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new EntityReferenceModel()));
        when(converterMock.convert(any(), eq(EntityReferenceTo.class))).thenReturn(new EntityReferenceTo());

        Optional<EntityReferenceTo> object = subject.findEntityReference(1);

        assertTrue(object.isPresent());
        verify(entityReferenceRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenEntityReferenceExistsAndDeleted_whenFindEntityReference_thenReturnEmpty() {
        EntityReferenceModel t = new EntityReferenceModel();
        t.setDeleted(true);
        when(entityReferenceRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<EntityReferenceTo> object = subject.findEntityReference(1);

        assertFalse(object.isPresent());
        verify(entityReferenceRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenEntityReferenceIdLessThan1_whenFindEntityReference_thenReturnEmpty() {
        Optional<EntityReferenceTo> object = subject.findEntityReference(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenEntityReferenceListExist_whenFindEntityReferences_thenReturnNotEmpty() {
        List<EntityReferenceModel> entityReferenceList = Arrays.asList(new EntityReferenceModel(), new EntityReferenceModel());
        when(entityReferenceRepositoryMock.findByCriteria(eq(EntityReferenceModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<EntityReferenceModel>(entityReferenceList, PageRequest.of(0, 10), 2));

        Page<EntityReferenceTo> object = subject.findEntityReferences(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(entityReferenceList);
        verify(entityReferenceRepositoryMock, times(1)).findByCriteria(eq(EntityReferenceModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenEntityReferenceListNotExists_whenFindEntityReference_thenReturnEmpty() {
        when(entityReferenceRepositoryMock.findByCriteria(eq(EntityReferenceModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<EntityReferenceModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<EntityReferenceTo> object = subject.findEntityReferences(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(entityReferenceRepositoryMock, times(1)).findByCriteria(eq(EntityReferenceModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidEntityReferenceInput_whenCreateEntityReference_thenPersistEntityReference() {
        when(converterMock.convert(any(),eq(EntityReferenceModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(EntityReferenceTo.class))).thenReturn(new EntityReferenceTo());
        when(entityReferenceRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        EntityReferenceTo result = subject.createEntityReference(inputStub);

        assertNotNull(result);
        verify(entityReferenceRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidEntityReferenceInput_whenModifyEntityReference_thenPersistEntityReference() {
        EntityReferenceModel model = new EntityReferenceModel();

        when(converterMock.convert(any(),eq(EntityReferenceModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(EntityReferenceTo.class))).thenReturn(new EntityReferenceTo());
        when(entityReferenceRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        EntityReferenceTo result = subject.modifyEntityReference(inputStub);

        assertNotNull(result);
        verify(entityReferenceRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenEntityReferenceNotExists_whenModifyEntityReference_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyEntityReference(inputStub));
        assertThat(e.getMessage()).isEqualTo("EntityReference not found");
    }
 
    @Test
    public void givenEntityReferenceExists_whenDeleteEntityReference_thenSuccess() {
        EntityReferenceModel model = new EntityReferenceModel();

        when(entityReferenceRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteEntityReference(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(entityReferenceRepositoryMock,times(1)).findById(anyInt());
        verify(entityReferenceRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenEntityReferenceNotExists_whenDeleteEntityReference_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteEntityReference(anyInt()));
        assertThat(e.getMessage()).isEqualTo("EntityReference not found");
    }
    
}
