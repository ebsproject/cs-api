package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.JobLogModel;
import org.ebs.model.JobWorkflowModel;
import org.ebs.model.TenantModel;
import org.ebs.model.TranslationModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.JobLogRepository;
import org.ebs.model.repos.JobWorkflowRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.Input.JobLogInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class JobLogServiceImplTest {

    private @Mock ContactRepository contactRepositoryMock;
    private @Mock JobLogRepository jobLogRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock JobWorkflowRepository jobWorkflowRepositoryMock;
    private @Mock TenantRepository tenantRepositoryMock;
    private @Mock TranslationRepository translationRepositoryMock;
    
    private JobLogServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private JobLogInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private JobLogModel modelStub;

    @BeforeEach
    public void init() {
        subject = new JobLogServiceImpl(contactRepositoryMock, jobLogRepositoryMock, 
            jobWorkflowRepositoryMock, tenantRepositoryMock, translationRepositoryMock, converterMock);
    }

    @Test
    public void givenJobLogExists_whenFindJobLog_thenReturnNotEmpty() {
        when(jobLogRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new JobLogModel()));
        when(converterMock.convert(any(), eq(JobLogTo.class))).thenReturn(new JobLogTo());

        Optional<JobLogTo> object = subject.findJobLog(1);

        assertTrue(object.isPresent());
        verify(jobLogRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenJobLogExistsAndDeleted_whenFindJobLog_thenReturnEmpty() {
        JobLogModel t = new JobLogModel();
        t.setDeleted(true);
        when(jobLogRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<JobLogTo> object = subject.findJobLog(1);

        assertFalse(object.isPresent());
        verify(jobLogRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenJobLogIdLessThan1_whenFindJobLog_thenReturnEmpty() {
        Optional<JobLogTo> object = subject.findJobLog(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenJobLogListExist_whenFindJobLogs_thenReturnNotEmpty() {
        List<JobLogModel> jobLogList = Arrays.asList(new JobLogModel(), new JobLogModel());
        when(jobLogRepositoryMock.findByCriteria(eq(JobLogModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<JobLogModel>(jobLogList, PageRequest.of(0, 10), 2));

        Page<JobLogTo> object = subject.findJobLogs(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(jobLogList);
        verify(jobLogRepositoryMock, times(1)).findByCriteria(eq(JobLogModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenJobLogListNotExists_whenFindJobLog_thenReturnEmpty() {
        when(jobLogRepositoryMock.findByCriteria(eq(JobLogModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<JobLogModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<JobLogTo> object = subject.findJobLogs(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(jobLogRepositoryMock, times(1)).findByCriteria(eq(JobLogModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidJobLogInput_whenCreateJobLog_thenPersistJobLog() {
        when(converterMock.convert(any(),eq(JobLogModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(JobLogTo.class))).thenReturn(new JobLogTo());
        when(jobLogRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(jobWorkflowRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new JobWorkflowModel()));
        when(tenantRepositoryMock.findById(inputStub.getTenantId())).thenReturn(Optional.of(new TenantModel()));
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new TranslationModel()));
        JobLogTo result = subject.createJobLog(inputStub);

        assertNotNull(result);
        verify(jobLogRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidJobLogInput_whenModifyJobLog_thenPersistJobLog() {
        JobLogModel model = new JobLogModel();

        when(converterMock.convert(any(),eq(JobLogModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(JobLogTo.class))).thenReturn(new JobLogTo());
        when(jobLogRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));
        when(jobWorkflowRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new JobWorkflowModel()));
        when(tenantRepositoryMock.findById(inputStub.getTenantId())).thenReturn(Optional.of(new TenantModel()));
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new TranslationModel()));
        JobLogTo result = subject.modifyJobLog(inputStub);

        assertNotNull(result);
        verify(jobLogRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenJobLogNotExists_whenModifyJobLog_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyJobLog(inputStub));
        assertThat(e.getMessage()).isEqualTo("Job Log not found");
    }
 
    @Test
    public void givenJobLogExists_whenDeleteJobLog_thenSuccess() {
        JobLogModel model = new JobLogModel();

        when(jobLogRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteJobLog(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(jobLogRepositoryMock,times(1)).findById(anyInt());
        verify(jobLogRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenJobLogNotExists_whenDeleteJobLog_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteJobLog(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Job Log not found");
    }
    
}
