package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.CropModel;
import org.ebs.model.repos.CropProgramRepository;
import org.ebs.model.repos.CropRepository;
import org.ebs.services.to.CropTo;
import org.ebs.services.to.Input.CropInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class CropServiceImplTest {

    private @Mock CropRepository cropRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock CropProgramRepository cropProgramRepositoryMock;
    
    private CropServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private CropInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private CropModel modelStub;

    @BeforeEach
    public void init() {
        subject = new CropServiceImpl(cropRepositoryMock, converterMock, cropProgramRepositoryMock);
    }

    @Test
    public void givenCropExists_whenFindCrop_thenReturnNotEmpty() {
        when(cropRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new CropModel()));
        when(converterMock.convert(any(), eq(CropTo.class))).thenReturn(new CropTo());

        Optional<CropTo> object = subject.findCrop(1);

        assertTrue(object.isPresent());
        verify(cropRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenCropExistsAndDeleted_whenFindCrop_thenReturnEmpty() {
        CropModel t = new CropModel();
        t.setDeleted(true);
        when(cropRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<CropTo> object = subject.findCrop(1);

        assertFalse(object.isPresent());
        verify(cropRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenCropIdLessThan1_whenFindCrop_thenReturnEmpty() {
        Optional<CropTo> object = subject.findCrop(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenCropListExist_whenFindCrops_thenReturnNotEmpty() {
        List<CropModel> cropList = Arrays.asList(new CropModel(), new CropModel());
        when(cropRepositoryMock.findByCriteria(eq(CropModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<CropModel>(cropList, PageRequest.of(0, 10), 2));

        Page<CropTo> object = subject.findCrops(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(cropList);
        verify(cropRepositoryMock, times(1)).findByCriteria(eq(CropModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenCropListNotExists_whenFindCrop_thenReturnEmpty() {
        when(cropRepositoryMock.findByCriteria(eq(CropModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<CropModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<CropTo> object = subject.findCrops(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(cropRepositoryMock, times(1)).findByCriteria(eq(CropModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidCropInput_whenCreateCrop_thenPersistCrop() {
        when(converterMock.convert(any(),eq(CropModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(CropTo.class))).thenReturn(new CropTo());
        when(cropRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        CropTo result = subject.createCrop(inputStub);

        assertNotNull(result);
        verify(cropRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidCropInput_whenModifyCrop_thenPersistCrop() {
        CropModel model = new CropModel();

        when(converterMock.convert(any(),eq(CropModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(CropTo.class))).thenReturn(new CropTo());
        when(cropRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        CropTo result = subject.modifyCrop(inputStub);

        assertNotNull(result);
        verify(cropRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenCropNotExists_whenModifyCrop_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyCrop(inputStub));
        assertThat(e.getMessage()).isEqualTo("Crop not found");
    }
 
    @Test
    public void givenCropExists_whenDeleteCrop_thenSuccess() {
        CropModel model = new CropModel();

        when(cropRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteCrop(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(cropRepositoryMock,times(1)).findById(anyInt());
        verify(cropRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenCropNotExists_whenDeleteCrop_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteCrop(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Crop not found");
    }
    
}
