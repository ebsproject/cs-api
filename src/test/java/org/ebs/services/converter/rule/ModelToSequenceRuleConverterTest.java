package org.ebs.services.converter.rule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.List;

import org.ebs.model.rule.SegmentModel;
import org.ebs.model.rule.SequenceRuleModel;
import org.ebs.model.rule.SequenceRuleSegmentModel;
import org.ebs.services.to.rule.SequenceRule;
import org.ebs.services.to.rule.StaticSegment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ModelToSequenceRuleConverterTest {
    
    @Mock
    ModelToSegmentConverter modelToSegmentConverterMock;

    ModelToSequenceRuleConverter subject;

    @BeforeEach
    public void init() {
        subject = new ModelToSequenceRuleConverter(modelToSegmentConverterMock);
    }

    @Test
    public void givenSequenceRuleModelWhenConvertThenReturnSequenceRule() {
        StaticSegment segment = new StaticSegment();
        when(modelToSegmentConverterMock.convert(any())).thenReturn(segment);

        SequenceRuleSegmentModel seqRulSegment = new SequenceRuleSegmentModel();
        seqRulSegment.setSegment(new SegmentModel());
        SequenceRule result = subject
                .convert(new SequenceRuleModel(123, "aName", List.of(seqRulSegment)));

        assertThat(result).extracting("id", "name").containsExactly(123, "aName");
    }

    @Test
    public void givenSequenceRuleModelWithUnsortedSegmentModelsWhenConvertThenReturnSortedSegments() {
        SequenceRuleSegmentModel segModel1 = new SequenceRuleSegmentModel(),
                segModel2 = new SequenceRuleSegmentModel();
        segModel1.setSegment(new SegmentModel());
        segModel2.setSegment(new SegmentModel());
        segModel1.setPosition(1);
        segModel2.setPosition(2);
            
        StaticSegment segment1 = new StaticSegment()
            ,segment2 = new StaticSegment();
            segment1.setPosition(1);
            segment2.setPosition(2);
            when(modelToSegmentConverterMock.convert(eq(segModel1))).thenReturn(segment1);
            when(modelToSegmentConverterMock.convert(eq(segModel2))).thenReturn(segment2);
                
        SequenceRule result = subject.convert(new SequenceRuleModel(123, "", List.of(segModel2, segModel1)));

        assertThat(result.getSegments().get(0)).isEqualTo(segment1);
        assertThat(result.getSegments().get(1)).isEqualTo(segment2);

        result = subject.convert(new SequenceRuleModel(123, "", List.of(segModel1, segModel2)));

        assertThat(result.getSegments().get(0)).isEqualTo(segment1);
        assertThat(result.getSegments().get(1)).isEqualTo(segment2);
    }
}
