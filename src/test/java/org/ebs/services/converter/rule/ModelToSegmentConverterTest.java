package org.ebs.services.converter.rule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.ebs.model.rule.ApiSegmentModel.ApiType.REST;
import static org.ebs.model.rule.SegmentModel.DataType.DATE;
import static org.ebs.model.rule.SegmentModel.DataType.NUMBER;
import static org.ebs.model.rule.SegmentModel.DataType.TEXT;
import static org.springframework.http.HttpMethod.GET;

import java.util.List;

import org.ebs.model.rule.ApiSegmentModel;
import org.ebs.model.rule.SegmentModel;
import org.ebs.model.rule.SegmentModel.DataType;
import org.ebs.model.rule.SequenceRuleSegmentModel;
import org.ebs.model.rule.SequenceSegmentModel;
import org.ebs.services.to.rule.ApiSegment;
import org.ebs.services.to.rule.ScaleSegment;
import org.ebs.services.to.rule.Segment;
import org.ebs.services.to.rule.SequenceSegment;
import org.ebs.services.to.rule.StaticSegment;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ModelToSegmentConverterTest {
    

    private ModelToSegmentConverter subject = new ModelToSegmentConverter();

    @Test
    public void givenStaticSegmentModelWhenConvertThenReturnStaticSegment() {
        SegmentModel segmentModel = new SegmentModel(12, "segA", "aFormat", "aFormula", true,
            DataType.DATE, null, null, null, null);
        SequenceRuleSegmentModel seqRuleSegment = new SequenceRuleSegmentModel(123, 3, 0, null, segmentModel, true, true, null, null);

        Segment s = subject.convert(seqRuleSegment);

        assertThat(s).isInstanceOf(StaticSegment.class)
                .extracting("id", "name", "format", "formula", "requiresInput", "dataType")
                .containsExactly(12, "segA", "aFormat", "aFormula", true, DATE);
    }

    @Test
    public void givenScaleSegmentModelWhenConvertThenReturnScaleSegment() {
        SegmentModel segmentModel = new SegmentModel(12, "segA", null, null, true,
            DataType.DATE, null, null, null, new ObjectMapper().valueToTree(List.of("A", "B").toArray()));
        SequenceRuleSegmentModel seqRuleSegment = new SequenceRuleSegmentModel(123, 3, 0, null, segmentModel, true, true, null, null);

        Segment s = subject.convert(seqRuleSegment);

        assertThat(s).isInstanceOf(ScaleSegment.class)
                .extracting("id", "name", "format", "formula", "requiresInput", "dataType")
                .containsExactly(12, "segA", null, null, true, DATE);
        assertThat(s.getScale()).containsExactly("A", "B");
    }

    @Test
    public void givenSequenceSegmentModelWhenConvertThenReturnSequenceSegment() {
        SequenceSegmentModel sqm = new SequenceSegmentModel(123, 100, 2, 110, null, null, null);
        SegmentModel segmentModel = new SegmentModel(15, "segA", "aFormat", "aFormula", true, NUMBER, null, null, sqm, null);
        SequenceRuleSegmentModel seqRuleSegment = new SequenceRuleSegmentModel(123, 3, 0, null, segmentModel, true, true, null, null);

        Segment s = subject.convert(seqRuleSegment);

        assertThat(s).isInstanceOf(SequenceSegment.class)
                .extracting("id", "name", "format", "formula", "requiresInput", "dataType", "lowest", "increment", "last", "resetCondition")
        .containsExactly(15, "segA", "aFormat", "aFormula", true, NUMBER, 100, 2, 110, null);
    }

    @Test
    public void givenApiSegmentModelWhenConvertThenReturnApiSegment() {
        ApiSegmentModel apm = new ApiSegmentModel(123, REST, GET, "aUrl", "aPath", "aBody", "aRespMapping");
        SegmentModel segmentModel = new SegmentModel(17, "segA", "aFormat", "aFormula", true, TEXT, null, apm, null, null);
        SequenceRuleSegmentModel seqRuleSegment = new SequenceRuleSegmentModel(123, 3, 0, null, segmentModel, true, true, null, null);
        
        Segment s = subject.convert(seqRuleSegment);

        assertThat(s).isInstanceOf(ApiSegment.class)
                .extracting("id", "name", "format", "formula", "requiresInput", "dataType", "type", "method", "serverUrl", "pathTemplate", "bodyTemplate", "responseMapping")
        .containsExactly(17, "segA", "aFormat", "aFormula", true, TEXT, REST, GET, "aUrl", "aPath", "aBody", "aRespMapping");
    }

}
