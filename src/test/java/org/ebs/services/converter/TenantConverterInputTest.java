package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Date;

import org.ebs.model.TenantModel;
import org.ebs.services.to.Input.TenantInput;
import org.junit.jupiter.api.Test;

public class TenantConverterInputTest {

    private TenantConverterInput subject = new TenantConverterInput();

    @Test
    public void givenSourceIsNotNull_whenConvert_thenReturnTarget() {
        Date date = Date.from(Instant.now());
        TenantInput object = new TenantInput(1, "a name", date, true, 0, 0, null, false);
        TenantModel result = subject.convert(object);

        assertThat(result).extracting("id","name","expiration", "expired")
            .contains(1,"a name", date, true);
    }

}
