package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.UserModel;
import org.ebs.services.to.UserTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserConverterTest {

    private UserConverter subject = new UserConverter();

    @BeforeEach
    public void init() {
    }

    @Test
    public void givenSourceIsNotNull_whenConvert_thenReturnTarget() {
    UserModel object = new UserModel(1, "a userName", null, null, 0, 1, null, null, null, true, null, false);
        UserTo actual = subject.convert(object);

        assertThat(actual).extracting("id","userName", "externalId", "isIs")
            .containsExactly(1, "a userName", 1, 0);
    }
}