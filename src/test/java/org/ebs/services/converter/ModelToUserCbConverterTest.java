package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.ContactModel;
import org.ebs.model.PersonModel;
import org.ebs.model.RoleModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.RoleRepository;
import org.ebs.services.to.UserCB;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor
@ExtendWith(MockitoExtension.class)
public class ModelToUserCbConverterTest {
        private RoleRepository roleRepository;

    ModelToUserCbConverter subject = new ModelToUserCbConverter(roleRepository);

    @Test
    public void givenUserModelIsNotNull_whenConvert_thenReturnUserCB() {
        RoleModel r = new RoleModel();
        UserCB cbUsr = new UserCB();
        if(r.getName() != null){
                if (r.getName().equals("Admin")) {
                        cbUsr.setPersonType("admin");
                } else if (r.getName().equals("User") && r.getName() != null) {
                        r.setName("user");
                }
        }else{r.setName("");}
        
        ContactModel c = new ContactModel();
        c.setPerson(
                new PersonModel(1, "lastName", "firstName", "other name", null, "NA", null, null, null, null, null));
        UserModel source = new UserModel(123, "admin@email.com", null, r, 0, 123, null, null,
        c, true, null, false);

        UserCB result = subject.convert(source);

        assertThat(result).extracting("personDbId", "email", "firstName", "lastName", "personType")
                .containsExactly(123, "admin@email.com", "firstName", "lastName", cbUsr.getPersonType());
    }

}