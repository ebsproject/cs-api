package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.ebs.model.UserModel;
import org.ebs.services.to.Input.UserInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserConverterInputTest {

    private UserConverterInput subject = new UserConverterInput();

    @BeforeEach
    public void init() {
    }

    @Test
    public void givenSourceIsNotNull_whenConvert_thenReturnTarget() {
        UserInput object = new UserInput(1, "a userName", new Date(System.currentTimeMillis()), 2, 222, 3, 4, true, null, null, false);
        UserModel actual = subject.convert(object);

        assertThat(actual).extracting("id","userName", "isIs", "externalId")
            .containsExactly(1, "a userName", 222, 4);
    }
}