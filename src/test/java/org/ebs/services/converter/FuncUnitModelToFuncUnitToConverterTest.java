package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.FunctionalUnitModel;
import org.ebs.services.to.FunctionalUnitTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FuncUnitModelToFuncUnitToConverterTest {

    private FuncUnitModelToFuncUnitToConverter subject = new FuncUnitModelToFuncUnitToConverter();

    @BeforeEach
    public void init() {
    }

    @Test
    public void givenSourceIsNotNull_whenConvert_thenReturnTarget() {
        FunctionalUnitModel object = new FunctionalUnitModel(1, "a name", "a code", "a type", "a desc", "a note", null, null, null, null);
        FunctionalUnitTo actual = subject.convert(object);

        assertThat(actual).extracting("id","name","code","type","description","notes")
            .contains(1,"a name","a code", "a type", "a desc", "a note");
    }

}
