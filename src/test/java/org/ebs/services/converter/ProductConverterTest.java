package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.ProductModel;
import org.ebs.services.to.ProductTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ProductConverterTest {

    private ProductConverter subject = new ProductConverter();

    @BeforeEach
    public void init() {
    }

    @Test
    public void givenSourceIsNotNull_whenConvert_thenReturnTarget() {
        ProductModel object = new ProductModel(1, "a name", "a desc", "a help",
                "icon/path.ico", 1, null, null, null, null, null, null, null, null, null, null, null);
        ProductTo actual = subject.convert(object);

        assertThat(actual).extracting("id", "name", "description", "help", "icon")
                .contains(1, "a name", "a desc", "a help", "icon/path.ico");
    }
}