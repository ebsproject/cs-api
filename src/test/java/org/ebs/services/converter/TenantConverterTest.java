package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.ebs.model.TenantModel;
import org.ebs.services.to.TenantTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TenantConverterTest {

    @Mock
    Date dateMock;

    private TenantConverter subject = new TenantConverter();

    @BeforeEach
    public void init() {
    }

    @Test
    public void givenSourceIsNotNull_whenConvert_thenReturnTarget() {
        TenantModel object = new TenantModel(1, "a name", dateMock, false, false, null, null, null,
                null, null, null, null, null, null, null);
        TenantTo actual = subject.convert(object);

        assertThat(actual).extracting("id", "name", "expiration", "expired").contains(1, "a name",
                dateMock, false);
    }
}