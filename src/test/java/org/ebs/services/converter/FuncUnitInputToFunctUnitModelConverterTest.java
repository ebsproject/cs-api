package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import org.ebs.model.FunctionalUnitModel;
import org.ebs.services.to.Input.FunctionalUnitInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class FuncUnitInputToFunctUnitModelConverterTest {

    private FunctionalUnitConverterInput subject = new FunctionalUnitConverterInput();

    @Test
    public void givenSourceIsNotNull_whenConvert_thenReturnTarget() {
        FunctionalUnitInput object = new FunctionalUnitInput(2, "a name", "a code", "a type", "a description", "a notes", null, null);

        FunctionalUnitModel result = subject.convert(object);

        assertThat(result).extracting("id","name","code","type","description", "notes")
            .contains(2, "a name", "a code", "a type", "a description", "a notes");
    }
}
