package org.ebs.services.converter;

import org.ebs.model.repos.RoleRepository;
import org.ebs.model.repos.TenantRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserCbToModelConverterTest {

    @Mock
    private RoleRepository roleRepositoryMock;
    @Mock
    private TenantRepository tenantRepositoryMock;

    UserCbToModelConverter subject;

    @BeforeEach
    void init() {
        subject = new UserCbToModelConverter(tenantRepositoryMock);
    }

   /*  @Test
    public void givenUserModelIsNotNull_whenConvert_thenReturnUserCB() {
        RoleModel roleAdmin = new RoleModel();
        roleAdmin.setDescription("admin");
        TenantModel defaultTenant = new TenantModel();
        when(roleRepositoryMock.findByDescription(eq("admin"))).thenReturn(of(roleAdmin));
        when(tenantRepositoryMock.findById(eq(DEFAULT_TENANT))).thenReturn(of(defaultTenant));

        UserCB source = new UserCB(123, "myuser@email.com", "myuser", "a firstName", "a lastName",
                "admin", 0, true);

        UserModel result = subject.convert(source);

        assertThat(result).extracting("externalId", "userName", "defaultRole.description")
                .containsExactly(123, "myuser@email.com", "admin");
        assertThat(result.getContact().getPerson()).extracting("givenName", "familyName")
                .containsExactly("a firstName", "a lastName");
        assertThat(result.getTenants()).containsExactly(defaultTenant);
        assertThat(result.getRoles()).containsExactly(roleAdmin);

    } */

}