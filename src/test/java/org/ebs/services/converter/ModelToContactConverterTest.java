package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;


import org.ebs.model.ContactModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.PersonModel;
import org.ebs.services.to.Contact;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

//TODO: Enable the unit test
@ExtendWith(MockitoExtension.class)
public class ModelToContactConverterTest {

    ModelToContactConverter subject = new ModelToContactConverter();
/*

    @Test
    public void givenContactModelIsPerson_whenConvert_thenReturnContactPerson() {

        PersonModel p = new PersonModel(11, "a fam name", "a givenName", "an additionalName", "NA",
                "a job", "knows sth", null);

        ContactModel c = new ContactModel(123, Person, null, null, null, p, null,null,null);

        Contact result = subject.convert(c);

        assertThat(result).extracting("id", "category", "institution").containsExactly(123, Person,
                null);
        assertThat(result.getPerson()).extracting("familyName", "givenName", "additionalName",
                "gender", "jobTitle", "knowsAbout").containsExactly("a fam name", "a givenName",
                        "an additionalName", "NA", "a job", "knows sth");
    }

    @Test
    public void givenContactModelIsInstitution_whenConvert_thenReturnContactPerson() {

        InstitutionModel i = new InstitutionModel();
        i.setCommonName("a commonName");
        i.setLegalName("a legalName");

        ContactModel c = new ContactModel(123, Institution, null, null, null, null, i,null,null);

        Contact result = subject.convert(c);

        assertThat(result).extracting("id", "category", "person").containsExactly(123, Institution,
                null);
        assertThat(result.getInstitution()).extracting("commonName", "legalName")
                .containsExactly("a commonName", "a legalName");
    }*/
}