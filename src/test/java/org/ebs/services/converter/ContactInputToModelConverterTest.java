package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.ebs.model.ContactModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.PersonModel;
import org.ebs.services.to.Input.ContactInput;
import org.ebs.services.to.Input.InstitutionInput;
import org.ebs.services.to.Input.PersonInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ContactInputToModelConverterTest {

    @Mock
    private PersonInputToModelConverter personConverterMock;
    @Mock
    private InstitutionInputToModelConverter institutionConverterMock;

    ContactInputToModelConverter subject = null;
/*
    @BeforeEach
    void init() {

        subject = new ContactInputToModelConverter(personConverterMock, institutionConverterMock);
    }

TODO: Enable unit test converter
    @Test
    public void givenContactInputIsPerson_whenConvert_thenReturnPersonContactModel() {
        when(personConverterMock.convert(any())).thenReturn(new PersonModel());

        PersonInput p = new PersonInput("a fam name", "a givenName", "an additionalName", "NA",
                "a job", "knows sth");

        ContactInput c = new ContactInput(99, Person, List.of(1, 2, 3), null, null, p, null,null,null);

        ContactModel result = subject.convert(c);

        assertThat(result).extracting("id", "category", "institution").containsExactly(99, Person,
                null);
        verify(personConverterMock, times(1)).convert(eq(p));
    }

    @Test
    public void givenContactInputIsInstitution_whenConvert_thenReturnInstitutionContactModel() {
        when(institutionConverterMock.convert(any())).thenReturn(new InstitutionModel());

        InstitutionInput i = new InstitutionInput();
        i.setCommonName("a commonName");
        i.setLegalName("a legalName");

        ContactInput c = new ContactInput(99, Institution, List.of(1, 2, 3), null, null, null, i,null,null);

        ContactModel result = subject.convert(c);

        assertThat(result).extracting("id", "category", "person").containsExactly(99, Institution,
                null);
        verify(institutionConverterMock, times(1)).convert(eq(i));
    }
    */
}