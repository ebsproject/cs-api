package org.ebs.services;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.ebs.model.JobWorkflowModel;
import org.ebs.model.TranslationModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.EmailTemplateRepository;
import org.ebs.model.repos.JobWorkflowRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.Input.JobLogInput;
import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import reactor.core.publisher.Sinks.Many;

@ExtendWith(MockitoExtension.class)
public class MessagingServiceImplTest {

    private @Mock UserRepository userRepositoryMock;
    private @Mock JobWorkflowRepository jobWorkflowRepositoryMock;
	private @Mock TranslationRepository translationRepositoryMock;
    private @Mock JobLogService jobLogServiceMock;
    private @Mock Many<JobLogTo> jobLogSinkMock;
    private @Mock EmailTemplateRepository emailTemplateRepositoryMock;
    private @Mock RestTemplate restTemplateMock;
    protected @Mock TokenGenerator tokenGeneratorMock;
    
    private MessagingServiceImpl subject;

    @BeforeEach
    public void init() {
        subject = new MessagingServiceImpl(userRepositoryMock, jobWorkflowRepositoryMock, translationRepositoryMock, jobLogServiceMock, jobLogSinkMock, emailTemplateRepositoryMock, restTemplateMock, tokenGeneratorMock);
        ReflectionTestUtils.setField(subject, "smtpServer", "http://smtp");
    }

    @Test
    public void givenValidPayload_WhenCreateJobLogFromPayload_ThenJobLogCreated() {
        String uuidString = "722ff050-a7e8-4257-a3c5-9068c5d5da22";
        JobLogTo to = new JobLogTo();
        to.setJobTrackId(UUID.fromString(uuidString));
        String payload = "{\"tenant_id\":1,\"source\":{\"submitter_id\":100,\"job_track_id\":\""+uuidString+"\"},\"message\":{\"job_workflow_id\":1,\"translation_id\":1,\"status\":\"submitted\"},\"target\":{\"users\":[1]}}";
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new UserModel()));
        when(jobWorkflowRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new JobWorkflowModel()));
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new TranslationModel()));
        when(jobLogServiceMock.createJobLog(any(JobLogInput.class))).thenReturn(to);
        String response = subject.createJobLogFromPayload(payload);
        assertNotNull(response);
    }

    @Test
    public void givenValidPayloadWithContactIdsArray_WhenCreateJobLogFromPayload_ThenJobLogCreated() {
        String uuidString = "722ff050-a7e8-4257-a3c5-9068c5d5da22";
        JobLogTo to = new JobLogTo();
        to.setJobTrackId(UUID.fromString(uuidString));
        String payload = "{\"tenant_id\":1,\"source\":{\"submitter_id\":100,\"job_track_id\":\""+uuidString+"\"},\"message\":{\"job_workflow_id\":1,\"translation_id\":1,\"status\":\"submitted\"},\"target\":{\"users\":[1],\"contacts\":[100]}}";
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new UserModel()));
        when(jobWorkflowRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new JobWorkflowModel()));
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new TranslationModel()));
        when(jobLogServiceMock.createJobLog(any(JobLogInput.class))).thenReturn(to);
        String response = subject.createJobLogFromPayload(payload);
        assertNotNull(response);
    }

    @Test
    public void givenValidPayload_WhenCreateNotificationSimple_ThenJobLogCreated() {
        String uuidString = "722ff050-a7e8-4257-a3c5-9068c5d5da22";
        JobLogTo to = new JobLogTo();
        to.setJobTrackId(UUID.fromString(uuidString));
        String payload = "{\"jobTrackId\":\""+uuidString+"\",\"recordId\":1,\"jobWorkflowId\":1,\"status\":\"submitted\",\"message\":{},\"contacts\":[100]}";
        when(jobLogServiceMock.createJobLog(any(JobLogInput.class))).thenReturn(to);
        String response = subject.createNotificationSimple(payload);
        assertNotNull(response);
    }

    @Test
    public void givenValidPayload_WhenSendEmail_ThenCallEmailAPI() throws Exception {
        String payload = "{\"to\":\"user@test.com\",\"from\":\"no-reply@ebsproject.org\",\"subject\":\"Plain Subject\",\"body\":\"email content\"}";
        when(restTemplateMock.exchange(anyString(), any(), any(), eq(String.class))).thenReturn(new ResponseEntity<String>("OK", HttpStatus.OK));
        String response = subject.sendEmail(payload, null);
        assertNotNull(response);
    }
    
}
