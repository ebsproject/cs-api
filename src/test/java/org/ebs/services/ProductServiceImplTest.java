package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.ProductModel;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.EntityReferenceRepository;
import org.ebs.model.repos.FunctionalUnitRepository;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.ProductAuthorizationRepository;
import org.ebs.model.repos.ProductFunctionRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.services.to.ProductTo;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepositoryMock;
    @Mock
    private ConversionService converterMock;
    @Mock
    private DomainRepository domainRepositoryMock;
    @Mock
    private HtmlTagRepository htmltagRepositoryMock;
    @Mock
    private ProductFunctionRepository productfunctionRepositoryMock;
    @Mock
    private ProductAuthorizationRepository productAuthorizationRepositoryMock;
    @Mock
    private FunctionalUnitRepository functionalUnitRepositoryMock;
    @Mock
    private EntityReferenceRepository entityReferenceRepositoryMock;

    @Mock
    private ProductService subject;

    @BeforeEach
    public void init() {
        subject = new ProductServiceImpl(productRepositoryMock, converterMock, domainRepositoryMock,
                htmltagRepositoryMock, productfunctionRepositoryMock,
                entityReferenceRepositoryMock,
                productAuthorizationRepositoryMock, functionalUnitRepositoryMock);
    }

    @Test
    public void givenProductExists_whenFindProduct_thenReturnNotEmpty() {
        when(productRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new ProductModel()));
        when(converterMock.convert(any(), eq(ProductTo.class))).thenReturn(new ProductTo(1));

        Optional<ProductTo> object = subject.findProduct(1);

        assertTrue(object.isPresent());
        verify(productRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenProductExistsAndDeleted_whenFindProduct_thenReturnEmpty() {
        ProductModel t = new ProductModel();
        t.setDeleted(true);
        when(productRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<ProductTo> object = subject.findProduct(1);

        assertFalse(object.isPresent());
        verify(productRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenProductNotExists_whenFindProduct_thenReturnEmpty() {
        when(productRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.empty());

        Optional<ProductTo> object = subject.findProduct(1);

        assertFalse(object.isPresent());
        verify(productRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenProductIdLessThan1_whenFindProduct_thenReturnEmpty() {
        Optional<ProductTo> object = subject.findProduct(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenProductListExist_whenFindProducts_thenReturnNotEmpty() {
        List<ProductModel> tenantList = Arrays.asList(new ProductModel(), new ProductModel());
        when(productRepositoryMock.findByCriteria(eq(ProductModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<ProductModel>(tenantList, PageRequest.of(0, 10), 2));

        Page<ProductTo> object = subject.findProducts(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(tenantList);
        verify(productRepositoryMock, times(1)).findByCriteria(eq(ProductModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenProductListNotExists_whenFindProduct_thenReturnEmpty() {
        when(productRepositoryMock.findByCriteria(eq(ProductModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<ProductModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<ProductTo> object = subject.findProducts(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(productRepositoryMock, times(1)).findByCriteria(eq(ProductModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }
}