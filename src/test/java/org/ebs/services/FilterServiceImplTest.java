package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.FilterModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.FilterRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.FilterTo;
import org.ebs.services.to.Input.FilterEntityInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class FilterServiceImplTest {

    private @Mock FilterRepository filterRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock TenantRepository tenantRepositoryMock;
	private @Mock DomainRepository domainRepositoryMock;
    
    private FilterServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private FilterEntityInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private FilterModel modelStub;

    @BeforeEach
    public void init() {
        subject = new FilterServiceImpl(filterRepositoryMock, tenantRepositoryMock, converterMock, domainRepositoryMock);
    }

    @Test
    public void givenFilterExists_whenFindFilter_thenReturnNotEmpty() {
        when(filterRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new FilterModel()));
        when(converterMock.convert(any(), eq(FilterTo.class))).thenReturn(new FilterTo());

        Optional<FilterTo> object = subject.findFilter(1);

        assertTrue(object.isPresent());
        verify(filterRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenFilterExistsAndDeleted_whenFindFilter_thenReturnEmpty() {
        FilterModel t = new FilterModel();
        t.setDeleted(true);
        when(filterRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<FilterTo> object = subject.findFilter(1);

        assertFalse(object.isPresent());
        verify(filterRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenFilterIdLessThan1_whenFindFilter_thenReturnEmpty() {
        Optional<FilterTo> object = subject.findFilter(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenFilterListExist_whenFindFilters_thenReturnNotEmpty() {
        List<FilterModel> filterList = Arrays.asList(new FilterModel(), new FilterModel());
        when(filterRepositoryMock.findByCriteria(eq(FilterModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<FilterModel>(filterList, PageRequest.of(0, 10), 2));

        Page<FilterTo> object = subject.findFilters(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(filterList);
        verify(filterRepositoryMock, times(1)).findByCriteria(eq(FilterModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenFilterListNotExists_whenFindFilter_thenReturnEmpty() {
        when(filterRepositoryMock.findByCriteria(eq(FilterModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<FilterModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<FilterTo> object = subject.findFilters(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(filterRepositoryMock, times(1)).findByCriteria(eq(FilterModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidFilterInput_whenCreateFilter_thenPersistFilter() {
        when(converterMock.convert(any(),eq(FilterModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(FilterTo.class))).thenReturn(new FilterTo());
        when(filterRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(tenantRepositoryMock.findById(inputStub.getTenantId())).thenReturn(Optional.of(new TenantModel(1)));
        FilterTo result = subject.createFilter(inputStub);

        assertNotNull(result);
        verify(filterRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidFilterInput_whenModifyFilter_thenPersistFilter() {
        FilterModel model = new FilterModel();

        when(converterMock.convert(any(),eq(FilterModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(FilterTo.class))).thenReturn(new FilterTo());
        when(filterRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));
        when(tenantRepositoryMock.findById(inputStub.getTenantId())).thenReturn(Optional.of(new TenantModel(1)));
        FilterTo result = subject.modifyFilter(inputStub);

        assertNotNull(result);
        verify(filterRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenFilterNotExists_whenModifyFilter_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyFilter(inputStub));
        assertThat(e.getMessage()).isEqualTo("Filter not found");
    }
 
    @Test
    public void givenFilterExists_whenDeleteFilter_thenSuccess() {
        FilterModel model = new FilterModel();

        when(filterRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteFilter(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(filterRepositoryMock,times(1)).findById(anyInt());
        verify(filterRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenFilterNotExists_whenDeleteFilter_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteFilter(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Filter not found");
    }
    
}
