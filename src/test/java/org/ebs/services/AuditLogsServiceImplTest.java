package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AuditLogsModel;
import org.ebs.model.InstanceModel;
import org.ebs.model.repos.AuditLogsRepository;
import org.ebs.model.repos.InstanceRepository;
import org.ebs.services.to.AuditLogsTo;
import org.ebs.services.to.Input.AuditLogsInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class AuditLogsServiceImplTest {

    private @Mock AuditLogsRepository auditLogsRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock InstanceRepository instanceRepositoryMock;
    
    private AuditLogsServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private AuditLogsInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private AuditLogsModel modelStub;

    @BeforeEach
    public void init() {
        subject = new AuditLogsServiceImpl(instanceRepositoryMock, converterMock, auditLogsRepositoryMock);
    }

    @Test
    public void givenAuditLogsExists_whenFindAuditLogs_thenReturnNotEmpty() {
        when(auditLogsRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new AuditLogsModel()));
        when(converterMock.convert(any(), eq(AuditLogsTo.class))).thenReturn(new AuditLogsTo());

        Optional<AuditLogsTo> object = subject.findAuditLogs(1);

        assertTrue(object.isPresent());
        verify(auditLogsRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAuditLogsExistsAndDeleted_whenFindAuditLogs_thenReturnEmpty() {
        AuditLogsModel t = new AuditLogsModel();
        t.setDeleted(true);
        when(auditLogsRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<AuditLogsTo> object = subject.findAuditLogs(1);

        assertFalse(object.isPresent());
        verify(auditLogsRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAuditLogsIdLessThan1_whenFindAuditLogs_thenReturnEmpty() {
        Optional<AuditLogsTo> object = subject.findAuditLogs(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAuditLogsListExist_whenFindAuditLogss_thenReturnNotEmpty() {
        List<AuditLogsModel> auditLogsList = Arrays.asList(new AuditLogsModel(), new AuditLogsModel());
        when(auditLogsRepositoryMock.findByCriteria(eq(AuditLogsModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AuditLogsModel>(auditLogsList, PageRequest.of(0, 10), 2));

        Page<AuditLogsTo> object = subject.findAuditLogss(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(auditLogsList);
        verify(auditLogsRepositoryMock, times(1)).findByCriteria(eq(AuditLogsModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAuditLogsListNotExists_whenFindAuditLogs_thenReturnEmpty() {
        when(auditLogsRepositoryMock.findByCriteria(eq(AuditLogsModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AuditLogsModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<AuditLogsTo> object = subject.findAuditLogss(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(auditLogsRepositoryMock, times(1)).findByCriteria(eq(AuditLogsModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidAuditLogsInput_whenCreateAuditLogs_thenPersistAuditLogs() {
        when(converterMock.convert(any(),eq(AuditLogsModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(AuditLogsTo.class))).thenReturn(new AuditLogsTo());
        when(auditLogsRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(instanceRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new InstanceModel()));
        AuditLogsTo result = subject.createAuditLogs(inputStub);

        assertNotNull(result);
        verify(auditLogsRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidAuditLogsInput_whenModifyAuditLogs_thenPersistAuditLogs() {
        AuditLogsModel model = new AuditLogsModel();

        when(converterMock.convert(any(),eq(AuditLogsModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(AuditLogsTo.class))).thenReturn(new AuditLogsTo());
        when(auditLogsRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        AuditLogsTo result = subject.modifyAuditLogs(inputStub);

        assertNotNull(result);
        verify(auditLogsRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenAuditLogsNotExists_whenModifyAuditLogs_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyAuditLogs(inputStub));
        assertThat(e.getMessage()).isEqualTo("AuditLogs not found");
    }
 
    @Test
    public void givenAuditLogsExists_whenDeleteAuditLogs_thenSuccess() {
        AuditLogsModel model = new AuditLogsModel();

        when(auditLogsRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteAuditLogs(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(auditLogsRepositoryMock,times(1)).findById(anyInt());
        verify(auditLogsRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenAuditLogsNotExists_whenDeleteAuditLogs_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteAuditLogs(anyInt()));
        assertThat(e.getMessage()).isEqualTo("AuditLogs not found");
    }
    
}
