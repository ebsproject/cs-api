package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.EmailTemplateModel;
import org.ebs.model.JobTypeModel;
import org.ebs.model.JobWorkflowModel;
import org.ebs.model.ProductFunctionModel;
import org.ebs.model.TenantModel;
import org.ebs.model.TranslationModel;
import org.ebs.model.repos.EmailTemplateRepository;
import org.ebs.model.repos.JobTypeRepository;
import org.ebs.model.repos.JobWorkflowRepository;
import org.ebs.model.repos.ProductFunctionRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.Input.JobWorkflowInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class JobWorkflowServiceImplTest {

    private @Mock JobWorkflowRepository jobWorkflowRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock ProductFunctionRepository productFunctionRepositoryMock;
    private @Mock JobTypeRepository jobTypeRepositoryMock;
    private @Mock TenantRepository tenantRepositoryMock;
    private @Mock TranslationRepository translationRepositoryMock;
    private @Mock EmailTemplateRepository emailTemplateRepositoryMock;
    
    private JobWorkflowServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private JobWorkflowInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private JobWorkflowModel modelStub;

    @BeforeEach
    public void init() {
        subject = new JobWorkflowServiceImpl(productFunctionRepositoryMock, jobTypeRepositoryMock, jobWorkflowRepositoryMock,
             tenantRepositoryMock, translationRepositoryMock, emailTemplateRepositoryMock, converterMock);
    }

    @Test
    public void givenJobWorkflowExists_whenFindJobWorkflow_thenReturnNotEmpty() {
        when(jobWorkflowRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new JobWorkflowModel()));
        when(converterMock.convert(any(), eq(JobWorkflowTo.class))).thenReturn(new JobWorkflowTo());

        Optional<JobWorkflowTo> object = subject.findJobWorkflow(1);

        assertTrue(object.isPresent());
        verify(jobWorkflowRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenJobWorkflowExistsAndDeleted_whenFindJobWorkflow_thenReturnEmpty() {
        JobWorkflowModel t = new JobWorkflowModel();
        t.setDeleted(true);
        when(jobWorkflowRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<JobWorkflowTo> object = subject.findJobWorkflow(1);

        assertFalse(object.isPresent());
        verify(jobWorkflowRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenJobWorkflowIdLessThan1_whenFindJobWorkflow_thenReturnEmpty() {
        Optional<JobWorkflowTo> object = subject.findJobWorkflow(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenJobWorkflowListExist_whenFindJobWorkflows_thenReturnNotEmpty() {
        List<JobWorkflowModel> jobWorkflowList = Arrays.asList(new JobWorkflowModel(), new JobWorkflowModel());
        when(jobWorkflowRepositoryMock.findByCriteria(eq(JobWorkflowModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<JobWorkflowModel>(jobWorkflowList, PageRequest.of(0, 10), 2));

        Page<JobWorkflowTo> object = subject.findJobWorkflows(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(jobWorkflowList);
        verify(jobWorkflowRepositoryMock, times(1)).findByCriteria(eq(JobWorkflowModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenJobWorkflowListNotExists_whenFindJobWorkflow_thenReturnEmpty() {
        when(jobWorkflowRepositoryMock.findByCriteria(eq(JobWorkflowModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<JobWorkflowModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<JobWorkflowTo> object = subject.findJobWorkflows(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(jobWorkflowRepositoryMock, times(1)).findByCriteria(eq(JobWorkflowModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidJobWorkflowInput_whenCreateJobWorkflow_thenPersistJobWorkflow() {
        when(converterMock.convert(any(),eq(JobWorkflowModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(JobWorkflowTo.class))).thenReturn(new JobWorkflowTo());
        when(jobWorkflowRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(productFunctionRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new ProductFunctionModel()));
        when(jobTypeRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new JobTypeModel()));
        when(tenantRepositoryMock.findById(inputStub.getTenantId())).thenReturn(Optional.of(new TenantModel()));
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new TranslationModel()));
        when(emailTemplateRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new EmailTemplateModel()));
        JobWorkflowTo result = subject.createJobWorkflow(inputStub);

        assertNotNull(result);
        verify(jobWorkflowRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidJobWorkflowInput_whenModifyJobWorkflow_thenPersistJobWorkflow() {
        JobWorkflowModel model = new JobWorkflowModel();

        when(converterMock.convert(any(),eq(JobWorkflowModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(JobWorkflowTo.class))).thenReturn(new JobWorkflowTo());
        when(jobWorkflowRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));
        when(productFunctionRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new ProductFunctionModel()));
        when(jobTypeRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new JobTypeModel()));
        when(tenantRepositoryMock.findById(inputStub.getTenantId())).thenReturn(Optional.of(new TenantModel()));
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new TranslationModel()));
        when(emailTemplateRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new EmailTemplateModel()));
        JobWorkflowTo result = subject.modifyJobWorkflow(inputStub);

        assertNotNull(result);
        verify(jobWorkflowRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenJobWorkflowNotExists_whenModifyJobWorkflow_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyJobWorkflow(inputStub));
        assertThat(e.getMessage()).isEqualTo("Job Workflow not found");
    }
 
    @Test
    public void givenJobWorkflowExists_whenDeleteJobWorkflow_thenSuccess() {
        JobWorkflowModel model = new JobWorkflowModel();

        when(jobWorkflowRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteJobWorkflow(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(jobWorkflowRepositoryMock,times(1)).findById(anyInt());
        verify(jobWorkflowRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenJobWorkflowNotExists_whenDeleteJobWorkflow_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteJobWorkflow(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Job Workflow not found");
    }
    
}
