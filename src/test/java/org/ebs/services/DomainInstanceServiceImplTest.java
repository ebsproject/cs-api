package org.ebs.services;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

import org.ebs.model.DomainInstanceModel;
import org.ebs.model.DomainModel;
import org.ebs.model.InstanceModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.DomainInstanceRepository;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.Input.DomainInstanceInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;

@ExtendWith(MockitoExtension.class)
public class DomainInstanceServiceImplTest {

	private @Mock DomainInstanceRepository domaininstanceRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock TenantRepository tenantRepositoryMock;
	private @Mock DomainRepository domainRepositoryMock;

    private DomainInstanceServiceImpl subject;

    @BeforeEach
    public void init() {
        subject = new DomainInstanceServiceImpl( domaininstanceRepositoryMock, converterMock,
            tenantRepositoryMock, domainRepositoryMock );
    }

    @Test
    public void givenInstanceExists_whenCreateDomainInstance_thenSuccess() {
        TenantModel tenantModel = new TenantModel(1);
        tenantModel.setInstances(new HashSet<>(singletonList(new InstanceModel())));

        when(converterMock.convert(any(), eq(DomainInstanceModel.class))).thenReturn(new DomainInstanceModel());
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(tenantModel));
        when(domainRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new DomainModel()));

        subject.createDomainInstance(new DomainInstanceInput(0, 0, 1, 1, "context", "sgContext", true));

        verify(domaininstanceRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenIsCoreBreeding_whenCheckIsMFE_thenReturnFalse() {
        boolean response = subject.checkIsMFE(new DomainInstanceInput(0, 0, DomainModel.CORE_BREEDING_ID, 0, "", "", false));
        assertFalse(response);
    }

    @Test
    public void givenIsCoreSystem_whenCheckIsMFE_thenReturnTrue() {
        boolean response = subject.checkIsMFE(new DomainInstanceInput(0, 0, DomainModel.CORE_SYSTEM_ID, 0, "", "", false));
        assertTrue(response);
    }

    @Test
    public void givenIsNotCoreDomain_whenCheckIsMFE_thenReturnSame() {
        boolean response = subject.checkIsMFE(new DomainInstanceInput(0, 0, 3, 0, "", "", false));
        assertFalse(response);

        response = subject.checkIsMFE(new DomainInstanceInput(0, 0, 3, 0, "", "", true));
        assertTrue(response);
    }

    @Test
    public void givenTenantNotExists_whenVerifyDependencies_thenThrowException() {
        DomainInstanceModel model = new DomainInstanceModel();
        DomainInstanceInput input = new DomainInstanceInput();

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyDependencies(model, input));
        assertEquals("Tenant not found", e.getMessage());
    }

    @Test
    public void givenDomainNotExists_whenVerifyDependencies_thenThrowException() {
        DomainInstanceModel model = new DomainInstanceModel();
        DomainInstanceInput input = new DomainInstanceInput();
        TenantModel tenantModel = new TenantModel(1);
        tenantModel.setInstances(new HashSet<>(Collections.singletonList(new InstanceModel())));

        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(tenantModel));

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyDependencies(model, input));
        assertEquals("Domain not found", e.getMessage());
    }

    @Test
    public void givenDomainExistsInInstance_whenVerifyDependencies_thenThrowException() {
        DomainModel domainModel = new DomainModel(1, "a name");
        DomainInstanceModel model = new DomainInstanceModel();
        model.setDomain(domainModel);
        model.setInstance(new InstanceModel());
        DomainInstanceInput input = new DomainInstanceInput();
        TenantModel tenantModel = new TenantModel(1);
        tenantModel.setInstances(new HashSet<>(Collections.singletonList(new InstanceModel())));

        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(tenantModel));
        when(domainRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(domainModel));
        when(domaininstanceRepositoryMock.findByInstanceIdAndDomainIdAndDeletedIsFalse(anyInt(), anyInt()))
            .thenReturn(Optional.of(model));

        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyDependencies(model, input));
        assertEquals("Domain 'a name' already exists for this tenant", e.getMessage());
    }

    @Test
    public void givenInstanceExists_whenAddDefaultDomainInstances_thenSuccess() {
        TenantModel tenantModel = new TenantModel(1);
        tenantModel.setInstances(new HashSet<>(Collections.singletonList(new InstanceModel())));
        DomainModel domainModel = new DomainModel(1, "a name");

        when(converterMock.convert(any(), eq(DomainInstanceModel.class))).thenReturn(new DomainInstanceModel());
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(tenantModel));
        when(domainRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(domainModel));

        subject.addDefaultDomainInstances(1);

        //must call createDomainInstance twice
        verify(tenantRepositoryMock, times(2)).findByIdAndDeletedIsFalse(anyInt());
        verify(domainRepositoryMock, times(2)).findByIdAndDeletedIsFalse(anyInt());
        verify(domaininstanceRepositoryMock, times(2)).findByInstanceIdAndDomainIdAndDeletedIsFalse(anyInt(), anyInt());
    }

    @Test
    public void givenDomainInstanceExists_whenDeleteDomainInstance_thenSuccess() {
        DomainInstanceModel model = new DomainInstanceModel();
        model.setDomain(new DomainModel());
        when(domaininstanceRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteDomainInstance(1);

        verify(domaininstanceRepositoryMock, times(1)).save(eq(model));
        assertEquals(1, result);
    }

    @Test
    public void givenAnyCoreDomain_whenIsCoreDomain_thenReturnTrue() {
        boolean result = subject.isCoreDomain(new DomainModel(DomainModel.CORE_BREEDING_ID, "a name"));
        assertTrue(result);

        result = subject.isCoreDomain(new DomainModel(DomainModel.CORE_SYSTEM_ID, "a name"));
        assertTrue(result);
    }

    @Test
    public void givenOtherDomain_whenIsCoreDomain_thenReturnFalse() {
        boolean result = subject.isCoreDomain(new DomainModel(3, "a name"));
        assertFalse(result);

        result = subject.isCoreDomain(new DomainModel(99, "a name"));
        assertFalse(result);
    }

    @Test
    public void givenExistingDomainInstance_whenVerifyDomainInstanceModel_thenSuccess() {
        when(domaininstanceRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new DomainInstanceModel()));

        DomainInstanceModel result = subject.verifyDomainInstanceModel(1);

        assertNotNull(result);
        verify(domaininstanceRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    public void givenNotExistingDomainInstance_whenVerifyDomainInstanceModel_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyDomainInstanceModel(1));

        assertEquals("DomainInstance not found", e.getMessage());
    }

    @Test
    public void givenDomainInstanceExists_whenModifyDomainInstance_thenSuccess() {
        DomainInstanceModel model = new DomainInstanceModel();
        DomainInstanceInput input = new DomainInstanceInput();

        when(domaininstanceRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        subject.modifyDomainInstance(input);

        verify(domaininstanceRepositoryMock, times(1)).save(eq(model));

    }



}