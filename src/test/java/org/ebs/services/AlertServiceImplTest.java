package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AlertModel;
import org.ebs.model.HtmlTagModel;
import org.ebs.model.repos.AlertRepository;
import org.ebs.model.repos.AlertRuleRepository;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.services.to.AlertTo;
import org.ebs.services.to.Input.AlertInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class AlertServiceImplTest {

    private @Mock AlertRepository alertRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock HtmlTagRepository htmltagRepositoryMock;
	private @Mock AlertRuleRepository alertruleRepositoryMock;
    
    private AlertServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private AlertInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private AlertModel modelStub;

    @BeforeEach
    public void init() {
        subject = new AlertServiceImpl(htmltagRepositoryMock, alertruleRepositoryMock, converterMock, alertRepositoryMock);
    }

    @Test
    public void givenAlertExists_whenFindAlert_thenReturnNotEmpty() {
        when(alertRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new AlertModel()));
        when(converterMock.convert(any(), eq(AlertTo.class))).thenReturn(new AlertTo());

        Optional<AlertTo> object = subject.findAlert(1);

        assertTrue(object.isPresent());
        verify(alertRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAlertExistsAndDeleted_whenFindAlert_thenReturnEmpty() {
        AlertModel t = new AlertModel();
        t.setDeleted(true);
        when(alertRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<AlertTo> object = subject.findAlert(1);

        assertFalse(object.isPresent());
        verify(alertRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAlertIdLessThan1_whenFindAlert_thenReturnEmpty() {
        Optional<AlertTo> object = subject.findAlert(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAlertListExist_whenFindAlerts_thenReturnNotEmpty() {
        List<AlertModel> alertList = Arrays.asList(new AlertModel(), new AlertModel());
        when(alertRepositoryMock.findByCriteria(eq(AlertModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AlertModel>(alertList, PageRequest.of(0, 10), 2));

        Page<AlertTo> object = subject.findAlerts(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(alertList);
        verify(alertRepositoryMock, times(1)).findByCriteria(eq(AlertModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAlertListNotExists_whenFindAlert_thenReturnEmpty() {
        when(alertRepositoryMock.findByCriteria(eq(AlertModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AlertModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<AlertTo> object = subject.findAlerts(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(alertRepositoryMock, times(1)).findByCriteria(eq(AlertModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidAlertInput_whenCreateAlert_thenPersistAlert() {
        when(converterMock.convert(any(),eq(AlertModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(AlertTo.class))).thenReturn(new AlertTo());
        when(alertRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(htmltagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new HtmlTagModel()));

        AlertTo result = subject.createAlert(inputStub);

        assertNotNull(result);
        verify(alertRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidAlertInput_whenModifyAlert_thenPersistAlert() {
        AlertModel model = new AlertModel();

        when(converterMock.convert(any(),eq(AlertModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(AlertTo.class))).thenReturn(new AlertTo());
        when(alertRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        AlertTo result = subject.modifyAlert(inputStub);

        assertNotNull(result);
        verify(alertRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenAlertNotExists_whenModifyAlert_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyAlert(inputStub));
        assertThat(e.getMessage()).isEqualTo("Alert not found");
    }
 
    @Test
    public void givenAlertExists_whenDeleteAlert_thenSuccess() {
        AlertModel model = new AlertModel();

        when(alertRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteAlert(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(alertRepositoryMock,times(1)).findById(anyInt());
        verify(alertRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenAlertNotExists_whenDeleteAlert_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteAlert(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Alert not found");
    }
    
}
