package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.ProcessModel;
import org.ebs.model.repos.ProcessRepository;
import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.Input.ProcessInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class ProcessServiceImplTest {

    private @Mock ProcessRepository processRepositoryMock;
	private @Mock ConversionService converterMock;
    
    private ProcessServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private ProcessInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private ProcessModel modelStub;

    @BeforeEach
    public void init() {
        subject = new ProcessServiceImpl(processRepositoryMock, converterMock);
    }

    @Test
    public void givenProcessExists_whenFindProcess_thenReturnNotEmpty() {
        when(processRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new ProcessModel()));
        when(converterMock.convert(any(), eq(ProcessTo.class))).thenReturn(new ProcessTo());

        Optional<ProcessTo> object = subject.findProcess(1);

        assertTrue(object.isPresent());
        verify(processRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenProcessExistsAndDeleted_whenFindProcess_thenReturnEmpty() {
        ProcessModel t = new ProcessModel();
        t.setDeleted(true);
        when(processRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<ProcessTo> object = subject.findProcess(1);

        assertFalse(object.isPresent());
        verify(processRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenProcessIdLessThan1_whenFindProcess_thenReturnEmpty() {
        Optional<ProcessTo> object = subject.findProcess(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenProcessListExist_whenFindProcesss_thenReturnNotEmpty() {
        List<ProcessModel> processList = Arrays.asList(new ProcessModel(), new ProcessModel());
        when(processRepositoryMock.findByCriteria(eq(ProcessModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<ProcessModel>(processList, PageRequest.of(0, 10), 2));

        Page<ProcessTo> object = subject.findProcesses(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(processList);
        verify(processRepositoryMock, times(1)).findByCriteria(eq(ProcessModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenProcessListNotExists_whenFindProcess_thenReturnEmpty() {
        when(processRepositoryMock.findByCriteria(eq(ProcessModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<ProcessModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<ProcessTo> object = subject.findProcesses(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(processRepositoryMock, times(1)).findByCriteria(eq(ProcessModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidProcessInput_whenCreateProcess_thenPersistProcess() {
        when(converterMock.convert(any(),eq(ProcessModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(ProcessTo.class))).thenReturn(new ProcessTo());
        when(processRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        ProcessTo result = subject.createProcess(inputStub);

        assertNotNull(result);
        verify(processRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidProcessInput_whenModifyProcess_thenPersistProcess() {
        ProcessModel model = new ProcessModel();

        when(converterMock.convert(any(),eq(ProcessModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(ProcessTo.class))).thenReturn(new ProcessTo());
        when(processRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        ProcessTo result = subject.modifyProcess(inputStub);

        assertNotNull(result);
        verify(processRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenProcessNotExists_whenModifyProcess_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyProcess(inputStub));
        assertThat(e.getMessage()).isEqualTo("Process not found");
    }
 
    @Test
    public void givenProcessExists_whenDeleteProcess_thenSuccess() {
        ProcessModel model = new ProcessModel();

        when(processRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteProcess(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(processRepositoryMock,times(1)).findById(anyInt());
        verify(processRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenProcessNotExists_whenDeleteProcess_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteProcess(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Process not found");
    }
    
}
