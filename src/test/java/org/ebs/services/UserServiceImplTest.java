package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactModel;
import org.ebs.model.RoleModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.DelegationRepository;
import org.ebs.model.repos.FunctionalUnitRepository;
import org.ebs.model.repos.RoleRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.UserInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    private @Mock
    UserRepository userRepositoryMock;
    private @Mock
    ConversionService converterMock;
    private @Mock
    DelegationRepository delegationRepositoryMock;
    private @Mock
    FunctionalUnitRepository funcUnitRepositoryMock;
    private @Mock
    RoleRepository roleRepositoryMock;
    private @Mock
    TenantRepository tenantRepositoryMock;
    private @Mock
    ContactRepository contactRepositoryMock;
    private @Mock
    UserService userServiceMock;
    private @Mock
    SynchronizeUserService synchronizeUserServiceMock;

    private UserServiceImpl subject;
    private UserInput inputStub;

    @BeforeEach
    public void init() {
        subject = new UserServiceImpl(userRepositoryMock, converterMock, delegationRepositoryMock,
                funcUnitRepositoryMock, roleRepositoryMock, tenantRepositoryMock,
                contactRepositoryMock, synchronizeUserServiceMock);
        inputStub = new UserInput(1, "userName", new Date(System.currentTimeMillis()), 1, 2, 3, 222, true, null, null, false);
    }

    @Test
    public void givenUserIdLessThan1_whenFindUser_thenReturnEmpty() {
        Optional<UserTo> object = subject.findUser(0);
        assertFalse(object.isPresent());
    }

    @Test
    public void givenUserExistsAndDeleted_whenFindUser_thenReturnEmpty() {
        UserModel t = new UserModel();
        t.setDeleted(true);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<UserTo> object = subject.findUser(1);

        assertFalse(object.isPresent());
        verify(userRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenUserNotExists_whenFindUser_thenReturnEmpty() {
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.empty());

        Optional<UserTo> object = subject.findUser(1);

        assertFalse(object.isPresent());
        verify(userRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenUserExists_whenFindUser_thenReturnNotEmpty() {
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new UserModel()));
        when(converterMock.convert(any(), eq(UserTo.class))).thenReturn(new UserTo());

        Optional<UserTo> object = subject.findUser(1);

        assertTrue(object.isPresent());
        verify(userRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenUserListExist_whenFindUsers_thenReturnNotEmpty() {
        List<UserModel> UserList = Arrays.asList(new UserModel(), new UserModel());
        when(userRepositoryMock.findByCriteria(eq(UserModel.class), isNull(), (List<SortInput>) isNull(),
                (PageInput) isNull(), anyBoolean()))
                        .thenReturn(new Connection<UserModel>(UserList, PageRequest.of(0, 10), 2));

        Page<UserTo> object = subject.findUsers(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(UserList);
        verify(userRepositoryMock, times(1)).findByCriteria(eq(UserModel.class), any(), (List<SortInput>) isNull(),
                (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenUserListNotExists_whenFindUser_thenReturnEmpty() {
        when(userRepositoryMock.findByCriteria(eq(UserModel.class), isNull(), (List<SortInput>) isNull(),
                (PageInput) isNull(), anyBoolean()))
                        .thenReturn(new Connection<UserModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<UserTo> object = subject.findUsers(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(userRepositoryMock, times(1)).findByCriteria(eq(UserModel.class), any(), (List<SortInput>) isNull(),
                (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidUserInput_whenCreateUser_thenPersistUser() {
        UserModel model = new UserModel();

        when(converterMock.convert(any(), eq(UserModel.class))).thenReturn(model);
        when(converterMock.convert(any(), eq(UserTo.class))).thenReturn(new UserTo());
        when(userRepositoryMock.save(eq(model))).thenReturn(model);
        when(roleRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new RoleModel()));
        when(contactRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new ContactModel()));
        inputStub.setUserName("user@mock.org");
        UserTo result = subject.createUser(inputStub);

        assertNotNull(result);
        verify(userRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidUserInput_whenModifyUser_thenPersistUser() {
        UserModel model = new UserModel();

        when(converterMock.convert(any(), eq(UserModel.class))).thenReturn(model);
        when(converterMock.convert(any(), eq(UserTo.class))).thenReturn(new UserTo());
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));
        when(roleRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new RoleModel()));
        when(contactRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new ContactModel()));
        inputStub.setUserName("user@mock.org");
        UserTo result = subject.modifyUser(inputStub);

        assertNotNull(result);
        verify(userRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenUserExists_whenDeleteUser_thenSuccess() {
        UserModel model = new UserModel();

        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteUser(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(userRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(userRepositoryMock, times(1)).save(any());
    }

    @Test
    public void givenUserExists_whenVerifyUserModel_thenReturnUserModel() {
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new UserModel()));

        UserModel result = subject.verifyUserModel(1);

        assertNotNull(result);
        verify(userRepositoryMock, times(1)).findByIdAndDeletedIsFalse(eq(1));
    }

    @Test
    public void givenUserNotExists_whenVerifyUserModel_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyUserModel(1));
        assertThat(e.getMessage()).isEqualTo("User not found");
    }

    @Test
    public void givenUserHasNoPerson_whenVerifyDependencies_thenFail() {

        Exception e = assertThrows(RuntimeException.class,
                () -> subject.verifyDependencies(new UserModel(), inputStub));
        assertThat(e.getMessage()).isEqualTo("Contact not found");
    }

    @Test
    public void givenUserHasNoRole_whenVerifyDependencies_thenFail() {
        when(contactRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new ContactModel()));

        Exception e = assertThrows(RuntimeException.class,
                () -> subject.verifyDependencies(new UserModel(), inputStub));
        assertThat(e.getMessage()).isEqualTo("Role not found");
    }

    @Test
    public void givenUserHasAllDependencies_whenVerifyDependencies_thenSuccess() {

        when(roleRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new RoleModel()));
        when(contactRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new ContactModel()));

        UserModel model = new UserModel();

        subject.verifyDependencies(model, inputStub);

        assertThat(model).extracting("contact", "defaultRole").noneMatch(v -> v == null);
    }

    @Test
    public void givenUserHasNotDependendies_whenVerifyDependencies_thenChangeNothing() {
        inputStub.setContactId(null);
        inputStub.setDefaultRoleId(null);
        UserModel model = new UserModel();

        subject.verifyDependencies(model, inputStub);

        assertThat(model).extracting("contact", "defaultRole").allMatch(v -> v == null);
    }

    @Test
    public void givenTenantExists_whenVerifyTenantModel_thenReturnTenantModel() {
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new TenantModel()));

        TenantModel result = subject.verifyTenantModel(1);

        assertNotNull(result);
        verify(tenantRepositoryMock, times(1)).findByIdAndDeletedIsFalse(eq(1));
    }

    @Test
    public void givenTenantNotExists_whenVerifyTenantModel_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyTenantModel(1));
        assertThat(e.getMessage()).isEqualTo("Tenant not found");
    }

/*     @Test
    public void givenValidInputs_whenAddUserToTenant_thenSucceed() {
        TenantModel tenantStub = new TenantModel();
        tenantStub.setUsers(new HashSet<>());
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new UserModel()));
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(tenantStub));
        when(converterMock.convert(any(), eq(UserTo.class))).thenReturn(new UserTo());

        UserTo result = subject.addUserToTenant(2, 2);

        assertNotNull(result);
        assertThat(tenantStub.getUsers()).hasSize(1);
    } */

    @Test
    public void givenValidInputs_whenRemoveUserFromTenant_thenSucceed() {
        UserModel userStub = new UserModel();
        TenantModel tenantStub = new TenantModel();
        tenantStub.setUsers(new HashSet<>());
        tenantStub.getUsers().add(userStub);
        when(userRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(userStub));
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(tenantStub));
        when(converterMock.convert(any(), eq(UserTo.class))).thenReturn(new UserTo());

        UserTo result = subject.removeUserFromTenant(2, 2);

        assertNotNull(result);
        assertThat(tenantStub.getUsers()).hasSize(0);
    }

}