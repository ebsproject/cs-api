package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.JobTypeModel;
import org.ebs.model.repos.JobTypeRepository;
import org.ebs.services.to.JobTypeTo;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class JobTypeServiceImplTest {

    private @Mock JobTypeRepository jobTypeRepositoryMock;
	private @Mock ConversionService converterMock;
    
    private JobTypeServiceImpl subject;

    @BeforeEach
    public void init() {
        subject = new JobTypeServiceImpl(jobTypeRepositoryMock, converterMock);
    }

    @Test
    public void givenJobTypeExists_whenFindJobType_thenReturnNotEmpty() {
        when(jobTypeRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new JobTypeModel()));
        when(converterMock.convert(any(), eq(JobTypeTo.class))).thenReturn(new JobTypeTo());

        Optional<JobTypeTo> object = subject.findJobType(1);

        assertTrue(object.isPresent());
        verify(jobTypeRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenJobTypeExistsAndDeleted_whenFindJobType_thenReturnEmpty() {
        JobTypeModel t = new JobTypeModel();
        t.setDeleted(true);
        when(jobTypeRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<JobTypeTo> object = subject.findJobType(1);

        assertFalse(object.isPresent());
        verify(jobTypeRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenJobTypeIdLessThan1_whenFindJobType_thenReturnEmpty() {
        Optional<JobTypeTo> object = subject.findJobType(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenJobTypeListExist_whenFindJobTypes_thenReturnNotEmpty() {
        List<JobTypeModel> jobTypeList = Arrays.asList(new JobTypeModel(), new JobTypeModel());
        when(jobTypeRepositoryMock.findByCriteria(eq(JobTypeModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<JobTypeModel>(jobTypeList, PageRequest.of(0, 10), 2));

        Page<JobTypeTo> object = subject.findJobTypes(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(jobTypeList);
        verify(jobTypeRepositoryMock, times(1)).findByCriteria(eq(JobTypeModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenJobTypeListNotExists_whenFindJobType_thenReturnEmpty() {
        when(jobTypeRepositoryMock.findByCriteria(eq(JobTypeModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<JobTypeModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<JobTypeTo> object = subject.findJobTypes(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(jobTypeRepositoryMock, times(1)).findByCriteria(eq(JobTypeModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

}
