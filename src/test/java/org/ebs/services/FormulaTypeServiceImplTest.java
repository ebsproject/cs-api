package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.FormulaTypeModel;
import org.ebs.model.repos.FormulaTypeRepository;
import org.ebs.services.to.FormulaTypeTo;
import org.ebs.services.to.Input.FormulaTypeInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class FormulaTypeServiceImplTest {

    private @Mock FormulaTypeRepository formulaTypeRepositoryMock;
	private @Mock ConversionService converterMock;
    
    private FormulaTypeServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private FormulaTypeInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private FormulaTypeModel modelStub;

    @BeforeEach
    public void init() {
        subject = new FormulaTypeServiceImpl(converterMock, formulaTypeRepositoryMock);
    }

    @Test
    public void givenFormulaTypeExists_whenFindFormulaType_thenReturnNotEmpty() {
        when(formulaTypeRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new FormulaTypeModel()));
        when(converterMock.convert(any(), eq(FormulaTypeTo.class))).thenReturn(new FormulaTypeTo());

        Optional<FormulaTypeTo> object = subject.findFormulaType(1);

        assertTrue(object.isPresent());
        verify(formulaTypeRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenFormulaTypeExistsAndDeleted_whenFindFormulaType_thenReturnEmpty() {
        FormulaTypeModel t = new FormulaTypeModel();
        t.setDeleted(true);
        when(formulaTypeRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<FormulaTypeTo> object = subject.findFormulaType(1);

        assertFalse(object.isPresent());
        verify(formulaTypeRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenFormulaTypeIdLessThan1_whenFindFormulaType_thenReturnEmpty() {
        Optional<FormulaTypeTo> object = subject.findFormulaType(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenFormulaTypeListExist_whenFindFormulaTypes_thenReturnNotEmpty() {
        List<FormulaTypeModel> formulaTypeList = Arrays.asList(new FormulaTypeModel(), new FormulaTypeModel());
        when(formulaTypeRepositoryMock.findByCriteria(eq(FormulaTypeModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<FormulaTypeModel>(formulaTypeList, PageRequest.of(0, 10), 2));

        Page<FormulaTypeTo> object = subject.findFormulaTypes(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(formulaTypeList);
        verify(formulaTypeRepositoryMock, times(1)).findByCriteria(eq(FormulaTypeModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenFormulaTypeListNotExists_whenFindFormulaType_thenReturnEmpty() {
        when(formulaTypeRepositoryMock.findByCriteria(eq(FormulaTypeModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<FormulaTypeModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<FormulaTypeTo> object = subject.findFormulaTypes(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(formulaTypeRepositoryMock, times(1)).findByCriteria(eq(FormulaTypeModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidFormulaTypeInput_whenCreateFormulaType_thenPersistFormulaType() {
        when(converterMock.convert(any(),eq(FormulaTypeModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(FormulaTypeTo.class))).thenReturn(new FormulaTypeTo());
        when(formulaTypeRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        FormulaTypeTo result = subject.createFormulaType(inputStub);

        assertNotNull(result);
        verify(formulaTypeRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidFormulaTypeInput_whenModifyFormulaType_thenPersistFormulaType() {
        FormulaTypeModel model = new FormulaTypeModel();

        when(converterMock.convert(any(),eq(FormulaTypeModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(FormulaTypeTo.class))).thenReturn(new FormulaTypeTo());
        when(formulaTypeRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        FormulaTypeTo result = subject.modifyFormulaType(inputStub);

        assertNotNull(result);
        verify(formulaTypeRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenFormulaTypeNotExists_whenModifyFormulaType_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyFormulaType(inputStub));
        assertThat(e.getMessage()).isEqualTo("FormulaType not found");
    }
 
    @Test
    public void givenFormulaTypeExists_whenDeleteFormulaType_thenSuccess() {
        FormulaTypeModel model = new FormulaTypeModel();

        when(formulaTypeRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteFormulaType(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(formulaTypeRepositoryMock,times(1)).findById(anyInt());
        verify(formulaTypeRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenFormulaTypeNotExists_whenDeleteFormulaType_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteFormulaType(anyInt()));
        assertThat(e.getMessage()).isEqualTo("FormulaType not found");
    }
    
}
