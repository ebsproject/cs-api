package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.LanguageModel;
import org.ebs.model.repos.LanguageRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.services.to.LanguageTo;
import org.ebs.services.to.Input.LanguageInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class LanguageServiceImplTest {

    private @Mock LanguageRepository languageRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock TranslationRepository translationRepositoryMock;
    
    private LanguageServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private LanguageInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private LanguageModel modelStub;

    @BeforeEach
    public void init() {
        subject = new LanguageServiceImpl(languageRepositoryMock, converterMock, translationRepositoryMock);
    }

    @Test
    public void givenLanguageExists_whenFindLanguage_thenReturnNotEmpty() {
        when(languageRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new LanguageModel()));
        when(converterMock.convert(any(), eq(LanguageTo.class))).thenReturn(new LanguageTo());

        Optional<LanguageTo> object = subject.findLanguage(1);

        assertTrue(object.isPresent());
        verify(languageRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenLanguageExistsAndDeleted_whenFindLanguage_thenReturnEmpty() {
        LanguageModel t = new LanguageModel();
        t.setDeleted(true);
        when(languageRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<LanguageTo> object = subject.findLanguage(1);

        assertFalse(object.isPresent());
        verify(languageRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenLanguageIdLessThan1_whenFindLanguage_thenReturnEmpty() {
        Optional<LanguageTo> object = subject.findLanguage(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenLanguageListExist_whenFindLanguages_thenReturnNotEmpty() {
        List<LanguageModel> languageList = Arrays.asList(new LanguageModel(), new LanguageModel());
        when(languageRepositoryMock.findByCriteria(eq(LanguageModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<LanguageModel>(languageList, PageRequest.of(0, 10), 2));

        Page<LanguageTo> object = subject.findLanguages(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(languageList);
        verify(languageRepositoryMock, times(1)).findByCriteria(eq(LanguageModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenLanguageListNotExists_whenFindLanguage_thenReturnEmpty() {
        when(languageRepositoryMock.findByCriteria(eq(LanguageModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<LanguageModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<LanguageTo> object = subject.findLanguages(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(languageRepositoryMock, times(1)).findByCriteria(eq(LanguageModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidLanguageInput_whenCreateLanguage_thenPersistLanguage() {
        when(converterMock.convert(any(),eq(LanguageModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(LanguageTo.class))).thenReturn(new LanguageTo());
        when(languageRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        LanguageTo result = subject.createLanguage(inputStub);

        assertNotNull(result);
        verify(languageRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidLanguageInput_whenModifyLanguage_thenPersistLanguage() {
        LanguageModel model = new LanguageModel();

        when(converterMock.convert(any(),eq(LanguageModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(LanguageTo.class))).thenReturn(new LanguageTo());
        when(languageRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        LanguageTo result = subject.modifyLanguage(inputStub);

        assertNotNull(result);
        verify(languageRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenLanguageNotExists_whenModifyLanguage_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyLanguage(inputStub));
        assertThat(e.getMessage()).isEqualTo("Language not found");
    }
 
    @Test
    public void givenLanguageExists_whenDeleteLanguage_thenSuccess() {
        LanguageModel model = new LanguageModel();

        when(languageRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteLanguage(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(languageRepositoryMock,times(1)).findById(anyInt());
        verify(languageRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenLanguageNotExists_whenDeleteLanguage_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteLanguage(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Language not found");
    }
    
}
