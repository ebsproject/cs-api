package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AlertModel;
import org.ebs.model.AlertRuleModel;
import org.ebs.model.repos.AlertRepository;
import org.ebs.model.repos.AlertRuleRepository;
import org.ebs.services.to.AlertRuleTo;
import org.ebs.services.to.Input.AlertRuleInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class AlertRuleServiceImplTest {

    private @Mock AlertRuleRepository alertRuleRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock AlertRepository alertRepositoryMock;
    
    private AlertRuleServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private AlertRuleInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private AlertRuleModel modelStub;

    @BeforeEach
    public void init() {
        subject = new AlertRuleServiceImpl(alertRepositoryMock, converterMock, alertRuleRepositoryMock);
    }

    @Test
    public void givenAlertRuleExists_whenFindAlertRule_thenReturnNotEmpty() {
        when(alertRuleRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new AlertRuleModel()));
        when(converterMock.convert(any(), eq(AlertRuleTo.class))).thenReturn(new AlertRuleTo());

        Optional<AlertRuleTo> object = subject.findAlertRule(1);

        assertTrue(object.isPresent());
        verify(alertRuleRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAlertRuleExistsAndDeleted_whenFindAlertRule_thenReturnEmpty() {
        AlertRuleModel t = new AlertRuleModel();
        t.setDeleted(true);
        when(alertRuleRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<AlertRuleTo> object = subject.findAlertRule(1);

        assertFalse(object.isPresent());
        verify(alertRuleRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenAlertRuleIdLessThan1_whenFindAlertRule_thenReturnEmpty() {
        Optional<AlertRuleTo> object = subject.findAlertRule(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAlertRuleListExist_whenFindAlertRules_thenReturnNotEmpty() {
        List<AlertRuleModel> alertRuleList = Arrays.asList(new AlertRuleModel(), new AlertRuleModel());
        when(alertRuleRepositoryMock.findByCriteria(eq(AlertRuleModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AlertRuleModel>(alertRuleList, PageRequest.of(0, 10), 2));

        Page<AlertRuleTo> object = subject.findAlertRules(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(alertRuleList);
        verify(alertRuleRepositoryMock, times(1)).findByCriteria(eq(AlertRuleModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenAlertRuleListNotExists_whenFindAlertRule_thenReturnEmpty() {
        when(alertRuleRepositoryMock.findByCriteria(eq(AlertRuleModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<AlertRuleModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<AlertRuleTo> object = subject.findAlertRules(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(alertRuleRepositoryMock, times(1)).findByCriteria(eq(AlertRuleModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidAlertRuleInput_whenCreateAlertRule_thenPersistAlertRule() {
        when(converterMock.convert(any(),eq(AlertRuleModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(AlertRuleTo.class))).thenReturn(new AlertRuleTo());
        when(alertRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new AlertModel()));
        when(alertRuleRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        AlertRuleTo result = subject.createAlertRule(inputStub);

        assertNotNull(result);
        verify(alertRuleRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidAlertRuleInput_whenModifyAlertRule_thenPersistAlertRule() {
        AlertRuleModel model = new AlertRuleModel();

        when(converterMock.convert(any(),eq(AlertRuleModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(AlertRuleTo.class))).thenReturn(new AlertRuleTo());
        when(alertRuleRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        AlertRuleTo result = subject.modifyAlertRule(inputStub);

        assertNotNull(result);
        verify(alertRuleRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenAlertRuleNotExists_whenModifyAlertRule_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyAlertRule(inputStub));
        assertThat(e.getMessage()).isEqualTo("AlertRule not found");
    }
 
    @Test
    public void givenAlertRuleExists_whenDeleteAlertRule_thenSuccess() {
        AlertRuleModel model = new AlertRuleModel();

        when(alertRuleRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteAlertRule(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(alertRuleRepositoryMock,times(1)).findById(anyInt());
        verify(alertRuleRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenAlertRuleNotExists_whenDeleteAlertRule_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteAlertRule(anyInt()));
        assertThat(e.getMessage()).isEqualTo("AlertRule not found");
    }
    
}
