package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.HtmlTagModel;
import org.ebs.model.LanguageModel;
import org.ebs.model.TranslationModel;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.LanguageRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.services.to.TranslationTo;
import org.ebs.services.to.Input.TranslationInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class TranslationServiceImplTest {

    private @Mock TranslationRepository translationRepositoryMock;
	private @Mock ConversionService converterMock;
    private @Mock LanguageRepository languageRepositoryMock;
    private @Mock HtmlTagRepository htmlTagRepositoryMock;
    
    private TranslationServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private TranslationInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private TranslationModel modelStub;

    @BeforeEach
    public void init() {
        subject = new TranslationServiceImpl(htmlTagRepositoryMock, languageRepositoryMock, converterMock, translationRepositoryMock);
    }

    @Test
    public void givenTranslationExists_whenFindTranslation_thenReturnNotEmpty() {
        when(translationRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new TranslationModel()));
        when(converterMock.convert(any(), eq(TranslationTo.class))).thenReturn(new TranslationTo());

        Optional<TranslationTo> object = subject.findTranslation(1);

        assertTrue(object.isPresent());
        verify(translationRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenTranslationExistsAndDeleted_whenFindTranslation_thenReturnEmpty() {
        TranslationModel t = new TranslationModel();
        t.setDeleted(true);
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<TranslationTo> object = subject.findTranslation(1);

        assertFalse(object.isPresent());
        verify(translationRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenTranslationIdLessThan1_whenFindTranslation_thenReturnEmpty() {
        Optional<TranslationTo> object = subject.findTranslation(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenTranslationListExist_whenFindTranslations_thenReturnNotEmpty() {
        List<TranslationModel> translationList = Arrays.asList(new TranslationModel(), new TranslationModel());
        when(translationRepositoryMock.findByCriteria(eq(TranslationModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<TranslationModel>(translationList, PageRequest.of(0, 10), 2));

        Page<TranslationTo> object = subject.findTranslations(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(translationList);
        verify(translationRepositoryMock, times(1)).findByCriteria(eq(TranslationModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenTranslationListNotExists_whenFindTranslation_thenReturnEmpty() {
        when(translationRepositoryMock.findByCriteria(eq(TranslationModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<TranslationModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<TranslationTo> object = subject.findTranslations(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(translationRepositoryMock, times(1)).findByCriteria(eq(TranslationModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidTranslationInput_whenCreateTranslation_thenPersistTranslation() {
        when(converterMock.convert(any(),eq(TranslationModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(TranslationTo.class))).thenReturn(new TranslationTo());
        when(translationRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(languageRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new LanguageModel()));
        when(htmlTagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new HtmlTagModel()));
        TranslationTo result = subject.createTranslation(inputStub);

        assertNotNull(result);
        verify(translationRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidTranslationInput_whenModifyTranslation_thenPersistTranslation() {
        TranslationModel model = new TranslationModel();

        when(converterMock.convert(any(),eq(TranslationModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(TranslationTo.class))).thenReturn(new TranslationTo());
        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        TranslationTo result = subject.modifyTranslation(inputStub);

        assertNotNull(result);
        verify(translationRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenTranslationNotExists_whenModifyTranslation_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyTranslation(inputStub));
        assertThat(e.getMessage()).isEqualTo("Translation not found");
    }
 
    @Test
    public void givenTranslationExists_whenDeleteTranslation_thenSuccess() {
        TranslationModel model = new TranslationModel();

        when(translationRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteTranslation(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(translationRepositoryMock,times(1)).findById(anyInt());
        verify(translationRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenTranslationNotExists_whenDeleteTranslation_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteTranslation(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Translation not found");
    }
    
}
