package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.PrintoutTemplateModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.PrintoutTemplateRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.PrintoutTemplate;
import org.ebs.services.to.Input.PrintoutTemplateInput;
import org.ebs.util.Connection;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class PrintoutTemplateServiceImplTest {

    private @Mock PrintoutTemplateRepository printoutTemplateRepositoryMock;
	private @Mock ConversionService converterMock;
    private @Mock ContactRepository contactRepositoryMock;
    private @Mock ProductRepository productRepositoryMock;
    private @Mock TenantRepository tenantRepositoryMock;
    private @Mock UserRepository userRepositoryMock;
    
    private PrintoutTemplateServiceImpl subject;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private PrintoutTemplateInput inputStub;
    @Mock
    private PrintoutTemplateModel modelStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private FilterInput filterInputStub;

    @BeforeEach
    public void init() {
        subject = new PrintoutTemplateServiceImpl(printoutTemplateRepositoryMock, converterMock, contactRepositoryMock, productRepositoryMock, tenantRepositoryMock, userRepositoryMock);
    }

    @Test
    public void givenPrintoutTemplateExists_whenFindPrintoutTemplate_thenReturnNotEmpty() {
        when(printoutTemplateRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new PrintoutTemplateModel()));
        when(converterMock.convert(any(), eq(PrintoutTemplate.class))).thenReturn(new PrintoutTemplate());

        Optional<PrintoutTemplate> object = subject.findPrintoutTemplate(1);

        assertTrue(object.isPresent());
        verify(printoutTemplateRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenPrintoutTemplateExistsAndDeleted_whenFindPrintoutTemplate_thenReturnEmpty() {
        PrintoutTemplateModel t = new PrintoutTemplateModel();
        t.setDeleted(true);
        when(printoutTemplateRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<PrintoutTemplate> object = subject.findPrintoutTemplate(1);

        assertFalse(object.isPresent());
        verify(printoutTemplateRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenPrintoutTemplateIdLessThan1_whenFindPrintoutTemplate_thenReturnEmpty() {
        Optional<PrintoutTemplate> object = subject.findPrintoutTemplate(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenPrintoutTemplateListExist_whenFindPrintoutTemplates_thenReturnNotEmpty() {
        List<PrintoutTemplateModel> printoutTemplateList = Arrays.asList(new PrintoutTemplateModel(), new PrintoutTemplateModel());
        when(printoutTemplateRepositoryMock.findByCriteria(eq(PrintoutTemplateModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<PrintoutTemplateModel>(printoutTemplateList, PageRequest.of(0, 10), 2));
        Page<PrintoutTemplate> object = subject.findPrintoutTemplates(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(printoutTemplateList);
        verify(printoutTemplateRepositoryMock, times(1)).findByCriteria(eq(PrintoutTemplateModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenPrintoutTemplateListNotExists_whenFindPrintoutTemplate_thenReturnEmpty() {
        when(printoutTemplateRepositoryMock.findByCriteria(eq(PrintoutTemplateModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<PrintoutTemplateModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<PrintoutTemplate> object = subject.findPrintoutTemplates(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(printoutTemplateRepositoryMock, times(1)).findByCriteria(eq(PrintoutTemplateModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidPrintoutTemplateInput_whenCreatePrintoutTemplate_thenPersistPrintoutTemplate() {
        when(converterMock.convert(any(),eq(PrintoutTemplateModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(PrintoutTemplate.class))).thenReturn(new PrintoutTemplate());
        when(printoutTemplateRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(modelStub.getName()).thenReturn(anyString());

        PrintoutTemplate result = subject.createPrintoutTemplate(inputStub);

        assertNotNull(result);
        verify(printoutTemplateRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidPrintoutTemplateInput_whenModifyPrintoutTemplate_thenPersistPrintoutTemplate() {
        PrintoutTemplateModel model = new PrintoutTemplateModel();

        when(converterMock.convert(any(),eq(PrintoutTemplate.class))).thenReturn(new PrintoutTemplate());
        when(printoutTemplateRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        PrintoutTemplate result = subject.modifyPrintoutTemplate(inputStub);

        assertNotNull(result);
        verify(printoutTemplateRepositoryMock, times(1)).save(any());
        verify(converterMock, times(1)).convert(any(), any());
    }

    @Test
    public void givenPrintoutTemplateNotExists_whenModifyPrintoutTemplate_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyPrintoutTemplate(inputStub));
        assertThat(e.getMessage()).isEqualTo("PrintoutTemplate not found");
    }
 
    @Test
    public void givenPrintoutTemplateExists_whenDeletePrintoutTemplate_thenSuccess() {
        PrintoutTemplateModel model = new PrintoutTemplateModel();

        when(printoutTemplateRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deletePrintoutTemplate(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(printoutTemplateRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(printoutTemplateRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenPrintoutTemplateNotExists_whenDeletePrintoutTemplate_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deletePrintoutTemplate(anyInt()));
        assertThat(e.getMessage()).isEqualTo("PrintoutTemplate not found");
    }
    
}
