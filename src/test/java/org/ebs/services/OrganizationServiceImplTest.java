package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AddressModel;
import org.ebs.model.CustomerModel;
import org.ebs.model.OrganizationModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.CustomerRepository;
import org.ebs.model.repos.OrganizationRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.OrganizationTo;
import org.ebs.services.to.Input.AddressInput;
import org.ebs.services.to.Input.OrganizationInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class OrganizationServiceImplTest {

    private @Mock OrganizationRepository organizationRepositoryMock;
	private @Mock ConversionService converterMock;
    private @Mock TenantRepository tenantRepositoryMock;
    private @Mock CustomerRepository customerRepositoryMock;
    private @Mock AddressRepository addressRepositoryMock;
    
    private OrganizationServiceImpl subject;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private OrganizationInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private OrganizationModel modelStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private AddressInput addressInputStub;

    @BeforeEach
    public void init() {
        subject = new OrganizationServiceImpl(organizationRepositoryMock, converterMock, tenantRepositoryMock, customerRepositoryMock, addressRepositoryMock);
    }

    @Test
    public void givenOrganizationExists_whenFindOrganization_thenReturnNotEmpty() {
        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new OrganizationModel()));
        when(converterMock.convert(any(), eq(OrganizationTo.class))).thenReturn(new OrganizationTo());

        Optional<OrganizationTo> object = subject.findOrganization(1);

        assertTrue(object.isPresent());
        verify(organizationRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenOrganizationExistsAndDeleted_whenFindOrganization_thenReturnEmpty() {
        OrganizationModel t = new OrganizationModel();
        t.setDeleted(true);
        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<OrganizationTo> object = subject.findOrganization(1);

        assertFalse(object.isPresent());
        verify(organizationRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenOrganizationIdLessThan1_whenFindOrganization_thenReturnEmpty() {
        Optional<OrganizationTo> object = subject.findOrganization(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenOrganizationListExist_whenFindOrganizations_thenReturnNotEmpty() {
        List<OrganizationModel> organizationList = Arrays.asList(new OrganizationModel(), new OrganizationModel());
        when(organizationRepositoryMock.findByCriteria(eq(OrganizationModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<OrganizationModel>(organizationList, PageRequest.of(0, 10), 2));

        Page<OrganizationTo> object = subject.findOrganizations(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(organizationList);
        verify(organizationRepositoryMock, times(1)).findByCriteria(eq(OrganizationModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenOrganizationListNotExists_whenFindOrganization_thenReturnEmpty() {
        when(organizationRepositoryMock.findByCriteria(eq(OrganizationModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<OrganizationModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<OrganizationTo> object = subject.findOrganizations(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(organizationRepositoryMock, times(1)).findByCriteria(eq(OrganizationModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidOrganizationInput_whenCreateOrganization_thenPersistOrganization() {
        when(converterMock.convert(any(),eq(OrganizationModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(OrganizationTo.class))).thenReturn(new OrganizationTo());
        when(converterMock.convert(any(),eq(AddressModel.class))).thenReturn(new AddressModel());
        when(organizationRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new CustomerModel()));
        when(addressRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new AddressModel()));
        when(inputStub.getAddress()).thenReturn(addressInputStub);
        OrganizationTo result = subject.createOrganization(inputStub);

        assertNotNull(result);
        verify(organizationRepositoryMock, times(1)).save(any());
        verify(converterMock, times(3)).convert(any(), any());
    }

    @Test
    public void givenValidOrganizationInput_whenModifyOrganization_thenPersistOrganization() {
        OrganizationModel model = new OrganizationModel();

        when(converterMock.convert(any(),eq(OrganizationModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(OrganizationTo.class))).thenReturn(new OrganizationTo());
        when(converterMock.convert(any(),eq(AddressModel.class))).thenReturn(new AddressModel());
        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new CustomerModel()));
        OrganizationTo result = subject.modifyOrganization(inputStub);

        assertNotNull(result);
        verify(organizationRepositoryMock, times(1)).save(any());
        verify(converterMock, times(3)).convert(any(), any());
    }

    @Test
    public void givenOrganizationNotExists_whenModifyOrganization_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyOrganization(inputStub));
        assertThat(e.getMessage()).isEqualTo("Organization not found");
    }
 
    @Test
    public void givenOrganizationExists_whenDeleteOrganization_thenSuccess() {
        OrganizationModel model = new OrganizationModel();

        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteOrganization(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(organizationRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(organizationRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenOrganizationNotExists_whenDeleteOrganization_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteOrganization(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Organization not found");
    }
    
}
