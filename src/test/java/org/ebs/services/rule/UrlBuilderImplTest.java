package org.ebs.services.rule;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.List;

import org.ebs.model.DomainInstanceModel;
import org.ebs.model.repos.DomainInstanceRepository;
import org.ebs.model.rule.ApiSegmentModel.ApiType;
import org.ebs.services.to.rule.ApiSegment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;

@ExtendWith(MockitoExtension.class)
public class UrlBuilderImplTest {
    
    @Mock
    private DomainInstanceRepository domainInstanceRepoMock;

    private UrlBuilderImpl subject;
    
    @BeforeEach
    public void init() {
        subject = new UrlBuilderImpl(domainInstanceRepoMock);
    }

    @Test
    public void givenGraphqlTypeWhenGetUrlThenReturnGraphqlEndpoint() {
        DomainInstanceModel mockInstance = new DomainInstanceModel();
        mockInstance.setSgContext("http://someserver:8080/url/");

        when(domainInstanceRepoMock.findByDomainIdAndDeletedIsFalse(anyInt()))
                .thenReturn(List.of(mockInstance));

        ApiSegment input = new ApiSegment(ApiType.GRAPHQL, HttpMethod.POST, "CS", "graphql", null,
                null);
        String result = subject.getUrl(input);
        assertEquals("http://someserver:8080/url/graphql", result);
    }

    @Test
    public void givenRestTypeWhenGetUrlThenReturnRestEndpoint() {
        DomainInstanceModel mockInstance = new DomainInstanceModel();
        mockInstance.setSgContext("http://someserver:8080/v3");

        when(domainInstanceRepoMock.findByDomainIdAndDeletedIsFalse(anyInt()))
                .thenReturn(List.of(mockInstance));

        ApiSegment input = new ApiSegment(ApiType.REST, HttpMethod.GET, "CB", "search-plots", null,
                null);
        String result = subject.getUrl(input);
        assertEquals("http://someserver:8080/v3/search-plots", result);

        mockInstance.setSgContext("http://someserver:8080/basepath");
        input = new ApiSegment(ApiType.REST, HttpMethod.GET, "SM", "batches", null,
                null);
        result = subject.getUrl(input);
        assertEquals("http://someserver:8080/basepath/batches", result);

    }
}
