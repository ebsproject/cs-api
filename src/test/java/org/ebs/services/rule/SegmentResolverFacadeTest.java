package org.ebs.services.rule;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.ebs.model.rule.repos.SegmentRepository;
import org.ebs.model.rule.repos.SequenceSegmentRepository;
import org.ebs.services.to.rule.ApiSegment;
import org.ebs.services.to.rule.SequenceSegment;
import org.ebs.services.to.rule.StaticSegment;
import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SegmentResolverFacadeTest {

    @Mock
    TokenGenerator tg;
    @Mock
    SegmentRepository segmentRepoMock;
    @Mock
    SequenceSegmentRepository seqSegmentRepoMock;
    @Mock
    RuleScripts ruleScriptsMock;
    @Mock
    private UrlBuilder urlBuilderMock;

    SegmentResolverFacade subject = new SegmentResolverFacade(List.of(
            new StaticSegmentResolver()
            , new SequenceSegmentResolver(segmentRepoMock, seqSegmentRepoMock, ruleScriptsMock)
        ,new ApiSegmentResolver(tg, ruleScriptsMock, urlBuilderMock)
    ));

    @Test
    void givenResolversExistWhenResolverForThenReturnResolver() {
        SegmentResolver<?> result = subject.resolverFor(new ApiSegment());
        assertEquals(ApiSegmentResolver.class, result.getClass());

        result = subject.resolverFor(new SequenceSegment());
        assertEquals(SequenceSegmentResolver.class, result.getClass());

        result = subject.resolverFor(new StaticSegment());
        assertEquals(StaticSegmentResolver.class, result.getClass());
    }

    @Test
    void givenRedundantResolversExistWhenConstructFacadeThenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> new SegmentResolverFacade(List.of(
            new StaticSegmentResolver(), new StaticSegmentResolver()
        )));

        assertEquals("Only one segment resolver can be defined for StaticSegment", e.getMessage());
    }
}
