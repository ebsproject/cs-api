package org.ebs.services.rule;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Calendar;

import org.ebs.services.to.rule.Segment;
import org.junit.jupiter.api.Test;

/**
 * Tests for default implementations of format()
 */
public class SegmentResolverTest {

    SegmentResolver<Segment> subject = new SegmentResolver<Segment>() {
        @Override
        public String resolve(Segment segment, String val, String context) {
            throw new UnsupportedOperationException("Unimplemented method 'resolve'");
        }

        @Override
        public Class<Segment> resolverFor() {
            throw new UnsupportedOperationException("Unimplemented method 'resolverFor'");
        }
        
    };

    @Test
    public void givenInstantValueWhenFormatThenFormatToString() {
        Calendar cal = Calendar.getInstance();
        cal.set(2023, 1, 11);
        
        String result = subject.format("yyyy_MM:dd", cal.toInstant());
        assertEquals("2023_02:11", result);

        result = subject.format("DD-MMM-YY", cal.toInstant());
        assertEquals("42-Feb-23", result);

    }

    @Test
    public void givenNumberValueWhenFormatThenFormatToString() {
        String result = subject.format("0000000", 24);
        assertEquals("0000024", result);

        result = subject.format("#####", 24);
        assertEquals("24", result);

    }

    @Test
    public void givenStringValueWhenFormatThenFormatToString() {
        String result = subject.format("[[%s]]", "abc");
        assertEquals("[[abc]]", result);
    }
}
