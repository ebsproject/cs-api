package org.ebs.services.rule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.script.ScriptException;

import org.ebs.model.rule.SegmentModel;
import org.ebs.model.rule.SequenceSegmentModel;
import org.ebs.model.rule.repos.SegmentRepository;
import org.ebs.model.rule.repos.SequenceSegmentRepository;
import org.ebs.services.to.rule.SequenceSegment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 *  in subject.resolve() value is ignored for sequence segments
 */
@ExtendWith(MockitoExtension.class)
public class SequenceSegmentResolverTest {
    
    @Mock
    private SegmentRepository segmentRepoMock;
    @Mock
    private SequenceSegmentRepository seqSegmentRepoMock;
    @Mock
    private RuleScripts ruleScriptsMock;

    private SegmentModel sm;

    private SequenceSegmentResolver subject;



    @BeforeEach
    public void init() {
        sm = new SegmentModel();
        sm.setSequenceSegment(new SequenceSegmentModel(123, 1, 1, 0, null, null, null));
        subject = new SequenceSegmentResolver(segmentRepoMock, seqSegmentRepoMock, ruleScriptsMock);
    }


    @Test
    public void givenSequenceSegmentFirstUseWhenResolveThenReturnLowestAsNextValue() {

        when(segmentRepoMock.findById(any())).thenReturn(Optional.ofNullable(sm));
        when(seqSegmentRepoMock.save(any())).thenReturn(sm.getSequenceSegment());

        SequenceSegment seg = new SequenceSegment();
        seg.setFormat("0000000");
        seg.setLowest(1);
        seg.setIncrement(1);
        seg.setLast(0);


        String result = subject.resolve(seg, null, null);
        assertEquals("0000001", result);
    }

    @Test
    public void givenSequenceSegmentWhenResolveThenReturnNextValue() {

        SequenceSegmentModel ssm = new SequenceSegmentModel(9, 100, 2, 104, null, null, null);
        sm.setSequenceSegment(ssm);

        when(segmentRepoMock.findById(any())).thenReturn(Optional.of(sm));
        when(seqSegmentRepoMock.save(any()))
            .thenReturn(sm.getSequenceSegment());

        SequenceSegment seg = new SequenceSegment();
        seg.setFormat("00000");


        String result = subject.resolve(seg, null, null);
        assertEquals("00106", result);
        result = subject.resolve(seg, "123", null);
        assertEquals("00108", result);
        result = subject.resolve(seg, "123", null);
        assertEquals("00110", result);
    }

    @Test
    public void givenSequenceSegmentNeedsResetWhenResolveThenReturnLowestAsNextValue() throws NoSuchMethodException, ScriptException, JsonMappingException, JsonProcessingException {
        JsonNode resetCondition = new ObjectMapper()
                .readTree("{\"reset-value\": \"123\", \"reset-condition\": \" $input != 0\", \"reset-value-function\": \"100\"}");
        SequenceSegmentModel ssm = new SequenceSegmentModel(9, 20, 2, 150, resetCondition, null, null);
        sm.setSequenceSegment(ssm);

        when(segmentRepoMock.findById(any())).thenReturn(Optional.of(sm));
        when(seqSegmentRepoMock.save(any())).thenReturn(ssm);
        when(ruleScriptsMock.execute(anyString())).thenReturn("true").thenReturn("100");

        SequenceSegment seg = new SequenceSegment();
        seg.setFormat("#####");

        String result = subject.resolve(seg, null, null);
        assertEquals("20", result);
    }

    @Test
    public void givenNoResetConditionWhenNeedsResetThenReturnFalse() {
        
        SequenceSegmentModel model = new SequenceSegmentModel(1, 1, 1, 0, null, "0", null);
        boolean result = subject.needsReset(model);

        assertFalse(result);
    }

    @Test
    public void givenResetConditionMetWhenNeedsResetThenReset() throws JsonMappingException,
            JsonProcessingException, NoSuchMethodException, ScriptException {
        JsonNode resetCondition = new ObjectMapper().readTree(
                "{\"reset-value\": \"123\", \"reset-condition\": \" $input != 0\", \"reset-value-function\": \"100\"}");

        when(ruleScriptsMock.execute(anyString())).thenReturn("true").thenReturn("100");

        SequenceSegmentModel model = new SequenceSegmentModel(1, 1, 1, 0, resetCondition, "0",
                null);
        boolean result = subject.needsReset(model);

        assertTrue(result);
        verify(seqSegmentRepoMock, times(1)).save(any());
    }

    @Test
    public void givenNoExistingKeyWhenNewFamilySequenceThenCreateNewFamilyKey() {
        SequenceSegmentModel saved = new SequenceSegmentModel(124, 1, 2, 0, null, "SOMEKEY", 123);
        when(seqSegmentRepoMock.save(any()))
                .thenReturn(saved);

        SequenceSegmentModel result = subject.newFamilySequence(new SequenceSegmentModel(), "SOMEKEY");

        assertThat(result).extracting("lowest","increment", "last", "familyKey", "parentId")
                .containsExactly(1,2,0,"SOMEKEY",123);
    }
}
