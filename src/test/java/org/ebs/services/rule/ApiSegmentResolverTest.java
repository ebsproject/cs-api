package org.ebs.services.rule;

import static org.ebs.model.rule.ApiSegmentModel.ApiType.GRAPHQL;
import static org.ebs.model.rule.ApiSegmentModel.ApiType.REST;
import static org.ebs.model.rule.SegmentModel.DataType.TEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

import java.io.IOException;

import javax.script.ScriptException;

import org.ebs.services.to.rule.ApiSegment;
import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

@ExtendWith(MockitoExtension.class)
public class ApiSegmentResolverTest {

    @Mock
    private TokenGenerator tokenGeneratorMock;
    @Mock
    private UrlBuilder urlBuilderMock;
    @Mock
    private RuleScripts ruleScriptsMock;
    private static MockWebServer mockServer;

    private static ApiSegmentResolver subject;
    


    @BeforeAll
    static void setUp() throws IOException {
        mockServer = new MockWebServer();
        mockServer.start();
    }
    @AfterAll
    static void shutdown() throws IOException {
        mockServer.shutdown();
    }

    @BeforeEach
    public void init() {
        subject = new ApiSegmentResolver(tokenGeneratorMock, ruleScriptsMock, urlBuilderMock);
    }

    @Test
    public void givenGraphQLApiSegmentWhenResolveThenReturnValue() {
        when(urlBuilderMock.getUrl(any())).thenReturn(String.format("http://%s:%s/%s",
                mockServer.getHostName(), mockServer.getPort(), "graphql"));
        try {
            when(ruleScriptsMock.invoke(eq("extract"), any())).thenReturn("SHLM");
        } catch (NoSuchMethodException | ScriptException e) {
            e.printStackTrace();
        }

        mockServer.enqueue(new MockResponse().setBody("{\"data\": {\"findServiceProvider\": {\"code\": \"SHLM\"}}}"));
        ApiSegment seg = new ApiSegment();
        seg.setDataType(TEXT);
        seg.setType(GRAPHQL);
        seg.setMethod(POST);
        seg.setServerUrl("CS");
        seg.setPathTemplate("graphql");
        seg.setBodyTemplate("query{findServiceProvider(id:{}){id,code}}");
        seg.setRequiresInput(true);
        seg.setResponseMapping("data.findServiceProvider.code");

        String result = subject.resolve(seg, "123", null);
        try {
            RecordedRequest recordedRequest = mockServer.takeRequest();
            assertEquals("POST", recordedRequest.getMethod());
            assertEquals("/graphql", recordedRequest.getPath());
            assertEquals("[text={\"query\":\"query{findServiceProvider(id:123){id,code}}\"}]",
                    recordedRequest.getBody().readByteString().toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals("SHLM", result);
    }

    @Test
    public void givenRestApiSegmentWhenResolveThenReturnValue() {
        when(urlBuilderMock.getUrl(any())).thenReturn( String.format("http://%s:%s/%s", mockServer.getHostName(), mockServer.getPort(), "user/{}"));
        mockServer.enqueue(new MockResponse()
                .setBody("{\"account\": \"user@email.com\"}"));
        try {
            when(ruleScriptsMock.invoke(eq("extract"), any())).thenReturn("user@email.com");
        } catch (NoSuchMethodException | ScriptException e) {
            e.printStackTrace();
        }
        
        ApiSegment seg = new ApiSegment();
        seg.setDataType(TEXT);
        seg.setType(REST);
        seg.setMethod(GET);
        seg.setServerUrl("CS");
        seg.setPathTemplate("user/{}");
        seg.setRequiresInput(true);
        seg.setResponseMapping("account");

        String result = subject.resolve(seg, "111", null);

        try {
            RecordedRequest recordedRequest = mockServer.takeRequest();
            assertEquals("GET", recordedRequest.getMethod());
            assertEquals("/user/111", recordedRequest.getPath());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals("user@email.com", result);
    }

    @Test
    public void givenSuccessfulResponseWhenExtractValueThenReturnValue() {
        try {
            when(ruleScriptsMock.invoke(eq("extract"), any())).thenReturn("ASD123").thenReturn("USR456");
        } catch (NoSuchMethodException | ScriptException e) {
            e.printStackTrace();
        }
        ApiSegment apiSegment = new ApiSegment();
        String response = "{\"id\": 123, \"code\": \"ASD123\", \"user\":{\"id\": 123, \"usercode\": \"USR456\"}}";
        String result = subject.extractValue(apiSegment, response, "code");
        assertEquals("ASD123", result);
        result = subject.extractValue(apiSegment, response, "user.usercode");
        assertEquals("USR456", result);
    }
}
