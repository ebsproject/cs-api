package org.ebs.services.rule;

import static org.ebs.model.rule.SegmentModel.DataType.TEXT;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.ebs.services.to.rule.StaticSegment;
import org.ebs.model.rule.SegmentModel.DataType;
import org.junit.jupiter.api.Test;

/*
 *  in subject.resolve() value is ignored for static segments
 */
public class StaticSegmentResolverTest {
    private StaticSegmentResolver subject = new StaticSegmentResolver();

    @Test
    public void givenStaticSegmentWhenResolveThenReturnStaticValue() {
        StaticSegment seg = new StaticSegment();
        seg.setDataType(TEXT);
        seg.setFormula("ABC");

        String result = subject.resolve(seg, null, null);
        assertEquals("ABC", result);
    }

    @Test
    public void givenStaticDateSegmentWhenResolveThenReturnStaticValue() {
        StaticSegment seg = new StaticSegment();
        seg.setFormula("2023-01-01"); //formula ignored for static date segments
        seg.setFormat("yyyy.MM.dd");
        seg.setDataType(DataType.DATE);

        ZonedDateTime zdt = Instant.now().atZone(ZoneId.systemDefault());
        String expected = String.format("%s.%02d.%02d", zdt.getYear(), zdt.getMonthValue(),zdt.getDayOfMonth());

        String result = subject.resolve(seg, null, null);
        assertEquals(expected, result);
    }

    @Test
    public void givenStaticNumberSegmentWhenResolveThenReturnStaticValue() {
        StaticSegment seg = new StaticSegment();
        seg.setFormula("777");
        seg.setFormat("00000");
        seg.setDataType(DataType.NUMBER);

        String result = subject.resolve(seg, null, null);
        assertEquals("00777", result);
    }
}
