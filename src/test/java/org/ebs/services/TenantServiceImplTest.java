package org.ebs.services;

import static java.time.Instant.now;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyIterable;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Period;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.model.CustomerModel;
import org.ebs.model.InstanceModel;
import org.ebs.model.OrganizationModel;
import org.ebs.model.ProductAuthorizationModel;
import org.ebs.model.ProductModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.CustomerRepository;
import org.ebs.model.repos.EmailTemplateRepository;
import org.ebs.model.repos.InstanceRepository;
import org.ebs.model.repos.NumberSequenceRuleRepository;
import org.ebs.model.repos.OrganizationRepository;
import org.ebs.model.repos.PrintoutTemplateRepository;
import org.ebs.model.repos.ProductAuthorizationRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.ProgramRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.TenantInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class TenantServiceImplTest {

    private @Mock
    TenantRepository tenantRepositoryMock;
    private @Mock
    ConversionService converterMock;
    private @Mock
    UserRepository userRepositoryMock;
    private @Mock
    InstanceRepository instanceRepositoryMock;
    private @Mock
    OrganizationRepository organizationRepositoryMock;
    private @Mock
    CustomerRepository customerRepositoryMock;
    private @Mock
    EmailTemplateRepository emailtemplateRepositoryMock;
    private @Mock
    NumberSequenceRuleRepository numbersequenceruleRepositoryMock;
    private @Mock
    ProductRepository productRepositoryMock;
    private @Mock
    ProgramRepository programRepositoryMock;
    private @Mock
    ProductAuthorizationRepository productAuthorizationRepoMock;
    private @Mock
    PrintoutTemplateRepository printoutTemplateRepositoryMock;
    private @Mock
    InstanceService instanceServiceMock;
    private @Mock
    CropRepository cropRepositoryMock;

    List<ProductModel> stubProducts = Arrays.asList(new ProductModel(1), new ProductModel(2),
            new ProductModel(3), new ProductModel(4), new ProductModel(5), new ProductModel(6),
            new ProductModel(7));

    /**
     * returns a set of {@link ProductModel} with id values in a given range.
     * Example: from = 3 to = 4 returns a set with two elements, id(3) and id(4)
     *
     * @param from
     *            starting id value
     * @param to
     *            ending id value
     * @return
     */
    private Set<ProductModel> stubProductModelSet(int from, int to) {
        assert from > 0 && to < 8;
        return new HashSet<>(stubProductModelList(from, to));

    }

    /**
     * returns a list of {@link ProductModel} with id values in a given range.
     * Example: from = 3 to = 4 returns a set with two elements, id(3) and id(4)
     *
     * @param from
     *            starting id value
     * @param to
     *            ending id value
     * @return
     */
    private List<ProductModel> stubProductModelList(int from, int to) {
        assert from > 0 && to < 8;
        return stubProducts.subList(from - 1, to);

    }

    private TenantServiceImpl subject;

    @BeforeEach
    public void init() {
        subject = new TenantServiceImpl(converterMock, tenantRepositoryMock,
                instanceRepositoryMock, organizationRepositoryMock,
                customerRepositoryMock,
                numbersequenceruleRepositoryMock, productRepositoryMock,
                productAuthorizationRepoMock, instanceServiceMock, userRepositoryMock);
    }

    @Test
    public void givenTenantExists_whenFindTenant_thenReturnNotEmpty() {
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new TenantModel()));
        when(converterMock.convert(any(), eq(TenantTo.class))).thenReturn(new TenantTo());

        Optional<TenantTo> object = subject.findTenant(1);

        assertTrue(object.isPresent());
        verify(tenantRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenTenantExistsAndDeleted_whenFindTenant_thenReturnEmpty() {
        TenantModel t = new TenantModel();
        t.setDeleted(true);
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<TenantTo> object = subject.findTenant(1);

        assertFalse(object.isPresent());
        verify(tenantRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenTenantNotExists_whenFindTenant_thenReturnEmpty() {
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.empty());

        Optional<TenantTo> object = subject.findTenant(1);

        assertFalse(object.isPresent());
        verify(tenantRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenTenantIdLessThan1_whenFindTenant_thenReturnEmpty() {
        Optional<TenantTo> object = subject.findTenant(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenTenantListExist_whenFindTenants_thenReturnNotEmpty() {
        List<TenantModel> tenantList = Arrays.asList(new TenantModel(), new TenantModel());
        when(tenantRepositoryMock.findByCriteria(eq(TenantModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<TenantModel>(tenantList, PageRequest.of(0, 10), 2));

        Page<TenantTo> object = subject.findTenants(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(tenantList);
        verify(tenantRepositoryMock, times(1)).findByCriteria(eq(TenantModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenTenantListNotExists_whenFindTenant_thenReturnEmpty() {
        when(tenantRepositoryMock.findByCriteria(eq(TenantModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<TenantModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<TenantTo> object = subject.findTenants(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(tenantRepositoryMock, times(1)).findByCriteria(eq(TenantModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidTenantInput_whenCreateTenant_thenPersistTenant() {
        TenantModel model = new TenantModel(0, "", new Date(), false, false, null, null, null, null,
                null, null, null, null, null, null);

        when(converterMock.convert(any(), eq(TenantModel.class))).thenReturn(model);
        when(converterMock.convert(any(), eq(TenantTo.class))).thenReturn(new TenantTo());
        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new OrganizationModel()));
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new CustomerModel()));
        when(tenantRepositoryMock.save(eq(model))).thenReturn(model);

        TenantTo result = subject.createTenant(new TenantInput(1, null, null, false, 0, 0, null,false));

        assertNotNull(result);
        verify(tenantRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenModelIsSaved_whenGenerateSingleInstanceModel_thenSucceed() {
        subject.generateSingleInstanceModel(new TenantModel(1));
        verify(instanceServiceMock, times(1)).createInstance(any());
    }

    @Test
    public void givenValidTenantInput_whenModifyTenant_thenPersistTenant() {
        TenantModel model = new TenantModel(1, "", new Date(), false, false, null, null, null, null,
                null, null, null, null, null, null);

        when(converterMock.convert(any(), eq(TenantModel.class))).thenReturn(model);
        when(converterMock.convert(any(), eq(TenantTo.class))).thenReturn(new TenantTo());
        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new OrganizationModel()));
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new CustomerModel()));
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new TenantModel()));

        TenantTo result = subject.modifyTenant(new TenantInput());

        assertNotNull(result);
        verify(tenantRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenTenantHasOrganizationAndCustomer_whenVerifyDependencies_thenSuccess() {
        Date date = Date.from(now().minus(Period.ofDays(1)));
        TenantInput input = new TenantInput(0, "a name", date, true, 1, 1, null,false);
        TenantModel model = new TenantModel(1, "a name", date, true,false, null, null, null, null,
                null, null, null, null, null, null);

        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new OrganizationModel()));
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new CustomerModel()));

        subject.verifyDependencies(model, input);

        assertNotNull(model.getOrganization());
        assertNotNull(model.getCustomer());
        assertTrue(model.isExpired());
    }

    @Test
    public void givenTenantHasNoOrganization_whenVerifyDependencies_thenFail() {
        Date date = Date.from(now().plus(Period.ofDays(1)));
        TenantInput input = new TenantInput(0, "a name", date, false, 1, 1, null, false);

        Exception e = assertThrows(RuntimeException.class,
                () -> subject.verifyDependencies(new TenantModel(), input));
        assertThat(e.getMessage()).isEqualTo("Organization not found");
    }

    @Test
    public void givenTenantHasNoCustomer_whenVerifyDependencies_thenFail() {
        Date date = Date.from(now().plus(Period.ofDays(1)));
        TenantInput input = new TenantInput(0, "a name", date, false, 1, 1, null, false);

        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new OrganizationModel()));

        Exception e = assertThrows(RuntimeException.class,
                () -> subject.verifyDependencies(new TenantModel(), input));
        assertThat(e.getMessage()).isEqualTo("Customer not found");
    }

    @Test
    public void givenTenantExists_whenVerifyTenantModel_thenReturnTenantModel() {
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new TenantModel()));

        TenantModel result = subject.verifyTenantModel(1);

        assertNotNull(result);
        verify(tenantRepositoryMock, times(1)).findByIdAndDeletedIsFalse(eq(1));
    }

    @Test
    public void givenTenantNotExists_whenVerifyTenantModel_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.verifyTenantModel(1));
        assertThat(e.getMessage()).isEqualTo("Tenant not found");
    }

    @Test
    public void givenValidInputs_whenAddProducts_thenSuccess() {
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new TenantModel()));
        when(converterMock.convert(any(), eq(TenantTo.class))).thenReturn(new TenantTo());

        TenantTo result = subject.addProducts(1, Arrays.asList(1, 2, 3));
        assertNotNull(result);
        verify(productAuthorizationRepoMock, times(1)).saveAll(anyIterable());
    }

    @Test
    public void givenAllNewProducts_whenExtractNewProductAuthorizations_thenAddAll() {
        Set<ProductModel> productsToAdd = stubProductModelSet(1, 3);
        Set<ProductModel> productsAuthorized = new HashSet<>();

        Set<ProductAuthorizationModel> result = subject.extractNewProductAuthorizations(
                new TenantModel(), productsToAdd, productsAuthorized);

        assertThat(result).hasSize(3);

    }

    @Test
    public void givenAllExistingProducts_whenExtractNewProductAuthorizations_thenAddNone() {
        Set<ProductModel> productsToAdd = stubProductModelSet(1, 3);
        Set<ProductModel> productsAuthorized = stubProductModelSet(1, 3);

        Set<ProductAuthorizationModel> result = subject.extractNewProductAuthorizations(
                new TenantModel(), productsToAdd, productsAuthorized);

        assertThat(result).hasSize(0);
    }

    @Test
    public void givenSomeNewProducts_whenExtractNewProductAuthorizations_thenAddOnlyNew() {
        Set<ProductModel> productsToAdd = stubProductModelSet(1, 4);
        Set<ProductModel> productsAuthorized = stubProductModelSet(3, 7);

        Set<ProductAuthorizationModel> result = subject.extractNewProductAuthorizations(
                new TenantModel(), productsToAdd, productsAuthorized);

        assertThat(result).hasSize(2);
    }

    @Test
    public void givenValidProducts_whenRemoveProducts_thenSuccess() {
        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new TenantModel()));
        when(productRepositoryMock.findAllById(any())).thenReturn(stubProductModelList(1, 3));
        when(converterMock.convert(any(), eq(TenantTo.class))).thenReturn(new TenantTo());

        TenantTo result = subject.removeProducts(1, Arrays.asList(1, 2, 3));

        assertNotNull(result);
        verify(productAuthorizationRepoMock, times(1)).findByTenantIdAndDeletedIsFalse(anyInt());
        verify(productAuthorizationRepoMock, times(1)).deleteAll(anyIterable());

    }

    @Test
    public void givenTenantExists_whenDeleteTenant_thenSuccess() {
        TenantModel model = new TenantModel();
        InstanceModel instanceModel = new InstanceModel();
        model.setInstances(new HashSet<>(asList(instanceModel)));

        when(tenantRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(model));

        int result = subject.deleteTenant(1);

        assertTrue(model.isDeleted());
        assertTrue(instanceModel.isDeleted());
        assertEquals(1, result);
        verify(tenantRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(tenantRepositoryMock, times(1)).save(any());
    }

}