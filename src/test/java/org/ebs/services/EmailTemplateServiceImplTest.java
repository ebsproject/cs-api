package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.EmailTemplateModel;
import org.ebs.model.repos.EmailTemplateRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.Input.EmailTemplateInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class EmailTemplateServiceImplTest {

    private @Mock EmailTemplateRepository emailTemplateRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock TenantRepository tenantRepositoryMock;
    
    private EmailTemplateServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private EmailTemplateInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private EmailTemplateModel modelStub;

    @BeforeEach
    public void init() {
        subject = new EmailTemplateServiceImpl(emailTemplateRepositoryMock, converterMock, tenantRepositoryMock);
    }

    @Test
    public void givenEmailTemplateExists_whenFindEmailTemplate_thenReturnNotEmpty() {
        when(emailTemplateRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new EmailTemplateModel()));
        when(converterMock.convert(any(), eq(EmailTemplateTo.class))).thenReturn(new EmailTemplateTo());

        Optional<EmailTemplateTo> object = subject.findEmailTemplate(1);

        assertTrue(object.isPresent());
        verify(emailTemplateRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenEmailTemplateExistsAndDeleted_whenFindEmailTemplate_thenReturnEmpty() {
        EmailTemplateModel t = new EmailTemplateModel();
        t.setDeleted(true);
        when(emailTemplateRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<EmailTemplateTo> object = subject.findEmailTemplate(1);

        assertFalse(object.isPresent());
        verify(emailTemplateRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenEmailTemplateIdLessThan1_whenFindEmailTemplate_thenReturnEmpty() {
        Optional<EmailTemplateTo> object = subject.findEmailTemplate(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenEmailTemplateListExist_whenFindEmailTemplates_thenReturnNotEmpty() {
        List<EmailTemplateModel> emailTemplateList = Arrays.asList(new EmailTemplateModel(), new EmailTemplateModel());
        when(emailTemplateRepositoryMock.findByCriteria(eq(EmailTemplateModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<EmailTemplateModel>(emailTemplateList, PageRequest.of(0, 10), 2));

        Page<EmailTemplateTo> object = subject.findEmailTemplates(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(emailTemplateList);
        verify(emailTemplateRepositoryMock, times(1)).findByCriteria(eq(EmailTemplateModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenEmailTemplateListNotExists_whenFindEmailTemplate_thenReturnEmpty() {
        when(emailTemplateRepositoryMock.findByCriteria(eq(EmailTemplateModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<EmailTemplateModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<EmailTemplateTo> object = subject.findEmailTemplates(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(emailTemplateRepositoryMock, times(1)).findByCriteria(eq(EmailTemplateModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidEmailTemplateInput_whenCreateEmailTemplate_thenPersistEmailTemplate() {
        when(converterMock.convert(any(),eq(EmailTemplateModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(EmailTemplateTo.class))).thenReturn(new EmailTemplateTo());
        when(emailTemplateRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        EmailTemplateTo result = subject.createEmailTemplate(inputStub);

        assertNotNull(result);
        verify(emailTemplateRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidEmailTemplateInput_whenModifyEmailTemplate_thenPersistEmailTemplate() {
        EmailTemplateModel model = new EmailTemplateModel();

        when(converterMock.convert(any(),eq(EmailTemplateModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(EmailTemplateTo.class))).thenReturn(new EmailTemplateTo());
        when(emailTemplateRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        EmailTemplateTo result = subject.modifyEmailTemplate(inputStub);

        assertNotNull(result);
        verify(emailTemplateRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenEmailTemplateNotExists_whenModifyEmailTemplate_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyEmailTemplate(inputStub));
        assertThat(e.getMessage()).isEqualTo("EmailTemplate not found");
    }
 
    @Test
    public void givenEmailTemplateExists_whenDeleteEmailTemplate_thenSuccess() {
        EmailTemplateModel model = new EmailTemplateModel();

        when(emailTemplateRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteEmailTemplate(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(emailTemplateRepositoryMock,times(1)).findById(anyInt());
        verify(emailTemplateRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenEmailTemplateNotExists_whenDeleteEmailTemplate_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteEmailTemplate(anyInt()));
        assertThat(e.getMessage()).isEqualTo("EmailTemplate not found");
    }
    
}
