package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.MessageModel;
import org.ebs.model.repos.MessageRepository;
import org.ebs.services.to.MessageTo;
import org.ebs.services.to.Input.MessageInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class MessageServiceImplTest {

    private @Mock MessageRepository messageRepositoryMock;
	private @Mock ConversionService converterMock;
    
    private MessageServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private MessageInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private MessageModel modelStub;

    @BeforeEach
    public void init() {
        subject = new MessageServiceImpl(messageRepositoryMock, converterMock);
    }

    @Test
    public void givenMessageExists_whenFindMessage_thenReturnNotEmpty() {
        when(messageRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new MessageModel()));
        when(converterMock.convert(any(), eq(MessageTo.class))).thenReturn(new MessageTo());

        Optional<MessageTo> object = subject.findMessage(1);

        assertTrue(object.isPresent());
        verify(messageRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenMessageExistsAndDeleted_whenFindMessage_thenReturnEmpty() {
        MessageModel t = new MessageModel();
        t.setDeleted(true);
        when(messageRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<MessageTo> object = subject.findMessage(1);

        assertFalse(object.isPresent());
        verify(messageRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenMessageIdLessThan1_whenFindMessage_thenReturnEmpty() {
        Optional<MessageTo> object = subject.findMessage(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenMessageListExist_whenFindMessages_thenReturnNotEmpty() {
        List<MessageModel> messageList = Arrays.asList(new MessageModel(), new MessageModel());
        when(messageRepositoryMock.findByCriteria(eq(MessageModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<MessageModel>(messageList, PageRequest.of(0, 10), 2));

        Page<MessageTo> object = subject.findMessages(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(messageList);
        verify(messageRepositoryMock, times(1)).findByCriteria(eq(MessageModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenMessageListNotExists_whenFindMessage_thenReturnEmpty() {
        when(messageRepositoryMock.findByCriteria(eq(MessageModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<MessageModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<MessageTo> object = subject.findMessages(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(messageRepositoryMock, times(1)).findByCriteria(eq(MessageModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidMessageInput_whenCreateMessage_thenPersistMessage() {
        when(converterMock.convert(any(),eq(MessageModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(MessageTo.class))).thenReturn(new MessageTo());
        when(messageRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        MessageTo result = subject.createMessage(inputStub);

        assertNotNull(result);
        verify(messageRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidMessageInput_whenModifyMessage_thenPersistMessage() {
        MessageModel model = new MessageModel();

        when(converterMock.convert(any(),eq(MessageModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(MessageTo.class))).thenReturn(new MessageTo());
        when(messageRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        MessageTo result = subject.modifyMessage(inputStub);

        assertNotNull(result);
        verify(messageRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenMessageNotExists_whenModifyMessage_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyMessage(inputStub));
        assertThat(e.getMessage()).isEqualTo("Message not found");
    }
 
    @Test
    public void givenMessageExists_whenDeleteMessage_thenSuccess() {
        MessageModel model = new MessageModel();

        when(messageRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteMessage(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(messageRepositoryMock,times(1)).findById(anyInt());
        verify(messageRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenMessageNotExists_whenDeleteMessage_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteMessage(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Message not found");
    }
    
}
