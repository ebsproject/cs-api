package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.HtmlTagModel;
import org.ebs.model.repos.AlertRepository;
import org.ebs.model.repos.AttributesRepository;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.MessageRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.Input.HtmlTagInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class HtmlTagServiceImplTest {

    private @Mock HtmlTagRepository htmlTagRepositoryMock;
	private @Mock ConversionService converterMock;
    private @Mock AttributesRepository attributesRepositoryMock;
    private @Mock MessageRepository messageRepositoryMock;
    private @Mock DomainRepository domainRepositoryMock;
    private @Mock AlertRepository alertRepositoryMock;
    private @Mock TranslationRepository translationRepositoryMock;
    private @Mock ProductRepository productRepositoryMock;
    
    private HtmlTagServiceImpl subject;
     @Mock(answer = RETURNS_DEEP_STUBS)
    private HtmlTagInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private HtmlTagModel modelStub;

    @BeforeEach
    public void init() {
        subject = new HtmlTagServiceImpl(htmlTagRepositoryMock, converterMock, attributesRepositoryMock, 
            messageRepositoryMock, domainRepositoryMock, alertRepositoryMock, translationRepositoryMock, productRepositoryMock);
    }

    @Test
    public void givenHtmlTagExists_whenFindHtmlTag_thenReturnNotEmpty() {
        when(htmlTagRepositoryMock.findById(anyInt()))
                .thenReturn(Optional.of(new HtmlTagModel()));
        when(converterMock.convert(any(), eq(HtmlTagTo.class))).thenReturn(new HtmlTagTo());

        Optional<HtmlTagTo> object = subject.findHtmlTag(1);

        assertTrue(object.isPresent());
        verify(htmlTagRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenHtmlTagExistsAndDeleted_whenFindHtmlTag_thenReturnEmpty() {
        HtmlTagModel t = new HtmlTagModel();
        t.setDeleted(true);
        when(htmlTagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(t));

        Optional<HtmlTagTo> object = subject.findHtmlTag(1);

        assertFalse(object.isPresent());
        verify(htmlTagRepositoryMock, times(1)).findById(anyInt());
    }

    @Test
    public void givenHtmlTagIdLessThan1_whenFindHtmlTag_thenReturnEmpty() {
        Optional<HtmlTagTo> object = subject.findHtmlTag(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenHtmlTagListExist_whenFindHtmlTags_thenReturnNotEmpty() {
        List<HtmlTagModel> htmlTagList = Arrays.asList(new HtmlTagModel(), new HtmlTagModel());
        when(htmlTagRepositoryMock.findByCriteria(eq(HtmlTagModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<HtmlTagModel>(htmlTagList, PageRequest.of(0, 10), 2));

        Page<HtmlTagTo> object = subject.findHtmlTags(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(htmlTagList);
        verify(htmlTagRepositoryMock, times(1)).findByCriteria(eq(HtmlTagModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenHtmlTagListNotExists_whenFindHtmlTag_thenReturnEmpty() {
        when(htmlTagRepositoryMock.findByCriteria(eq(HtmlTagModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<HtmlTagModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<HtmlTagTo> object = subject.findHtmlTags(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(htmlTagRepositoryMock, times(1)).findByCriteria(eq(HtmlTagModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidHtmlTagInput_whenCreateHtmlTag_thenPersistHtmlTag() {
        when(converterMock.convert(any(),eq(HtmlTagModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(HtmlTagTo.class))).thenReturn(new HtmlTagTo());
        when(htmlTagRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);

        HtmlTagTo result = subject.createHtmlTag(inputStub);

        assertNotNull(result);
        verify(htmlTagRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenValidHtmlTagInput_whenModifyHtmlTag_thenPersistHtmlTag() {
        HtmlTagModel model = new HtmlTagModel();

        when(converterMock.convert(any(),eq(HtmlTagModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(HtmlTagTo.class))).thenReturn(new HtmlTagTo());
        when(htmlTagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        HtmlTagTo result = subject.modifyHtmlTag(inputStub);

        assertNotNull(result);
        verify(htmlTagRepositoryMock, times(1)).save(any());
        verify(converterMock, times(2)).convert(any(), any());
    }

    @Test
    public void givenHtmlTagNotExists_whenModifyHtmlTag_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyHtmlTag(inputStub));
        assertThat(e.getMessage()).isEqualTo("HtmlTag not found");
    }
 
    @Test
    public void givenHtmlTagExists_whenDeleteHtmlTag_thenSuccess() {
        HtmlTagModel model = new HtmlTagModel();

        when(htmlTagRepositoryMock.findById(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteHtmlTag(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(htmlTagRepositoryMock,times(1)).findById(anyInt());
        verify(htmlTagRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenHtmlTagNotExists_whenDeleteHtmlTag_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteHtmlTag(anyInt()));
        assertThat(e.getMessage()).isEqualTo("HtmlTag not found");
    }
    
}
