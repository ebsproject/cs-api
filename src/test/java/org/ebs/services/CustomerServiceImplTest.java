package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.model.AddressModel;
import org.ebs.model.CustomerModel;
import org.ebs.model.OrganizationModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.CustomerRepository;
import org.ebs.model.repos.OrganizationRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.Input.AddressInput;
import org.ebs.services.to.Input.CustomerInput;
import org.ebs.util.Connection;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTest {

    private @Mock CustomerRepository customerRepositoryMock;
	private @Mock ConversionService converterMock;
	private @Mock OrganizationRepository organizationRepositoryMock;
    private @Mock TenantRepository tenantRepositoryMock;
    private @Mock AddressRepository addressRepositoryMock;
    private @Mock UserRepository userRepositoryMock;
    
    private CustomerServiceImpl subject;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private CustomerInput inputStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private CustomerModel modelStub;
    @Mock(answer = RETURNS_DEEP_STUBS)
    private AddressInput addressInputStub;

    @BeforeEach
    public void init() {
        subject = new CustomerServiceImpl(customerRepositoryMock, converterMock, organizationRepositoryMock, tenantRepositoryMock, addressRepositoryMock, userRepositoryMock);
    }

    @Test
    public void givenCustomerExists_whenFindCustomer_thenReturnNotEmpty() {
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt()))
                .thenReturn(Optional.of(new CustomerModel()));
        when(converterMock.convert(any(), eq(CustomerTo.class))).thenReturn(new CustomerTo());

        Optional<CustomerTo> object = subject.findCustomer(1);

        assertTrue(object.isPresent());
        verify(customerRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenCustomerExistsAndDeleted_whenFindCustomer_thenReturnEmpty() {
        CustomerModel t = new CustomerModel();
        t.setDeleted(true);
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(t));

        Optional<CustomerTo> object = subject.findCustomer(1);

        assertFalse(object.isPresent());
        verify(customerRepositoryMock, times(1)).findByIdAndDeletedIsFalse(anyInt());
    }

    @Test
    public void givenCustomerIdLessThan1_whenFindCustomer_thenReturnEmpty() {
        Optional<CustomerTo> object = subject.findCustomer(0);
        assertFalse(object.isPresent());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenCustomerListExist_whenFindCustomers_thenReturnNotEmpty() {
        List<CustomerModel> customerList = Arrays.asList(new CustomerModel(), new CustomerModel());
        when(customerRepositoryMock.findByCriteria(eq(CustomerModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<CustomerModel>(customerList, PageRequest.of(0, 10), 2));

        Page<CustomerTo> object = subject.findCustomers(null, null, null, false);

        assertThat(object.getContent()).hasSameSizeAs(customerList);
        verify(customerRepositoryMock, times(1)).findByCriteria(eq(CustomerModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void givenCustomerListNotExists_whenFindCustomer_thenReturnEmpty() {
        when(customerRepositoryMock.findByCriteria(eq(CustomerModel.class), isNull(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean())).thenReturn(
                        new Connection<CustomerModel>(emptyList(), PageRequest.of(0, 10), 0));

        Page<CustomerTo> object = subject.findCustomers(null, null, null, false);

        assertThat(object.getContent()).isEmpty();
        verify(customerRepositoryMock, times(1)).findByCriteria(eq(CustomerModel.class), any(),
                (List<SortInput>) isNull(), (PageInput) isNull(), anyBoolean());
    }

    @Test
    public void givenValidCustomerInput_whenCreateCustomer_thenPersistCustomer() {
        when(converterMock.convert(any(),eq(CustomerModel.class))).thenReturn(modelStub);
        when(converterMock.convert(any(),eq(CustomerTo.class))).thenReturn(new CustomerTo());
        when(converterMock.convert(any(),eq(AddressModel.class))).thenReturn(new AddressModel());
        when(customerRepositoryMock.save(eq(modelStub))).thenReturn(modelStub);
        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new OrganizationModel()));
        when(addressRepositoryMock.findById(anyInt())).thenReturn(Optional.of(new AddressModel()));
        when(inputStub.getAddress()).thenReturn(addressInputStub);
        CustomerTo result = subject.createCustomer(inputStub);

        assertNotNull(result);
        verify(customerRepositoryMock, times(1)).save(any());
        verify(converterMock, times(3)).convert(any(), any());
    }

    @Test
    public void givenValidCustomerInput_whenModifyCustomer_thenPersistCustomer() {
        CustomerModel model = new CustomerModel();

        when(converterMock.convert(any(),eq(CustomerModel.class))).thenReturn(model);
        when(converterMock.convert(any(),eq(CustomerTo.class))).thenReturn(new CustomerTo());
        when(converterMock.convert(any(),eq(AddressModel.class))).thenReturn(new AddressModel());
        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));
        when(organizationRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(new OrganizationModel()));
        CustomerTo result = subject.modifyCustomer(inputStub);

        assertNotNull(result);
        verify(customerRepositoryMock, times(1)).save(any());
        verify(converterMock, times(3)).convert(any(), any());
    }

    @Test
    public void givenCustomerNotExists_whenModifyCustomer_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.modifyCustomer(inputStub));
        assertThat(e.getMessage()).isEqualTo("Customer not found");
    }
 
    @Test
    public void givenCustomerExists_whenDeleteCustomer_thenSuccess() {
        CustomerModel model = new CustomerModel();

        when(customerRepositoryMock.findByIdAndDeletedIsFalse(anyInt())).thenReturn(Optional.of(model));

        int result = subject.deleteCustomer(1);

        assertTrue(model.isDeleted());
        assertEquals(1, result);
        verify(customerRepositoryMock,times(1)).findByIdAndDeletedIsFalse(anyInt());
        verify(customerRepositoryMock,times(1)).save(any());
    }

    @Test
    public void givenCustomerNotExists_whenDeleteCustomer_thenFail() {
        Exception e = assertThrows(RuntimeException.class, () -> subject.deleteCustomer(anyInt()));
        assertThat(e.getMessage()).isEqualTo("Customer not found");
    }
    
}
