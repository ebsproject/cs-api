function extract(value, extractor) {
    var json = JSON.parse(value);
    var tokens= extractor.split('.');
    return get(json,tokens)
}

function extractIndex(str) {
  var regex = /\[(\d+)\]/;
  var match = regex.exec(str);
  return match ? parseInt(match[1]) : null;
}

function extractPrefix(str) {
  var bracketIndex = str.indexOf('[');
  if (bracketIndex !== -1) {
      return str.substring(0, bracketIndex);
  }
  return str;
}

function get(value, extractor){
  if(extractor.length === 1) {
    return  value[extractor[0]]
  } else {
    var token = extractor.shift()
    if(token.indexOf('[') !== -1){
      return get(value[extractPrefix(token)][extractIndex(token)], extractor)
    }else{
      return get(value[token], extractor)
    }
  }
}

function isSkipped(input, expression) {
    if (!expression)
        return false;
    else
        return eval(expression.replace('$input', "'" + input + "'"))
}