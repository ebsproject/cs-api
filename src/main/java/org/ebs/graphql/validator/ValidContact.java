package org.ebs.graphql.validator;

 

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.ebs.services.to.Input.ContactInput;

import lombok.extern.log4j.Log4j2;

@Documented
@Constraint(validatedBy = ContactInputValidator.class)
@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidContact {
    String message()

    default "Invalid Contact input";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

@Log4j2
class ContactInputValidator implements ConstraintValidator<ValidContact, ContactInput> {

    private static final String MISSING_PERSON_DATA = "Missing person data";
    private static final String MISSING_INSTITUTION_DATA = "Missing institution data";

    @Override
    public boolean isValid(ContactInput value, ConstraintValidatorContext context) {
        return checkAttributesRequiredByCategory(value, context);
    }

    /**
     * If a contact's category is Institution, the attribute 'institution' must be
     * present, when is Person, 'person' attribute must be present
     *
     * @param value
     * @param context
     * @return
     * TODO: Review it
     */
    boolean checkAttributesRequiredByCategory(ContactInput value, ConstraintValidatorContext context) {
        boolean valid = true;
/*
        if (value.getCategory()("Person") && value.getPerson() == null) {
            valid = false;
            context.buildConstraintViolationWithTemplate(MISSING_PERSON_DATA).addConstraintViolation();
            log.debug(MISSING_PERSON_DATA);
        } else if (value.getCategory().equals("Institution") && value.getInstitution() == null) {
            valid = false;
            context.buildConstraintViolationWithTemplate(MISSING_INSTITUTION_DATA).addConstraintViolation();
            log.debug(MISSING_INSTITUTION_DATA);

        }
*/
        return valid;
    }

}
