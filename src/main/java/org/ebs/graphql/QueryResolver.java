package org.ebs.graphql;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.Min;

import org.ebs.services.AddressService;
import org.ebs.services.CFTypeService;
import org.ebs.services.CFValueService;
import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactInfoTypeService;
import org.ebs.services.ContactService;
import org.ebs.services.ContactTypeService;
import org.ebs.services.CountryService;
import org.ebs.services.CropService;
import org.ebs.services.CustomerService;
import org.ebs.services.DelegationService;
import org.ebs.services.DomainService;
import org.ebs.services.EmailTemplateService;
import org.ebs.services.EntityReferenceService;
import org.ebs.services.EventService;
import org.ebs.services.FileTypeService;
import org.ebs.services.FilterService;
import org.ebs.services.FunctionalUnitService;
import org.ebs.services.HierarchyService;
import org.ebs.services.InstanceService;
import org.ebs.services.InstitutionService;
import org.ebs.services.IwinOccurrenceService;
import org.ebs.services.JobLogService;
import org.ebs.services.JobWorkflowService;
import org.ebs.services.NodeCFService;
import org.ebs.services.NodeService;
import org.ebs.services.NodeTypeService;
import org.ebs.services.OccurrenceShipmentService;
import org.ebs.services.OrganizationService;
import org.ebs.services.OrganizationalUnitService;
import org.ebs.services.PhaseService;
import org.ebs.services.PrintoutTemplateService;
import org.ebs.services.ProcessService;
import org.ebs.services.ProductFunctionService;
import org.ebs.services.ProductService;
import org.ebs.services.ProgramService;
import org.ebs.services.PurposeService;
import org.ebs.services.RoleService;
import org.ebs.services.SecurityRuleService;
import org.ebs.services.ServiceFileService;
import org.ebs.services.ServiceItemService;
import org.ebs.services.ServiceService;
import org.ebs.services.ShipmentFileService;
import org.ebs.services.ShipmentItemService;
import org.ebs.services.ShipmentService;
import org.ebs.services.StageService;
import org.ebs.services.StatusService;
import org.ebs.services.StatusTypeService;
import org.ebs.services.SynchronizeProgramService;
import org.ebs.services.SynchronizeUserService;
import org.ebs.services.TenantService;
import org.ebs.services.UserService;
import org.ebs.services.WorkflowInstanceService;
import org.ebs.services.WorkflowService;
import org.ebs.services.WorkflowViewTypeService;
import org.ebs.services.to.Address;
import org.ebs.services.to.CFTypeTo;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactInfo;
import org.ebs.services.to.ContactInfoType;
import org.ebs.services.to.ContactType;
// import org.ebs.services.to.CoreHierarchyTo;
import org.ebs.services.to.Country;
import org.ebs.services.to.CropTo;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.DelegationTo;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.EntityReferenceTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.FilterTo;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.IWINOccurrenceTo;
import org.ebs.services.to.InstanceTo;
import org.ebs.services.to.Institution;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.NodeTypeTo;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.OrganizationSync;
import org.ebs.services.to.OrganizationTo;
import org.ebs.services.to.OrganizationalUnit;
import org.ebs.services.to.PersonSync;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.PrintoutTemplate;
import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.ProgramSync;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.RoleTo;
import org.ebs.services.to.SecurityRuleTo;
import org.ebs.services.to.SelectReference;
import org.ebs.services.to.ServiceFileTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.ShipmentFileTo;
import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.StatusTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.JsonNode;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {

    private final InstanceService instanceService;
    private final UserService userService;
    private final ContactService contactService;
    private final ProductService productService;
    private final TenantService tenantService;
    private final EntityReferenceService entityReferenceService;
    private final DomainService domainService;
    private final CropService cropService;
    private final ProgramService programService;
    private final RoleService roleService;
    private final SecurityRuleService securityRuleService;
    private final PrintoutTemplateService printoutTemplateService;
    private final CountryService countryService;
    private final ContactTypeService contactTypeService;
    private final CustomerService customerService;
    private final OrganizationService organizationService;
    private final PurposeService purposeService;
    private final HierarchyService hierarchyService;
    private final ContactInfoTypeService contactInfoTypeService;
    private final ContactInfoService contactInfoService;
    private final DelegationService delegationService;
    private final ProductFunctionService productFunctionService;
    private final FunctionalUnitService functionalUnitService;
    private final FilterService filterService;
    private final JobWorkflowService jobWorkflowService;
    private final JobLogService jobLogService;
    private final WorkflowInstanceService workflowInstanceService;
    private final EventService eventService;
    private final StageService stageService;
    private final ShipmentService shipmentService;
    private final ShipmentItemService shipmentItemService;
    private final ServiceItemService serviceItemService;
    private final ShipmentFileService shipmentFileService;
    private final ServiceFileService fileService;
    private final EmailTemplateService emailTemplateService;
    private final CFTypeService cfTypeService;
    private final WorkflowViewTypeService workflowViewTypeService;
    private final NodeService nodeService;
    private final WorkflowService workflowService;
    private final NodeCFService nodeCFService;
    private final NodeTypeService nodeTypeService;
    private final CFValueService cfValueService;
    private final StatusTypeService statusTypeService;
    private final StatusService statusService;
    private final PhaseService phaseService;
    private final ServiceService serviceService;
    private final ProcessService processService;
    private final OrganizationalUnitService organizationalUnitService;
    private final FileTypeService fileTypeService;
    private final AddressService addressService;
    private final InstitutionService institutionService;
    private final SynchronizeUserService synchronizeUserService;
    private final SynchronizeProgramService synchronizeProgramService;
    private final OccurrenceShipmentService occurrenceShipmentService;
    private final IwinOccurrenceService iwinOccurrenceService;

    public Optional<CropTo> findCrop(int cropId) {
        return this.cropService.findCrop(cropId);
    }

    public Page<CropTo> findCropList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.cropService.findCrops(page, sort, filters, disjunctionFilters);
    }

    //@PreAuthorize("hasAuthority('Tenant Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<DomainTo> findDomain(int domainId) {
        return domainService.findDomain(domainId);
    }

    //@PreAuthorize("hasAuthority('Tenant Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<DomainTo> findDomainList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.domainService.findDomains(page, sort, filters, disjunctionFilters);
    }

    public Optional<EntityReferenceTo> findEntityReference(int entityReferenceId) {
        return this.entityReferenceService.findEntityReference(entityReferenceId);
    }

    public Page<EntityReferenceTo> findEntityReferenceList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.entityReferenceService.findEntityReferences(page, sort, filters,
                disjunctionFilters);
    }

    //@PreAuthorize("hasAuthority('Tenant Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<InstanceTo> findInstance(int instanceId) {
        return this.instanceService.findInstance(instanceId);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<InstanceTo> findInstanceList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.instanceService.findInstances(page, sort, filters, disjunctionFilters);
    }

    public Optional<ProductTo> findProduct(@Min(1)
    int productId) {
        return this.productService.findProduct(productId);
    }

    public Page<ProductTo> findProductList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.productService.findProducts(page, sort, filters, disjunctionFilters);
    }

    public Optional<ProgramTo> findProgram(int programId) {
        return this.programService.findProgram(programId);
    }

    public Page<ProgramTo> findProgramList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.programService.findPrograms(page, sort, filters, disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<RoleTo> findRole(int roleId) {
        return roleService.findRole(roleId);
    }

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<RoleTo> findRoleList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return roleService.findRoles(page, sort, filters, disjunctionFilters);
    }
    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<SecurityRuleTo> findSecurityRule(int ruleId) {
        return securityRuleService.findSecurityRule(ruleId);
    }
    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<SecurityRuleTo> findSecurityRuleList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return securityRuleService.findSecurityRules(page, sort, filters, disjunctionFilters);
    }

    public Optional<TenantTo> findTenant(int tenantId) {
        return this.tenantService.findTenant(tenantId);
    }

    public Page<TenantTo> findTenantList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.tenantService.findTenants(page, sort, filters, disjunctionFilters);
    }

    //@PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<UserTo> findUser(int userId) {
        return userService.findUser(userId);
    }

    //@PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<UserTo> findUserList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return userService.findUsers(page, sort, filters, disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Contact findContact(int contactId) {
        return contactService.findById(contactId);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Page<Contact> findContactList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return contactService.findContacts(page, sort, filters, disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('Printout_Read') or hasRole('ROLE_ADMIN')")
    public Optional<PrintoutTemplate> findPrintoutTemplate(@Min(1)
    int printoutTemplateId) {
        return printoutTemplateService.findPrintoutTemplate(printoutTemplateId);
    }

    @PreAuthorize("hasAuthority('Printout_Read') or hasRole('ROLE_ADMIN')")
    public Page<PrintoutTemplate> findPrintoutTemplateList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return printoutTemplateService.findPrintoutTemplates(page, sort, filters,
                disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Country findCountry(@Min(1)
    int countryId) {
        return countryService.findById(countryId);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Page<Country> findCountryList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return countryService.findCountrys(page, sort, filters, disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public ContactType findContactType(@Min(1)
    int contactTypeId) {
        return contactTypeService.findById(contactTypeId);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Page<ContactType> findContactTypeList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return contactTypeService.findContactTypes(page, sort, filters, disjunctionFilters);
    }

    public Optional<CustomerTo> findCustomer(int customerId) {
        return this.customerService.findCustomer(customerId);
    }

    public Page<CustomerTo> findCustomerList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.customerService.findCustomers(page, sort, filters, disjunctionFilters);
    }

    public Optional<OrganizationTo> findOrganization(int organizationId) {
        return this.organizationService.findOrganization(organizationId);
    }

    public Page<OrganizationTo> findOrganizationList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.organizationService.findOrganizations(page, sort, filters, disjunctionFilters);
    }

    /**
	 * 
	 * @param purposeId
	 */
    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
	public Optional<PurposeTo> findPurpose(int purposeId){
		return this.purposeService.findPurpose(purposeId);
	}

	/**
	 * 
	 * @param page
	 * @param sort
	 * @param filters
	 */
    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
	public Page<PurposeTo> findPurposeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
		return this.purposeService.findPurposes(page,sort,filters,disjunctionFilters);
	}

    public Optional<HierarchyTo> findHierarchy(int id) {
        return this.hierarchyService.findHierarchy(id);
    }

    public Page<HierarchyTo> findHierarchyList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
		return this.hierarchyService.findHierarchyList(page,sort,filters,disjunctionFilters);
	}

    	/**
	 * 
	 * @param institutionId	 
	 */
    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
	public List<Contact> findContactsByInstitution(int institutionId){
		return null;
	}

    
    	/**
	 * 
	 * @param contactId	 
	 */
    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
	public List<Institution> findInstitutionsByContact(int contactId){
		return this.institutionService.findInstitutionsByContact(contactId);
	}

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Optional<ContactInfoType> findContactInfoType(int id) {
        return this.contactInfoTypeService.findContactInfoType(id);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Page<ContactInfoType> findContactInfoTypeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
		return this.contactInfoTypeService.findContactInfoTypes(page,sort,filters,disjunctionFilters);
	}

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Optional<ContactInfo> findContactInfo(int id) {
        return this.contactInfoService.findContactInfo(id);
    }

    @PreAuthorize("hasAuthority('CRM_Read') or hasRole('ROLE_ADMIN')")
    public Page<ContactInfo> findContactInfoList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
		return this.contactInfoService.findContactInfos(page,sort,filters,disjunctionFilters);
	}

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<DelegationTo> findDelegation(int id) {
        return this.delegationService.findDelegation(id);
    }

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<DelegationTo> findDelegationList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.delegationService.findDelegations(page, sort, filters, disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<ProductFunctionTo> findProductFunction(int id) {
        return this.productFunctionService.findProductFunction(id);
    }

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<ProductFunctionTo> findProductFunctionList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.productFunctionService.findProductFunctions(page, sort, filters, disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Optional<FunctionalUnitTo> findFunctionalUnit(int id) {
        return this.functionalUnitService.findFunctionalUnit(id);
    }

    @PreAuthorize("hasAuthority('User Management_Read') or hasRole('ROLE_ADMIN')")
    public Page<FunctionalUnitTo> findFunctionalUnitList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.functionalUnitService.findFunctionalUnitList(page, sort, filters, disjunctionFilters);
    }

    @PreAuthorize("hasAuthority('Hierarchy_Read') or hasRole('ROLE_ADMIN')")
    public List<SelectReference> findSelectReference(String schema,String table,String fieldValue,String fieldText){
        return this.entityReferenceService.findReference(schema, table, fieldValue, fieldText);
    }


    public Optional<FilterTo> findFilter(int id) {
        return this.filterService.findFilter(id);
    }

    public Page<FilterTo> findFilterList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.filterService.findFilters(page, sort, filters, disjunctionFilters);
    }

    public Optional<JobWorkflowTo> findJobWorkflow(int id) {
        return this.jobWorkflowService.findJobWorkflow(id);
    }

    public Page<JobWorkflowTo> findJobWorkflowList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.jobWorkflowService.findJobWorkflows(page, sort, filters, disjunctionFilters);
    }

    public Optional<JobLogTo> findJobLog(int id) {
        return this.jobLogService.findJobLog(id);
    }

    public Page<JobLogTo> findJobLogList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.jobLogService.findJobLogs(page, sort, filters, disjunctionFilters);
    }

    public Optional<WorkflowInstanceTo> findWorkflowInstance(int id) {
        return this.workflowInstanceService.findWorkflowInstance(id);
    }

    public Page<WorkflowInstanceTo> findWorkflowInstanceList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.workflowInstanceService.findWorkflowInstances(page, sort, filters, disjunctionFilters);
    }

    public Optional<EventTo> findEvent(int id) {
        return this.eventService.findEvent(id);
    }

    public Page<EventTo> findEventList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.eventService.findEvents(page, sort, filters, disjunctionFilters);
    }

    public Optional<StageTo> findStage(int id) {
        return this.stageService.findStage(id);
    }

    public Page<StageTo> findStageList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.stageService.findStages(page, sort, filters, disjunctionFilters);
    }

    public Optional<ShipmentTo> findShipment(int id) {
        return this.shipmentService.findShipment(id);
    }

    public Page<ShipmentTo> findShipmentList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.shipmentService.findShipments(page, sort, filters, disjunctionFilters);
    }

    public Optional<ShipmentItemTo> findShipmentItem(int id) {
        return this.shipmentItemService.findShipmentItem(id);
    }

    public Page<ShipmentItemTo> findShipmentItemList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.shipmentItemService.findShipmentItems(page, sort, filters, disjunctionFilters);
    }

    public Optional<ServiceItemTo> findServiceItem(int id) {
        return this.serviceItemService.findServiceItem(id);
    }

    public Page<ServiceItemTo> findServiceItemList(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return this.serviceItemService.findServiceItems(page, sort, filters, disjunctionFilters);
    }

    public Optional<ShipmentFileTo> findShipmentFile(int id) {
        return this.shipmentFileService.findShipmentFile(id);
    }

    public Page<ShipmentFileTo> findShipmentFileList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.shipmentFileService.findShipmentFiles(page, sort, filters, disjunctionFilters);
    }

    public Optional<ServiceFileTo> findServiceFile(int id) {
        return this.fileService.findServiceFile(id);
    }

    public Page<ServiceFileTo> findServiceFileList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.fileService.findServiceFiles(page, sort, filters, disjunctionFilters);
    }

    public Optional<EmailTemplateTo> findEmailTemplate(int id) {
        return this.emailTemplateService.findEmailTemplate(id);
    }

    public Page<EmailTemplateTo> findEmailTemplateList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.emailTemplateService.findEmailTemplates(page, sort, filters, disjunctionFilters);
    }

    public Optional<CFTypeTo> findCFType(int id) {
        return this.cfTypeService.findCFType(id);
    }

    public Page<CFTypeTo> findCFTypeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.cfTypeService.findCFTypes(page, sort, filters, disjunctionFilters);
    }

    public Optional<WorkflowViewTypeTo> findWorkflowViewType(int id) {
        return this.workflowViewTypeService.findWorkflowViewType(id);
    }

    public Page<WorkflowViewTypeTo> findWorkflowViewTypeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.workflowViewTypeService.findWorkflowViewTypes(page, sort, filters, disjunctionFilters);
    }

    public Optional<NodeTypeTo> findNodeType(int id) {
        return this.nodeTypeService.findNodeType(id);
    }

    public Page<NodeTypeTo> findNodeTypeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.nodeTypeService.findNodeTypes(page, sort, filters, disjunctionFilters);
    }


    public Optional<NodeTo> findNode(int id) {
        return this.nodeService.findNode(id);
    }

    public Page<NodeTo> findNodeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.nodeService.findNodes(page, sort, filters, disjunctionFilters);
    }

    public Optional<WorkflowTo> findWorkflow(int id) {
        return this.workflowService.findWorkflow(id);
    }

    public Page<WorkflowTo> findWorkflowList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.workflowService.findWorkflows(page, sort, filters, disjunctionFilters);
    }

    public Page<JsonNode> findWorkflowCustomFieldList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters) {
        return this.workflowService.getWorkflowCustomFieldList(filters, sort, page, disjunctionFilters);
    }

    public Optional<NodeCFTo> findNodeCF(int id) {
        return this.nodeCFService.findNodeCF(id);
    }

    public Page<NodeCFTo> findNodeCFList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.nodeCFService.findNodeCFs(page, sort, filters, disjunctionFilters);
    }

    public Optional<CFValueTo> findCFValue(int id) {
        return this.cfValueService.findCFValue(id);
    }

    public Page<CFValueTo> findCFValueList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.cfValueService.findCFValues(page, sort, filters, disjunctionFilters);
    }

    public Optional<StatusTypeTo> findStatusType(int id) {
        return this.statusTypeService.findStatusType(id);
    }

    public Page<StatusTypeTo> findStatusTypeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.statusTypeService.findStatusTypes(page, sort, filters, disjunctionFilters);
    }

    public Optional<StatusTo> findStatus(int id) {
        return this.statusService.findStatus(id);
    }

    public Page<StatusTo> findStatusList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.statusService.findStatuses(page, sort, filters, disjunctionFilters);
    }

    public Optional<PhaseTo> findPhase(int id) {
        return this.phaseService.findPhase(id);
    }

    public Page<PhaseTo> findPhaseList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.phaseService.findPhases(page, sort, filters, disjunctionFilters);
    }

    public Optional<ServiceTo> findService(int id) {
        return this.serviceService.findService(id);
    }

    public Page<ServiceTo> findServiceList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.serviceService.findServices(page, sort, filters, disjunctionFilters);
    }

    public Optional<ProcessTo> findProcess(int id) {
        return this.processService.findProcess(id);
    }

    public Page<ProcessTo> findProcessList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.processService.findProcesses(page, sort, filters, disjunctionFilters);
    }

    public OrganizationalUnit findOrganizationalUnit(int id) {
        //return organizationalUnitService.findOrganizationalUnit(id);
        return null;
    }

    public Page<OrganizationalUnit> findOrganizationalUnitList(List<FilterInput> filters, List<SortInput> sort, PageInput page, boolean disjunctionFilters) {
        //return organizationalUnitService.findOrganizationalUnitList(filters, sort, page, disjunctionFilters);
        return null;
    }

    public Optional<FileTypeTo> findFileType(int id) {
        return this.fileTypeService.findFileType(id);
    }

    public Page<FileTypeTo> findFileTypeList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.fileTypeService.findFileTypes(page, sort, filters, disjunctionFilters);
    }

    public Optional<Address> findAddress(int id) {
        return this.addressService.findAddress(id);
    }

    public Page<Address> findAddressList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.addressService.findAddresses(page, sort, filters, disjunctionFilters);
    }

    public Institution findInstitution(int id) {
        return this.institutionService.findById(id);
    }

    public Page<Institution> findInstitutionList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.institutionService.findInstitutions(page, sort, filters, disjunctionFilters);
    }

    public List<PersonSync> findPersonSyncList() {
        return this.synchronizeUserService.findPersonSyncList();
    }

    public List<OrganizationSync> findOrganizationSyncList() {
        return this.synchronizeProgramService.findOrganizationSyncList();
    }

    public List<ProgramSync> findProgramSyncList() {
        return this.synchronizeProgramService.findProgramSyncList();
    }

    public OccurrenceShipmentTo findOccurrenceShipment(int id) {
        return this.occurrenceShipmentService.findOccurrenceShipment(id);
    }

    public Page<OccurrenceShipmentTo> findOccurrenceShipmentList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.occurrenceShipmentService.findOccurrenceShipments(page, sort, filters, disjunctionFilters);
    }

    public Page<IWINOccurrenceTo> findIwinOccurrenceByCoopkeyList(PageInput page, List<SortInput> sort,
    List<FilterInput> filters, boolean disjunctionFilters){
        return this.iwinOccurrenceService.getIwinOccurrenceByCoopkey(page, sort, filters, disjunctionFilters);
    }

}