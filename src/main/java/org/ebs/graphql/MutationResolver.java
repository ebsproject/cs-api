package org.ebs.graphql;

import java.util.List;
import java.util.Set;

import org.ebs.graphql.validator.ValidContact;
import org.ebs.services.AddressService;
import org.ebs.services.CFTypeService;
import org.ebs.services.CFValueService;
import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactInfoTypeService;
import org.ebs.services.ContactService;
import org.ebs.services.ContactWorkflowService;
import org.ebs.services.CustomerService;
import org.ebs.services.DelegationService;
import org.ebs.services.DomainInstanceService;
import org.ebs.services.EmailTemplateService;
import org.ebs.services.EventService;
import org.ebs.services.FileObjectService;
import org.ebs.services.FileTypeService;
import org.ebs.services.FilterService;
import org.ebs.services.FunctionalUnitService;
import org.ebs.services.HierarchyService;
import org.ebs.services.InstanceService;
import org.ebs.services.JobLogService;
import org.ebs.services.JobWorkflowService;
import org.ebs.services.NodeCFService;
import org.ebs.services.NodeService;
import org.ebs.services.NodeTypeService;
import org.ebs.services.OccurrenceShipmentService;
import org.ebs.services.OrganizationService;
import org.ebs.services.OrganizationalUnitService;
import org.ebs.services.PhaseService;
import org.ebs.services.PrintoutTemplateService;
import org.ebs.services.ProcessService;
import org.ebs.services.ProductFunctionService;
import org.ebs.services.ProductService;
import org.ebs.services.ProgramService;
import org.ebs.services.PurposeService;
import org.ebs.services.RoleContactHierarchyService;
import org.ebs.services.RoleProductService;
import org.ebs.services.RoleService;
import org.ebs.services.SecurityRuleRoleService;
import org.ebs.services.SecurityRuleService;
import org.ebs.services.ServiceFileService;
import org.ebs.services.ServiceItemService;
import org.ebs.services.ServiceService;
import org.ebs.services.ShipmentFileService;
import org.ebs.services.ShipmentItemService;
import org.ebs.services.ShipmentService;
import org.ebs.services.StageService;
import org.ebs.services.StatusService;
import org.ebs.services.StatusTypeService;
import org.ebs.services.TenantService;
import org.ebs.services.UserService;
import org.ebs.services.WorkflowInstanceService;
import org.ebs.services.WorkflowService;
import org.ebs.services.WorkflowViewTypeService;
import org.ebs.services.to.CFTypeTo;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactInfo;
import org.ebs.services.to.ContactInfoType;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.DelegationTo;
import org.ebs.services.to.DomainInstanceTo;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.FilterTo;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.InstanceTo;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.NodeTypeTo;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.OrganizationTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.PrintoutTemplate;
import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.RoleProductTo;
import org.ebs.services.to.RoleTo;
import org.ebs.services.to.SecurityRuleRoleTo;
import org.ebs.services.to.SecurityRuleTo;
import org.ebs.services.to.ServiceFileTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.ShipmentFileTo;
import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.StatusTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.Input.CFTypeInput;
import org.ebs.services.to.Input.CFValueInput;
import org.ebs.services.to.Input.ContactInfoInput;
import org.ebs.services.to.Input.ContactInfoTypeInput;
import org.ebs.services.to.Input.ContactInput;
import org.ebs.services.to.Input.ContactWorkflowInput;
import org.ebs.services.to.Input.CustomerInput;
import org.ebs.services.to.Input.DelegationInput;
import org.ebs.services.to.Input.DomainInstanceInput;
import org.ebs.services.to.Input.EmailTemplateInput;
import org.ebs.services.to.Input.EventInput;
import org.ebs.services.to.Input.FileObjectInput;
import org.ebs.services.to.Input.FileTypeInput;
import org.ebs.services.to.Input.FilterEntityInput;
import org.ebs.services.to.Input.FunctionalUnitInput;
import org.ebs.services.to.Input.HierarchyInput;
import org.ebs.services.to.Input.InstanceInput;
import org.ebs.services.to.Input.JobLogInput;
import org.ebs.services.to.Input.JobWorkflowInput;
import org.ebs.services.to.Input.NodeCFInput;
import org.ebs.services.to.Input.NodeInput;
import org.ebs.services.to.Input.NodeTypeInput;
import org.ebs.services.to.Input.OccurrenceShipmentInput;
import org.ebs.services.to.Input.OrganizationInput;
import org.ebs.services.to.Input.OrganizationalUnitInput;
import org.ebs.services.to.Input.PhaseInput;
import org.ebs.services.to.Input.PrintoutTemplateInput;
import org.ebs.services.to.Input.ProcessInput;
import org.ebs.services.to.Input.ProductFunctionInput;
import org.ebs.services.to.Input.ProductInput;
import org.ebs.services.to.Input.ProgramInput;
import org.ebs.services.to.Input.PurposeInput;
import org.ebs.services.to.Input.RoleInput;
import org.ebs.services.to.Input.RoleProductInput;
import org.ebs.services.to.Input.SecurityRuleInput;
import org.ebs.services.to.Input.SecurityRuleRoleInput;
import org.ebs.services.to.Input.ServItemInput;
import org.ebs.services.to.Input.ServiceFileInput;
import org.ebs.services.to.Input.ServiceInput;
import org.ebs.services.to.Input.ServiceItemInput;
import org.ebs.services.to.Input.ShipItemInput;
import org.ebs.services.to.Input.ShipmentFileInput;
import org.ebs.services.to.Input.ShipmentInput;
import org.ebs.services.to.Input.ShipmentItemInput;
import org.ebs.services.to.Input.StageInput;
import org.ebs.services.to.Input.StatusInput;
import org.ebs.services.to.Input.StatusTypeInput;
import org.ebs.services.to.Input.TenantInput;
import org.ebs.services.to.Input.UserInput;
import org.ebs.services.to.Input.WorkflowInput;
import org.ebs.services.to.Input.WorkflowInstanceInput;
import org.ebs.services.to.Input.WorkflowViewTypeInput;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Sinks.Many;

@Component
@Validated
@RequiredArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {

    private final ContactService contactService;
    private final PrintoutTemplateService printoutTemplateService;
    private final CustomerService customerService;
    private final DomainInstanceService domainInstanceService;
    private final PurposeService purposeService;
    private final HierarchyService hierarchyService;
    private final OrganizationService organizationService;
    private final ContactInfoTypeService contactInfoTypeService;
    private final RoleProductService roleProductService;
    private final ContactInfoService contactInfoService;
    private final ProductFunctionService productFunctionService;
    private final SecurityRuleRoleService securityRuleRoleService;
    private final SecurityRuleService securityRuleService;
    private final ProductService productService;
    private final RoleService roleService;
    private final UserService userService;
    private final DelegationService delegationService;
    private final FunctionalUnitService functionalUnitService;
    private final FilterService filterService;
    private final InstanceService instanceService;
    private final TenantService tenantService;
    private final JobWorkflowService jobWorkflowService;
    private final JobLogService jobLogService;
    private final WorkflowInstanceService workflowInstanceService;
    private final EventService eventService;
    private final ShipmentService shipmentService;
    private final ShipmentItemService shipmentItemService;
    private final ServiceItemService serviceItemService;
    private final ShipmentFileService shipmentFileService;
    private final ServiceFileService fileService;
    private final FileObjectService fileObjectService;
    private final EmailTemplateService emailTemplateService;
    private final CFTypeService cfTypeService;
    private final WorkflowViewTypeService workflowViewTypeService;
    private final NodeService nodeService;
    private final WorkflowService workflowService;
    private final NodeCFService nodeCFService;
    private final CFValueService cfValueService;
    private final StatusTypeService statusTypeService;
    private final StatusService statusService;
    private final StageService stageService;
    private final PhaseService phaseService;
    private final ServiceService serviceService;
    private final ProgramService programService;
    private final NodeTypeService nodeTypeService;
    private final ProcessService processService;
    private final FileTypeService fileTypeService;
    private final ContactWorkflowService contactWorkflowService;
    private final OrganizationalUnitService organizationalUnitService;
    private final RoleContactHierarchyService roleContactHierarchyService;
    private final AddressService addressService;
    private final OccurrenceShipmentService occurrenceShipmentService;
    private final Many<JobLogTo> jobLogSink;

    @PreAuthorize("hasAuthority('CRM_Create') or hasAuthority('User Management_Create') or hasRole('ROLE_ADMIN')")
    public Contact createContact(@ValidContact ContactInput contactInput) {
        return contactService.createContact(contactInput);
    }

    @PreAuthorize("hasAuthority('CRM_Modify') or hasAuthority('User Management_Modify') or hasRole('ROLE_ADMIN')")
    public Contact modifyContact(@ValidContact ContactInput contactInput) {
        return contactService.modifyContact(contactInput);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteContact(int contactId) {
        return contactService.deleteContact(contactId);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasRole('ROLE_ADMIN')")
    public boolean restoreContact(int contactId) {
        return contactService.restoreContact(contactId);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasAuthority('User Management_Delete') or hasRole('ROLE_ADMIN')")
    public int removeAddress(int addressId, int contactId) {
        return contactService.removeAddress(addressId, contactId);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasAuthority('User Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteAddress(int addressId) {
        return addressService.deleteAddress(addressId);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasRole('ROLE_ADMIN')")
    public int removeContactInfo(int contactInfoId) {
        return contactService.removeContactInfo(contactInfoId);
    }

    // public FunctionalUnitTo addUsersToUnit(Integer functionalUnitId,
    // List<Integer> userIds) {
    // return functionalUnitService.addUsers(functionalUnitId, userIds);
    // }

    // public FunctionalUnitTo removeUsersFromUnit(Integer functionalUnitId,
    // List<Integer> userIds) {
    // return functionalUnitService.removeUsers(functionalUnitId, userIds);
    // }

    // public FunctionalUnitTo addProductsToUnit(Integer functionalUnitId,
    // Set<Integer> productIds) {
    // return functionalUnitService.addProductsToFunctionalUnit(functionalUnitId,
    // productIds);
    // }

    // public FunctionalUnitTo removeProductsFromUnit(Integer functionalUnitId,
    // Set<Integer> productIds) {
    // return
    // functionalUnitService.removeProductsFromFunctionalUnit(functionalUnitId,
    // productIds);
    // }

    @PreAuthorize("hasAuthority('Printout_Create') or hasRole('ROLE_ADMIN')")
    public PrintoutTemplate createPrintoutTemplate(PrintoutTemplateInput printoutTemplate) {
        return printoutTemplateService.createPrintoutTemplate(printoutTemplate);
    }

    @PreAuthorize("hasAuthority('Printout_Modify') or hasRole('ROLE_ADMIN')")
    public PrintoutTemplate modifyPrintoutTemplate(PrintoutTemplateInput printoutTemplate) {
        return printoutTemplateService.modifyPrintoutTemplate(printoutTemplate);
    }

    @PreAuthorize("hasAuthority('Printout_Delete') or hasRole('ROLE_ADMIN')")
    public int deletePrintoutTemplate(int printoutTemplateId) {
        return printoutTemplateService.deletePrintoutTemplate(printoutTemplateId);
    }

    public PrintoutTemplate addPrintoutTemplateToPrograms(int printoutTemplateId,
            Set<Integer> programIds) {
        return printoutTemplateService.addPrintoutTemplateToPrograms(printoutTemplateId,
                programIds);
    }

    public PrintoutTemplate removePrintoutTemplateFromPrograms(int printoutTemplateId,
            Set<Integer> programIds) {
        return printoutTemplateService.removePrintoutTemplateFromPrograms(printoutTemplateId,
                programIds);
    }

    public PrintoutTemplate addPrintoutTemplateToProducts(int printoutTemplateId,
            Set<Integer> productIds) {
        return printoutTemplateService.addPrintoutTemplateToProducts(printoutTemplateId,
                productIds);
    }

    public PrintoutTemplate removePrintoutTemplateFromProducts(int printoutTemplateId,
            Set<Integer> productIds) {
        return printoutTemplateService.removePrintoutTemplateFromProducts(printoutTemplateId,
                productIds);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Create') or hasRole('ROLE_ADMIN')")
    public CustomerTo createCustomer(CustomerInput customerInput) {
        return customerService.createCustomer(customerInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Modify') or hasRole('ROLE_ADMIN')")
    public CustomerTo modifyCustomer(CustomerInput customerInput) {
        return customerService.modifyCustomer(customerInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteCustomer(int customerId) {
        return customerService.deleteCustomer(customerId);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Create') or hasRole('ROLE_ADMIN')")
    public OrganizationTo createOrganization(OrganizationInput organizationInput) {
        return organizationService.createOrganization(organizationInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Modify') or hasRole('ROLE_ADMIN')")
    public OrganizationTo modifyOrganization(OrganizationInput organizationInput) {
        return organizationService.modifyOrganization(organizationInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteOrganization(int organizationId) {
        return organizationService.deleteOrganization(organizationId);
    }

    /**
     * 
     * @param purposeInput
     */
    public PurposeTo createPurpose(PurposeInput purposeInput) {
        return purposeService.createPurpose(purposeInput);
    }

    /**
     * 
     * @param idpurpose
     */
    public int deletePurpose(int idpurpose) {
        return purposeService.deletePurpose(idpurpose);
    }

    /**
     * 
     * @param purposeInput
     */
    public PurposeTo modifyPurpose(PurposeInput purposeInput) {
        return purposeService.modifyPurpose(purposeInput);
    }

    @PreAuthorize("hasAuthority('CRM_Create') or hasRole('ROLE_ADMIN')")
    public HierarchyTo createHierarchy(HierarchyInput hierarchyInput) {
        return hierarchyService.createHierarchy(hierarchyInput);
    }

    @PreAuthorize("hasAuthority('CRM_Modify') or hasRole('ROLE_ADMIN')")
    public HierarchyTo modifyHierarchy(HierarchyInput hierarchyInput) {
        return hierarchyService.modifyHierarchy(hierarchyInput, false);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasRole('ROLE_ADMIN')")
    public String deleteHierarchy(Integer contactId, Integer institutionId) {
        return hierarchyService.deleteHierarchy(contactId, institutionId);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteHierarchyById(int hierarchyId) {
        return hierarchyService.deleteHierarchy(hierarchyId);
    }

    @PreAuthorize("hasAuthority('CRM_Create') or hasRole('ROLE_ADMIN')")
    public ContactInfoType createContactInfoType(ContactInfoTypeInput infoTypeInput) {
        return contactInfoTypeService.createContactInfoType(infoTypeInput);
    }

    @PreAuthorize("hasAuthority('CRM_Modify') or hasRole('ROLE_ADMIN')")
    public ContactInfoType modifyContactInfoType(ContactInfoTypeInput infoTypeInput) {
        return contactInfoTypeService.modifyContactInfoType(infoTypeInput);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteContactInfoType(int infoTypeId) {
        return contactInfoTypeService.deleteContactInfoType(infoTypeId);
    }

    @PreAuthorize("hasAuthority('CRM_Create') or hasRole('ROLE_ADMIN')")
    public ContactInfo createContactInfo(ContactInfoInput contactInfoInput) {
        return contactInfoService.createContactInfo(contactInfoInput);
    }

    @PreAuthorize("hasAuthority('CRM_Modify') or hasRole('ROLE_ADMIN')")
    public ContactInfo modifyContactInfo(ContactInfoInput contactInfoInput) {
        return contactInfoService.modifyContactInfo(contactInfoInput);
    }

    @PreAuthorize("hasAuthority('CRM_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteContactInfo(int contactInfoId) {
        return contactInfoService.deleteContactInfo(contactInfoId);
    }

    /**
     * 
     * @param roleProductInput
     */
    public RoleProductTo createRoleProduct(RoleProductInput roleProductInput) {
        return roleProductService.createRoleProduct(roleProductInput);
    }

    /**
     * 
     * @param productFunctionInput
     */
    public ProductFunctionTo createProductFunction(ProductFunctionInput productFunctionInput) {
        return productFunctionService.createProductFunction(productFunctionInput);
    }

    /**
     * 
     * @param productFunctionInput
     */
    public ProductFunctionTo modifyProductFunction(ProductFunctionInput productFunctionInput) {
        return productFunctionService.modifyProductFunction(productFunctionInput);
    }

    /**
     * 
     * @param productFunctionId
     */
    public int deleteProductFunction(int productFunctionId) {
        return productFunctionService.deleteProductFunction(productFunctionId);
    }

      /**
     * 
     * @param securityRuleRoleInput
     */

     @PreAuthorize("hasAuthority('User Management_Create') or hasRole('ROLE_ADMIN')")
     public SecurityRuleRoleTo createSecurityRuleRole(SecurityRuleRoleInput securityRuleRoleInput) {
         return securityRuleRoleService.createSecurityRuleRole(securityRuleRoleInput);
     }

         /**
     * 
     * @param securityRuleRoleInput
     */
    @PreAuthorize("hasAuthority('User Management_Modify') or hasRole('ROLE_ADMIN')")
    public SecurityRuleRoleTo modifySecurityRuleRole(SecurityRuleRoleInput securityRuleRoleInput) {
        return securityRuleRoleService.modifySecurityRuleRole(securityRuleRoleInput);
    }

      /**
     * 
     * @param securityRuleRole
     */
    @PreAuthorize("hasAuthority('User Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteSecurityRule(Integer id) {
        return securityRuleService.deleteSecurityRule(id);
    }
          /**
     * 
     * @param securityRuleRoleInput
     */

     @PreAuthorize("hasAuthority('User Management_Create') or hasRole('ROLE_ADMIN')")
     public SecurityRuleTo createSecurityRule(SecurityRuleInput securityRuleInput) {
         return securityRuleService.createSecurityRule(securityRuleInput);
     }

         /**
     * 
     * @param securityRuleRoleInput
     */
    @PreAuthorize("hasAuthority('User Management_Modify') or hasRole('ROLE_ADMIN')")
    public SecurityRuleTo modifySecurityRule(SecurityRuleInput securityRuleInput) {
        return securityRuleService.modifySecurityRule(securityRuleInput);
    }

      /**
     * 
     * @param roleRuleId
     */
    @PreAuthorize("hasAuthority('User Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteSecurityRuleRole(Integer roleRuleId) {
        return securityRuleRoleService.deleteSecurityRuleRole(roleRuleId);
    }
          /**
     * 
     * @param roleId
     */
    @PreAuthorize("hasAuthority('User Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteSecurityRuleRoleByRoleId(Integer roleId) {
        return securityRuleRoleService.deleteSecurityRuleRoleByRoleId(roleId);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Create') or hasRole('ROLE_ADMIN')")
    public DomainInstanceTo createDomainInstance(DomainInstanceInput domainInstanceInput) {
        return domainInstanceService.createDomainInstance(domainInstanceInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Modify') or hasRole('ROLE_ADMIN')")
    public DomainInstanceTo modifyDomainInstance(DomainInstanceInput domainInstanceInput) {
        return domainInstanceService.modifyDomainInstance(domainInstanceInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteDomainInstance(int domainInstanceId) {
        return domainInstanceService.deleteDomainInstance(domainInstanceId);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Create') or hasRole('ROLE_ADMIN')")
    public ProductTo createProduct(ProductInput productInput) {
        return productService.createProduct(productInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Modify') or hasRole('ROLE_ADMIN')")
    public ProductTo modifyProduct(ProductInput productInput) {
        return productService.modifyProduct(productInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteProduct(int productId) {
        return productService.deleteProduct(productId);
    }

    @PreAuthorize("hasAuthority('User Management_Create') or hasRole('ROLE_ADMIN')")
    public RoleTo createRole(RoleInput roleInput) {
        return roleService.createRole(roleInput);
    }

    @PreAuthorize("hasAuthority('User Management_Modify') or hasRole('ROLE_ADMIN')")
    public RoleTo modifyRole(RoleInput roleInput) {
        return roleService.modifyRole(roleInput);
    }

    @PreAuthorize("hasAuthority('User Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteRole(int roleId) {
        return roleService.deleteRole(roleId);
    }

    @PreAuthorize("hasAuthority('User Management_Create') or hasRole('ROLE_ADMIN')")
    public UserTo createUser(UserInput userInput) {
        return userService.createUser(userInput);
    }

    // @PreAuthorize("hasAuthority('User Management_Modify') or
    // hasRole('ROLE_ADMIN')")
    public UserTo modifyUser(UserInput userInput) {
        return userService.modifyUser(userInput);
    }

    @PreAuthorize("hasAuthority('User Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteUser(int userId) {
        return userService.deleteUser(userId);
    }

    public DelegationTo createDelegation(DelegationInput delegationInput) {
        return delegationService.createDelegation(delegationInput);
    }

    public DelegationTo modifyDelegation(DelegationInput delegationInput) {
        return delegationService.modifyDelegation(delegationInput);
    }

    public int deleteDelegation(int delegationId) {
        return delegationService.deleteDelegation(delegationId);
    }

    public String deleteRoleProduct(Integer roleId, Set<Integer> roleProductIds) {
        return roleProductService.deleteRoleProduct(roleId, roleProductIds);
    }

    public FunctionalUnitTo createFunctionalUnit(FunctionalUnitInput functionalUnitInput) {
        return functionalUnitService.createFunctionalUnit(functionalUnitInput);
    }

    public FunctionalUnitTo modifyFunctionalUnit(FunctionalUnitInput functionalUnitInput) {
        return functionalUnitService.modifyFunctionalUnit(functionalUnitInput);
    }

    public int deleteFunctionalUnit(int functionalUnitId) {
        return functionalUnitService.removeFunctionalUnit(functionalUnitId);
    }

    public FilterTo createFilter(FilterEntityInput filterInput) {
        return filterService.createFilter(filterInput);
    }

    public FilterTo modifyFilter(FilterEntityInput filterInput) {
        return filterService.modifyFilter(filterInput);
    }

    public int deleteFilter(int filterId) {
        return filterService.deleteFilter(filterId);
    }

    public String createFilterDomain(int filterId, int domainId) {
        return filterService.createFilterDomain(filterId, domainId);
    }

    public String deleteFilterDomain(int filterId, int domainId) {
        return filterService.deleteFilterDomain(filterId, domainId);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Create') or hasRole('ROLE_ADMIN')")
    public InstanceTo createInstance(InstanceInput instanceInput) {
        return instanceService.createInstance(instanceInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Modify') or hasRole('ROLE_ADMIN')")
    public InstanceTo modifyInstance(InstanceInput instanceInput) {
        return instanceService.modifyInstance(instanceInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteInstance(int instanceId) {
        return instanceService.deleteInstance(instanceId);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Create') or hasRole('ROLE_ADMIN')")
    public TenantTo createTenant(TenantInput tenantInput) {
        return tenantService.createTenant(tenantInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Modify') or hasRole('ROLE_ADMIN')")
    public TenantTo modifyTenant(TenantInput tenantInput) {
        return tenantService.modifyTenant(tenantInput);
    }

    @PreAuthorize("hasAuthority('Tenant Management_Delete') or hasRole('ROLE_ADMIN')")
    public int deleteTenant(int tenantId) {
        return tenantService.deleteTenant(tenantId);
    }

    public JobWorkflowTo createJobWorkflow(JobWorkflowInput jobWorkflowInput) {
        return jobWorkflowService.createJobWorkflow(jobWorkflowInput);
    }

    public JobWorkflowTo modifyJobWorkflow(JobWorkflowInput jobWorkflowInput) {
        return jobWorkflowService.modifyJobWorkflow(jobWorkflowInput);
    }

    public int deleteJobWorkflow(int jobWorkflowId) {
        return jobWorkflowService.deleteJobWorkflow(jobWorkflowId);
    }

    public JobLogTo createJobLog(JobLogInput jobLogInput) {
        JobLogTo jobLogTo = jobLogService.createJobLog(jobLogInput);
        jobLogSink.tryEmitNext(jobLogTo);
        return jobLogTo;
    }

    public JobLogTo modifyJobLog(JobLogInput jobLogInput) {
        JobLogTo jobLogTo = jobLogService.modifyJobLog(jobLogInput);
        jobLogSink.tryEmitNext(jobLogTo);
        return jobLogTo;
    }

    public int deleteJobLog(int jobLogId) {
        return jobLogService.deleteJobLog(jobLogId);
    }

    public WorkflowInstanceTo createWorkflowInstance(WorkflowInstanceInput input) {
        return workflowInstanceService.createWorkflowInstance(input);
    }

    public WorkflowInstanceTo modifyWorkflowInstance(WorkflowInstanceInput input) {
        return workflowInstanceService.modifyWorkflowInstance(input);
    }

    public int deleteWorkflowInstance(int id) {
        return workflowInstanceService.deleteWorkflowInstance(id);
    }

    public EventTo createEvent(EventInput input) {
        return eventService.createEvent(input);
    }

    public EventTo modifyEvent(EventInput input) {
        return eventService.modifyEvent(input);
    }

    public int deleteEvent(int id) {
        return eventService.deleteEvent(id);
    }

    public ShipmentTo createShipment(ShipmentInput input) {
        return shipmentService.createShipment(input);
    }

    public ShipmentTo modifyShipment(ShipmentInput input) {
        return shipmentService.modifyShipment(input);
    }

    public int deleteShipment(int id) {
        return shipmentService.deleteShipment(id);
    }

    public ShipmentItemTo createShipmentItem(ShipmentItemInput input) {
        return shipmentItemService.createShipmentItem(input);
    }

    public int createShipmentItems(List<ShipmentItemInput> input) {
        return shipmentItemService.createShipmentItems(input);
    }

    public ShipmentItemTo modifyShipmentItem(ShipmentItemInput input) {
        return shipmentItemService.modifyShipmentItem(input);
    }

    public int modifyShipmentItems(Integer shipmentId, List<ShipItemInput> input) {
        return shipmentItemService.modifyShipmentItems(shipmentId, input);
    }

    public int deleteShipmentItem(int id) {
        return shipmentItemService.deleteShipmentItem(id);
    }

    public int deleteShipmentItems(Integer shipmentId, List<Integer> ids) {
        return shipmentItemService.deleteShipmentItems(shipmentId, ids);
    }

    public ServiceItemTo createServiceItem(ServiceItemInput input) {
        return serviceItemService.createServiceItem(input);
    }

    public int createServiceItems(List<ServiceItemInput> input) {
        return serviceItemService.createServiceItems(input);
    }

    public ServiceItemTo modifyServiceItem(ServiceItemInput input) {
        return serviceItemService.modifyServiceItem(input);
    }

    public int modifyServiceItems(Integer serviceId, List<ServItemInput> input) {
        return serviceItemService.modifyServiceItems(serviceId, input);
    }
    public int deleteServiceItem(int id) {
        return serviceItemService.deleteServiceItem(id);
    }

    public int deleteServiceItems(Integer serviceId, List<Integer> ids) {
        return serviceItemService.deleteServiceItems(serviceId, ids);
    }

    public ShipmentFileTo createShipmentFile(ShipmentFileInput input) {
        return shipmentFileService.createShipmentFile(input);
    }

    public ShipmentFileTo modifyShipmentFile(ShipmentFileInput input) {
        return shipmentFileService.modifyShipmentFile(input);
    }

    public ServiceFileTo createServiceFile(ServiceFileInput input) {
        return fileService.createServiceFile(input);
    }

    public ServiceFileTo modifyServiceFile(ServiceFileInput input) {
        return fileService.modifyServiceFile(input);
    }

    public FileObjectTo modifyFileObject(FileObjectInput input) {
        return fileObjectService.modifyFileObject(input);
    }

    public int deleteShipmentFile(int id) {
        return shipmentFileService.deleteShipmentFile(id);
    }
    
    public int deleteServiceFile(int id) {
        return fileService.deleteServiceFile(id);
    }

    public EmailTemplateTo createEmailTemplate(EmailTemplateInput input) {
        return emailTemplateService.createEmailTemplate(input);
    }
    public EmailTemplateTo modifyEmailTemplate(EmailTemplateInput input) {
        return emailTemplateService.modifyEmailTemplate(input);
    }
    public int deleteEmailTemplate(int id) {
        return emailTemplateService.deleteEmailTemplate(id);
    }

    public CFTypeTo createCFType(CFTypeInput input) {
        return cfTypeService.createCFType(input);
    }

    public CFTypeTo modifyCFType(CFTypeInput input) {
        return cfTypeService.modifyCFType(input);
    }

    public int deleteCFType(int id) {
        return cfTypeService.deleteCFType(id);
    }

    public WorkflowViewTypeTo createWorkflowViewType(WorkflowViewTypeInput input) {
        return workflowViewTypeService.createWorkflowViewType(input);
    }

    public WorkflowViewTypeTo modifyWorkflowViewType(WorkflowViewTypeInput input) {
        return workflowViewTypeService.modifyWorkflowViewType(input);
    }

    public int deleteWorkflowViewType(int id) {
        return workflowViewTypeService.deleteWorkflowViewType(id);
    }

        public NodeTypeTo createNodeType(NodeTypeInput input) {
        return nodeTypeService.createNodeType(input);
    }

    public NodeTypeTo modifyNodeType(NodeTypeInput input) {
        return nodeTypeService.modifyNodeType(input);
    }

    public int deleteNodeType(int id) {
        return nodeTypeService.deleteNodeType(id);
    }

    public NodeTo createNode(NodeInput input) {
        return nodeService.createNode(input);
    }

    public NodeTo modifyNode(NodeInput input) {
        return nodeService.modifyNode(input);
    }

    public int deleteNode(int id) {
        return nodeService.deleteNode(id);
    }

    public StageTo createStage(StageInput input) {
        return stageService.createStage(input);
    }

    public StageTo modifyStage(StageInput input) {
        return stageService.modifyStage(input);
    }

    public int deleteStage(int id) {
        return stageService.deleteStage(id);
    }

    public WorkflowTo createWorkflow(WorkflowInput input) {
        return workflowService.createWorkflow(input);
    }

    public WorkflowTo modifyWorkflow(WorkflowInput input) {
        return workflowService.modifyWorkflow(input);
    }

    public int deleteWorkflow(int id) {
        return workflowService.deleteWorkflow(id);
    }

    public NodeCFTo createNodeCF(NodeCFInput input) {
        return nodeCFService.createNodeCF(input);
    }

    public NodeCFTo modifyNodeCF(NodeCFInput input) {
        return nodeCFService.modifyNodeCF(input);
    }

    public int deleteNodeCF(int id) {
        return nodeCFService.deleteNodeCF(id);
    }

    public CFValueTo createCFValue(CFValueInput input) {
        return cfValueService.createCFValue(input);
    }
    public Integer createCFValues(List<CFValueInput> input) {
        return cfValueService.createCFValues(input);
    }
    public CFValueTo modifyCFValue(CFValueInput input) {
        return cfValueService.modifyCFValue(input);
    }
    public Integer modifyCFValues(List<CFValueInput> input) {
        return cfValueService.modifyCFValues(input);
    }

    public int deleteCFValue(int id) {
        return cfValueService.deleteCFValue(id);
    }

    public StatusTypeTo createStatusType(StatusTypeInput input) {
        return statusTypeService.createStatusType(input);
    }

    public StatusTypeTo modifyStatusType(StatusTypeInput input) {
        return statusTypeService.modifyStatusType(input);
    }

    public int deleteStatusType(int id) {
        return statusTypeService.deleteStatusType(id);
    }

    public StatusTo createStatus(StatusInput input) {
        return statusService.createStatus(input);
    }

    public StatusTo modifyStatus(StatusInput input) {
        return statusService.modifyStatus(input);
    }

    public int deleteStatus(int id) {
        return statusService.deleteStatus(id);
    }

    public PhaseTo createPhase(PhaseInput input) {
        return phaseService.createPhase(input);
    }

    public PhaseTo modifyPhase(PhaseInput input) {
        return phaseService.modifyPhase(input);
    }

    public int deletePhase(int id) {
        return phaseService.deletePhase(id);
    }

    public ServiceTo createService(ServiceInput input) {
        return serviceService.createService(input);
    }

    public ServiceTo modifyService(ServiceInput input) {
        return serviceService.modifyService(input);
    }

    public int deleteService(int id) {
        return serviceService.deleteService(id);
    }
    /**
     * 
     * @param programInput
     */
    public ProgramTo createProgram(ProgramInput programInput) {
        return programService.createProgram(programInput);
    }

    /**
     * 
     * @param programInput
     */
    public ProgramTo modifyProgram(ProgramInput programInput) {
        return programService.modifyProgram(programInput);
    }

    /**
     * 
     * @param programId
     */
    public int deleteProgram(int programId) {
        return programService.deleteProgram(programId);
    }

    public ProcessTo createProcess(ProcessInput input) {
        return processService.createProcess(input);
    }

    public ProcessTo modifyProcess(ProcessInput input) {
        return processService.modifyProcess(input);
    }

    public int deleteProcess(int id) {
        return processService.deleteProcess(id);
    }

    public FileTypeTo createFileType(FileTypeInput input) {
        return fileTypeService.createFileType(input);
    }

    public FileTypeTo modifyFileType(FileTypeInput input) {
        return fileTypeService.modifyFileType(input);
    }

    public int deleteFileType(int id) {
        return fileTypeService.deleteFileType(id);
    }

    public ContactWorkflowsTo createContactWorkflow(ContactWorkflowInput input) {
        return contactWorkflowService.createContactWorkflow(input);
    }

    public ContactWorkflowsTo modifyContactWorkflow(ContactWorkflowInput input) {
        return contactWorkflowService.modifyContactWorkflow(input);
    }

    public int deleteContactWorkflowById(int id) {
        return contactWorkflowService.deleteContactWorkflow(id);
    }

    public String deleteContactWorkflow(int contactId, int workflowId) {
        return contactWorkflowService.deleteContactWorkflow(contactId, workflowId);
    }

    public Contact createOrganizationalUnit(OrganizationalUnitInput input) {
        return organizationalUnitService.createOrganizationalUnit(input);
    }

    public Contact modifyOrganizationalUnit(OrganizationalUnitInput input) {
        return organizationalUnitService.modifyOrganizationalUnit(input);
    }

    public boolean deleteOrganizationalUnit(int organizationalUnitId) {
        return organizationalUnitService.deleteOrganizationalUnit(organizationalUnitId);
    }

    public boolean createRoleContactHierarchy(int roleId, int contactHierarchyId) {
        return roleContactHierarchyService.createRoleContactHierarchy(roleId, contactHierarchyId);
    }

    public boolean deleteRoleContactHierarchy(int roleId, int contactHierarchyId) {
        return roleContactHierarchyService.deleteRoleContactHierarchy(roleId, contactHierarchyId);
    }

    public OccurrenceShipmentTo createOccurrenceShipment(OccurrenceShipmentInput input) {
        return occurrenceShipmentService.createOccurrenceShipment(input);
    }

    public OccurrenceShipmentTo modifyOccurrenceShipment(OccurrenceShipmentInput input) {
        return occurrenceShipmentService.modifyOccurrenceShipment(input);
    }

    public int deleteOccurrenceShipment(int id) {
        return occurrenceShipmentService.deleteOccurrenceShipment(id);
    }

}