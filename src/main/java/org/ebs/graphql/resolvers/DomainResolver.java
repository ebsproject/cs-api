///////////////////////////////////////////////////////////
//  DomainResolver.java
//  Macromedia ActionScript Implementation of the Class DomainResolver
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:55 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.DomainService;
import org.ebs.services.to.DomainInstanceTo;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.EntityReferenceTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.UserTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:55 AM
 */
@Component
@Validated
@RequiredArgsConstructor
public class DomainResolver implements GraphQLResolver<DomainTo> {

    private final DomainService domainService;

    /**
     *
     * @param domain
     */
    public List<DomainInstanceTo> getDomaininstances(DomainTo domain) {
        return domainService.findDomainInstances(domain.getId());
    }

    /**
     *
     * @param domainTo
     */
    public List<EntityReferenceTo> getEntityreferences(DomainTo domainTo) {
        return domainService.findEntityReferences(domainTo.getId());
    }

    /**
     *
     * @param domainTo
     */
    public HtmlTagTo getHtmltag(DomainTo domainTo) {
        return domainService.findHtmlTag(domainTo.getId()).get();
    }

    /**
     *
     * @param domain
     */
    public List<ProductTo> getProducts(DomainTo domain) {
        return domainService.findProducts(domain.getId());
    }

    public UserTo getCreatedBy(DomainTo to) {
        return domainService.findCreatedBy(to.getId());
    }

    public UserTo getUpdatedBy(DomainTo to) {
        return domainService.findUpdatedBy(to.getId());
    }

}