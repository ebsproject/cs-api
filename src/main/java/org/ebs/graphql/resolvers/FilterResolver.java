package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.FilterService;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.FilterTo;
import org.ebs.services.to.TenantTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class FilterResolver implements GraphQLResolver<FilterTo> {

    private final FilterService filterService;

    public TenantTo getTenant(FilterTo filterTo) {
        return filterService.findTenant(filterTo.getId());
    }

    public List<DomainTo> getDomains(FilterTo filterTo) {
        return filterService.findDomains(filterTo.getId());
    }

}
