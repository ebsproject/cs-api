package org.ebs.graphql.resolvers;

import org.ebs.services.ContactWorkflowService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.WorkflowTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor

public class ContactWorkflowsResolver implements GraphQLResolver<ContactWorkflowsTo> {
 
    private final ContactWorkflowService contactWorkflowService;

    public WorkflowTo getWorkflow(ContactWorkflowsTo to) {
        return contactWorkflowService.findWorkflow(to.getId());
    }

    public Contact getContact(ContactWorkflowsTo to) {
        return contactWorkflowService.findContact(to.getId());
    }
    
}
