package org.ebs.graphql.resolvers;

import org.ebs.services.NodeCFService;
import org.ebs.services.to.AttributesTo;
import org.ebs.services.to.CFTypeTo;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.TenantTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class NodeCFResolver implements GraphQLResolver<NodeCFTo> {
    
    private final NodeCFService nodeCFService;

    public AttributesTo getAttributes(NodeCFTo nodeCFTo) {
        return nodeCFService.findAttributes(nodeCFTo.getId());
    }

    public CFTypeTo getCfType(NodeCFTo nodeCFTo) {
        return nodeCFService.findCFType(nodeCFTo.getId());
    }

    public HtmlTagTo getHtmlTag(NodeCFTo nodeCFTo) {
        return nodeCFService.findHtmlTag(nodeCFTo.getId());
    }

    public NodeTo getNode(NodeCFTo nodeCFTo) {
        return nodeCFService.findNode(nodeCFTo.getId());
    }

    public CFValueTo getCfValue(NodeCFTo nodeCFTo) {
        return nodeCFService.findCFValue(nodeCFTo.getId());
    }

    public TenantTo getTenant(NodeCFTo nodeCFTo) {
        return nodeCFService.findTenant(nodeCFTo.getId());
    }
}
