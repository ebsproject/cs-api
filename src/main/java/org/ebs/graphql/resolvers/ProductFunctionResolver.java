package org.ebs.graphql.resolvers;

import java.util.Set;

import graphql.kickstart.tools.GraphQLResolver;

import org.ebs.services.ProductFunctionService;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.RoleTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ProductFunctionResolver implements GraphQLResolver<ProductFunctionTo> {

    private final ProductFunctionService productfunctionService;

    /**
     *
     * @param productfunctionTo
     */
    public ProductTo getProduct(ProductFunctionTo productfunctionTo) {
        return productfunctionService.findProduct(productfunctionTo.getId()).get();
    }

    /**
     *
     * @param productfunctionTo
     */
    public Set<RoleTo> getRoles(ProductFunctionTo productfunctionTo) {
        return productfunctionService.findRoles(productfunctionTo.getId());
    }

}