package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.ShipmentService;
import org.ebs.services.to.Address;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ShipmentResolver implements GraphQLResolver<ShipmentTo> {

    private final ShipmentService shipmentService;
    
    public WorkflowInstanceTo getWorkflowInstance(ShipmentTo shipmentTo) {
        return shipmentService.findWorkflowInstance(shipmentTo.getId());
    }

    public ProgramTo getProgram(ShipmentTo shipmentTo) {
        return shipmentService.findProgram(shipmentTo.getId());
    }

    public Contact getSender(ShipmentTo shipmentTo) {
        return shipmentService.findSender(shipmentTo.getId());
    }

    public Contact getProcessor(ShipmentTo shipmentTo) {
        return shipmentService.findProcessor(shipmentTo.getId());
    }

    public Contact getRequester(ShipmentTo shipmentTo) {
        return shipmentService.findRequester(shipmentTo.getId());
    }

    public Contact getRecipient(ShipmentTo shipmentTo) {
        return shipmentService.findRecipient(shipmentTo.getId());
    }

    public TenantTo getTenant(ShipmentTo shipmentTo) {
        return shipmentService.findTenant(shipmentTo.getId());
    }

    public String getStatus(ShipmentTo shipmentTo) {
        return shipmentService.getStatus(shipmentTo.getId());
    }

    public Address getAddress(ShipmentTo shipmentTo) {
        return shipmentService.findAddress(shipmentTo.getId());
    }

    public Address getSenderAddress(ShipmentTo shipmentTo) {
        return shipmentService.findSenderAddress(shipmentTo.getId());
    }

    public Address getRequestorAddress(ShipmentTo shipmentTo) {
        return shipmentService.findRequestorAddress(shipmentTo.getId());
    }

    public List<ShipmentItemTo> getItems(ShipmentTo shipmentTo) {
        return shipmentService.findItems(shipmentTo.getId());
    }

}
