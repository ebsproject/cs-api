package org.ebs.graphql.resolvers;

import org.ebs.services.ServiceFileService;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.ServiceFileTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ServiceFileResolver implements GraphQLResolver<ServiceFileTo> {

    private final ServiceFileService fileService;

    public FileObjectTo getFile(ServiceFileTo fileTo) {
        return fileService.findFileObject(fileTo.getId());
    }

    public ServiceTo getService(ServiceFileTo fileTo) {
        return fileService.findService(fileTo.getId());
    }

    public TenantTo getTenant(ServiceFileTo fileTo) {
        return fileService.findTenant(fileTo.getId());
    }

    public FileTypeTo getFileType(ServiceFileTo fileTo) {
        return fileService.findFileType(fileTo.getId());
    }

    public UserTo getCreatedBy(ServiceFileTo fileTo) {
        return fileService.findCreatedBy(fileTo.getId());
    }

    public UserTo getUpdatedBy(ServiceFileTo fileTo) {
        return fileService.findUpdatedBy(fileTo.getId());
    }
    
}
