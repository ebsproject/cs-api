package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.ServiceService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.Person;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ServiceResolver implements GraphQLResolver<ServiceTo> {

    private final ServiceService serviceService;

    public Person getPerson(ServiceTo serviceTo) {
        return serviceService.findPerson(serviceTo.getId());
    }

    public Contact getProgram(ServiceTo serviceTo) {
        return serviceService.findProgram(serviceTo.getId());
    }

    public WorkflowInstanceTo getWorkflowInstance(ServiceTo serviceTo) {
        return serviceService.findWorkflowInstance(serviceTo.getId());
    }

    public TenantTo getTenant(ServiceTo serviceTo) {
        return serviceService.findTenant(serviceTo.getId());
    }

    public Contact getSender(ServiceTo serviceTo) {
        return serviceService.findSender(serviceTo.getId());
    }

    public Contact getRequestor(ServiceTo serviceTo) {
        return serviceService.findRequestor(serviceTo.getId());
    }

    public Contact getRecipient(ServiceTo serviceTo) {
        return serviceService.findRecipient(serviceTo.getId());
    }

    public List<ServiceItemTo> getItems(ServiceTo serviceTo) {
        return serviceService.findItems(serviceTo.getId());
    }

    public Contact getServiceProvider(ServiceTo serviceTo) {
        return serviceService.findServiceProvider(serviceTo.getId());
    }
}
