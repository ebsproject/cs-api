package org.ebs.graphql.resolvers;

import org.ebs.services.WorkflowInstanceService;
import org.ebs.services.to.StatusTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.WorkflowTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class WorkflowInstanceResolver implements GraphQLResolver<WorkflowInstanceTo> {
    
    private final WorkflowInstanceService workflowInstanceService;

    public WorkflowTo getWorkflow(WorkflowInstanceTo workflowInstanceTo) {
        return workflowInstanceService.findWorkflow(workflowInstanceTo.getId());
    }

    public StatusTo getStatus(WorkflowInstanceTo workflowInstanceTo) { 
        return workflowInstanceService.findStatus(workflowInstanceTo.getId());
    }

}
