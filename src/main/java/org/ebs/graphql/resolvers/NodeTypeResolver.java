package org.ebs.graphql.resolvers;

import org.ebs.services.to.NodeTypeTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class NodeTypeResolver implements GraphQLResolver<NodeTypeTo> {
    
}
