package org.ebs.graphql.resolvers;

import static java.util.Optional.ofNullable;

import java.util.List;
import java.util.Optional;

import graphql.kickstart.tools.GraphQLResolver;

import org.ebs.services.AddressService;
import org.ebs.services.CountryService;
import org.ebs.services.to.Address;
import org.ebs.services.to.Contact;
import org.ebs.services.to.Country;
import org.ebs.services.to.PurposeTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class AddressResolver implements GraphQLResolver<Address> {

    private final CountryService countryService;
    private final AddressService addressService;

    public Country getCountry(Address address) {
        return countryService.findByAddress(address.getId());
    }

    public List<PurposeTo> getPurposes(Address address) {
		return ofNullable(addressService.findPurposes(address.getId())).map(c -> c)
		.orElse(null);
	}

    public List<Contact> getContacts(Address address) {
        return addressService.findContacts(address.getId());
    }

    public Optional<Contact> getInstitution(Address address) {
        return addressService.findInstitution(address.getId());
    }

}