package org.ebs.graphql.resolvers;

import static java.util.Optional.ofNullable;

import java.util.List;
import java.util.Set;

import org.ebs.services.AddressService;
import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactService;
import org.ebs.services.ContactTypeService;
import org.ebs.services.TenantService;
import org.ebs.services.to.Address;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactInfo;
import org.ebs.services.to.ContactType;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.Institution;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.Person;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ContactResolver implements GraphQLResolver<Contact> {

    private final ContactService contactService;
    private final ContactTypeService contactTypeService;
    private final AddressService addressService;
    private final ContactInfoService contactInfoService;
    private final TenantService tenantService;

    public List<HierarchyTo> getParents(Contact contact) {
        return contactService.findParents(contact.getId());
    }

    public List<HierarchyTo> getMembers(Contact contact) {
        return contactService.findMembers(contact.getId());
    }

    public List<ContactType> getContactTypes(Contact contact) {
        return contactTypeService.findByContact(contact.getId());
    }

    public List<Address> getAddresses(Contact contact) {
        return addressService.findByContact(contact.getId());
    }

    public List<ContactInfo> getContactInfos(Contact contact) {
        return contactInfoService.findByContact(contact.getId());
    }

    public Person getPerson(Contact contact) {
        return ofNullable(contactService.findById(contact.getId())).map(c -> c.getPerson())
                .orElse(null);
    }

    public Institution getInstitution(Contact contact) {
        return ofNullable(contactService.findById(contact.getId())).map(c -> c.getInstitution())
                .orElse(null);
    }
    
    public String getEmailDuplicateMessage(Contact contact) {
        String emailDuplicateMessage = "Email OK.";
        int emailCount = contactService.countByEmail(contact.getEmail());
        if (emailCount > 1)
            emailDuplicateMessage = "The email entered already exists for other contact.";
        return emailDuplicateMessage;
    }

    public Set<PurposeTo> getPurposes(Contact contact) {
        return contactService.findPurposes(contact.getId());
    }

    public List<TenantTo> getTenants(Contact contact) {
        return tenantService.findByContact(contact.getId());
    }

    public Set<JobLogTo> getJobLogs(Contact contact) {
        return contactService.findJobLogs(contact.getId());
    }

    public Set<UserTo> getUsers(Contact contact) {
        return contactService.findUsers(contact.getId());
    }

    public List<ContactWorkflowsTo> getWorkflows(Contact contact) {
        return contactService.findWorkflows(contact.getId());
    }

    public UserTo getCreatedBy(Contact contact) {
        return contactService.findCreatedBy(contact.getId());
    }

    public UserTo getUpdatedBy(Contact contact) {
        return contactService.findUpdatedBy(contact.getId());
    }
}