package org.ebs.graphql.resolvers;

import org.ebs.services.WorkflowViewTypeService;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.TenantTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class WorkflowViewTypeResolver implements GraphQLResolver<WorkflowViewTypeTo> {
    
    private final WorkflowViewTypeService workflowViewTypeService;

    public TenantTo getTenant(WorkflowViewTypeTo workflowViewTypeTo) {
        return workflowViewTypeService.findTenant(workflowViewTypeTo.getId());
    }
}
