package org.ebs.graphql.resolvers;

import java.util.Set;

import graphql.kickstart.tools.GraphQLResolver;

import org.ebs.services.HtmlTagService;
import org.ebs.services.to.AlertTo;
import org.ebs.services.to.AttributesTo;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.MessageTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.TranslationTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class HtmlTagResolver implements GraphQLResolver<HtmlTagTo> {

    private final HtmlTagService htmltagService;

    /**
     *
     * @param htmltag
     */
    public Set<AlertTo> getAlerts(HtmlTagTo htmltag) {
        return htmltagService.findAlerts(htmltag.getId());
    }

    /**
     *
     * @param htmltag
     */
    public Set<AttributesTo> getAttributess(HtmlTagTo htmltag) {
        return htmltagService.findAttributess(htmltag.getId());
    }

    /**
     *
     * @param htmltag
     */
    public Set<DomainTo> getDomains(HtmlTagTo htmltag) {
        return htmltagService.findDomains(htmltag.getId());
    }

    /**
     *
     * @param htmltag
     */
    public Set<MessageTo> getMessages(HtmlTagTo htmltag) {
        return htmltagService.findMessages(htmltag.getId());
    }

    /**
     *
     * @param htmltag
     */
    public Set<ProductTo> getProducts(HtmlTagTo htmltag) {
        return htmltagService.findProducts(htmltag.getId());
    }

    /**
     *
     * @param htmltag
     */
    public Set<TranslationTo> getTranslations(HtmlTagTo htmltag) {
        return htmltagService.findTranslations(htmltag.getId());
    }

}