
package org.ebs.graphql.resolvers;

import java.util.List;
import java.util.Optional;

import org.ebs.services.HierarchyService;
import org.ebs.services.ProductFunctionService;
import org.ebs.services.RoleProductService;
import org.ebs.services.RoleService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.RoleProductTo;
import org.ebs.services.to.RoleTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class RoleProductResolver implements GraphQLResolver<RoleProductTo> {

    private final RoleService roleService;
    private final ProductFunctionService  productFunctionService;

    public List<RoleTo> getRole(RoleTo role) {
        return roleService.findByRole(role.getId());
    }

}