package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.EventService;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class EventResolver implements GraphQLResolver<EventTo> {

    private final EventService eventService;

    public StageTo getStage(EventTo eventTo) {
        return eventService.findStage(eventTo.getId());
    }

    public TenantTo getTenant(EventTo eventTo) {
        return eventService.findTenant(eventTo.getId());
    }

    public WorkflowInstanceTo getWorkflowInstance(EventTo eventTo) {
        return eventService.findWorkflowInstance(eventTo.getId());
    }

    public List<CFValueTo> getCfValues(EventTo eventTo) {
        return eventService.findCFValues(eventTo.getId());
    }

    public NodeTo getNode(EventTo eventTo) {
        return eventService.findNode(eventTo.getId());
    }
    
}
