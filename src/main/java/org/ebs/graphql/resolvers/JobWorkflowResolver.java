package org.ebs.graphql.resolvers;

import java.util.Set;

import org.ebs.services.JobWorkflowService;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobTypeTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.TranslationTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class JobWorkflowResolver implements GraphQLResolver<JobWorkflowTo> {

    private final JobWorkflowService jobWorkflowService;

    public JobTypeTo getJobType(JobWorkflowTo jobWorkflowTo) {
        return jobWorkflowService.findJobType(jobWorkflowTo.getId());
    }

    public ProductFunctionTo getProductFunction(JobWorkflowTo jobWorkflowTo) {
        return jobWorkflowService.findProductFunction(jobWorkflowTo.getId());
    }

    public TranslationTo getTranslation(JobWorkflowTo jobWorkflowTo) {
        return jobWorkflowService.findTranslation(jobWorkflowTo.getId());
    }

    public Set<JobLogTo> getJobLogs(JobWorkflowTo jobWorkflowTo) {
        return jobWorkflowService.findJobLogs(jobWorkflowTo.getId());
    }

    public TenantTo getTenant(JobWorkflowTo jobWorkflowTo) {
        return jobWorkflowService.findTenant(jobWorkflowTo.getId());
    }

    public EmailTemplateTo getEmailTemplate(JobWorkflowTo jobWorkflowTo) {
        return jobWorkflowService.findEmailTemplate(jobWorkflowTo.getId());
    }
}
