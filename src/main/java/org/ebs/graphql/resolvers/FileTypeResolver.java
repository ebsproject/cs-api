package org.ebs.graphql.resolvers;

import org.ebs.services.FileTypeService;
import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.WorkflowTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class FileTypeResolver implements GraphQLResolver<FileTypeTo> {
    
    private final FileTypeService fileTypeService;

    public WorkflowTo getWorkflow(FileTypeTo fileTypeTo) {
        return fileTypeService.findWorkflow(fileTypeTo.getId());
    }

}
