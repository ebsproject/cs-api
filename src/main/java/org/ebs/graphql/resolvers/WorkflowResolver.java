package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.TenantService;
import org.ebs.services.WorkflowService;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class WorkflowResolver implements GraphQLResolver<WorkflowTo> {
    
    private final WorkflowService workflowService;
    private final TenantService tenantService;

    public HtmlTagTo getHtmlTag(WorkflowTo workflowTo) {
        return workflowService.findHtmlTag(workflowTo.getId());
    }

    public ProductTo getProduct(WorkflowTo workflowTo) {
        return workflowService.findProduct(workflowTo.getId());
    }

    public List<PhaseTo> getPhases(WorkflowTo workflowTo) {
        return workflowService.findPhases(workflowTo.getId());
    }

    public List<StatusTypeTo> getStatusTypes(WorkflowTo workflowTo) {
        return workflowService.findStatusTypes(workflowTo.getId());
    }

    public List<TenantTo> getTenants(WorkflowTo workflowTo) {
        return tenantService.findByWorkflow(workflowTo.getId());
    }

    public List<ContactWorkflowsTo> getWorkflowContacts(WorkflowTo workflowTo) {
        return workflowService.findContacts(workflowTo.getId());
    }
}
