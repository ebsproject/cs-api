package org.ebs.graphql.resolvers;

import org.ebs.services.to.JobLogTo;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Component;

import graphql.kickstart.tools.GraphQLSubscriptionResolver;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Component
@RequiredArgsConstructor
public class JobLogSubscriptionResolver implements GraphQLSubscriptionResolver {
    private final Flux<JobLogTo> flux;

    public Publisher<JobLogTo> jobLog() {
        return flux;
    }
}
