package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.NodeService;
import org.ebs.services.StageService;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class StageResolver implements GraphQLResolver<StageTo> {
    
    private final StageService stageService;
    private final NodeService nodeService;

    public HtmlTagTo getHtmlTag(StageTo stageTo) {
        return stageService.findHtmlTag(stageTo.getId());
    }

    public StageTo getParent(StageTo stageTo) {
        return stageService.findParent(stageTo.getId());
    }

    public PhaseTo getPhase(StageTo stageTo) {
        return stageService.findPhase(stageTo.getId());
    }

    public WorkflowViewTypeTo getWorkflowViewType(StageTo stageTo) {
        return stageService.findWorkflowViewType(stageTo.getId());
    }

    public List<NodeTo> getNodes(StageTo stageTo) {
        return nodeService.findByStage(stageTo.getId());
    }

    public TenantTo getTenant(StageTo stageTo) {
        return stageService.findTenant(stageTo.getId());
    }

}
