package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.ContactService;
import org.ebs.services.TenantService;
import org.ebs.services.UserService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.RoleTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class UserResolver implements GraphQLResolver<UserTo> {

    private final ContactService contactService;
    private final TenantService tenantService;
    private final UserService userService;

    public Contact getContact(UserTo user) {
        return contactService.findByUser(user.getId());
    }

    public List<TenantTo> getTenants(UserTo user) {
        return tenantService.findByUser(user.getId());
    }

    public RoleTo getDefaultRole(UserTo user) {
        return userService.findDefaultRole(user.getId());
    }

    public UserTo getCreatedBy(UserTo user) {
        return userService.findCreatedBy(user.getId());
    }

    public UserTo getUpdatedBy(UserTo user) {
        return userService.findUpdatedBy(user.getId());
    }

}