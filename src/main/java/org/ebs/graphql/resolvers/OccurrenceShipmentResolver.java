package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.OccurrenceShipmentService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class OccurrenceShipmentResolver implements GraphQLResolver<OccurrenceShipmentTo> {
    
    private final OccurrenceShipmentService occurrenceShipmentService;

    public ServiceTo getService(OccurrenceShipmentTo occurrenceShipmentTo) {
        return occurrenceShipmentService.findService(occurrenceShipmentTo.getId());
    }

    public Contact getRecipient(OccurrenceShipmentTo occurrenceShipmentTo) {
        return occurrenceShipmentService.findRecipient(occurrenceShipmentTo.getId());
    }

    public List<ServiceItemTo> getItems(OccurrenceShipmentTo occurrenceShipmentTo) {
        return occurrenceShipmentService.findItems(occurrenceShipmentTo.getId());
    }
}
