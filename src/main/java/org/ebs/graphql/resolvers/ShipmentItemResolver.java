package org.ebs.graphql.resolvers;

import org.ebs.services.ShipmentItemService;
import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ShipmentItemResolver implements GraphQLResolver<ShipmentItemTo> {
    
    private final ShipmentItemService shipmentItemService;

    public ShipmentTo getShipment(ShipmentItemTo shipmentItemTo) {
        return shipmentItemService.findShipment(shipmentItemTo.getId());
    }

    public TenantTo getTenant(ShipmentItemTo shipmentItemTo) {
        return shipmentItemService.findTenant(shipmentItemTo.getId());
    }

}
