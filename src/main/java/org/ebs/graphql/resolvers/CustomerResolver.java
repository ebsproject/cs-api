package org.ebs.graphql.resolvers;

import org.ebs.services.CustomerService;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.UserTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class CustomerResolver implements GraphQLResolver<CustomerTo> {

    private final CustomerService customerService;

    public UserTo getCreatedBy(CustomerTo to) {
        return customerService.findCreatedBy(to.getId());
    }

    public UserTo getUpdatedBy(CustomerTo to) {
        return customerService.findUpdatedBy(to.getId());
    }

}