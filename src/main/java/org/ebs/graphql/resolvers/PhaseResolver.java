package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.PhaseService;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class PhaseResolver implements GraphQLResolver<PhaseTo> {
    
    private final PhaseService phaseService;

    public HtmlTagTo getHtmlTag(PhaseTo phaseTo) {
        return phaseService.findHtmlTag(phaseTo.getId());
    }

    public WorkflowTo getWorkflow(PhaseTo phaseTo) {
        return phaseService.findWorkflow(phaseTo.getId());
    }

    public WorkflowViewTypeTo getWorkflowViewType(PhaseTo phaseTo) {
        return phaseService.findWorkflowViewType(phaseTo.getId());
    }

    public List<StageTo> getStages(PhaseTo phaseTo) {
        return phaseService.findStages(phaseTo.getId());
    }

    public TenantTo getTenant(PhaseTo phaseTo) {
        return phaseService.findTenant(phaseTo.getId());
    }

}
