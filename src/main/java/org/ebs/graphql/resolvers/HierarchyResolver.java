package org.ebs.graphql.resolvers;

import java.util.List;
import java.util.Set;

import org.ebs.services.HierarchyService;
import org.ebs.services.RoleService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.RoleTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class HierarchyResolver implements GraphQLResolver<HierarchyTo> {

    private final HierarchyService hierarchyService;
    private final RoleService roleService;

    public Contact getContact(HierarchyTo hierarchyTo) {
        return hierarchyService.findContact(hierarchyTo.getContactId());
    }

    public Contact getInstitution(HierarchyTo hierarchyTo) {
        return hierarchyService.findContact(hierarchyTo.getInstitutionId());
    }

    public List<RoleTo> getRoles(HierarchyTo hierarchyTo) {
        return roleService.findByContactHierarchy(hierarchyTo.getId());
    }
}