package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.ContactInfoService;
import org.ebs.services.InstitutionService;
import org.ebs.services.to.Institution;
import org.ebs.services.to.Person;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class PersonResolver implements GraphQLResolver<Person> {

    private final InstitutionService institutionService;
    private final ContactInfoService contactInfoService;

    public List<Institution> getInstitutions(Person person){
        return institutionService.findInstitutionsByContact(person.getContact().getId());
    }

    public String getFullNameWithCoopkey(Person person) {
        String personCoopkey = contactInfoService.getPersonCoopkey(person.getId());
        return person.getFullName() + "(" + personCoopkey + ")";
    }
}
