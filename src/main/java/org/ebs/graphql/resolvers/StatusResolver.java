package org.ebs.graphql.resolvers;

import org.ebs.services.StatusService;
import org.ebs.services.to.StatusTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class StatusResolver implements GraphQLResolver<StatusTo> {
    
    private final StatusService statusService;

    public StatusTypeTo getStatusType(StatusTo statusTo) {
        return statusService.findStatusType(statusTo.getId());
    }

    public WorkflowInstanceTo getWorkflowInstance(StatusTo statusTo) {
        return statusService.findWorkflowInstance(statusTo.getId());
    }

    public TenantTo getTenant(StatusTo statusTo) {
        return statusService.findTenant(statusTo.getId());
    }
}
