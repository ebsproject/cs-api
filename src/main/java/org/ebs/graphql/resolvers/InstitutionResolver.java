package org.ebs.graphql.resolvers;

import graphql.kickstart.tools.GraphQLResolver;

import java.util.List;

import org.ebs.services.ContactService;
import org.ebs.services.InstitutionService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.CropTo;
import org.ebs.services.to.Institution;
import org.ebs.services.to.UnitTypeTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class InstitutionResolver implements GraphQLResolver<Institution> {

    private final ContactService contactService;
    private final InstitutionService institutionService;

    public Contact getPrincipalContact(Institution institution) {
        return institutionService.findPrincipalContact(institution.getId());
    }

    public List<Contact> getContacts(Institution institution) {
        return contactService.findContactsByInstitution(institution.getContact().getId());
    }

    public List<CropTo> getCrops(Institution institution) {
        return institutionService.findInstitutionCrops(institution.getId());
    }

    public UnitTypeTo getUnitType(Institution institution) {
        return institutionService.findUnitType(institution.getId());
    }
}