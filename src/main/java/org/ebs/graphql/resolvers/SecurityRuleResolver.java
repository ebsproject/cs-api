

package org.ebs.graphql.resolvers;

import java.util.Set;
import java.util.List;

import org.ebs.services.SecurityRuleService;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.SecurityRuleTo;
import org.ebs.services.to.UserTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.ebs.services.to.SecurityRuleTo;

import graphql.kickstart.tools.GraphQLResolver;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:24 AM
 */
@Component
@Validated
public class SecurityRuleResolver implements GraphQLResolver<SecurityRuleTo> {

    private SecurityRuleService securityRuleService;

    /**
     *
     * @param securityRuleTo
     */
    // public Set<ProductFunctionTo> getProductfunctions(SecurityRuleTo securityRuleTo) {
    //     return securityRuleService.findProductFunctions(securityRuleTo.getId());
    // }

    /**
     *
     * @param securityRuleTo
     */
    // public Set<UserTo> getUsers(SecurityRuleTo securityRuleTo) {
    //     return securityRuleService.findUsers(securityRuleTo.getId());
    // }

    /**
     *
     * @param securityRuleTo
     */
    // public List<SecurityRuleTo> getRules(SecurityRuleTo securityRuleTo) {
    //     return securityRuleService.findRuleBySecurityRule(securityRuleTo.getId());
    // }

    /**
     *
     * @param securityRuleService
     */
    @Autowired
    public SecurityRuleResolver(SecurityRuleService securityRuleService) {
        this.securityRuleService = securityRuleService;

    }

    public UserTo getCreatedBy(SecurityRuleTo securityRule) {
        return securityRuleService.findCreatedBy(securityRule.getId());
    }

    public UserTo getUpdatedBy(SecurityRuleTo securityRule) {
        return securityRuleService.findUpdatedBy(securityRule.getId());
    }

}