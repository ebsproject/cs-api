package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.PrintoutTemplateService;
import org.ebs.services.ProductService;
import org.ebs.services.TenantService;
import org.ebs.services.to.PrintoutTemplate;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class PrintoutTemplateResolver implements GraphQLResolver<PrintoutTemplate> {

    private final TenantService tenantService;
    private final ProductService productService;
    private final PrintoutTemplateService printoutTemplateService;

    public List<TenantTo> getTenants(PrintoutTemplate printoutTemplate) {
        return tenantService.findByPrintoutTemplate(printoutTemplate.getId());
    }

    public List<ProductTo> getProducts(PrintoutTemplate printoutTemplate) {
        return productService.findByPrintoutTemplate(printoutTemplate.getId());
    }

    public List<ProgramTo> getPrograms(PrintoutTemplate printoutTemplate) {
        return printoutTemplateService.findPrograms(printoutTemplate.getId());
    }

    public UserTo getCreatedBy(PrintoutTemplate printoutTemplate) {
        return printoutTemplateService.findCreatedBy(printoutTemplate.getId());
    }

    public UserTo getUpdatedBy(PrintoutTemplate printoutTemplate) {
        return printoutTemplateService.findUpdatedBy(printoutTemplate.getId());
    }

}