package org.ebs.graphql.resolvers;

import org.ebs.services.CFValueService;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.TenantTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class CFValueResolver implements GraphQLResolver<CFValueTo> {
    
    private final CFValueService cfValueService;

    public NodeCFTo getNodeCF(CFValueTo cfValueTo) {
        return cfValueService.findNodeCF(cfValueTo.getId());
    }

    public EventTo getEvent(CFValueTo cfValueTo) {
        return cfValueService.findEvent(cfValueTo.getId());
    }

    public TenantTo getTenant(CFValueTo cfValueTo) {
        return cfValueService.findTenant(cfValueTo.getId());
    }
}
