package org.ebs.graphql.resolvers;

import graphql.kickstart.tools.GraphQLResolver;

import org.ebs.services.ContactInfoService;
import org.ebs.services.ContactInfoTypeService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactInfo;
import org.ebs.services.to.ContactInfoType;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ContactInfoResolver implements GraphQLResolver<ContactInfo> {

    private final ContactInfoTypeService contactInfoTypeService;
    private final ContactInfoService contactInfoService;

    public ContactInfoType getContactInfoType(ContactInfo contactInfo) {
        return contactInfoTypeService.findByContactInfo(contactInfo.getId());
    }

    public Contact getContact(ContactInfo contactInfo) {
        return contactInfoService.findContactInfoContact(contactInfo.getId());
    }
}