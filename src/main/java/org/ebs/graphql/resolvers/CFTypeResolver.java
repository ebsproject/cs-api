package org.ebs.graphql.resolvers;

import org.ebs.services.to.CFTypeTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class CFTypeResolver implements GraphQLResolver<CFTypeTo> {
    
}
