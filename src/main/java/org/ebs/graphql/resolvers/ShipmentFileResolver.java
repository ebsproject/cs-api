package org.ebs.graphql.resolvers;

import org.ebs.services.ShipmentFileService;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.ShipmentFileTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ShipmentFileResolver implements GraphQLResolver<ShipmentFileTo> {

    private final ShipmentFileService shipmentFileService;

    public FileObjectTo getFile(ShipmentFileTo shipmentFileTo) {
        return shipmentFileService.findFileObject(shipmentFileTo.getId());
    }

    public ShipmentTo getShipment(ShipmentFileTo shipmentFileTo) {
        return shipmentFileService.findShipment(shipmentFileTo.getId());
    }

    public TenantTo getTenant(ShipmentFileTo shipmentFileTo) {
        return shipmentFileService.findTenant(shipmentFileTo.getId());
    }
    
}
