package org.ebs.graphql.resolvers;

import org.ebs.services.ServiceItemService;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class ServiceItemResolver implements GraphQLResolver<ServiceItemTo> {
    
    private final ServiceItemService serviceItemService;

    public ServiceTo getService(ServiceItemTo serviceItemTo) {
        return serviceItemService.findService(serviceItemTo.getId());
    }

    public TenantTo getTenant(ServiceItemTo serviceItemTo) {
        return serviceItemService.findTenant(serviceItemTo.getId());
    }

    public OccurrenceShipmentTo getOccurrenceShipment(ServiceItemTo serviceItemTo) {
        return serviceItemService.findOccurrenceShipment(serviceItemTo.getId());
    }

}
