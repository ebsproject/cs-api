package org.ebs.graphql.resolvers;

import java.util.List;

import org.ebs.services.NodeService;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.NodeTypeTo;
import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class NodeResolver implements GraphQLResolver<NodeTo> {
    
    private final NodeService nodeService;

    public HtmlTagTo getHtmlTag(NodeTo nodeTo) {
        return nodeService.findHtmlTag(nodeTo.getId());
    }

    public ProductTo getProduct(NodeTo nodeTo) {
        return nodeService.findProduct(nodeTo.getId());
    }

    public WorkflowTo getWorkflow(NodeTo nodeTo) {
        return nodeService.findWorkflow(nodeTo.getId());
    }

    public ProcessTo getProcess(NodeTo nodeTo) {
        return nodeService.findProcess(nodeTo.getId());
    }

    public WorkflowViewTypeTo getWorkflowViewType(NodeTo nodeTo) {
        return nodeService.findWorkflowViewType(nodeTo.getId());
    }

    public NodeTypeTo getNodeType(NodeTo nodeTo) {
        return nodeService.findNodeType(nodeTo.getId());
    }

    public List<NodeCFTo> getNodeCFs(NodeTo nodeTo) {
        return nodeService.findNodeCFs(nodeTo.getId());
    }

    public TenantTo getTenant(NodeTo nodeTo) {
        return nodeService.findTenant(nodeTo.getId());
    }
}
