package org.ebs.graphql.resolvers;

import org.ebs.services.StatusTypeService;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class StatusTypeResolver implements GraphQLResolver<StatusTypeTo> {
    
    private final StatusTypeService statusTypeService;

    public WorkflowTo getWorkflow(StatusTypeTo statusTypeTo) {
        return statusTypeService.findWorkflow(statusTypeTo.getId());
    }

    public TenantTo getTenant(StatusTypeTo statusTypeTo) {
        return statusTypeService.findTenant(statusTypeTo.getId());
    }
}
