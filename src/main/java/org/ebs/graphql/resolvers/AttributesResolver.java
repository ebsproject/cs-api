///////////////////////////////////////////////////////////
//  AttributesResolver.java
//  Macromedia ActionScript Implementation of the Class AttributesResolver
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:47 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.graphql.resolvers;

import graphql.kickstart.tools.GraphQLResolver;
import org.ebs.services.to.AttributesTo;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.ebs.services.AttributesService;
import org.ebs.services.to.EntityReferenceTo;
import org.ebs.services.to.HtmlTagTo;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:47 AM
 */
@Component
@Validated
public class AttributesResolver implements GraphQLResolver<AttributesTo> {

    private AttributesService attributesService;

    /**
     *
     * @param attributesService
     */
    @Autowired
    public AttributesResolver(AttributesService attributesService) {
        this.attributesService = attributesService;

    }

    /**
     *
     * @param attributesTo
     */
    public EntityReferenceTo getEntityreference(AttributesTo attributesTo) {
        return attributesService.findEntityReference(attributesTo.getId()).get();
    }

    /**
     *
     * @param attributesTo
     */
    public HtmlTagTo getHtmltag(AttributesTo attributesTo) {
        return attributesService.findHtmlTag(attributesTo.getId()).get();
    }

}