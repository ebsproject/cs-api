package org.ebs.graphql.resolvers;

import java.util.Set;

import org.ebs.services.JobLogService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.TranslationTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component
@Validated
@RequiredArgsConstructor
public class JobLogResolver implements GraphQLResolver<JobLogTo> {

    private final JobLogService jobLogService;

    public Set<Contact> getContacts(JobLogTo jobLogTo) {
        return jobLogService.findContacts(jobLogTo.getId());
    }

    public JobWorkflowTo getJobWorkflow(JobLogTo jobLogTo) {
        return jobLogService.findJobWorkflow(jobLogTo.getId());
    }

    public TranslationTo getTranslation(JobLogTo jobLogTo) {
        return jobLogService.findTranslation(jobLogTo.getId());
    }

    public TenantTo getTenant(JobLogTo jobLogTo) {
        return jobLogService.findTenant(jobLogTo.getId());
    }
}
