///////////////////////////////////////////////////////////
//  FunctionalUnitResolver.java
//  Macromedia ActionScript Implementation of the Class FunctionalUnitResolver
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:00 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.graphql.resolvers;

import java.util.Set;

import org.ebs.services.FunctionalUnitService;
import org.ebs.services.ProductService;
import org.ebs.services.UserService;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.UserTo;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:00 AM
 */
@Component
@Validated
@RequiredArgsConstructor
public class FunctionalUnitResolver implements GraphQLResolver<FunctionalUnitTo> {

    private final FunctionalUnitService functionalUnitService;
    private final UserService userService;
    private final ProductService productService;

    /**
     *
     * @param functionalunitTo
     */
    public Set<UserTo> getUsers(FunctionalUnitTo functionalUnit) {
        return userService.findByFunctionalUnit(functionalUnit.getId());
    }

    Set<FunctionalUnitTo> getSubUnits(FunctionalUnitTo functionalUnit) {
        return functionalUnitService.findByParent(functionalUnit.getId());
    }

    Set<ProductTo> getProducts(FunctionalUnitTo functionalUnit) {
        return productService.findProductsByFunctionalUnit(functionalUnit.getId());
    }
}