package org.ebs.graphql.client;

import java.io.IOException;

import org.ebs.util.brapi.TokenGenerator;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.NonNull;

public class GraphQLClient {
    
    private WebClient webClient;
    protected final TokenGenerator tokenGenerator;
    //private final String variables;

  public GraphQLClient(String url, TokenGenerator tokenGenerator) {
    this.tokenGenerator = tokenGenerator;
    this.webClient = WebClient
    .builder()
    .exchangeStrategies(ExchangeStrategies.builder()
    .codecs(configurer -> configurer
      .defaultCodecs()
      .maxInMemorySize(16 * 1024 * 1024))
    .build())
    .build();
    //this.variables = variables;
  }

  public String getQueryData(@NonNull String url, String query) throws IOException {

    GraphQLRequestBody graphQLRequestBody = new GraphQLRequestBody();

    graphQLRequestBody.setQuery(query);
    //graphQLRequestBody.setVariables(variables);

    return webClient
        .post()
        .uri(url)
        .header("Authorization", "Bearer " + tokenGenerator.getToken())
        .bodyValue(graphQLRequestBody)
        .retrieve()
        .bodyToMono(String.class)
        .block();
  }

}
