package org.ebs.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity @Table(name="contact_workflow", schema="crm")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactWorkflowModel extends Auditable {
	@ManyToOne(fetch=FetchType.LAZY, optional =false)
	@JoinColumn(name="contact_id")
    private ContactModel contact;

	@ManyToOne(fetch=FetchType.LAZY, optional =false)
	@JoinColumn(name="workflow_id")
    private WorkflowModel workflow;

    @Column(name = "provide")
    private boolean provide;

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;
}
