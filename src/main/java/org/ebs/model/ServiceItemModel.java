package org.ebs.model;

import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "items", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServiceItemModel extends Auditable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "service_id")
    private ServiceModel service;

    @Column(name = "germplasm_id")
    private Integer germplasmId;

    @Column(name = "seed_id")
    private Integer seedId;

    @Column(name = "package_id")
    private Integer packageId;

    @Column(name = "number")
    private Integer number;

    @Column(name = "code")
    private String code;

    @Column(name = "status")
    private String status;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "package_unit")
    private String packageUnit;

    @Column(name = "package_count")
    private Integer packageCount;

    @Column(name = "test_code")
    private String testCode;

    @Column(name = "mta_status")
    private String mtaStatus;

    @Column(name = "availability")
    private String availability;

    @Column(name = "use")
    private String use;

    @Column(name = "smta_id")
    private String smtaId;

    @Column(name = "mls_ancestors")
    private String mlsAncestors;

    @Column(name = "genetic_stock")
    private String geneticStock;

    @Column(name = "origin")
    private String origin;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "package_name")
    private String packageName;

    @Column(name = "package_label")
    private String packageLabel;

    @Column(name = "seed_name")
    private String seedName;

    @Column(name = "seed_code")
    private String seedCode;

    @Column(name = "designation")
    private String designation;

    @Column(name = "parentage")
    private String parentage;
    
    @Column(name = "taxonomy_name")
    private String taxonomyName;

    @Column(name = "is_dangerous")
    private Boolean isDangerous;

    @Column(name = "reference_number")
    private String referenceNumber;

    @Column(name = "received_date")
    private Date receivedDate;

    @Column(name = "list_id")
    private Integer listId;

    @Column(name = "germplasm_name")
    private String germplasmName;

    @Column(name = "other_names")
    private String otherNames;

    @Column(name = "source_study")
    private String sourceStudy;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "occurrence_shipment_id")
    private OccurrenceShipmentModel occurrenceShipment;

    @Override
    public String toString() {
        DateTimeFormatter dateFormat = DateTimeFormatter.ISO_LOCAL_DATE.withZone(ZoneOffset.UTC);
        return "{\"id\":" + id + ", \"germplasmId\":" + germplasmId + ", \"seedId\":" + seedId + ", \"packageId\":"
                + packageId + ", \"number\":" + number + ", \"code\":\"" + code + "\", \"status\":\"" + status + "\", \"weight\":" + weight
                + ", \"packageUnit\":\"" + packageUnit + "\", \"packageCount\":" + packageCount + ", \"testCode\":\"" + testCode
                + "\", \"mtaStatus\":\"" + mtaStatus + "\", \"availability\":\"" + availability + "\", \"use\":\"" + use + "\", \"smtaId\":\"" + smtaId
                + "\", \"mlsAncestors\":\"" + mlsAncestors + "\", \"geneticStock\":\"" + geneticStock + "\", \"origin\":\"" + origin
                + "\", \"remarks\":\"" + remarks + "\", \"packageName\":\"" + packageName + "\", \"packageLabel\":\"" + packageLabel
                + "\", \"seedName\":\"" + seedName + "\", \"seedCode\":\"" + seedCode + "\", \"designation\":\"" + designation + "\", \"parentage\":\""
                + parentage + "\", \"taxonomyName\":\"" + taxonomyName + "\", \"isDangerous\":" + isDangerous 
                + ", \"receivedDate\":" + ( receivedDate == null ? "null" : "\"" + dateFormat.format((receivedDate).toInstant()) + "\"") + ", \"referenceNumber\":\""
                + referenceNumber + "\"}";
    }

}
