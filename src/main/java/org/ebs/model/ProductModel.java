package org.ebs.model;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "product", schema = "core")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProductModel extends Auditable {

    private static final long serialVersionUID = -252164331;

    public ProductModel(int id) {
        this.id = id;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String help;
    @Column
    private String icon;
    @Column(name = "menu_order")
    private int menuOrder;
    @Column
    private String path;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    private Set<ProductFunctionModel> productfunctions;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "domain_id")
    @ToString.Exclude
    private DomainModel domain;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "htmltag_id")
    @ToString.Exclude
    private HtmlTagModel htmltag;

    @ManyToMany(mappedBy = "products")
    @ToString.Exclude
    private Set<PrintoutTemplateModel> printoutTemplates;

    @Column
    private String abbreviation;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "entity_reference_id")
    @ToString.Exclude
    private EntityReferenceModel entityReference;

    @Column(name = "has_workflow")
    private Boolean hasWorkflow;

    @Column(name = "is_external")
    private Boolean isExternal;

    @Column(name = "is_redirect")
    private Boolean isRedirect;

    @Column(name = "external_id")
    private Integer externalId;

    public Set<ProductFunctionModel> getProductfunctions() {
        if (productfunctions == null)
            return null;
        return productfunctions.stream().filter(u -> !u.isDeleted()).collect(Collectors.toSet());
    }

}