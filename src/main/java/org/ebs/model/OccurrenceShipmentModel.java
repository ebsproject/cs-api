package org.ebs.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "occurrence_shipment", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OccurrenceShipmentModel extends Auditable {

    private static final long serialVersionUID = 995558905;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "service_id")
    private ServiceModel service;

    @Column(name = "occurrence_id")
    private int occurrenceDbId;

    @Column(name = "occurrence_number")
    private int occurrenceNumber;

    @Column(name = "occurrence_name")
    private String occurrenceName;

    @Column(name = "experiment_name")
    private String experimentName;
   
    @Column(name = "experiment_code")
    private String experimentCode;

    @Column(name = "experiment_year")
    private int experimentYear;

    @Column(name = "experiment_season")
    private String experimentSeason;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipient_id")
    private ContactModel recipient;

    @Column(name = "is_validated")
    private boolean validated;

    @Column
    private Integer weight;

    @Column(name = "test_code")
    private String testCode;

    @Column
    private String status;

    @Column(name = "unit")
    private String unit;

    @OneToMany(mappedBy = "occurrenceShipment", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    private List<ServiceItemModel> items;

    @Column
    private String coopkey;
    
}
