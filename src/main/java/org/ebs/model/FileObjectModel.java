package org.ebs.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.AuditableTenant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "file_object", schema = "core")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class FileObjectModel  extends AuditableTenant{
    
    @Id
    @Column
    private UUID id;
    @Column
    private String name;
    @Column(name="owner_id")
    private int ownerId;
    private long size;
    private String tags;
    private String description;
    private int status;
    private int tenantId;
}
