package org.ebs.model;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "event", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EventModel extends Auditable {
    
    private static final long serialVersionUID = 993258894;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name="completed")
	private Date completed;

    @Column(name = "error")
    private String error;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stage_id")
    private StageModel stage;

    @Column(name = "instance_id")
    private Integer recordId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "node_id")
    private NodeModel node;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wf_instance_id")
    private WorkflowInstanceModel workflowInstance;

    @OneToMany(mappedBy = "event", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<CFValueModel> cfValues;

    public Set<CFValueModel> getStages() {
        return cfValues.stream().filter(c -> !c.isDeleted()).collect(Collectors.toSet());
    }

}
