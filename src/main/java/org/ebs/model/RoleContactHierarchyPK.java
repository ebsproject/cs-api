package org.ebs.model;

import java.io.Serializable;

public class RoleContactHierarchyPK implements Serializable {
    
    protected int role;
    protected int hierarchy;

     public RoleContactHierarchyPK(){}

     public RoleContactHierarchyPK(RoleModel role,HierarchyModel hierarchy){
         this.role=role.getId();
         this.hierarchy=hierarchy.getId();
     }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + role;
        result = prime * result + hierarchy;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RoleContactHierarchyPK other = (RoleContactHierarchyPK) obj;
        if (role != other.role)
            return false;
        if (hierarchy != other.hierarchy)
            return false;
        return true;
    }

}
