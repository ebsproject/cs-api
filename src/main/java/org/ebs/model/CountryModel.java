package org.ebs.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "country", schema = "crm")
@Getter
@Setter
public class CountryModel extends Auditable {

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;

    @Column
    private String name;

    @Column
    private String iso;

}