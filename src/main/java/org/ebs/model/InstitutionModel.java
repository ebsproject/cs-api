package org.ebs.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "institution", schema = "crm")
@Getter
@Setter
public class InstitutionModel extends Auditable {

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "common_name")
    private String commonName;

    @Column(name = "legal_name")
    private String legalName;

    @OneToOne(optional = false)
    @JoinColumn(name = "contact_id", unique = true, nullable = false, updatable = false)
    private ContactModel contact;

    @Column(name = "external_code")
    private Integer externalCode;

    @Column(name = "is_cgiar")
    private boolean isCgiar;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    @JoinTable(name = "institution_crop", schema = "crm", joinColumns = @JoinColumn(name = "institution_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "crop_id", referencedColumnName = "id"))
    private List<CropModel> crops = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_type_id")
    private UnitTypeModel unitType;
}