package org.ebs.model;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "shipment", schema = "shm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentModel extends Auditable {
    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "wf_instance_id")
    private WorkflowInstanceModel workflowInstance;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "program_id")
    private ProgramModel program;
    
    @Column(name = "project_id")
    private Integer projectId;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sender_id")
    private ContactModel sender;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "processor_id")
    private ContactModel processor;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "requester_id")
    private ContactModel requester;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipient_id")
    private ContactModel recipient;
    
    @Column(name = "code")
    private String code;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "shu_reference_number")
    private String shuReferenceNumber;
    
    @Column(name = "purpose")
    private String purpose;
    
    @Column(name = "material_type")
    private String materialType;
    
    @Column(name = "material_subtype")
    private String materialSubtype;
    
    @Column(name = "document_generated_date")
    private Date documentGeneratedDate;
    
    @Column(name = "document_signed_date")
    private Date documentSignedDate;
    
    @Column(name = "authorized_signatory")
    private String authorizedSignatory;
    
    @Column(name = "shipped_date")
    private Date shippedDate;
    
    @Column(name = "received_date")
    private Date receivedDate;
    
    @Column(name = "airway_bill_number")
    private String airwayBillNumber;
    
    @Column(name = "remarks")
    private String remarks;

    @Column(name = "recipient_institution")
    private String recipientInstitution;

    @Column(columnDefinition = "jsonb", name = "institution_data")
	private JsonNode institutionData;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private AddressModel address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "requestor_address_id")
    private AddressModel requestorAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_address_id")
    private AddressModel senderAddress;

    @OneToMany(mappedBy = "shipment", fetch = LAZY, cascade = ALL)
    @ToString.Exclude
    private List<ShipmentItemModel> items;
}
