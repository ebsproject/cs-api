package org.ebs.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "filter", schema = "core")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilterModel extends Auditable {

    private static final long serialVersionUID = -403056820;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "is_graph_service")
    private Boolean isGraphService;

    @Column(name = "api_url")
    private String apiUrl;

    @Column(name = "param_path")
    private String paramPath;

    @Column(name = "body")
    private String body;

    @Column(name = "param_query")
    private String paramQuery;

    @Column(name = "method")
    private String method;

    @Column(name = "is_system")
    private Boolean isSystem;

    @Column(name = "tooltip")
    private String tooltip;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "filter_domain", schema = "core", 
        joinColumns = @JoinColumn(name = "filter_id", referencedColumnName = "id"), 
        inverseJoinColumns = @JoinColumn(name = "domain_id", referencedColumnName = "id")
    )
    private List<DomainModel> domains;

}
