
package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Lob;

import org.ebs.util.Auditable;
import org.hibernate.annotations.Type;

import lombok.Getter;
import lombok.Setter;

@Entity @Table(name="organization",schema="core")
@Getter @Setter
public class OrganizationModel extends Auditable {

	private static final long serialVersionUID = 311396891;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@Column(name="legal_name")
	private String legalName;
	@Column(name="phone")
	private String phone;
	@Column(name="web_page")
	private String webPage;
	@Column(name="slogan")
	private String slogan;
	@Column(name="name")
	private String name;
	@Column(name="tax_id")
	private String taxId;
	@Lob
	@Type(type="org.hibernate.type.BinaryType")
	@Column(name="logo")
	private byte[] logo;
	@Column(name="is_active")
	private boolean active;
	@Column(name="default_authentication")
	private Integer defaultAuthenticationId;
	@Column(name="default_theme")
	private Integer defaultThemeId;
	@Column(name="code")
	private String code;
	@OneToMany(mappedBy = "organization",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<TenantModel> tenants;
	@ManyToOne(fetch=FetchType.LAZY, optional =false) @JoinColumn(name="customer_id")
	private CustomerModel owningCustomer;
	@OneToMany(mappedBy = "owningOrganization",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<CustomerModel> ownedCustomers;
	@ManyToOne(fetch=FetchType.LAZY, optional =false) @JoinColumn(name="address_id")
	private AddressModel address;
}