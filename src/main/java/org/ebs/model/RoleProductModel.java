package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 

@Entity @Table(name="role_product_function",schema="security")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleProductModel extends Auditable {

	private static final long serialVersionUID = -343810207;

	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true)
	@JoinColumn(name="role_id", referencedColumnName = "id")
	private RoleModel role;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true)
	@JoinColumn(name="product_function_id", referencedColumnName = "id")
	private ProductFunctionModel productFunction;

}