package org.ebs.model.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// import io.swagger.annotations.ApiModel;
// import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
// @Api(description = "Object return from a page with pagination service from B4R")
public class BreedingResponse {

    // @ApiModelProperty(name= "Total of pages", example= "10", notes = "Total fo page merge on the endpoint")
    private int totalPage;
    // @ApiModelProperty(name="Data", notes = "All data from all page")
    private List<Map<String, Object>> data;
    // @ApiModelProperty(name = "Error message", notes ="Display error from server")
    private String error;

    public void AddData(Map<String, Object> item){
        if(data == null){
            data = new ArrayList<Map<String, Object>>();
        }
        data.add(item);
    }
}
