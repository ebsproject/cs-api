package org.ebs.model;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "stage", schema = "workflow")
@TypeDef(
    typeClass = JsonType.class,
    defaultForType = JsonNode.class
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StageModel extends Auditable {
    
    private static final long serialVersionUID = 993258892;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "help")
    private String help;

    @Column(name = "sequence")
    private Integer sequence;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private StageModel parent;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "htmltag_id")
    private HtmlTagModel htmlTag;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "phase_id")
    private PhaseModel phase;

    @Column(columnDefinition = "jsonb", name = "depend_on")
	private JsonNode dependOn;

    @Column
    private String icon;

    @Column(name = "design_ref")
    @Type(type="pg-uuid")
    private UUID designRef;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "wf_view_type_id")
    private WorkflowViewTypeModel workflowViewType;

    @ManyToMany(mappedBy = "stages")
    private Set<NodeModel> nodes = new HashSet<>();

}
