package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "job_type", schema = "core")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobTypeModel extends Auditable {

    private static final long serialVersionUID = -358073001;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "tenant_id")
    private int tenantId;

    @OneToMany(mappedBy = "jobType",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<JobWorkflowModel> jobWorkflows;

}
