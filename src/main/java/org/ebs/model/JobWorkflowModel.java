package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "job_workflow", schema = "core")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobWorkflowModel extends Auditable {

    private static final long serialVersionUID = -358073003;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "job_type_id")
    private JobTypeModel jobType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_function_id")
    private ProductFunctionModel productFunction;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "translation_id")
    private TranslationModel translation;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "request_code")
    private Integer requestCode;

    @Column(name = "notes")
    private String notes;

    @Column(name = "tenant_id", nullable = false)
    private Integer tenantId;

    @OneToMany(mappedBy = "jobWorkflow",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<JobLogModel> jobLogs;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "email_template_id")
    private EmailTemplateModel emailTemplate;

}
