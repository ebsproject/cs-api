package org.ebs.model;

import java.io.Serializable;

public class HierarchyPK implements Serializable {
    protected int contact;
    protected int institution;

     public HierarchyPK(){}

     public HierarchyPK(ContactModel contact,ContactModel institution){
         this.contact=contact.getId();
         this.institution=institution.getId();
     }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + contact;
        result = prime * result + institution;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HierarchyPK other = (HierarchyPK) obj;
        if (contact != other.contact)
            return false;
        if (institution != other.institution)
            return false;
        return true;
    }
 
}
