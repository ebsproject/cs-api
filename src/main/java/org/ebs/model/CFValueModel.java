package org.ebs.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "cf_value", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CFValueModel extends Auditable {

    private static final long serialVersionUID = 993258904;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "flag_value")
    private Boolean flagValue;
    
    @Column(name = "text_value")
    private String textValue;

    @Column(name = "num_value")
    private Integer numValue;

    @Column(name = "date_value")
    private Date dateValue;

    @Column(name = "code_value")
    private Integer codeValue;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "nodecf_id")
    private NodeCFModel nodeCF;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "event_id")
    private EventModel event;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;
    
}
