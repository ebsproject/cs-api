package org.ebs.model;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "node", schema = "workflow")
@TypeDef(
    typeClass = JsonType.class,
    defaultForType = JsonNode.class
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NodeModel extends Auditable {

    private static final long serialVersionUID = 993258902;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column
    private String name;
    
    @Column
    private String description;

    @Column
    private String help;

    @Column
    private Integer sequence;

    @Column(name = "require_approval")
    private Boolean requireApproval;

	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="htmltag_id")
	private HtmlTagModel htmltag;

	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="product_id")
	private ProductModel product;

	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="workflow_id")
	private WorkflowModel workflow;

	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="process_id")
	private ProcessModel process;

    @Column(columnDefinition = "jsonb", name = "define")
	private JsonNode define;

    @Column(columnDefinition = "jsonb", name = "depend_on")
	private JsonNode dependOn;
    
    @Column
    private String icon;
    
    @Column(columnDefinition = "jsonb", name = "message")
	private JsonNode message;

    @Column(columnDefinition = "jsonb", name = "require_input")
	private JsonNode requireInput;

    @Column(columnDefinition = "jsonb", name = "security_definition")
	private JsonNode securityDefinition;

    @Column(name = "validation_code")
    private String validationCode;

    @Column(name = "validation_type")
    private String validationType;

    @Column(name = "design_ref")
    @Type(type="pg-uuid")
    private UUID designRef;

	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="wf_view_type_id")
	private WorkflowViewTypeModel workflowViewType;

    @ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="node_type_id")
	private NodeTypeModel nodeType;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "node_stage", 
        schema = "workflow", 
        joinColumns = @JoinColumn(name = "node_id", referencedColumnName = "id"), 
        inverseJoinColumns = @JoinColumn(name = "stage_id", referencedColumnName = "id")
    )
    private Set<StageModel> stages;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @OneToMany(mappedBy = "node", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<NodeCFModel> nodeCFs = new HashSet<>();

    public Set<NodeCFModel> getNodeCFs() {
        return nodeCFs.stream().filter(n -> !n.isDeleted()).collect(Collectors.toSet());
    }
    
}
