package org.ebs.model.parameters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OtherParameters{
    private String new_status;
    private String title;
    private String message;
    private String url_response;
    private CustomData custom_data;
}
