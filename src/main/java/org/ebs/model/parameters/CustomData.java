package org.ebs.model.parameters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomData {
    private String batch_code;
    private String well_position;
    private String sample_code;
}
