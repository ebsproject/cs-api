package org.ebs.model.parameters;

// import io.swagger.annotations.ApiModel;
// import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
// @ApiModel(description = "Parameter to util/pagination/remove")
public class PaginationRemoveGetParameter {

    // @ApiModelProperty(notes = "URL from endpoint paginate", example = "https://server/v1/resource?parameter1=1")
    private String paginatedURL;
    
}
