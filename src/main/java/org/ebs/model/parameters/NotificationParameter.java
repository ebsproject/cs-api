package org.ebs.model.parameters;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationParameter {
    
    private int job_workflow_id;
    private int submitter_id;
    private int record_id;
    private OtherParameters other_parameters;
}
