///////////////////////////////////////////////////////////
//  SeasonModel.java
//  Macromedia ActionScript Implementation of the Class SeasonModel
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:25 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:25 AM
 */
@Entity @Table(name="season",schema="program") @Getter @Setter
public class SeasonModel extends Auditable {

	private static final long serialVersionUID = 392635500;
	@Column(name="tenant_id")
	private int tenant;
	@Column(name="code")
	private String code;
	@Column(name="name")
	private String name;
	@Column(name="description")
	private String description;
	@Column(name="notes")
	private String notes;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;

	@Override
	public String toString(){
		return "SeasonModel [id=" + id + ",]";
	}

}