package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity @Table(name="contact_address", schema="crm")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(ContactAddressPK.class)
public class ContactAddressModel {
    @Id 
	@ManyToOne(fetch=FetchType.LAZY, optional =false)
	@JoinColumn(name="contact_id", referencedColumnName = "id")
    private ContactModel contact;

    @Id 
	@ManyToOne(fetch=FetchType.LAZY, optional =false)
	@JoinColumn(name="address_id", referencedColumnName = "id")
    private AddressModel address;

    @Column(name = "is_default")
    private boolean isDefault;
}
