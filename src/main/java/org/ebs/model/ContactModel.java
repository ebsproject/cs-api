package org.ebs.model;

import static java.util.stream.Collectors.toList;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "contact", schema = "crm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactModel extends Auditable {

    public ContactModel(int id) {
        this.id = id;
    }

    public ContactModel(int id, CategoryModel category, List<ContactTypeModel> contactTypes,
            List<AddressModel> addresses, List<ContactInfoModel> contactInfos, PersonModel person,
            InstitutionModel institution, String email, Set<PurposeModel> purposes,
            List<HierarchyModel> parents, List<HierarchyModel> members,
            Set<FunctionalUnitModel> functionalUnits, Set<TenantModel> tenants, Set<JobLogModel> jobLogs,
            Integer externalId, Set<UserModel> users, Set<ContactAddressModel> contactAddresses) {
        this.id = id;
        this.category = category;
        this.contactTypes = contactTypes;
        this.addresses = addresses;
        this.contactInfos = contactInfos;
        this.person = person;
        this.institution = institution;
        this.email = email;
        this.purposes = purposes;
        this.parents = parents;
        this.members = members;
        this.functionalUnits = functionalUnits;
        this.tenants = tenants;
        this.jobLogs = jobLogs;
        this.externalId = externalId;
        this.users = users;
        this.contactAddresses = contactAddresses;
    }

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private CategoryModel category;

    @ManyToMany(cascade = ALL)
    @JoinTable(name = "contact_contact_type", schema = "crm", joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "contact_type_id", referencedColumnName = "id"))
    private List<ContactTypeModel> contactTypes;

    @ManyToMany(cascade = ALL)
    @JoinTable(name = "contact_address", schema = "crm", joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "id"))
    private List<AddressModel> addresses = new ArrayList<>();

    @OneToMany(mappedBy = "contact", fetch = LAZY, cascade = ALL)
    private List<ContactInfoModel> contactInfos = new ArrayList<>();

    @ManyToMany(mappedBy="programs")
	@ToString.Exclude
	private Set<PrintoutTemplateModel> printoutTemplates;

    @OneToOne(optional = true, mappedBy = "contact")
    public PersonModel person;

    @OneToOne(optional = true, mappedBy = "contact")
    public InstitutionModel institution;

    @Column
    private String email;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "contact_purpose", schema = "crm", joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "purpose_id", referencedColumnName = "id"))
    private Set<PurposeModel> purposes = new HashSet<>();

    @OneToMany(mappedBy = "contact", fetch = LAZY, cascade = ALL)
    private List<HierarchyModel> parents;

    @OneToMany(mappedBy = "institution", fetch = LAZY, cascade = ALL)
    private List<HierarchyModel> members;

    @ManyToMany(mappedBy = "contacts")
    private Set<FunctionalUnitModel> functionalUnits;

    @ManyToMany(mappedBy = "contacts")
    private Set<TenantModel> tenants = new HashSet<>();

    @ManyToMany(mappedBy = "contacts")
    private Set<JobLogModel> jobLogs;

    @Column(name = "external_id")
    private Integer externalId;

    @OneToMany(mappedBy = "contact", fetch = LAZY, cascade = ALL)
    private Set<UserModel> users;

    @OneToMany(mappedBy = "contact", fetch = LAZY, cascade = ALL)
    private Set<ContactAddressModel> contactAddresses;

    @OneToMany(mappedBy = "sender", fetch = LAZY, cascade = ALL)
    private Set<ShipmentModel> shipmentsAsSender;

    @OneToMany(mappedBy = "processor", fetch = LAZY, cascade = ALL)
    private Set<ShipmentModel> shipmentsAsProcessor;

    @OneToMany(mappedBy = "requester", fetch = LAZY, cascade = ALL)
    private Set<ShipmentModel> shipmentsAsRequester;

    @OneToMany(mappedBy = "recipient", fetch = LAZY, cascade = ALL)
    private Set<ShipmentModel> shipmentsAsRecipient;

    @ManyToMany(cascade = {PERSIST, DETACH})
    @JoinTable(name = "contact_workflow", schema = "crm", joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "workflow_id", referencedColumnName = "id"))
    private List<WorkflowModel> workflows = new ArrayList<>();

    @OneToMany(mappedBy = "contact", fetch = LAZY, cascade = ALL)
    private List<ContactWorkflowModel> contactWorkflows;

    public void setAddresses(List<AddressModel> addresses) {
        this.addresses.addAll(addresses);
    }

    public List<AddressModel> getAddresses() {
        return addresses.stream().filter(a -> !a.isDeleted()).collect(toList());
    }

    /* public List<ContactInfoModel> getContactInfos() {
        return contactInfos.stream().filter(c -> !c.isDeleted()).collect(toList());
    } */

    public Set<UserModel> getUsers() {
        return users.stream().filter(u -> !u.isDeleted()).collect(Collectors.toSet());
    }

    public List<ContactWorkflowModel> getContactWorkflows() {
        return contactWorkflows.stream().filter(cw -> !cw.getWorkflow().isDeleted()).collect(Collectors.toList());
    }

    public void setContactInfos(List<ContactInfoModel> contactInfos) {
        this.contactInfos.addAll(contactInfos);
    }

    public boolean contains(HierarchyModel orElse) {
        return false;
    }

}
