package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ProductFunctionModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductFunctionRepository
        extends JpaRepository<ProductFunctionModel, Integer>, RepositoryExt<ProductFunctionModel> {

    /**
     *
     * @param productId
     */
    public List<ProductFunctionModel> findByProductIdAndDeletedIsFalse(int productId);
    
    /**
     *
     * @param productFunctionId
     */
   public  Optional<ProductFunctionModel> findByIdAndDeletedIsFalse (int productFunctionId);

}