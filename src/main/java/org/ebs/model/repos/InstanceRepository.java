///////////////////////////////////////////////////////////
//  InstanceRepository.java
//  Macromedia ActionScript Implementation of the Interface InstanceRepository
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:03 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.model.repos;

import org.ebs.model.InstanceModel;
import org.ebs.util.RepositoryExt;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:03 AM
 */
public interface InstanceRepository extends RepositoryExt<InstanceModel>, JpaRepository<InstanceModel,Integer> {

	/**
	 * 
	 * @param tenantId
	 */
	public List<InstanceModel> findByTenantIdAndDeletedIsFalse(int tenantId);

	public Optional<InstanceModel> findByIdAndDeletedIsFalse(int instanceId);
}