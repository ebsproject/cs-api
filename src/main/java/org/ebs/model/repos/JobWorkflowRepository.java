package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.JobWorkflowModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobWorkflowRepository extends JpaRepository<JobWorkflowModel,Integer>, RepositoryExt<JobWorkflowModel> {
    Optional<JobWorkflowModel> findByIdAndDeletedIsFalse(int jobWorkflowId);

    @Override
    default Optional<JobWorkflowModel> findById(Integer jobWorkflowId) {
        return findByIdAndDeletedIsFalse(jobWorkflowId);
    }
}
