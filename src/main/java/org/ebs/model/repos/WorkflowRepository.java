package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.WorkflowModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


public interface WorkflowRepository extends JpaRepository<WorkflowModel, Integer>, RepositoryExt<WorkflowModel> {

    Optional<WorkflowModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<WorkflowModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    List<WorkflowModel> findByName(String name);
    
}