package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.OccurrenceShipmentModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface OccurrenceShipmentRepository extends JpaRepository<OccurrenceShipmentModel, Integer>, RepositoryExt<OccurrenceShipmentModel> {
    
    Optional<OccurrenceShipmentModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<OccurrenceShipmentModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    @Modifying
    @Query("delete from OccurrenceShipmentModel o where o in ?1")
    void deleteByList(List<OccurrenceShipmentModel> list);
}