package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.ShipmentModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentRepository extends JpaRepository<ShipmentModel, Integer>, RepositoryExt<ShipmentModel> {

    Optional<ShipmentModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<ShipmentModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }
    
}
