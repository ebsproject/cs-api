package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactTypeRepository
        extends JpaRepository<ContactTypeModel, Integer>, RepositoryExt<ContactTypeModel> {

    /**
     * Finds the types of a given contact
     * <p>
     * Note: use the shorter method findByContact()
     * </p>
     *
     * @param contactId
     * @return types of the contact
     */
    List<ContactTypeModel> findByContactsIdAndContactsDeletedIsFalseAndDeletedIsFalse(
            int contactId);

    /**
     * Finds the types of a given contact
     *
     * @param contactId
     * @return types of the contact
     */
    default List<ContactTypeModel> findByContact(int contactId) {
        return findByContactsIdAndContactsDeletedIsFalseAndDeletedIsFalse(contactId);
    }

    List<ContactTypeModel> findByIdInAndDeletedIsFalse(List<Integer> ids);

    default List<ContactTypeModel> findByIds(List<Integer> ids) {
        return findByIdInAndDeletedIsFalse(ids);
    }

    /**
     * Finds a ContactType
     * <p>
     * Note: use the shorter method findById()
     * </p>
     *
     * @param contactTypeId
     * @return Optional with the ContactType, empty if not found
     */
    Optional<ContactTypeModel> findByIdAndDeletedIsFalse(int contactTypeId);

    /**
     * Finds a ContactType
     *
     * @param contactTypeId
     * @return Optional with the ContactType, empty if not found
     */
    @Override
    default Optional<ContactTypeModel> findById(Integer contactTypeId) {
        return findByIdAndDeletedIsFalse(contactTypeId);
    }

    List<ContactTypeModel> findByNameAndDeletedIsFalse(String name);

}