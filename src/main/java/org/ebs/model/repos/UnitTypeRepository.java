package org.ebs.model.repos;

import org.ebs.model.UnitTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnitTypeRepository extends JpaRepository<UnitTypeModel,Integer>, RepositoryExt<UnitTypeModel> {
    
}
