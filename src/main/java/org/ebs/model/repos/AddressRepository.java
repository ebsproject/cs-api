package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.AddressModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository
        extends JpaRepository<AddressModel, Integer>, RepositoryExt<AddressModel> {

    /**
     * Finds the addresses of a given contact
     * <p>
     * Note: use the shorter method findByContact()
     * </p>
     *
     * @param contactId
     * @return address list of the contact
     */
    List<AddressModel> findByContactsIdAndContactsDeletedIsFalseAndDeletedIsFalse(int contactId);

    /**
     * Finds the addresses of a given contact
     *
     * @param contactId
     * @return address list of the contact
     */
    default List<AddressModel> findByContact(int contactId) {
        return findByContactsIdAndContactsDeletedIsFalseAndDeletedIsFalse(contactId);
    }

    Optional<AddressModel> findByCustomersIdAndCustomersDeletedIsFalseAndDeletedIsFalse(int customerId);

    default Optional<AddressModel> findByCustomer(int customerId) {
        return findByCustomersIdAndCustomersDeletedIsFalseAndDeletedIsFalse(customerId);
    }

    Optional<AddressModel> findByOrganizationsIdAndOrganizationsDeletedIsFalseAndDeletedIsFalse(int organizationId);

    default Optional<AddressModel> findByOrganization(int organizationId) {
        return findByOrganizationsIdAndOrganizationsDeletedIsFalseAndDeletedIsFalse(organizationId);
    }

    /**
     * Finds an address
     * <p>
     * Note: use the shorter method findById()
     * </p>
     *
     * @param addressId
     * @return optional with the address, empty if not found
     */
    Optional<AddressModel> findByIdAndDeletedIsFalse(int addressId);

    /**
     * Finds an address
     *
     * @param addressId
     * @return optional with the address, empty if not found
     */
    @Override
    default Optional<AddressModel> findById(Integer addressId) {
        return findByIdAndDeletedIsFalse(addressId);
    }
}