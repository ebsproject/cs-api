package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.JobLogModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Date;


public interface JobLogRepository extends JpaRepository<JobLogModel,Integer>, RepositoryExt<JobLogModel> {
    Optional<JobLogModel> findByIdAndDeletedIsFalse(int jobLogId);

    @Override
    default Optional<JobLogModel> findById(Integer jobLogId) {
        return findByIdAndDeletedIsFalse(jobLogId);
    }

    List<JobLogModel> findByStartTimeBefore(Date startTime);
}
