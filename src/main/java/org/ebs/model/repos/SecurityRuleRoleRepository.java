package org.ebs.model.repos;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import org.ebs.model.SecurityRuleRoleModel;
import org.ebs.util.RepositoryExt;
public interface SecurityRuleRoleRepository
    extends JpaRepository<SecurityRuleRoleModel, Integer>, RepositoryExt<SecurityRuleRoleModel> {
  public List<SecurityRuleRoleModel> findByRoleId(int roleId);
  public List<SecurityRuleRoleModel> findByRoleIdAndRuleId(int ruleId, int roleId);
  public List<SecurityRuleRoleModel> findByRuleId(int ruleId);
}