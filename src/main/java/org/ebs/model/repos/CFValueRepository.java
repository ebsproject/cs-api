package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.CFValueModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CFValueRepository extends JpaRepository<CFValueModel, Integer>, RepositoryExt<CFValueModel> {
    
    Optional<CFValueModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<CFValueModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    List<CFValueModel> findByNodeCFIdAndDeletedIsFalse(int nodeCFId);

    default List<CFValueModel> findByNodeCFId(int nodeCFId) {
        return findByNodeCFIdAndDeletedIsFalse(nodeCFId);
    }

    List<CFValueModel> findByEventIdAndDeletedIsFalse(int eventId);

    default List<CFValueModel> findByEventId(int eventId) {
        return findByEventIdAndDeletedIsFalse(eventId);
    }

}
