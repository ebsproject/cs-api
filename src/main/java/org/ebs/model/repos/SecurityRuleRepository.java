package org.ebs.model.repos;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

import org.ebs.model.SecurityRuleModel;
import org.ebs.util.RepositoryExt;
public interface SecurityRuleRepository
    extends JpaRepository<SecurityRuleModel, Integer>, RepositoryExt<SecurityRuleModel> {

        default Optional<SecurityRuleModel> findByName(String name) {
            return findByNameIgnoreCaseAndDeletedIsFalse(name);
        }
    
        Optional<SecurityRuleModel> findByNameIgnoreCaseAndDeletedIsFalse(String name);
}