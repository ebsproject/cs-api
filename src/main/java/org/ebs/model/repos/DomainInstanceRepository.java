///////////////////////////////////////////////////////////
//  DomainInstanceRepository.java
//  Macromedia ActionScript Implementation of the Interface DomainInstanceRepository
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:54 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.DomainInstanceModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:54 AM
 */
public interface DomainInstanceRepository extends JpaRepository<DomainInstanceModel,Integer>, RepositoryExt<DomainInstanceModel> {

	/**
	 * 
	 * @param domainId
	 */
	public List<DomainInstanceModel> findByDomainIdAndDeletedIsFalse(int domainId);

	/**
	 * 
	 * @param instanceId
	 */
	public List<DomainInstanceModel> findByInstanceIdAndDeletedIsFalse(int instanceId);

	public Optional<DomainInstanceModel> findByInstanceIdAndDomainIdAndDeletedIsFalse(int instanceId, int domainId);
	public Optional<DomainInstanceModel> findByIdAndDeletedIsFalse(int domainInstanceId);

}