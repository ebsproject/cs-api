package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.NodeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.UUID;


public interface NodeRepository extends JpaRepository<NodeModel, Integer>, RepositoryExt<NodeModel> {
    
    Optional<NodeModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<NodeModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    List<NodeModel> findByStagesIdAndStagesDeletedIsFalseAndDeletedIsFalse(int stageId);

    default List<NodeModel> findByStage(int stageId) {
        return findByStagesIdAndStagesDeletedIsFalseAndDeletedIsFalse(stageId);
    }

    List<NodeModel> findByStagesPhaseWorkflowIdAndStagesDeletedIsFalseAndStagesPhaseDeletedIsFalseAndStagesPhaseWorkflowDeletedIsFalseOrderByStagesPhaseSequenceAscStagesSequenceAscSequenceAsc(int workflowId);

    default List<NodeModel> findWorkflowNodes(int workflowId) {
        return findByStagesPhaseWorkflowIdAndStagesDeletedIsFalseAndStagesPhaseDeletedIsFalseAndStagesPhaseWorkflowDeletedIsFalseOrderByStagesPhaseSequenceAscStagesSequenceAscSequenceAsc(workflowId);
    }

    List<NodeModel> findByDesignRef(UUID designRef);
}
