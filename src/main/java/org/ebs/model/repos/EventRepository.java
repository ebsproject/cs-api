package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.EventModel;
import org.ebs.model.NodeModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<EventModel, Integer>, RepositoryExt<EventModel> {

    Optional<EventModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<EventModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }
    
    List<EventModel> findByWorkflowInstanceAndDeletedIsFalse(WorkflowInstanceModel workflowInstance);

    List<EventModel> findByWorkflowInstanceAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAscNodeSequenceAsc(WorkflowInstanceModel workflowInstance);

    List<EventModel> findByWorkflowInstanceAndCompletedIsNullAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAsc(WorkflowInstanceModel workflowInstance);

    List<EventModel> findByWorkflowInstanceAndCompletedIsNullAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAscNodeSequenceAsc(WorkflowInstanceModel workflowInstance);

    List<EventModel> findByWorkflowInstanceAndCompletedNotNullAndDeletedIsFalseOrderByStagePhaseSequenceDescStageSequenceDesc(WorkflowInstanceModel workflowInstance);

    List<EventModel> findByWorkflowInstanceAndCompletedNotNullAndDeletedIsFalseOrderByStagePhaseSequenceDescStageSequenceDescNodeSequenceDesc(WorkflowInstanceModel workflowInstance);

    List<EventModel> findByRecordIdAndCompletedIsNullAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAsc(int recordId);

    Optional<EventModel> findByNodeAndWorkflowInstanceAndDeletedIsFalse(NodeModel node, WorkflowInstanceModel workflowInstance);

}
