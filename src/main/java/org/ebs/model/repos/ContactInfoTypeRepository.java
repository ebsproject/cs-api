package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.ContactInfoTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


public interface ContactInfoTypeRepository
        extends JpaRepository<ContactInfoTypeModel, Integer>, RepositoryExt<ContactInfoTypeModel> {

    /**
     * Finds a ContactInfoType
     * <p>
     * Note: use the shorter method findById()
     * </p>
     *
     * @param infoTypeId
     * @return optional of ContactInfoType or empty if not found
     */
    Optional<ContactInfoTypeModel> findByIdAndDeletedIsFalse(int infoTypeId);

    /**
     * Finds a ContactInfoType
     *
     * @param infoTypeId
     * @return optional of ContactInfoType or empty if not found
     */
    @Override
    default Optional<ContactInfoTypeModel> findById(Integer infoTypeId) {
        return findByIdAndDeletedIsFalse(infoTypeId);
    }

    List<ContactInfoTypeModel> findByNameIgnoreCase(String name);
}