package org.ebs.model.repos;

import java.util.Optional;
import java.util.UUID;

import org.ebs.model.FileObjectModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileObjectRepository
        extends JpaRepository<FileObjectModel, UUID>, RepositoryExt<FileObjectModel> {
    
    public Optional<FileObjectModel> findByIdAndDeletedIsFalse(UUID id);
    
    @Override
    default Optional<FileObjectModel> findById(UUID id) {
        return findByIdAndDeletedIsFalse(id);
    }

    public Optional<FileObjectModel> findByTenantIdAndIdAndDeletedIsFalse(int tenantId, UUID id);

    default Optional<FileObjectModel> findById(int tenantId, UUID id) {
        return findByTenantIdAndIdAndDeletedIsFalse(tenantId, id);
    }

}
