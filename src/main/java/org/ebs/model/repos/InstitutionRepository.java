package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.InstitutionModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstitutionRepository
        extends JpaRepository<InstitutionModel, Integer>, RepositoryExt<InstitutionModel> {

    /**
     * Finds an institution
     * <p>
     * Note: use the shorter method findById()
     * </p>
     *
     * @param institutionId
     * @return and optional of the institution. Empty if not found
     */
    Optional<InstitutionModel> findByIdAndDeletedIsFalse(int institutionId);

    /**
     * Finds an institution
     *
     * @param institutionId
     * @return and optional of the institution. Empty if not found
     */
    @Override
    default Optional<InstitutionModel> findById(Integer institutionId) {
        return findByIdAndDeletedIsFalse(institutionId);
    }
        
    /**
     * Check if institution already exists
     *
     * @param legalName
     * 
     */
   public int countByLegalNameIgnoreCaseAndDeletedIsFalse(String legalName);

   public int countByCommonNameIgnoreCaseAndDeletedIsFalse(String commonName);

   public List<InstitutionModel> findByExternalCodeAndDeletedIsFalse(int externalCode);

   List<InstitutionModel> findByLegalNameAndDeletedIsFalse(String legalName);

   List<InstitutionModel> findByExternalCode(Integer externalCode);

   List<InstitutionModel> findByExternalCodeAndUnitTypeName(Integer externalCode, String unitTypeName);

   List<InstitutionModel> findByCommonNameAndDeletedIsFalse(String commonName);

   List<InstitutionModel> findByCommonName(String commonName);

   List<InstitutionModel> findByCommonNameAndUnitTypeNameAndDeletedIsFalse(String commonName, String unitTypeName);

   List<InstitutionModel> findByCommonNameAndUnitTypeName(String commonName, String unitTypeName);

   List<InstitutionModel> findByContactCategoryName(String categoryName);

   List<InstitutionModel> findByUnitTypeName(String unitTypeName);

   List<InstitutionModel> findByContactCategoryNameAndIsCgiarIsTrue(String contactCategory);

}