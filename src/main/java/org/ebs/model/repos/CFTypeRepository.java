package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.CFTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CFTypeRepository extends JpaRepository<CFTypeModel, Integer>, RepositoryExt<CFTypeModel> {
    
    Optional<CFTypeModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<CFTypeModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
