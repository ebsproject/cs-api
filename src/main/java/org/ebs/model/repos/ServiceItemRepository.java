package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.OccurrenceShipmentModel;
import org.ebs.model.ServiceItemModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ServiceItemRepository
        extends JpaRepository<ServiceItemModel, Integer>, RepositoryExt<ServiceItemModel> {

    Optional<ServiceItemModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<ServiceItemModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    List<ServiceItemModel> findByIdInAndDeletedIsFalse(Iterable<Integer> ids);

    @Override
    default List<ServiceItemModel> findAllById(Iterable<Integer> ids) {
        return findByIdInAndDeletedIsFalse(ids);
    }

    public List<ServiceItemModel> findByServiceIdAndDeletedIsFalseAndServiceDeletedIsFalse(int serviceId);

    default List<ServiceItemModel> findByServiceId(int serviceId) {
        return findByServiceIdAndDeletedIsFalseAndServiceDeletedIsFalse(serviceId);
    }

    public List<ServiceItemModel> findByOccurrenceShipmentIdAndDeletedIsFalseAndOccurrenceShipmentDeletedIsFalse(int serviceId);

    default List<ServiceItemModel> findByOccurrenceShipment(int occurrenceShipmentId) {
        return findByOccurrenceShipmentIdAndDeletedIsFalseAndOccurrenceShipmentDeletedIsFalse(occurrenceShipmentId);
    }

    @Query("select item from ServiceItemModel item " + 
        "join ServiceModel s on item.service.id = s.id " + 
        "join WorkflowInstanceModel wf on s.workflowInstance.id = wf.id " + 
        "where wf.complete is null and (item.testCode is null or TRIM(item.testCode) = '') " + 
        "and item.deleted = false and s.deleted = false and wf.deleted = false")
    public List<ServiceItemModel> findServiceItemsToUpdate();

    @Query("select item from ServiceItemModel item " + 
        "join ServiceModel s on item.service.id = s.id " + 
        "join WorkflowInstanceModel wf on s.workflowInstance.id = wf.id " + 
        "where wf.complete is null and (item.testCode is null or TRIM(item.testCode) = '') " + 
        "and s.id = ?1 " +
        "and item.deleted = false and s.deleted = false and wf.deleted = false")
    public List<ServiceItemModel> findServiceItemsToUpdateByService(int serviceId);

    public List<ServiceItemModel> findBySeedIdAndPackageIdAndDeletedIsFalse(int seedId, int packageId);

    default List<ServiceItemModel> findBySeedIdAndPackageId(int seedId, int packageId) {
        return findBySeedIdAndPackageIdAndDeletedIsFalse(seedId, packageId);
    }

    public List<ServiceItemModel> findByServiceIdAndDeletedIsFalseAndServiceDeletedIsFalseAndPackageIdIsNotNull(int serviceId);

    default List<ServiceItemModel> findByServideIdAndPackageIsNotNull(int serviceId) {
        return findByServiceIdAndDeletedIsFalseAndServiceDeletedIsFalseAndPackageIdIsNotNull(serviceId);
    }

    @Modifying
    @Query("delete from ServiceItemModel s where s.occurrenceShipment in ?1")
    void deleteByOccurrenceShipmentList(List<OccurrenceShipmentModel> list);

}
