package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.ServiceModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<ServiceModel, Integer>, RepositoryExt<ServiceModel> {
    
    Optional<ServiceModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<ServiceModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
