package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.PrintoutTemplateModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrintoutTemplateRepository extends JpaRepository<PrintoutTemplateModel,Integer>, RepositoryExt<PrintoutTemplateModel> {

	Optional<PrintoutTemplateModel> findByIdAndDeletedIsFalse(int printoutTemplateId);
}