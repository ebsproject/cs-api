///////////////////////////////////////////////////////////
//  EntityReferenceRepository.java
//  Macromedia ActionScript Implementation of the Interface EntityReferenceRepository
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:57 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.model.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

import org.ebs.model.EntityReferenceModel;
import org.ebs.util.RepositoryExt;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:57 AM
 */
public interface EntityReferenceRepository
        extends JpaRepository<EntityReferenceModel, Integer>, RepositoryExt<EntityReferenceModel> {
    // Optional<EntityReferenceModel> findByHierarchyDesignsIdAndDeletedIsFalse(int hierarchyDesignId);
}