package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.FilterModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilterRepository extends JpaRepository<FilterModel, Integer>, RepositoryExt<FilterModel> {
    Optional<FilterModel> findByIdAndDeletedIsFalse(int filterId);

    @Override
    default Optional<FilterModel> findById(Integer filterId) {
        return findByIdAndDeletedIsFalse(filterId);
    }
}
