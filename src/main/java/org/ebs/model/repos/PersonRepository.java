package org.ebs.model.repos;

import org.ebs.model.PersonModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<PersonModel, Integer>, RepositoryExt<PersonModel> {
     /**
	 * 
	 * @param givenName
     * @param familyName
	 */
    public int countByGivenNameIgnoreCaseAndFamilyNameIgnoreCaseAndDeletedIsFalse(String givenName, String familyName);

}