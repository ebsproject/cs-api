
package org.ebs.model.repos;

import java.util.Optional;
import java.util.Set;

import org.ebs.model.ProductAuthorizationModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductAuthorizationRepository extends JpaRepository<ProductAuthorizationModel, Integer>,
   RepositoryExt<ProductAuthorizationModel>{

     Optional<ProductAuthorizationModel> findByIdAndDeletedIsFalse(int productAuthorizationId);

     Set<ProductAuthorizationModel> findByTenantIdAndDeletedIsFalse(int tenantId);

     Set<ProductAuthorizationModel> findByTenantIdAndFunctionalUnitIsNullAndDeletedIsFalse(int tenantId);

     Set<ProductAuthorizationModel> findByTenantIdAndFunctionalUnitIdAndDeletedIsFalse(int tenantId, int functionalUnitId);
   }