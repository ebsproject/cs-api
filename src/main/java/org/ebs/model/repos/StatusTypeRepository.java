package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.StatusTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusTypeRepository extends JpaRepository<StatusTypeModel, Integer>, RepositoryExt<StatusTypeModel> {
    
    Optional<StatusTypeModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<StatusTypeModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
