package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.ProductFunctionModel;
import org.ebs.model.RoleModel;
import org.ebs.model.RoleProductModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


public interface RoleProductRepository extends JpaRepository<RoleProductModel,Integer>, RepositoryExt<RoleProductModel> {
	Optional<RoleProductModel> findByRoleAndProductFunction(RoleModel role,ProductFunctionModel productFunction);
	Optional<RoleProductModel> findByRoleIdAndProductFunctionId(Integer roleId,Integer productFunctionId);
    Integer deleteByRoleIdAndProductFunctionId( Integer roleId, Integer productFunctionId );
	List<RoleProductModel> findByRole(RoleModel role);
	List<RoleProductModel> findByRoleId(Integer roleId);
}