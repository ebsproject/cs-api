package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.CropProgramModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CropProgramRepository extends JpaRepository<CropProgramModel,Integer>, RepositoryExt<CropProgramModel> {

	Optional<CropProgramModel> findByIdAndDeletedIsFalse(int elementId);
	List<CropProgramModel> findByCropIdAndDeletedIsFalse(int cropId);

}