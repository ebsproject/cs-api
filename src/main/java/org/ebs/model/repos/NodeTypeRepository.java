package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.NodeTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NodeTypeRepository extends JpaRepository<NodeTypeModel, Integer>, RepositoryExt<NodeTypeModel> {
    
    Optional<NodeTypeModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<NodeTypeModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
