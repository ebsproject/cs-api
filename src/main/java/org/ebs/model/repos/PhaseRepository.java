package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.PhaseModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.UUID;


public interface PhaseRepository extends JpaRepository<PhaseModel, Integer>, RepositoryExt<PhaseModel> {

    Optional<PhaseModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<PhaseModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    Optional<PhaseModel> findBySequenceAndWorkflowAndDeletedIsFalse(int sequence, int workflow);

    List<PhaseModel> findByWorkflowIdAndDeletedIsFalse(int workflowId);

    default List<PhaseModel> findByWorkflowId(int workflowId) {
        return findByWorkflowIdAndDeletedIsFalse(workflowId);
    }

    List<PhaseModel> findByDesignRef(UUID designRef);
    
}
