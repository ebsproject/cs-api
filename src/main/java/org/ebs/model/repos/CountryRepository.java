package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.CountryModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository
        extends JpaRepository<CountryModel, Integer>, RepositoryExt<CountryModel> {

    /**
     * Finds a country
     * <p>
     * Note: use the shorter method findById()
     * </p>
     *
     * @param countryId
     * @return Optional with the country, empty if not found
     */
    Optional<CountryModel> findByIdAndDeletedIsFalse(int countryId);

    /**
     * Finds a country
     *
     * @param countryId
     * @return Optional with the country, empty if not found
     */
    @Override
    default Optional<CountryModel> findById(Integer countryId) {
        return findByIdAndDeletedIsFalse(countryId);
    }
}