package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.HierarchyModel;
import org.ebs.model.RoleContactHierarchyModel;
import org.ebs.model.RoleContactHierarchyPK;
import org.ebs.model.RoleModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleContactHierarchyRepository extends JpaRepository<RoleContactHierarchyModel,RoleContactHierarchyPK>, RepositoryExt<RoleContactHierarchyModel> {
	Optional<RoleContactHierarchyModel> findByRoleAndHierarchy(RoleModel role, HierarchyModel hierarchy);
	void deleteByRoleIdAndHierarchyId(int roleId, int hierarchyId);
	Optional<RoleContactHierarchyModel> findByRoleIdAndHierarchyId(int roleId, int hierarchyId);
	void deleteByHierarchyId(int hierarchyId);
}