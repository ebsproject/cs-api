package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ShipmentItemModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ShipmentItemRepository
        extends JpaRepository<ShipmentItemModel, Integer>, RepositoryExt<ShipmentItemModel> {

    Optional<ShipmentItemModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<ShipmentItemModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    List<ShipmentItemModel> findByIdInAndDeletedIsFalse(Iterable<Integer> ids);

    @Override
    default List<ShipmentItemModel> findAllById(Iterable<Integer> ids) {
        return findByIdInAndDeletedIsFalse(ids);
    }

    public List<ShipmentItemModel> findByShipmentIdAndDeletedIsFalseAndShipmentDeletedIsFalse(int shipmentId);

    default List<ShipmentItemModel> findByShipmentId(int shipmentId) {
        return findByShipmentIdAndDeletedIsFalseAndShipmentDeletedIsFalse(shipmentId);
    }

    @Query("select item from ShipmentItemModel item " + 
        "join ShipmentModel s on item.shipment.id = s.id " + 
        "join WorkflowInstanceModel wf on s.workflowInstance.id = wf.id " + 
        "where wf.complete is null and (item.testCode is null or TRIM(item.testCode) = '') " + 
        "and item.deleted = false and s.deleted = false and wf.deleted = false")
    public List<ShipmentItemModel> findShipmentItemsToUpdate();

    @Query("select item from ShipmentItemModel item " + 
        "join ShipmentModel s on item.shipment.id = s.id " + 
        "join WorkflowInstanceModel wf on s.workflowInstance.id = wf.id " + 
        "where wf.complete is null and (item.testCode is null or TRIM(item.testCode) = '') " + 
        "and s.id = ?1 " +
        "and item.deleted = false and s.deleted = false and wf.deleted = false")
    public List<ShipmentItemModel> findShipmentItemsToUpdateByShipment(int shipmentId);

}
