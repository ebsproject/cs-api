package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.StatusModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusRepository extends JpaRepository<StatusModel, Integer>, RepositoryExt<StatusModel> {
    
    Optional<StatusModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<StatusModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    List<StatusModel> findByWorkflowInstanceAndWorkflowInstanceDeletedIsFalseAndDeletedIsFalse(WorkflowInstanceModel workflowInstance);

    default List<StatusModel> findByWorkflowInstance(WorkflowInstanceModel workflowInstance) {
        return findByWorkflowInstanceAndWorkflowInstanceDeletedIsFalseAndDeletedIsFalse(workflowInstance);
    }

}
