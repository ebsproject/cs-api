package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.AddressModel;
import org.ebs.model.ContactAddressModel;
import org.ebs.model.ContactAddressPK;
import org.ebs.model.ContactModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactAddressRepository extends JpaRepository<ContactAddressModel, ContactAddressPK>, RepositoryExt<ContactAddressModel> {
    public List<ContactAddressModel> findByContact(ContactModel contact);

    public List<ContactAddressModel> findByAddress(AddressModel address);

    public Optional<ContactAddressModel> findByContactAndIsDefaultIsTrue(ContactModel contact);

    public Optional<ContactAddressModel> findByContactAndAddress(ContactModel contact, AddressModel address);

    public Optional<ContactAddressModel> findByContactIdAndAddressId(int contactId, int addressId);
}
