package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.StageModel;
import org.ebs.model.WorkflowModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.UUID;


public interface StageRepository extends JpaRepository<StageModel, Integer>, RepositoryExt<StageModel> {

    Optional<StageModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<StageModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }
    
    Optional<StageModel> findBySequenceAndPhaseSequenceAndPhaseWorkflowAndDeletedIsFalse(int sequence, int phaseSequence, WorkflowModel workflow);

    List<StageModel> findByPhaseIdAndDeletedIsFalse(int phaseId);

    default List<StageModel> findByPhaseId(int phaseId) {
        return findByPhaseIdAndDeletedIsFalse(phaseId);
    }

    List<StageModel> findByDesignRef(UUID designRef);

}
