package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactInfoModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactInfoRepository
        extends JpaRepository<ContactInfoModel, Integer>, RepositoryExt<ContactInfoModel> {

    /**
     * Finds a contact info element
     * <p>
     * Note: use the shorter method findById()
     * </p>
     *
     * @param contactId
     * @return optional of contact info element or empty if not found
     */
    Optional<ContactInfoModel> findByIdAndDeletedIsFalse(int contactInfoId);

    /**
     * Finds a contact info element
     *
     * @param contactInfoId
     * @return optional of contact info element or empty if not found
     */
    @Override
    default Optional<ContactInfoModel> findById(Integer contactInfoId) {
        return findByIdAndDeletedIsFalse(contactInfoId);
    }

    /**
     * Finds the information items of a given contact
     * <p>
     * Note: use the shorter method findByContact()
     * </p>
     *
     * @param contactId
     * @return types of the contact
     */
    List<ContactInfoModel> findByContactIdAndContactDeletedIsFalseAndDeletedIsFalse(int contactId);

    /**
     * Finds the information items of a given contact
     *
     * @param contactId
     * @return info items of the contact
     */
    default List<ContactInfoModel> findByContact(int contactId) {
        return findByContactIdAndContactDeletedIsFalseAndDeletedIsFalse(contactId);
    }

    List<ContactInfoModel> findByContactPersonIdAndContactDeletedIsFalseAndDeletedIsFalse(int contactId);

    List<ContactInfoModel> findByContactInfoTypeIdAndContactIdAndValueIgnoreCaseAndDeletedIsFalse(int infoTypeId, int contactId, String value);

}