package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.NodeCFModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NodeCFRepository extends JpaRepository<NodeCFModel, Integer>, RepositoryExt<NodeCFModel> {
    
    Optional<NodeCFModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<NodeCFModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    List<NodeCFModel> findByNodeIdAndDeletedIsFalse(int nodeId);

    default List<NodeCFModel> findByNodeId(int nodeId) {
        return findByNodeIdAndDeletedIsFalse(nodeId);
    }

}
