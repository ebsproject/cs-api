package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.ServiceFileModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceFileRepository extends JpaRepository<ServiceFileModel, Integer>, RepositoryExt<ServiceFileModel>  {
    
    Optional<ServiceFileModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<ServiceFileModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
