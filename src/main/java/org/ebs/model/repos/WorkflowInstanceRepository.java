package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.WorkflowInstanceModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkflowInstanceRepository extends JpaRepository<WorkflowInstanceModel, Integer>, RepositoryExt<WorkflowInstanceModel> {
    
    Optional<WorkflowInstanceModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<WorkflowInstanceModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
