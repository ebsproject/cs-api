package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactModel;
import org.ebs.model.ContactWorkflowModel;
import org.ebs.model.WorkflowModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactWorkflowRepository extends JpaRepository<ContactWorkflowModel, Integer>, RepositoryExt<ContactWorkflowModel> {
    public List<ContactWorkflowModel> findByContact(ContactModel contact);

    public List<ContactWorkflowModel> findByContactId(int contactId);

    public void deleteByContactId(int contactId);

    public List<ContactWorkflowModel> findByWorkflow(WorkflowModel workflow);

    public List<ContactWorkflowModel> findByWorkflowIdAndProvideIsTrue(int workflowId);

    public Optional<ContactWorkflowModel> findByContactAndProvideIsTrue(ContactModel contact);

    public Optional<ContactWorkflowModel> findByContactAndWorkflow(ContactModel contact, WorkflowModel workflow);

    public Optional<ContactWorkflowModel> findByContactIdAndWorkflowId(int contactId, int workflowId);

    public Optional<ContactWorkflowModel> findById(int id);
}
