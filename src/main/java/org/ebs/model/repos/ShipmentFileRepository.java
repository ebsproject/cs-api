package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.ShipmentFileModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentFileRepository extends JpaRepository<ShipmentFileModel, Integer>, RepositoryExt<ShipmentFileModel>  {
    
    Optional<ShipmentFileModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<ShipmentFileModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
