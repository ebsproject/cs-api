package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.FileTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FileTypeRepository extends JpaRepository<FileTypeModel, Integer>, RepositoryExt<FileTypeModel> {

    Optional<FileTypeModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<FileTypeModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }
    
}