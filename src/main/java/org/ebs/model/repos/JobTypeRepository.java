package org.ebs.model.repos;

import org.ebs.model.JobTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobTypeRepository extends JpaRepository<JobTypeModel,Integer>, RepositoryExt<JobTypeModel> {
    
}
