package org.ebs.model.repos;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository
        extends JpaRepository<ContactModel, Integer>, RepositoryExt<ContactModel> {

    /**
     * Finds a contact
     * <p>
     * Note: use the shorter method findById()
     * </p>
     *
     * @param contactId
     * @return and optional of the contact. Empty if not found
     */
    Optional<ContactModel> findByIdAndDeletedIsFalse(int contactId);

    /**
     * Finds a contact
     *
     * @param contactId
     * @return and optional of the contact. Empty if not found
     */
    @Override
    default Optional<ContactModel> findById(Integer contactId) {
        return findByIdAndDeletedIsFalse(contactId);
    }
    Optional<ContactModel> findByExternalId(int externalId);

    Optional<ContactModel> findByIdAndDeletedIsTrue(int contactId);

    /**
	 * 
	 * @param purposeId
	 */
	public List<ContactModel> findByPurposesId(int purposeId);
    /**
	 * 
	 * @param email
	 */
    public int countByEmailIgnoreCaseAndDeletedIsFalse(String email);

    Optional<ContactModel> findByCategoryNameAndInstitutionCommonNameAndDeletedIsFalseAndInstitutionDeletedIsFalse(String categoryName, String institutionCommonName);

    List<ContactModel> findByCategoryNameAndInstitutionExternalCodeAndDeletedIsFalseAndInstitutionUnitTypeNotNullAndInstitutionDeletedIsFalse(String categoryName, int externalCode);

    List<ContactModel> findByCategoryNameAndDeletedIsFalseAndInstitutionUnitTypeNotNullAndInstitutionDeletedIsFalse(String categoryName);

    List<ContactModel> findByContactInfosContactInfoTypeNameInIgnoreCaseAndContactInfosValueAndContactInfosDeletedIsFalseAndDeletedIsFalse(List<String> contactInfoTypeNames, String contactInfoValue);
}