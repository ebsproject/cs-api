package org.ebs.model.repos;

import java.util.Optional;

import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkflowViewTypeRepository extends JpaRepository<WorkflowViewTypeModel, Integer>, RepositoryExt<WorkflowViewTypeModel> {
    
    Optional<WorkflowViewTypeModel> findByIdAndDeletedIsFalse(Integer id);

    @Override
    default Optional<WorkflowViewTypeModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

}
