package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "node_cf", schema = "workflow")
@TypeDef(
    typeClass = JsonType.class,
    defaultForType = JsonNode.class
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NodeCFModel extends Auditable {

    private static final long serialVersionUID = 993258903;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column
    private String name;
    
    @Column
    private String description;

    @Column
    private String help;

    @Column
    private Boolean required;

    @Column(columnDefinition = "jsonb", name = "field_attributes")
	private JsonNode fieldAttributes;

    @Column(name = "api_attributes_name")
    private String apiAttributesName;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "attributes_id")
    private AttributesModel attributes;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "cftype_id")
    private CFTypeModel cfType;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "htmltag_id")
    private HtmlTagModel htmlTag;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "node_id")
    private NodeModel node;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;
    
}
