package org.ebs.model;

import java.io.Serializable;

public class ContactWorkflowPK implements Serializable {

    private int contact;
    private int workflow;

    public ContactWorkflowPK(){};

    public ContactWorkflowPK(int contact, int workflow) {
        this.contact = contact;
        this.workflow = workflow;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + contact;
        result = prime * result + workflow;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContactWorkflowPK other = (ContactWorkflowPK) obj;
        if (contact != other.contact)
            return false;
        if (workflow != other.workflow)
            return false;
        return true;
    }
}
