package org.ebs.model;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "workflow", schema = "workflow")
@TypeDef(
    typeClass = JsonType.class,
    defaultForType = JsonNode.class
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowModel extends Auditable {

    private static final long serialVersionUID = 993258890;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "help")
    private String help;

    @Column(name = "sort_no")
    private Integer sortNo;

    @Column(name = "icon")
    private String icon;

    @ManyToMany(mappedBy = "workflows")
    private Set<TenantModel> tenants = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "htmltag_id")
    private HtmlTagModel htmlTag;

    @Column(name = "api")
    private String api;

    @Column(name = "navigation_icon")
    private String navigationIcon;

    @Column(name = "navigation_name")
    private String navigationName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private ProductModel product;

    @Column(name = "sequence")
    private Integer sequence;

    @Column(name = "show_menu")
    private boolean showMenu;

    @Column(name = "is_system")
    private Boolean isSystem;

    @Column(columnDefinition = "jsonb", name = "security_definition")
	private JsonNode securityDefinition;

/*     @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "node_id")
    private NodeModel node; */

    @OneToMany(mappedBy = "workflow", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PhaseModel> phases;

    @OneToMany(mappedBy = "workflow", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<StatusTypeModel> statusTypes;

    @ManyToMany(mappedBy = "workflows")
    private List<ContactModel> contacts;

    @OneToMany(mappedBy = "workflow", fetch = LAZY, cascade = ALL)
    private List<ContactWorkflowModel> workflowContacts;

    public Set<PhaseModel> getPhases() {
        return phases.stream().filter(p -> !p.isDeleted()).collect(Collectors.toSet());
    }

    public List<StatusTypeModel> getStatusTypes() {
        return statusTypes.stream().filter(s -> !s.isDeleted()).collect(Collectors.toList());
    }

    public List<ContactWorkflowModel> getWorkflowContacts() {
        return workflowContacts.stream().filter(wf -> !wf.getContact().isDeleted()).collect(Collectors.toList());
    }
}