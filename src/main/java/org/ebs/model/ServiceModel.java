package org.ebs.model;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "service", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServiceModel extends Auditable {

    private static final long serialVersionUID = 993258907;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "instance_id")
    private Integer recordId;

    @Column
    private String description;

    @Column
    private String requester;

    @Column(name = "request_code")
    private String requestCode;

    @Column(name = "submition_date")
    private Date submitionDate;

    @Column(name = "admin_contact")
    private String adminContact;

    @Column(name = "charge_account")
    private String chargeAccount;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id")
    private PersonModel person;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "wf_instance_id")
    private WorkflowInstanceModel workflowInstance;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sender_id")
    private ContactModel sender;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "requestor_id")
    private ContactModel requestor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipient_id")
    private ContactModel recipient;

    @OneToMany(mappedBy = "service", fetch = LAZY, cascade = ALL)
    @ToString.Exclude
    private List<ServiceItemModel> items;

    @OneToMany(mappedBy = "service", fetch = LAZY, cascade = ALL)
    @ToString.Exclude
    private List<OccurrenceShipmentModel> occurrenceShipments;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "program_id")
    private ContactModel program;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "serviceprovider_id")
    private ContactModel serviceProvider;

    @Column(name = "by_occurrence")
    private boolean byOccurrence;

}
