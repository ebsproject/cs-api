package org.ebs.model;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name = "rule", schema = "security")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SecurityRuleModel extends Auditable{
  @SuppressWarnings("unused")
private static final long serialVersionUID = 1678908587;
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column
  private int id;
  @Column
  private String name;
  @Column
  private String description;
  @Column(name = "used_workflow")
  private boolean usedByWorkflow;
  @OneToMany(mappedBy = "role")
  Set<SecurityRuleRoleModel> roles;
}