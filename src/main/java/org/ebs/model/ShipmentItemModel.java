package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "shipment_item", schema = "shm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentItemModel extends Auditable {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "shipment_id")
    private ShipmentModel shipment;

    @Column(name = "germplasm_id")
    private Integer germplasmId;

    @Column(name = "seed_id")
    private Integer seedId;

    @Column(name = "package_id")
    private Integer packageId;

    @Column(name = "number")
    private Integer number;

    @Column(name = "code")
    private String code;

    @Column(name = "status")
    private String status;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "package_unit")
    private String packageUnit;

    @Column(name = "package_count")
    private Integer packageCount;

    @Column(name = "test_code")
    private String testCode;

    @Column(name = "mta_status")
    private String mtaStatus;

    @Column(name = "availability")
    private String availability;

    @Column(name = "use")
    private String use;

    @Column(name = "smta_id")
    private String smtaId;

    @Column(name = "mls_ancestors")
    private String mlsAncestors;

    @Column(name = "genetic_stock")
    private String geneticStock;

    @Column(name = "origin")
    private String origin;

    @Column(name = "remarks")
    private String remarks;

    @Column(name = "package_name")
    private String packageName;

    @Column(name = "package_label")
    private String packageLabel;

    @Column(name = "seed_name")
    private String seedName;

    @Column(name = "seed_code")
    private String seedCode;

    @Column(name = "designation")
    private String designation;

    @Column(name = "parentage")
    private String parentage;
    
    @Column(name = "taxonomy_name")
    private String taxonomyName;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

}
