package org.ebs.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "info_type", schema = "crm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactInfoTypeModel extends Auditable {

    public static final int INFO_TYPE_TELEPHONE = 1;

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;

    @Column
    private String name;

    @Column
    private String description;

}