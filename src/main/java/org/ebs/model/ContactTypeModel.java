package org.ebs.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "contact_type", schema = "crm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactTypeModel extends Auditable {

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;


    @ManyToOne(fetch=FetchType.EAGER) @JoinColumn(name="category_id")
    private CategoryModel category;

    @Column
    private String name;

   @ManyToMany(mappedBy = "contactTypes")
    private List<ContactModel> contacts;
}