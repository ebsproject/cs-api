///////////////////////////////////////////////////////////
//  EmailTemplateModel.java
//  Macromedia ActionScript Implementation of the Class EmailTemplateModel
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:56 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.model;

import org.ebs.util.Auditable;
import javax.persistence.ManyToMany;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:56 AM
 */
@Entity
@Table(name = "email_template", schema = "core")
@Getter
@Setter
@ToString
public class EmailTemplateModel extends Auditable {
    private static final long serialVersionUID = -415557581;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "subject")
    private String subject;
    @Column(name = "template")
    private String template;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "emailtemplate_entityreference", schema = "core", joinColumns = @JoinColumn(name = "emailtemplate_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "entityreference_id", referencedColumnName = "id"))
    @ToString.Exclude
    private Set<EntityReferenceModel> entityreferences;
    // @ManyToOne(fetch = FetchType.LAZY, optional = true)
    // @JoinColumn(name = "tenant_id")
    // @ToString.Exclude
    // private TenantModel tenant;
    @ManyToMany(mappedBy = "emailTemplates")
    private Set<TenantModel> tenants = new HashSet<>();
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "person_id")
    @ToString.Exclude
    private ContactModel contact;

}