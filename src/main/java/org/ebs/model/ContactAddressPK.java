package org.ebs.model;

import java.io.Serializable;

public class ContactAddressPK implements Serializable {

    private int contact;
    private int address;

    public ContactAddressPK(){};

    public ContactAddressPK(int contact, int address) {
        this.contact = contact;
        this.address = address;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + contact;
        result = prime * result + address;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContactAddressPK other = (ContactAddressPK) obj;
        if (contact != other.contact)
            return false;
        if (address != other.address)
            return false;
        return true;
    }
}
