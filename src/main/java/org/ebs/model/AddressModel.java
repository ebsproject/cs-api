package org.ebs.model;

import static javax.persistence.FetchType.LAZY;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "address", schema = "crm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddressModel extends Auditable {
    private static final long serialVersionUID = -515972981;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "location")
    private String location;

    @Column(name = "region")
    private String region;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "street_address")
    private String streetAddress;

    @ManyToOne(fetch = LAZY, optional = true)
    @JoinColumn(name = "country_id")
    private CountryModel country;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "address_purpose", 
        schema = "crm", 
        joinColumns = @JoinColumn(name = "address_id", referencedColumnName = "id"), 
        inverseJoinColumns = @JoinColumn(name = "purpose_id", referencedColumnName = "id")
    )
    private Set<PurposeModel> purposes = new HashSet<>();

    @Column(name = "full_address")
    private String fullAddress;

    @Column(name = "street_address_addtional")
    private String additionalStreetAddress;

    @ManyToMany(mappedBy = "addresses")
    private List<ContactModel> contacts;

    @OneToMany(mappedBy = "address",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private List<OrganizationModel> organizations;

    @OneToMany(mappedBy = "address",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private List<CustomerModel> customers;

    @OneToMany(mappedBy = "address",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InstanceModel> instances;

    @OneToMany(mappedBy = "address", fetch = LAZY, cascade = CascadeType.ALL)
    private Set<ContactAddressModel> contactAddresses;

    @OneToMany(mappedBy = "address", fetch = LAZY, cascade = CascadeType.ALL)
    private Set<ShipmentModel> shipments;

}