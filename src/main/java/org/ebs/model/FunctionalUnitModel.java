///////////////////////////////////////////////////////////
//  FunctionalUnitModel.java
//  Macromedia ActionScript Implementation of the Class FunctionalUnitModel
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:00 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.model;

import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:00 AM
 */
@Entity @Table(name="functional_unit",schema="security") @Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class FunctionalUnitModel extends Auditable {

	private static final long serialVersionUID = -477996611;

	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;

	@Column(name="name")
	private String name;
	@Column(name="code")
	private String code;
	@Column(name="type")
	private String type;
	@Column(name="description")
	private String description;
	@Column(name="notes")
	private String notes;
	@ManyToOne(fetch=FetchType.LAZY) @JoinColumn(name="parent_func_unit_id")
   	private FunctionalUnitModel parent;
	@ManyToMany(cascade =CascadeType.ALL) @JoinTable(name = "functional_unit_contact", schema="security", joinColumns  = @JoinColumn(name="functional_unit_id",referencedColumnName = "id"),inverseJoinColumns = @JoinColumn(name="contact_id",referencedColumnName = "id"))
	private Set<ContactModel> contacts = new HashSet<>();
	@OneToMany(mappedBy = "functionalUnit",fetch=FetchType.LAZY)
	private Set<ProductAuthorizationModel> productAuthorizations;
	@OneToMany(mappedBy = "parent",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<FunctionalUnitModel> children;

	public Set<ContactModel> getContacts() {
		return contacts.stream().filter(c -> !c.isDeleted()).collect(toSet());
	}

	@Override
	public String toString(){
		return "FunctionalUnitModel [id=" + id + ",]";
	}

}