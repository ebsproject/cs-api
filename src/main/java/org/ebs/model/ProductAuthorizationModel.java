package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table( name = "product_authorization", schema = "security")
@Getter @Setter @ToString
@AllArgsConstructor @NoArgsConstructor
public class ProductAuthorizationModel extends Auditable{

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue( strategy = GenerationType.IDENTITY)
   private Integer id;

   @ManyToOne(fetch=FetchType.LAZY, optional = false)
   @JoinColumn(name="product_id")
   private ProductModel product;

   @ManyToOne(fetch=FetchType.LAZY, optional = false)
   @JoinColumn(name="tenant_id")
   private TenantModel tenant;

   @ManyToOne(fetch=FetchType.LAZY)
   @JoinColumn(name="functional_unit_id")
   private FunctionalUnitModel functionalUnit;
}