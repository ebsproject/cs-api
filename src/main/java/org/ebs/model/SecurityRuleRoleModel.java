package org.ebs.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name = "rule_role", schema = "security")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SecurityRuleRoleModel extends Auditable {
  @SuppressWarnings("unused")
private static final long serialVersionUID = 1678908587;
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  @Column
  private int id;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "role_id", referencedColumnName = "id")
  private RoleModel role;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "rule_id", referencedColumnName = "id")
  private SecurityRuleModel rule;
}