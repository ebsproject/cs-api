///////////////////////////////////////////////////////////
//  ProductFunctionModel.java
//  Macromedia ActionScript Implementation of the Class ProductFunctionModel
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:19 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:19 AM
 */
@Entity
@Table(name = "product_function", schema = "security")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductFunctionModel extends Auditable {

    private static final long serialVersionUID = -66302096;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "description")
    private String description;
    @Column(name = "system_type")
    private boolean systemType;
    @Column(name = "action")
    private String action;
    @ManyToMany(mappedBy = "productfunctions")
    private Set<RoleModel> roles;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_id")
    private ProductModel product;
    @Column(name = "is_data_action")
    private boolean isDataAction;
    @OneToMany(mappedBy = "productFunction")
	Set<RoleProductModel> roleProductFunctions;

    @Override
    public String toString() {
        return "ProductFunctionModel [systemType=" + systemType + ",id=" + id + ",]";
    }

}