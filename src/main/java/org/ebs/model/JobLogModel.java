package org.ebs.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "job_log", schema = "core")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@TypeDef(
    typeClass = JsonType.class,
    defaultForType = JsonNode.class
)
public class JobLogModel extends Auditable {

    private static final long serialVersionUID = -358073002;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "job_workflow_id")
    private JobWorkflowModel jobWorkflow;

    @Column(name = "job_track_id")
    @Type(type="pg-uuid")
    private UUID jobTrackId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "translation_id")
    private TranslationModel translation;

    @Column(columnDefinition = "jsonb", name = "message")
    private JsonNode message;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "status")
    private String status;

    @Column(name = "tenant_id", nullable = false)
    private Integer tenantId;

    @Column(name = "record_id")
    private Integer recordId;

    @ManyToMany(cascade = {CascadeType.DETACH, CascadeType.PERSIST})
    @JoinTable(
        name = "job_log_contact", 
        schema = "core", 
        joinColumns = @JoinColumn(name = "job_log_id", referencedColumnName = "id"), 
        inverseJoinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "id")
    )
    private Set<ContactModel> contacts = new HashSet<>();

    public Set<ContactModel> getContacts() {
        return contacts.stream().filter(c -> !c.isDeleted()).collect(Collectors.toSet());
    }

}
