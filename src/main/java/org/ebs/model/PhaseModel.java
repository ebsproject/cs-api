package org.ebs.model;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "phase", schema = "workflow")
@TypeDef(
    typeClass = JsonType.class,
    defaultForType = JsonNode.class
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PhaseModel extends Auditable {

    private static final long serialVersionUID = 993258891;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "help")
    private String help;

    @Column(name = "sequence")
    private Integer sequence;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "htmltag_id")
    private HtmlTagModel htmlTag;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "workflow_id")
    private WorkflowModel workflow;

    @Column(columnDefinition = "jsonb", name = "depend_on")
	private JsonNode dependOn;

    @Column
    private String icon;

    @Column(name = "design_ref")
    @Type(type="pg-uuid")
    private UUID designRef;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "wf_view_type_id")
    private WorkflowViewTypeModel workflowViewType;

    @OneToMany(mappedBy = "phase", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<StageModel> stages;

    public Set<StageModel> getStages() {
        return stages.stream().filter(s -> !s.isDeleted()).collect(Collectors.toSet());
    }
}
