package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "cf_type", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CFTypeModel extends Auditable {

    private static final long serialVersionUID = 993258900;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column
    private String name;
    
    @Column
    private String description;

    @Column
    private String help;

    @Column
    private String type;
    
}
