package org.ebs.model;

import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "contact_info", schema = "crm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactInfoModel extends Auditable {

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;

    @Column
    private String value;

    @Column(name = "is_default")
    private boolean Default;

    @ManyToOne(fetch = LAZY, optional = false)
    @JoinColumn(name = "contact_id")
    private ContactModel contact;

    @ManyToOne(fetch = LAZY, optional = false)
    @JoinColumn(name = "info_type_id")
    private ContactInfoTypeModel contactInfoType;

}