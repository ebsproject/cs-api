package org.ebs.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "person", schema = "crm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PersonModel extends Auditable {

    @GeneratedValue(strategy = IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "family_name")
    private String familyName;

    @Column(name = "given_name")
    private String givenName;

    @Column(name = "additional_name")
    private String additionalName;

    @Column(name = "full_name")
    private String fullName;

    @Column
    private String gender;

    @Column(name = "job_title")
    private String jobTitle;

    @Column(name = "knows_about")
    private String knowsAbout;

    @OneToOne(optional = false)
    @JoinColumn(name = "contact_id", unique = true, nullable = false, updatable = false)
    @ToString.Exclude
    private ContactModel contact;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "language_id")
    @ToString.Exclude
    private LanguageModel language;

    @Column(name = "salutation")
    private String salutation;

    @ManyToOne

    public String getFullName() {
        if (this.fullName == null)
            return null;
        if (this.fullName.endsWith(",")) {
            this.fullName = this.fullName.substring(0, this.fullName.length() - 1);
        }
        return this.fullName;
    }

}