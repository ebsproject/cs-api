package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "unit_type", schema = "crm")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UnitTypeModel {

	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;

	@Column(name="name")
	private String name;

}