package org.ebs.model.rule;

import static javax.persistence.EnumType.STRING;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.AuditableTenant;
import org.springframework.http.HttpMethod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity @Table(name = "segment_api", schema = "core")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor @AllArgsConstructor
public class ApiSegmentModel extends AuditableTenant {

    public enum ApiType {
        REST, GRAPHQL
    }

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private int id;

    @Column
    @Enumerated(STRING)
    private ApiType type;
    
    @Column
    @Enumerated(STRING)
    private HttpMethod method;
    
    @Column(name = "server_url")
    private String serverUrl;
    
    @Column(name = "path_template")
    private String pathTemplate;
    
    @Column(name = "body_template")
    private String bodyTemplate;
    
    @Column(name = "response_mapping")
    private String responseMapping;

}
