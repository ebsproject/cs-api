package org.ebs.model.rule;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.AuditableTenant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity @Table(name = "sequence_rule", schema = "core")
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor @NoArgsConstructor
public class SequenceRuleModel extends AuditableTenant {
    
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id @Column
    private int id;
    
    @Column
    private String name;
    
    @OneToMany(mappedBy = "rule",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@ToString.Exclude
    private List<SequenceRuleSegmentModel> ruleSegments;
}

