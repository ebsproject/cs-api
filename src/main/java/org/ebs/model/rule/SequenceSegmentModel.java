package org.ebs.model.rule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.ebs.util.AuditableTenant;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * When first created, last = lowest - increment, so the first
 * time last is computed returns lowest as first value of the sequence
 */
@Entity @Table(name = "segment_sequence", schema = "core")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor @AllArgsConstructor
public class SequenceSegmentModel extends AuditableTenant{

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id @Column
    private int id;
    
    @Column
    private int lowest;
    @Column
    private int increment;
    @Column
    private int last;
    @Column(name = "reset_condition", columnDefinition = "jsonb")
    private JsonNode resetCondition;
    @Column(name = "family_key")
    private String familyKey;
    @Column(name = "parent_id")
    private Integer parentId;
    
}
