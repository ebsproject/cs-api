package org.ebs.model.rule;

import static javax.persistence.EnumType.STRING;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.AuditableTenant;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "rule_segment", schema = "core")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor @AllArgsConstructor
public class SegmentModel extends AuditableTenant{
    
    public enum DataType {
        TEXT, DATE, NUMBER, DATETIME
    }

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column
    private String name;
    
    @Column
    private String format;
    
    @Column
    private String formula;
    
    @Column(name = "requires_input")
    private boolean requiresInput;
    
    @Column(name = "data_type")
    @Enumerated(STRING)
    private DataType dataType;
    
    @OneToMany(mappedBy = "segment",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@ToString.Exclude
    private List<SequenceRuleSegmentModel> ruleSegments;
     
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="segment_api_id")
	@ToString.Exclude
    private ApiSegmentModel apiSegment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="segment_sequence_id")
	@ToString.Exclude
    private SequenceSegmentModel sequenceSegment;

    @Column(columnDefinition = "jsonb", name = "scale")
    private JsonNode scale;
}
