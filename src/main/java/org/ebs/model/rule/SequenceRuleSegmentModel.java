package org.ebs.model.rule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.AuditableTenant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "sequence_rule_segment", schema = "core")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor @AllArgsConstructor
public class SequenceRuleSegmentModel extends AuditableTenant{
    
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column
    private Integer position;
    
    @Column(name = "segment_family")
    private Integer family;
    
     
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="sequence_rule_id")
	@ToString.Exclude
    private SequenceRuleModel rule;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="rule_segment_id")
	@ToString.Exclude
    private SegmentModel segment;

    @Column(name = "is_displayed")
    private boolean displayed;

    @Column(name = "is_required")
    private boolean required;

    @Column(name = "skip")
    private String skip;

    @Column(name = "skip_input_rule_segment_id")
    private Integer skipInputSegmentId;
}
