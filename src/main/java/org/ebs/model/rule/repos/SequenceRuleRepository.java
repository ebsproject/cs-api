package org.ebs.model.rule.repos;

import java.util.Optional;

import org.ebs.model.rule.SequenceRuleModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SequenceRuleRepository extends JpaRepository<SequenceRuleModel, Integer>, RepositoryExt<SequenceRuleModel>{
    
    @Override
    default Optional<SequenceRuleModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }

    Optional<SequenceRuleModel> findByIdAndDeletedIsFalse(Integer id);

}
