package org.ebs.model.rule.repos;

import java.util.Optional;

import org.ebs.model.rule.SequenceSegmentModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SequenceSegmentRepository extends JpaRepository<SequenceSegmentModel, Integer> {

    default Optional<SequenceSegmentModel> findByParentIdAndFamilyKey(int parentId, String familyKey) {
        return findByParentIdAndFamilyKeyAndDeletedIsFalse(parentId, familyKey);
    }

    Optional<SequenceSegmentModel> findByParentIdAndFamilyKeyAndDeletedIsFalse(int parentId, String familyKey);
}
