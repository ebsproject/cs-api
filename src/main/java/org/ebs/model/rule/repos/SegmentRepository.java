package org.ebs.model.rule.repos;

import java.util.Optional;

import org.ebs.model.rule.SegmentModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SegmentRepository extends JpaRepository<SegmentModel, Integer>{

    @Override
    default Optional<SegmentModel> findById(Integer id) {
        return findByIdAndDeletedIsFalse(id);
    }


    Optional<SegmentModel> findByIdAndDeletedIsFalse(Integer id);
}
