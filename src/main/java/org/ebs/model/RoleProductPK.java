package org.ebs.model;

import java.io.Serializable;

public class RoleProductPK implements Serializable {
    protected int role;
    protected int productFunction;

     public RoleProductPK(){}

     public RoleProductPK(RoleModel role,ProductFunctionModel productFunction){
         this.role=role.getId();
         this.productFunction=productFunction.getId();
     }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + role;
        result = prime * result + productFunction;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
            RoleProductPK other = (RoleProductPK) obj;
        if (role != other.role)
            return false;
        if (productFunction != other.productFunction)
            return false;
        return true;
    }
 
}
