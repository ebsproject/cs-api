package org.ebs.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "status", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatusModel extends Auditable {

    private static final long serialVersionUID = 993258906;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name = "initiate_date")
    private Date initiateDate;
    
    @Column(name = "completion_date")
    private Date completionDate;
    
    @Column(name = "instance_id")
    private Integer recordId;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "statustype_id")
    private StatusTypeModel statusType;
    
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "wf_instance_id")
    private WorkflowInstanceModel workflowInstance;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "tenant_id")
    private TenantModel tenant;
    
}
