package org.ebs.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "wf_instance", schema = "workflow")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowInstanceModel extends Auditable {

    private static final long serialVersionUID = 993258893;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column
    private int id;

    @Column(name="complete")
	private Date complete;
    
	@Column(name="initiated")
	private Date initiated;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "workflow_id")
    private WorkflowModel workflow;

    @OneToMany(mappedBy = "workflowInstance", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<StatusModel> status;

}
