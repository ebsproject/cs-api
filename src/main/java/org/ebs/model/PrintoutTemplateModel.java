package org.ebs.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "printout_template", schema = "core")
@Getter
@Setter
@ToString
public class PrintoutTemplateModel extends Auditable {

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column
   private String name;

   @Column
   private String description;

   @Column
   private String zpl;

   @Column(name = "default_format")
   private String defaultFormat;

   @Column
   private String label;


   @ManyToMany(cascade = CascadeType.ALL)
   @JoinTable(name = "printout_template_contact", schema = "core", joinColumns = @JoinColumn(name = "printout_template_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "program_id", referencedColumnName = "id"))
   @ToString.Exclude
   List<ContactModel> programs;

   @ManyToMany(cascade = CascadeType.ALL)
   @JoinTable(name = "printout_template_product", schema = "core", joinColumns = @JoinColumn(name = "printout_template_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"))
   @ToString.Exclude
   List<ProductModel> products;

   @ManyToMany(mappedBy = "printoutTemplates")
   private Set<TenantModel> tenants = new HashSet<>();

   @PrePersist
   public void prePersist() {
      if (defaultFormat == null) {
         defaultFormat = "pdf";
      }
   }
}