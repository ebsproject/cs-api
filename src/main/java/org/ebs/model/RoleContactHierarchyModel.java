package org.ebs.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 

@Entity @Table(name="role_contact_hierarchy",schema="security")
@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(RoleContactHierarchyPK.class)
public class RoleContactHierarchyModel {

    @Id 
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="role_id", referencedColumnName = "id")
	private RoleModel role;
	
	@Id 
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="contact_hierarchy_id", referencedColumnName = "id")
	private HierarchyModel hierarchy;
    
}
