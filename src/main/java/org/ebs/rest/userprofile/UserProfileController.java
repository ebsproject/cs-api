package org.ebs.rest.userprofile;

import java.util.ArrayList;
import java.util.List;

import org.ebs.security.EbsUser;
import org.ebs.services.UserProfileService;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/")
@SecurityRequirement(name = "Bearer Token")
@Tag(name = "User Profile", description = "APIs related to users and hierarchy data")
@RequiredArgsConstructor
@Validated
public class UserProfileController {
    
    private final UserProfileService userProfileService;
    private final ObjectMapper om;

    private String buildBrAPIResponse(String payload) throws JsonMappingException, JsonProcessingException {

        om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

        String jsonResponse = new String();

        jsonResponse = om.writeValueAsString(BrapiResponseBuilder.forData(om.readTree(payload))
        .withStatusSuccess()
        .build());

        return jsonResponse;

    }

    private String buildBrAPIPagedResponse(JsonNode records, Integer pageSize, Integer pageNumber) throws JsonMappingException, JsonProcessingException {

        pageNumber = pageNumber == null ? 1 : pageNumber;

        pageSize = pageSize == null ? 100 : pageSize;

        if (pageNumber <= 0 || pageSize <= 0)
            return null;

        om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

        List<JsonNode> recordStrings = new ArrayList<>();
        if (records.isArray()) {
            for (JsonNode record : records) {
                recordStrings.add(record);
            }
        } else {
            recordStrings.add(records);
        }

        String jsonResponse = new String();

        PageRequest pageRequest = PageRequest.of(pageNumber - 1, pageSize);

        List<JsonNode> recordStringsInPage = new ArrayList<>();

        int totalElements = recordStrings.size();

        int start = (pageNumber - 1) * pageSize;

        int end = pageNumber * pageSize;

        if (start < totalElements) {
            recordStringsInPage = recordStrings.subList(start, Math.min(end, totalElements));
        }

        jsonResponse = om.writeValueAsString(
            BrapiResponseBuilder.forData(new PageImpl<>(recordStringsInPage, pageRequest, totalElements))
                .withStatusSuccess()
                .build());

        return jsonResponse;
        
    }

    private String buildBrAPIErrorResponse(String message) throws JsonProcessingException {

        om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

        String response = new String();

        response = om.writeValueAsString(BrapiResponseBuilder.forError(message));

        return response;
        
    }

    private String buildBrAPINotFoundResponse() throws JsonProcessingException {

        om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

        String response = new String();

        response = om.writeValueAsString(BrapiResponseBuilder.forNotFound());

        return response;
        
    }

    @RequestMapping(
        value = "/user/{id}",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get user profile by id")
    public ResponseEntity<String> getUser(@PathVariable("id") Integer id, 
        @RequestParam(required = false) String domainPrefix,
        @RequestParam(required = false) Integer programDbId,
        @AuthenticationPrincipal EbsUser user) {

        if (!user.isAdmin() && user.getId() != id) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
        }

        String profile = userProfileService.buildUserProfile(id, domainPrefix, programDbId);

        if (profile == null || profile.isBlank()) {
            return ResponseEntity.ok("user not found");
        } else if (profile.equals("Internal Error")) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(profile);
        } else {
            return ResponseEntity.ok(profile);
        }
        
    }

    @RequestMapping(
        value = "brapi/user/{id}",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get user profile by id, response in BrApi format")
    public ResponseEntity<String> getUserBrApi(@PathVariable("id") Integer id, 
        @RequestParam(required = false) String domainPrefix,
        @RequestParam(required = false) Integer programDbId,
        @AuthenticationPrincipal EbsUser user) {

        if (!user.isAdmin() && user.getId() != id) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
        }
        
        String profile = userProfileService.buildUserProfile(id, domainPrefix, programDbId);
        
        try {
            if (profile == null || profile.isBlank()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(buildBrAPINotFoundResponse());
            } else {
                JsonNode records = om.readTree(profile);
                profile = buildBrAPIPagedResponse(records, 100, 1);
                return ResponseEntity.ok(profile);
            }
        } catch(Exception e) {
            System.err.println(e);
            return ResponseEntity.internalServerError().body("{\"ERROR\":\"Server encountered a problem while processing the request\"}");
        }
        
    }

    @RequestMapping(
        value = "/user/external/{id}",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get user profile by external id (PersonDbId in Core Breeding)")
    public ResponseEntity<String> getUserWithExternalId(@PathVariable("id") Integer id,
        @RequestParam(required = false) String domainPrefix,
        @RequestParam(required = false) Integer programDbId,
        @AuthenticationPrincipal EbsUser user) {

        String profile = userProfileService.buildUserProfileExternalId(id, domainPrefix, programDbId, user);

        if (profile == null || profile.isBlank()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
        } else if (profile.equals("Unauthorized")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
        } else {
            return ResponseEntity.ok(profile);
        }
        
    }

    @RequestMapping(
        value = "brapi/user/external/{id}",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get user profile by external id (PersonDbId in Core Breeding), response in BrApi format")
    public ResponseEntity<String> getUserWithExternalIdBrApi(@PathVariable("id") Integer id,
        @RequestParam(required = false) String domainPrefix,
        @RequestParam(required = false) Integer programDbId,
        @AuthenticationPrincipal EbsUser user) {

        String profile = userProfileService.buildUserProfileExternalId(id, domainPrefix, programDbId, user);

        try {
            if (profile == null || profile.isBlank()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(buildBrAPINotFoundResponse());
            } else if (profile.equals("Unauthorized")) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
            } else {
                JsonNode records = om.readTree(profile);
                profile = buildBrAPIPagedResponse(records, 100, 1);
                return ResponseEntity.ok(profile);
            }
        } catch(Exception e) {
            return ResponseEntity.internalServerError().body("{\"ERROR\":\"Server encountered a problem while processing the request\"}");
        }
        
    }

    @RequestMapping(
        value = "/user/username/{username}",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get user profile by username/email (i.e. user@ebs.org)")
    public ResponseEntity<String> getUserByUsername(@PathVariable("username") String username,
        @RequestParam(required = false) String domainPrefix,
        @RequestParam(required = false) Integer programDbId,
        @AuthenticationPrincipal EbsUser user) {

        if (!user.isAdmin() && !user.getUsername().toLowerCase().equals(username.toLowerCase())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");  
        } 

        String profile = userProfileService.buildUserProfileUsername(username, domainPrefix, programDbId);

        if (profile == null || profile.isBlank()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
        } else {
            return ResponseEntity.ok(profile);
        }
        
    }

    @RequestMapping(
        value = "/user/{id}/programs",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get the program(s) a user belongs to using the user's id")
    public ResponseEntity<String> getUserPrograms(@PathVariable("id") Integer id) {

        String programs = userProfileService.getUserPrograms(id);

        if (programs.indexOf("does not exist in hierarchy chart") > 0 
            || programs.indexOf("does not belong to any program") > 0)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(programs);

        return ResponseEntity.ok(programs);
        
    }

    @RequestMapping(
        value = "brapi/user/{id}/programs",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get the program(s) a user belongs to using the user's id, response in BrApi format")
    @Hidden
    public ResponseEntity<String> getUserProgramsBrApi(@PathVariable("id") Integer id, 
        @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit) {

        String programs = userProfileService.getUserPrograms(id);

        try {
            if (programs.indexOf("does not exist") > 0
                || programs.indexOf("does not belong to any program") > 0)
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(buildBrAPINotFoundResponse());
            JsonNode records = om.readTree(programs).get("programs");
            programs = buildBrAPIPagedResponse(records, limit, page);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        if (programs == null)
            return ResponseEntity.badRequest().build();

        return ResponseEntity.ok(programs);
        
    }

    @RequestMapping(
        value = "/unit/{unitId}/members",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get the users of a unit using the unit's id")
    public ResponseEntity<String> getUnitMembers(@PathVariable("unitId") Integer unitId) {

        String members = userProfileService.getUnitMembers(unitId);

        if (members.indexOf("does not exist in hierarchy chart") > 0 
            || members.indexOf("Members not found for the specified unitId") > 0)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(members);

        return ResponseEntity.ok(members);
        
    }

    @RequestMapping(
        value = "/program/{programId}/members",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get the users of a program using the program's id")
    public ResponseEntity<String> getProgramMembers(@PathVariable("programId") Integer programId) {

        String members = userProfileService.getProgramMembers(programId);

        return ResponseEntity.ok(members);
        
    }

    @RequestMapping(
        value = "/product/{productId}/members",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Operation(summary = "Get the users with access to a particular product using the product's id")
    public ResponseEntity<String> getProductMembers(@PathVariable("productId") Integer productId) {

        String members = userProfileService.getProductMembers(productId);

        return ResponseEntity.ok(members);
        
    }
}