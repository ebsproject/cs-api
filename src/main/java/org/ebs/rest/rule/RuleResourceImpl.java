package org.ebs.rest.rule;

import java.util.List;
import java.util.Map;

import org.ebs.services.rule.SequenceRuleService;
import org.ebs.services.to.rule.SequenceRule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RuleResourceImpl implements RuleResource{

    private final SequenceRuleService ruleService;

    @Override
    public Page<SequenceRule> findAll(Pageable page) {
        log.debug("findAll for {}", page);
        return ruleService.findAll(page);
    }

    @Override
    public SequenceRule findById(int sequenceRuleId) {
        log.debug("findById({})", sequenceRuleId);
        return ruleService.findById(sequenceRuleId);
    }

    @Override
    public String next(int ruleId, Map<String, String> args) {
        log.debug("next for ({}) with params: {}", ruleId, args);
        return ruleService.next(ruleId, args);
    }

    @Override
    public List<String> next(int ruleId, int batchSize, Map<String, String> args) {
        log.debug("next for ({}) with params: {}", ruleId, args);
        return ruleService.nextBatch(ruleId, batchSize, args);
    }

}
