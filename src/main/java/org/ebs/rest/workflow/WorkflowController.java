package org.ebs.rest.workflow;

import org.ebs.services.WorkflowEngineService;
import org.ebs.services.redirect.MicroIntegrationService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Validated
@Hidden
public class WorkflowController {

    private final WorkflowEngineService workflowEngineService;
    
    @PostMapping("/workflow/move/node")
    public ResponseEntity<String> moveNode(
        @RequestHeader(value = HttpHeaders.AUTHORIZATION)
            String authorizations,
            @RequestBody
            String payload){
        MicroIntegrationService service = new MicroIntegrationService(WebClient
        .builder()
        .baseUrl("http://localhost:8290")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .defaultHeader(HttpHeaders.AUTHORIZATION, authorizations)
        .build());

        return ResponseEntity.ok(service.redirectRequestToMI("/workflow/engine/move/node", payload));
    }

    @PostMapping("/workflow/send/email")
    public ResponseEntity<String> sendEmail(
        @RequestHeader(value = HttpHeaders.AUTHORIZATION)
            String authorizations,
            @RequestBody
            String payload){
        MicroIntegrationService service = new MicroIntegrationService(WebClient
        .builder()
        .baseUrl("http://localhost:8290")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .defaultHeader(HttpHeaders.AUTHORIZATION, authorizations)
        .build());

        return ResponseEntity.ok(service.redirectRequestToMI("/workflow/engine/send/email", payload));
    }

    @PostMapping("/workflow/send/notification")
    public ResponseEntity<String> sendNotification(
        @RequestHeader(value = HttpHeaders.AUTHORIZATION)
            String authorizations,
            @RequestBody
            String payload){
        MicroIntegrationService service = new MicroIntegrationService(WebClient
        .builder()
        .baseUrl("http://localhost:8290")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .defaultHeader(HttpHeaders.AUTHORIZATION, authorizations)
        .build());

        return ResponseEntity.ok(service.redirectRequestToMI("/workflow/engine/send/notification", payload));
    }

    @PostMapping("/workflow/change/status")
    public ResponseEntity<String> changeStatusRedirect(
        @RequestHeader(value = HttpHeaders.AUTHORIZATION)
            String authorizations,
            @RequestBody
            String payload){
        MicroIntegrationService service = new MicroIntegrationService(WebClient
        .builder()
        .baseUrl("http://localhost:8290")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .defaultHeader(HttpHeaders.AUTHORIZATION, authorizations)
        .build());

        return ResponseEntity.ok(service.redirectRequestToMI("/workflow/engine/changeStatus", payload));
    }

    @RequestMapping(
        value = "/workflow/instance/{wfInstanceId}/record/{recordId}/move/{action}", 
        method = RequestMethod.POST, 
        produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> moveNode(@PathVariable("wfInstanceId") Integer instanceId,
    @PathVariable("recordId") Integer recordId, @PathVariable("action") String action) {

        String response = workflowEngineService.moveNode(instanceId, recordId, action);
        
        if (response.startsWith("400"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.substring(4));
        else if (response.startsWith("500"))
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.substring(4));

        return ResponseEntity.ok(response);

    }

    @RequestMapping(
        value = "/workflow/instance/{wfInstanceId}/record/{recordId}/toNode/{nodeId}", 
        method = RequestMethod.POST, 
        produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> moveToNode(@PathVariable("wfInstanceId") Integer instanceId,
    @PathVariable("recordId") Integer recordId, @PathVariable("nodeId") Integer nodeId) {

        String response = workflowEngineService.moveNode(instanceId, recordId, nodeId);
        
        if (response.startsWith("400"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.substring(4));
        else if (response.startsWith("500"))
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.substring(4));

        return ResponseEntity.ok(response);

    }

    @RequestMapping(
    value = "/workflow/changeStatus", 
    method = RequestMethod.POST, 
    produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> changeStatus(@RequestBody String payload) {

        String response = workflowEngineService.changeStatus(payload);
        
        if (response.indexOf("400 Error") > 0)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response.replace("400 Error ", ""));
        else if (response.indexOf("500 Error") > 0)
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.replace("500 Error ", ""));

        return ResponseEntity.ok(response);

    }

}
