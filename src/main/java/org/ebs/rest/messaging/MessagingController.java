package org.ebs.rest.messaging;

import java.util.List;

import org.ebs.security.EbsUser;
import org.ebs.services.MessagingService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Validated
@Hidden
public class MessagingController {

    //private final SendToMIService sendToMIService;

    private final MessagingService messagingService;
    
    @PostMapping(
        value = "/createJob",
        consumes = "application/json",
        produces = "application/json"
    )
    //@ResponseBody
    public ResponseEntity<String> createJob(@RequestBody String payload) {

        //sendToMIService.sendPayload(payload);

        String messsagingServiceResponse = messagingService.createJobLogFromPayload(payload);

        return ResponseEntity.ok(messsagingServiceResponse);
        
    }

    @PostMapping(
    value = "/createJobSimple",
    consumes = "application/json",
    produces = "application/json"
    )
    //@ResponseBody
    public ResponseEntity<String> createJobSimple(@RequestBody String payload) {

        String messsagingServiceResponse = messagingService.createNotificationSimple(payload);

        return ResponseEntity.ok(messsagingServiceResponse);
        
    }

    @PostMapping("/send/email")
    public ResponseEntity<String> sendEmail(@RequestParam String payload, 
        @RequestParam(name = "file", required = false) List<MultipartFile> multipartFiles) {
        String response = new String();
        try {
            response = messagingService.sendEmail(payload, multipartFiles);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().body("{\"message\":\"" + e.getMessage() + "\"}");
        }

        return ResponseEntity.ok("{\"message\":\"" + response + "\"}");
        
    }

    @PostMapping(
    value = "/print-report",
    consumes = "application/json",
    produces = "application/json"
    )
    public ResponseEntity<String> printReport(@RequestHeader(
        value = HttpHeaders.AUTHORIZATION) String authorizations,
        @RequestBody String payload, @AuthenticationPrincipal EbsUser user) {
        String messsagingServiceResponse = new String();
        try {
            messsagingServiceResponse = messagingService.printReport(payload, authorizations, user.getId());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().body("{\"message\":\"" + e.getMessage() + "\"}");
        }

        return ResponseEntity.ok("{\"message\":\"" + messsagingServiceResponse + "\"}");
        
    }

    @PostMapping(
    value = "/queue/gigwa-loader",
    consumes = "application/json",
    produces = "application/json"
    )
    public ResponseEntity<String> queueGigwaLoader(@RequestHeader(
        value = HttpHeaders.AUTHORIZATION) String authorizations,
        @RequestBody String payload) {
        String messsagingServiceResponse = new String();
        try {
            messsagingServiceResponse = messagingService.queueGigwaLoader(payload, authorizations);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.internalServerError().body("{\"message\":\"" + e.getMessage() + "\"}");
        }
        if (messsagingServiceResponse.startsWith("400 - "))
            return ResponseEntity.badRequest().body("{\"message\":\"" + messsagingServiceResponse.substring(6) + "\"}");
        else if (messsagingServiceResponse.startsWith("500 - "))
            return ResponseEntity.internalServerError().body("{\"message\":\"" + messsagingServiceResponse.substring(6) + "\"}");
        return ResponseEntity.ok("{\"message\":\"" + messsagingServiceResponse + "\"}");
        
    }

}