package org.ebs.rest.placemanager;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Facility {
    private int id;
    private String name;
    private String code;
    private String type;
    private String dateCreated;
    private String siteName;
    private JsonNode coordinates;
    private List<Subfacility> subfacilities = new ArrayList<>();
    private List<Container> containers = new ArrayList<>();
}
