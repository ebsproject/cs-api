package org.ebs.rest.placemanager;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Field {
    private int id;
    private String name;
    private String dateCreated;
    private JsonNode coordinates;
    private List<PlantingArea> plantingAreas = new ArrayList<>();
}
