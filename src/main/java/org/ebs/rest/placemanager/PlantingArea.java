package org.ebs.rest.placemanager;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PlantingArea {
    private int id;
    private String name;
    private String dateCreated;
    private JsonNode coordinates;
}
