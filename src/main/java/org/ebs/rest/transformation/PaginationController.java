package org.ebs.rest.transformation;

// import io.swagger.annotations.*;

import org.ebs.model.parameters.PaginationRemoveGetParameter;
import org.ebs.model.services.BreedingResponse;
import org.ebs.services.transformation.BreedingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import io.swagger.v3.oas.annotations.Hidden;

import java.util.HashMap;
import java.util.Map;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("/pagination")
@Hidden
// @Api(value = "Operation tu support other API or servers", tags = "util, pagination")
public class PaginationController {
    Logger logger = LoggerFactory.getLogger(PaginationController.class);

    @PostMapping("/remove/get")
    // @ApiOperation(value = "Remove the functionality of pagination and get all data from experiments, occurrences or plots")
    // @ApiResponses(@ApiResponse(code = 200, message = "Get all data without pagination"))
    public ResponseEntity<?> searchExperiment(
            @RequestHeader(value = HttpHeaders.AUTHORIZATION)
            // @ApiParam (name = "Authentication token from service", example = "Bearer {token}" ) 
            String authorizations,
            @RequestBody(required = true) 
            // @ApiParam(name = "URL to remove the pagination", required = true, type = "PaginationRemoveGetParameter") 
            PaginationRemoveGetParameter parameters) {
        Map<String, Object> message = new HashMap<>();

        BreedingService service = new BreedingService(WebClient
                .builder()
                .baseUrl(parameters.getPaginatedURL())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, authorizations)
                .build());
        BreedingResponse response = new BreedingResponse();
        int page = 1;
        response = service.getAnyPageWithPagination(parameters.getPaginatedURL(), page, response);

        if (response.getError() == null) {
            int totalPage = response.getTotalPage();
            for (int nowPage = 2; nowPage <= totalPage; nowPage++) {
                response = service.getAnyPageWithPagination(parameters.getPaginatedURL(), nowPage, response);
            }
            message.put("success", Boolean.TRUE);
            message.put("paginatedURL", parameters);
            message.put("result", response.getData());
            message.put("message", "");
            message.put("Total items", response.getData().size());
        } else {
            message.put("success", Boolean.FALSE);
            message.put("paginatedURL", parameters);
            message.put("result", response.getData());
            message.put("message", response.getError());
            message.put("Total items", 0);
        }
        return ResponseEntity.ok(message);
    }

}
