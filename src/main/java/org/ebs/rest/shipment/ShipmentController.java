package org.ebs.rest.shipment;

import org.ebs.security.EbsUser;
import org.ebs.services.ReorderItemsService;
import org.ebs.services.ShipmentStatusService;
import org.ebs.services.UpdateSMTAIdItemsService;
import org.ebs.services.UpdateShipmentItemsService;
import org.ebs.services.WorkflowService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@Validated
@Hidden
public class ShipmentController {

    private final ShipmentStatusService shipmentStatusService;
    private final UpdateShipmentItemsService updateShipmentItemsService;
    private final ReorderItemsService reorderItemsService;
    private final UpdateSMTAIdItemsService updateSMTAIdItemsService;
    private final WorkflowService workflowService;

    @RequestMapping(
        value = "/workflow/{workflowId}/instance/{instanceId}/move/{action}", 
        method = RequestMethod.GET, 
        produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> changeShipmentStatus(@PathVariable("workflowId") Integer workflowId,
            @PathVariable("instanceId") Integer instanceId, @PathVariable("action") String action) {

        String shipmentStatusResponse = shipmentStatusService.changeShipmentStatus(workflowId, instanceId, action);
        
        /* if (shipmentStatusResponse.startsWith("400"))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(shipmentStatusResponse.substring(4));
        else if (shipmentStatusResponse.startsWith("500"))
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(shipmentStatusResponse.substring(4));
        else {
            if (shipmentStatusResponse.substring(shipmentStatusResponse.indexOf(':') + 2).equals("Completed") && action.equals("back"))
                shipmentStatusResponse = shipmentStatusService.changeShipmentStatus(workflowId, instanceId, action);
            return ResponseEntity.ok(shipmentStatusResponse);
        } */

        return ResponseEntity.ok(shipmentStatusResponse);
            

    }

    @RequestMapping(
    value = "/shipment/{shipmentId}/items/graph-update", 
    method = RequestMethod.GET, 
    produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> updateShipmentItemsCBGraphQL(@PathVariable("shipmentId") Integer shipmentId) {

        String response = updateShipmentItemsService.updateSingleShipmentShipmentItemsCBGraphQL(shipmentId);

        if (response.indexOf("404 Error") >= 0)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.replace("404 Error ", ""));

        return ResponseEntity.ok(response);

    }

    @RequestMapping(
    value = "/shipment/{shipmentId}/items/all-data", 
    method = RequestMethod.GET, 
    produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> updateShipmentItemsAllData(@PathVariable("shipmentId") Integer shipmentId,
        @AuthenticationPrincipal EbsUser secUser) {

        updateShipmentItemsService.updateSingleShipmentItemsAllData(shipmentId, secUser.getId());

        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Request successfully received");

    }

    @RequestMapping(
    value = "/list/{listId}/items/update", 
    method = RequestMethod.GET, 
    produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> updateListItems(@PathVariable("listId") Integer listId) {

        String response = updateShipmentItemsService.updateListItemsCB(listId);

        if (response.indexOf("404 Error") >= 0)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.replace("404 Error ", ""));

        return ResponseEntity.ok(response);

    }

    @RequestMapping(value = "/reorder-items/{entity}/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> reorderItems(@PathVariable("entity") String entity, @PathVariable("id") Integer id) {

        String response = reorderItemsService.reorderItems(id, entity);

        if (response.indexOf("404 Error") >= 0)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.replace("404 Error ", ""));

        return ResponseEntity.ok(response);

    }
    @RequestMapping(value = "/assign-smta/{smtaId}/{mtaStatus}/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> updateSmtaIdItems(@PathVariable("smtaId") String smtaId,@PathVariable("mtaStatus") String mtaStatus, @PathVariable("id") Integer id) {

        String response = updateSMTAIdItemsService.updateSmtaIdItems(id, smtaId, mtaStatus);

        if (response.indexOf("404 Error") >= 0)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response.replace("404 Error ", ""));
        return ResponseEntity.ok(response);

    }
    @RequestMapping(value = "/workflow/import", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> uploadWorkflowJsonFile(@RequestParam("jsonFile") MultipartFile file) {

        return workflowService.importWorkflow(file);
    }


}
