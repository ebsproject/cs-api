package org.ebs.rest.redirection;

import java.util.Map;

import org.ebs.model.parameters.NotificationParameter;
import org.ebs.services.redirect.MicroIntegrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import io.swagger.v3.oas.annotations.Hidden;


@RestController
@RequestMapping("/notification")
@Hidden
public class MicroIntegrationController {
    Logger logger = LoggerFactory.getLogger(MicroIntegrationController.class);

    @PostMapping("/v2/send")
    public ResponseEntity<?> redirectNotification(
        @RequestHeader(value = HttpHeaders.AUTHORIZATION)
            String authorizations,
            @RequestBody
            NotificationParameter body){
        MicroIntegrationService service = new MicroIntegrationService(WebClient
        .builder()
        .baseUrl("http://localhost:8290")
        .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        .defaultHeader(HttpHeaders.AUTHORIZATION, authorizations)
        .build());

        return ResponseEntity.ok(service.postNotification(body));
    }
    
}
