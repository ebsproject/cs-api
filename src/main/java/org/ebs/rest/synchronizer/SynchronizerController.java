package org.ebs.rest.synchronizer;

import java.util.UUID;

import org.ebs.model.UserModel;
import org.ebs.model.repos.UserRepository;
import org.ebs.security.EbsUser;
import org.ebs.services.MessagingService;
import org.ebs.services.SynchronizeProgramService;
import org.ebs.services.SynchronizeUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/sync")
@SecurityRequirement(name = "Bearer Token")
@RequiredArgsConstructor
@Validated
@Hidden
@Slf4j
public class SynchronizerController {

    private final SynchronizeProgramService synchronizeProgramService;
    private final SynchronizeUserService synchronizeUserService;
    private final MessagingService messagingService;
    private final UserRepository userRepository;

    @RequestMapping(
        value = "/organizations",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Hidden
    public ResponseEntity<String> synchronizeOrganizations(@AuthenticationPrincipal EbsUser secUser) {

        try {
            synchronizeProgramService.synchronizeOrganizations();
            
            UserModel user = userRepository.findById(secUser.getId()).orElse(null);
            if (user != null && user.getContact() != null) {
                UUID uuid = UUID.randomUUID();
                String notificationPayload = "{" + //
                                "\"recordId\" : 1," + //
                                "\"jobWorkflowId\" : 3," + //
                                "\"message\" : {\"message\": {\"status\": \"Complete\", \"title\": \"Sync job complete\", \"description\": \"Programs synchronization job has finished\"}}," + //
                                "\"contacts\" : [" + user.getContact().getId() + "]," + //
                                "\"jobTrackId\": \"" + uuid + "\"" + //
                                "}";
                
                messagingService.createNotificationSimple(notificationPayload);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error synchronizing organizations");
        }
        return ResponseEntity.ok("Success");
    }

    @RequestMapping(
        value = "/programs",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Hidden
    public ResponseEntity<String> synchronizePrograms(@AuthenticationPrincipal EbsUser secUser) {

        try {
            synchronizeProgramService.synchronizePrograms();
            
            UserModel user = userRepository.findById(secUser.getId()).orElse(null);
            if (user != null && user.getContact() != null) {
                UUID uuid = UUID.randomUUID();
                String notificationPayload = "{" + //
                                "\"recordId\" : 1," + //
                                "\"jobWorkflowId\" : 3," + //
                                "\"message\" : {\"message\": {\"status\": \"Complete\", \"title\": \"Sync job complete\", \"description\": \"Programs synchronization job has finished\"}}," + //
                                "\"contacts\" : [" + user.getContact().getId() + "]," + //
                                "\"jobTrackId\": \"" + uuid + "\"" + //
                                "}";
                
                messagingService.createNotificationSimple(notificationPayload);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error synchronizing programs");
        }
        return ResponseEntity.ok("Success");
    }

    @RequestMapping(
        value = "/users",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Hidden
    public ResponseEntity<String> synchronizeUsers(@AuthenticationPrincipal EbsUser secUser) {
        UUID uuid = UUID.randomUUID();
        log.info("\n\n[{}]Start on demand request to sync users from CB...\n\n", uuid);
        try {
            synchronizeUserService.synchronizeUsers();
            /* UserModel user = userRepository.findById(secUser.getId()).orElse(null);
            if (user != null && user.getContact() != null) {
                UUID uuid = UUID.randomUUID();
                String notificationPayload = "{" + //
                                "\"recordId\" : 1," + //
                                "\"jobWorkflowId\" : 3," + //
                                "\"message\" : {\"message\": {\"status\": \"Complete\", \"title\": \"Sync job complete\", \"description\": \"Users synchronization job has finished\"}}," + //
                                "\"contacts\" : [" + user.getContact().getId() + "]," + //
                                "\"jobTrackId\": \"" + uuid + "\"" + //
                                "}";
                
                messagingService.createNotificationSimple(notificationPayload);
            } */
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("[" + uuid + "]Error synchronizing users");
        }
        log.info("\n\n\n[{}]Finish on demand request to sync users from CB\n\n\n", uuid);
        return ResponseEntity.ok("Success");
    }

    @RequestMapping(
        value = "/user/{userId}",
        method = RequestMethod.GET,
        produces = "application/json"
    )
    @ResponseBody
    @Hidden
    public ResponseEntity<String> synchronizeUser(@PathVariable(name = "userId") Integer userId) {
        String response = new String();
        try {
            if (userId != null && userId > 0)
                response = synchronizeUserService.synchronizePerson(userId);
            else
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid user id");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error synchronizing user");
        }
        return ResponseEntity.ok(response);
    }
}
