package org.ebs.rest.importfiles;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.ebs.model.parameters.BasicResponse;
import org.ebs.services.ImportService;
import org.ebs.services.OccurrenceShipmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.RequiredArgsConstructor;
 
@RestController
@RequiredArgsConstructor
@Hidden
public class ImportFilesController {

    private final ImportService importService;
    private final OccurrenceShipmentService occurrenceShipmentService;
     
    @PostMapping("/importFiles/institution")
    public ResponseEntity<String> uploadFileInstitution(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        
        String response = importService.importInstitutions(multipartFile);

        if (response.contains("Successfully"))
            return new ResponseEntity<>(response, HttpStatus.OK);
        else
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PostMapping("/importFiles/person")
    public ResponseEntity<String> uploadFilePerson(@RequestParam("file") MultipartFile multipartFile) throws IOException {
         
        String response = importService.importPersons(multipartFile);
    
        if (response.contains("Successfully"))
            return new ResponseEntity<>(response, HttpStatus.OK);
        else
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            
    }

    @PostMapping("/importFiles/shipmentItems")
    public ResponseEntity<JsonNode> importShipmentItems(@RequestParam("file") MultipartFile multipartFile,
        @RequestParam("shipmentId") Integer shipmentId, @RequestParam("entity") String entity)
                    throws Exception {
        List<String> entities = Arrays.asList("shipment", "workflow");
        if (multipartFile == null || shipmentId == null || entity == null || entity.isBlank()
            || !entities.contains(entity)) {
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(false, "invalid parameters/values"));
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
                        
        String response;
        try {
            response = importService.importShipmentItems(multipartFile, entity, shipmentId);
        } catch (Exception e) {
            e.printStackTrace();
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(false, "internal error - " + e.getMessage()));
            return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    
        if (response.contains("success")) {
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(true, "process completed successfully"));
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(false, "internal error"));
            return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/importFiles/seeds")
    public ResponseEntity<JsonNode> importSeeds(@RequestParam("file") MultipartFile multipartFile,
        @RequestParam("shipmentId") Integer shipmentId, @RequestParam("entity") String entity)
                    throws Exception {
        List<String> entities = Arrays.asList("shipment", "workflow");
        if (multipartFile == null || shipmentId == null || entity == null || entity.isBlank()
            || !entities.contains(entity)) {
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(false, "invalid parameters/values"));
            return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
        }
                        
        String response;
        try {
            response = importService.importSeeds(multipartFile, entity, shipmentId);
        } catch (Exception e) {
            e.printStackTrace();
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(false, "internal error - " + e.getMessage()));
            return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    
        if (response != null && response.contains("success")) {
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(true, "process completed successfully, " + response.split(":")[1]));
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            JsonNode res = new ObjectMapper().valueToTree(new BasicResponse(false, "internal error"));
            return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/importFiles/occurrenceShipment")
    public ResponseEntity<String> importOccurrenceShipments(@RequestParam("file") MultipartFile multipartFile, @RequestParam("serviceId") Integer serviceId) throws Exception {

        if (multipartFile == null || serviceId == null) {
            return new ResponseEntity<>("missing parameters", HttpStatus.BAD_REQUEST);
        }

        String response;
        try {
            response = Integer.toString(occurrenceShipmentService.importOccurrenceShipments(multipartFile, serviceId));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Error: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    
        return new ResponseEntity<>("Successfully imported " + response + " occurrence shipment(s)", HttpStatus.OK);
    }

}  