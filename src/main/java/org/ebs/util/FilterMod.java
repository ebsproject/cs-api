package org.ebs.util;

public enum FilterMod {
    EQ,
    LK,
    NULL,
    NOTNULL,
    BEFORE,
    AFTER;
}
