package org.ebs.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.ebs.model.ContactModel;
import org.ebs.model.repos.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class FileUpload {

    private final UserRepository userRepository;
    private String institutionsPath = "/home/wso2carbon/import-files/institutions/in";
    private String personsPath = "/home/wso2carbon/import-files/persons/in";

    public String saveFile(String fileName, MultipartFile multipartFile, int userId)
            throws IOException {
        
        ContactModel contact = userRepository.findById(userId).map(u -> u.getContact()).orElse(null);

        if (contact == null)
            return "could not save file: there is no contact to be notified about the process";

        Path uploadPath = Paths.get("/home/wso2carbon/discarded");

        if (fileName.contains("institutions"))
            uploadPath = Paths.get(institutionsPath);
        else if (fileName.contains("persons"))
            uploadPath = Paths.get(personsPath);
          
        if (!Files.exists(uploadPath)) {
            System.out.println("Creating directory " + uploadPath);
            Files.createDirectories(uploadPath);
        }
         
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(contact.getId() + "-" + fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {    
            ioe.printStackTrace();   
            return "could not save file: " + fileName;
        }

        return "file successfully uploaded for processing";
    }
}