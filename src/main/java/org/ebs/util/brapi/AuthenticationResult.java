package org.ebs.util.brapi;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@ToString
public class AuthenticationResult {
    @JsonProperty("AccessToken")
    private String accessToken;
    @JsonProperty("IdToken")
    private String idToken;
    @JsonProperty("RefreshToken")
    private String refreshToken;
    @JsonProperty("ExpiresIn")
    private int expiresIn;
}