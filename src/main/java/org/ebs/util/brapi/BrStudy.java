package org.ebs.util.brapi;

import lombok.Data;

@Data
public class BrStudy {
    private String studyName;
    private String studyType;
    private String trialDbId;
    private String studyDbId;
}
