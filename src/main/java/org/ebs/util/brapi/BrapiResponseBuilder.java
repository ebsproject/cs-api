package org.ebs.util.brapi;

import java.util.ArrayList;

import org.springframework.data.domain.Page;

/**
 * Convenient class for dispatching any service response in BrAPI format.
 * @author JAROJAS
 *
 */
public class BrapiResponseBuilder {

private static BrapiResponseBuilder responseBuilder = new BrapiResponseBuilder();
	
	private BrapiResponseBuilder(){}

	/**
	 * Builder's entry point
	 * @param payload to add in the response
	 * @return intermediate {@link Response} for further configuration
	 */
	public static <T>BasicBuilder<T> forData(T payload){
		return responseBuilder.new BasicBuilder<T>(payload);
	}

	public static <T>PagedBuilder<T> forData(Page<T> payload){
		return responseBuilder.new PagedBuilder<T>(payload);
	}

	public static BrResponse<String> forNotFound(){
		return responseBuilder.new BasicBuilder<String>("")
			.withStatusNotFound()
			.build();
	}
	
	public static BrResponse<String> forError(String errorMessage){
	return responseBuilder.new BasicBuilder<String>("")
			.withStatusError(errorMessage)
			.build();
	}

	
	private class Builder<T> {
		protected BrMetadata meta;
		protected T payload;
		protected Page<T> payloadPage;
		
		private Builder(T payload){ 
			meta = new BrMetadata();
			meta.setStatus(new ArrayList<>());
			meta.setDataFiles(new ArrayList<>());
			this.payload = payload;
		}
		private Builder(Page<T> payloadPage){
			meta = new BrMetadata();
			meta.setStatus(new ArrayList<>());
			meta.setDataFiles(new ArrayList<>());
			this.payloadPage = payloadPage;
		}
		
		private void addStatusSuccess(){
			addStatus(BrStatus.SUCCESS);
		}
		private void addStatusNotFound(){
			addStatus(BrStatus.NOT_FOUND_STATUS);
		}
		private void addStatusError(){
			addStatus(BrStatus.ERROR);
		}
		private void addStatusError(String message){
			addStatus(new BrStatus(BrStatus.INTERNAL, message));
		}
		
		private void addStatus(BrStatus status){
			meta.getStatus().add(status);			
		}
		
		private void addDataFile(String dataFileUrl){
			meta.getDataFiles().add(dataFileUrl);
		}
		
		private void withNoPagination() {
			meta.setPagination(
				new BrPagination()
			);
		}
		private void withPagination(Page<T> page) {
			addPagination(page);
		}
		
		private void addPagination(Page<?> page){
			meta.setPagination(
					new BrPagination(page.getSize(),
							page.getNumber() + 1,
							page.getTotalElements(),
							page.getTotalPages()));
		}
		
	}
	
	public class BasicBuilder<T> extends BrapiResponseBuilder.Builder<T>{
			private BasicBuilder(T payload){
				super(payload);
				super.withNoPagination();
			}
		
			public BrResponse<T> build(){
				BrResponse<T> response = new BrResponse<T>();
				response.setMetadata(this.meta);
				response.setResult(this.payload);
			
				return response;
			}
			
			public BasicBuilder<T> withStatusSuccess(){
				super.addStatusSuccess();
				return this;
			}
			public BasicBuilder<T> withStatusNotFound(){
				super.addStatusNotFound();
				return this;
			}
			public BasicBuilder<T> withStatusError(){
				super.addStatusError();
				return this;
			}
			public BasicBuilder<T> withStatusError(String message){
				super.addStatusError(message);
				return this;
			}
			public BasicBuilder<T> withDataFile(String dataFileUrl){
				super.addDataFile(dataFileUrl);
				return this;
			}
			
			
	}

	public class PagedBuilder<T> extends BrapiResponseBuilder.Builder<T>{
		private PagedBuilder(Page<T> payloadPage){
			super(payloadPage);
			super.withPagination(payloadPage);
		}
		
		public BrPagedResponse<T> build() {
			BrPagedResponse<T> response = new BrPagedResponse<T>();
			response.setMetadata(this.meta);
			response.setResult(new BrResult<T>(this.payloadPage.getContent()));
			
			return response;
		}

		public PagedBuilder<T> withStatusSuccess(){
			super.addStatusSuccess();
			return this;
		}
		public PagedBuilder<T> withStatusError(){
			super.addStatusError();
			return this;
		}
		public PagedBuilder<T> withStatusError(String message){
			super.addStatusError(message);
			return this;
		}
		public PagedBuilder<T> withDataFile(String dataFileUrl){
			super.addDataFile(dataFileUrl);
			return this;
		}
	}
}
