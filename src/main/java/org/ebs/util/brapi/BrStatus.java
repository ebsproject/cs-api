package org.ebs.util.brapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models status section of a BrAPI response as well as status codes and
 * messages
 * 
 * @author JAROJAS
 *
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
class BrStatus {

    static final String INFO = "INFO";
    static final String NOT_FOUND = "NotFound";
    static final String INTERNAL = "Internal";
    static final BrStatus SUCCESS = new BrStatus("Request has been successfully completed.", INFO);
    static final BrStatus NOT_FOUND_STATUS = new BrStatus("The record you have requested does not exist.", NOT_FOUND);
    static final BrStatus ERROR = new BrStatus("Internal Error. An error has occurred while processing your request.", INTERNAL);

    private String message;
    private String messageType;

}
