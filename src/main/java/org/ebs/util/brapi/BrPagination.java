package org.ebs.util.brapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Models pagination section of a BrAPI response
 *
 * @author JAROJAS
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BrPagination {

    private Integer pageSize;
    private Integer currentPage;
    private Long totalCount;
    private Integer totalPages;

}
