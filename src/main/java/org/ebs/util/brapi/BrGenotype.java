package org.ebs.util.brapi;

import lombok.Data;

@Data
public class BrGenotype {
    private String[] values;
}
