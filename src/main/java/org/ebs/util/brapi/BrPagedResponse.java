package org.ebs.util.brapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Global format for any paged BrAPI call.
 *
 * @author JAROJAS
 *
 * @param <T>
 *            the payload type in this response
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BrPagedResponse<T> {

    private BrMetadata metadata = new BrMetadata();
    /**
     * Contains the payload
     */
    private BrResult<T> result;

}
