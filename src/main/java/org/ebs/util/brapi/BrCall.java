package org.ebs.util.brapi;

import java.util.Map;

import lombok.Data;

@Data
public class BrCall {
    private String callSetDbId;
    private String variantDbId;
    private Map<String,String> additionalInfo;
    private BrGenotype genotype;
}
