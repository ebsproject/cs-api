package org.ebs.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.model.ContactModel;
import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "user", schema = "security")
@Getter
@Setter
@ToString
public class SecUserModel extends Auditable {

   private static final long serialVersionUID = 1L;

   @Id
   @Column
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;

   @Column(name = "user_name")
   private String name;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "contact_id")
   private ContactModel contact;

   @Column(name = "is_admin")
   private boolean isAdmin;
}