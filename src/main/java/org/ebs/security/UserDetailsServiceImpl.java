package org.ebs.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.log4j.Log4j2;

/**
 * Service required by Spring Security to obtain additional information about
 * users
 *
 * @author jarojas
 *
 */
@Service
@Transactional(readOnly=true)
@Profile({"default", "prod"})
@Log4j2
class UserDetailsServiceImpl implements UserDetailsService {

    private final SecUserRepository userRepo;

    public UserDetailsServiceImpl(SecUserRepository userRepo) {
        log.info("Creating UserDetailsService filter for PRODUCTION");
        this.userRepo = userRepo;
    }

	@Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SecUserModel u = userRepo.findByNameAndDeletedIsFalseIgnoreCase(username.toLowerCase())
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        if (u.getContact() == null || u.getContact().isDeleted())
            throw new UsernameNotFoundException("User not found");

        List<GrantedAuthority> auths = new ArrayList<>();

        if (u.isAdmin())
            auths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        u.getContact().getParents().forEach(p -> {
            p.getRoles().forEach(role -> {
                auths.add(new SimpleGrantedAuthority("ROLE_" + role.getName().toUpperCase()));
                role.getProductfunctions().forEach(profuctFunction -> {
                    auths.add(new SimpleGrantedAuthority(profuctFunction.getProduct().getName() + "_" + profuctFunction.getAction()));
                });
            });
        });

        return new EbsUser(u.getId(), username, auths, true, u.isAdmin());
    }

}