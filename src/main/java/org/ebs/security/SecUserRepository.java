package org.ebs.security;

import java.util.Optional;

import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

interface SecUserRepository extends JpaRepository<SecUserModel, Integer>,
   RepositoryExt<SecUserModel>{

      Optional<SecUserModel> findByIdAndDeletedIsFalse(int id);

      Optional<SecUserModel> findByNameAndDeletedIsFalse(String username);

      Optional<SecUserModel> findByNameAndDeletedIsFalseIgnoreCase(String username);
}