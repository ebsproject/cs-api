package org.ebs;

import java.util.concurrent.TimeUnit;

import org.ebs.model.repos.TenantRepository;
import org.ebs.services.HierarchyService;
import org.ebs.services.JobLogService;
import org.ebs.services.SynchronizeProgramService;
import org.ebs.services.SynchronizeUserService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class SchedulingConfig {

    private final JobLogService jobLogService;
    private final SynchronizeUserService synchronizeUserService;
    private final SynchronizeProgramService synchronizeProgramService;
    private final TenantRepository tenantRepository;
    private final HierarchyService hierarchyService;
    private static final int DEFAULT_TENANT = 1;

    @Scheduled(fixedDelayString = "5", initialDelayString = "0", timeUnit = TimeUnit.MINUTES)
    public void callSync() {
        if (performSync())
            if (synchronizeProgramService.synchronizeOrganizations())
                if (synchronizeProgramService.synchronizePrograms())
                    synchronizeUserService.synchronizeUsers();
    }

    @Scheduled(fixedDelayString = "12", timeUnit = TimeUnit.HOURS)
    public void clearNotifications() {
        jobLogService.clearJobLogs();
    }

    private boolean performSync() {
        return tenantRepository.findById(DEFAULT_TENANT)
            .map(t -> t.getSync()).orElse(false);
    }

    @Scheduled(fixedDelayString = "6", timeUnit = TimeUnit.HOURS)
    public void cleanHierarchies() {
        hierarchyService.cleanHierarchies();
    }

}
