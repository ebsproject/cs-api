package org.ebs;

import java.sql.Types;

import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;

import org.hibernate.dialect.PostgreSQL94Dialect;

public class PostgreSQL94DialectJson extends PostgreSQL94Dialect {

    public PostgreSQL94DialectJson() {
        super();
        this.registerHibernateType(Types.OTHER, JsonNodeBinaryType.class.getName());
    }
}