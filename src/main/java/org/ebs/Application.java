package org.ebs;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.ebs.graphql.client.GraphQLClient;
import org.ebs.services.to.JobLogTo;
import org.ebs.util.DateCoercing;
import org.ebs.util.DateTimeCoercing;
import org.ebs.util.JsonCoercing;
import org.ebs.util.UUIDCoercing;
import org.ebs.util.brapi.BrapiClient;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import graphql.kickstart.tools.GraphQLResolver;
import graphql.kickstart.tools.SchemaParser;
import graphql.parser.ParserOptions;
import graphql.schema.GraphQLScalarType;
import graphql.schema.GraphQLSchema;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.Sinks.Many;
import reactor.util.concurrent.Queues;

@SpringBootApplication
@EnableAsync
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public static ThreadLocal<String> REQUEST_TOKEN = new ThreadLocal<>();
    public static Map<String, String> TOKEN_MAP = new ConcurrentHashMap<>();

    @Value("${ebs.cb.api.endpoint}")
    private String cbApiEndpoint;

    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;
    
    @Bean
    public GraphQLScalarType dateScalar() {
        return GraphQLScalarType.newScalar().name("Date")
                .description("Date Scalar, custom implementation").coercing(new DateCoercing())
                .build();
    }

    @Bean
    public GraphQLScalarType datetimeScalar() {
        return GraphQLScalarType.newScalar().name("Datetime")
                .description("Datetime Scalar, custom implementation")
                .coercing(new DateTimeCoercing()).build();
    }

    @Bean
    public GraphQLScalarType uuidScalar() {
        return GraphQLScalarType.newScalar().name("UUID")
                .description("Uuid Scalar, custom implementation").coercing(new UUIDCoercing())
                .build();
    }

    @Bean
    public GraphQLScalarType jsonScalar() {
        return GraphQLScalarType.newScalar().name("Json")
                .description("Json Scalar, custom implementation").coercing(new JsonCoercing())
                .build();
    }

    @Bean
    BrapiClient brapiClientCB(TokenGenerator tokenGenerator) {
        return new BrapiClient(URI.create(cbApiEndpoint), tokenGenerator);
    }

    @Bean
    GraphQLClient graphQLClient(TokenGenerator tokenGenerator) {
        return new GraphQLClient(cbGraphQlEndpoint, tokenGenerator);
    }

    @Bean
    GraphQLSchema schema(List<GraphQLResolver<?>> resolvers, List<GraphQLScalarType> scalars) {
        ParserOptions.setDefaultParserOptions(ParserOptions.newParserOptions().maxTokens(25000).build());
        return SchemaParser.newParser().file("schema.graphqls").resolvers(resolvers)
                .scalars(scalars).build().makeExecutableSchema();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public Many<JobLogTo> jobLogSink() {
      return Sinks.many().multicast().onBackpressureBuffer(Queues.SMALL_BUFFER_SIZE, false);
    }
  
    @Bean
    public Flux<JobLogTo> jobLogFlux(Many<JobLogTo> joblogSink) {
      return joblogSink.asFlux();
    }
}
