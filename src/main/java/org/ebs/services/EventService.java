package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.EventInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface EventService {

    public Optional<EventTo> findEvent(int id);

    public Page<EventTo> findEvents(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public EventTo createEvent(EventInput input);

    public EventTo modifyEvent(EventInput input);

    public int deleteEvent(int id);

    public StageTo findStage(int eventId);

    public TenantTo findTenant(int eventId);

    //public InstanceTo findInstance(int eventId);

    public NodeTo findNode(int eventId);

    public WorkflowInstanceTo findWorkflowInstance(int eventId);

    public List<CFValueTo> findCFValues(int eventId);
    
}
