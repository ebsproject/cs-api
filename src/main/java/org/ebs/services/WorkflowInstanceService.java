package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.StatusTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.WorkflowInstanceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface WorkflowInstanceService {
    
    public Optional<WorkflowInstanceTo> findWorkflowInstance(int id);

    public Page<WorkflowInstanceTo> findWorkflowInstances(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public WorkflowInstanceTo createWorkflowInstance(WorkflowInstanceInput input);

    public WorkflowInstanceTo modifyWorkflowInstance(WorkflowInstanceInput input);

    public int deleteWorkflowInstance(int id);

    public WorkflowTo findWorkflow(int workflowInstanceId);

    public StatusTo findStatus(int workflowInstanceId);

}