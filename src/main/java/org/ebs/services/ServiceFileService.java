package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.ServiceFileTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.ServiceFileInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ServiceFileService {

    public Optional<ServiceFileTo> findServiceFile(int id);

    public Page<ServiceFileTo> findServiceFiles(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public ServiceFileTo createServiceFile(ServiceFileInput input);

    public ServiceFileTo modifyServiceFile(ServiceFileInput input);

    public int deleteServiceFile(int id);

    public ServiceTo findService(int filesId);

    public FileObjectTo findFileObject(int serviceFileId);

    public TenantTo findTenant(int serviceFileId);

    public FileTypeTo findFileType(int serviceFileId);

    UserTo findCreatedBy(int serviceFileId);

    UserTo findUpdatedBy(int serviceFileId);

}
