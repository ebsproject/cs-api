package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.WorkflowInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
public interface WorkflowService {

    public Optional<WorkflowTo> findWorkflow(int id);

    public Page<WorkflowTo> findWorkflows(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public WorkflowTo createWorkflow(WorkflowInput input);
    
    public ResponseEntity<String> importWorkflow(MultipartFile file);

    public WorkflowTo modifyWorkflow(WorkflowInput input);

    public int deleteWorkflow(int id);

    public HtmlTagTo findHtmlTag(int workflowId);

    public ProductTo findProduct(int workflowId);

    public List<PhaseTo> findPhases(int workflowId);

    public List<StatusTypeTo> findStatusTypes(int workflowId);

    public Page<JsonNode> getWorkflowCustomFieldList(List<FilterInput> filters,
    List<SortInput> sorts, PageInput page, boolean disjunctionFilters);

    public List<ContactWorkflowsTo> findContacts(int workflowId);
    
}