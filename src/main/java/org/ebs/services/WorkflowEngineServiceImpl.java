package org.ebs.services;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.ebs.model.CFValueModel;
import org.ebs.model.EventModel;
import org.ebs.model.NodeModel;
import org.ebs.model.StatusModel;
import org.ebs.model.StatusTypeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.model.repos.CFValueRepository;
import org.ebs.model.repos.EventRepository;
import org.ebs.model.repos.NodeRepository;
import org.ebs.model.repos.StatusRepository;
import org.ebs.model.repos.StatusTypeRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowInstanceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class WorkflowEngineServiceImpl implements WorkflowEngineService {

    private final EventRepository eventRepository;
    private final WorkflowInstanceRepository workflowInstanceRepository;
    private final NodeRepository nodeRepository;
    private final CFValueRepository cfValueRepository;
    private final StatusTypeRepository statusTypeRepository;
    private final StatusRepository statusRepository;
    private final TenantRepository tenantRepository;

    private void traverseFront(List<NodeModel> nodes, WorkflowInstanceModel wfInstance, int recordId, TenantModel defaultTenant) {
        for (NodeModel node : nodes) {
            EventModel currentEvent = eventRepository.findByNodeAndWorkflowInstanceAndDeletedIsFalse(node, wfInstance)
                .orElse(null);
            if (currentEvent == null) {
                currentEvent = new EventModel();
                currentEvent.setRecordId(recordId);
                currentEvent.setWorkflowInstance(wfInstance);
                currentEvent.setNode(node);
                currentEvent.setStage(node.getStages().stream().filter(
                    s -> s.getPhase().getWorkflow().equals(wfInstance.getWorkflow())
                ).findFirst().orElse(null));
                currentEvent.setTenant(wfInstance.getWorkflow().getTenants().stream().findFirst().orElse(defaultTenant));
                if (nodes.indexOf(node) + 1 < nodes.size())
                    currentEvent.setCompleted(new Date());
            } else {
                if (nodes.indexOf(node) + 1 < nodes.size())
                    currentEvent.setCompleted(new Date());
            }
            eventRepository.save(currentEvent);
        }
    }

    private void traverseBack(List<NodeModel> nodes, WorkflowInstanceModel wfInstance, int recordId, TenantModel defaultTenant) {
        Collections.reverse(nodes);
        for (NodeModel node : nodes) {
            EventModel currentEvent = eventRepository.findByNodeAndWorkflowInstanceAndDeletedIsFalse(node, wfInstance)
                .orElse(null);
            if (currentEvent == null) {
                currentEvent = new EventModel();
                currentEvent.setRecordId(recordId);
                currentEvent.setWorkflowInstance(wfInstance);
                currentEvent.setNode(node);
                currentEvent.setStage(node.getStages().stream().filter(
                    s -> s.getPhase().getWorkflow().equals(wfInstance.getWorkflow())
                ).findFirst().orElse(null));
                currentEvent.setTenant(wfInstance.getWorkflow().getTenants().stream().findFirst().orElse(defaultTenant));
            } else {
                currentEvent.setCompleted(null);
            }
            eventRepository.save(currentEvent);
        }
    }

    @Override
    public String moveNode(int workflowInstanceId, int recordId, int targetNodeId) {

        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        WorkflowInstanceModel workflowInstance = workflowInstanceRepository.findById(workflowInstanceId).orElse(null);

        if (workflowInstance == null) {
            jsonObjectBuilder.add("error", "400 Error: Workflow instance with id " + workflowInstanceId + " does not exist");
            return jsonObjectBuilder.build().toString();
        }

        NodeModel targetNode = nodeRepository.findById(targetNodeId).orElse(null);

        if (targetNode == null) {
            jsonObjectBuilder.add("error", "400 Error: Node with id " + targetNodeId + " does not exist");
            return jsonObjectBuilder.build().toString();
        }

        TenantModel defaultTenant = tenantRepository.findById(1).get();

        List<EventModel> events = eventRepository.findByWorkflowInstanceAndCompletedIsNullAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAscNodeSequenceAsc(workflowInstance);
        
        List<NodeModel> workflowNodes = nodeRepository.findWorkflowNodes(workflowInstance.getWorkflow().getId());

        if (workflowNodes.isEmpty()) {
            jsonObjectBuilder.add("error", "400 Error: No node definitions found for this workflow");
            return jsonObjectBuilder.build().toString();
        }

        if (!workflowNodes.contains(targetNode)) {
            jsonObjectBuilder.add("error", "400 Error: Target node is not a part of this workflow");
            return jsonObjectBuilder.build().toString();
        }

        events.forEach(e -> {
            e.setCompleted(new Date());
            eventRepository.save(e);
        });

        EventModel newEvent = new EventModel();
        newEvent.setRecordId(recordId);
        newEvent.setWorkflowInstance(workflowInstance);
        newEvent.setNode(targetNode);
        //TODO oct-2023 current implementation assumes one node is only assigned to one stage, db model association is many to many
        newEvent.setStage(targetNode.getStages().stream().findFirst().orElse(null));
        newEvent.setTenant(workflowInstance.getWorkflow().getTenants().stream().findFirst().orElse(defaultTenant));
        eventRepository.save(newEvent);

        return jsonObjectBuilder.add("success","Action executed successfully, current node id is now: " + newEvent.getNode().getId()).build().toString();
    }

    @Override
    public String moveNode(int workflowInstanceId, int recordId, String action) {

        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

        //String status = new String();

        List<String> validActions = new ArrayList<>(Arrays.asList("reset", "next", "back"));

        NodeModel currentNode = new NodeModel();

        if (!validActions.contains(action)) {
            System.out.println("Invalid action");
            jsonObjectBuilder.add("error", "400 Error: Invalid action [" + action + "]");
            JsonObject message = jsonObjectBuilder.build();
            return message.toString();
        }

        WorkflowInstanceModel workflowInstance = workflowInstanceRepository.findById(workflowInstanceId).orElse(null);

        if (workflowInstance == null) {
            jsonObjectBuilder.add("error", "400 Error: Workflow instance with id " + workflowInstanceId + " does not exist");
            return jsonObjectBuilder.build().toString();
        }

        //TODO check which tenant to assign, relation workflow tenant temporarily removed on cs-db 1762
        TenantModel defaultTenant = tenantRepository.findById(1).get();

        List<EventModel> events = eventRepository.findByWorkflowInstanceAndDeletedIsFalse(workflowInstance);

        List<NodeModel> workflowNodes = nodeRepository.findWorkflowNodes(workflowInstance.getWorkflow().getId());

        if (workflowNodes.isEmpty()) {
            jsonObjectBuilder.add("error", "400 Error: No node definitions found for this workflow");
            return jsonObjectBuilder.build().toString();
        }

        if (action.equals("reset")) {
            events.forEach(event -> {
                event.setCompleted(null);
                event.setError(null);
                event.setDescription(null);
                eventRepository.save(event);
                List<CFValueModel> cfValues = cfValueRepository.findByEventId(event.getId());
                cfValues.forEach(cfValue -> {
                    cfValue.setDeleted(true);
                    cfValueRepository.save(cfValue);
                });
            });
            workflowInstance.setComplete(null);
            workflowInstanceRepository.save(workflowInstance);
            return jsonObjectBuilder.add("success", "Action executed successfully: The workflow for this record was reset").build().toString();
        } else {
            if (action.equals("next")) {

                if (events.isEmpty()) {
                    /* jsonObjectBuilder.add("error", "400 Error: This workflow has not been initiated ");
                    return jsonObjectBuilder.build().toString(); */
                    EventModel firstEvent = new EventModel();
                    firstEvent.setRecordId(recordId);
                    firstEvent.setWorkflowInstance(workflowInstance);
                    firstEvent.setNode(workflowNodes.get(0));
                    firstEvent.setStage(workflowNodes.get(0).getStages().stream().filter(
                        s -> s.getPhase().getWorkflow().equals(workflowInstance.getWorkflow())
                    ).findFirst().orElse(null));
                    firstEvent.setTenant(workflowInstance.getWorkflow().getTenants().stream().findFirst().orElse(defaultTenant));
                    eventRepository.save(firstEvent);
                    currentNode = firstEvent.getNode();
                } else {
                    events = eventRepository
                        .findByWorkflowInstanceAndCompletedIsNullAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAscNodeSequenceAsc(
                                workflowInstance);
                    if (events.isEmpty())
                        return jsonObjectBuilder.add("success","This workflow has already been completed").build().toString();
                    EventModel currentEvent = events.stream().findFirst().orElse(null);
                    if (currentEvent.getStage() == null || currentEvent.getNode() == null)
                        return jsonObjectBuilder.add("error","500 Error: There is no information about the current stage/node of the workflow").build().toString();
                    //TODO check node's dependOn before setting event as completed    
                    currentEvent.setCompleted(new Date());
                    eventRepository.save(currentEvent);
                    int nextNodeIndex = workflowNodes.indexOf(currentEvent.getNode()) + 1;
                    if (nextNodeIndex < workflowNodes.size()) {
                        final NodeModel nextNode = workflowNodes.get(nextNodeIndex);
                        events.stream().filter(
                            e -> e.getNode().equals(nextNode) && e.getWorkflowInstance().equals(workflowInstance)
                        ).findFirst().ifPresentOrElse(ev -> {
                            // Only to validate if there is an event with the next node in the sequence and workflow instance
                        }, () -> {
                            // There was not an event with the next node in the sequence and workflow instance, create it
                            EventModel newEvent = new EventModel();
                            newEvent.setCompleted(null);
                            newEvent.setRecordId(recordId);
                            newEvent.setWorkflowInstance(workflowInstance);
                            newEvent.setNode(nextNode);
                            newEvent.setStage(nextNode.getStages().stream().filter(
                                s -> s.getPhase().getWorkflow().equals(workflowInstance.getWorkflow())
                            ).findFirst().orElse(null));
                            newEvent.setTenant(workflowInstance.getWorkflow().getTenants().stream().findFirst().orElse(defaultTenant));
                            eventRepository.save(newEvent);
                        });
                        currentNode = nextNode;
                    } else {
                        workflowInstance.setComplete(new Date());
                        workflowInstanceRepository.save(workflowInstance);
                        return jsonObjectBuilder.add("success","Action executed successfully, workflow for this record has been completed").build().toString();
                    }
                }                

            } else if (action.equals("back")) {
                events = eventRepository
                        .findByWorkflowInstanceAndCompletedNotNullAndDeletedIsFalseOrderByStagePhaseSequenceDescStageSequenceDescNodeSequenceDesc(
                                workflowInstance);
                if (events.isEmpty())
                    return jsonObjectBuilder.add("success","This workflow is in its first node").build().toString();
                EventModel currentEvent = events.stream().findFirst().orElse(null);
                if (currentEvent.getStage() == null || currentEvent.getNode() == null)
                    return jsonObjectBuilder.add("error","500 Error: There is no information about the current stage/node of the workflow").build().toString();
                currentEvent.setCompleted(null);
                currentEvent.setError(null);
                currentEvent.setDescription(null);
                if ((workflowNodes.indexOf(currentEvent.getNode()) + 1) == workflowNodes.size()) {
                    workflowInstance.setComplete(null);
                    workflowInstanceRepository.save(workflowInstance);
                }
                eventRepository.save(currentEvent);
                currentNode = currentEvent.getNode();
            }
        }
        return jsonObjectBuilder.add("success","Action executed successfully, current node id is now: " + currentNode.getId()).build().toString();
    }

    @Override
    public String changeStatus(String payload) {

        JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

        StatusModel status = new StatusModel();

        WorkflowInstanceModel wfInstance = new WorkflowInstanceModel();

        StatusTypeModel statusType = new StatusTypeModel();

        TenantModel tenant = new TenantModel();

        try {

            JsonReader reader = Json.createReader(new StringReader(payload));
            JsonObject json = reader.readObject();
    
            if (json.get("recordId") != null) {
                if (json.get("recordId").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0)
                    status.setRecordId(Integer.parseInt(json.get("recordId").toString()));
                else 
                    return jsonBuilder.add("error", "400 Error recordId has an incorrect data type").build().toString();
            }
            if (json.get("workflowInstanceId") != null && json.get("workflowInstanceId").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                wfInstance = workflowInstanceRepository.findById(Integer.parseInt(json.get("workflowInstanceId").toString())).orElse(null);
                if (wfInstance == null) {
                    jsonBuilder.add("error", "400 Error Workflow instance with id " + json.get("workflowInstanceId") + " does not exist");
                    return jsonBuilder.build().toString();
                }
                status.setWorkflowInstance(wfInstance);
            } else 
                return jsonBuilder.add("error", "400 Error workflowInstanceId is missing or has an incorrect data type").build().toString();
            if (json.get("statusTypeId") != null && json.get("statusTypeId").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                statusType = statusTypeRepository.findById(Integer.parseInt(json.get("statusTypeId").toString())).orElse(null);
                if (statusType == null) {
                    jsonBuilder.add("error", "400 Error Status type with id " + json.get("statusTypeId") + " does not exist");
                    return jsonBuilder.build().toString();
                }
                status.setStatusType(statusType);
            } else 
                return jsonBuilder.add("error", "400 Error statusTypeId is missing or has an incorrect data type").build().toString();
            if (json.get("tenantId") != null && json.get("tenantId").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                tenant = tenantRepository.findById(Integer.parseInt(json.get("tenantId").toString())).orElse(null);
                if (tenant == null) {
                    jsonBuilder.add("error", "400 Error Tenant with id " + json.get("tenantId") + " does not exist");
                    return jsonBuilder.build().toString();
                }
                status.setTenant(tenant);
            } else 
                return jsonBuilder.add("error", "400 Error tenantId is missing or has an incorrect data type").build().toString();
    
            status.setInitiateDate(new Date());
    
        } catch (Exception e) {
            return jsonBuilder.add("error", "500 Error" + e.getMessage()).build().toString();
        }

        //TODO check if status types need to have a sequence
        List<StatusModel> wfStatuses = statusRepository.findByWorkflowInstance(wfInstance);

        if (wfStatuses.isEmpty()) {
            System.out.println("no statuses for wf, creating one");
            wfStatuses.add(status);
        } else {
            boolean statusTypeExists = false;
            for (StatusModel s : wfStatuses) {
                if (s.getStatusType().equals(statusType) && !statusTypeExists) {
                    statusTypeExists = true;
                    s.setCompletionDate(null);
                } else 
                    s.setCompletionDate(new Date());
            }
            if (!statusTypeExists)
                wfStatuses.add(status);
        }

        statusRepository.saveAll(wfStatuses);

        return jsonBuilder.add("success", "status changed successfully").build().toString();
    }

}
