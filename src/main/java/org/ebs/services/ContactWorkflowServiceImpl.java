package org.ebs.services;

import java.util.List;
import java.util.stream.Collectors;

import org.ebs.model.ContactModel;
import org.ebs.model.ContactWorkflowModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.ContactWorkflowRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.ContactWorkflowInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor

public class ContactWorkflowServiceImpl implements ContactWorkflowService {
    
    private final ConversionService converter;
    private final ContactWorkflowRepository contactWorkflowRepository;
    private final ContactRepository contactRepository;
    private final WorkflowRepository workflowRepository;

    @Override
    public WorkflowTo findWorkflow(int contactWorkflowId) {
        return contactWorkflowRepository.findById(contactWorkflowId).map(r -> converter.convert(r.getWorkflow(), WorkflowTo.class)).orElse(null);
    }

    @Override
    public Contact findContact(int contactWorkflowId) {
        return contactWorkflowRepository.findById(contactWorkflowId).map(r -> converter.convert(r.getContact(), Contact.class)).orElse(null);
    }

    @Override
    @Transactional(readOnly = false)
    public ContactWorkflowsTo createContactWorkflow(ContactWorkflowInput input) {
        contactWorkflowRepository.findByContactIdAndWorkflowId(input.getContactId(), input.getWorkflowId())
            .ifPresent(r -> {
                throw new RuntimeException("Contact - Workflow relation already exists");
            });
        ContactWorkflowModel target = converter.convert(input, ContactWorkflowModel.class);
        
        setDependencies(target, input);
        return converter.convert(contactWorkflowRepository.save(target), ContactWorkflowsTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public ContactWorkflowsTo modifyContactWorkflow(ContactWorkflowInput input) {
        int id = 0;
        ContactWorkflowModel target = new ContactWorkflowModel();
        if (input.getId() > 0) {
            target = contactWorkflowRepository.findById(input.getId()).orElse(null);
        }
        if (target.getContact() == null) {
            target = contactWorkflowRepository.findByContactIdAndWorkflowId(input.getContactId(), input.getWorkflowId())
                .orElseThrow(() -> new RuntimeException("Contact - Worfklow relation not found")); 
        }
        id = target.getId();
        ContactWorkflowModel source = converter.convert(input, ContactWorkflowModel.class);
        Utils.copyNotNulls(source, target);
        target.setId(id);
        return converter.convert(contactWorkflowRepository.save(target), ContactWorkflowsTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public String deleteContactWorkflow(int contactId, int workflowId) {
        contactWorkflowRepository.findByContactIdAndWorkflowId(contactId, workflowId)
            .ifPresentOrElse(r -> {
                contactWorkflowRepository.delete(r);
            }, () -> {
                throw new RuntimeException("Contact - Worfklow relation not found");
            });
            return "Contact - Workflow relation deleted";
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteContactWorkflow(int id) {
        contactWorkflowRepository.findById(id)
            .ifPresentOrElse(r -> {
                contactWorkflowRepository.delete(r);
            }, () -> {
                throw new RuntimeException("Contact - Worfklow relation not found");
            });
        return id;
    }

    private void setDependencies(ContactWorkflowModel model, ContactWorkflowInput input) {
        setContact(input.getContactId(), model);
        setWorkflow(input.getWorkflowId(), model);
    }

    private void setContact(int contactId, ContactWorkflowModel model) {
        ContactModel contact = contactRepository.findById(contactId)
            .orElseThrow(() -> new RuntimeException("Contact " + contactId + " not found"));
        model.setContact(contact);
    }

    private void setWorkflow(int workflowId, ContactWorkflowModel model) {
        WorkflowModel workflow = workflowRepository.findById(workflowId)
            .orElseThrow(() -> new RuntimeException("Workflow " + workflowId + " not found"));
        model.setWorkflow(workflow);
    }

    @Override
    @Transactional(readOnly = false)
    public boolean deleteContactWorkflowsRelation(int contactId) {

        contactWorkflowRepository.deleteByContactId(contactId);

        return true;

    }

    @Override
    public List<ContactWorkflowsTo> findByContactId(int contactId) {
        return contactWorkflowRepository.findByContactId(contactId)
            .stream().map(r -> converter.convert(r, ContactWorkflowsTo.class))
            .collect(Collectors.toList());
    }
    
    @Override
    public List<ContactWorkflowModel> findModelByContactId(int contactId) {
        return contactWorkflowRepository.findByContactId(contactId)
            .stream().collect(Collectors.toList());
    }

}
