package org.ebs.services;

import static java.util.Collections.rotate;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.SecurityRuleModel;
import org.ebs.model.SecurityRuleRoleModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.SecurityRuleRepository;
import org.ebs.model.repos.SecurityRuleRoleRepository;
import org.ebs.model.repos.UserRepository;
// import org.ebs.model.repos.UserSecurityRuleRepository;
// import org.ebs.model.repos.SecurityRuleSecurityRuleRepository;

import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.SecurityRuleTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.SecurityRuleTo;

import org.ebs.services.to.Input.SecurityRuleInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:24 AM
 */
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class SecurityRuleServiceImpl implements SecurityRuleService {

    private final SecurityRuleRepository securityRuleRepository;
    private final SecurityRuleRoleRepository securityRuleRoleRepository;
    private final ConversionService converter;
    private final UserRepository userRepository;
    // private final UserSecurityRuleRepository userSecurityRuleRepository;
    // private final SecurityRuleSecurityRuleRepository securityRuleSecurityRuleRepository;


    /**
     *
     * @param SecurityRule
     */
    @Override
    @Transactional(readOnly = false)
    public SecurityRuleTo createSecurityRule(SecurityRuleInput securityRule) {

        securityRuleRepository.findByName(securityRule.getName()).ifPresent(r -> {
            throw new RuntimeException("A securityRule with name " + securityRule.getName() + " already exists");
        });

        SecurityRuleModel model = converter.convert(securityRule, SecurityRuleModel.class);

        model.setId(0);

        // checkUsers(model, securityRule.getUserIds());

        model = securityRuleRepository.save(model);

        return converter.convert(model, SecurityRuleTo.class);
    }


    // void checkUsers(SecurityRuleModel securityRuleModel, Set<Integer> users) {
    //     ofNullable(users).ifPresent(userSet -> {
    //             Set<UserModel> userModelSet = userSet.stream().map(userId -> 
    //                 {
    //                     return userRepository.findById(userId).map(r -> {
    //                         r.getSecurityRules().add(securityRuleModel);
    //                         return r;
    //                     }).orElseThrow(() -> new RuntimeException("User id " + userId + " not found"));
    //                 }
    //             ).collect(toSet());
    //             securityRuleModel.setUsers(userModelSet);
    //         }
    //     );
    // }

    /**
     *
     * @param securityRuleId
     */
    @Override
    @Transactional(readOnly = false)
    public int deleteSecurityRule(int securityRuleId) {
        SecurityRuleModel securityRule = securityRuleRepository.findById(securityRuleId).orElseThrow(() -> new RuntimeException("SecurityRule not found"));
        deleteRoleRule(securityRuleId);     
        securityRule.setDeleted(true);
        securityRuleRepository.save(securityRule);
        return securityRuleId;
    }

    void deleteRoleRule(Integer ruleId) {
        var roleRule = securityRuleRoleRepository.findByRuleId(ruleId);
        if (roleRule.size() > 0) {
            for (SecurityRuleRoleModel securityRuleRoleModel : roleRule) {
                securityRuleRoleRepository.deleteById(securityRuleRoleModel.getId());
            }
        }
    }
    /**
     *
     * @param securityRuleId
     */
    // public Set<ProductFunctionTo> findProductFunctions(int securityRuleId) {
    //     return securityRuleRepository.findById(securityRuleId).get().getProductfunctions().stream()
    //             .map(e -> converter.convert(e, ProductFunctionTo.class)).collect(Collectors.toSet());
    // }

    /**
     *
     * @param securityRuleId
     */
    @Override
    public Optional<SecurityRuleTo> findSecurityRule(int securityRuleId) {
        if (securityRuleId < 1) {
            return Optional.empty();
        }
        return securityRuleRepository.findById(securityRuleId).filter(r -> !r.isDeleted()).map(r -> converter.convert(r, SecurityRuleTo.class));
    }

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    @Override
    public Page<SecurityRuleTo> findSecurityRules(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return securityRuleRepository.findByCriteria(SecurityRuleModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, SecurityRuleTo.class));
    }

    /**
     *
     * @param securityRuleId
     */
    // public Set<UserTo> findUsers(int securityRuleId) {
    //     return securityRuleRepository.findById(securityRuleId).get().getUsers().stream().map(e -> converter.convert(e, UserTo.class))
    //             .collect(Collectors.toSet());
    // }

    /**
     *
     * @param securityRule
     */
    @Override
    @Transactional(readOnly = false)
    public SecurityRuleTo modifySecurityRule(SecurityRuleInput securityRule) {
        SecurityRuleModel target = securityRuleRepository.findById(securityRule.getId())
                .orElseThrow(() -> new RuntimeException("SecurityRule not found"));
        securityRuleRepository.findByName(securityRule.getName()).ifPresent(r -> {
            if (r.getId() != securityRule.getId())
                throw new RuntimeException("A securityRule with name " + securityRule.getName() + " already exists");
        });
        SecurityRuleModel source = converter.convert(securityRule, SecurityRuleModel.class);
        // checkUsers(target, securityRule.getUserIds());
        Utils.copyNotNulls(source, target);
        return converter.convert(securityRuleRepository.save(target), SecurityRuleTo.class);
    }

    // @Override
    // public SecurityRuleTo findDefaultByUserId(int userId) {
    //     return userRepository.findByIdAndDeletedIsFalse(userId).map(u -> u.getDefaultSecurityRule()).filter(r -> !r.isDeleted())
    //             .map(r -> converter.convert(r, SecurityRuleTo.class)).orElse(null);
    // }

    // @Override
    // public List<SecurityRuleTo> findByUser(int userId) {
    //     return securityRuleRepository.findByUser(userId).stream().map(e -> converter.convert(e, SecurityRuleTo.class))
    //             .collect(toList());
    // }

    // @Override
    // public List<SecurityRuleTo> findBySecurityRule(int userId) {
    //     return securityRuleRepository.findById(userId).stream().map(e -> converter.convert(e, SecurityRuleTo.class))
    //             .collect(toList());
    // }

    // @Override
    // public List<SecurityRuleTo> findByContactHierarchy(int contactHierarchyId) {
    //     return securityRuleRepository.findByContactHierarchiesId(contactHierarchyId).stream().map(e -> converter.convert(e, SecurityRuleTo.class))
    //             .collect(toList());
    // }

    // @Override
    // public List<SecurityRuleTo> findRuleBySecurityRule(int securityRuleId) {
    //     return securityRuleSecurityRuleRepository.findBySecurityRuleId(securityRuleId).stream()
    //             .map(e -> converter.convert(e.getRule(), SecurityRuleTo.class)).collect(toList());
    // }


    private SecurityRuleModel verifySecurityRule(int securityRuleId) {
        return securityRuleRepository.findById(securityRuleId)
            .orElseThrow(() -> new RuntimeException("SecurityRule not found"));
    }

    @Override
    public UserTo findCreatedBy(int securityRuleId) {
        SecurityRuleModel securityRule = verifySecurityRule(securityRuleId);
        return userRepository.findById(securityRule.getCreatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

    @Override
    public UserTo findUpdatedBy(int securityRuleId) {
        SecurityRuleModel securityRule = verifySecurityRule(securityRuleId);
        if (securityRule.getUpdatedBy() == null)
            return null;
        return userRepository.findById(securityRule.getUpdatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

}