package org.ebs.services;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.ebs.util.Utils.copyNotNulls;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.ebs.model.AddressModel;
import org.ebs.model.ContactAddressModel;
import org.ebs.model.ContactInfoModel;
import org.ebs.model.ContactModel;
import org.ebs.model.ContactTypeModel;
import org.ebs.model.CropModel;
import org.ebs.model.PersonModel;
import org.ebs.model.PurposeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UnitTypeModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.ContactAddressRepository;
import org.ebs.model.repos.ContactInfoRepository;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.ContactTypeRepository;
import org.ebs.model.repos.ContactWorkflowRepository;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.HierarchyRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.model.repos.PersonRepository;
import org.ebs.model.repos.PurposeRepository;
import org.ebs.model.repos.RoleContactHierarchyRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UnitTypeRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.model.rule.SequenceRuleModel;
import org.ebs.model.rule.SequenceRuleSegmentModel;
import org.ebs.model.rule.repos.SequenceRuleRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.AddressInput;
import org.ebs.services.to.Input.ContactInfoInput;
import org.ebs.services.to.Input.ContactInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ContactServiceImpl implements ContactService {

    private final ConversionService converter;
    private final ContactRepository contactRepository;
    private final UserRepository userRepository;
    private final ContactTypeRepository contactTypeRepository;
    private final PersonRepository personRepository;
    private final InstitutionRepository institutionRepository;
    private final AddressRepository addressRepository;
    private final ContactInfoRepository contactInfoRepository;
    private final HierarchyRepository hierarchyRepository;
    private final PurposeRepository purposeRepository;
    private final TenantRepository tenantRepository;
    private final ContactAddressRepository contactAddressRepository;
    private final SequenceRuleRepository sequenceRuleRepository;
    private final CropRepository cropRepository;
    private final UnitTypeRepository unitTypeRepository;
    private final RoleContactHierarchyRepository roleContactHierarchyRepository;
    private final ContactWorkflowRepository contactWorkflowRepository;
    private final SynchronizeUserService synchronizeUserService;
    //private final SequenceRuleService sequenceRuleService;

    @Override
    public Contact findByUser(int userId) {
        return userRepository.findById(userId).map(u -> u.getContact())
                .map(c -> converter.convert(c, Contact.class)).orElse(null);
    }

    @Override
    public Contact findById(int contactId) {
        return contactRepository.findById(contactId).map(c -> converter.convert(c, Contact.class))
                .orElse(null);
    }

    @Override
    public Page<Contact> findContacts(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {

        return contactRepository
                .findByCriteria(ContactModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, Contact.class));

    }

    @Override
    @Transactional(readOnly = false)
    public Contact createContact(ContactInput contactInput) {
        contactInput.setId(0);
        ContactModel model = converter.convert(contactInput, ContactModel.class);
        verifyDependencies(model, contactInput);
        model = contactRepository.save(model);
        if (model.getCategory().getName().equals("Institution")
                || model.getCategory().getName().equals("Internal Unit")) {
            model.getInstitution().setContact(model);
            model.getInstitution().setExternalCode(model.getId());
            institutionRepository.save(model.getInstitution());
            if (verifyInstitutionExistsWithLegalName(model.getInstitution().getLegalName()) > 1)
                throw new RuntimeException("The Legal Name for the institution already exists.");
            if (verifyInstitutionExistsWithCommonName(model.getInstitution().getCommonName()) > 1)
                throw new RuntimeException("The Common Name/Abbreviation for the institution already exists");
            if (model.getCategory().getName().equals("Institution"))
                setInstitutionAddress(model, contactInput);
            else 
                setAddresses(model, contactInput);
            setInstitutionCrops(model, contactInput.getInstitution().getCropIds());
            setInstitutionUnitType(model, contactInput.getInstitution().getUnitTypeId());
        } else {
            model.getPerson().setContact(model);
            setPersonFullName(model.getPerson());
            personRepository.save(model.getPerson());
            /* if (verifyPersonExists(model.getPerson().getGivenName(), model.getPerson().getFamilyName()) > 1)
                throw new RuntimeException("The Given Name and Family Name combinations already exists within EBS."); */
            if (verifyEmailExists(model.getEmail()) > 1)
                throw new RuntimeException("The Email already exists within EBS.");
            setAddresses(model, contactInput);      
        }
        setContactTenants(model, contactInput);
        return converter.convert(model, Contact.class);

    }

    void setPersonFullName(PersonModel person) {
        person.setFullName(person.getFamilyName() + ", " + person.getGivenName() + (person.getAdditionalName() == null ? "" : " " + person.getAdditionalName()));
        // Sequence rule id 7 'cs-fullname' cs-db feature CS-1868
        //SequenceRuleModel sequenceRule = sequenceRuleRepository.findById(7).orElse(null);
        /*if (sequenceRule != null) {
            Map<String, String> valuesMap = Map.of(
                "contact-familyName", person.getFamilyName(),
                "contact-givenName", person.getGivenName(),
                "contact-additionalName", person.getAdditionalName() != null ? person.getAdditionalName() : "",
                "colon", ",",
                "blank", " "
            );
            String fullName = "";
            Comparator<SequenceRuleSegmentModel> posComparator = Comparator.comparing(SequenceRuleSegmentModel::getPosition);
            List<SequenceRuleSegmentModel> ruleSegments = sequenceRule.getRuleSegments();
            ruleSegments.sort(posComparator);
            for (SequenceRuleSegmentModel rule : ruleSegments) {
                fullName += valuesMap.get(rule.getSegment().getName());
            }
            if (fullName.trim().endsWith(","))
                fullName = fullName.substring(0, fullName.lastIndexOf(","));
            person.setFullName(fullName);
        } */
    }

    void setFullAddress(AddressModel address) {
        // Sequence rule id 8 'cs-fulladdress' cs-db feature CS-1868
        SequenceRuleModel sequenceRule = sequenceRuleRepository.findById(8).orElse(null);
        if (sequenceRule != null) {
            Map<String, String> valuesMap = Map.of(
                "address-location", address.getLocation() != null ? address.getLocation() : "",
                "address-region", address.getRegion() != null ? address.getRegion() : "",
                "address-zipCode", address.getZipCode() != null ? address.getZipCode() : "",
                "address-streetAddress", address.getStreetAddress(),
                "address-additionalStreetAddress", address.getAdditionalStreetAddress() != null ? address.getAdditionalStreetAddress() : "",
                "colon", ",",
                "blank", " "
            );
            String fullAddress = "";
            Comparator<SequenceRuleSegmentModel> posComparator = Comparator.comparing(SequenceRuleSegmentModel::getPosition);
            List<SequenceRuleSegmentModel> ruleSegments = sequenceRule.getRuleSegments();
            ruleSegments.sort(posComparator);
            for (SequenceRuleSegmentModel rule : ruleSegments) {
                fullAddress += valuesMap.get(rule.getSegment().getName());
            }
            if (fullAddress.trim().endsWith(","))
                fullAddress = fullAddress.substring(0, fullAddress.lastIndexOf(","));
            address.setFullAddress(fullAddress);
        }
    }

    void setContactTenants(ContactModel model, ContactInput input) {

        if (input.getTenantIds() == null)
            return;

        input.getTenantIds().forEach(tenantId -> {
            TenantModel tenant = tenantRepository.findByIdAndDeletedIsFalse(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant not found"));
            tenant.setContacts(new HashSet<>(Arrays.asList(model)));
            tenantRepository.save(tenant);
        });

        model.getTenants().forEach(tenant -> {
            if (!input.getTenantIds().contains(tenant.getId())) {
                tenant.getContacts().remove(model);
                tenantRepository.save(tenant);
            }
        });
    }

    @Override
    public int countByEmail(String email) {
        int result = contactRepository.countByEmailIgnoreCaseAndDeletedIsFalse(email);
        return result;
    }

    private int verifyPersonExists(String givenName, String familyName) {
        int result = personRepository.countByGivenNameIgnoreCaseAndFamilyNameIgnoreCaseAndDeletedIsFalse(givenName,
                familyName);
        return result;
    }

    private int verifyEmailExists(String Email) {
        int result = contactRepository.countByEmailIgnoreCaseAndDeletedIsFalse(Email);
        return result;
    }

    private int verifyInstitutionExistsWithLegalName(String legalName) {
        int result = institutionRepository.countByLegalNameIgnoreCaseAndDeletedIsFalse(legalName);
        return result;
    }

    private int verifyInstitutionExistsWithCommonName(String commonName) {
        int result = institutionRepository.countByCommonNameIgnoreCaseAndDeletedIsFalse(commonName);
        return result;
    }

    /**
     * Verifies all dependent entities are properly created and set
     *
     * @param model
     *                     new transient instace to verify
     * @param contactInput
     *                     with possible new associations like addresses, types, etc
     */
    void verifyDependencies(ContactModel model, ContactInput contactInput) {
        verifyPurposes(contactInput.getPurposeIds(), model);
        verifyContactTypes(contactInput.getContactTypeIds(), model);
        verifyContactInfos(contactInput.getContactInfos(), model);
    }

    void setInstitutionAddress(ContactModel model, ContactInput contactInput) {
        if (contactInput.getAddresses() == null || contactInput.getAddresses().isEmpty()) {
            //TODO check which default address to add, not enough information on current tenant
            //hardcoding default address with id 1
            AddressModel defaultAddress = addressRepository.findById(1)
                .orElseThrow(() -> new RuntimeException("could not get a default address to copy"));
            if (defaultAddress != null) {
                AddressModel defaultCopy = new AddressModel();
                defaultCopy.setLocation(defaultAddress.getLocation());
                defaultCopy.setRegion(defaultAddress.getRegion());
                defaultCopy.setZipCode(defaultAddress.getZipCode());
                defaultCopy.setStreetAddress(defaultAddress.getStreetAddress());
                defaultCopy.setCountry(defaultAddress.getCountry());
                defaultCopy.setFullAddress(defaultAddress.getFullAddress());
                defaultCopy.setId(0);
                defaultCopy = addressRepository.save(defaultCopy);
                contactAddressRepository.save(new ContactAddressModel(model, defaultCopy, true));
            }
        } else {
            contactInput.getAddresses().forEach(ad -> {
                AddressModel address = addressRepository.findById(ad.getId()).orElse(null);
                if (address != null) {
                    address.getContacts().forEach(c -> {
                        if (c.getCategory().getName().equals("Institution")  && !c.equals(model)) {
                            throw new RuntimeException("The specified address " 
                                + address.getStreetAddress() + "[" + address.getId() + "]" 
                                + " is already set to another institution[" + c.getId() + "]");
                        }
                    });
                    copyNotNulls(converter.convert(ad, AddressModel.class), address);
                    setFullAddress(address);
                    ContactAddressModel existingRelation = contactAddressRepository
                                .findByContactAndAddress(model, address).orElse(null);
                    if (existingRelation == null) {
                        contactAddressRepository.save(new ContactAddressModel(model, address, ad.isDefault()));
                    } else {
                        existingRelation.setDefault(ad.isDefault());
                        contactAddressRepository.save(existingRelation);
                    }
                } else {
                    ad.setId(0);
                    AddressModel newAddress = addressRepository.save(converter.convert(ad,AddressModel.class));
                    setFullAddress(newAddress);
                    contactAddressRepository.save(new ContactAddressModel(model, newAddress, ad.isDefault()));
                }
            });
        }
        List<ContactAddressModel> contactAddresses = contactAddressRepository.findByContact(model);
        checkDefaultAddress(model, contactAddresses);
    }

    void setInstitutionCrops(ContactModel contact, Set<Integer> cropIds) {
        List<CropModel> crops = new ArrayList<>();
        if (cropIds != null && !cropIds.isEmpty()) {
            cropIds.forEach(id -> {
                CropModel crop = cropRepository.findById(id).orElse(null);
                if (crop != null)
                    crops.add(crop);
            });
        }
        contact.getInstitution().setCrops(crops);
    }

    void setInstitutionUnitType(ContactModel model, Integer unitTypeId) {
        if (model.getCategory().getName().equals("Internal Unit")) {
            if (unitTypeId != null && (unitTypeId == 1 || unitTypeId == 2)) {
                UnitTypeModel unitType = unitTypeRepository.findById(unitTypeId)
                    .orElse(null);
                if (unitType != null)
                    model.getInstitution().setUnitType(unitType);
            } else {
                throw new RuntimeException("Invalid unit type for the institution");
            }
        }
    }

    void setAddresses(ContactModel contact, ContactInput contactInput) {
        if (contactInput.getAddresses() == null || contactInput.getAddresses().isEmpty()) {
            addDefaultAddress(contact);
        } else {
            verifyAddresses(contactInput.getAddresses(), contact);
        }
        List<ContactAddressModel> contactAddresses = contactAddressRepository.findByContact(contact);
        checkDefaultAddress(contact, contactAddresses);
    }

    void addDefaultAddress(ContactModel contact) {
        AddressModel addressToSave = new AddressModel();
        if (contact.getEmail() != null) {
            String emailDomain = contact.getEmail().substring(contact.getEmail().indexOf("@") + 1);
            if (emailDomain.equals("cimmyt.org"))
                addressToSave = addressRepository.findById(2).orElse(null);
            else if (emailDomain.equals("irri.org"))
                addressToSave = addressRepository.findById(3).orElse(null);
            else
                addressToSave = addressRepository.findById(2).orElse(null);
        } else
            addressToSave = addressRepository.findById(2).orElse(null);

        ContactAddressModel existingRelation = contactAddressRepository.findByContactAndAddress(contact, addressToSave)
                .orElse(null);
        if (existingRelation == null) {
            contactAddressRepository.save(new ContactAddressModel(contact, addressToSave, true));
        }
    }

    /**
     * Verifies new addresses are properly linked to a contact
     *
     * @param addresses
     *                  to link
     * @param contact
     *                  owner of the addresses
     */
    void verifyAddresses(List<AddressInput> addresses, ContactModel contact) {
        ofNullable(addresses).ifPresent(ad -> {
            ad.stream().forEach(a -> {
                AddressModel addressToSave = addressRepository.findById(a.getId()).orElse(null);
                if (addressToSave != null) {
                    copyNotNulls(converter.convert(a, AddressModel.class), addressToSave);
                    setFullAddress(addressToSave);
                    ContactAddressModel existingRelation = contactAddressRepository
                        .findByContactAndAddress(contact, addressToSave).orElse(null);
                    if (existingRelation == null) {
                        contactAddressRepository.save(new ContactAddressModel(contact, addressToSave, a.isDefault()));
                    } else {
                        existingRelation.setDefault(a.isDefault());
                        contactAddressRepository.save(existingRelation);
                    }
                } else {
                    a.setId(0);
                    addressToSave = converter.convert(a, AddressModel.class);
                    setFullAddress(addressToSave);
                    addressToSave = addressRepository.save(addressToSave);
                    contactAddressRepository.save(new ContactAddressModel(contact, addressToSave, a.isDefault()));
                }
            });
        });
    }

    void verifyPurposes(Set<Integer> purposeIds, ContactModel contact) {
        ofNullable(purposeIds).ifPresent(ids -> {
            List<PurposeModel> purposes = purposeRepository.findByIds(new ArrayList<>(ids));
            Set<Integer> purposeModelIds = purposes.stream().map(p -> p.getId()).collect(toSet());

            ids.forEach(id -> {
                if (!purposeModelIds.contains(id))
                    throw new RuntimeException("Invalid contact type: " + id);
            });
            contact.setPurposes(new HashSet<>(purposes));
        });
    }

    void verifyContactTypes(List<Integer> contactTypeIds, ContactModel contact) {
        ofNullable(contactTypeIds).ifPresent(ids -> {
            List<ContactTypeModel> contactTypes = contactTypeRepository.findByIds(ids);
            List<Integer> typeIds = contactTypes.stream().map(t -> t.getId()).collect(toList());

            ids.forEach(id -> {
                if (!typeIds.contains(id))
                    throw new RuntimeException("Invalid contact type: " + id);
            });
            contact.setContactTypes(contactTypes);
        });
    }

    /**
     * Checks at most one default address is set. Others must have default=false
     *
     * @param contact
     */
    void checkDefaultAddress(ContactModel contact, List<ContactAddressModel> contactAddresses) {
        if (contactAddresses == null || contactAddresses.isEmpty())
            return;
        long defaultAddresses = contactAddresses.stream().filter(a -> a.isDefault()).count();
        if (defaultAddresses > 1)
            throw new RuntimeException("Only one address can be set as default");
        else if (defaultAddresses == 0)
            throw new RuntimeException("At least one address must be set as default");
    }

    /**
     * Checks at most one default contact info is set by type. Others must have
     * default=false
     *
     * @param contact
     */
    void checkDefaultContactInfo(ContactModel contact) {
        contact.getContactInfos().stream().filter(a -> a.isDefault())
                .collect(groupingBy(c -> c.getContactInfoType().getName(), counting()))
                .forEach((k, v) -> {
                    if (v > 1)
                        throw new RuntimeException("Only one " + k + " can be set as default");
                });

    }

    /**
     * Verifies new contactInfo items are properly linked to a contact
     *
     * @param contactInfos
     * @param contact
     */
    void verifyContactInfos(List<ContactInfoInput> contactInfos, ContactModel contact) {
        ofNullable(contactInfos).ifPresent(ci -> {
            List<ContactInfoModel> infos = ci.stream().map(a -> {
                a.setId(0);
                return converter.convert(a, ContactInfoModel.class);
            }).collect(toList());
            infos.forEach(c -> c.setContact(contact));
            contact.setContactInfos(infos);
        });
        checkDefaultContactInfo(contact);
        checkDuplicateInfos(contact);
    }

    void checkDuplicateInfos(ContactModel contact) {
        List<ContactInfoModel> contactInfos = new ArrayList<>(contact.getContactInfos());
        contact.getContactInfos().stream().filter(ci -> !ci.isDeleted())
        .forEach(info -> {
            List<ContactInfoModel> existingInfos = contactInfos.stream()
                .filter(existingInfo -> !existingInfo.isDeleted() && existingInfo.getContactInfoType().equals(info.getContactInfoType()) && existingInfo.getValue().equals(info.getValue()))
                .collect(Collectors.toList());
            if (existingInfos.size() > 1)
                throw new RuntimeException("Cannot set duplicate values for a contact info type (" + info.getContactInfoType().getName() + ")");
        });
    }

    @Override
    @Transactional(readOnly = false)
    public Contact modifyContact(ContactInput contactInput) {

        ContactModel contact = verifyContact(contactInput.getId());

        String contactCategoryName = contact.getCategory().getName();
        List<ContactAddressModel> contactAddresses = contactAddressRepository.findByContact(contact);
        if (contactCategoryName.equals("Institution") || contactCategoryName.equals("Internal Unit")) {
            ofNullable(contactInput.getInstitution()).ifPresent(o -> {
                copyNotNulls(o, contact.getInstitution());
                institutionRepository.save(contact.getInstitution());
                if (verifyInstitutionExistsWithLegalName(o.getLegalName()) > 1)
                    throw new RuntimeException("The Legal Name for the institution already exists.");
                if (verifyInstitutionExistsWithCommonName(o.getCommonName()) > 1)
                    throw new RuntimeException("The Common Name/Abbreviation for the institution already exists");
                //contactAddressRepository.deleteAll(contactAddresses);
            });
            if (contact.getCategory().getName().equals("Institution"))
                setInstitutionAddress(contact, contactInput);
            else 
                setAddresses(contact, contactInput);
            setInstitutionCrops(contact, contactInput.getInstitution().getCropIds());
            setInstitutionUnitType(contact, contactInput.getInstitution().getUnitTypeId());
        } else if (contactCategoryName.equals("Person")) {
            ofNullable(contactInput.getPerson()).ifPresent(o -> {
                copyNotNulls(o, contact.getPerson());
                setPersonFullName(contact.getPerson());
                personRepository.save(contact.getPerson());
            });
            saveAddresses(contactInput.getAddresses(), contact, contactAddresses);
        }
        saveContactInfos(contactInput.getContactInfos(), contact);
        verifyContactTypes(contactInput.getContactTypeIds(), contact);
        savePurposes(contactInput.getPurposeIds(), contact);
        saveEmail(contactInput.getEmail(), contact);
        checkDefaultContactInfo(contact);
        setContactTenants(contact, contactInput);
        contact.setUpdatedOn(new Date());

        return converter.convert(contactRepository.save(contact), Contact.class);
    }

    void saveEmail(String email, ContactModel contact) {
        if (email == null)
            return;
        if (!isValidEmail(email.trim()))
            throw new RuntimeException("Invalid username/email");
        contact.setEmail(email.trim());
    }

    private boolean isValidEmail(String email) {
        String regexPattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
        return Pattern.compile(regexPattern)
            .matcher(email)
            .matches();
    }

    ContactModel verifyContact(int contactId) {
        return contactRepository.findById(contactId)
                .orElseThrow(() -> new RuntimeException("Contact " + contactId + " not found"));
    }

    void saveAddresses(List<AddressInput> addressInputs, ContactModel contact,
            List<ContactAddressModel> contactAddresses) {

        if (addressInputs == null)
            return;
        updateExistingAddresses(addressInputs, contact.getAddresses(), contactAddresses);
        addNewAddresses(addressInputs, contact);
        contactAddresses = contactAddressRepository.findByContact(contact);
        checkDefaultAddress(contact, contactAddresses);
    }

    /**
     * Looks for existing addresses and tries to update their state
     *
     * @param addressInputs
     *                      list of all addresses
     * @param addresses
     *                      list of addresses currently linked to a contact and
     *                      candidates to
     *                      be updated
     *
     */
    void updateExistingAddresses(List<AddressInput> addressInputs, List<AddressModel> addresses,
            List<ContactAddressModel> contactAddresses) {
        addressInputs.forEach(changed -> {
            addresses.stream().filter(current -> current.getId() == changed.getId()).findFirst()
                    .ifPresent(current -> {
                        copyNotNulls(converter.convert(changed, AddressModel.class), current);
                        setFullAddress(current);
                        contactAddresses.stream().filter(addr -> addr.getAddress().getId() == current.getId())
                                .findFirst().ifPresent(ad -> ad.setDefault(changed.isDefault()));
                    });
        });

    }

    /**
     * Tries to link new addresses (with id=0) to a contact
     *
     * @param addressInputs
     *                      list of all addresses
     * @param contact
     *                      to have new addresses added
     */
    void addNewAddresses(List<AddressInput> addressInputs, ContactModel contact) {
        List<Integer> currentContactAddresses = contact.getAddresses().stream().map(a -> a.getId()).collect(toList());
        verifyAddresses(
                addressInputs.stream().filter(a -> !currentContactAddresses.contains(a.getId())).collect(toList()),
                contact);
    }

    void saveContactInfos(List<ContactInfoInput> contactInfoInputs, ContactModel contact) {

        if (contactInfoInputs == null)
            return;

        updateExistingContactInfos(contactInfoInputs, contact.getContactInfos());
        addNewContactInfos(contactInfoInputs, contact);

    }

    void updateExistingContactInfos(List<ContactInfoInput> contactInfoInputs,
            List<ContactInfoModel> infos) {
        contactInfoInputs.forEach(changed -> {
            infos.stream().filter(current -> current.getId() == changed.getId()).findFirst()
                    .ifPresent(current -> {
                        copyNotNulls(converter.convert(changed, ContactInfoModel.class), current);
                    });
        });

    }

    void addNewContactInfos(List<ContactInfoInput> infoInputs, ContactModel contact) {
        verifyContactInfos(infoInputs.stream().filter(a -> a.getId() == 0).collect(toList()),
                contact);
    }

    void savePurposes(Set<Integer> purposeIds, ContactModel contact) {
        if (purposeIds == null)
            return;
        Set<PurposeModel> purposes = new HashSet<>();
        purposeIds.forEach(purposeId -> {
            purposeRepository.findById(purposeId)
                    .ifPresentOrElse(p -> {
                        purposes.add(p);
                    },
                            () -> {
                                throw new RuntimeException("Purpose not found " + purposeId);
                            });
        });
        contact.setPurposes(purposes);
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteContact(int contactId) {
        ContactModel contact = verifyContact(contactId);
        // contact.getAddresses().forEach(a -> a.setDeleted(true));
        contact.getContactInfos().forEach(i -> i.setDeleted(true));
        contact.getParents().forEach(p -> {
            roleContactHierarchyRepository.deleteByHierarchyId(p.getId());
            p.setDeleted(true);
            hierarchyRepository.save(p);
        });
        contact.getMembers().forEach(m -> {
            roleContactHierarchyRepository.deleteByHierarchyId(m.getId());
            m.setDeleted(true);
            hierarchyRepository.save(m);
        });
        contactAddressRepository.findByContact(contact).forEach(ad -> {
                contactAddressRepository.delete(ad);
        });
        contact.getUsers().forEach(u -> {
            u.setDeleted(true);
            u.setActive(false);
            userRepository.save(u);
            synchronizeUserService.synchronizePerson(u.getId());
        });
        contact.setContactTypes(new ArrayList<>());
        contact.setPurposes(new HashSet<>());
        contact.getContactWorkflows().forEach(cw -> {
            cw.setDeleted(true);
            contactWorkflowRepository.save(cw);
        });
        if (contact.getCategory().getName().equals("Person") && contact.getPerson() != null) {
            contact.getPerson().setDeleted(true);
            personRepository.save(contact.getPerson());
        } else if (contact.getInstitution() != null) {
            contact.getInstitution().setDeleted(true);
            institutionRepository.save(contact.getInstitution());
        }
        contact.setDeleted(true);
        contactRepository.save(contact);

        return contactId;
    }

    @Override
    @Transactional(readOnly = false)
    public boolean restoreContact(int contactId) {
        ContactModel contact = contactRepository.findByIdAndDeletedIsTrue(contactId).orElse(null);
        if (contact == null) {
            contact = verifyContact(contactId);
        } else {
            contact.setDeleted(false);
            contact.getContactInfos().forEach(i -> i.setDeleted(false));
            contactRepository.save(contact);
        }
        return !contact.isDeleted();
    }

    @Override
    @Transactional(readOnly = false)
    public int removeAddress(int addressId, int contactId) {

        ContactAddressModel contactAddress = contactAddressRepository.findByContactIdAndAddressId(contactId, addressId)
                .orElse(null);
    
        if (contactAddress == null)
            throw new RuntimeException("Could not find address-contact relationship");

        // verify if the relationship between address and contact determinate if the
        // user is removing a default address
        if (contactAddress.isDefault()) {
            throw new RuntimeException("Cannot remove a default address");
        }

        List<ContactAddressModel> contactAddressees = contactAddressRepository.findByContact(contactAddress.getContact());

        if (contactAddressees.size() == 1) {
            throw new RuntimeException("Cannot remove the only address for a contact");
        }
        try {
            Set<ContactAddressModel> contactAddressesToDelete = new HashSet<>();
            contactAddressesToDelete.add(contactAddress);
            contactAddressRepository.delete(contactAddress);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return addressId;
    }

    @Override
    @Transactional(readOnly = false)
    public int removeContactInfo(int contactInfoId) {
        contactInfoRepository.findById(contactInfoId).ifPresentOrElse((c -> {
            c.setDeleted(true);
            ContactInfoModel info = contactInfoRepository.save(c);

            // info.getContact().getContactInfos().stream()
            //         .filter(i -> i.getContactInfoType().getId()  ).findFirst()
            //         .orElseThrow(
            //                 () -> new RuntimeException("At least one Phone number must remain"));

        }), () -> {
            throw new RuntimeException("Contact Info not found");
        });
        return contactInfoId;
    }

    @Override
    public List<Contact> findContactsByInstitution(
            int institutionId) {
        ContactModel contactModel = contactRepository.findById(institutionId).orElse(null);
        if (contactModel == null)
            return null;
        return hierarchyRepository.findByInstitution(contactModel).stream()
                .map(e -> converter.convert(e.getContact(), Contact.class)).collect(toList());
    }

    @Override
    public Set<JobLogTo> findJobLogs(int contactId) {
        ContactModel contact = verifyContact(contactId);

        return contact.getJobLogs().stream().map(r -> converter.convert(r, JobLogTo.class)).collect(toSet());
    }

    @Override
    public Set<UserTo> findUsers(int contactId) {
        ContactModel contact = verifyContact(contactId);

        return contact.getUsers().stream().map(r -> converter.convert(r, UserTo.class)).collect(toSet());
    }

    @Override
    public Set<PurposeTo> findPurposes(int contactId) {
        ContactModel contact = verifyContact(contactId);

        return contact.getPurposes().stream().map(r -> converter.convert(r, PurposeTo.class)).collect(toSet());
    }

    @Override
    public List<ContactWorkflowsTo> findWorkflows(int contactId) {
        ContactModel contact = verifyContact(contactId);

        return contact.getContactWorkflows().stream().map(r -> converter.convert(r, ContactWorkflowsTo.class)).collect(toList());
    }

    @Override
    public List<HierarchyTo> findParents(int contactId) {
        return hierarchyRepository.findByContactIdAndDeletedIsFalse(contactId)
            .stream().map(r -> converter.convert(r, HierarchyTo.class)).collect(Collectors.toList());
    }

    @Override
    public List<HierarchyTo> findMembers(int contactId) {
        return hierarchyRepository.findByInstitutionIdAndDeletedIsFalse(contactId)
            .stream().map(r -> converter.convert(r, HierarchyTo.class)).collect(Collectors.toList());
    }

    @Override
    public UserTo findCreatedBy(int contactId) {
        ContactModel contact = verifyContact(contactId);
        return userRepository.findById(contact.getCreatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

    @Override
    public UserTo findUpdatedBy(int contactId) {
        ContactModel contact = verifyContact(contactId);
        if (contact.getUpdatedBy() == null)
            return null;
        return userRepository.findById(contact.getUpdatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

}