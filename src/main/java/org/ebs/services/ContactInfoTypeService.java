package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.ContactInfoType;
import org.ebs.services.to.Input.ContactInfoTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ContactInfoTypeService {

    public Optional<ContactInfoType> findContactInfoType(int infoTypeId);

    public ContactInfoType findByContactInfo(int contactId);

    public Page<ContactInfoType> findContactInfoTypes(PageInput page, List<SortInput> sort,
	List<FilterInput> filters, boolean disjunctionFilters);

    public ContactInfoType createContactInfoType(ContactInfoTypeInput infoTypeInput);

    public ContactInfoType modifyContactInfoType(ContactInfoTypeInput infoTypeInput);

    public int deleteContactInfoType(int infoTypeId);
}