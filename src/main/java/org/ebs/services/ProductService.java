///////////////////////////////////////////////////////////
//  ProductService.java
//  Macromedia ActionScript Implementation of the Interface ProductService
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:20 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.DomainTo;
import org.ebs.services.to.EntityReferenceTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.Input.ProductInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:20 AM
 */
public interface ProductService {

    /**
     *
     * @param Product
     */
    public ProductTo createProduct(ProductInput Product);

    /**
     *
     * @param productId
     */
    public int deleteProduct(int productId);

    /**
     *
     * @param productId
     */
    public Optional<DomainTo> findDomain(int productId);

    /**
     *
     * @param productId
     */
    public Optional<HtmlTagTo> findHtmlTag(int productId);

    /**
     *
     * @param productId
     */
    public Optional<ProductTo> findProduct(int productId);

    /**
     *
     * @param productId
     */
    public Set<ProductFunctionTo> findProductFunctions(int productId);

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    public Page<ProductTo> findProducts(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters);

    /**
     *
     * @param product
     */
    public ProductTo modifyProduct(ProductInput product);

    public Set<ProductTo> findProductsByFunctionalUnit(int functionalUnitId);

    public List<ProductTo> findByPrintoutTemplate(int printoutTemplateId);

    public EntityReferenceTo findEntityReference(int entityReferenceId);
}