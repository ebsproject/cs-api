package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.AttributesTo;
import org.ebs.services.to.CFTypeTo;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.NodeCFInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface NodeCFService {

    public Optional<NodeCFTo> findNodeCF(int id);

    public Page<NodeCFTo> findNodeCFs(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public NodeCFTo createNodeCF(NodeCFInput input);

    public NodeCFTo modifyNodeCF(NodeCFInput input);

    public int deleteNodeCF(int id);

    public AttributesTo findAttributes(int nodeCfId);

    public CFTypeTo findCFType(int nodeCFId);

    public HtmlTagTo findHtmlTag(int nodeCFId);

    public NodeTo findNode(int nodeCFId);

    public CFValueTo findCFValue(int nodeCFId);

    public void deleteCFValueByNodeId(int nodeCFId);

    public TenantTo findTenant(int nodeCFId);
    
}