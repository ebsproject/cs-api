package org.ebs.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ebs.model.CategoryModel;
import org.ebs.model.ContactModel;
import org.ebs.model.ContactTypeModel;
import org.ebs.model.HierarchyModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.LanguageModel;
import org.ebs.model.PersonModel;
import org.ebs.model.RoleContactHierarchyModel;
import org.ebs.model.RoleModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.CategoryRepository;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.ContactTypeRepository;
import org.ebs.model.repos.HierarchyRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.model.repos.LanguageRepository;
import org.ebs.model.repos.PersonRepository;
import org.ebs.model.repos.RoleContactHierarchyRepository;
import org.ebs.model.repos.RoleRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.redirect.MicroIntegrationService;
import org.ebs.services.to.PersonSync;
import org.ebs.services.to.ProgramPersonTo;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Moves information from Core Breeding domain (CB) into Core System domain (CS)
 */
@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
class SynchronizeUserServiceImpl implements SynchronizeUserService {

    private final UserRepository userRepository;
    private final ContactRepository contactRepository;
    private final RoleRepository roleRepository;
    private final ContactTypeRepository contactTypeRepository;
    private final CBGraphQLService cbGraphQLService;
    private final CategoryRepository categoryRepository;
    private final InstitutionRepository institutionRepository;
    private final HierarchyRepository hierarchyRepository;
    private final RoleContactHierarchyRepository roleContactHierarchyRepository;
    private final PersonRepository personRepository;
    private final LanguageRepository languageRepository;
    private final TenantRepository tenantRepository;

    protected final TokenGenerator tokenGenerator;

    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;

    private static final int DEFAULT_TENANT = 1;
    private static final String defaultUserPreference = "{\"filters\": [], \"language\": [], \"favorites\": {\"domain\": [], \"columns\": [], \"products\": []}}";

    @Override
    public void synchronizeUsers() {
        log.info("synchronizing users...");
        userRepository.setExternalIdsToNull();
        TenantModel defaultTenant = tenantRepository.findById(DEFAULT_TENANT).orElse(null);
        if (defaultTenant == null) {
            log.info("No default tenant found, aborting sync process");
            return;
        }
        cbGraphQLService.findAllPersonsPaged()
            .forEach(cbPerson -> {
                //log.info("\n\n\nverifying person {} , active: {}", cbPerson.getEmail(), cbPerson.getIsActive());
                if (cbPerson.getEmail() != null && cbPerson.getPrograms() != null) {
                    importUser(cbPerson, defaultTenant);
                 } 
            });
        log.info("finish synchronizing users");
    }

    void importUser(PersonSync cbUser, TenantModel defaultTenant) {
        try {
            List<UserModel> csUser = userRepository.findByUserNameIgnoreCase(cbUser.getEmail().toLowerCase());
            if (csUser.isEmpty()) {
                createUser(cbUser, defaultTenant);
            } else {
                UserModel user = csUser.get(0);
                if (csUser.size() > 1) {
                    for (UserModel u : csUser) {
                        if (!u.getUserName().equals(cbUser.getEmail().toLowerCase())) {
                            log.info("removing " + u.getUserName());
                            u.setUserName(u.getUserName() + "_voided");
                            u.setDeleted(true);
                            u.setActive(false);
                            userRepository.save(u);
                        } else {
                            user = u;
                        }
                    }
                }
                updateUser(cbUser, user, defaultTenant);
            }
        } catch (Exception e) {
            log.info("Error synchronizing user {}. Cause: {}", cbUser.getEmail(), e.getMessage());
        }
    }

    private void createUser(PersonSync cbUser, TenantModel defaultTenant) throws Exception {
        //log.info("creating user...");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode preference = mapper.readTree(defaultUserPreference);
        boolean isAdmin = (cbUser.getPersonType() != null && cbUser.getPersonType().equals("admin")) ? true : false;
        ContactModel csContact = new ContactModel();
        List<ContactTypeModel> contactTypes = contactTypeRepository.findByNameAndDeletedIsFalse("EBS member");
        CategoryModel personCategory = categoryRepository.findByName("Person")
            .orElseThrow(() -> new RuntimeException("No 'Person' category found"));
        LanguageModel defaultLanguage = languageRepository.findByIdAndDeletedIsFalse(1)
            .orElseThrow(() -> new RuntimeException("No default language found"));
        csContact.setCategory(personCategory);
        csContact.setContactTypes(contactTypes);
        csContact.setEmail(cbUser.getEmail().toLowerCase());
        csContact = contactRepository.save(csContact);
        PersonModel csPerson = new PersonModel();
        csPerson.setGivenName(cbUser.getFirstName());
        csPerson.setFamilyName(cbUser.getLastName());
        csPerson.setFullName(csPerson.getFamilyName() + ", " + csPerson.getGivenName());
        csPerson.setGender("");
        csPerson.setContact(csContact);
        csPerson.setLanguage(defaultLanguage);
        personRepository.save(csPerson);
        if (cbUser.getPrograms() != null && !cbUser.getPrograms().isEmpty())
            setPrograms(csContact, cbUser.getPrograms());
        RoleModel role = new RoleModel();
        if (isAdmin)
            role = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse("Admin").orElse(null);
        else
            role = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse("CB User").orElse(null);
        UserModel csUser = new UserModel();
        csUser.setActive(cbUser.getIsActive());
        csUser.setAdmin(isAdmin);
        csUser.setUserName(cbUser.getEmail().toLowerCase());
        csUser.setExternalId(cbUser.getId());
        csUser.setContact(csContact);
        csUser.setPreference(preference);
        if (role != null)
            csUser.setDefaultRole(role);
        userRepository.save(csUser);
        defaultTenant.setUsers(new HashSet<>(Arrays.asList(csUser)));
        tenantRepository.save(defaultTenant);
    }

    private void setPrograms(ContactModel contact, Set<ProgramPersonTo> programs) {
        programs.forEach(program -> {
            if (program.getProgramDbId() != 0 && program.getPersonRole() != null) {
                List<InstitutionModel> csProgram = institutionRepository.findByExternalCodeAndUnitTypeName(program.getProgramDbId(), "Program");
                if (csProgram.isEmpty())
                    log.info("Could not assign program with DbId {}, no record with external code exists in CS", program.getProgramDbId());
                else if (csProgram.size() > 1)
                    log.info("Could not assign program with DbId {}, multiple records with external code exist in CS {}",
                        program.getProgramDbId());
                else {
                    RoleModel role = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse("CB User").orElse(null);
                    if (program.getPersonRole().toLowerCase().indexOf("team member") == -1)
                        role = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse(program.getPersonRole()).orElse(role);        
                    if (role == null) {
                        log.info("Could not assign program with external code {}, CB User role not found", programs);
                            return;
                    } else {
                        //log.info("assigning program {} with role {}", program.getProgramDbId(), role.getName());
                        HierarchyModel contactProgramRelation = new HierarchyModel();
                        contactProgramRelation.setContact(contact);
                        contactProgramRelation.setInstitution(csProgram.get(0).getContact());
                        contactProgramRelation.setPrincipalContact(false);
                        contactProgramRelation = hierarchyRepository.save(contactProgramRelation);
                        roleContactHierarchyRepository.save(new RoleContactHierarchyModel(role, contactProgramRelation));
                    }
                }
            } else {
                log.info("Could not assign program with DbId {}, not enough information in data fetched from CB", program.getProgramDbId());
            }
        });
    }

    private void updatePrograms(ContactModel contact, Set<ProgramPersonTo> programs) {
        programs.forEach(program -> {
            if (program.getProgramDbId() != 0 && program.getPersonRole() != null) {
                //log.info("verifying program {} to assign", program.getProgramCode());
                List<InstitutionModel> csProgram = institutionRepository.findByCommonNameAndUnitTypeNameAndDeletedIsFalse(program.getProgramCode(), "Program");
                if (csProgram.isEmpty())
                    log.info("Could not assign program {}, no record with code exists in CS", program.getProgramCode());
                else if (csProgram.size() > 1)
                    log.info("Could not assign program , multiple records with code {} exist in CS",
                        program.getProgramCode());
                else {
                    RoleModel role = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse("CB User").orElse(null);       
                    if (role == null) {
                        log.info("Could not assign program with external code {}, CB User role not found", programs);
                            return;
                    } else {
                        HierarchyModel existingRelation = hierarchyRepository
                            .findByContactAndInstitution(contact, csProgram.get(0).getContact())
                            .orElse(null);
                        if (existingRelation != null) {
                            //log.info("found relation with program");
                            existingRelation.setDeleted(false);
                            hierarchyRepository.save(existingRelation);
                            if (existingRelation.getRoles().isEmpty()) {
                                //log.info("saving with role {}", role.getName());
                                roleContactHierarchyRepository.save(new RoleContactHierarchyModel(role, existingRelation));
                            }
                        } else if (existingRelation == null) {
                            //log.info("assigning program {} with role {}", program.getProgramCode(), role.getName());
                            HierarchyModel contactProgramRelation = new HierarchyModel();
                            contactProgramRelation.setContact(contact);
                            contactProgramRelation.setInstitution(csProgram.get(0).getContact());
                            contactProgramRelation.setPrincipalContact(false);
                            contactProgramRelation = hierarchyRepository.save(contactProgramRelation);
                            roleContactHierarchyRepository.save(new RoleContactHierarchyModel(role, contactProgramRelation));
                        }
                    }
                }
            } else {
                log.info("Could not assign program with DbId {}, not enough information in data fetched from CB", program.getProgramDbId());
            }
        });
        RoleModel defaultRole = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse("CB User").orElse(null);
        //log.info("checking programs roles");
        contact.getParents().stream().filter(parent -> !parent.isDeleted()).forEach(parent -> {
            if (parent.getRoles() == null || parent.getRoles().isEmpty()) {
                //log.info("found program with no roles, assigning CB User role");
                if (defaultRole != null)
                    roleContactHierarchyRepository.save(new RoleContactHierarchyModel(defaultRole, parent));
                else
                    log.info("Could not assign role to program {}, CB User role not found", parent.getInstitution().getInstitution().getCommonName());
            }
        });
    }

    private void updateUser(PersonSync cbUser, UserModel csUser, TenantModel defaultTenant) {
        //log.info("updating...");
        csUser.setExternalId(cbUser.getId());
        csUser.setUserName(csUser.getUserName().toLowerCase());
        if (!cbUser.getIsActive())
            csUser.setActive(false);
        else 
            csUser.setActive(true);
        if (cbUser.isDeleted()) {
            csUser.setDeleted(true);
            csUser.getContact().setDeleted(true);
            csUser.getContact().getParents().forEach(parent -> {
                parent.setDeleted(true);
                hierarchyRepository.save(parent);
                roleContactHierarchyRepository.deleteByHierarchyId(parent.getId());
            });
        } else {
            csUser.setDeleted(false);
            csUser.getContact().setDeleted(false);
            updatePrograms(csUser.getContact(), cbUser.getPrograms());
            if (!defaultTenant.getUsers().contains(csUser)) {
                defaultTenant.setUsers(new HashSet<>(Arrays.asList(csUser)));
                tenantRepository.save(defaultTenant);
            }
            boolean isAdmin = (cbUser.getPersonType() != null && cbUser.getPersonType().equals("admin")) ? true : false;
            csUser.setAdmin(isAdmin);
            RoleModel role = new RoleModel();
            if (isAdmin)
                role = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse("Admin").orElse(null);
            else
                role = roleRepository.findByNameIgnoreCaseAndDeletedIsFalse("CB User").orElse(null);
            if (role != null)
                csUser.setDefaultRole(role);
        }
        userRepository.save(csUser);
        contactRepository.save(csUser.getContact());
    }

    @Override
    public List<PersonSync> findPersonSyncList() {

        List<PersonSync> personSyncs = new ArrayList<>();

        userRepository.findAll().forEach(user -> {
            if (!user.isDeleted()) {
                PersonSync personSync = createPersonSync(user);
                if (personSync != null)
                    personSyncs.add(personSync);
                else
                    log.info("Could not set person sync data for user {}", user.getUserName());
            }
        });

        return personSyncs;

    }

    @Override
    public void synchronizePersons() {
        log.info("Setting up Person Sync list data");
        callCbSynchronizePersons(findPersonSyncList());
    }

    private String callCbSynchronizePersons(List<PersonSync> csUsers) {
        ObjectMapper mapper = new ObjectMapper();
        String cbEndpoint = cbGraphQlEndpoint.replace("graphql", "");
        cbEndpoint = cbEndpoint.replace("cbapi", "cbgraphqlapi");
        if (cbEndpoint == null || cbEndpoint.isBlank()) {
            log.info("Could not get cbgraphql url from configuration");
            return "Could not get cbgraphql url from configuration";
        }
        try {
            log.info("Calling cbgraphql person sync API - {}", cbEndpoint);
            MicroIntegrationService service = new MicroIntegrationService(WebClient
                .builder()
                .baseUrl(cbEndpoint)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
                .build());
            String cbGraphResponse = service.redirectRequestToMI("/sync/persons", mapper.writeValueAsString(csUsers));
            log.info("Response from cbgraphql {}", cbGraphResponse);
            return cbGraphResponse;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("Error calling cbgraphql person sync API");
            return "Error calling cbgraphql person sync API";
        }
    }

    @Override
    public String synchronizePerson(int userId) {
        UserModel user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            log.info("Error synchronizing person to CB, provided user id {} not found", userId);
            return "Error synchronizing person to CB, provided user id not found";
        }
        PersonSync personSync = createPersonSync(user);    
        if (personSync != null) { 
            String cbGraphResponse = callCbSynchronizePersons(new ArrayList<>(Arrays.asList(personSync)));
            try {
                ObjectMapper om = new ObjectMapper();
                JsonNode node = om.readTree(cbGraphResponse);
                if (node.get("message") != null) {
                    if (node.get("message").asText().equals("success")) {
                        if (node.get("personId") != null && !user.isDeleted()) {
                            int externalId = node.get("personId").asInt();
                            userRepository.findByExternalId(externalId).ifPresent(u -> {
                                u.setExternalId(null);
                                userRepository.save(u);
                            });
                            user.setExternalId(externalId);
                            userRepository.save(user);    
                        }
                        return "success";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Error syncing user " + userId;
            }
            return "Error syncing user " + userId;
        }
        else {
            log.info("Could not set person sync data for user {}", user.getUserName());
            return "Could not set person sync data for user " + userId;
        }
    }

    private PersonSync createPersonSync(UserModel user) {
        if (user.getContact().getPerson() != null) {
            PersonSync personSync = new PersonSync();
            personSync.setEmail(user.getUserName());
            personSync.setFirstName(user.getContact().getPerson().getGivenName());
            personSync.setLastName(user.getContact().getPerson().getFamilyName());
            personSync.setIsActive(user.isActive());
            personSync.setPersonName(personSync.getLastName() + ", " + personSync.getFirstName());
            personSync.setPersonType(user.isAdmin() ? "admin" : "user");
            Set<ProgramPersonTo> personPrograms = new HashSet<>();
            if (user.getContact().getParents() != null) {
                user.getContact().getParents()
                    .removeIf(p -> p.getInstitution().getInstitution() == null
                        || p.getInstitution().getInstitution().getUnitType() == null
                        || !p.getInstitution().getInstitution().getUnitType().getName().equals("Program")
                        || p.isDeleted());
                user.getContact().getParents().forEach(p -> {
                    ProgramPersonTo program = new ProgramPersonTo();
                    if (p.getRoles() == null || p.getRoles().isEmpty())
                        program.setPersonRole("Team Member");
                    else
                        program.setPersonRole(p.getRoles().stream().findFirst().map(r -> r.getName()).orElse("Team Member"));
                    program.setProgramCode(p.getInstitution().getInstitution().getCommonName());
                    program.setProgramName(p.getInstitution().getInstitution().getLegalName());
                    personPrograms.add(program);
                });
            }
            personSync.setPrograms(personPrograms);
            log.info("{}", personSync.toString());
            return personSync;
        } else {
            return null;
        }
    }

}
