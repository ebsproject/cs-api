///////////////////////////////////////////////////////////
//  ProgramServiceImpl.java
//  Macromedia ActionScript Implementation of the Class ProgramServiceImpl
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:22 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactModel;
import org.ebs.model.ProgramModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.ProgramRepository;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.Input.ProgramInput;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:22 AM
 */
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ProgramServiceImpl implements ProgramService {

    private final ProgramRepository programRepository;
    private final ContactRepository contactRepository;
    private final ConversionService converter;

    /**
     *
     * @param Program
     */
    @Override
    @Transactional(readOnly = false)
    public ProgramTo createProgram(ProgramInput programInput) {
        programInput.setId(0);
        ProgramModel model = converter.convert(programInput, ProgramModel.class);
        verifyDependencies(model, programInput);

        model = programRepository.save(model);
        return converter.convert(model, ProgramTo.class);
    }

    void verifyDependencies(ProgramModel model, ProgramInput input) {
        
    }

    /**
     *
     * @param programId
     */
    @Override
    @Transactional(readOnly = false)
    public int deleteProgram(int programId) {
        ProgramModel program = verifyProgramModel(programId);
        program.setDeleted(true);
        programRepository.save(program);
        return programId;
    }

    ProgramModel verifyProgramModel(int programId) {
        return programRepository.findByIdAndDeletedIsFalse(programId)
                .orElseThrow(() -> new RuntimeException("Program not found"));
    }

    /**
     *
     * @param programId
     */
    @Override
    public Optional<ProgramTo> findProgram(int programId) {
        if (programId < 1) {
            return Optional.empty();
        }
        return contactRepository.findByIdAndDeletedIsFalse(programId)
                .map(r -> {
                    if (r.getInstitution() != null && r.getInstitution().getUnitType() != null
                        && r.getInstitution().getUnitType().getName().equals("Program"))
                        return converter.convert(r, ProgramTo.class);
                    else return null;
                });
    }

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    @Override
    public Page<ProgramTo> findPrograms(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
                if (filters == null) 
                    filters = new ArrayList<>();
                filters.forEach(f -> {
                    if (f.getCol().equals("code"))
                        f.setCol("institution.commonName");
                    else if (f.getCol().equals("name"))
                        f.setCol("institution.legalName");
                });
                filters.add(new FilterInput("institution.unitType.name", "Program", FilterMod.EQ));
                if (sort != null) {
                    sort.forEach(s -> {
                        if (!s.getCol().equals("id"))
                            s.setCol("institution." + s.getCol());
                    });
                }
        return contactRepository
                .findByCriteria(ContactModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ProgramTo.class));
    }

    /**
     *
     * @param input
     */
    @Override
    @Transactional(readOnly = false)
    public ProgramTo modifyProgram(ProgramInput input) {
        ProgramModel target = verifyProgramModel(input.getId());
        ProgramModel source = converter.convert(input, ProgramModel.class);

        verifyDependencies(source, input);
        Utils.copyNotNulls(source, target);

        return converter.convert(programRepository.save(target), ProgramTo.class);
    }

    /* @Override
    public List<ProgramTo> findByPrintoutTemplate(int printoutTemplateId) {
        return programRepository.findByPrintoutTemplatesId(printoutTemplateId).stream()
                .map(p -> converter.convert(p, ProgramTo.class)).collect(toList());
    } */


}