package org.ebs.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ebs.graphql.client.GraphQLClient;
import org.ebs.services.to.OrganizationCB;
import org.ebs.services.to.PersonSync;
import org.ebs.services.to.ProgramCB;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CBGraphQLServiceImpl implements CBGraphQLService {

    private static final String programListQuery = "query{findProgramList(page:{size:200 number:1}){content{programDbId programCode programName programType programStatus cropProgram{cropProgramCode cropProgramName crop{cropName cropCode} organization{id organizationName organizationCode}}}}}";
    private static final String organizationListQuery = "query{findOrganizationList{content{id organizationCode organizationName}}}";
    private final GraphQLClient graphQLClient;
    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;

    @Override 
    public List<OrganizationCB> findAllOrganizations() {
        try {
            String queryResult = graphQLClient.getQueryData(cbGraphQlEndpoint, organizationListQuery);
            ObjectMapper om = new ObjectMapper();
            JsonNode data = om.readTree(queryResult);
            if (data == null || data.get("data") == null
                || data.get("data").get("findOrganizationList") == null
                || data.get("data").get("findOrganizationList").get("content") == null) {
                    log.error("\n\findOrganizationList query returned a null response\n\n");
                    return null;
            }
            ObjectReader reader = om.readerFor(new TypeReference<List<OrganizationCB>>() {});
            
            return reader.readValue(data.get("data").get("findOrganizationList").get("content"));

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public List<ProgramCB> findAllPrograms() {
        try {
            String queryResult = graphQLClient.getQueryData(cbGraphQlEndpoint, programListQuery);
            ObjectMapper om = new ObjectMapper();
            JsonNode data = om.readTree(queryResult);
            if (data == null || data.get("data") == null
                || data.get("data").get("findProgramList") == null
                || data.get("data").get("findProgramList").get("content") == null) {
                    log.error("\n\nfindProgramList query returned a null response\n\n");
                    return null;
            }
            ObjectReader reader = om.readerFor(new TypeReference<List<ProgramCB>>() {});
            
            return reader.readValue(data.get("data").get("findProgramList").get("content"));

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<PersonSync> findAllPersons() {
        try {
            String personSyncListQuery = "query{findPersonSyncList{id email firstName lastName isActive deleted personName personType username programs{programDbId programCode programName programType personRole}}}";
            String queryResult = graphQLClient.getQueryData(cbGraphQlEndpoint, personSyncListQuery);
            ObjectMapper om = new ObjectMapper();
            JsonNode data = om.readTree(queryResult);
            if (data == null || data.get("data") == null
                || data.get("data").get("findPersonSyncList") == null) {
                    log.error("\n\nfindPersonSyncList query returned a null response\n\n");
                    return null;
            }
            ObjectReader reader = om.readerFor(new TypeReference<List<PersonSync>>() {});
            
            return reader.readValue(data.get("data").get("findPersonSyncList"));

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<PersonSync> findAllPersonsPaged() {
        try {
            List<PersonSync> allPersonSyncList = new ArrayList<>();
            String personSyncListQuery = "query{findPersonSyncListPaged(page:{size:50 number:1}){totalPages}}";
            String queryResult = graphQLClient.getQueryData(cbGraphQlEndpoint, personSyncListQuery);
            ObjectMapper om = new ObjectMapper();
            JsonNode data = om.readTree(queryResult);
            if (data == null || data.get("data") == null
                || data.get("data").get("findPersonSyncListPaged") == null
                || data.get("data").get("findPersonSyncListPaged").get("totalPages") == null) {
                    log.error("\n\nfindPersonSyncList query returned a null response\n\n");
                    return null;
            }
            int totalPages = data.get("data").get("findPersonSyncListPaged").get("totalPages").intValue();
            for (int i = 1; i <= totalPages; i++) {
                personSyncListQuery = "query{findPersonSyncListPaged(page:{size:50 number:" + i + "}){totalElements totalPages content {id email firstName lastName isActive deleted personName personType username programs{programDbId programCode programName programType personRole}}}}";
                queryResult = graphQLClient.getQueryData(cbGraphQlEndpoint, personSyncListQuery);
                om = new ObjectMapper();
                data = om.readTree(queryResult);
                if (data == null || data.get("data") == null
                    || data.get("data").get("findPersonSyncListPaged") == null
                    || data.get("data").get("findPersonSyncListPaged").get("content") == null) {
                        log.error("\n\nfindPersonSyncList query returned a null response\n\n");
                        continue;
                }
                ObjectReader reader = om.readerFor(new TypeReference<List<PersonSync>>() {});
                
                allPersonSyncList.addAll(reader.readValue(data.get("data").get("findPersonSyncListPaged").get("content")));
            }
            log.info("findPersonSyncListPaged total pages {}", totalPages);
            log.info("allPersonSyncList size {}", allPersonSyncList.size());
            return allPersonSyncList;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
