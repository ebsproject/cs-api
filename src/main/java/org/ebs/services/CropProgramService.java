package org.ebs.services;

import java.util.List;

import org.ebs.services.to.CropProgramTo;

public interface CropProgramService {

	public List<CropProgramTo> findByCrop(int cropId);

}