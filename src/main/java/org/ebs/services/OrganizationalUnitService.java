package org.ebs.services;

import java.util.List;

import org.ebs.services.to.Contact;
import org.ebs.services.to.OrganizationalUnit;
import org.ebs.services.to.Input.OrganizationalUnitInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface OrganizationalUnitService {
    
/*     public Page<OrganizationalUnit> findOrganizationalUnitList(List<FilterInput> filters, List<SortInput> sorts, PageInput page, boolean disjunctionFilters);

    public OrganizationalUnit findOrganizationalUnit(int id); */

    public Contact createOrganizationalUnit(OrganizationalUnitInput input);

    public Contact modifyOrganizationalUnit(OrganizationalUnitInput input);

    public boolean deleteOrganizationalUnit(int organizationalUnitId);

}
