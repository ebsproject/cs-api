package org.ebs.services;

import org.ebs.model.HierarchyModel;
import org.ebs.model.RoleContactHierarchyModel;
import org.ebs.model.RoleModel;
import org.ebs.model.repos.HierarchyRepository;
import org.ebs.model.repos.RoleContactHierarchyRepository;
import org.ebs.model.repos.RoleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class RoleContactHierarchyServiceImpl implements RoleContactHierarchyService {

    private final RoleContactHierarchyRepository roleContactHierarchyRepository;
    private final RoleRepository roleRepository;
    private final HierarchyRepository hierarchyRepository;

    @Override
    @Transactional(readOnly = false)
    public boolean createRoleContactHierarchy(int roleId, int contactHierarchyId) {

        deleteContactHierarchyRolesRelation(contactHierarchyId);

        RoleContactHierarchyModel model = roleContactHierarchyRepository.findByRoleIdAndHierarchyId(roleId, contactHierarchyId)
            .orElse(new RoleContactHierarchyModel());

        verifyRoleContactHierarchy(model);

        verifyDependenciesRoleContactHierarchy(model, roleId, contactHierarchyId);

        model = roleContactHierarchyRepository.save(model);
        
        return true;

    }

    @Override
    @Transactional(readOnly = false)
    public boolean deleteRoleContactHierarchy(int roleId, int contactHierarchyId) {

        if (!roleContactHierarchyRepository.findByRoleIdAndHierarchyId(roleId, contactHierarchyId)
                .isPresent())
            throw new RuntimeException("Role-ContactHierarchy relationship not found");
        else {
            HierarchyModel hierarchy = hierarchyRepository.findById(contactHierarchyId).orElse(null);
            if (hierarchy != null) {
                hierarchy.setDeleted(true);
                hierarchyRepository.save(hierarchy);
            }
            roleContactHierarchyRepository.deleteByHierarchyId(contactHierarchyId);
        }

        return true;

    }

    private void verifyRoleContactHierarchy(RoleContactHierarchyModel userRole) {
        roleContactHierarchyRepository.findByRoleAndHierarchy(userRole.getRole(), userRole.getHierarchy())
                .ifPresent(r -> {
                    throw new RuntimeException("Role-ContactHierarchy relationship already exists");
                });
    }

    private void verifyDependenciesRoleContactHierarchy(RoleContactHierarchyModel model, int roleId, int contactHierarchyId) {
        RoleModel role = roleRepository.findById(roleId)
                .orElseThrow(() -> new RuntimeException("Role not found"));
        HierarchyModel contactHierarchy = hierarchyRepository.findById(contactHierarchyId)
                .orElseThrow(() -> new RuntimeException("Contact Hierarchy not found"));
        model.setRole(role);
        model.setHierarchy(contactHierarchy);
    }

    @Override
    @Transactional(readOnly = false)
    public boolean deleteContactHierarchyRolesRelation(int contactHierarchyId) {

        hierarchyRepository.findById(contactHierarchyId)
            .orElseThrow(() -> new RuntimeException("Contact hierarchy not found"));
        
        roleContactHierarchyRepository.deleteByHierarchyId(contactHierarchyId);

        return true;

    }
    
}
