package org.ebs.services;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

public interface MessagingService {
    public String createJobLogFromPayload(String payload);

    public Set<Integer> getContactIdsToNotify(Set<Integer> userIds);

    public String createNotificationSimple(String payload);

    public String sendEmail(String payload, List<MultipartFile> file) throws Exception;

    public String printReport(String payload, String authorization, int userId) throws Exception;

    public String queueGigwaLoader(String payload, String authorization) throws Exception;
}
