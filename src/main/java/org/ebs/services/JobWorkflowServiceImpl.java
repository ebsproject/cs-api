package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.EmailTemplateModel;
import org.ebs.model.JobTypeModel;
import org.ebs.model.JobWorkflowModel;
import org.ebs.model.ProductFunctionModel;
import org.ebs.model.TenantModel;
import org.ebs.model.TranslationModel;
import org.ebs.model.repos.EmailTemplateRepository;
import org.ebs.model.repos.JobTypeRepository;
import org.ebs.model.repos.JobWorkflowRepository;
import org.ebs.model.repos.ProductFunctionRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobTypeTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.TranslationTo;
import org.ebs.services.to.Input.JobWorkflowInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class JobWorkflowServiceImpl implements JobWorkflowService {

    private final ProductFunctionRepository productFunctionRepository;
    private final JobTypeRepository jobTypeRepository;
    private final JobWorkflowRepository jobWorkflowRepository;
    private final TenantRepository tenantRepository;
    private final TranslationRepository translationRepository;
    private final EmailTemplateRepository emailTemplateRepository;
    private final ConversionService converter;

    @Override
    public Optional<JobWorkflowTo> findJobWorkflow(int jobWorkflowId) {
        if (jobWorkflowId < 1) {
            return Optional.empty();
        }
        return jobWorkflowRepository.findById(jobWorkflowId).filter(r -> !r.isDeleted())
                .map(r -> converter.convert(r, JobWorkflowTo.class));
    }

    @Override
    public Page<JobWorkflowTo> findJobWorkflows(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return jobWorkflowRepository.findByCriteria(JobWorkflowModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, JobWorkflowTo.class));
    }

    @Override
    public JobTypeTo findJobType(int jobWorkflowId) {
        JobWorkflowModel jobWorkflow = verifyJobWorkflow(jobWorkflowId);
        return converter.convert(jobWorkflow.getJobType(), JobTypeTo.class);
    }

    @Override
    public ProductFunctionTo findProductFunction(int jobWorkflowId) {
        JobWorkflowModel jobWorkflow = verifyJobWorkflow(jobWorkflowId);
        return converter.convert(jobWorkflow.getProductFunction(), ProductFunctionTo.class);
    }

    @Override
    public TranslationTo findTranslation(int jobWorkflowId) {
        JobWorkflowModel jobWorkflow = verifyJobWorkflow(jobWorkflowId);
        return converter.convert(jobWorkflow.getTranslation(), TranslationTo.class);
    }

    @Override
    public Set<JobLogTo> findJobLogs(int jobWorkflowId) {
        JobWorkflowModel jobWorkflow = verifyJobWorkflow(jobWorkflowId);
        return jobWorkflow.getJobLogs().stream().map(r -> converter.convert(r, JobLogTo.class)).collect(Collectors.toSet());
    }

    @Override
    public TenantTo findTenant(int jobWorkflowId) {
        JobWorkflowModel jobWorkflow = verifyJobWorkflow(jobWorkflowId);
        TenantModel tenant = tenantRepository.findById(jobWorkflow.getTenantId()).orElse(null);
        if (tenant == null)
            return null;
        return converter.convert(tenant, TenantTo.class);
    }

    @Override
    public EmailTemplateTo findEmailTemplate(int jobWorkflowId) {
        JobWorkflowModel jobWorkflow = verifyJobWorkflow(jobWorkflowId);
        return converter.convert(jobWorkflow.getEmailTemplate(), EmailTemplateTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public JobWorkflowTo createJobWorkflow(JobWorkflowInput input) {

        JobWorkflowModel model = converter.convert(input, JobWorkflowModel.class);

        model.setId(0);

        setJobWorkflowDependencies(input, model);

        return converter.convert(jobWorkflowRepository.save(model), JobWorkflowTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public JobWorkflowTo modifyJobWorkflow(JobWorkflowInput input) {

        JobWorkflowModel target = verifyJobWorkflow(input.getId());

        JobWorkflowModel source = converter.convert(input, JobWorkflowModel.class);

        Utils.copyNotNulls(source, target);

        setJobWorkflowDependencies(input, target);

        if (input.getEmailTemplateId() == null)
            target.setEmailTemplate(null);

        return converter.convert(jobWorkflowRepository.save(target), JobWorkflowTo.class);
        
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteJobWorkflow(int jobWorkflowId) {
        JobWorkflowModel jobWorkflow = verifyJobWorkflow(jobWorkflowId);

        jobWorkflow.setDeleted(true);

        jobWorkflowRepository.save(jobWorkflow);

        return jobWorkflowId;
    }

    JobWorkflowModel verifyJobWorkflow(int jobWorkflowId) {
        return jobWorkflowRepository.findById(jobWorkflowId)
                .orElseThrow(() -> new RuntimeException("Job Workflow not found"));
    }

    public void setJobWorkflowDependencies(JobWorkflowInput input, JobWorkflowModel model) {
        setJobWorkflowProductFunction(input, model);
        setJobWorkflowJobType(input, model);
        setJobWorkflowTenant(input, model);
        setJobWorkflowTranslation(input, model);
        setJobWorkflowEmailTemplate(input, model);
    }

    public void setJobWorkflowProductFunction(JobWorkflowInput input, JobWorkflowModel model) {
        if (input.getProductFunctionId() == null)
            throw new RuntimeException("A product function was not provided for the job workflow");
        ProductFunctionModel productFunction = productFunctionRepository.findById(input.getProductFunctionId())
            .orElseThrow(() -> new RuntimeException("Product function " + input.getProductFunctionId() + " not found"));
        model.setProductFunction(productFunction);
    }

    public void setJobWorkflowJobType(JobWorkflowInput input, JobWorkflowModel model) {
        if (input.getJobTypeId() == null)
            throw new RuntimeException("A job type was not provided to the job workflow");
        JobTypeModel jobType = jobTypeRepository.findById(input.getJobTypeId())
            .orElseThrow(() -> new RuntimeException("Job type " + input.getJobTypeId() + " not found"));
        model.setJobType(jobType);
    }

    public void setJobWorkflowTenant(JobWorkflowInput input, JobWorkflowModel model) {
        if (input.getTenantId() == null)
            throw new RuntimeException("A tenant was not provided for the job workflow");
        tenantRepository.findById(input.getTenantId())
            .orElseThrow(() -> new RuntimeException("Tenant " + input.getTenantId() + " not found"));
        model.setTenantId(input.getTenantId());
    }

    public void setJobWorkflowTranslation(JobWorkflowInput input, JobWorkflowModel model) {
        if (input.getTranslationId() != null) {
            TranslationModel translation = translationRepository.findById(input.getTranslationId())
                .orElseThrow(() -> new RuntimeException("Translation " + input.getTranslationId() + " not found"));
            model.setTranslation(translation);
        }
    }

    public void setJobWorkflowEmailTemplate(JobWorkflowInput input, JobWorkflowModel model) {
        if (input.getEmailTemplateId() != null && input.getEmailTemplateId() >= 0) {
            EmailTemplateModel emailTemplate = emailTemplateRepository.findById(input.getEmailTemplateId())
                .orElseThrow(() -> new RuntimeException("Email template " + input.getEmailTemplateId() + " not found"));
            model.setEmailTemplate(emailTemplate);
        }
    }

}
