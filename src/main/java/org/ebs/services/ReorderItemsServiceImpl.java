package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import org.ebs.model.ShipmentModel;
import org.ebs.model.ServiceItemModel;
import org.ebs.model.ServiceModel;
import org.ebs.model.ShipmentItemModel;
import org.ebs.model.repos.ShipmentRepository;
import org.ebs.model.repos.ServiceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static java.util.stream.Collectors.toList;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class ReorderItemsServiceImpl implements ReorderItemsService {

    private final ShipmentRepository shipmentRepository;
    private final ServiceRepository serviceRepository;

    @Override
    @Transactional(readOnly = false)
    public String reorderItems(Integer id, String entity) {

        if (entity.equals("Shipment")) {

            final List<ShipmentItemModel> target = new ArrayList<>();
            ShipmentModel ship = verifyShipment(id);
            target.addAll(ship.getItems().stream().filter(si -> !si.isDeleted()).collect(toList()));
            target.sort((a, b) -> a.getNumber() - b.getNumber());

            for (int i = 0; i < target.size(); i++) {
                int Id = target.get(i).getId();
                target.get(i).setId(Id);
                target.get(i).setNumber(i + 1);
            }

        } else {
            final List<ServiceItemModel> target = new ArrayList<>();
            ServiceModel ship = verifyService(id);
            target.addAll(ship.getItems().stream().filter(si -> !si.isDeleted()).collect(toList()));
            target.sort((a, b) -> a.getNumber() - b.getNumber());

            for (int i = 0; i < target.size(); i++) {
                int Id = target.get(i).getId();
                target.get(i).setId(Id);
                target.get(i).setNumber(i + 1);
            }
        }

        return "The items for " + entity + " have been reordered";
    }

    ShipmentModel verifyShipment(Integer shipmentId) {
        return shipmentRepository.findById(shipmentId).orElseThrow(
                () -> new RuntimeException("Shipment " + shipmentId + " not found"));
    }

    ServiceModel verifyService(Integer serviceId) {
        return serviceRepository.findById(serviceId).orElseThrow(
                () -> new RuntimeException("Service " + serviceId + " not found"));
    }

}
