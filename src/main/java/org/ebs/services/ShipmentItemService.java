package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.ShipItemInput;
import org.ebs.services.to.Input.ShipmentItemInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ShipmentItemService {

    public Optional<ShipmentItemTo> findShipmentItem(int id);

    public Page<ShipmentItemTo> findShipmentItems(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    public ShipmentItemTo createShipmentItem(ShipmentItemInput input);
    public int createShipmentItems(List<ShipmentItemInput> input);

    public ShipmentItemTo modifyShipmentItem(ShipmentItemInput input);

    /**
     * Perfoms bulk update of shipment items in two modes: by shipmentItem and by Shipment
     * -By shipment item: modifies only the items passed as argument
     * -By shipment: updates all items in a shipment using the first item in input as template
     * @param shipmentId the shipment to have all of its items updated (optional)
     * @param input the list of items to update, or the template to use when modifying all items of a shipment
     * @return the number of elements updated
     * 
     */
    public int modifyShipmentItems(Integer shipmentId, List<ShipItemInput> input);

    public int deleteShipmentItem(int id);

        /**
        * Perfoms bulk delete of shipment items in two modes: by shipmentItem and by Shipment
        * -By shipment item: deletes only the items with the ids passed as argument
        * -By shipment: deletes all items in a given shipment
        * @param shipmentId the shipment to have all of its items removed (optional)
        * @param input the list of item ids to remove (optional)
        * @return the number of elements deleted
        * 
        */
    public int deleteShipmentItems(Integer shipmentId, List<Integer> input);

    public ShipmentTo findShipment(int shipmentItemId);

    public TenantTo findTenant(int shipmentItemId);

}
