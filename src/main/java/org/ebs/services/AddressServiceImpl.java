package org.ebs.services;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ebs.model.AddressModel;
import org.ebs.model.ContactAddressModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.ContactAddressRepository;
import org.ebs.services.to.Address;
import org.ebs.services.to.Contact;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.Input.AddressInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final ContactAddressRepository contactAddressRepository;
    private final ConversionService converter;

    /**
     *
     * @param Address
     */
    @Override
    @Transactional(readOnly = false)
    public Address createAddress(AddressInput Address) {
        AddressModel model = converter.convert(Address, AddressModel.class);
        model.setId(0);

        model = addressRepository.save(model);
        return converter.convert(model, Address.class);
    }

    /**
     *
     * @param addressId
     */
    @Override
    @Transactional(readOnly = false)
    public int deleteAddress(int addressId) {
        AddressModel address = addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException("Address not found"));
        contactAddressRepository.deleteAll(contactAddressRepository.findByAddress(address));
        address.setDeleted(true);
        addressRepository.save(address);
        return addressId;
    }

    /**
     *
     * @param addressId
     */
    @Override
    public Optional<Address> findAddress(int addressId) {
        if (addressId < 1) {
            return Optional.empty();
        }
        return addressRepository.findById(addressId).filter(r -> !r.isDeleted())
                .map(r -> converter.convert(r, Address.class));
    }

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    @Override
    public Page<Address> findAddresses(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return addressRepository.findByCriteria(AddressModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, Address.class));
    }

    /**
     *
     * @param addressId
     */
    public List<Contact> findContacts(int addressId) {
        return addressRepository.findById(addressId).map(a -> a.getContacts().stream()
            .map(c -> converter.convert(c, Contact.class)).collect(toList())).orElse(null);
    }

    /**
     *
     * @param address
     */
    @Override
    @Transactional(readOnly = false)
    public Address modifyAddress(AddressInput address) {
        AddressModel target = addressRepository.findById(address.getId())
                .orElseThrow(() -> new RuntimeException("Address not found"));
        AddressModel source = converter.convert(address, AddressModel.class);
        Utils.copyNotNulls(source, target);
        return converter.convert(addressRepository.save(target), Address.class);
    }

    @Override
    public List<Address> findByContact(int contactId) {
        return addressRepository.findByContact(contactId).stream().map(a -> {
            Address addressTo = converter.convert(a, Address.class);
            ContactAddressModel ca = contactAddressRepository.findByContactIdAndAddressId(contactId, a.getId())
                .orElse(null);
            if (ca != null)
                addressTo.setDefault(ca.isDefault());
            return addressTo;
        }).collect(toList());
    }

    @Override
    public List<PurposeTo> findPurposes(int addressId) {
        return addressRepository.findById(addressId).map(r -> 
            r.getPurposes().stream().map(p -> converter.convert(p, PurposeTo.class)).collect(Collectors.toList())
        ).orElse(null);
    }

    @Override
    public Optional<Contact> findInstitution(int addressId) {

        return addressRepository.findById(addressId).map(a -> a.getContacts().stream()
            .filter(c -> c.getCategory().getName().equals("Institution")).findFirst()
            .map(r -> converter.convert(r, Contact.class))).orElse(null);

    }

    @Override
    public void setFullAddress() {
        return;
    }

}