package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ebs.model.ContactModel;
import org.ebs.model.PersonModel;
import org.ebs.model.ServiceModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.PersonRepository;
import org.ebs.model.repos.ServiceItemRepository;
import org.ebs.model.repos.ServiceRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowInstanceRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.Person;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.ServiceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ServiceServiceImpl implements ServiceService {

    private final ConversionService converter;
    private final ServiceRepository serviceRepository;
    private final PersonRepository personRepository;
    private final ContactRepository contactRepository;
    private final WorkflowInstanceRepository workflowInstanceRepository;
    private final TenantRepository tenantRepository;
    private final ServiceItemRepository serviceItemRepository;

    @Override
    public Optional<ServiceTo> findService(int id) {
        if (id < 1)
            return Optional.empty();

        return serviceRepository.findById(id).map(r -> converter.convert(r, ServiceTo.class));
    }

    @Override
    public Page<ServiceTo> findServices(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return serviceRepository
                .findByCriteria(ServiceModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ServiceTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public ServiceTo createService(ServiceInput input) {

        input.setId(0);

        ServiceModel model = converter.convert(input, ServiceModel.class);

        setServiceDependencies(model, input);

        model = serviceRepository.save(model);

        return converter.convert(model, ServiceTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public ServiceTo modifyService(ServiceInput input) {

        ServiceModel target = verifyServiceModel(input.getId());

        ServiceModel source = converter.convert(input, ServiceModel.class);

        Utils.copyNotNulls(source, target);

        if (input.getSubmitionDate() == null)
            target.setSubmitionDate(null);

        setServiceDependencies(target, input);

        return converter.convert(serviceRepository.save(target), ServiceTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteService(int id) {

        ServiceModel model = verifyServiceModel(id);

        model.setDeleted(true);

        serviceRepository.save(model);

        return id;

    }

    @Override
    public Person findPerson(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getPerson() == null)
            return null;

        return converter.convert(model.getPerson(), Person.class);

    }

    @Override
    public Contact findProgram(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getProgram() == null ||
            model.getProgram().getInstitution() == null ||
            model.getProgram().getInstitution().getUnitType() == null ||
            !model.getProgram().getInstitution().getUnitType().getName().equals("Program"))
            return null;

        return converter.convert(model.getProgram(), Contact.class);

    }

    @Override
    public Contact findSender(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getSender() == null)
            return null;

        return converter.convert(model.getSender(), Contact.class);

    }

    @Override
    public Contact findRequestor(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getRequestor() == null)
            return null;

        return converter.convert(model.getRequestor(), Contact.class);

    }

    @Override
    public Contact findRecipient(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getRecipient() == null)
            return null;

        return converter.convert(model.getRecipient(), Contact.class);

    }

    @Override
    public WorkflowInstanceTo findWorkflowInstance(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getWorkflowInstance() == null)
            return null;

        return converter.convert(model.getWorkflowInstance(), WorkflowInstanceTo.class);

    }

    @Override
    public TenantTo findTenant(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getTenant() == null)
            return null;

        return converter.convert(model.getTenant(), TenantTo.class);

    }

    ServiceModel verifyServiceModel(int serviceId) {
        return serviceRepository.findById(serviceId)
                .orElseThrow(() -> new RuntimeException("Service id " + serviceId + " not found"));
    }

    void setServiceDependencies(ServiceModel model, ServiceInput input) {
        setServicePerson(model, input.getPersonId());
        setServiceProgram(model, input.getProgramId());
        setServiceRecipient(model, input.getRecipientId());
        setServiceRequestor(model, input.getRequestorId());
        setServiceSender(model, input.getSenderId());
        setServiceWorkflowInstance(model, input.getWorkflowInstanceId());
        setServiceTenant(model, input.getTenantId());
        setServiceServiceProvider(model, input.getServiceProviderId());
    }

    void setServiceWorkflowInstance(ServiceModel model, Integer workflowInstanceId) {
        if (workflowInstanceId != null && workflowInstanceId > 0) {
            WorkflowInstanceModel workflowInstance = workflowInstanceRepository.findById(workflowInstanceId)
                    .orElseThrow(
                            () -> new RuntimeException("WorkflowInstance id " + workflowInstanceId + " not found"));
            model.setWorkflowInstance(workflowInstance);
        }
    }

    void setServiceProgram(ServiceModel model, Integer programId) {
        if (programId != null) {
            ContactModel program = contactRepository.findById(programId)
                    .orElseThrow(() -> new RuntimeException("Program " + programId + " not found"));
            if (program.getInstitution() == null ||
                program.getInstitution().getUnitType() == null ||
                !program.getInstitution().getUnitType().getName().equals("Program")) {
                    throw new RuntimeException("Invalid program");
                }
            model.setProgram(program);
        }
    }

    void setServicePerson(ServiceModel model, Integer personId) {
        if (personId != null && personId > 0) {
            PersonModel person = personRepository.findById(personId)
                    .orElseThrow(() -> new RuntimeException("Person id " + personId + " not found"));
            model.setPerson(person);
        }
    }

    void setServiceRecipient(ServiceModel model, Integer recipientId) {
        if (recipientId != null && recipientId > 0) {
            ContactModel contact = contactRepository.findById(recipientId)
                    .orElseThrow(() -> new RuntimeException("Person id " + recipientId + " not found"));
            model.setRecipient(contact);
        }
    }

    void setServiceRequestor(ServiceModel model, Integer requestorId) {
        if (requestorId != null && requestorId > 0) {
            ContactModel contact = contactRepository.findById(requestorId)
                    .orElseThrow(() -> new RuntimeException("Person id " + requestorId + " not found"));
            model.setRequestor(contact);
        }
    }

    void setServiceSender(ServiceModel model, Integer senderId) {
        if (senderId != null && senderId > 0) {
            ContactModel contact = contactRepository.findById(senderId)
                    .orElseThrow(() -> new RuntimeException("Person id " + senderId + " not found"));
            model.setSender(contact);
        }
    }

    void setServiceTenant(ServiceModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

    void setServiceServiceProvider(ServiceModel model, Integer serviceProviderId) {
        if (serviceProviderId != null && serviceProviderId > 0) {
            ContactModel contact = contactRepository.findById(serviceProviderId)
                    .orElseThrow(() -> new RuntimeException("Contact id " + serviceProviderId + " not found"));
            model.setServiceProvider(contact);
        }
    }

    @Override
    public List<ServiceItemTo> findItems(int serviceId) {

        return serviceItemRepository.findByServiceId(serviceId).stream()
                .map(r -> converter.convert(r, ServiceItemTo.class)).collect(Collectors.toList());

    }

    @Override
    public Contact findServiceProvider(int serviceId) {

        ServiceModel model = verifyServiceModel(serviceId);

        if (model.getServiceProvider() == null)
            return null;

        return converter.convert(model.getServiceProvider(), Contact.class);

    }

}
