package org.ebs.services;

import org.ebs.security.EbsUser;

public interface UserProfileService {

    public String buildUserProfile(int userId, String domainPrefixFilter, Integer programDbIdFilter);

    public String buildUserProfileExternalId(Integer id, String domainPrefixFilter, Integer programDbIdFilter, EbsUser tokenUser);

    public String buildUserProfileUsername(String username, String domainPrefixFilter, Integer programDbIdFilter);

    public String getUserPrograms(int userId);

    public String getUnitMembers(int unitId);

    public String getProgramMembers(int programId);

    public String getProductMembers(int productId);
    
}