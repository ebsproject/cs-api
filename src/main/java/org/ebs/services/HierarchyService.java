package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.Contact;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.Input.HierarchyInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface HierarchyService {

    Contact findContact(int contactId);

    Contact findInstitution(int institutionId);

    HierarchyTo createHierarchy(HierarchyInput hierarchy);

    HierarchyTo modifyHierarchy(HierarchyInput hierarchy, boolean verified);

    Optional<HierarchyTo> findHierarchy(int hierarchyId);

    HierarchyTo findHierarchy(int contactId, int institutionId);

    String deleteHierarchy(int contactId, int institutionId);

    int deleteHierarchy(int hierarchyId);

    Page<HierarchyTo> findHierarchyList(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    public boolean deleteContactParentsRelation(int contactId);

    public void cleanHierarchies();
}