package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.CFTypeTo;
import org.ebs.services.to.Input.CFTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface CFTypeService {

    public Optional<CFTypeTo> findCFType(int id);

    public Page<CFTypeTo> findCFTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public CFTypeTo createCFType(CFTypeInput input);

    public CFTypeTo modifyCFType(CFTypeInput input);

    public int deleteCFType(int id);
    
}