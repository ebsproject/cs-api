package org.ebs.services;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;


import com.fasterxml.jackson.databind.node.ObjectNode;

import org.ebs.model.AttributesModel;
import org.ebs.model.CFTypeModel;
import org.ebs.model.EmailTemplateModel;
import org.ebs.model.FileTypeModel;
import org.ebs.model.HtmlTagModel;
import org.ebs.model.NodeCFModel;
import org.ebs.model.NodeModel;
import org.ebs.model.NodeTypeModel;
import org.ebs.model.PhaseModel;
import org.ebs.model.ProcessModel;
import org.ebs.model.ProductModel;
import org.ebs.model.ServiceItemModel;
import org.ebs.model.StageModel;
import org.ebs.model.StatusTypeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UserModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.model.repos.AttributesRepository;
import org.ebs.model.repos.CFTypeRepository;
import org.ebs.model.repos.EmailTemplateRepository;
import org.ebs.model.repos.FileTypeRepository;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.NodeCFRepository;
import org.ebs.model.repos.NodeRepository;
import org.ebs.model.repos.NodeTypeRepository;
import org.ebs.model.repos.PhaseRepository;
import org.ebs.model.repos.ProcessRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.ServiceItemRepository;
import org.ebs.model.repos.StageRepository;
import org.ebs.model.repos.StatusTypeRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.ebs.model.repos.WorkflowViewTypeRepository;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.EmailTemplateInput;
import org.ebs.services.to.Input.FileTypeInput;
import org.ebs.services.to.Input.NodeCFInput;
import org.ebs.services.to.Input.NodeInput;
import org.ebs.services.to.Input.PhaseInput;
import org.ebs.services.to.Input.StageInput;
import org.ebs.services.to.Input.StatusTypeInput;
import org.ebs.services.to.Input.WorkflowInput;
import org.ebs.util.Connection;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class WorkflowServiceImpl implements WorkflowService {

    private final ConversionService converter;
    private final WorkflowRepository workflowRepository;
    private final HtmlTagRepository htmlTagRepository;
    private final ProductRepository productRepository;
    private final PhaseRepository phaseRepository;
    private final TenantRepository tenantRepository;
    private final ServiceItemRepository serviceItemRepository;
    private final StageRepository stageRepository;
    private final ObjectMapper objectMapper;
    private final WorkflowViewTypeRepository workflowViewTypeRepository;
    private final NodeTypeRepository nodeTypeRepository;
    private final NodeRepository nodeRepository;
    private final ProcessRepository processRepository;
    private final AttributesRepository attributesRepository;
    private final CFTypeRepository cfTypeRepository;
    private final NodeCFRepository nodeCFRepository;
    private final FileTypeRepository fileTypeRepository;
    private final StatusTypeRepository statusTypeRepository;
    private final EmailTemplateRepository emailTemplateRepository;
    private final int minPageNumber = 0;
    @Value("${spring.data.web.pageable.max-page-size}")
    private int maxPageSize = 50;
    private Pageable defaultPage = PageRequest.of(minPageNumber, 20);

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Optional<WorkflowTo> findWorkflow(int id) {
        if (id < 1)
            return Optional.empty();

        return workflowRepository.findById(id).map(r -> converter.convert(r, WorkflowTo.class));
    }

    @Override
    public Page<WorkflowTo> findWorkflows(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return workflowRepository
                .findByCriteria(WorkflowModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, WorkflowTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public WorkflowTo createWorkflow(WorkflowInput input) {

        if (!workflowRepository.findByName(input.getName())
                .stream().filter(r -> !r.isDeleted())
                .collect(Collectors.toList()).isEmpty())
            throw new RuntimeException("Workflow name must be unique");

        input.setId(0);

        WorkflowModel model = converter.convert(input, WorkflowModel.class);

        setWorkflowDependencies(model, input);

        model = workflowRepository.save(model);

        setWorkflowTenants(model, input);

        return converter.convert(model, WorkflowTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public WorkflowTo modifyWorkflow(WorkflowInput input) {

        WorkflowModel target = verifyWorkflowModel(input.getId());

        if (workflowRepository.findByName(input.getName())
                .stream()
                .filter(r -> !r.isDeleted() && !r.equals(target)).count() > 0)
            throw new RuntimeException("Workflow name must be unique");

        Utils.copyNotNulls(input, target);

        setWorkflowDependencies(target, input);

        setWorkflowTenants(target, input);

        return converter.convert(workflowRepository.save(target), WorkflowTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteWorkflow(int id) {

        WorkflowModel model = verifyWorkflowModel(id);

        model.setDeleted(true);

        workflowRepository.save(model);

        return id;

    }

    WorkflowModel verifyWorkflowModel(int workflowId) {
        return workflowRepository.findById(workflowId)
                .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
    }

    void setWorkflowDependencies(WorkflowModel model, WorkflowInput input) {
        setWorkflowHtmlTag(model, input.getHtmlTagId());
        setWorkflowProduct(model, input.getProductId());
    }

    @Override
    public HtmlTagTo findHtmlTag(int workflowId) {

        WorkflowModel model = verifyWorkflowModel(workflowId);

        if (model.getHtmlTag() == null)
            return null;

        return converter.convert(model.getHtmlTag(), HtmlTagTo.class);

    }

    @Override
    public ProductTo findProduct(int workflowId) {

        WorkflowModel model = verifyWorkflowModel(workflowId);

        if (model.getProduct() == null)
            return null;

        return converter.convert(model.getProduct(), ProductTo.class);

    }

    void setWorkflowHtmlTag(WorkflowModel model, Integer htmlTagId) {
        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                    .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
            model.setHtmlTag(htmlTag);
        }
    }

    void setWorkflowProduct(WorkflowModel model, Integer productId) {
        if (productId != null && productId > 0) {
            ProductModel product = productRepository.findById(productId)
                    .orElseThrow(() -> new RuntimeException("Product id " + productId + " not found"));
            model.setProduct(product);
        }
    }

    void setWorkflowTenants(WorkflowModel model, WorkflowInput input) {
        if (input.getTenantIds() == null)
            return;

        input.getTenantIds().forEach(tenantId -> {
            TenantModel tenant = tenantRepository.findByIdAndDeletedIsFalse(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant not found"));
            tenant.setWorkflows(new HashSet<>(Arrays.asList(model)));
            tenantRepository.save(tenant);
        });

        model.getTenants().forEach(tenant -> {
            if (!input.getTenantIds().contains(tenant.getId())) {
                tenant.getWorkflows().remove(model);
                tenantRepository.save(tenant);
            }
        });
    }

    @Override
    public List<PhaseTo> findPhases(int workflowId) {
        return phaseRepository.findByWorkflowId(workflowId)
                .stream().map(r -> converter.convert(r, PhaseTo.class)).collect(Collectors.toList());
    }

    @Override
    public List<StatusTypeTo> findStatusTypes(int workflowId) {
        WorkflowModel model = verifyWorkflowModel(workflowId);

        if (model.getStatusTypes() == null)
            return null;

        return model.getStatusTypes()
                .stream().map(r -> converter.convert(r, StatusTypeTo.class)).collect(Collectors.toList());
    }

    @Override
    public List<ContactWorkflowsTo> findContacts(int workflowId) {
        WorkflowModel workflow = verifyWorkflowModel(workflowId);

        return workflow.getWorkflowContacts().stream().map(r -> converter.convert(r, ContactWorkflowsTo.class))
                .collect(toList());
    }

    @Override
    @Transactional(readOnly = false)
    public Page<JsonNode> getWorkflowCustomFieldList(List<FilterInput> filters,
            List<SortInput> sorts, PageInput page, boolean disjunctionFilters) {

        if (filters == null || filters.isEmpty())
            throw new RuntimeException("filters array missing");
        if (sorts == null)
            sorts = new ArrayList<SortInput>();

        FilterInput workflowFilter = filters.stream().filter(f -> f.getCol().equals("workflowId"))
                .findFirst().orElse(null);
        FilterInput userFilter = filters.stream().filter(f -> f.getCol().equals("userId"))
                .findFirst().orElse(null);
        FilterInput includeItemsFilter = filters.stream().filter(f -> f.getCol().equals("includeItems"))
                .findFirst().orElse(null);
        FilterInput includeUniqueAncestorsFilter = filters.stream()
                .filter(f -> f.getCol().equals("includeUniqueAncestors"))
                .findFirst().orElse(null);

        if (workflowFilter == null || userFilter == null)
            throw new RuntimeException("incorrect filters");

        if (workflowFilter.getVal().trim().isBlank() || userFilter.getVal().trim().isBlank())
            throw new RuntimeException("incorrect filter values");

        int workflowId = Integer.parseInt(workflowFilter.getVal());
        int userId = Integer.parseInt(userFilter.getVal());
        boolean includeItems = false;
        if (includeItemsFilter != null) {
            includeItems = Boolean.parseBoolean(includeItemsFilter.getVal());
        }
        boolean includeUniqueAncestors = false;
        if (includeUniqueAncestorsFilter != null) {
            includeUniqueAncestors = Boolean.parseBoolean(includeUniqueAncestorsFilter.getVal());
        }

        filters.remove(workflowFilter);
        filters.remove(userFilter);
        filters.remove(includeItemsFilter);
        filters.remove(includeUniqueAncestorsFilter);

        StoredProcedureQuery storedProcedure = manager.createStoredProcedureQuery("workflow.jsonb_cf_filter")
                .registerStoredProcedureParameter("_workflow_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("_user_id", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("_filters_txt", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("_sorts_txt", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("pagenumber", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("pagesize", Integer.class, ParameterMode.IN);

        storedProcedure.setParameter("_workflow_id", workflowId)
                .setParameter("_user_id", userId)
                .setParameter("_filters_txt", filters.toString())
                .setParameter("_sorts_txt", sorts.toString())
                .setParameter("pagenumber", 1)
                .setParameter("pagesize", 100);

        try {
            if (storedProcedure.execute()) {
                Object s = storedProcedure.getSingleResult();
                ObjectMapper om = new ObjectMapper();
                JsonNode content = om.readTree(s.toString()).get("content");
                JsonNode count = om.readTree(s.toString()).get("totalelements");
                int totalElements = 1;
                if (count != null && count.getNodeType().equals(JsonNodeType.NUMBER))
                    totalElements = Integer.parseInt(count.toString());
                if (s != null && content != null && content.isArray() && !content.isEmpty()) {
                    List<JsonNode> list = new ArrayList<>();
                    for (JsonNode jsonNode : content) {
                        if (includeItems) {
                            FilterInput serviceFilter = filters.stream().filter(f -> f.getCol().equals("id"))
                                    .findFirst().orElseThrow(() -> new RuntimeException(
                                            "id filter must be included when retrieving items"));
                            int serviceId = Integer.parseInt(serviceFilter.getVal());
                            List<ServiceItemModel> items = serviceItemRepository.findByServiceId(serviceId);
                            JsonReader reader = Json.createReader(new StringReader(om.writeValueAsString(jsonNode)));
                            JsonObjectBuilder builder = Json.createObjectBuilder();
                            reader.readObject().entrySet().forEach(e -> builder.add(e.getKey(), e.getValue()));
                            reader = Json.createReader(
                                    new StringReader(om.writeValueAsString(om.readTree(items.toString()))));
                            builder.add("items", reader.readArray());
                            if (includeUniqueAncestors) {
                                /*
                                 * Set<String> uniqueAncestors = getUniqueAncestors(items);
                                 * if (uniqueAncestors.isEmpty())
                                 * builder.add("uniqueAncestors", JsonValue.NULL);
                                 * else
                                 * builder.add("uniqueAncestors", uniqueAncestors.toString().substring(1,
                                 * uniqueAncestors.toString().length()-1));
                                 */
                                getUniqueAncestors(items, builder);
                            }
                            list.add(om.readTree(builder.build().toString()));
                        } else
                            list.add(jsonNode);
                    }
                    return buildPage(list, page);
                } else {
                    return new Connection<JsonNode>(new ArrayList<JsonNode>(), createPage(page), totalElements);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error while building result data");
        }
        return null;

    }

    private void getUniqueAncestors(List<ServiceItemModel> items, JsonObjectBuilder builder) {
        Set<String> uniqueAncestorsPUD1 = new HashSet<>();
        Set<String> uniqueAncestorsPUD2 = new HashSet<>();
        items
                .stream()
                .filter(i -> i.getMtaStatus() != null
                        && i.getMlsAncestors() != null
                        && !i.getMlsAncestors().isEmpty()
                        && !i.getMlsAncestors().equals("NA"))
                .forEach(i -> {
                    switch (i.getMtaStatus().trim()) {
                        case "PUD":
                        case "PUD1": {
                            addMlsAncestors(i.getMlsAncestors().split(","), uniqueAncestorsPUD1);
                            break;
                        }
                        case "PUD2":
                            addMlsAncestors(i.getMlsAncestors().split(","), uniqueAncestorsPUD2);
                            break;
                    }
                });
        if (uniqueAncestorsPUD1.isEmpty())
            builder.add("uniqueAncestorsPUD1", JsonValue.NULL);
        else
            builder.add("uniqueAncestorsPUD1",
                    uniqueAncestorsPUD1.toString().substring(1, uniqueAncestorsPUD1.toString().length() - 1));
        if (uniqueAncestorsPUD2.isEmpty())
            builder.add("uniqueAncestorsPUD2", JsonValue.NULL);
        else
            builder.add("uniqueAncestorsPUD2",
                    uniqueAncestorsPUD2.toString().substring(1, uniqueAncestorsPUD2.toString().length() - 1));
    }

    private void addMlsAncestors(String[] mlsAncestors, Set<String> uniqueAncestors) {
        for (int split = 0; split < mlsAncestors.length; split++) {
            uniqueAncestors.add(mlsAncestors[split].trim());
        }
    }

    private Pageable createPage(PageInput pageInput) {
        return pageInput == null ? defaultPage
                : PageRequest.of(Math.max(minPageNumber, pageInput.getNumber() - 1),
                        Math.min(maxPageSize, pageInput.getSize()));
    }

    private Page<JsonNode> buildPage(List<JsonNode> list, PageInput page) {

        Pageable p = createPage(page);

        int totalElements = list.size();

        int start = p.getPageNumber() * p.getPageSize();

        int end = (p.getPageNumber() + 1) * p.getPageSize();

        if (start < totalElements) {
            list = list.subList(start, Math.min(end, totalElements));
        } else {
            throw new RuntimeException("Incorrect start page");
        }

        return new Connection<JsonNode>(list, p, totalElements);
    }

    @Override
    @Transactional(readOnly = false)
    public ResponseEntity<String> importWorkflow(MultipartFile file) {

        try {
            int workflowId = 0;
            JsonNode rootNode = objectMapper.readTree(file.getInputStream());
            WorkflowModel workflow = objectMapper.treeToValue(rootNode.get("workflow"), WorkflowModel.class);
            List<FileTypeModel> documents = objectMapper.readValue(rootNode.get("documents").toString(),
                    new TypeReference<List<FileTypeModel>>() {
                    });
            List<EmailTemplateModel> emailTemplates = objectMapper.readValue(rootNode.get("emailTemplate").toString(),
                    new TypeReference<List<EmailTemplateModel>>() {
                    });

            Set<PhaseModel> workflowPhases = workflow.getPhases();
            List<StatusTypeModel> statusTypeModels = workflow.getStatusTypes();
            WorkflowInput cInput = new WorkflowInput();
            Set<Integer> tenantsIds = Set.of(1);
            Integer htmlTagId = workflow.getHtmlTag().getId();
            cInput.setId(0);
            cInput.setTitle(workflow.getTitle());
            cInput.setName(workflow.getName() + " (Imported)");
            cInput.setDescription(workflow.getDescription() + " (Imported)");
            cInput.setHelp(workflow.getHelp());
            cInput.setSortNo(workflow.getSortNo());
            cInput.setIcon(workflow.getIcon());
            cInput.setTenantIds(tenantsIds);
            cInput.setHtmlTagId(workflow.getHtmlTag().getId());
            cInput.setApi(workflow.getApi());
            cInput.setNavigationIcon(workflow.getNavigationIcon());
            cInput.setNavigationName(workflow.getNavigationName()  + " (Imported)");
            cInput.setSequence(workflow.getSequence());
            cInput.setShowMenu(true);
            cInput.setSystem(workflow.getIsSystem());
            cInput.setSecurityDefinition(workflow.getSecurityDefinition());
            cInput.setRoleIds(null);
            WorkflowModel model = converter.convert(cInput, WorkflowModel.class);
            List<ProductModel> products = productRepository.findAll().stream()
                    .map(r -> converter.convert(r, ProductModel.class))
                    .collect(Collectors.toList());
            ProductModel product = products.stream()
                    .filter(f -> f.getName().equals(workflow.getProduct().getName())).findAny().orElse(null);
            model.setProduct(product);

            if (htmlTagId != null && htmlTagId > 0) {
                HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                        .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
                model.setHtmlTag(htmlTag);
            }

            try {
                model = workflowRepository.save(model);
                workflowId = model.getId();
                for (PhaseModel phase : workflowPhases) {
                    importPhases(phase, workflowId);
                }
                for (StatusTypeModel statusType : statusTypeModels) {
                    importWorkflowStatusType(statusType, workflowId);
                }

                for (FileTypeModel doc : documents) {
                    importDocuments(doc, workflowId);
                }
                for (EmailTemplateModel template : emailTemplates) {
                    importEmailTemplates(template);
                }

                updateWorkflowStatusTypeNodeNewId(model);
                updateMoveNodeNewId(model);
                updateExecuteNodeIfConditionNewId(model);
                updateFormNodeNewId(model);
                updateEmailTemplateId(model);

            } catch (Exception e) {
                return ResponseEntity.badRequest().body("There was an error in the import process: "+ e.toString());
            }
            return ResponseEntity
                    .ok("The workflow has been imported successfully: "
                            + workflow.getDescription());
        } catch (IOException e) {
            return ResponseEntity.badRequest().body("Invalid JSON file for workflow definition");
        }
    }

    private void importPhases(PhaseModel phase, int workflowId) {
        int phaseId = 0;
        Set<StageModel> stages = phase.getStages();
        PhaseInput pInput = new PhaseInput();
        Integer tenantId = 1;
        Integer htmlTagId = phase.getHtmlTag().getId();
        Integer workflowViewTypeId = phase.getWorkflowViewType().getId();
        pInput.setId(0);
        pInput.setName(phase.getName());
        pInput.setDescription(phase.getDescription());
        pInput.setHelp(phase.getHelp());
        pInput.setSequence(phase.getSequence());
        pInput.setDependOn(phase.getDependOn());
        pInput.setIcon(phase.getIcon());
        pInput.setDesignRef(phase.getDesignRef());
        PhaseModel model = converter.convert(pInput, PhaseModel.class);
        if (workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                    .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                    .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
            model.setHtmlTag(htmlTag);
        }
        if (workflowViewTypeId != null && workflowViewTypeId > 0) {
            WorkflowViewTypeModel workflowViewType = workflowViewTypeRepository.findById(workflowViewTypeId)
                    .orElseThrow(
                            () -> new RuntimeException("WorkflowViewType id " + workflowViewTypeId + " not found"));
            model.setWorkflowViewType(workflowViewType);
        }
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
        try {
            model = phaseRepository.save(model);
            phaseId = model.getId();
            for (StageModel stage : stages) {
                importStages(stage, phaseId, workflowId);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void importStages(StageModel stage, int _phaseId, Integer workflowId) {
        Integer stageId = 0;
        Integer tenantId = 1;
        StageInput sInput = new StageInput();
        Set<NodeModel> nodes = stage.getNodes();
        sInput.setId(0);
        sInput.setName(stage.getName());
        sInput.setDescription(stage.getDescription());
        sInput.setHelp(stage.getHelp());
        sInput.setSequence(stage.getSequence());
        sInput.setDependOn(stage.getDependOn());
        sInput.setIcon(stage.getIcon());
        sInput.setDesignRef(stage.getDesignRef());
        Integer htmlTagId = stage.getHtmlTag().getId();
        Integer workflowViewTypeId = stage.getWorkflowViewType().getId();
        Integer phaseId = _phaseId;
        StageModel model = converter.convert(sInput, StageModel.class);

        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                    .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
            model.setHtmlTag(htmlTag);
        }

        if (phaseId != null && phaseId > 0) {
            PhaseModel phase = phaseRepository.findById(phaseId)
                    .orElseThrow(() -> new RuntimeException("Phase id " + phaseId + " not found"));
            model.setPhase(phase);
        }
        if (workflowViewTypeId != null && workflowViewTypeId > 0) {
            WorkflowViewTypeModel workflowViewType = workflowViewTypeRepository.findById(workflowViewTypeId)
                    .orElseThrow(
                            () -> new RuntimeException("WorkflowViewType id " + workflowViewTypeId + " not found"));
            model.setWorkflowViewType(workflowViewType);
        }

        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
        try {
            model = stageRepository.save(model);
            stageId = model.getId();
            for (NodeModel node : nodes) {
                importNodes(node, stageId, workflowId);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void importNodes(NodeModel node, Integer stageId, Integer workflowId) {
        NodeInput nInput = new NodeInput();
        Set<NodeCFModel> nodeCfs = Optional.ofNullable(node.getNodeCFs()).orElse(Collections.emptySet());

        Integer htmlTagId = 1;
        Integer processId = node.getProcess().getId();
        Integer workflowViewTypeId = node.getWorkflowViewType().getId();
        Integer nodeTypeId = node.getNodeType().getId();
        Integer tenantId = 1;

        nInput.setId(0);
        nInput.setName(node.getName());
        nInput.setDescription(node.getDescription());
        nInput.setHelp(node.getHelp());
        nInput.setDefine(node.getDefine());
        nInput.setDependOn(node.getDependOn());
        nInput.setIcon(node.getIcon());
        nInput.setDesignRef(node.getDesignRef());
        nInput.setMessage(node.getMessage());
        nInput.setSecurityDefinition(node.getSecurityDefinition());
        nInput.setSequence(node.getSequence());
        nInput.setRequireApproval(node.getRequireApproval());
        nInput.setValidationCode(node.getValidationCode());
        nInput.setValidationType(node.getValidationType());
        nInput.setRequireInput(node.getRequireInput());

        NodeModel model = converter.convert(nInput, NodeModel.class);

        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                    .orElseThrow(() -> new RuntimeException("Html tag id " + htmlTagId + " not found"));
            model.setHtmltag(htmlTag);
        }

        if (workflowId != null && workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                    .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
        if (processId != null && processId > 0) {
            ProcessModel process = processRepository.findById(processId)
                    .orElseThrow(() -> new RuntimeException("Process id " + processId + " not found"));
            model.setProcess(process);
        }
        if (workflowViewTypeId > 0) {
            WorkflowViewTypeModel workflowViewType = workflowViewTypeRepository.findById(workflowViewTypeId)
                    .orElseThrow(
                            () -> new RuntimeException("WorkflowViewType id " + workflowViewTypeId + " not found"));
            model.setWorkflowViewType(workflowViewType);
        }
        if (nodeTypeId > 0) {
            NodeTypeModel nodeType = nodeTypeRepository.findById(nodeTypeId)
                    .orElseThrow(() -> new RuntimeException("NodeType id " + nodeTypeId + " not found"));
            model.setNodeType(nodeType);
        }
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }

        try {
            model = nodeRepository.save(model);
            int nodeId = model.getId();
            setStageNodes(stageId, nodeId);
            if (nodeCfs.size() > 0) {
                for (NodeCFModel nodeCf : nodeCfs) {
                    importNodeCfs(nodeCf, nodeId);
                }
            }
        } catch (Exception e) {
            System.out.println("Line 684" + e);
        }
    }

    private void importNodeCfs(NodeCFModel nodeCf, Integer nodeId) {
        NodeCFInput cfInput = new NodeCFInput();
        Integer attributesId = 0;
        Integer cfTypeId = 0;
        Integer htmlTagId = 1;
        Integer tenantId = 1;
        if (nodeCf.getAttributes() != null)
            attributesId = nodeCf.getAttributes().getId();
        if (nodeCf.getCfType() != null)
            cfTypeId = nodeCf.getCfType().getId();

        cfInput.setId(0);
        cfInput.setName(nodeCf.getName());
        cfInput.setDescription(nodeCf.getDescription());
        cfInput.setHelp(nodeCf.getHelp());
        cfInput.setRequired(nodeCf.getRequired());
        cfInput.setApiAttributesName(nodeCf.getApiAttributesName());
        cfInput.setFieldAttributes(nodeCf.getFieldAttributes());

        NodeCFModel model = converter.convert(cfInput, NodeCFModel.class);

        if (attributesId != null && attributesId > 0) {
            AttributesModel attributes = attributesRepository.findById(attributesId)
                    .orElseThrow(() -> new RuntimeException("Attributes id  not found"));
            model.setAttributes(attributes);
        }
        if (cfTypeId != null && cfTypeId > 0) {
            CFTypeModel cfType = cfTypeRepository.findById(cfTypeId)
                    .orElseThrow(() -> new RuntimeException("CFType id not found"));
            model.setCfType(cfType);
        }
        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                    .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
            model.setHtmlTag(htmlTag);
        }
        if (nodeId != null && nodeId > 0) {
            NodeModel node = nodeRepository.findById(nodeId)
                    .orElseThrow(() -> new RuntimeException("Node id " + nodeId + " not found"));
            model.setNode(node);
        }
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }

        try {
            model = nodeCFRepository.save(model);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    private void setStageNodes(Integer stageId, Integer nodeId) {

        StageModel stage = stageRepository.findByIdAndDeletedIsFalse(stageId)
                .orElseThrow(() -> new RuntimeException("Stage not found"));
        NodeModel model = nodeRepository.findByIdAndDeletedIsFalse(nodeId)
                .orElseThrow(() -> new RuntimeException("Node not found"));
        model.setStages(new HashSet<>(Arrays.asList(stage)));
        nodeRepository.save(model);
    }

    private void importWorkflowStatusType(StatusTypeModel statusType, Integer workflowId) {

        StatusTypeInput stInput = new StatusTypeInput();
        Integer tenantId = 1;
        stInput.setId(0);
        stInput.setName(statusType.getName());
        stInput.setDescription(statusType.getDescription());
        stInput.setHelp(statusType.getHelp());
        StatusTypeModel model = converter.convert(stInput, StatusTypeModel.class);
        if (workflowId != null && workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                    .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
        try {
            model = statusTypeRepository.save(model);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void updateWorkflowStatusTypeNodeNewId(WorkflowModel workflow) {
        List<PhaseModel> phases = phaseRepository.findByWorkflowId(workflow.getId())
                .stream().map(r -> converter.convert(r, PhaseModel.class)).collect(Collectors.toList());
        for (PhaseModel phase : phases) {
            List<StageModel> stages = stageRepository.findByPhaseId(phase.getId())
                    .stream().map(r -> converter.convert(r, StageModel.class)).collect(Collectors.toList());
            for (StageModel stage : stages) {
                List<NodeModel> nodes = nodeRepository.findByStage(stage.getId())
                        .stream().map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                for (NodeModel node : nodes) {
                    if (node.getProcess().getId() == 5) {
                        JsonNode jsonNode = node.getDefine();
                        if (jsonNode != null && jsonNode.has("statusName")) {
                            String statusName = jsonNode.get("statusName").asText();
                            List<StatusTypeModel> statusTypes = statusTypeRepository.findAll().stream()
                                    .filter(f -> f.getWorkflow().getId() == workflow.getId())
                                    .map(r -> converter.convert(r, StatusTypeModel.class)).collect(Collectors.toList());
                            StatusTypeModel statusType = statusTypes.stream()
                                    .filter(f -> f.getName().equals(statusName)).findAny().orElse(null);
                            if (statusTypes != null) {
                                Integer newId = statusType.getId();
                                ((ObjectNode) jsonNode).put("status", String.valueOf(newId));
                                node.setDefine(jsonNode);
                                nodeRepository.save(node);
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateMoveNodeNewId(WorkflowModel workflow) {
        List<PhaseModel> phases = phaseRepository.findByWorkflowId(workflow.getId())
                .stream().map(r -> converter.convert(r, PhaseModel.class)).collect(Collectors.toList());
        for (PhaseModel phase : phases) {
            List<StageModel> stages = stageRepository.findByPhaseId(phase.getId())
                    .stream().map(r -> converter.convert(r, StageModel.class)).collect(Collectors.toList());
            for (StageModel stage : stages) {
                List<NodeModel> nodes = nodeRepository.findByStage(stage.getId())
                        .stream().map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                for (NodeModel node : nodes) {
                    if (node.getProcess().getId() == 6) {
                        JsonNode jsonNode = node.getDefine();

                        if (jsonNode != null && jsonNode.has("nodeName")) {
                            String nodeName = jsonNode.get("nodeName").asText();

                            List<NodeModel> nodeList = nodeRepository.findAll().stream()
                                    .filter(f -> f.getWorkflow().getId() == workflow.getId())
                                    .map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                            NodeModel nodeItem = nodeList.stream()
                                    .filter(f -> f.getName().equals(nodeName)).findAny().orElse(null);
                            if (nodeItem != null) {
                                Integer newId = nodeItem.getId();
                                ((ObjectNode) jsonNode).put("node", String.valueOf(newId));
                                node.setDefine(jsonNode);
                                nodeRepository.save(node);
                            }
                        }
                    }
                }
            }
        }
    }

    private void importDocuments(FileTypeModel file, Integer workflowId) {
        FileTypeInput fInput = new FileTypeInput();
        fInput.setId(0);
        fInput.setDescription(file.getDescription());

        FileTypeModel model = converter.convert(fInput, FileTypeModel.class);
        if (workflowId != null && workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                    .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
        model = fileTypeRepository.save(model);

    }

    private void importEmailTemplates(EmailTemplateModel emailTemplate) {
        EmailTemplateInput eInput = new EmailTemplateInput();
        eInput.setId(0);
        eInput.setName(emailTemplate.getName());
        eInput.setSubject(emailTemplate.getSubject());
        eInput.setTemplate(emailTemplate.getTemplate());
        EmailTemplateModel model = converter.convert(eInput, EmailTemplateModel.class);
        model = emailTemplateRepository.save(model);

    }

    private void updateEmailTemplateId(WorkflowModel workflow) {

        List<PhaseModel> phases = phaseRepository.findByWorkflowId(workflow.getId())
                .stream().map(r -> converter.convert(r, PhaseModel.class)).collect(Collectors.toList());
        for (PhaseModel phase : phases) {
            List<StageModel> stages = stageRepository.findByPhaseId(phase.getId())
                    .stream().map(r -> converter.convert(r, StageModel.class)).collect(Collectors.toList());
            for (StageModel stage : stages) {
                List<NodeModel> nodes = nodeRepository.findByStage(stage.getId())
                        .stream().map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                for (NodeModel node : nodes) {
                    if (node.getProcess().getId() == 4) {
                        JsonNode jsonNode = node.getDefine();
                        if (jsonNode != null && jsonNode.has("emailName")) {
                            String emailName = jsonNode.get("emailName").asText();
                            List<EmailTemplateModel> templates = emailTemplateRepository.findAll().stream()
                                    .map(r -> converter.convert(r, EmailTemplateModel.class))
                                    .sorted(Comparator.comparing(EmailTemplateModel::getId).reversed())
                                    .collect(Collectors.toList());
                            EmailTemplateModel template = templates.stream()
                                    .filter(f -> f.getName().equals(emailName)).findAny().orElse(null);
                            if (template != null) {
                                Integer newId = template.getId();
                                ((ObjectNode) jsonNode).put("email", String.valueOf(newId));
                                node.setDefine(jsonNode);
                                nodeRepository.save(node);
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateExecuteNodeIfConditionNewId(WorkflowModel workflow) {
        List<PhaseModel> phases = phaseRepository.findByWorkflowId(workflow.getId())
                .stream().map(r -> converter.convert(r, PhaseModel.class)).collect(Collectors.toList());
        for (PhaseModel phase : phases) {
            List<StageModel> stages = stageRepository.findByPhaseId(phase.getId())
                    .stream().map(r -> converter.convert(r, StageModel.class)).collect(Collectors.toList());
            for (StageModel stage : stages) {
                List<NodeModel> nodes = nodeRepository.findByStage(stage.getId())
                        .stream().map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                for (NodeModel node : nodes) {
                    if (node.getProcess().getId() == 11) {
                        JsonNode jsonNode = node.getDefine();

                        if (jsonNode != null && jsonNode.has("executeNodeIfTrueName")) {
                            String nodeName = jsonNode.get("executeNodeIfTrueName").asText();

                            List<NodeModel> nodeList = nodeRepository.findAll().stream()
                                    .filter(f -> f.getWorkflow().getId() == workflow.getId())
                                    .map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                            NodeModel nodeItem = nodeList.stream()
                                    .filter(f -> f.getName().equals(nodeName)).findAny().orElse(null);
                            if (nodeItem != null) {
                                Integer newId = nodeItem.getId();
                                ((ObjectNode) jsonNode).put("executeNodeIfTrue", String.valueOf(newId));
                                node.setDefine(jsonNode);
                                nodeRepository.save(node);
                            }
                        }
                        
                        if (jsonNode != null && jsonNode.has("executeNodeIfFalseName")) {
                            String nodeName = jsonNode.get("executeNodeIfFalseName").asText();

                            List<NodeModel> nodeList = nodeRepository.findAll().stream()
                                    .filter(f -> f.getWorkflow().getId() == workflow.getId())
                                    .map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                            NodeModel nodeItem = nodeList.stream()
                                    .filter(f -> f.getName().equals(nodeName)).findAny().orElse(null);
                            if (nodeItem != null) {
                                Integer newId = nodeItem.getId();
                                ((ObjectNode) jsonNode).put("executeNodeIfFalse", String.valueOf(newId));
                                node.setDefine(jsonNode);
                                nodeRepository.save(node);
                            }
                        }
                    }
                }
            }
        }
    }
    private void updateFormNodeNewId(WorkflowModel workflow) {
        List<PhaseModel> phases = phaseRepository.findByWorkflowId(workflow.getId())
                .stream().map(r -> converter.convert(r, PhaseModel.class)).collect(Collectors.toList());
        for (PhaseModel phase : phases) {
            List<StageModel> stages = stageRepository.findByPhaseId(phase.getId())
                    .stream().map(r -> converter.convert(r, StageModel.class)).collect(Collectors.toList());
            for (StageModel stage : stages) {
                List<NodeModel> nodes = nodeRepository.findByStage(stage.getId())
                        .stream().map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                for (NodeModel node : nodes) {
                    if (node.getProcess().getId() == 9) {
                        JsonNode jsonNode = node.getDefine();

                        if (jsonNode != null && jsonNode.has("formName")) {
                            String formName = jsonNode.get("formName").asText();

                            List<NodeModel> nodeList = nodeRepository.findAll().stream()
                                    .filter(f -> f.getWorkflow().getId() == workflow.getId())
                                    .map(r -> converter.convert(r, NodeModel.class)).collect(Collectors.toList());
                            NodeModel nodeItem = nodeList.stream()
                                    .filter(f -> f.getName().equals(formName)).findAny().orElse(null);
                            if (nodeItem != null) {
                                Integer newId = nodeItem.getId();
                                ((ObjectNode) jsonNode).put("form", String.valueOf(newId));
                                node.setDefine(jsonNode);
                                nodeRepository.save(node);
                            }
                        }
                    }
                }
            }
        }
    }


}
