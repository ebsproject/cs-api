package org.ebs.services;

import java.util.List;

import org.ebs.services.to.PersonSync;

public interface SynchronizeUserService {
    /**
     * Synchronizes users between domains, from Core Breeding into Core System
     */
    void synchronizeUsers();

    List<PersonSync> findPersonSyncList();

    /**
     * Synchronizes users between domains, from Core System into Core Breeding
     */
    void synchronizePersons();

    String synchronizePerson(int userId);

}
