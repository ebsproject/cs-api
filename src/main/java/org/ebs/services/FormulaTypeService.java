///////////////////////////////////////////////////////////
//  FormulaTypeService.java
//  Macromedia ActionScript Implementation of the Interface FormulaTypeService
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:59 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.Set;
import java.util.List;
import java.util.Optional;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
import org.ebs.services.to.FormulaTypeTo;
import org.ebs.services.to.Input.FormulaTypeInput;
import org.ebs.services.to.SegmentTo;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:59 AM
 */
public interface FormulaTypeService {

	/**
	 *
	 * @param FormulaType
	 */
	public FormulaTypeTo createFormulaType(FormulaTypeInput FormulaType);

	/**
	 *
	 * @param formulaTypeId
	 */
	public int deleteFormulaType(int formulaTypeId);

	/**
	 *
	 * @param formulaTypeId
	 */
	public Optional<FormulaTypeTo> findFormulaType(int formulaTypeId);

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	public Page<FormulaTypeTo> findFormulaTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

	/**
	 *
	 * @param formulaType
	 */
	public FormulaTypeTo modifyFormulaType(FormulaTypeInput formulaType);

}