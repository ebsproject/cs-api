package org.ebs.services;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.RoleModel;
import org.ebs.model.repos.RoleRepository;
import org.ebs.model.repos.SecurityRuleRoleRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.RoleTo;
import org.ebs.services.to.SecurityRuleTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.RoleInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:24 AM
 */
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final ConversionService converter;
    private final UserRepository userRepository;
    private final SecurityRuleRoleRepository securityRuleRoleRepository;


    /**
     *
     * @param Role
     */
    @Override
    @Transactional(readOnly = false)
    public RoleTo createRole(RoleInput role) {

        roleRepository.findByName(role.getName()).ifPresent(r -> {
            throw new RuntimeException("A role with name " + role.getName() + " already exists");
        });

        RoleModel model = converter.convert(role, RoleModel.class);

        model.setId(0);

        model = roleRepository.save(model);

        return converter.convert(model, RoleTo.class);
    }

    /**
     *
     * @param roleId
     */
    @Override
    @Transactional(readOnly = false)
    public int deleteRole(int roleId) {
        RoleModel role = roleRepository.findById(roleId).orElseThrow(() -> new RuntimeException("Role not found"));
        if (role.isSystem())
            throw new RuntimeException("System role cannot be deleted");
        role.setDeleted(true);
        roleRepository.save(role);
        return roleId;
    }

    /**
     *
     * @param roleId
     */
    public Set<ProductFunctionTo> findProductFunctions(int roleId) {
        return roleRepository.findById(roleId).get().getProductfunctions().stream()
                .filter(pf -> !pf.getProduct().isDeleted())
                .map(e -> converter.convert(e, ProductFunctionTo.class)).collect(Collectors.toSet());
    }

    /**
     *
     * @param roleId
     */
    @Override
    public Optional<RoleTo> findRole(int roleId) {
        if (roleId < 1) {
            return Optional.empty();
        }
        return roleRepository.findById(roleId).filter(r -> !r.isDeleted()).map(r -> converter.convert(r, RoleTo.class));
    }

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    @Override
    public Page<RoleTo> findRoles(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return roleRepository.findByCriteria(RoleModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, RoleTo.class));
    }

    /**
     *
     * @param role
     */
    @Override
    @Transactional(readOnly = false)
    public RoleTo modifyRole(RoleInput role) {
        RoleModel target = roleRepository.findById(role.getId())
                .orElseThrow(() -> new RuntimeException("Role not found"));
        roleRepository.findByName(role.getName()).ifPresent(r -> {
            if (r.getId() != role.getId())
                throw new RuntimeException("A role with name " + role.getName() + " already exists");
        });
        RoleModel source = converter.convert(role, RoleModel.class);
        Utils.copyNotNulls(source, target);
        return converter.convert(roleRepository.save(target), RoleTo.class);
    }

    @Override
    public List<RoleTo> findByRole(int roleId) {
        return roleRepository.findById(roleId).stream().map(e -> converter.convert(e, RoleTo.class))
                .collect(toList());
    }

    @Override
    public List<RoleTo> findByContactHierarchy(int contactHierarchyId) {
        return roleRepository.findByContactHierarchiesId(contactHierarchyId).stream().map(e -> converter.convert(e, RoleTo.class))
                .collect(toList());
    }

    @Override
    public List<SecurityRuleTo> findRuleByRole(int roleId) {
        return securityRuleRoleRepository.findByRoleId(roleId).stream()
                .map(e -> converter.convert(e.getRule(), SecurityRuleTo.class)).collect(toList());
    }


    private RoleModel verifyRole(int roleId) {
        return roleRepository.findById(roleId)
            .orElseThrow(() -> new RuntimeException("Role not found"));
    }

    @Override
    public UserTo findCreatedBy(int roleId) {
        RoleModel role = verifyRole(roleId);
        return userRepository.findById(role.getCreatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

    @Override
    public UserTo findUpdatedBy(int roleId) {
        RoleModel role = verifyRole(roleId);
        if (role.getUpdatedBy() == null)
            return null;
        return userRepository.findById(role.getUpdatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

}