package org.ebs.services;

import java.util.List;

import org.ebs.model.ContactWorkflowModel;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.ContactWorkflowInput;

public interface ContactWorkflowService {

    //List<ContactWorkflowsTo> findContactWorkflows(int contactId);

    Contact findContact(int contactWorkflowId);

    WorkflowTo findWorkflow(int contactWorkflowId);

    ContactWorkflowsTo createContactWorkflow(ContactWorkflowInput input);

    ContactWorkflowsTo modifyContactWorkflow(ContactWorkflowInput input);

    String deleteContactWorkflow(int contactId, int workflowId);

    int deleteContactWorkflow(int id);

    boolean deleteContactWorkflowsRelation(int contactId);

    List<ContactWorkflowsTo> findByContactId(int contactId);

    List<ContactWorkflowModel> findModelByContactId(int contactId);

   // Page<ContactWorkflowTo> findContactWorkflowList(PageInput page, List<SortInput> sort, List<FilterInput> filters,
    //        boolean disjunctionFilters);
}