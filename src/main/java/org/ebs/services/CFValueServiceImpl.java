package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.ebs.model.CFValueModel;
import org.ebs.model.EventModel;
import org.ebs.model.NodeCFModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.CFValueRepository;
import org.ebs.model.repos.EventRepository;
import org.ebs.model.repos.NodeCFRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.CFValueInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CFValueServiceImpl implements CFValueService {
    
    private final ConversionService converter;
    private final CFValueRepository cfValueRepository;
    private final EventRepository eventRepository;
    private final NodeCFRepository nodeCFRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<CFValueTo> findCFValue(int id) {
        if (id < 1)
            return Optional.empty();

        return cfValueRepository.findById(id).map(r -> converter.convert(r, CFValueTo.class));
    }

    @Override
    public Page<CFValueTo> findCFValues(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return cfValueRepository
                .findByCriteria(CFValueModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, CFValueTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public CFValueTo createCFValue(CFValueInput input) {
        
        input.setId(0);

        CFValueModel model = converter.convert(input, CFValueModel.class);

        setCFValueDependencies(model, input);

        model = cfValueRepository.save(model);

        return converter.convert(model, CFValueTo.class);

    }
    
    @Override
    @Transactional(readOnly = false, timeout = 60000)
    public Integer createCFValues(List<CFValueInput> input) {
        List<CFValueModel> cfValueModelModel = new ArrayList<>();
        input.forEach(item -> {
            item.setId(0);
            CFValueModel model = converter.convert(item, CFValueModel.class);
            setCFValueDependencies(model, item);
            cfValueModelModel.add(model);
        });
        cfValueRepository.saveAll(cfValueModelModel);
        return input.size();
    }

    @Override
    @Transactional(readOnly = false)
    public CFValueTo modifyCFValue(CFValueInput input) {
        
        CFValueModel target = verifyCFValueModel(input.getId());

        CFValueModel source = converter.convert(input, CFValueModel.class);

        Utils.copyNotNulls(source, target);

        setCFValueDependencies(target, input);

        return converter.convert(cfValueRepository.save(target), CFValueTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public Integer modifyCFValues(List<CFValueInput> input) {
        input.forEach(item -> {
          modifyCFValue(item);
        });
        return input.size();
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteCFValue(int id) {

        CFValueModel model = verifyCFValueModel(id);

        model.setDeleted(true);

        cfValueRepository.save(model);

        return id;

    }

    @Override
    public NodeCFTo findNodeCF(int cfValueId) {
        
        CFValueModel model = verifyCFValueModel(cfValueId);

        if (model.getNodeCF() == null)
            return null;
        
        return converter.convert(model.getNodeCF(), NodeCFTo.class);

    }

    @Override
    public EventTo findEvent(int cfValueId) {
        
        CFValueModel model = verifyCFValueModel(cfValueId);

        if (model.getEvent() == null)
            return null;
        
        return converter.convert(model.getEvent(), EventTo.class);

    }

    @Override
    public TenantTo findTenant(int cfValueId) {
        
        CFValueModel model = verifyCFValueModel(cfValueId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    CFValueModel verifyCFValueModel(int cfValueId) {
        return cfValueRepository.findById(cfValueId)
            .orElseThrow(() -> new RuntimeException("CFValue id " + cfValueId + " not found"));
    }

    void setCFValueDependencies(CFValueModel model, CFValueInput input) {
        setCFValueNodeCF(model, input.getNodeCFId());
        setCFValueEvent(model, input.getEventId());
        setCFValueTenant(model, input.getTenantId());
    }

    void setCFValueNodeCF(CFValueModel model, Integer nodeCFId) {
        if (nodeCFId != null && nodeCFId > 0) {
            NodeCFModel nodeCF = nodeCFRepository.findById(nodeCFId)
                .orElseThrow(() -> new RuntimeException("NodeCF id " + nodeCFId + " not found"));
            model.setNodeCF(nodeCF);
        }
    } 

    void setCFValueEvent(CFValueModel model, Integer eventId) {
        if (eventId != null && eventId > 0) {
            EventModel event = eventRepository.findById(eventId)
                .orElseThrow(() -> new RuntimeException("Event id " + eventId + " not found"));
            model.setEvent(event);
        }
    } 

    void setCFValueTenant(CFValueModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    } 

}
