package org.ebs.services;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactInfoModel;
import org.ebs.model.ContactModel;
import org.ebs.model.repos.ContactInfoRepository;
import org.ebs.model.repos.ContactRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactInfo;
import org.ebs.services.to.Input.ContactInfoInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ContactInfoServiceImpl implements ContactInfoService {

    private final ContactInfoRepository contactInfoRepository;
    private final ContactRepository contactRepository;
    private final ConversionService converter;

    public List<ContactInfo> findByContact(int contactId) {
        return contactInfoRepository.findByContact(contactId).stream().map(t -> converter.convert(t, ContactInfo.class))
                .collect(toList());
    }

    @Override
    public Contact findContactInfoContact(int contactInfoId) {

        return contactInfoRepository.findById(contactInfoId).map(
            r -> converter.convert(r.getContact(), Contact.class)
        ).orElse(null);
        
    }

    @Override
    public Optional<ContactInfo> findContactInfo(int contactInfoId) {
        return contactInfoRepository.findById(contactInfoId)
            .map(r -> converter.convert(r, ContactInfo.class));
    }

    @Override
    public Page<ContactInfo> findContactInfos(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
                return contactInfoRepository.findByCriteria(ContactInfoModel.class,filters,sort,page,disjunctionFilters)
                    .map(r -> converter.convert(r,ContactInfo.class));
    }

    @Transactional(readOnly = false)
    @Override
    public ContactInfo createContactInfo(ContactInfoInput input) {

        input.setValue(input.getValue().trim());

        input.setId(0);

        List<ContactInfoModel> existingContactInfos = 
            contactInfoRepository.findByContactInfoTypeIdAndContactIdAndValueIgnoreCaseAndDeletedIsFalse(input.getContactInfoTypeId(), input.getContactId(), input.getValue());

        ContactInfoModel model = converter.convert(input, ContactInfoModel.class);

        if (!existingContactInfos.isEmpty())
            throw new RuntimeException("Cannot add duplicate Additional Information for " + model.getContactInfoType().getName());

        setContactInfoContact(model, input.getContactId());

        return converter.convert(contactInfoRepository.save(model), ContactInfo.class);
    }

    void setContactInfoContact(ContactInfoModel model, int contactId) {
        ContactModel contact = contactRepository.findById(contactId)
            .orElseThrow(() -> new RuntimeException("Contact not found"));
        model.setContact(contact);
    }

    @Transactional(readOnly = false)
    @Override
    public ContactInfo modifyContactInfo(ContactInfoInput input) {

        input.setValue(input.getValue().trim());

        ContactInfoModel target = contactInfoRepository.findById(input.getId())
            .orElseThrow(() -> new RuntimeException("Contact Info not found"));

        List<ContactInfoModel> existingContactInfos = 
            contactInfoRepository.findByContactInfoTypeIdAndContactIdAndValueIgnoreCaseAndDeletedIsFalse(input.getContactInfoTypeId(), input.getContactId(), input.getValue());

        ContactInfoModel source = converter.convert(input, ContactInfoModel.class);

        if (existingContactInfos.size() > 1) 
            throw new RuntimeException("Cannot add duplicate Additional Information for " + source.getContactInfoType().getName());
        if (existingContactInfos.size() == 1 && existingContactInfos.get(0).getId() != target.getId())
            throw new RuntimeException("Cannot add duplicate Additional Information for " + source.getContactInfoType().getName());

        Utils.copyNotNulls(source, target);

        ofNullable(input.getContactId()).ifPresent(
            ci -> {
                setContactInfoContact(target, input.getContactId());
            }
        );

        return converter.convert(contactInfoRepository.save(target), ContactInfo.class);
    }

    @Transactional(readOnly = false)
    @Override
    public int deleteContactInfo(int contactInfoId) {
        
        ContactInfoModel contactInfo = contactInfoRepository.findById(contactInfoId)
            .orElseThrow(() -> new RuntimeException("Contact Info not found"));

        contactInfo.setDeleted(true);

        contactInfoRepository.save(contactInfo);

        return contactInfoId;
    }

    @Override
    public String getPersonCoopkey(int personId) {
        return contactInfoRepository
            .findByContactPersonIdAndContactDeletedIsFalseAndDeletedIsFalse(personId)
            .stream()
            .filter(ci -> ci.getContactInfoType().getName().toLowerCase().equals("coopkey"))
            .findFirst()
            .map(ci -> ci.getValue())
            .orElse(null);
    }

}