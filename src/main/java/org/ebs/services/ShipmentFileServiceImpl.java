package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.ebs.model.FileObjectModel;
import org.ebs.model.ShipmentFileModel;
import org.ebs.model.ShipmentModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.FileObjectRepository;
import org.ebs.model.repos.ShipmentFileRepository;
import org.ebs.model.repos.ShipmentRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.ShipmentFileTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.ShipmentFileInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ShipmentFileServiceImpl implements ShipmentFileService {

    private final ConversionService converter;
    private final FileObjectRepository fileObjectRepository;
    private final ShipmentRepository shipmentRepository;
    private final ShipmentFileRepository shipmentFileRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<ShipmentFileTo> findShipmentFile(int id) {
        if (id < 1)
            return Optional.empty();

        return shipmentFileRepository.findById(id).map(r -> converter.convert(r, ShipmentFileTo.class));
    }

    @Override
    public Page<ShipmentFileTo> findShipmentFiles(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return shipmentFileRepository
                .findByCriteria(ShipmentFileModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ShipmentFileTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public ShipmentFileTo createShipmentFile(ShipmentFileInput input) {

        input.setId(0);

        ShipmentFileModel model = converter.convert(input, ShipmentFileModel.class);

        setShipmentFileDependencies(model, input);

        model = shipmentFileRepository.save(model);

        return converter.convert(model, ShipmentFileTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public ShipmentFileTo modifyShipmentFile(ShipmentFileInput input) {

        ShipmentFileModel target = verifyShipmentFile(input.getId());

        ShipmentFileModel source = converter.convert(input, ShipmentFileModel.class);

        Utils.copyNotNulls(source, target);

        setShipmentFileDependencies(target, input);

        return converter.convert(shipmentFileRepository.save(target), ShipmentFileTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteShipmentFile(int id) {

        ShipmentFileModel model = verifyShipmentFile(id);

        model.setDeleted(true);

        shipmentFileRepository.save(model);

        return id;

    }

    @Override
    public ShipmentTo findShipment(int shipmentFileId) {

        ShipmentFileModel model = verifyShipmentFile(shipmentFileId);

        if (model.getShipment() == null)
            return null;

        return converter.convert(model.getShipment(), ShipmentTo.class);

    }

    @Override
    public FileObjectTo findFileObject(int shipmentFileId) {

        ShipmentFileModel model = verifyShipmentFile(shipmentFileId);

        if (model.getFile() == null)
            return null;

        return converter.convert(model.getFile(), FileObjectTo.class);

    }

    @Override
    public TenantTo findTenant(int shipmentFileId) {

        ShipmentFileModel model = verifyShipmentFile(shipmentFileId);

        if (model.getTenant() == null)
            return null;

        return converter.convert(model.getTenant(), TenantTo.class);

    }

    ShipmentFileModel verifyShipmentFile(int shipmentFileId) {
        return shipmentFileRepository.findById(shipmentFileId)
                .orElseThrow(
                        () -> new RuntimeException("Shipment File relationship id " + shipmentFileId + " not found"));
    }

    void setShipmentFileDependencies(ShipmentFileModel model, ShipmentFileInput input) {

        setShipmentFileShipment(model, input.getShipmentId());
        setShipmentFileRelationship(model, input.getFileId());
        setShipmentFileTenant(model, input.getTenantId());

    }

    void setShipmentFileShipment(ShipmentFileModel model, Integer shipmentId) {

        if (shipmentId != null) {
            ShipmentModel shipment = shipmentRepository.findById(shipmentId)
                    .orElseThrow(() -> new RuntimeException("Shipment " + shipmentId + " not found"));
            model.setShipment(shipment);
        }

    }

    void setShipmentFileRelationship(ShipmentFileModel model, UUID fileObjectId) {

        if (fileObjectId != null) {
            FileObjectModel file = fileObjectRepository.findById(fileObjectId)
                    .orElseThrow(() -> new RuntimeException("File " + fileObjectId + " not found"));
            model.setFile(file);
        }

    }

    void setShipmentFileTenant(ShipmentFileModel model, Integer tenantId) {
        if (tenantId != null) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

}
