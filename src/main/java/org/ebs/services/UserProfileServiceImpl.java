package org.ebs.services;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.ebs.model.ContactModel;
import org.ebs.model.ContactWorkflowModel;
import org.ebs.model.CropModel;
import org.ebs.model.DomainModel;
import org.ebs.model.HierarchyModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.PersonModel;
import org.ebs.model.ProductFunctionModel;
import org.ebs.model.ProductModel;
import org.ebs.model.RoleModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.ContactWorkflowRepository;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.security.EbsUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

    private final UserRepository userRepository;
    private final DomainRepository domainRepository;
    private final ProductRepository productRepository;
    private final InstitutionRepository institutionRepository;
    private final ContactRepository contactRepository;
    private final CropRepository cropRepository;
    private final ContactWorkflowRepository contactWorkflowRepository;

    private boolean canRequestGenotyping(List<ContactWorkflowModel> genotypingProviders, int userId, Set<String> roleNames, Set<Integer> parentIds) {
/*         for (ContactWorkflowModel provider : genotypingProviders) {
            if (parentIds.contains(provider.getContact().getId()))
                return true;
        } */
        JsonNode securityDefinition = genotypingProviders.get(0).getWorkflow().getSecurityDefinition();
        if (securityDefinition == null || roleNames.isEmpty() || parentIds.isEmpty())
            return false;
        if (securityDefinition.get("roles") != null && securityDefinition.get("roles").isArray()) {
            for (JsonNode role : securityDefinition.get("roles")) {
                if(role.get("name") != null) {
                    if (roleNames.contains(role.get("name").asText()))
                        return true;
                }
            }
        }
        if (securityDefinition.get("users") != null) {
            for (JsonNode user : securityDefinition.get("users")) {
                if(user.get("id") != null) {
                    if (userId == user.get("id").asInt())
                        return true;
                }
            }
        }
        if (securityDefinition.get("programs") != null) {
            for (JsonNode program : securityDefinition.get("programs")) {
                if(program.get("id") != null) {
                    if (parentIds.contains(program.get("id").asInt()))
                        return true;
                }
            }
        }
        return false;
    }

    private void buildUserTenantSubscriptionsJson(JsonObjectBuilder mainBuilder, UserModel user, Integer tenantId, Set<CropModel> crops) {

        JsonObjectBuilder singleTenantBuilder = Json.createObjectBuilder();
        JsonArrayBuilder tenantsBuilder = Json.createArrayBuilder();
        JsonObjectBuilder subscriptionBuilder = Json.createObjectBuilder();

        List<TenantModel> tenantList = new ArrayList<>();

        if (tenantId != null) {
            tenantList = user.getTenants().stream().filter(t -> t.getId() == tenantId).collect(Collectors.toList());
            if (tenantList.isEmpty()) {
                throw new RuntimeException("User is not a part of tenant with id " + tenantId);
            }
        } else {
            tenantList = user.getTenants().stream().sorted(new Comparator<TenantModel>() {
                public int compare(TenantModel t1, TenantModel t2) {
                    return Integer.compare(t1.getId(), t2.getId());
                }
            }).collect(Collectors.toList());
        }

        for (TenantModel tenant : tenantList) {
            singleTenantBuilder.add("id", tenant.getId());
            singleTenantBuilder.add("crop_id", "1");
            singleTenantBuilder.add("crop_name", "Rice");
            singleTenantBuilder.add("customer", "IRRI");
            singleTenantBuilder.add("organization", "Default");
            tenantsBuilder.add(singleTenantBuilder);
            singleTenantBuilder = Json.createObjectBuilder();
        }

        subscriptionBuilder.add("tenants", tenantsBuilder);

        setSubscriptions(subscriptionBuilder, crops);

        mainBuilder.add("subscription", subscriptionBuilder);

    }

    @Override
    public String buildUserProfileExternalId(Integer externalId, String domainPrefixFilter, Integer programDbIdFilter, EbsUser tokenUser) {

        UserModel user = userRepository.findByExternalIdAndDeletedIsFalse(externalId).orElse(null);

        if (user == null || user.getContact() == null)
            return null;
        
        if (!tokenUser.isAdmin() && tokenUser.getId() != user.getId()) {
            return "Unauthorized";
        }

        return buildUserProfile(user.getId(), domainPrefixFilter, programDbIdFilter);

    }

    @Override
    public String buildUserProfileUsername(String username, String domainPrefixFilter, Integer programDbIdFilter) {

        UserModel user = userRepository.findByUserNameIgnoreCaseAndDeletedIsFalse(username.toLowerCase()).orElse(null);
        if (user == null || user.getContact() == null)
            return null;

        return buildUserProfile(user.getId(), domainPrefixFilter, programDbIdFilter);

    }

    @Override
    public String getUserPrograms(int userId) {

        JsonObjectBuilder mainBuilder = Json.createObjectBuilder();
        JsonArrayBuilder programsBuilder = Json.createArrayBuilder();

        UserModel user = userRepository.findById(userId).orElse(null);

        if (user == null || user.getContact() == null) {
            mainBuilder.add("message", "user " + userId + " does not exist");
            return mainBuilder.build().toString();
        }

        List<HierarchyModel> programs = user.getContact().getParents();

        if (programs == null || programs.isEmpty()) {
            mainBuilder.add("message", "user " + userId + " does not belong to any program");
            return mainBuilder.build().toString();
        }

        programs.removeIf(p -> p.isDeleted() || p.getInstitution() == null || p.getInstitution().getInstitution() == null
            || p.getInstitution().getInstitution().getUnitType() == null
            || p.getInstitution().isDeleted());

        programsBuilder = Json.createArrayBuilder();
        for (HierarchyModel program : programs) {
            JsonObjectBuilder singleProgramBuilder = Json.createObjectBuilder();
            singleProgramBuilder.add("type", program.getInstitution().getInstitution().getUnitType().getName());
            singleProgramBuilder.add("id", program.getInstitution().getId());
            if (program.getInstitution().getInstitution().getExternalCode() == null)
                singleProgramBuilder.add("externalDbId", JsonValue.NULL);
            else
                singleProgramBuilder.add("externalDbId", program.getInstitution().getInstitution().getExternalCode());
            singleProgramBuilder.add("name", program.getInstitution().getInstitution().getLegalName());
            singleProgramBuilder.add("code", program.getInstitution().getInstitution().getCommonName());
            programsBuilder.add(singleProgramBuilder);
        }

        mainBuilder.add("programs", programsBuilder);

        JsonObject programsJson = mainBuilder.build();

       return programsJson.toString();

    }

    @Override
    public String getUnitMembers(int unitId) {

        JsonObjectBuilder mainBuilder = Json.createObjectBuilder();

        try {
            ContactModel c = contactRepository.findByIdAndDeletedIsFalse(unitId).orElse(null);
            if (c == null) {
                mainBuilder.add("message", "Unit not found");
                return mainBuilder.build().toString();
            }
            setOrganizationalUnitMembers(mainBuilder, c);
        } catch(Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return "Internal Server Error";
        }

        return mainBuilder.build().toString();

    }

    @Override
    public String getProgramMembers(int programId) {

        JsonObjectBuilder mainBuilder = Json.createObjectBuilder();

        try {
            ContactModel program = contactRepository.findByIdAndDeletedIsFalse(programId).orElse(null);
            if (program == null || program.getInstitution() == null
                || program.getInstitution().getUnitType() == null) {
                mainBuilder.add("message", "Program not found");
                return mainBuilder.build().toString();
            }
            setOrganizationalUnitMembers(mainBuilder, program);
        } catch(Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return "Internal Server Error";
        }

        return mainBuilder.build().toString();

    }

    private void setOrganizationalUnitMembers(JsonObjectBuilder mainBuilder, ContactModel organizationalUnit) {

            JsonArrayBuilder membersBuilder = Json.createArrayBuilder();

            for (HierarchyModel member : organizationalUnit.getMembers()) {
                ContactModel memberContact = member.getContact();
                if (memberContact == null || memberContact.getUsers() == null
                    || memberContact.getUsers().isEmpty())
                    continue;
                for (UserModel user : memberContact.getUsers()) {
                    JsonArrayBuilder memberRolesBuilder = Json.createArrayBuilder();
                    JsonObjectBuilder singleMemberBuilder = Json.createObjectBuilder();
                    singleMemberBuilder.add("userId", user.getId() + "");
                    singleMemberBuilder.add("contactId", memberContact.getId() + "");
                    PersonModel personInfo = memberContact.getPerson();
                    if (personInfo != null && personInfo.getFullName() != null)
                        singleMemberBuilder.add("name", personInfo.getFullName());
                    else if (personInfo != null
                        && personInfo.getGivenName() != null
                        && personInfo.getFamilyName() != null)
                            singleMemberBuilder.add("name", personInfo.getGivenName() + " " + personInfo.getFamilyName());
                    else
                        singleMemberBuilder.add("name", "");
                    singleMemberBuilder.add("username", user.getUserName());
                    for (RoleModel role : member.getRoles()) {
                        memberRolesBuilder.add(role.getName());
                    }
                    singleMemberBuilder.add("roles", memberRolesBuilder);
                    membersBuilder.add(singleMemberBuilder);
                }
            }

            /* for (HierarchyModel member : organizationalUnit.getMembers()) {
                ContactModel memberContact = member.getContact();
                if (memberContact == null)
                    continue;
                JsonArrayBuilder memberRolesBuilder = Json.createArrayBuilder();
                JsonObjectBuilder singleMemberBuilder = Json.createObjectBuilder();
                JsonArrayBuilder userAccountsBuilder = Json.createArrayBuilder();
                singleMemberBuilder.add("contactId", memberContact.getId() + "");
                PersonModel personInfo = memberContact.getPerson();
                if (personInfo != null && personInfo.getFullName() != null)
                singleMemberBuilder.add("name", personInfo.getFullName());
                else if (personInfo != null
                    && personInfo.getGivenName() != null
                    && personInfo.getFamilyName() != null)
                        singleMemberBuilder.add("name", personInfo.getGivenName() + " " + personInfo.getFamilyName());
                else
                    singleMemberBuilder.add("name", "");
                for (UserModel user : memberContact.getUsers()) {
                    JsonObjectBuilder userAccountBuilder = Json.createObjectBuilder();
                    userAccountBuilder.add("userId", user.getId());
                    userAccountBuilder.add("userName", user.getUserName());
                    userAccountBuilder.add("active", user.isActive());
                    userAccountsBuilder.add(userAccountBuilder);
                }
                singleMemberBuilder.add("accounts", userAccountsBuilder);
                for (RoleModel role : member.getRoles()) {
                    memberRolesBuilder.add(role.getName());
                }
                singleMemberBuilder.add("roles", memberRolesBuilder);
                membersBuilder.add(singleMemberBuilder);
            } */
            mainBuilder.add("members", membersBuilder);
    }

    @Override
    public String getProductMembers(int productId) {

        JsonObjectBuilder mainBuilder = Json.createObjectBuilder();

        ProductModel product = productRepository.findById(productId).orElse(null);

        if (product == null) {
            mainBuilder.add("error", "product not found");
        } else {
            JsonArrayBuilder membersArrayBuilder = Json.createArrayBuilder();
            Set<Integer> userIds = new HashSet<>();
            product.getProductfunctions().forEach(pf -> {
                pf.getRoles().forEach(role -> {
                    role.getContactHierarchies().forEach(ch -> {
                        ch.getContact().getUsers().forEach(user -> {
                            if (!userIds.contains(user.getId())) {
                                userIds.add(user.getId());
                                membersArrayBuilder.add(user.getId()+"");
                            }
                        });
                    });
                });
            });
            mainBuilder.add("members", membersArrayBuilder);
        }

        return mainBuilder.build().toString();
    }

    @Override
    public String buildUserProfile(int userId, String domainPrefixFilter, Integer programDbIdFilter) {

        try {
            UserModel user = userRepository.findByIdAndDeletedIsFalse(userId).orElse(null);
            if (user == null || user.getContact() == null)
                return null;

            ContactModel contact = user.getContact();

            JsonObjectBuilder mainBuilder = Json.createObjectBuilder();
            JsonObjectBuilder memberOBuilder = Json.createObjectBuilder();
            JsonArrayBuilder serviceProvidersBuilder = Json.createArrayBuilder();
            //JsonObjectBuilder subscriptionBuilder = Json.createObjectBuilder();
            JsonObjectBuilder permissionsBuilder = Json.createObjectBuilder();
            List<HierarchyModel> userOrganizationalUnits = contact.getParents();
            Set<RoleModel> userRoles = new HashSet<>();
            Set<ProductModel> userProducts = new HashSet<>();
            Set<DomainModel> userDomains = new HashSet<>();
            Set<ProductFunctionModel> userProductFunctions = new HashSet<>();
            Set<CropModel> userCrops = new HashSet<>();
            HashMap<String, Set<ProductFunctionModel>> userProductFunctionsByParent = new HashMap<>();

            setUserBasicInfo(mainBuilder, user);
            if (programDbIdFilter != null) {
                List<InstitutionModel> programWithId = institutionRepository.findByExternalCodeAndDeletedIsFalse(programDbIdFilter);
                if (programWithId.size() > 1)
                    return "More than one program found with external code " + programWithId;
                extractUserDataProgramFilter(user, user.isAdmin() ? true : false,userOrganizationalUnits, memberOBuilder, serviceProvidersBuilder, userDomains, 
                    userProducts, userProductFunctions, userProductFunctionsByParent, userRoles, userCrops, programDbIdFilter);
            } else {
                extractUserData(user, user.isAdmin() ? true : false, userOrganizationalUnits, memberOBuilder, serviceProvidersBuilder, userDomains, 
                    userProducts, userProductFunctions, userProductFunctionsByParent, userRoles, userCrops);
            }
            if (user.isAdmin()) {
                userCrops = cropRepository.findAll()
                    .stream().filter(c -> !c.isDeleted())
                    .collect(Collectors.toSet());
            }
            //setSubscriptions(subscriptionBuilder, userCrops);
            permissionsBuilder.add("memberOf", memberOBuilder);
            if (domainPrefixFilter != null) {
                if (user.isAdmin())
                    setAdminApplicationsDomainFiltered(permissionsBuilder, userProductFunctions, userProductFunctionsByParent, domainPrefixFilter, programDbIdFilter);
                else
                    setApplicationsDomainFiltered(permissionsBuilder, new ArrayList<>(userDomains), new ArrayList<>(userProducts), userProductFunctions, userProductFunctionsByParent, domainPrefixFilter, programDbIdFilter);
            } else {
                if (user.isAdmin())
                    setAdminApplications(permissionsBuilder, userProductFunctions, userProductFunctionsByParent);
                else
                    setApplications(permissionsBuilder, new ArrayList<>(userDomains), new ArrayList<>(userProducts), userProductFunctions, userProductFunctionsByParent);
            }

            buildUserTenantSubscriptionsJson(mainBuilder, user, null, userCrops);
            //mainBuilder.add("subscriptions", subscriptionBuilder);
            mainBuilder.add("service_providers", serviceProvidersBuilder);
            mainBuilder.add("permissions", permissionsBuilder);
            if (user.getPreference() != null) {
                JsonReader jsonReader = Json.createReader(new StringReader(user.getPreference().toString()));
                JsonObject o = jsonReader.readObject();
                mainBuilder.add("preference", o);
            } else {
                mainBuilder.add("preference", "{}");
            } 
            
            return mainBuilder.build().toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "Internal Error";
        }
    }

    private void setUserBasicInfo(JsonObjectBuilder mainBuilder, UserModel user) {
        
        mainBuilder.add("account", user.getUserName());

        mainBuilder.add("dbId", user.getId());

        Optional.ofNullable(user.getExternalId()).ifPresentOrElse(eid -> {
            mainBuilder.add("externalId", user.getExternalId());
        }, () -> {
            mainBuilder.add("externalId", "not available");
        });

        String fullName = user.getContact().getPerson() != null && user.getContact().getPerson().getFullName() != null
            ? user.getContact().getPerson().getFullName().trim()
            : "null";
        if (fullName.endsWith(","))
            fullName = fullName.substring(0, fullName.length() - 1);
        mainBuilder.add("fullName", fullName);
        mainBuilder.add("full_name", fullName);

    }

    private void extractUserData(UserModel user, boolean isAdmin, List<HierarchyModel> userOrganizationalUnits, JsonObjectBuilder memberOfBuilder, 
        JsonArrayBuilder serviceProvidersBuilder, Set<DomainModel> userDomains,
        Set<ProductModel> userProducts, Set<ProductFunctionModel> userProductFunctions,
        HashMap<String, Set<ProductFunctionModel>> userProductFunctionsByParent, Set<RoleModel> userRoles, Set<CropModel> userCrops) {
            
            JsonArrayBuilder organizationalUnits = Json.createArrayBuilder();
            JsonArrayBuilder programsBuilder = Json.createArrayBuilder();
            JsonArrayBuilder allRolesBuilder = Json.createArrayBuilder();
            Set<String> allRoles = new HashSet<>();
            Set<Integer> allParentIds = new HashSet<>();
            Set<Integer> serviceProviderIds = new HashSet<>();
            userOrganizationalUnits.removeIf(unit -> unit.isDeleted());
            if (userOrganizationalUnits.isEmpty() && user.getDefaultRole() != null) {
                user.getDefaultRole().getProductfunctions().stream().filter(f -> !f.isDeleted()).forEach(f -> {
                    userProductFunctions.add(f);
                    if (f.getProduct() != null) {
                        userProducts.add(f.getProduct());
                        if (f.getProduct().getDomain() != null)
                            userDomains.add(f.getProduct().getDomain());
                    }
                });
                userProductFunctionsByParent.put("General", userProductFunctions);
                allRoles.add(user.getDefaultRole().getName());
            }

            userOrganizationalUnits.forEach(parent -> {
                if (parent.getInstitution() != null && parent.getInstitution().getInstitution() != null
                    && parent.getInstitution().getInstitution().getUnitType() != null && !parent.isDeleted()) {
                    InstitutionModel organizationalUnit = parent.getInstitution().getInstitution();
                    JsonObjectBuilder organizationalUnitBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder programBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder serviceProviderBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder organizationalUnitCrops = Json.createArrayBuilder();
                    organizationalUnit.getCrops().removeIf(c -> c.getId() == 0);
                    organizationalUnit.getCrops().forEach(crop -> {
                        JsonObjectBuilder cropBuilder = Json.createObjectBuilder();
                        cropBuilder.add("id", crop.getId());
                        cropBuilder.add("name", crop.getName());
                        organizationalUnitCrops.add(cropBuilder);
                        userCrops.add(crop);
                    });
                    if (organizationalUnit.getUnitType() != null && 
                    organizationalUnit.getUnitType().getName().equals("Program")) {
                        organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                        organizationalUnitBuilder.add("externalCode", 
                            organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                        organizationalUnitBuilder.add("type", "Program");
                        organizationalUnitBuilder.add("code", organizationalUnit.getCommonName());
                        organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                        programBuilder.add("id", organizationalUnit.getContact().getId());
                        programBuilder.add("externalCode", 
                            organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                        programBuilder.add("code", organizationalUnit.getCommonName());
                        programBuilder.add("name", organizationalUnit.getLegalName());
                        programsBuilder.add(programBuilder);
                    } else if (organizationalUnit.getUnitType() != null && 
                        organizationalUnit.getUnitType().getName().equals("Service Unit")) {
                            organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                            organizationalUnitBuilder.add("code", 
                                organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                            organizationalUnitBuilder.add("type", "Service Unit");
                            organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                            if (organizationalUnit.getContact().getContactWorkflows()
                                .stream().filter(cw -> cw.getWorkflow().getId() == 583 && cw.isProvide()).findAny().isPresent()) {
                                    serviceProviderBuilder.add("id", organizationalUnit.getContact().getId());
                                    serviceProviderBuilder.add("name", organizationalUnit.getLegalName());
                                    serviceProviderBuilder.add("crops", organizationalUnitCrops);
                                    serviceProvidersBuilder.add(serviceProviderBuilder);
                                    serviceProviderIds.add(organizationalUnit.getContact().getId());
                            }
                    }  
                    allParentIds.add(parent.getInstitution().getId());
                    JsonArrayBuilder rolesBuilder = Json.createArrayBuilder();
                    Set<ProductFunctionModel> userProductFunctionsInParent = new HashSet<>();
                    parent.getRoles().forEach(role -> {
                        role.getProductfunctions().stream().filter(f -> !f.isDeleted() && !f.getProduct().isDeleted() && !f.getProduct().getDomain().isDeleted()).forEach(f -> {
                            userProductFunctions.add(f);
                            userProductFunctionsInParent.add(f);
                            if (f.getProduct() != null) {
                                userProducts.add(f.getProduct());
                                if (f.getProduct().getDomain() != null)
                                    userDomains.add(f.getProduct().getDomain());
                            }
                        });
                        userProductFunctionsByParent.put(organizationalUnit.getLegalName(), userProductFunctionsInParent);
                        rolesBuilder.add(role.getName());
                        allRoles.add(role.getName());
                    });
                    organizationalUnitBuilder.add("crops", organizationalUnitCrops);
                    organizationalUnitBuilder.add("roles", rolesBuilder);
                    organizationalUnits.add(organizationalUnitBuilder);
                }
            });
            List<ContactWorkflowModel> genotypingProviders = contactWorkflowRepository.findByWorkflowIdAndProvideIsTrue(583)
                .stream()
                .filter(r -> !r.isDeleted())
                .collect(Collectors.toList());
            if (genotypingProviders != null && !genotypingProviders.isEmpty()) {
                if (isAdmin || canRequestGenotyping(genotypingProviders, user.getId(), allRoles, allParentIds)) {
                    genotypingProviders.forEach(provider -> {
                        if (!serviceProviderIds.contains(provider.getContact().getId())) {
                            InstitutionModel i = provider.getContact().getInstitution();
                            if (i != null) {
                                JsonArrayBuilder providerCrops = Json.createArrayBuilder();
                                i.getCrops().forEach(c -> {
                                    JsonObjectBuilder cropBuilder = Json.createObjectBuilder();
                                    cropBuilder.add("id", c.getId());
                                    cropBuilder.add("name", c.getName());
                                    providerCrops.add(cropBuilder);
                                });
                                JsonObjectBuilder serviceProviderBuilder = Json.createObjectBuilder();
                                serviceProviderBuilder.add("id", provider.getContact().getId());
                                serviceProviderBuilder.add("name", provider.getContact().getInstitution().getLegalName());
                                serviceProviderBuilder.add("crops", providerCrops);
                                serviceProvidersBuilder.add(serviceProviderBuilder);
                                serviceProviderIds.add(provider.getContact().getId());
                            }
                        }
                    });
                }
            }
            if (isAdmin) {
                List<ContactModel> allUnits = 
                    contactRepository.findByCategoryNameAndDeletedIsFalseAndInstitutionUnitTypeNotNullAndInstitutionDeletedIsFalse("Internal Unit");
                allUnits.removeIf(u -> allParentIds.contains(u.getId()));
                allUnits.forEach(u -> {
                    InstitutionModel organizationalUnit = u.getInstitution();
                    JsonObjectBuilder organizationalUnitBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder programBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder organizationalUnitCrops = Json.createArrayBuilder();
                    organizationalUnit.getCrops().removeIf(c -> c.getId() == 0);
                    organizationalUnit.getCrops().forEach(crop -> {
                        JsonObjectBuilder cropBuilder = Json.createObjectBuilder();
                        cropBuilder.add("id", crop.getId());
                        cropBuilder.add("name", crop.getName());
                        organizationalUnitCrops.add(cropBuilder);
                        userCrops.add(crop);
                    });
                    if (u.getInstitution().getUnitType().getName().equals("Program")) {
                        organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                        organizationalUnitBuilder.add("externalCode", 
                            organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                        organizationalUnitBuilder.add("type", "Program");
                        organizationalUnitBuilder.add("code", organizationalUnit.getCommonName());
                        organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                        programBuilder.add("id", organizationalUnit.getContact().getId());
                        programBuilder.add("externalCode", 
                            organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                        programBuilder.add("code", organizationalUnit.getCommonName());
                        programBuilder.add("name", organizationalUnit.getLegalName());
                        programsBuilder.add(programBuilder);
                    } else if (u.getInstitution().getUnitType().getName().equals("Service Unit")) {
                        organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                        organizationalUnitBuilder.add("code", 
                            organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                        organizationalUnitBuilder.add("type", "Service Unit");
                        organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                    }
                    JsonArrayBuilder rolesBuilder = Json.createArrayBuilder();
                    rolesBuilder.add("Admin");
                    organizationalUnitBuilder.add("crops", organizationalUnitCrops);
                    organizationalUnitBuilder.add("roles", rolesBuilder);
                    organizationalUnits.add(organizationalUnitBuilder);
                });
                allRoles.add("Admin");
            }
            memberOfBuilder.add("units", organizationalUnits);
            memberOfBuilder.add("programs", programsBuilder);
            allRoles.forEach(r -> allRolesBuilder.add(r));
            memberOfBuilder.add("roles", allRolesBuilder);
    }

    private void extractUserDataProgramFilter(UserModel user, boolean isAdmin, List<HierarchyModel> userOrganizationalUnits, JsonObjectBuilder memberOfBuilder, 
        JsonArrayBuilder serviceProvidersBuilder, Set<DomainModel> userDomains,
        Set<ProductModel> userProducts, Set<ProductFunctionModel> userProductFunctions,
        HashMap<String, Set<ProductFunctionModel>> userProductFunctionsByParent, Set<RoleModel> userRoles,
        Set<CropModel> userCrops, Integer programDbId) {
            
            JsonArrayBuilder organizationalUnits = Json.createArrayBuilder();
            JsonArrayBuilder programsBuilder = Json.createArrayBuilder();
            JsonArrayBuilder allRolesBuilder = Json.createArrayBuilder();
            Set<String> allRoles = new HashSet<>();
            Set<Integer> allParentIds = new HashSet<>();
            Set<Integer> serviceProviderIds = new HashSet<>();

            userOrganizationalUnits.removeIf(u -> u.getInstitution().getInstitution().getExternalCode() == null 
                || u.getInstitution().getInstitution().getExternalCode().intValue() != programDbId.intValue()
                || u.isDeleted());
            
            userOrganizationalUnits.forEach(parent -> {
                if (parent.getInstitution() != null && parent.getInstitution().getInstitution() != null
                    && parent.getInstitution().getInstitution().getUnitType() != null && !parent.isDeleted() 
                    && !parent.getInstitution().isDeleted()
                    && !parent.getInstitution().getInstitution().isDeleted()) {
                    InstitutionModel organizationalUnit = parent.getInstitution().getInstitution();
                    JsonObjectBuilder organizationalUnitBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder programBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder serviceProviderBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder organizationalUnitCrops = Json.createArrayBuilder();
                    organizationalUnit.getCrops().removeIf(c -> c.getId() == 0);
                    organizationalUnit.getCrops().forEach(crop -> {
                        JsonObjectBuilder cropBuilder = Json.createObjectBuilder();
                        cropBuilder.add("id", crop.getId());
                        cropBuilder.add("name", crop.getName());
                        organizationalUnitCrops.add(cropBuilder);
                        userCrops.add(crop);
                    });
                    if (organizationalUnit.getUnitType() != null && 
                        organizationalUnit.getUnitType().getName().equals("Program")) {
                        organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                        organizationalUnitBuilder.add("externalCode", 
                            organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                        organizationalUnitBuilder.add("type", "Program");
                        organizationalUnitBuilder.add("code", organizationalUnit.getCommonName());
                        organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                        programBuilder.add("id", organizationalUnit.getContact().getId());
                        programBuilder.add("externalCode", 
                            organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                        programBuilder.add("code", organizationalUnit.getCommonName());
                        programBuilder.add("name", organizationalUnit.getLegalName());
                        programsBuilder.add(programBuilder);
                    } else if (organizationalUnit.getUnitType() != null && 
                        organizationalUnit.getUnitType().getName().equals("Service Unit")) {
                            organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                            organizationalUnitBuilder.add("code", 
                                organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                            organizationalUnitBuilder.add("type", "Service Unit");
                            organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                            if (organizationalUnit.getContact().getContactWorkflows()
                                .stream().filter(cw -> cw.getWorkflow().getId() == 583 && cw.isProvide()).findAny().isPresent()) {
                                    serviceProviderBuilder.add("id", organizationalUnit.getContact().getId());
                                    serviceProviderBuilder.add("name", organizationalUnit.getLegalName());
                                    serviceProviderBuilder.add("crops", organizationalUnitCrops);
                                    serviceProvidersBuilder.add(serviceProviderBuilder);
                                    serviceProviderIds.add(organizationalUnit.getContact().getId());
                            }
                    }  
                    allParentIds.add(parent.getInstitution().getId());  
                    JsonArrayBuilder rolesBuilder = Json.createArrayBuilder();
                    Set<ProductFunctionModel> userProductFunctionsInParent = new HashSet<>();
                    parent.getRoles().forEach(role -> {
                        role.getProductfunctions().stream().filter(f -> !f.isDeleted() && !f.getProduct().isDeleted() && !f.getProduct().getDomain().isDeleted()).forEach(f -> {
                            userProductFunctions.add(f);
                            userProductFunctionsInParent.add(f);
                            if (f.getProduct() != null) {
                                userProducts.add(f.getProduct());
                                if (f.getProduct().getDomain() != null)
                                    userDomains.add(f.getProduct().getDomain());
                            }
                        });
                        userProductFunctionsByParent.put(organizationalUnit.getLegalName(), userProductFunctionsInParent);
                        rolesBuilder.add(role.getName());
                        allRoles.add(role.getName());
                    });
                    organizationalUnitBuilder.add("crops", organizationalUnitCrops);
                    organizationalUnitBuilder.add("roles", rolesBuilder);
                    organizationalUnits.add(organizationalUnitBuilder);
                }
            });
            List<ContactWorkflowModel> genotypingProviders = contactWorkflowRepository.findByWorkflowIdAndProvideIsTrue(583)
                .stream()
                .filter(r -> !r.isDeleted())
                .collect(Collectors.toList());
            if (genotypingProviders != null && !genotypingProviders.isEmpty()) {
                if (isAdmin || canRequestGenotyping(genotypingProviders, user.getId(), allRoles, allParentIds)) {
                    genotypingProviders.forEach(provider -> {
                        if (!serviceProviderIds.contains(provider.getContact().getId())) {
                            InstitutionModel i = provider.getContact().getInstitution();
                            if (i != null) {
                                JsonArrayBuilder providerCrops = Json.createArrayBuilder();
                                i.getCrops().forEach(c -> {
                                    JsonObjectBuilder cropBuilder = Json.createObjectBuilder();
                                    cropBuilder.add("id", c.getId());
                                    cropBuilder.add("name", c.getName());
                                    providerCrops.add(cropBuilder);
                                });
                                JsonObjectBuilder serviceProviderBuilder = Json.createObjectBuilder();
                                serviceProviderBuilder.add("id", provider.getContact().getId());
                                serviceProviderBuilder.add("name", provider.getContact().getInstitution().getLegalName());
                                serviceProviderBuilder.add("crops", providerCrops);
                                serviceProvidersBuilder.add(serviceProviderBuilder);
                                serviceProviderIds.add(provider.getContact().getId());
                            }
                        }
                    });
                }
            }
            if (isAdmin) {
                List<ContactModel> allUnits = 
                    contactRepository.findByCategoryNameAndInstitutionExternalCodeAndDeletedIsFalseAndInstitutionUnitTypeNotNullAndInstitutionDeletedIsFalse("Internal Unit", programDbId);
                    allUnits.removeIf(u -> allParentIds.contains(u.getInstitution().getId()));
                    allUnits.forEach(u -> {
                        InstitutionModel organizationalUnit = u.getInstitution();
                        JsonObjectBuilder organizationalUnitBuilder = Json.createObjectBuilder();
                        JsonObjectBuilder programBuilder = Json.createObjectBuilder();
                        JsonArrayBuilder organizationalUnitCrops = Json.createArrayBuilder();
                        organizationalUnit.getCrops().removeIf(c -> c.getId() == 0);
                        organizationalUnit.getCrops().forEach(crop -> {
                            JsonObjectBuilder cropBuilder = Json.createObjectBuilder();
                            cropBuilder.add("id", crop.getId());
                            cropBuilder.add("name", crop.getName());
                            organizationalUnitCrops.add(cropBuilder);
                            userCrops.add(crop);
                        });
                        if (u.getInstitution().getUnitType().getName().equals("Program")) {
                            organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                            organizationalUnitBuilder.add("externalCode", 
                                organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                            organizationalUnitBuilder.add("type", "Program");
                            organizationalUnitBuilder.add("code", organizationalUnit.getCommonName());
                            organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                            programBuilder.add("id", organizationalUnit.getContact().getId());
                            programBuilder.add("externalCode", 
                                organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                            programBuilder.add("code", organizationalUnit.getCommonName());
                            programBuilder.add("name", organizationalUnit.getLegalName());
                            programsBuilder.add(programBuilder);
                        } else if (u.getInstitution().getUnitType().getName().equals("Service Unit")) {
                            organizationalUnitBuilder.add("id", organizationalUnit.getContact().getId());
                            organizationalUnitBuilder.add("code", 
                                organizationalUnit.getExternalCode() != null ? Integer.toString(organizationalUnit.getExternalCode()) : "");
                            organizationalUnitBuilder.add("type", "Service Unit");
                            organizationalUnitBuilder.add("name", organizationalUnit.getLegalName());
                        }
                        JsonArrayBuilder rolesBuilder = Json.createArrayBuilder();
                        rolesBuilder.add("Admin");
                        organizationalUnitBuilder.add("crops", organizationalUnitCrops);
                        organizationalUnitBuilder.add("roles", rolesBuilder);
                        organizationalUnits.add(organizationalUnitBuilder);
                    });
                allRoles.add("Admin");
            }
            memberOfBuilder.add("units", organizationalUnits);
            memberOfBuilder.add("programs", programsBuilder);
            allRoles.forEach(r -> allRolesBuilder.add(r));
            memberOfBuilder.add("roles", allRolesBuilder);
    }

    private void setSubscriptions(JsonObjectBuilder subscriptionBuilder, Set<CropModel> userCrops) {

        JsonArrayBuilder cropsBuilder = Json.createArrayBuilder();
        userCrops = userCrops.stream().sorted(new Comparator<CropModel>() {
            @Override
            public int compare(CropModel o1, CropModel o2) {
                return Integer.compare(o1.getId(), o2.getId());
            }
        }).collect(Collectors.toSet());

        userCrops.removeIf(crop -> crop.getId() == 0);

        userCrops.forEach(c -> {
            JsonObjectBuilder cropBuilder = Json.createObjectBuilder();
            cropBuilder.add("id", c.getId());
            cropBuilder.add("name", c.getName());
            cropsBuilder.add(cropBuilder);
        });

        subscriptionBuilder.add("crops", cropsBuilder);
    }

    private void setApplications(JsonObjectBuilder permissionsBuilder, List<DomainModel> userDomains,
        List<ProductModel> userProducts, Set<ProductFunctionModel> userProductFunctions,
        HashMap<String, Set<ProductFunctionModel>> userProductFunctionsByParent) {
            JsonArrayBuilder domainsBuilder = Json.createArrayBuilder();
            userDomains.sort(new Comparator<DomainModel>() {
                @Override
                public int compare(DomainModel o1, DomainModel o2) {
                    return Integer.compare(o1.getId(), o2.getId());
                }
            });
            userDomains.forEach(domain -> {
                JsonObjectBuilder domainBuilder = Json.createObjectBuilder();
                JsonArrayBuilder productsBuilder = Json.createArrayBuilder();
                boolean showDomain = false;
                domain.getProducts().removeIf(p -> !userProducts.contains(p) || p.isDeleted());
                domain.getProducts().sort(new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return Integer.compare(o1.getMenuOrder(), o2.getMenuOrder());
                    }
                });
                for (ProductModel product : domain.getProducts()) {
                    JsonObjectBuilder productBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder actionsBuilder = Json.createArrayBuilder();
                    JsonArrayBuilder dataActionsBuilder = Json.createArrayBuilder();
                    JsonObjectBuilder unitActionBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder unitDataActionBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder unitActions = Json.createArrayBuilder();
                    JsonArrayBuilder unitDataActions = Json.createArrayBuilder();
                    boolean hasActions = false;
                    boolean hasDataActions = false;
                    boolean showMenu = false;
                    for (Map.Entry<String,Set<ProductFunctionModel>> element : userProductFunctionsByParent.entrySet()) {
                        String unitName = element.getKey();
                        Set<ProductFunctionModel> unitProductFunctions = element.getValue()
                            .stream()
                            .filter(f -> f.getProduct().equals(product)).collect(Collectors.toSet());
                        if (!unitProductFunctions.isEmpty()) {
                            unitActionBuilder = Json.createObjectBuilder();
                            unitDataActionBuilder = Json.createObjectBuilder();
                            unitActions = Json.createArrayBuilder();
                            unitDataActions = Json.createArrayBuilder();
                            boolean hasUnitActions = false;
                            boolean hasUnitDataActions = false;
                            for (ProductFunctionModel f : unitProductFunctions) {
                                if (f.getAction().equals("SHOW_MENU"))
                                    showMenu = true;
                                if (!f.isDataAction()) {
                                    unitActions.add(f.getAction());
                                    hasUnitActions = true;
                                    hasActions = true;
                                } else { 
                                    unitDataActions.add(f.getAction());
                                    hasUnitDataActions = true;
                                    hasDataActions = true;
                                }
                            }
                            if (hasUnitActions) {
                                unitActionBuilder.add(unitName, unitActions);
                                actionsBuilder.add(unitActionBuilder);
                                
                            }
                            if (hasUnitDataActions) {
                                unitDataActionBuilder.add(unitName, unitDataActions);
                                dataActionsBuilder.add(unitDataActionBuilder);
                                
                            }
                        }
                    }
                    if (product.getDomain().getName().equals("Core Breeding"))
                        showMenu = true;
                    if (showMenu)
                        showDomain = true;
                    productBuilder.add("name", product.getName());
                    productBuilder.add("abbrev", product.getAbbreviation() != null ? product.getAbbreviation() : "");
                    productBuilder.add("id", product.getId());
                    productBuilder.add("menu_order", product.getMenuOrder());
                    String path = product.getPath();
                    if (product.getIsRedirect() && !product.getIsExternal()
                        && product.getDomain().getDomaininstances().get(0) != null
                        && product.getDomain().getDomaininstances().get(0).getContext() != null) {
                        path = product.getDomain()
                            .getDomaininstances().get(0).getContext() + product.getPath();
                    }
                    productBuilder.add("path", path);
                    productBuilder.add("description", product.getDescription());
                    productBuilder.add("hasWorkflow", product.getHasWorkflow());
                    productBuilder.add("showMenu", showMenu);
                    if (hasActions)
                        productBuilder.add("actions", actionsBuilder);
                    if (hasDataActions)
                        productBuilder.add("dataActions", dataActionsBuilder);
                    productsBuilder.add(productBuilder);   
                }
                domainBuilder.add("domain", domain.getName());
                domainBuilder.add("id", domain.getId());
                domainBuilder.add("prefix", domain.getPrefix());
                domainBuilder.add("showMenu", showDomain);
                domainBuilder.add("products", productsBuilder);
                domainsBuilder.add(domainBuilder);
            });
            permissionsBuilder.add("applications", domainsBuilder);
    }

    private void setApplicationsDomainFiltered(JsonObjectBuilder permissionsBuilder, List<DomainModel> userDomains,
        List<ProductModel> userProducts, Set<ProductFunctionModel> userProductFunctions,
        HashMap<String, Set<ProductFunctionModel>> userProductFunctionsByParent, String domainPrefixFilter, Integer programDbId) {

            JsonArrayBuilder domainsBuilder = Json.createArrayBuilder();
            userDomains.sort(new Comparator<DomainModel>() {
                @Override
                public int compare(DomainModel o1, DomainModel o2) {
                    return Integer.compare(o1.getId(), o2.getId());
                }
            });
            userDomains.removeIf(d -> !d.getPrefix().equals(domainPrefixFilter));
            userDomains.forEach(domain -> {
                JsonObjectBuilder domainBuilder = Json.createObjectBuilder();
                JsonArrayBuilder productsBuilder = Json.createArrayBuilder();
                boolean showDomain = false;
                domain.getProducts().removeIf(p -> !userProducts.contains(p));
                domain.getProducts().sort(new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return Integer.compare(o1.getMenuOrder(), o2.getMenuOrder());
                    }
                });
                for (ProductModel product : domain.getProducts()) {
                    JsonObjectBuilder productBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder actionsBuilder = Json.createArrayBuilder();
                    JsonArrayBuilder dataActionsBuilder = Json.createArrayBuilder();
                    JsonObjectBuilder unitActionBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder unitDataActionBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder unitActions = Json.createArrayBuilder();
                    JsonArrayBuilder unitDataActions = Json.createArrayBuilder();
                    boolean hasActions = false;
                    boolean hasDataActions = false;
                    boolean showMenu = false;
                    for (Map.Entry<String,Set<ProductFunctionModel>> element : userProductFunctionsByParent.entrySet()) {
                        String unitName = element.getKey();
                        Set<ProductFunctionModel> unitProductFunctions = element.getValue()
                            .stream()
                            .filter(f -> f.getProduct().equals(product)).collect(Collectors.toSet());
                        if (!unitProductFunctions.isEmpty()) {
                            unitActionBuilder = Json.createObjectBuilder();
                            unitDataActionBuilder = Json.createObjectBuilder();
                            unitActions = Json.createArrayBuilder();
                            unitDataActions = Json.createArrayBuilder();
                            boolean hasUnitActions = false;
                            boolean hasUnitDataActions = false;
                            for (ProductFunctionModel f : unitProductFunctions) {
                                if (f.getAction().equals("SHOW_MENU"))
                                    showMenu = true;
                                if (!f.isDataAction()) {
                                    unitActions.add(f.getAction());
                                    hasUnitActions = true;
                                    hasActions = true;
                                } else { 
                                    unitDataActions.add(f.getAction());
                                    hasUnitDataActions = true;
                                    hasDataActions = true;
                                }
                            }
                            if (hasUnitActions) {
                                unitActionBuilder.add(unitName, unitActions);
                                actionsBuilder.add(unitActionBuilder);
                                
                            }
                            if (hasUnitDataActions) {
                                unitDataActionBuilder.add(unitName, unitDataActions);
                                dataActionsBuilder.add(unitDataActionBuilder);
                                
                            }
                        }
                    }
                    if (product.getDomain().getName().equals("Core Breeding"))
                        showMenu = true;
                    if (showMenu)
                        showDomain = true;
                    productBuilder.add("name", product.getName());
                    productBuilder.add("abbrev", product.getAbbreviation() != null ? product.getAbbreviation() : "");
                    productBuilder.add("id", product.getId());
                    if (product.getExternalId() != null)
                        productBuilder.add("externalApplicationDbId", product.getExternalId());
                    else
                        productBuilder.add("externalApplicationDbId", JsonValue.NULL);
                    productBuilder.add("menu_order", product.getMenuOrder());
                    String path = product.getPath();
                    if (product.getIsRedirect() && !product.getIsExternal()
                        && product.getDomain().getDomaininstances().get(0) != null
                        && product.getDomain().getDomaininstances().get(0).getContext() != null) {
                        path = product.getDomain()
                            .getDomaininstances().get(0).getContext() + product.getPath();
                    }
                    productBuilder.add("path", path);
                    productBuilder.add("description", product.getDescription());
                    productBuilder.add("hasWorkflow", product.getHasWorkflow());
                    productBuilder.add("showMenu", showMenu);
                    if (hasActions)
                        productBuilder.add("actions", actionsBuilder);
                    if (hasDataActions)
                        productBuilder.add("dataActions", dataActionsBuilder);
                    productsBuilder.add(productBuilder);   
                }
                domainBuilder.add("domain", domain.getName());
                domainBuilder.add("id", domain.getId());
                if (programDbId != null)
                    domainBuilder.add("externalProgramDbId", programDbId);
                domainBuilder.add("prefix", domain.getPrefix());
                domainBuilder.add("showMenu", showDomain);
                domainBuilder.add("products", productsBuilder);
                domainsBuilder.add(domainBuilder);
            });
            permissionsBuilder.add("applications", domainsBuilder);
    }

    private void setAdminApplications(JsonObjectBuilder permissionsBuilder, Set<ProductFunctionModel> userProductFunctions,
        HashMap<String, Set<ProductFunctionModel>> userProductFunctionsByParent) {
            List<DomainModel> userDomains = domainRepository.findAll().stream().filter(d -> !d.isDeleted()).collect(Collectors.toList());
            JsonArrayBuilder domainsBuilder = Json.createArrayBuilder();
            userDomains.sort(new Comparator<DomainModel>() {
                @Override
                public int compare(DomainModel o1, DomainModel o2) {
                    return Integer.compare(o1.getId(), o2.getId());
                }
            });
            userDomains.forEach(domain -> {
                JsonObjectBuilder domainBuilder = Json.createObjectBuilder();
                JsonArrayBuilder productsBuilder = Json.createArrayBuilder();
                domain.getProducts().removeIf(p -> p.isDeleted());
                boolean showDomain = false;
                domain.getProducts().sort(new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return Integer.compare(o1.getMenuOrder(), o2.getMenuOrder());
                    }
                });
                for (ProductModel product : domain.getProducts()) {
                    JsonObjectBuilder productBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder actionsBuilder = Json.createArrayBuilder();
                    JsonArrayBuilder dataActionsBuilder = Json.createArrayBuilder();
                    Set<ProductFunctionModel> allProductFunctions = Set.copyOf(product.getProductfunctions());
                    JsonObjectBuilder unitActionBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder unitDataActionBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder unitActions = Json.createArrayBuilder();
                    JsonArrayBuilder unitDataActions = Json.createArrayBuilder();

                    for (Map.Entry<String,Set<ProductFunctionModel>> element : userProductFunctionsByParent.entrySet()) {
                        String unitName = element.getKey();
                        Set<ProductFunctionModel> unitProductFunctions = element.getValue()
                            .stream()
                            .filter(f -> f.getProduct().equals(product)).collect(Collectors.toSet());
                        if (!unitProductFunctions.isEmpty()) {
                            unitActionBuilder = Json.createObjectBuilder();
                            unitDataActionBuilder = Json.createObjectBuilder();
                            unitActions = Json.createArrayBuilder();
                            unitDataActions = Json.createArrayBuilder();
                            for (ProductFunctionModel f : unitProductFunctions) {
                                if (!f.isDataAction())
                                    unitActions.add(f.getAction());
                                else 
                                    unitDataActions.add(f.getAction());
                            }
                            unitActionBuilder.add(unitName, unitActions);
                            unitDataActionBuilder.add(unitName, unitDataActions);
                            actionsBuilder.add(unitActionBuilder);
                            dataActionsBuilder.add(unitDataActionBuilder);
                        }
                    }
                    unitActionBuilder = Json.createObjectBuilder();
                    unitDataActionBuilder = Json.createObjectBuilder();
                    unitActions = Json.createArrayBuilder();
                    unitDataActions = Json.createArrayBuilder();
                    boolean hasActions = false;
                    boolean hasDataActions = false;
                    boolean showMenu = false;
                    for (ProductFunctionModel f : allProductFunctions) {
                        if (f.getAction().equals("SHOW_MENU"))
                            showMenu = true;
                        if (!f.isDataAction()) {
                            unitActions.add(f.getAction());
                            hasActions = true;
                        } else { 
                            unitDataActions.add(f.getAction());
                            hasDataActions = true;
                        }
                    }
                    if (hasActions || hasDataActions) {
                        if (product.getDomain().getName().equals("Core Breeding"))
                            showMenu = true;
                        if (showMenu)
                            showDomain = true;
                        productBuilder.add("name", product.getName());
                        productBuilder.add("abbrev", product.getAbbreviation() != null ? product.getAbbreviation() : "");
                        productBuilder.add("id", product.getId());
                        productBuilder.add("menu_order", product.getMenuOrder());
                        String path = product.getPath();
                        if (product.getIsRedirect() && !product.getIsExternal()
                            && product.getDomain().getDomaininstances().get(0) != null
                            && product.getDomain().getDomaininstances().get(0).getContext() != null) {
                            path = product.getDomain()
                                .getDomaininstances().get(0).getContext() + product.getPath();
                        }
                        productBuilder.add("path", path);
                        productBuilder.add("description", product.getDescription());
                        productBuilder.add("hasWorkflow", product.getHasWorkflow());
                        productBuilder.add("showMenu", showMenu);
                        if (hasActions) {
                            unitActionBuilder.add("General", unitActions);
                            actionsBuilder.add(unitActionBuilder);
                            productBuilder.add("actions", actionsBuilder);
                        }
                        if (hasDataActions) {
                            unitDataActionBuilder.add("General", unitDataActions);
                            dataActionsBuilder.add(unitDataActionBuilder);
                            productBuilder.add("dataActions", dataActionsBuilder);
                        } 
                        productsBuilder.add(productBuilder);
                    }
                }
                domainBuilder.add("domain", domain.getName());
                domainBuilder.add("id", domain.getId());
                domainBuilder.add("prefix", domain.getPrefix());
                domainBuilder.add("showMenu", showDomain);
                domainBuilder.add("products", productsBuilder);
                domainsBuilder.add(domainBuilder);
            });
            permissionsBuilder.add("applications", domainsBuilder);
    }

    private void setAdminApplicationsDomainFiltered(JsonObjectBuilder permissionsBuilder, Set<ProductFunctionModel> userProductFunctions,
        HashMap<String, Set<ProductFunctionModel>> userProductFunctionsByParent, String domainPrefixFilter, Integer programDbId) {
            List<DomainModel> userDomains = domainRepository.findAll().stream().filter(d -> !d.isDeleted()).collect(Collectors.toList());
            JsonArrayBuilder domainsBuilder = Json.createArrayBuilder();
            userDomains.sort(new Comparator<DomainModel>() {
                @Override
                public int compare(DomainModel o1, DomainModel o2) {
                    return Integer.compare(o1.getId(), o2.getId());
                }
            });
            userDomains.removeIf(d -> !d.getPrefix().equals(domainPrefixFilter));
            userDomains.forEach(domain -> {
                JsonObjectBuilder domainBuilder = Json.createObjectBuilder();
                JsonArrayBuilder productsBuilder = Json.createArrayBuilder();
                boolean showDomain = false;
                domain.getProducts().removeIf(p -> p.isDeleted());
                domain.getProducts().sort(new Comparator<ProductModel>() {
                    @Override
                    public int compare(ProductModel o1, ProductModel o2) {
                        return Integer.compare(o1.getMenuOrder(), o2.getMenuOrder());
                    }
                });
                for (ProductModel product : domain.getProducts()) {
                    JsonObjectBuilder productBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder actionsBuilder = Json.createArrayBuilder();
                    JsonArrayBuilder dataActionsBuilder = Json.createArrayBuilder();
                    Set<ProductFunctionModel> allProductFunctions = Set.copyOf(product.getProductfunctions());
                    JsonObjectBuilder unitActionBuilder = Json.createObjectBuilder();
                    JsonObjectBuilder unitDataActionBuilder = Json.createObjectBuilder();
                    JsonArrayBuilder unitActions = Json.createArrayBuilder();
                    JsonArrayBuilder unitDataActions = Json.createArrayBuilder();

                    for (Map.Entry<String,Set<ProductFunctionModel>> element : userProductFunctionsByParent.entrySet()) {
                        String unitName = element.getKey();
                        Set<ProductFunctionModel> unitProductFunctions = element.getValue()
                            .stream()
                            .filter(f -> f.getProduct().equals(product)).collect(Collectors.toSet());
                        if (!unitProductFunctions.isEmpty()) {
                            unitActionBuilder = Json.createObjectBuilder();
                            unitDataActionBuilder = Json.createObjectBuilder();
                            unitActions = Json.createArrayBuilder();
                            unitDataActions = Json.createArrayBuilder();
                            for (ProductFunctionModel f : unitProductFunctions) {
                                if (!f.isDataAction())
                                    unitActions.add(f.getAction());
                                else 
                                    unitDataActions.add(f.getAction());
                            }
                            unitActionBuilder.add(unitName, unitActions);
                            unitDataActionBuilder.add(unitName, unitDataActions);
                            actionsBuilder.add(unitActionBuilder);
                            dataActionsBuilder.add(unitDataActionBuilder);
                        }
                    }
                    unitActionBuilder = Json.createObjectBuilder();
                    unitDataActionBuilder = Json.createObjectBuilder();
                    unitActions = Json.createArrayBuilder();
                    unitDataActions = Json.createArrayBuilder();
                    boolean hasActions = false;
                    boolean hasDataActions = false;
                    boolean showMenu = false;
                    for (ProductFunctionModel f : allProductFunctions) {
                        if (f.getAction().equals("SHOW_MENU"))
                            showMenu = true;
                        if (!f.isDataAction()) {
                            unitActions.add(f.getAction());
                            hasActions = true;
                        } else { 
                            unitDataActions.add(f.getAction());
                            hasDataActions = true;
                        }
                    }
                    if (hasActions || hasDataActions) {
                        if (product.getDomain().getName().equals("Core Breeding"))
                            showMenu = true;
                        if (showMenu)
                            showDomain = true;
                        productBuilder.add("name", product.getName());
                        productBuilder.add("abbrev", product.getAbbreviation() != null ? product.getAbbreviation() : "");
                        productBuilder.add("id", product.getId());
                        if (product.getExternalId() != null)
                            productBuilder.add("externalApplicationDbId", product.getExternalId());
                        else
                            productBuilder.add("externalApplicationDbId", JsonValue.NULL);
                        productBuilder.add("menu_order", product.getMenuOrder());
                        String path = product.getPath();
                        if (product.getIsRedirect() && !product.getIsExternal()
                            && product.getDomain().getDomaininstances().get(0) != null
                            && product.getDomain().getDomaininstances().get(0).getContext() != null) {
                            path = product.getDomain()
                                .getDomaininstances().get(0).getContext() + product.getPath();
                        }
                    productBuilder.add("path", path);
                    productBuilder.add("description", product.getDescription());
                    productBuilder.add("hasWorkflow", product.getHasWorkflow());
                    productBuilder.add("showMenu", showMenu);
                        if (hasActions) {
                            unitActionBuilder.add("General", unitActions);
                            actionsBuilder.add(unitActionBuilder);
                            productBuilder.add("actions", actionsBuilder);
                        }
                        if (hasDataActions) {
                            unitDataActionBuilder.add("General", unitDataActions);
                            dataActionsBuilder.add(unitDataActionBuilder);
                            productBuilder.add("dataActions", dataActionsBuilder);
                        } 
                        productsBuilder.add(productBuilder);
                    }
                }
                domainBuilder.add("domain", domain.getName());
                domainBuilder.add("id", domain.getId());
                if (programDbId != null)
                    domainBuilder.add("externalProgramDbId", programDbId);
                domainBuilder.add("prefix", domain.getPrefix());
                domainBuilder.add("showMenu", showDomain);
                domainBuilder.add("products", productsBuilder);
                domainsBuilder.add(domainBuilder);
            });
            permissionsBuilder.add("applications", domainsBuilder);
    }

}