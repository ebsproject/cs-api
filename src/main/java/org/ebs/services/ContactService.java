package org.ebs.services;

import java.util.List;
import java.util.Set;

import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactWorkflowsTo;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.ContactInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ContactService {

    Contact findByUser(int userId);

    Contact findById(int contactId);

    Page<Contact> findContacts(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    Contact createContact(ContactInput contactInput);

    Contact modifyContact(ContactInput contactInput);

    int deleteContact(int contactId);

    int removeAddress(int addressId, int contactId);

    int removeContactInfo(int contactInfoId);

    List<Contact> findContactsByInstitution(int institutionId);

    int countByEmail(String email);

    Set<JobLogTo> findJobLogs(int contactId);

    Set<UserTo> findUsers(int contactId);

    Set<PurposeTo> findPurposes(int contactId);

    List<ContactWorkflowsTo> findWorkflows(int contactId);

    boolean restoreContact(int contactId);

    List<HierarchyTo> findParents(int contactId);

    List<HierarchyTo> findMembers(int contactId);

    UserTo findCreatedBy(int contactId);

    UserTo findUpdatedBy(int contactId);

}