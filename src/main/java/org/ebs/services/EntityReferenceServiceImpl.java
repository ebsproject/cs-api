package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.ebs.model.EntityReferenceModel;
import org.ebs.model.repos.AttributesRepository;
import org.ebs.model.repos.EntityReferenceRepository;
import org.ebs.services.to.AttributesTo;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.EntityReferenceTo;
import org.ebs.services.to.NumberSequenceRuleTo;
import org.ebs.services.to.SegmentTo;
import org.ebs.services.to.SelectReference;
import org.ebs.services.to.Input.EntityReferenceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class EntityReferenceServiceImpl implements EntityReferenceService {

    private final EntityReferenceRepository entityreferenceRepository;
    private final ConversionService converter;
    private final AttributesRepository attributesRepository;

    @PersistenceContext
    private EntityManager manager;

    /**
     *
     * @param EntityReference
     */
    @Override
    @Transactional(readOnly = false)
    public EntityReferenceTo createEntityReference(EntityReferenceInput EntityReference) {
        EntityReferenceModel model = converter.convert(EntityReference, EntityReferenceModel.class);
        model.setId(0);

        model = entityreferenceRepository.save(model);
        return converter.convert(model, EntityReferenceTo.class);
    }

    /**
     *
     * @param entityReferenceId
     */
    @Override
    @Transactional(readOnly = false)
    public int deleteEntityReference(int entityReferenceId) {
        EntityReferenceModel entityreference = entityreferenceRepository.findById(entityReferenceId)
                .orElseThrow(() -> new RuntimeException("EntityReference not found"));
        entityreference.setDeleted(true);
        entityreferenceRepository.save(entityreference);
        return entityReferenceId;
    }

    /**
     *
     * @param entityreferenceId
     */
    public Set<AttributesTo> findAttributess(int entityreferenceId) {
        return attributesRepository.findByEntityreferenceId(entityreferenceId).stream()
                .map(e -> converter.convert(e, AttributesTo.class)).collect(Collectors.toSet());
    }

    /**
     *
     * @param entityreferenceId
     */
    public Set<DomainTo> findDomains(int entityreferenceId) {
        return entityreferenceRepository.findById(entityreferenceId).get().getDomains().stream()
                .map(e -> converter.convert(e, DomainTo.class)).collect(Collectors.toSet());
    }

    /**
     *
     * @param entityreferenceId
     */
    public Set<EmailTemplateTo> findEmailTemplates(int entityreferenceId) {
        return entityreferenceRepository.findById(entityreferenceId).get().getEmailtemplates()
                .stream().map(e -> converter.convert(e, EmailTemplateTo.class))
                .collect(Collectors.toSet());
    }

    /**
     *
     * @param entityReferenceId
     */
    @Override
    public Optional<EntityReferenceTo> findEntityReference(int entityReferenceId) {
        if (entityReferenceId < 1) {
            return Optional.empty();
        }
        return entityreferenceRepository.findById(entityReferenceId).filter(r -> !r.isDeleted())
                .map(r -> converter.convert(r, EntityReferenceTo.class));
    }

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    @Override
    public Page<EntityReferenceTo> findEntityReferences(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return entityreferenceRepository
                .findByCriteria(EntityReferenceModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, EntityReferenceTo.class));
    }

    /**
     *
     * @param entityreferenceId
     */
    public Set<NumberSequenceRuleTo> findNumberSequenceRules(int entityreferenceId) {
        return entityreferenceRepository.findById(entityreferenceId).get().getNumbersequencerules()
                .stream().map(e -> converter.convert(e, NumberSequenceRuleTo.class))
                .collect(Collectors.toSet());
    }


    /**
     *
     * @param entityReference
     */
    @Override
    @Transactional(readOnly = false)
    public EntityReferenceTo modifyEntityReference(EntityReferenceInput entityReference) {
        EntityReferenceModel target = entityreferenceRepository.findById(entityReference.getId())
                .orElseThrow(() -> new RuntimeException("EntityReference not found"));
        EntityReferenceModel source = converter.convert(entityReference,
                EntityReferenceModel.class);
        Utils.copyNotNulls(source, target);
        return converter.convert(entityreferenceRepository.save(target), EntityReferenceTo.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SelectReference> findReference(String schema, String table, String fieldValue, String fieldText) {
        List<SelectReference> list = new ArrayList<>();
        StoredProcedureQuery storedProcedure = manager.createStoredProcedureQuery("core.dynamic_select","SelectReference")
                .registerStoredProcedureParameter("s_name", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("t_name", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("f_value", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("f_text", String.class, ParameterMode.IN);

        storedProcedure.setParameter("s_name", schema)
                .setParameter("t_name", table)
                .setParameter("f_value", fieldValue)
                .setParameter("f_text", fieldText);

               

             
                    // Execute query
                    storedProcedure.execute();
                   list = storedProcedure.getResultList();
                
          //  list=    (List<SelectReference>) storedProcedure.getResultList().stream().map(e -> converter.convert(e, SelectReference.class)).collect(Collectors.toList());

        return list;
    }

}