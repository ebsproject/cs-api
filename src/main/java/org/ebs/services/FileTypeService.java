package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.FileTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface FileTypeService {

    public Optional<FileTypeTo> findFileType(int id);

    public Page<FileTypeTo> findFileTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public FileTypeTo createFileType(FileTypeInput input);

    public FileTypeTo modifyFileType(FileTypeInput input);

    public int deleteFileType(int id);

    public WorkflowTo findWorkflow(int fileTypeId);
    
}