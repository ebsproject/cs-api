
package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ProcessModel;
import org.ebs.model.repos.ProcessRepository;
import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.Input.ProcessInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ProcessServiceImpl implements ProcessService {

    private final ProcessRepository processRepository;
    private final ConversionService converter;

    /**
     *
     * @param Process
     */
    @Override
    @Transactional(readOnly = false)
    public ProcessTo createProcess(ProcessInput Process) {
        ProcessModel model = converter.convert(Process, ProcessModel.class);
        model.setId(0);

        model = processRepository.save(model);
        return converter.convert(model, ProcessTo.class);
    }

    /**
     *
     * @param processId
     */
    @Override
    @Transactional(readOnly = false)
    public int deleteProcess(int processId) {
        ProcessModel process = processRepository.findById(processId)
                .orElseThrow(() -> new RuntimeException("Process not found"));
        process.setDeleted(true);
        processRepository.save(process);
        return processId;
    }

    /**
     *
     * @param processId
     */
    @Override
    public Optional<ProcessTo> findProcess(int processId) {
        if (processId < 1) {
            return Optional.empty();
        }
        return processRepository.findById(processId).filter(r -> !r.isDeleted())
                .map(r -> converter.convert(r, ProcessTo.class));
    }

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    @Override
    public Page<ProcessTo> findProcesses(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return processRepository
                .findByCriteria(ProcessModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ProcessTo.class));
    }

    /**
     *
     * @param process
     */
    @Override
    @Transactional(readOnly = false)
    public ProcessTo modifyProcess(ProcessInput process) {
        ProcessModel target = processRepository.findById(process.getId())
                .orElseThrow(() -> new RuntimeException("Process not found"));
        ProcessModel source = converter.convert(process, ProcessModel.class);
        Utils.copyNotNulls(source, target);
        return converter.convert(processRepository.save(target), ProcessTo.class);
    }

}