package org.ebs.services;

import java.util.List;

import org.ebs.services.to.OrganizationSync;
import org.ebs.services.to.ProgramSync;

public interface SynchronizeProgramService {

    boolean synchronizeOrganizations();
    
    boolean synchronizePrograms();

    List<ProgramSync> findProgramSyncList();

    List<OrganizationSync> findOrganizationSyncList();

    void synchronizeProgramsToCB();

    void synchronizeOrganizationsToCB();

    void synchronizeProgramsToCB(List<ProgramSync> programs);

    void deleteCBProgram(int programId);

    void deleteCBProgram(String programCode);

}
