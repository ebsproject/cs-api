package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.StatusTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.StatusInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface StatusService {

    public Optional<StatusTo> findStatus(int id);

    public Page<StatusTo> findStatuses(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public StatusTo createStatus(StatusInput input);

    public StatusTo modifyStatus(StatusInput input);

    public int deleteStatus(int id);

    public StatusTypeTo findStatusType(int statusId);

    public WorkflowInstanceTo findWorkflowInstance(int statusId);

    public TenantTo findTenant(int statusId);
    
}