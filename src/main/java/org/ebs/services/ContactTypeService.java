package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.CategoryTo;
import org.ebs.services.to.ContactType;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ContactTypeService {

    List<ContactType> findByContact(int contactId);

    ContactType findById(int ContactTypeId);

    Page<ContactType> findContactTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

 
	public CategoryTo findCategory(int contactTypeId);

}