package org.ebs.services;

import java.util.List;

import org.ebs.model.CountryModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.CountryRepository;
import org.ebs.services.to.Country;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class CountryServiceImpl implements CountryService {

    private final AddressRepository addressRepository;
    private final CountryRepository countryRepository;
    private final ConversionService converter;

    @Override
    public Country findByAddress(int addressId) {
        return addressRepository.findById(addressId).map(a -> a.getCountry())
                .map(c -> converter.convert(c, Country.class)).orElse(null);
    }

    @Override
    public Country findById(int countryId) {
        return countryRepository.findById(countryId).map(c -> converter.convert(c, Country.class)).orElse(null);
    }

    @Override
    public Page<Country> findCountrys(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return countryRepository.findByCriteria(CountryModel.class, filters, sort, page, disjunctionFilters)
                .map(c -> converter.convert(c, Country.class));
    }
}