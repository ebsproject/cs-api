package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ebs.model.HtmlTagModel;
import org.ebs.model.NodeModel;
import org.ebs.model.ProcessModel;
import org.ebs.model.ProductModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.model.NodeTypeModel;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.NodeCFRepository;
import org.ebs.model.repos.NodeRepository;
import org.ebs.model.repos.ProcessRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.ebs.model.repos.WorkflowViewTypeRepository;
import org.ebs.model.repos.NodeTypeRepository;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.NodeTypeTo;
import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.Input.NodeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class NodeServiceImpl implements NodeService {
    
    private final ConversionService converter;
    private final NodeRepository nodeRepository;
    private final HtmlTagRepository htmlTagRepository;
    private final ProductRepository productRepository;
    private final WorkflowRepository workflowRepository;
    private final ProcessRepository processRepository;
    private final WorkflowViewTypeRepository workflowViewTypeRepository;
    private final NodeTypeRepository nodeTypeRepository;
    private final NodeCFRepository nodeCFRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<NodeTo> findNode(int id) {
        if (id < 1)
            return Optional.empty();

        return nodeRepository.findById(id).map(r -> converter.convert(r, NodeTo.class));
    }

    @Override
    public Page<NodeTo> findNodes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return nodeRepository
                .findByCriteria(NodeModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, NodeTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public NodeTo createNode(NodeInput input) {

        // if (!nodeRepository.findByDesignRef(input.getDesignRef()).isEmpty())
        //     throw new RuntimeException("Design ref must be unique");
        
        input.setId(0);

        NodeModel model = converter.convert(input, NodeModel.class);

        setNodeDependencies(model, input);

        model = nodeRepository.save(model);

        return converter.convert(model, NodeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public NodeTo modifyNode(NodeInput input) {
        
        NodeModel target = verifyNodeModel(input.getId());

        // if (nodeRepository.findByDesignRef(input.getDesignRef())
        // .stream()
        // .filter(r -> !r.equals(target)).count() > 0)
        //     throw new RuntimeException("Design ref must be unique");

        NodeModel source = converter.convert(input, NodeModel.class);

        Utils.copyNotNulls(source, target);

        setNodeDependencies(target, input);

        return converter.convert(nodeRepository.save(target), NodeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteNode(int id) {

        NodeModel model = verifyNodeModel(id);

        model.setDeleted(true);

        nodeRepository.save(model);

        return id;

    }

    NodeModel verifyNodeModel(int nodeId) {
        return nodeRepository.findById(nodeId)
            .orElseThrow(() -> new RuntimeException("Node id " + nodeId + " not found"));
    }

    void setNodeDependencies(NodeModel model, NodeInput input) {
        setNodeHtmlTag(model, input.getHtmlTagId());
        setNodeProduct(model, input.getProductId());
        setNodeWorkflow(model, input.getWorkflowId());
        setNodeProcess(model, input.getProcessId());
        setNodeWorkflowViewType(model, input.getWorkflowViewTypeId());
        setNodeType(model, input.getNodeTypeId());
        setNodeTenant(model, input.getTenantId());
    }

    void setNodeHtmlTag(NodeModel model, Integer htmlTagId) {
        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                .orElseThrow(() -> new RuntimeException("Html tag id " + htmlTagId + " not found"));
            model.setHtmltag(htmlTag);
        }
    }

    void setNodeProduct(NodeModel model, Integer productId) {
        if (productId != null && productId > 0) {
            ProductModel product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Product id " + productId + " not found"));
            model.setProduct(product);
        }
    }

    void setNodeWorkflow(NodeModel model, Integer workflowId) {
        if (workflowId != null && workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
    }

    void setNodeProcess(NodeModel model, Integer processId) {
        if (processId != null && processId > 0) {
            ProcessModel process = processRepository.findById(processId)
                .orElseThrow(() -> new RuntimeException("Process id " + processId + " not found"));
            model.setProcess(process);
        }
    }

    void setNodeWorkflowViewType(NodeModel model, int workflowViewTypeId) {
        if (workflowViewTypeId > 0) {
            WorkflowViewTypeModel workflowViewType = workflowViewTypeRepository.findById(workflowViewTypeId)
                .orElseThrow(() -> new RuntimeException("WorkflowViewType id " + workflowViewTypeId + " not found"));
            model.setWorkflowViewType(workflowViewType);
        }
    }

        void setNodeType(NodeModel model, int nodeTypeId) {
        if (nodeTypeId > 0) {
            NodeTypeModel nodeType = nodeTypeRepository.findById(nodeTypeId)
                .orElseThrow(() -> new RuntimeException("NodeType id " + nodeTypeId + " not found"));
            model.setNodeType(nodeType);
        }
    }


    void setNodeTenant(NodeModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

    @Override
    public HtmlTagTo findHtmlTag(int nodeId) {

        NodeModel model = verifyNodeModel(nodeId);

        if (model.getHtmltag() == null)
            return null;
        
        return converter.convert(model.getHtmltag(), HtmlTagTo.class);

    }

    @Override
    public ProductTo findProduct(int nodeId) {

        NodeModel model = verifyNodeModel(nodeId);

        if (model.getProduct() == null)
            return null;
        
        return converter.convert(model.getProduct(), ProductTo.class);

    }

    @Override
    public WorkflowTo findWorkflow(int nodeId) {

        NodeModel model = verifyNodeModel(nodeId);

        if (model.getWorkflow() == null)
            return null;
        
        return converter.convert(model.getWorkflow(), WorkflowTo.class);

    }

    @Override
    public ProcessTo findProcess(int nodeId) {

        NodeModel model = verifyNodeModel(nodeId);

        if (model.getProcess() == null)
            return null;
        
        return converter.convert(model.getProcess(), ProcessTo.class);

    }

    @Override
    public WorkflowViewTypeTo findWorkflowViewType(int nodeId) {

        NodeModel model = verifyNodeModel(nodeId);

        if (model.getWorkflowViewType() == null)
            return null;
        
        return converter.convert(model.getWorkflowViewType(), WorkflowViewTypeTo.class);

    } 
    @Override
    public NodeTypeTo findNodeType(int nodeId) {

        NodeModel model = verifyNodeModel(nodeId);

        if (model.getNodeType() == null)
            return null;
        
        return converter.convert(model.getNodeType(), NodeTypeTo.class);

    } 

    @Override
    public TenantTo findTenant(int nodeId) {
        
        NodeModel model = verifyNodeModel(nodeId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    @Override
    public List<NodeTo> findByStage(int stageId) {
        return nodeRepository.findByStage(stageId).stream()
                .map(t -> converter.convert(t, NodeTo.class)).collect(Collectors.toList());
    }

    @Override
    public List<NodeCFTo> findNodeCFs(int nodeId) {
        return nodeCFRepository.findByNodeId(nodeId)
            .stream().map(r -> converter.convert(r, NodeCFTo.class)).collect(Collectors.toList());
    } 

}
