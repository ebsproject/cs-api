package org.ebs.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.ebs.model.AddressModel;
import org.ebs.model.CategoryModel;
import org.ebs.model.ContactAddressModel;
import org.ebs.model.ContactModel;
import org.ebs.model.CropModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UnitTypeModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.CategoryRepository;
import org.ebs.model.repos.ContactAddressRepository;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UnitTypeRepository;
import org.ebs.services.redirect.MicroIntegrationService;
import org.ebs.services.to.OrganizationCB;
import org.ebs.services.to.OrganizationSync;
import org.ebs.services.to.ProgramCB;
import org.ebs.services.to.ProgramSync;
import org.ebs.services.to.Input.HierarchyInput;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Moves information from Core Breeding domain (CB) into Core System domain (CS)
 */
@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
class SynchronizeProgramServiceImpl implements SynchronizeProgramService {

    private final CBGraphQLService cbGraphQLService;
    private final InstitutionRepository institutionRepository;
    private final CropRepository cropRepository;
    private final ContactRepository contactRepository;
    private final CategoryRepository categoryRepository;
    private final UnitTypeRepository unitTypeRepository;
    private final TenantRepository tenantRepository;
    private final HierarchyService hierarchyService;
    private final AddressRepository addressRepository;
    private final ContactAddressRepository contactAddressRepository;
    static final int DEFAULT_TENANT = 1;

    protected final TokenGenerator tokenGenerator;

    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;

    @Override
    public boolean synchronizeOrganizations() {
        log.info("synchronizing organizations...");
        List<InstitutionModel> csOrganizations = institutionRepository.findByContactCategoryName("Institution");
        List<String> csOrganizationCodes = csOrganizations
            .stream()
            .map(org -> org.getCommonName())
            .collect(Collectors.toList());
        boolean noErrors = true;
        for (OrganizationCB cbOrganization : cbGraphQLService.findAllOrganizations()) {
            log.info("verifying {}", cbOrganization.getOrganizationCode());
            if (csOrganizationCodes.contains(cbOrganization.getOrganizationCode())) {
                if (!updateOrganization(csOrganizations
                    .stream()
                    .filter(org -> org.getCommonName().equals(cbOrganization.getOrganizationCode()))
                    .findFirst().orElse(null)
                ))
                    noErrors = false;
            } else {
                if (!createOrganization(cbOrganization))
                    noErrors = false;
            }
        }
        return noErrors;
    }

    private boolean updateOrganization(InstitutionModel csOrganization) {
        log.info("updating organization");
        if (csOrganization == null) {
            log.info("Could not get cs institution info, skipping...");
            return false;
        }
        AddressModel defaultAddress = addressRepository.findById(1).orElse(null);
        List<ContactAddressModel> orgAddresses = contactAddressRepository
            .findByContact(csOrganization.getContact());
        if (orgAddresses.isEmpty()) {
            if (defaultAddress == null) {
                log.info("Could not find default address to copy");
                log.info("skipping...");
                return false;
            }
            AddressModel defaultCopy = new AddressModel();
            defaultCopy.setLocation(defaultAddress.getLocation());
            defaultCopy.setRegion(defaultAddress.getRegion());
            defaultCopy.setZipCode(defaultAddress.getZipCode());
            defaultCopy.setStreetAddress(csOrganization.getCommonName() + " " + defaultAddress.getStreetAddress());
            defaultCopy.setCountry(defaultAddress.getCountry());
            defaultCopy.setFullAddress(defaultAddress.getFullAddress());
            defaultCopy.setId(0);
            defaultCopy = addressRepository.save(defaultCopy);
            contactAddressRepository.save(new ContactAddressModel(csOrganization.getContact(), defaultCopy, true));
        } else {
            orgAddresses.forEach(ad -> {
                if (ad.getAddress().equals(defaultAddress)) {
                    contactAddressRepository.delete(ad);
                }
            });
            orgAddresses = contactAddressRepository
                .findByContact(csOrganization.getContact());
            if (orgAddresses.isEmpty()) {
                    if (defaultAddress == null) {
                        log.info("Could not find default address to copy");
                        log.info("skipping...");
                        return false;
                    }
                    AddressModel defaultCopy = new AddressModel();
                    defaultCopy.setLocation(defaultAddress.getLocation());
                    defaultCopy.setRegion(defaultAddress.getRegion());
                    defaultCopy.setZipCode(defaultAddress.getZipCode());
                    defaultCopy.setStreetAddress(csOrganization.getCommonName() + " " + defaultAddress.getStreetAddress());
                    defaultCopy.setCountry(defaultAddress.getCountry());
                    defaultCopy.setFullAddress(defaultAddress.getFullAddress());
                    defaultCopy.setId(0);
                    defaultCopy = addressRepository.save(defaultCopy);
                    contactAddressRepository.save(new ContactAddressModel(csOrganization.getContact(), defaultCopy, true));
            } else {
                List<ContactAddressModel> defaultAddresses = 
                    orgAddresses.stream().filter(ad -> ad.isDefault()).collect(Collectors.toList());
                if (defaultAddresses.isEmpty()) {
                    orgAddresses.get(0).setDefault(true);
                    contactAddressRepository.save(orgAddresses.get(0));
                } else {
                    defaultAddresses.forEach(ad -> {
                        ad.setDefault(false);
                        contactAddressRepository.save(ad);
                    });
                    orgAddresses.get(0).setDefault(true);
                    contactAddressRepository.save(orgAddresses.get(0));
                }
            }
        }
        csOrganization.setCgiar(true);
        institutionRepository.save(csOrganization);
        return true;
    }

    private boolean createOrganization(OrganizationCB cbOrganization) {
        log.info("creating organization");
        try {
            CategoryModel institutionCategory = categoryRepository.findById(2).orElse(null);
            if (institutionCategory == null) {
                log.info("Could not find institution category");
                log.info("skipping...");
                return false;
            }
            TenantModel defaultTenant = tenantRepository.findById(DEFAULT_TENANT).orElse(null);
            if (defaultTenant == null) {
                log.info("Could not find default tenant");
                log.info("skipping...");
                return false;
            }
            AddressModel defaultAddress = addressRepository.findById(1).orElse(null);
            if (defaultAddress == null) {
                log.info("Could not find default address");
                log.info("skipping...");
                return false;
            }
            ContactModel contactModel = new ContactModel();
            contactModel.setId(0);
            contactModel.setCategory(institutionCategory);
            contactModel.setEmail(cbOrganization.getOrganizationCode() + "@" + cbOrganization.getOrganizationCode() + ".org");
            contactModel.setTenants(new HashSet<>(Arrays.asList(defaultTenant)));
            contactModel = contactRepository.save(contactModel);
            AddressModel defaultCopy = new AddressModel();
            defaultCopy.setLocation(defaultAddress.getLocation());
            defaultCopy.setRegion(defaultAddress.getRegion());
            defaultCopy.setZipCode(defaultAddress.getZipCode());
            defaultCopy.setStreetAddress(cbOrganization.getOrganizationCode() + " " + defaultAddress.getStreetAddress());
            defaultCopy.setCountry(defaultAddress.getCountry());
            defaultCopy.setFullAddress(defaultAddress.getFullAddress());
            defaultCopy.setId(0);
            defaultCopy = addressRepository.save(defaultCopy);
            contactAddressRepository.save(new ContactAddressModel(contactModel, defaultCopy, true));
            InstitutionModel institutionModel = new InstitutionModel();
            institutionModel.setLegalName(cbOrganization.getOrganizationName());
            institutionModel.setCommonName(cbOrganization.getOrganizationCode());
            institutionModel.setCgiar(true);
            institutionModel.setContact(contactModel);
            institutionRepository.save(institutionModel);
        } catch (Exception e) {
            log.info("Error creating organization {}", cbOrganization.getOrganizationCode());
            log.info("{}", e.getLocalizedMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean synchronizePrograms() {
        log.debug("synchronizing programs...");
        boolean noErrors = true;
        for (ProgramCB cbProgram : cbGraphQLService.findAllPrograms())  {
            log.info("\nverifying program {}, {}...", cbProgram.getProgramCode(), cbProgram.getProgramDbId());
            if (cbProgram.getCropProgram() == null) {
                log.info("cannot update program, no cropProgram information found");
                return false;
            }
            if (cbProgram.getCropProgram() != null
                && cbProgram.getCropProgram().getCrop() != null
                && cbProgram.getCropProgram().getOrganization() != null) {
                        if (!importProgram(cbProgram))
                            noErrors = false;
                }
        }
        return noErrors;
    }

    boolean importProgram(ProgramCB cbProgram) {
        List<InstitutionModel> csProgram = 
            institutionRepository.findByCommonNameAndUnitTypeNameAndDeletedIsFalse(cbProgram.getProgramCode(), "Program");
        if (csProgram.size() > 1) {
            log.info("Error Synchronizing program: Multiple records with the same common name (program code) exist");
            return false;
        }
        if (csProgram.isEmpty()) {
            return createProgram(cbProgram);
        } else {
            return updateProgram(cbProgram, csProgram.get(0));
        }
    }

    private boolean createProgram(ProgramCB cbProgram) {
        log.info("creating new program {}...", cbProgram.getProgramCode());
        institutionRepository.findByExternalCode(cbProgram.getProgramDbId())
            .forEach(i -> {
                i.setExternalCode(0);
                institutionRepository.save(i);
            });
        UnitTypeModel programUnitType = unitTypeRepository.findById(1).orElse(null);
        if (programUnitType == null) {
            log.info("Could not find unit type information");
            return false;
        }
        CategoryModel programCategory = categoryRepository.findById(4).orElse(null);
        if (programCategory == null) {
            log.info("Could not find internal unit category");
            return false;
        }
        TenantModel defaultTenant = tenantRepository.findById(DEFAULT_TENANT).orElse(null);
        if (defaultTenant == null) {
            log.info("Could not find default tenant");
            return false;
        }
        ContactModel contactModel = new ContactModel();
        contactModel.setId(0);
        contactModel.setCategory(programCategory);
        contactModel.setEmail(cbProgram.getProgramCode().toLowerCase() + "@cgiar.org");
        contactModel.setTenants(new HashSet<>(Arrays.asList(defaultTenant)));
        contactModel = contactRepository.save(contactModel);
        InstitutionModel institutionModel = new InstitutionModel();
        institutionModel.setLegalName(cbProgram.getProgramName());
        institutionModel.setCommonName(cbProgram.getProgramCode());
        institutionModel.setExternalCode(cbProgram.getProgramDbId());
        institutionModel.setCgiar(false);
        institutionModel.setUnitType(programUnitType);
        setProgramCrop(institutionModel, cbProgram.getCropProgram().getCrop().getCropCode());
        institutionModel.setContact(contactModel);
        institutionRepository.save(institutionModel);
        setParentOrganization(contactModel, cbProgram.getCropProgram().getOrganization().getOrganizationCode());
        return true;
    }

    private boolean updateProgram(ProgramCB cbProgram, InstitutionModel csProgram) {
        log.info("updating program {}...", cbProgram.getProgramCode());
        UnitTypeModel programUnitType = unitTypeRepository.findById(1).orElse(null);
        if (programUnitType == null) {
            log.info("Could not find unit type information");
            return false;
        }
        institutionRepository.findByExternalCode(cbProgram.getProgramDbId())
            .forEach(i -> {
                i.setExternalCode(0);
                institutionRepository.save(i);
            });
        csProgram.setLegalName(cbProgram.getProgramName());
        csProgram.setExternalCode(cbProgram.getProgramDbId());
        csProgram.setUnitType(programUnitType);
        if (csProgram.getCrops() == null || csProgram.getCrops().isEmpty())
            setProgramCrop(csProgram, cbProgram.getCropProgram().getCrop().getCropCode());
        InstitutionModel institution = institutionRepository.save(csProgram);
        if (!csProgram.isDeleted())
            setParentOrganization(institution.getContact(), cbProgram.getCropProgram().getOrganization().getOrganizationCode());
        return true;
    }

    private void setProgramCrop(InstitutionModel program, String cropCode) {
        CropModel programCrop = cropRepository.findByCodeAndDeletedIsFalse(cropCode)
            .orElse(null);
        if (programCrop == null) {
            log.info("No crop with code " + cropCode + " found in CS");
            log.info("updating program with no crops...");
            program.setCrops(new ArrayList<>());
        } else
            program.setCrops(new ArrayList<>(Arrays.asList(programCrop)));
    }

    private void setParentOrganization(ContactModel program, String organizationName) {
        hierarchyService.deleteContactParentsRelation(program.getId());
        ContactModel parentContact = 
            contactRepository.findByCategoryNameAndInstitutionCommonNameAndDeletedIsFalseAndInstitutionDeletedIsFalse("Institution", "CIMMYT")
            .orElse(null);
        List<InstitutionModel> parent = institutionRepository.findByCommonNameAndDeletedIsFalse(organizationName);
        if (parent.size() > 1) {
           if (organizationName.equals("IRRI"))
                parentContact = 
                    contactRepository.findByCategoryNameAndInstitutionCommonNameAndDeletedIsFalseAndInstitutionDeletedIsFalse("Institution", "IRRI")
                    .orElse(parentContact);
        } else if (parent.size() == 1) {
            parentContact = parent.get(0).getContact();
        }
        if (parentContact == null)
            log.info("Could not assign parent {} to program {}", organizationName, program.getInstitution().getCommonName());
        else {
            setProgramAddress(program, parentContact);
            hierarchyService.createHierarchy(new HierarchyInput(program.getId(), parentContact.getId(), false));
        }
    }

    private void setProgramAddress(ContactModel program, ContactModel parentContact) {
        List<ContactAddressModel> programAddresses = contactAddressRepository.findByContact(program);
        if (programAddresses.isEmpty()) {
            AddressModel address = addressRepository.findById(1).orElse(null);
            if (parentContact.getAddresses().size() > 1) {
                if (parentContact.getInstitution().getCommonName().equals("CIMMYT")) 
                    address = addressRepository.findById(2).orElse(address);
                else if (parentContact.getInstitution().getCommonName().equals("IRRI"))
                    address = addressRepository.findById(3).orElse(address);
            } else if (!parentContact.getAddresses().isEmpty()) {
                address = parentContact.getAddresses().get(0);
            }
            if (address != null) {
                ContactAddressModel programAddress = new ContactAddressModel(program, address, true);
                contactAddressRepository.save(programAddress);
            } else
                log.info("Could not assign address to program {}", program.getInstitution().getLegalName());
        } else {
            List<ContactAddressModel> defaultAddresses = programAddresses.stream().filter(ad -> ad.isDefault()).collect(Collectors.toList());
            if (defaultAddresses.isEmpty()) {
                programAddresses.get(0).setDefault(true);
                contactAddressRepository.save(programAddresses.get(0));
            } else if (defaultAddresses.size() > 1) {
                defaultAddresses.forEach(da -> {
                    da.setDefault(false);
                    contactAddressRepository.save(da);
                });
                programAddresses.get(0).setDefault(true);
                contactAddressRepository.save(programAddresses.get(0));
            }
        }
    }

    public List<ProgramSync> findProgramSyncList() {
        log.info("setting up program sync list");
        List<ProgramSync> programSyncList = new ArrayList<>();
        institutionRepository.findByUnitTypeName("Program")
            .forEach(i -> {
                if (i.getCrops() != null && !i.getCrops().isEmpty()) {
                    ProgramSync programSync = new ProgramSync();
                    programSync.setProgramCode(i.getCommonName());
                    programSync.setProgramName(i.getLegalName());
                    programSync.setOrganizationCode(null);
                    if (i.getContact().getParents() == null || i.getContact().getParents().isEmpty())
                        programSync.setOrganizationCode("EBS");
                    else {
                        programSync.setOrganizationCode(i.getContact().getParents().stream().filter(p -> p.getInstitution().getInstitution() != null
                            && p.getInstitution().getInstitution().isCgiar()).findFirst().map(pi -> pi.getInstitution().getInstitution().getCommonName())
                            .orElse("EBS"));
                    }
                    programSync.setCropCode(i.getCrops().get(0).getCode());
                    programSync.setActive(!i.isDeleted() && !i.getContact().isDeleted());
                    programSyncList.add(programSync);
                } else {
                    log.info("Program {} has no crops, cannot sync to CB...", i.getCommonName());
                }
            });
        return programSyncList;
    }

    public List<OrganizationSync> findOrganizationSyncList() {
        log.info("setting up organization sync list");
        List<OrganizationSync> organizationSyncList = new ArrayList<>();
        institutionRepository.findByContactCategoryNameAndIsCgiarIsTrue("Institution")
            .forEach(i -> {
                organizationSyncList.add(
                    new OrganizationSync(i.getCommonName(), i.getLegalName(), !i.isDeleted() && !i.getContact().isDeleted())
                );
            });
        return organizationSyncList;
    }

    private String callCbSynchronizeApi(List<?> csList, String syncObject) {
        ObjectMapper mapper = new ObjectMapper();
        String cbEndpoint = cbGraphQlEndpoint.replace("graphql", "");
        cbEndpoint = cbEndpoint.replace("cbapi", "cbgraphqlapi");
        if (cbEndpoint == null || cbEndpoint.isBlank()) {
            log.info("Could not get cbgraphql url from configuration");
            return "Could not get cbgraphql url from configuration";
        }
        try {
            log.info("Calling cbgraphql " + syncObject + " sync API");
            MicroIntegrationService service = new MicroIntegrationService(WebClient
                .builder()
                .baseUrl(cbEndpoint)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
                .build());
            String cbGraphResponse = service.redirectRequestToMI("/sync/" + syncObject, mapper.writeValueAsString(csList));
            log.info("Response from cbgraphql {}", cbGraphResponse);
            return cbGraphResponse;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("Error calling cbgraphql " + syncObject + " sync API");
            return "Error calling cbgraphql " + syncObject + " sync API";
        }
    }

    private String callCbDeleteProgramApi(String payload) {
        ObjectMapper mapper = new ObjectMapper();
        String cbEndpoint = cbGraphQlEndpoint.replace("graphql", "");
        cbEndpoint = cbEndpoint.replace("cbapi", "cbgraphqlapi");
        if (cbEndpoint == null || cbEndpoint.isBlank()) {
            log.info("Could not get cbgraphql url from configuration");
            return "Could not get cbgraphql url from configuration";
        }
        try {
            log.info("Calling cbgraphql delete program API with payload {}", payload);
            MicroIntegrationService service = new MicroIntegrationService(WebClient
                .builder()
                .baseUrl(cbEndpoint)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
                .build());
            String cbGraphResponse = service.redirectRequestToMI("/sync/program/delete", payload);
            log.info("Response from cbgraphql {}", cbGraphResponse);
            return cbGraphResponse;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("Error calling cbgraphql delete program API");
            return "Error calling cbgraphql delete program API";
        }
    }

    @Override
    public void synchronizeProgramsToCB() {
        callCbSynchronizeApi(findProgramSyncList(), "programs");
    }

    @Override
    public void synchronizeOrganizationsToCB() {
        callCbSynchronizeApi(findOrganizationSyncList(), "organizations");
    }

    @Override
    public void synchronizeProgramsToCB(List<ProgramSync> programs) {
        callCbSynchronizeApi(programs, "programs");
    }

    @Override
    public void deleteCBProgram(int programId) {
        ;
    }

    @Override
    public void deleteCBProgram(String programCode) {
        callCbDeleteProgramApi("{\"programCode\":\"" + programCode + "\"}");
    }

}