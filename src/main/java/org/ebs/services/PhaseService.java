package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.Input.PhaseInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface PhaseService {

    public Optional<PhaseTo> findPhase(int id);

    public Page<PhaseTo> findPhases(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public PhaseTo createPhase(PhaseInput input);

    public PhaseTo modifyPhase(PhaseInput input);

    public int deletePhase(int id);

    public HtmlTagTo findHtmlTag(int phaseId);

    public WorkflowTo findWorkflow(int phaseId);

    public WorkflowViewTypeTo findWorkflowViewType(int phaseId);

    public TenantTo findTenant(int phaseId);

    public List<StageTo> findStages(int phaseId);
}
