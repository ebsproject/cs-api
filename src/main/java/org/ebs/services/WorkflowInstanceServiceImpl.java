package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.StatusModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.repos.WorkflowInstanceRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.ebs.services.to.StatusTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.WorkflowInstanceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class WorkflowInstanceServiceImpl implements WorkflowInstanceService {

    private final ConversionService converter;
    private final WorkflowInstanceRepository workflowInstanceRepository;
    private final WorkflowRepository workflowRepository;

    @Override
    public Optional<WorkflowInstanceTo> findWorkflowInstance(int id) {
        if (id < 1)
            return Optional.empty();

        return workflowInstanceRepository.findById(id).map(r -> converter.convert(r, WorkflowInstanceTo.class));
    }

    @Override
    public Page<WorkflowInstanceTo> findWorkflowInstances(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
            return workflowInstanceRepository
                .findByCriteria(WorkflowInstanceModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, WorkflowInstanceTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public WorkflowInstanceTo createWorkflowInstance(WorkflowInstanceInput input) {

        input.setId(0);

        WorkflowInstanceModel model = converter.convert(input, WorkflowInstanceModel.class);

        setWorkflowInstanceDependencies(model, input);

        model = workflowInstanceRepository.save(model);

        return converter.convert(model, WorkflowInstanceTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public WorkflowInstanceTo modifyWorkflowInstance(WorkflowInstanceInput input) {
        
        WorkflowInstanceModel target = verifyWorkflowInstanceModel(input.getWorkflowId());

        WorkflowInstanceModel source = converter.convert(input, WorkflowInstanceModel.class);

        Utils.copyNotNulls(source, target);

        setWorkflowInstanceDependencies(target, input);

        return converter.convert(workflowInstanceRepository.save(target), WorkflowInstanceTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteWorkflowInstance(int id) {

        WorkflowInstanceModel model = verifyWorkflowInstanceModel(id);

        model.setDeleted(true);

        workflowInstanceRepository.save(model);

        return id;

    }

    @Override
    public WorkflowTo findWorkflow(int workflowInstanceId) {

        WorkflowInstanceModel model = verifyWorkflowInstanceModel(workflowInstanceId);

        if (model.getWorkflow() == null)
            return null;

        return converter.convert(model.getWorkflow(), WorkflowTo.class);

    }

    @Override
    public StatusTo findStatus(int workflowInstanceId) {

        WorkflowInstanceModel model = verifyWorkflowInstanceModel(workflowInstanceId);

        if (model.getStatus() == null || model.getStatus().isEmpty())
            return null;

        StatusModel currentStatus = model.getStatus().stream().filter(s -> s.getCompletionDate() == null)
            .findFirst().orElse(null);
        
        if (currentStatus == null)
            return null;

        return converter.convert(currentStatus, StatusTo.class);

    }

    void setWorkflowInstanceDependencies(WorkflowInstanceModel model, WorkflowInstanceInput input) {
        setWorkflowInstanceWorkflow(model, input);
    }

    void setWorkflowInstanceWorkflow(WorkflowInstanceModel model, WorkflowInstanceInput input) {
        if (input.getWorkflowId() > 0) {
            WorkflowModel workflow = workflowRepository.findById(input.getWorkflowId())
                .orElseThrow(() -> new RuntimeException("Workflow id " + input.getWorkflowId() + " not found"));
            model.setWorkflow(workflow);
        }
    }

    WorkflowInstanceModel verifyWorkflowInstanceModel(int workflowInstanceId) {
        return workflowInstanceRepository.findById(workflowInstanceId)
            .orElseThrow(() -> new RuntimeException("Workflow instance id " + workflowInstanceId + " not found"));
    }
    
}
