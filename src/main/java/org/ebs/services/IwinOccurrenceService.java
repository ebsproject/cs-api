package org.ebs.services;

import java.util.List;

import org.ebs.services.to.IWINOccurrenceTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface IwinOccurrenceService {

    public Page<IWINOccurrenceTo> getIwinOccurrenceByCoopkey(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);
    
}
