package org.ebs.services;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.util.List;

import org.ebs.model.ContactModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.HierarchyRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.CropTo;
import org.ebs.services.to.Institution;
import org.ebs.services.to.UnitTypeTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class InstitutionServiceImpl implements InstitutionService {

    private final ConversionService converter;
    private final ContactRepository contactRepository;
    private final InstitutionRepository institutionRepository;
    private final HierarchyRepository hierarchyRepository;

    @Override
    public Institution findById(int institutionId) {
        if (institutionId < 0)
            return null;
        return institutionRepository.findById(institutionId)
            .map(i -> converter.convert(i, Institution.class)).orElse(null);
    }

    @Override
    public Page<Institution> findInstitutions(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return institutionRepository
                .findByCriteria(InstitutionModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, Institution.class));
    }

    @Override
    public Contact findPrincipalContact(int institutionId) {
        return institutionRepository.findById(institutionId)
                .map(i -> hierarchyRepository
                        .findByInstitutionAndIsPrincipalContactIsTrueAndDeletedIsFalse(i.getContact())
                        .map(r -> converter.convert(r.getContact(), Contact.class)).orElse(null))
                .orElse(null);
    }

    @Override
    public List<Institution> findInstitutionsByContact(int contactId) {
        ContactModel contactModel = contactRepository.findById(contactId).orElse(null);
        if (contactModel != null && contactModel.getInstitution() != null) {
            return hierarchyRepository.findByContact(contactModel).stream()
                    .map(e -> converter.convert(e.getInstitution(), Institution.class)).collect(toList());
        } else
            return null;
    }

    @Override
    public List<CropTo> findInstitutionCrops(int institutionId) {
        InstitutionModel institution = institutionRepository.findById(institutionId).orElse(null);

        if (institution == null || institution.getCrops().isEmpty())
            return emptyList();

        return institution.getCrops().stream().map(c -> converter.convert(c, CropTo.class)).collect(toList());
    }

    @Override
    public UnitTypeTo findUnitType(int institutionId) {
        return institutionRepository.findById(institutionId)
            .filter(i -> i.getUnitType() != null)
            .map(i -> converter.convert(i.getUnitType(), UnitTypeTo.class)).orElse(null);
    }

}
