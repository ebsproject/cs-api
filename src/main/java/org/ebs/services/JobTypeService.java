package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.JobTypeTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface JobTypeService {

	public Optional<JobTypeTo> findJobType(int jobTypeId);

	public Page<JobTypeTo> findJobTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);
	
/* 	public JobLogTo createJobLog(JobLogInput jobLogInput);

	public JobLogTo modifyJobLog(JobLogInput jobLogInput);

	public int deleteJobLog(int jobLogId); */

}