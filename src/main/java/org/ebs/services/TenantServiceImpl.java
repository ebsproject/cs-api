package org.ebs.services;

import static java.time.Instant.now;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.CustomerModel;
import org.ebs.model.OrganizationModel;
import org.ebs.model.ProductAuthorizationModel;
import org.ebs.model.ProductModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.CustomerRepository;
import org.ebs.model.repos.InstanceRepository;
import org.ebs.model.repos.NumberSequenceRuleRepository;
import org.ebs.model.repos.OrganizationRepository;
import org.ebs.model.repos.ProductAuthorizationRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.InstanceTo;
import org.ebs.services.to.NumberSequenceRuleTo;
import org.ebs.services.to.OrganizationTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.InstanceInput;
import org.ebs.services.to.Input.TenantInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class TenantServiceImpl implements TenantService {

    private final ConversionService converter;
    private final TenantRepository tenantRepository;
    private final InstanceRepository instanceRepository;
    private final OrganizationRepository organizationRepository;
    private final CustomerRepository customerRepository;
    private final NumberSequenceRuleRepository numbersequenceruleRepository;
    private final ProductRepository productRepository;
    private final ProductAuthorizationRepository productAuthorizationRepo;
    private final InstanceService instanceService;
    private final UserRepository userRepository;

    /**
     *
     * @param tenant
     */
    @Override
    @Transactional(readOnly = false)
    public TenantTo createTenant(TenantInput tenant) {
        TenantModel model = converter.convert(tenant, TenantModel.class);
        model.setId(0);
        verifyDependencies(model, tenant);
        model.setExpired(model.getExpiration().toInstant().isBefore(now()));

        model = tenantRepository.save(model);
        generateSingleInstanceModel(model);
        return converter.convert(model, TenantTo.class);
    }

    void verifyDependencies(TenantModel model, TenantInput input) {
        OrganizationModel organizationModel = organizationRepository
                .findByIdAndDeletedIsFalse(input.getOrganizationId())
                .orElseThrow(() -> new RuntimeException("Organization not found"));
        model.setOrganization(organizationModel);

        CustomerModel customerModel = customerRepository
                .findByIdAndDeletedIsFalse(input.getCustomerId())
                .orElseThrow(() -> new RuntimeException("Customer not found"));
        model.setCustomer(customerModel);
        model.setInstances(Collections.emptySet());
    }

    InstanceTo generateSingleInstanceModel(TenantModel model) {
        InstanceInput input = new InstanceInput(0, model.getId(), null, "NA", null, null);
        return instanceService.createInstance(input);
    }

    /**
     *
     * @param tenantId
     */
    @Override
    @Transactional(readOnly = false)
    public int deleteTenant(int tenantId) {
        TenantModel tenant = verifyTenantModel(tenantId);
        tenant.setDeleted(true);
        tenant.getInstances().forEach(i -> i.setDeleted(true));
        tenantRepository.save(tenant);
        return tenantId;
    }

    TenantModel verifyTenantModel(int tenantId) {
        return tenantRepository.findByIdAndDeletedIsFalse(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant not found"));
    }

    /**
     *
     * @param tenantId
     */
    public Optional<CustomerTo> findCustomer(int tenantId) {
        return tenantRepository.findByIdAndDeletedIsFalse(tenantId)
                .map(r -> converter.convert(r.getCustomer(), CustomerTo.class));
    }

    // /**
    //  *
    //  * @param tenantId
    //  */
    // public Set<EmailTemplateTo> findEmailTemplates(int tenantId) {
    //     return emailtemplateRepository.findByTenantIdsAndDeletedIsFalse(tenantId).stream()
    //             .map(e -> converter.convert(e, EmailTemplateTo.class)).collect(Collectors.toSet());
    // }

    /**
     *
     * @param tenantId
     */
    public Set<InstanceTo> findInstances(int tenantId) {
        return instanceRepository.findByTenantIdAndDeletedIsFalse(tenantId).stream()
                .map(e -> converter.convert(e, InstanceTo.class)).collect(Collectors.toSet());
    }

    /**
     *
     * @param tenantId
     */
    public Set<NumberSequenceRuleTo> findNumberSequenceRules(int tenantId) {
        return numbersequenceruleRepository.findByTenantIdAndDeletedIsFalse(tenantId).stream()
                .map(e -> converter.convert(e, NumberSequenceRuleTo.class))
                .collect(Collectors.toSet());
    }

    /**
     *
     * @param tenantId
     */
    public Optional<OrganizationTo> findOrganization(int tenantId) {
        return tenantRepository.findByIdAndDeletedIsFalse(tenantId)
                .map(r -> converter.convert(r.getOrganization(), OrganizationTo.class));
    }

    /**
     *
     * @param tenantId
     */
    public Set<Contact> findContacts(int tenantId) {
        // return personRepository.findByTenantIdAndDeletedIsFalse(tenantId).stream()
        // .map(e -> converter.convert(e, Contact.class)).collect(Collectors.toSet());
        return null;
    }

    /**
     *
     * @param tenantId
     */
    /* public Set<ProgramTo> findPrograms(int tenantId) {
        return programRepository.findByTenantIdAndDeletedIsFalse(tenantId).stream()
                .map(e -> converter.convert(e, ProgramTo.class)).collect(Collectors.toSet());
    } */

    /**
     *
     * @param tenantId
     */
    @Override
    public Optional<TenantTo> findTenant(int tenantId) {
        if (tenantId < 1) {
            return Optional.empty();
        }
        return tenantRepository.findByIdAndDeletedIsFalse(tenantId)
                .map(r -> converter.convert(r, TenantTo.class));
    }

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    @Override
    public Page<TenantTo> findTenants(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        return tenantRepository
                .findByCriteria(TenantModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, TenantTo.class));
    }

    /**
     *
     * @param tenantId
     */
    public Set<UserTo> findUsers(int tenantId) {
        return tenantRepository.findByIdAndDeletedIsFalse(tenantId).get().getUsers().stream()
                .map(e -> converter.convert(e, UserTo.class)).collect(Collectors.toSet());
    }

    /**
     *
     * @param tenant
     */
    @Override
    @Transactional(readOnly = false)
    public TenantTo modifyTenant(TenantInput tenant) {
        TenantModel target = verifyTenantModel(tenant.getId());
        TenantModel source = converter.convert(tenant, TenantModel.class);
        Utils.copyNotNulls(source, target);
        verifyDependencies(target, tenant);

        return converter.convert(tenantRepository.save(target), TenantTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public TenantTo addProducts(Integer tenantId, List<Integer> productIds) {
        TenantModel tenant = verifyTenantModel(tenantId);
        Set<ProductModel> productsToAdd = new HashSet<>(productRepository.findAllById(productIds));
        Set<ProductModel> productsAuthorized = productAuthorizationRepo
                .findByTenantIdAndFunctionalUnitIsNullAndDeletedIsFalse(tenant.getId()).stream()
                .map(a -> a.getProduct()).collect(toSet());

        Set<ProductAuthorizationModel> authorizations = extractNewProductAuthorizations(tenant,
                productsToAdd, productsAuthorized);
        productAuthorizationRepo.saveAll(authorizations);

        return converter.convert(tenant, TenantTo.class);
    }

    Set<ProductAuthorizationModel> extractNewProductAuthorizations(TenantModel tenant,
            Set<ProductModel> productsToAdd, Set<ProductModel> productsAuthorized) {

        productsToAdd.removeAll(productsAuthorized);

        Set<ProductAuthorizationModel> authorizations = productsToAdd.stream()
                .map(p -> new ProductAuthorizationModel(null, p, tenant, null))
                .collect(Collectors.toSet());

        return authorizations;
    }

    @Override
    @Transactional(readOnly = false)
    public TenantTo removeProducts(Integer tenantId, List<Integer> productIds) {
        TenantModel tenant = verifyTenantModel(tenantId);
        Set<ProductModel> productsToRemove = new HashSet<>(
                productRepository.findAllById(productIds));

        // we remove from all units
        Set<ProductAuthorizationModel> authorizationsToRemove = productAuthorizationRepo
                .findByTenantIdAndDeletedIsFalse(tenant.getId()).stream()
                .filter(a -> productsToRemove.contains(a.getProduct())).collect(toSet());

        productAuthorizationRepo.deleteAll(authorizationsToRemove);

        return converter.convert(tenant, TenantTo.class);
    }

    @Override
    public Set<ProductTo> findProducts(int tenantId) {
        return productAuthorizationRepo
                .findByTenantIdAndFunctionalUnitIsNullAndDeletedIsFalse(tenantId).stream()
                .map(a -> converter.convert(a.getProduct(), ProductTo.class)).collect(toSet());
    }

    @Override
    public List<TenantTo> findByPrintoutTemplate(int templateId) {
        return tenantRepository.findByPrintoutTemplate(templateId).stream()
                .map(t -> converter.convert(t, TenantTo.class)).collect(toList());
    }
    @Override
    public List<TenantTo> findByEmailTemplate(int templateId) {
        return tenantRepository.findByEmailTemplate(templateId).stream()
                .map(t -> converter.convert(t, TenantTo.class)).collect(toList());
    }

    @Override
    public List<TenantTo> findByUser(int userId) {
        return tenantRepository.findByUser(userId).stream()
                .map(t -> converter.convert(t, TenantTo.class)).collect(toList());
    }

    @Override
    public List<TenantTo> findByContact(int contactId) {
        return tenantRepository.findByContact(contactId).stream()
                .map(t -> converter.convert(t, TenantTo.class)).collect(toList());
    }

    @Override
    public List<TenantTo> findByWorkflow(int workflowId) {
        return tenantRepository.findByWorkflow(workflowId).stream()
                .map(t -> converter.convert(t, TenantTo.class)).collect(toList());
    }

    @Override
    public UserTo findCreatedBy(int tenantId) {
        TenantModel tenant = verifyTenantModel(tenantId);
        return userRepository.findById(tenant.getCreatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

    @Override
    public UserTo findUpdatedBy(int tenantId) {
        TenantModel tenant = verifyTenantModel(tenantId);
        if (tenant.getUpdatedBy() == null)
            return null;
        return userRepository.findById(tenant.getUpdatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

}