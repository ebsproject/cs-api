package org.ebs.services;

import static java.time.Instant.now;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.CustomerModel;
import org.ebs.model.OrganizationModel;
import org.ebs.model.ProductAuthorizationModel;
import org.ebs.model.ProductModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.CropRepository;
import org.ebs.model.repos.CustomerRepository;
import org.ebs.model.repos.EmailTemplateRepository;
import org.ebs.model.repos.InstanceRepository;
import org.ebs.model.repos.NumberSequenceRuleRepository;
import org.ebs.model.repos.OrganizationRepository;
import org.ebs.model.repos.PrintoutTemplateRepository;
import org.ebs.model.repos.ProductAuthorizationRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.ProgramRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.InstanceTo;
import org.ebs.services.to.NumberSequenceRuleTo;
import org.ebs.services.to.OrganizationTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.InstanceInput;
import org.ebs.services.to.Input.TenantInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SendToMIServiceImpl implements SendToMIService {

    private final RestTemplate restTemplate;
    private Class responseType;
    private Object uriVariables;

    @Override
    public String sendPayload(String payload) {
        
        restTemplate.postForEntity("http://localhost:8290/createJob", payload, String.class);

        return null;
    }
}
