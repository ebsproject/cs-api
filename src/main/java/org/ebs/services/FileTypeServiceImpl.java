package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.FileTypeModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.repos.FileTypeRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.FileTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class FileTypeServiceImpl implements FileTypeService {
    
    private final ConversionService converter;
    private final FileTypeRepository fileTypeRepository;
    private final WorkflowRepository workflowRepository;

    @Override
    public Optional<FileTypeTo> findFileType(int id) {
        if (id < 1)
            return Optional.empty();

        return fileTypeRepository.findById(id).map(r -> converter.convert(r, FileTypeTo.class));
    }

    @Override
    public Page<FileTypeTo> findFileTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return fileTypeRepository
                .findByCriteria(FileTypeModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, FileTypeTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public FileTypeTo createFileType(FileTypeInput input) {
        
        input.setId(0);

        FileTypeModel model = converter.convert(input, FileTypeModel.class);

        setFileTypeDependencies(model, input);

        model = fileTypeRepository.save(model);

        return converter.convert(model, FileTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public FileTypeTo modifyFileType(FileTypeInput input) {
        
        FileTypeModel target = verifyFileTypeModel(input.getId());

        FileTypeModel source = converter.convert(input, FileTypeModel.class);

        Utils.copyNotNulls(source, target);

        setFileTypeDependencies(target, input);

        return converter.convert(fileTypeRepository.save(target), FileTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteFileType(int id) {

        FileTypeModel model = verifyFileTypeModel(id);

        model.setDeleted(true);

        fileTypeRepository.save(model);

        return id;

    }

    @Override
    public WorkflowTo findWorkflow(int fileTypeId) {
        
        FileTypeModel model = verifyFileTypeModel(fileTypeId);

        if (model.getWorkflow() == null)
            return null;
        
        return converter.convert(model.getWorkflow(), WorkflowTo.class);

    }

    FileTypeModel verifyFileTypeModel(int fileTypeId) {
        return fileTypeRepository.findById(fileTypeId)
            .orElseThrow(() -> new RuntimeException("FileType id " + fileTypeId + " not found"));
    }

    void setFileTypeDependencies(FileTypeModel model, FileTypeInput input) {
        setFileTypeWorkflow(model, input.getWorkflowId());
    }

    void setFileTypeWorkflow(FileTypeModel model, Integer workflowId) {
        if (workflowId != null && workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
    } 

}
