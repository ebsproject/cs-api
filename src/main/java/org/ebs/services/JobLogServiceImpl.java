package org.ebs.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.ebs.model.ContactModel;
import org.ebs.model.JobLogModel;
import org.ebs.model.JobWorkflowModel;
import org.ebs.model.TenantModel;
import org.ebs.model.TranslationModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.JobLogRepository;
import org.ebs.model.repos.JobWorkflowRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.TranslationTo;
import org.ebs.services.to.Input.JobLogInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class JobLogServiceImpl implements JobLogService {

    private final ContactRepository contactRepository;
    private final JobLogRepository jobLogRepository;
    private final JobWorkflowRepository jobWorkflowRepository;
    private final TenantRepository tenantRepository;
    private final TranslationRepository translationRepository;
    private final ConversionService converter;

    JobLogModel verifyJobLog(int jobLogId) {
        return jobLogRepository.findById(jobLogId)
                .orElseThrow(() -> new RuntimeException("Job Log not found"));
    }

    @Override
    public Optional<JobLogTo> findJobLog(int jobLogId) {
        if (jobLogId < 1) {
            return Optional.empty();
        }
        return jobLogRepository.findById(jobLogId).map(r -> converter.convert(r, JobLogTo.class));
    }

    @Override
    public Page<JobLogTo> findJobLogs(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return jobLogRepository.findByCriteria(JobLogModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, JobLogTo.class));
    }

    @Override
    public Set<Contact> findContacts(int jobLogId) {
        JobLogModel jobLog = verifyJobLog(jobLogId);
        
        return jobLog.getContacts().stream().map(r -> {
            return converter.convert(r, Contact.class);
        }).collect(Collectors.toSet());
    }

    @Override
    public JobWorkflowTo findJobWorkflow(int jobLogId) {
        JobLogModel jobLog = verifyJobLog(jobLogId);
        return converter.convert(jobLog.getJobWorkflow(), JobWorkflowTo.class);
    }

    @Override
    public TranslationTo findTranslation(int jobLogId) {
        JobLogModel jobLog = verifyJobLog(jobLogId);
        return converter.convert(jobLog.getTranslation(), TranslationTo.class);
    }

    @Override
    public TenantTo findTenant(int jobLogId) {
        JobLogModel jobLog = verifyJobLog(jobLogId);
        TenantModel tenant = tenantRepository.findById(jobLog.getTenantId()).orElse(null);
        if (tenant == null)
            return null;
        return converter.convert(tenant, TenantTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public JobLogTo createJobLog(JobLogInput input) {

        input.setId(0);

        JobLogModel model = converter.convert(input, JobLogModel.class);

        setJobLogDependencies(model, input);

        return converter.convert(jobLogRepository.save(model), JobLogTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public JobLogTo modifyJobLog(JobLogInput input) {

        JobLogModel target = verifyJobLog(input.getId());

        JobLogModel source = converter.convert(input, JobLogModel.class);

        Utils.copyNotNulls(source, target);

        setJobLogDependencies(target, input);

        return converter.convert(jobLogRepository.save(target), JobLogTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteJobLog(int jobLogId) {

        JobLogModel model = verifyJobLog(jobLogId);

        model.setDeleted(true);

        jobLogRepository.save(model);

        return jobLogId;

    }

    @Override
    @Transactional(readOnly = false)
    public void clearJobLogs() {
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -30);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        log.info("clearing all notifications before - " + sdf.format(calendar.getTime()));
        jobLogRepository.findByStartTimeBefore(calendar.getTime()).forEach(j -> {
            jobLogRepository.delete(j);
        });
    }

    public void setJobLogDependencies(JobLogModel model, JobLogInput input) {
        setJobLogContacts(model, input);
        setJobLogDateTimes(model, input);
        setJobLogJobWorkflow(model, input);
        setJobLogTenant(model, input);
        setJobLogTranslation(model, input);
    }

    public void setJobLogContacts(JobLogModel model, JobLogInput input) {

        if (input.getContactIds() == null)
            return;

        Set<ContactModel> contacts = new HashSet<>();
        input.getContactIds().forEach(contactId -> {
            ContactModel contact = contactRepository.findByIdAndDeletedIsFalse(contactId)
                .orElseThrow(() -> new RuntimeException("Contact" + contactId + " not found"));
            contacts.add(contact);
        });

        model.setContacts(contacts);

    }

    public void setJobLogDateTimes(JobLogModel model, JobLogInput input) {

        Pattern dateTimePattern = Pattern.compile("([0-9]){4}-([0-9]){2}-([0-9]){2} ([0-9]){2}:([0-9]){2}:([0-9]){2}");

        if (input.getStartTime() != null) {
            Matcher match = dateTimePattern.matcher(input.getStartTime());

            if (match.matches()) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC-5:00"));
                try {
                    Date startDate = formatter.parse(input.getStartTime());
                    model.setStartTime(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Error parsing start date");
                }
            }
        }
        if (input.getEndTime() != null) {
            Matcher match = dateTimePattern.matcher(input.getEndTime());

            if (match.matches()) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC-5:00"));
                try {
                    Date endDate = formatter.parse(input.getEndTime());
                    model.setEndTime(endDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                    throw new RuntimeException("Error parsing end date");
                }
            }
        }
    }

    public void setJobLogJobWorkflow(JobLogModel model, JobLogInput input) {
        if (input.getJobWorkflowId() == null)
            throw new RuntimeException("A job workflow was not provided for the job log");
        JobWorkflowModel jobWorkflow = jobWorkflowRepository.findById(input.getJobWorkflowId())
            .orElseThrow(() -> new RuntimeException("Job workflow " + input.getJobWorkflowId() + " not found"));
        model.setJobWorkflow(jobWorkflow);
    }

    public void setJobLogTenant(JobLogModel model, JobLogInput input) {
        if (input.getTenantId() == null)
            throw new RuntimeException("A tenant was not provided for the job log");
        tenantRepository.findById(input.getTenantId())
            .orElseThrow(() -> new RuntimeException("Tenant " + input.getTenantId() + " not found"));
        model.setTenantId(input.getTenantId());
    }

    public void setJobLogTranslation(JobLogModel model, JobLogInput input) {
        if (input.getTranslationId() == null)
            return;
        TranslationModel translation = translationRepository.findById(input.getTranslationId())
            .orElseThrow(() -> new RuntimeException("Translation " + input.getTranslationId() + " not found"));
        model.setTranslation(translation);
    }
}
