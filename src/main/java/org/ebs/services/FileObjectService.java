package org.ebs.services;

import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.Input.FileObjectInput;

public interface FileObjectService {
    
    public FileObjectTo modifyFileObject(FileObjectInput input);


}
