package org.ebs.services;

import org.ebs.model.RoleModel;
import org.ebs.model.SecurityRuleModel;
import org.ebs.model.SecurityRuleRoleModel;
import org.ebs.model.repos.RoleRepository;
import org.ebs.model.repos.SecurityRuleRepository;
import org.ebs.model.repos.SecurityRuleRoleRepository;
import org.ebs.services.to.SecurityRuleRoleTo;
import org.ebs.services.to.Input.SecurityRuleRoleInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class SecurityRuleRoleServiceImpl implements SecurityRuleRoleService {

    private final SecurityRuleRoleRepository securityRuleRoleRepository;
    private final RoleRepository roleRepository;
    private final SecurityRuleRepository securityRuleRepository;
    private final ConversionService converter;

    @Override
    @Transactional(readOnly = false)
    public SecurityRuleRoleTo createSecurityRuleRole(SecurityRuleRoleInput securityRuleRoleInput) {

        SecurityRuleRoleModel model = converter.convert(securityRuleRoleInput, SecurityRuleRoleModel.class);

        verifyDependenciesSecurityRuleRole(model, securityRuleRoleInput);

        verifySecurityRuleRole(model);

        model = securityRuleRoleRepository.save(model);
        return converter.convert(model, SecurityRuleRoleTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public SecurityRuleRoleTo modifySecurityRuleRole(SecurityRuleRoleInput securityRuleRoleInput) {
        return null;

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteSecurityRuleRole(Integer ruleId) {

        if (!securityRuleRoleRepository.findById(ruleId)
                .isPresent())
            throw new RuntimeException("Role-SecurityRule relationship not found");
        else
            securityRuleRoleRepository.deleteById(ruleId);
        return ruleId;

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteSecurityRuleRoleByRoleId(Integer roleId) {
        var response = securityRuleRoleRepository.findByRoleId(roleId);
        if (response.size() > 0) {
            for (SecurityRuleRoleModel securityRuleRoleModel : response) {
                securityRuleRoleRepository.deleteById(securityRuleRoleModel.getId());
            }
        }
        return roleId;
    }

    private void verifySecurityRuleRole(SecurityRuleRoleModel securityRuleRole) {
     var response = securityRuleRoleRepository.findByRoleIdAndRuleId(securityRuleRole.getRole().getId(), securityRuleRole.getRule().getId());
              System.out.println(response);
                    if(response.size() > 0)
                   throw new RuntimeException("Role-SecurityRule relationship already exists");
            
    }

    private void verifyDependenciesSecurityRuleRole(SecurityRuleRoleModel model, SecurityRuleRoleInput input) {
        RoleModel role = roleRepository.findById(input.getRole().getId())
                .orElseThrow(() -> new RuntimeException("Role not found"));
        SecurityRuleModel securityRule = securityRuleRepository.findById(input.getRule().getId())
                .orElseThrow(() -> new RuntimeException("SecurityRule not found"));
        model.setRole(role);
        model.setRule(securityRule);
    }

}
