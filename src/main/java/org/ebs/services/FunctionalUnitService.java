package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.model.FunctionalUnitModel;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.services.to.Input.FunctionalUnitInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface FunctionalUnitService {

    Optional<FunctionalUnitTo> findFunctionalUnit (int functionalUnitId);

    Page<FunctionalUnitTo> findFunctionalUnitList(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    FunctionalUnitTo createFunctionalUnit( FunctionalUnitInput functionalUnit);

    FunctionalUnitTo modifyFunctionalUnit( FunctionalUnitInput functionalUnit);

    int removeFunctionalUnit( int functionalUnitId);

    Set<FunctionalUnitTo> findByParent(int parentFuncUnitId);

    FunctionalUnitTo addContacts(FunctionalUnitModel unit, List<Integer> contactIds);

    FunctionalUnitTo removeContacts(Integer functionalUnitId, List<Integer> contactIds);

}