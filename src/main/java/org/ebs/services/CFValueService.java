package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.CFValueInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface CFValueService {

    public Optional<CFValueTo> findCFValue(int id);

    public Page<CFValueTo> findCFValues(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    public CFValueTo createCFValue(CFValueInput input);

    public Integer createCFValues(List<CFValueInput> input);
    
    public Integer modifyCFValues(List<CFValueInput> input);

    public CFValueTo modifyCFValue(CFValueInput input);

    public int deleteCFValue(int id);

    public NodeCFTo findNodeCF(int cfValueId);

    public EventTo findEvent(int cfValueId);

    public TenantTo findTenant(int cfValueId);
    
}