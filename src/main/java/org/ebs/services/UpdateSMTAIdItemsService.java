package org.ebs.services;

public interface UpdateSMTAIdItemsService {
    public String updateSmtaIdItems(Integer id, String smtaId, String mtaStatus);
}
