package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.StatusModel;
import org.ebs.model.StatusTypeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.model.repos.StatusRepository;
import org.ebs.model.repos.StatusTypeRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowInstanceRepository;
import org.ebs.services.to.StatusTo;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.StatusInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class StatusServiceImpl implements StatusService {
    
    private final ConversionService converter;
    private final StatusRepository statusRepository;
    private final StatusTypeRepository statusTypeRepository;
    private final WorkflowInstanceRepository workflowInstanceRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<StatusTo> findStatus(int id) {
        if (id < 1)
            return Optional.empty();

        return statusRepository.findById(id).map(r -> converter.convert(r, StatusTo.class));
    }

    @Override
    public Page<StatusTo> findStatuses(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return statusRepository
                .findByCriteria(StatusModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, StatusTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public StatusTo createStatus(StatusInput input) {
        
        input.setId(0);

        StatusModel model = converter.convert(input, StatusModel.class);

        setStatusDependencies(model, input);

        model = statusRepository.save(model);

        return converter.convert(model, StatusTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public StatusTo modifyStatus(StatusInput input) {
        
        StatusModel target = verifyStatusModel(input.getId());

        StatusModel source = converter.convert(input, StatusModel.class);

        Utils.copyNotNulls(source, target);

        setStatusDependencies(target, input);

        return converter.convert(statusRepository.save(target), StatusTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteStatus(int id) {

        StatusModel model = verifyStatusModel(id);

        model.setDeleted(true);

        statusRepository.save(model);

        return id;

    }

    @Override
    public StatusTypeTo findStatusType(int statusId) {
        
        StatusModel model = verifyStatusModel(statusId);

        if (model.getStatusType() == null)
            return null;
        
        return converter.convert(model.getStatusType(), StatusTypeTo.class);

    }

    @Override
    public WorkflowInstanceTo findWorkflowInstance(int statusId) {
        
        StatusModel model = verifyStatusModel(statusId);

        if (model.getWorkflowInstance() == null)
            return null;
        
        return converter.convert(model.getWorkflowInstance(), WorkflowInstanceTo.class);

    }

    @Override
    public TenantTo findTenant(int statusId) {
        
        StatusModel model = verifyStatusModel(statusId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    StatusModel verifyStatusModel(int statusId) {
        return statusRepository.findById(statusId)
            .orElseThrow(() -> new RuntimeException("Status id " + statusId + " not found"));
    }

    void setStatusDependencies(StatusModel model, StatusInput input) {
        setStatusStatusType(model, input.getStatusTypeId());
        setStatusWorkflowInstance(model, input.getWorkflowInstanceId());
        setStatusTenant(model, input.getTenantId());
    }

    void setStatusStatusType(StatusModel model, int statusTypeId) {
        if (statusTypeId > 0) {
            StatusTypeModel statusType = statusTypeRepository.findById(statusTypeId)
                .orElseThrow(() -> new RuntimeException("StatusType id " + statusTypeId + " not found"));
            model.setStatusType(statusType);
        }
    } 

    void setStatusWorkflowInstance(StatusModel model, Integer workflowInstanceId) {
        if (workflowInstanceId != null && workflowInstanceId > 0) {
            WorkflowInstanceModel workflowInstance = workflowInstanceRepository.findById(workflowInstanceId)
                .orElseThrow(() -> new RuntimeException("WorkflowInstance id " + workflowInstanceId + " not found"));
            model.setWorkflowInstance(workflowInstance);
        }
    } 

    void setStatusTenant(StatusModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    } 

}
