///////////////////////////////////////////////////////////
//  DomainInstanceServiceImpl.java
//  Macromedia ActionScript Implementation of the Class DomainInstanceServiceImpl
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:54 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.DomainInstanceModel;
import org.ebs.model.DomainModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.DomainInstanceRepository;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.DomainInstanceTo;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.InstanceTo;
import org.ebs.services.to.Input.DomainInstanceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:54 AM
 */
@Service @Transactional(readOnly = true)
@RequiredArgsConstructor
class DomainInstanceServiceImpl implements DomainInstanceService {

	private final DomainInstanceRepository domaininstanceRepository;
	private final ConversionService converter;
	private final TenantRepository tenantRepository;
	private final DomainRepository domainRepository;

	/**
	 *
	 * @param input
	 */
	@Override @Transactional(readOnly = false)
	public DomainInstanceTo createDomainInstance(DomainInstanceInput input){
		input.setId(0);
		input.setMfe(checkIsMFE(input));
		DomainInstanceModel model = converter.convert(input, DomainInstanceModel.class);
		verifyDependencies(model, input);

		model = domaininstanceRepository.save(model);
		return converter.convert(model, DomainInstanceTo.class);
	}

	boolean checkIsMFE(DomainInstanceInput input) {
		switch( input.getDomainId() ) {
			case DomainModel.CORE_SYSTEM_ID: return true;
			case DomainModel.CORE_BREEDING_ID: return false;
			default: return input.isMfe();
		}
	}

	void verifyDependencies(DomainInstanceModel model, DomainInstanceInput input) {
		TenantModel tenantModel = tenantRepository.findByIdAndDeletedIsFalse(input.getTenantId())
			.orElseThrow(() -> new RuntimeException("Tenant not found"));
		model.setTenantId(tenantModel.getId());
		model.setInstance(tenantModel.getInstances().iterator().next());

		DomainModel domainModel = domainRepository.findByIdAndDeletedIsFalse(input.getDomainId())
			.orElseThrow(() -> new RuntimeException("Domain not found"));
		model.setDomain(domainModel);

		domaininstanceRepository.findByInstanceIdAndDomainIdAndDeletedIsFalse(model.getInstance().getId(), model.getDomain().getId())
			.ifPresent(d -> {throw new RuntimeException("Domain '" + d.getDomain().getName() + "' already exists for this tenant");});
	}

	@Override @Transactional(readOnly = false)
	public void addDefaultDomainInstances(int tenantId) {
		DomainInstanceInput coreBreeding = new DomainInstanceInput(0, 0, DomainModel.CORE_BREEDING_ID, tenantId, "", "", false)
			,coreSystem = new DomainInstanceInput(0, 0, DomainModel.CORE_SYSTEM_ID, tenantId, "", "", false);
		createDomainInstance(coreBreeding);
		createDomainInstance(coreSystem);
	}

	/**
	 *
	 * @param domainInstanceId
	 */
	@Override @Transactional(readOnly = false)
	public int deleteDomainInstance(int domainInstanceId){
		DomainInstanceModel domaininstance = verifyDomainInstanceModel(domainInstanceId);

		if(isCoreDomain(domaininstance.getDomain())) throw new RuntimeException("Cannot delete a Core Domain");

		domaininstance.setDeleted(true);
		domaininstanceRepository.save(domaininstance);
		return domainInstanceId;
	}

	boolean isCoreDomain(DomainModel model){
		switch (model.getId()) {
			case DomainModel.CORE_BREEDING_ID:
			case DomainModel.CORE_SYSTEM_ID: return true;
			default: return false;
		}

	}

	DomainInstanceModel verifyDomainInstanceModel(int domainInstanceId) {
		return domaininstanceRepository.findByIdAndDeletedIsFalse(domainInstanceId)
			.orElseThrow(() -> new RuntimeException("DomainInstance not found"));
	}

	/**
	 *
	 * @param domaininstanceId
	 */
	public Optional<DomainTo> findDomain(int domaininstanceId){
		return domaininstanceRepository.findByIdAndDeletedIsFalse(domaininstanceId).map(r -> converter.convert(r.getDomain(),DomainTo.class));
	}

	/**
	 *
	 * @param domainInstanceId
	 */
	@Override
	public Optional<DomainInstanceTo> findDomainInstance(int domainInstanceId){
		if(domainInstanceId <1)
		 {return Optional.empty();}
		 return domaininstanceRepository.findByIdAndDeletedIsFalse(domainInstanceId).map(r -> converter.convert(r,DomainInstanceTo.class));
	}

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	@Override
	public Page<DomainInstanceTo> findDomainInstances(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters){
		return domaininstanceRepository.findByCriteria(DomainInstanceModel.class,filters,sort,page,disjunctionFilters).map(r -> converter.convert(r,DomainInstanceTo.class));
	}

	/**
	 *
	 * @param domaininstanceId
	 */
	public Optional<InstanceTo> findInstance(int domaininstanceId){
		return domaininstanceRepository.findByIdAndDeletedIsFalse(domaininstanceId).map(r -> converter.convert(r.getInstance(),InstanceTo.class));
	}

	/**
	 *
	 * @param domainInstance
	 */
	@Override @Transactional(readOnly = false)
	public DomainInstanceTo modifyDomainInstance(DomainInstanceInput domainInstance){
		DomainInstanceModel model = verifyDomainInstanceModel(domainInstance.getId());
		// using copyNotNulls modifies values for primitives (as they have default value when not set)
		model.setContext(domainInstance.getContext());
		model.setSgContext(domainInstance.getSgContext());
		return converter.convert(domaininstanceRepository.save(model), DomainInstanceTo.class);
	}

}