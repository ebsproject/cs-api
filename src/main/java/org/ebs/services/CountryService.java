package org.ebs.services;

import java.util.List;

import org.ebs.services.to.Country;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface CountryService {

    Country findByAddress(int addressId);

    Country findById(int countryId);

    Page<Country> findCountrys(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

}