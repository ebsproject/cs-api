package org.ebs.services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.ebs.model.ProductFunctionModel;
import org.ebs.model.RoleModel;
import org.ebs.model.RoleProductModel;
import org.ebs.model.repos.ProductFunctionRepository;
import org.ebs.model.repos.RoleProductRepository;
import org.ebs.model.repos.RoleRepository;
import org.ebs.services.to.RoleProductTo;
import org.ebs.services.to.Input.ProductFunctionInput;
import org.ebs.services.to.Input.RoleProductInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class RoleProductServiceImpl implements RoleProductService {

    private final RoleProductRepository roleProductRepository;
    private final RoleRepository roleRepository;
    private final ProductFunctionRepository productFunctionRepository;
    private final ConversionService converter;

    @Override
    @Transactional(readOnly = false)
    public RoleProductTo createRoleProduct(RoleProductInput roleProductInput) {

        RoleModel roleModel = roleRepository.findById(roleProductInput.getRole().getId())
            .orElseThrow(() -> new RuntimeException("Role not found"));
        
        Set<Integer> inputProductFunctions = new HashSet<>();

        roleProductInput.getProductFunction().forEach(pfi -> {
            inputProductFunctions.add(pfi.getId());
        });

        for (Integer pfId : inputProductFunctions) {

            ProductFunctionModel pf = productFunctionRepository.findById(pfId)
                .orElseThrow(() -> new RuntimeException("Product function not found"));

            RoleProductModel rpf = roleProductRepository.findByRoleAndProductFunction(roleModel, pf).orElse(null);

            if (rpf == null) {
                rpf = new RoleProductModel(0, roleModel, pf);
            } else if (rpf.isDeleted()) {
                rpf.setDeleted(false);
            }

            roleProductRepository.save(rpf);

        }

        verifyDuplicateRoleProductFunctions(roleProductInput.getRole().getId());
        
        List<RoleProductModel> model = roleProductRepository.findByRole(roleModel);

        return converter.convert(model.get(0), RoleProductTo.class);
    }

    void verifyDuplicateRoleProductFunctions(Integer roleId) {
        RoleModel roleModel = roleRepository.findById(roleId)
            .orElseThrow(() -> new RuntimeException("Role not found"));
        Set<ProductFunctionModel> productFunctions = roleModel.getProductfunctions();
        roleRepository.findByProductfunctionsInAndDeletedIsFalseAndProductfunctionsDeletedIsFalse(productFunctions)
            .stream().forEach(r -> {
                if (r.getId() != roleId 
                    && r.getProductfunctions().size() == productFunctions.size()) {
                    if (r.getProductfunctions().equals(productFunctions)) {
                        throw new RuntimeException("Role " + r.getName() + " has the same permissions definition");
                    }
                }
            });
    }

    void verifyRoleProduct(RoleProductModel roleProduct) {
        roleProductRepository.findByRoleAndProductFunction(roleProduct.getRole(), roleProduct.getProductFunction())
                .ifPresent(r -> {
                    throw new RuntimeException("Role Product relationship already exists");
                });
    }

    void verifyDependenciesRoleProduct(ProductFunctionInput input, RoleProductModel model) {
        ProductFunctionModel productFunction = productFunctionRepository.findById(input.getId())
                .orElseThrow(() -> new RuntimeException("Product function not found"));
        model.setProductFunction(productFunction);
    }

    @Override
    @Transactional(readOnly = false)
    public String deleteRoleProduct(Integer roleId, Set<Integer> productFunctionIds) {
        int deleteRowsCount = 0;
        for (Integer productFunctionId : productFunctionIds) {
            RoleProductModel roleProductModel = roleProductRepository.findByRoleIdAndProductFunctionId(roleId, productFunctionId).orElse(null);
            if (roleProductModel != null) {
                roleProductModel.setDeleted(true);
                roleProductRepository.save(roleProductModel);
                deleteRowsCount = deleteRowsCount + 1;
            }
        }
        verifyDuplicateRoleProductFunctions(roleId);
        String messageResult = "Number of Role-Product Function deleted : " + deleteRowsCount;
        return messageResult;
    }

}
