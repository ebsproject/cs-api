package org.ebs.services;

import org.springframework.web.multipart.MultipartFile;

public interface ImportService {
    public String importShipmentItems(MultipartFile file, String entity, int shipmentId) throws Exception;
    public String importSeeds(MultipartFile file, String entity, int shipmentId) throws Exception;
    public String importPersons(MultipartFile file);
    public String importInstitutions(MultipartFile file);
}
