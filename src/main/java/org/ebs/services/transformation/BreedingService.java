package org.ebs.services.transformation;

import java.io.IOException;
import java.util.Map;

import org.ebs.model.services.BreedingResponse;
import org.springframework.web.reactive.function.client.WebClient;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class BreedingService {
    private WebClient webClient;

    public BreedingService(WebClient webClient) {
        this.webClient = webClient;
    }

    public BreedingResponse getAnyPageWithPagination(String uri, int page, BreedingResponse response) {
        if (uri.contains("page=")) {
            uri = uri.replaceAll("page=\\d+", "page=" + page);
        } else {
            uri = uri + "&page=" + page;
        }
        try {
            String responseString = webClient
                    .get()
                    .uri(uri)
                    .retrieve()
                    .bodyToMono(String.class)
                    .block();

            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readValue(responseString, JsonNode.class);
            // response.setPage(Integer.parseInt(
            //         jsonNode
            //                 .get("metadata")
            //                 .get("pagination")
            //                 .get("currentPage").asText()));
            response.setTotalPage(Integer.parseInt(
                    jsonNode
                            .get("metadata")
                            .get("pagination")
                            .get("totalPages").asText()));
            ArrayNode arrayNode = (ArrayNode) (jsonNode.get("result").get("data"));
            for (JsonNode item : arrayNode) {
                Map<String, Object> result = objectMapper.convertValue(item, new TypeReference<Map<String, Object>>() {
                });
                response.AddData(result);
            }
        } catch (IOException e) {
            response.setError(e.toString());
        } catch (Exception ex) {
            response.setError(ex.toString());
        }
        return response;
    }

}
