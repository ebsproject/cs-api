package org.ebs.services;

import static java.util.stream.Collectors.toList;
import static org.ebs.util.Utils.copyNotNulls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.ebs.model.ShipmentItemModel;
import org.ebs.model.ShipmentModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.ShipmentItemRepository;
import org.ebs.model.repos.ShipmentRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.ShipItemInput;
import org.ebs.services.to.Input.ShipmentItemInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ShipmentItemServiceImpl implements ShipmentItemService {

    private final ConversionService converter;
    private final ShipmentRepository shipmentRepository;
    private final ShipmentItemRepository shipmentItemRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<ShipmentItemTo> findShipmentItem(int id) {
        if (id < 1)
            return Optional.empty();

        return shipmentItemRepository.findById(id).map(r -> converter.convert(r, ShipmentItemTo.class));
    }

    @Override
    public Page<ShipmentItemTo> findShipmentItems(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return shipmentItemRepository
                .findByCriteria(ShipmentItemModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ShipmentItemTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public ShipmentItemTo createShipmentItem(ShipmentItemInput input) {

        input.setId(0);

        ShipmentItemModel model = converter.convert(input, ShipmentItemModel.class);

        setShipmentItemDependencies(model, input);

        model = shipmentItemRepository.save(model);

        return converter.convert(model, ShipmentItemTo.class);

    }

    @Override
    @Transactional(readOnly = false , timeout = 60000)
    public int createShipmentItems(List<ShipmentItemInput> input) {
        List<ShipmentItemModel> shipmentItemModel = new ArrayList<>();
        input.forEach(item -> {
            item.setId(0);
            ShipmentItemModel model = converter.convert(item, ShipmentItemModel.class);
            setShipmentItemDependencies(model, item);
            shipmentItemModel.add(model);
        });
        shipmentItemRepository.saveAll(shipmentItemModel);
        return input.size();
    }

    @Override
    @Transactional(readOnly = false)
    public ShipmentItemTo modifyShipmentItem(ShipmentItemInput input) {

        ShipmentItemModel target = verifyShipmentItem(input.getId());

        ShipmentItemModel source = converter.convert(input, ShipmentItemModel.class);

        Utils.copyNotNulls(source, target);

        setShipmentItemDependencies(target, input);

        return converter.convert(shipmentItemRepository.save(target), ShipmentItemTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteShipmentItem(int id) {

        ShipmentItemModel model = verifyShipmentItem(id);

        model.setDeleted(true);

        shipmentItemRepository.save(model);

        return id;

    }

    @Override
    public ShipmentTo findShipment(int shipmentItemId) {

        ShipmentItemModel model = verifyShipmentItem(shipmentItemId);

        if (model.getShipment() == null)
            return null;

        return converter.convert(model.getShipment(), ShipmentTo.class);

    }

    @Override
    public TenantTo findTenant(int shipmentItemId) {

        ShipmentItemModel model = verifyShipmentItem(shipmentItemId);

        if (model.getTenant() == null)
            return null;

        return converter.convert(model.getTenant(), TenantTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int modifyShipmentItems(Integer shipmentId, List<ShipItemInput> input) {
        final List<ShipmentItemModel> target = new ArrayList<>();
        final Map<Integer,ShipmentItemModel> targetMap = new HashMap<>();

        if (shipmentId != null) {
            ShipmentModel ship = verifyShipment(shipmentId);
            target.addAll(ship.getItems().stream().filter(si -> !si.isDeleted()).collect(toList()));
            target.sort((a, b)-> a.getNumber() - b.getNumber());
            ShipmentItemModel template = converter.convert(input.get(0), ShipmentItemModel.class);
            template.setId(0);
            for (int i = 0; i < target.size(); i++) {
                int id = target.get(i).getId();
                copyNotNulls(template, target.get(i));
                target.get(i).setId(id);
                target.get(i).setNumber(i + 1);
            }
           
        } else {
            target.addAll(verifyShipmentItems(input));
            target.forEach(t -> targetMap.put(t.getId(), t));
            input.forEach(
                    si -> {
                    ShipmentItemModel sim = converter.convert(si, ShipmentItemModel.class);
                    copyNotNulls(sim, targetMap.get(si.getId()));
                });
        }
        
        return target.size();

    }

    ShipmentItemModel verifyShipmentItem(int shipmentItemId) {
        return shipmentItemRepository.findById(shipmentItemId)
                .orElseThrow(() -> new RuntimeException("Shipment item " + shipmentItemId + " not found"));
    }

    private List<ShipmentItemModel> verifyShipmentItems(List<ShipItemInput> input) {
            return verifyShipmentItemsById(input.stream()
                .map(ShipItemInput::getId)
                .collect(toList()));
    }

    private List<ShipmentItemModel> verifyShipmentItemsById(List<Integer> ids) {
        List<ShipmentItemModel> result = shipmentItemRepository
            .findAllById(ids);

            if (result.size() != ids.size()) {
                throw new RuntimeException( "Invalid Shipment Item Id(s)");
            }
            return result;
    }

    void setShipmentItemDependencies(ShipmentItemModel model, ShipmentItemInput input) {
        setShipmentItemShipment(model, input.getShipmentId());
        setShipmentItemTenant(model, input.getTenantId());
    }

    void setShipmentItemShipment(ShipmentItemModel model, Integer shipmentId) {
        if (shipmentId != null) {
            model.setShipment(verifyShipment(shipmentId));
        }
    }

    ShipmentModel verifyShipment(Integer shipmentId) {
        return shipmentRepository.findById(shipmentId).orElseThrow(
                () -> new RuntimeException("Shipment " + shipmentId + " not found"));
    }

    void setShipmentItemTenant(ShipmentItemModel model, Integer tenantId) {
        if (tenantId != null) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteShipmentItems(Integer shipmentId, List<Integer> input) {
        List<ShipmentItemModel> items = null;        
        if (shipmentId != null) {
            ShipmentModel ship = verifyShipment(shipmentId);
            items = ship.getItems().stream().filter(si -> !si.isDeleted()).collect(toList());
        } else {
            items = verifyShipmentItemsById(input);
        }
        
        items.forEach(si -> si.setDeleted(true));
        return items.size();

    }
}
