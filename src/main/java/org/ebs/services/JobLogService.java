package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.Contact;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.TranslationTo;
import org.ebs.services.to.Input.JobLogInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface JobLogService {

	public Optional<JobLogTo> findJobLog(int jobLogId);

	public Page<JobLogTo> findJobLogs(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);
	
/* 	public JobLogTo createJobLog(JobLogInput jobLogInput);

	public JobLogTo modifyJobLog(JobLogInput jobLogInput);

	public int deleteJobLog(int jobLogId); */

	public Set<Contact> findContacts(int jobLogId);

	public JobWorkflowTo findJobWorkflow(int jobLogId);

	public TranslationTo findTranslation(int jobLogId);

	public TenantTo findTenant(int jobLogId);

	public JobLogTo createJobLog(JobLogInput input);

	public JobLogTo modifyJobLog(JobLogInput input);

	public int deleteJobLog(int jobLogId);

	public void clearJobLogs();

}