package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.WorkflowViewTypeRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.WorkflowViewTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class WorkflowViewTypeServiceImpl implements WorkflowViewTypeService {
    
    private final ConversionService converter;
    private final WorkflowViewTypeRepository workflowViewTypeRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<WorkflowViewTypeTo> findWorkflowViewType(int id) {
        if (id < 1)
            return Optional.empty();

        return workflowViewTypeRepository.findById(id).map(r -> converter.convert(r, WorkflowViewTypeTo.class));
    }

    @Override
    public Page<WorkflowViewTypeTo> findWorkflowViewTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return workflowViewTypeRepository
                .findByCriteria(WorkflowViewTypeModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, WorkflowViewTypeTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public WorkflowViewTypeTo createWorkflowViewType(WorkflowViewTypeInput input) {
        
        input.setId(0);

        WorkflowViewTypeModel model = converter.convert(input, WorkflowViewTypeModel.class);

        setWorkflowViewTypeDependencies(model, input);

        model = workflowViewTypeRepository.save(model);

        return converter.convert(model, WorkflowViewTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public WorkflowViewTypeTo modifyWorkflowViewType(WorkflowViewTypeInput input) {
        
        WorkflowViewTypeModel target = verifyWorkflowViewTypeModel(input.getId());

        WorkflowViewTypeModel source = converter.convert(input, WorkflowViewTypeModel.class);

        Utils.copyNotNulls(source, target);

        setWorkflowViewTypeDependencies(target, input);

        return converter.convert(workflowViewTypeRepository.save(target), WorkflowViewTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteWorkflowViewType(int id) {

        WorkflowViewTypeModel model = verifyWorkflowViewTypeModel(id);

        model.setDeleted(true);

        workflowViewTypeRepository.save(model);

        return id;

    }

    @Override
    public TenantTo findTenant(int workflowViewTypeId) {
        
        WorkflowViewTypeModel model = verifyWorkflowViewTypeModel(workflowViewTypeId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    WorkflowViewTypeModel verifyWorkflowViewTypeModel(int workflowViewTypeId) {
        return workflowViewTypeRepository.findById(workflowViewTypeId)
            .orElseThrow(() -> new RuntimeException("WorkflowViewType id " + workflowViewTypeId + " not found"));
    }

    void setWorkflowViewTypeDependencies(WorkflowViewTypeModel model, WorkflowViewTypeInput input) {
        setWorkflowViewTypeTenant(model, input.getTenantId());
    }

    void setWorkflowViewTypeTenant(WorkflowViewTypeModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    } 

}
