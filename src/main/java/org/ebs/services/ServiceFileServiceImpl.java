package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.ebs.model.FileObjectModel;
import org.ebs.model.FileTypeModel;
import org.ebs.model.ServiceFileModel;
import org.ebs.model.ServiceModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.FileObjectRepository;
import org.ebs.model.repos.FileTypeRepository;
import org.ebs.model.repos.ServiceFileRepository;
import org.ebs.model.repos.ServiceRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.FileTypeTo;
import org.ebs.services.to.ServiceFileTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.ServiceFileInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ServiceFileServiceImpl implements ServiceFileService {

    private final ConversionService converter;
    private final FileObjectRepository fileObjectRepository;
    private final FileTypeRepository fileTypeRepository;
    private final ServiceRepository serviceRepository;
    private final ServiceFileRepository serviceFileRepository;
    private final TenantRepository tenantRepository;
    private final UserRepository userRepository;

    @Override
    public Optional<ServiceFileTo> findServiceFile(int id) {
        if (id < 1)
            return Optional.empty();

        return serviceFileRepository.findById(id).map(r -> converter.convert(r, ServiceFileTo.class));
    }

    @Override
    public Page<ServiceFileTo> findServiceFiles(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return serviceFileRepository
                .findByCriteria(ServiceFileModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ServiceFileTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public ServiceFileTo createServiceFile(ServiceFileInput input) {

        input.setId(0);

        ServiceFileModel model = converter.convert(input, ServiceFileModel.class);

        setFilesDependencies(model, input);

        model = serviceFileRepository.save(model);

        return converter.convert(model, ServiceFileTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public ServiceFileTo modifyServiceFile(ServiceFileInput input) {

        ServiceFileModel target = verifyFiles(input.getId());

        ServiceFileModel source = converter.convert(input, ServiceFileModel.class);

        Utils.copyNotNulls(source, target);

        setFilesDependencies(target, input);

        return converter.convert(serviceFileRepository.save(target), ServiceFileTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteServiceFile(int id) {

        ServiceFileModel model = verifyFiles(id);

        model.setDeleted(true);

        serviceFileRepository.save(model);

        return id;

    }

    @Override
    public ServiceTo findService(int filesId) {

        ServiceFileModel model = verifyFiles(filesId);

        if (model.getService() == null)
            return null;

        return converter.convert(model.getService(), ServiceTo.class);

    }

    @Override
    public FileObjectTo findFileObject(int serviceFileId) {

        ServiceFileModel model = verifyFiles(serviceFileId);

        if (model.getFile() == null)
            return null;

        return converter.convert(model.getFile(), FileObjectTo.class);

    }

    @Override
    public TenantTo findTenant(int serviceFileId) {

        ServiceFileModel model = verifyFiles(serviceFileId);

        if (model.getTenant() == null)
            return null;

        return converter.convert(model.getTenant(), TenantTo.class);

    }

    @Override
    public FileTypeTo findFileType(int filesId) {

        ServiceFileModel model = verifyFiles(filesId);

        if (model.getFileType() == null)
            return null;

        return converter.convert(model.getFileType(), FileTypeTo.class);

    }

    @Override
    public UserTo findCreatedBy(int serviceFileId) {
        ServiceFileModel sf = verifyFiles(serviceFileId);
        return userRepository.findById(sf.getCreatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

    @Override
    public UserTo findUpdatedBy(int serviceFileId) {
        ServiceFileModel sf = verifyFiles(serviceFileId);
        if (sf.getUpdatedBy() == null)
            return null;
        return userRepository.findById(sf.getUpdatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

    ServiceFileModel verifyFiles(int serviceFileId) {
        return serviceFileRepository.findById(serviceFileId)
                .orElseThrow(
                        () -> new RuntimeException("Shipment File relationship id " + serviceFileId + " not found"));
    }

    void setFilesDependencies(ServiceFileModel model, ServiceFileInput input) {

        setFilesService(model, input.getServiceId());
        setFilesRelationship(model, input.getFileId());
        setFilesTenant(model, input.getTenantId());
        setFilesFileType(model, input.getFileTypeId());
    }

    void setFilesService(ServiceFileModel model, Integer serviceId) {

        if (serviceId != null) {
            ServiceModel service = serviceRepository.findById(serviceId)
                    .orElseThrow(() -> new RuntimeException("Service " + serviceId + " not found"));
            model.setService(service);
        }

    }

    void setFilesRelationship(ServiceFileModel model, UUID fileObjectId) {

        if (fileObjectId != null) {
            FileObjectModel file = fileObjectRepository.findById(fileObjectId)
                    .orElseThrow(() -> new RuntimeException("File " + fileObjectId + " not found"));
            model.setFile(file);
        }

    }

    void setFilesTenant(ServiceFileModel model, Integer tenantId) {
        if (tenantId != null) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

    void setFilesFileType(ServiceFileModel model, Integer fileTypeId) {

        if (fileTypeId != null && fileTypeId > 0) {
            FileTypeModel fileType = fileTypeRepository.findById(fileTypeId)
                    .orElseThrow(() -> new RuntimeException("File Type " + fileTypeId + " not found"));
            model.setFileType(fileType);
        }

    }

}
