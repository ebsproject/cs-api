package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.Contact;
import org.ebs.services.to.Person;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.ServiceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ServiceService {

    public Optional<ServiceTo> findService(int id);

    public Page<ServiceTo> findServices(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    public ServiceTo createService(ServiceInput input);

    public ServiceTo modifyService(ServiceInput input);

    public int deleteService(int id);

    public Person findPerson(int serviceId);

    public WorkflowInstanceTo findWorkflowInstance(int serviceId);

    public TenantTo findTenant(int serviceId);

    public Contact findSender(int serviceId);

    public Contact findRequestor(int serviceId);

    public Contact findRecipient(int serviceId);

    public List<ServiceItemTo> findItems(int serviceId);

    public Contact findProgram(int serviceId);

    public Contact findServiceProvider(int serviceId);

}