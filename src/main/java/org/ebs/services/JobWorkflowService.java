package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.JobTypeTo;
import org.ebs.services.to.JobWorkflowTo;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.TranslationTo;
import org.ebs.services.to.Input.JobWorkflowInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface JobWorkflowService {

	public Optional<JobWorkflowTo> findJobWorkflow(int jobWorkflowId);

	public Page<JobWorkflowTo> findJobWorkflows(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);
	
/* 	public JobWorkflowTo createJobWorkflow(JobWorkflowInput jobWorkflowInput);

	public JobWorkflowTo modifyJobWorkflow(JobWorkflowInput jobWorkflowInput);

	public int deleteJobWorkflow(int jobWorkflowId); */

	public JobTypeTo findJobType(int jobWorkflowId);

	public ProductFunctionTo findProductFunction(int jobWorkflowId);

	public TranslationTo findTranslation(int jobWorkflowId);

	public Set<JobLogTo> findJobLogs(int jobWorkflowId);

	public TenantTo findTenant(int jobWorkflowId);

	public EmailTemplateTo findEmailTemplate(int jobWorkflowId);

	public JobWorkflowTo createJobWorkflow(JobWorkflowInput input);

	public JobWorkflowTo modifyJobWorkflow(JobWorkflowInput input);

	public int deleteJobWorkflow(int jobWorkflowId);

}