package org.ebs.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.ebs.model.HtmlTagModel;
import org.ebs.model.NodeModel;
import org.ebs.model.PhaseModel;
import org.ebs.model.StageModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.NodeRepository;
import org.ebs.model.repos.PhaseRepository;
import org.ebs.model.repos.StageRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowViewTypeRepository;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.Input.StageInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class StageServiceImpl implements StageService {

    private final ConversionService converter;
    private final StageRepository stageRepository;
    private final HtmlTagRepository htmlTagRepository;
    private final PhaseRepository phaseRepository;
    private final WorkflowViewTypeRepository workflowViewTypeRepository;
    private final NodeRepository nodeRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<StageTo> findStage(int id) {
        if (id < 1)
            return Optional.empty();

        return stageRepository.findById(id).map(r -> converter.convert(r, StageTo.class));
    }

    @Override
    public Page<StageTo> findStages(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return stageRepository
                .findByCriteria(StageModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, StageTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public StageTo createStage(StageInput input) {

        // if (!stageRepository.findByDesignRef(input.getDesignRef()).isEmpty())
        //     throw new RuntimeException("Design ref must be unique");
        
        input.setId(0);

        StageModel model = converter.convert(input, StageModel.class);

        setStageDependencies(model, input);

        model = stageRepository.save(model);

        return converter.convert(model, StageTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public StageTo modifyStage(StageInput input) {
        
        StageModel target = verifyStageModel(input.getId());

        // if (stageRepository.findByDesignRef(input.getDesignRef())
        // .stream()
        // .filter(r -> !r.equals(target)).count() > 0)
        //     throw new RuntimeException("Design ref must be unique");

        Utils.copyNotNulls(input, target);

        setStageDependencies(target, input);

        return converter.convert(stageRepository.save(target), StageTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteStage(int id) {

        StageModel model = verifyStageModel(id);

        model.setDeleted(true);

        stageRepository.save(model);

        return id;

    }

    @Override
    public HtmlTagTo findHtmlTag(int stageId) {

        StageModel model = verifyStage(stageId);

        if (model.getHtmlTag() == null)
            return null;
        
        return converter.convert(model.getHtmlTag(), HtmlTagTo.class);

    }

    @Override
    public StageTo findParent(int stageId) {

        StageModel model = verifyStage(stageId);

        if (model.getParent() == null)
            return null;
        
        return converter.convert(model.getParent(), StageTo.class);

    }

    @Override
    public PhaseTo findPhase(int stageId) {

        StageModel model = verifyStage(stageId);

        if (model.getPhase() == null)
            return null;
        
        return converter.convert(model.getPhase(), PhaseTo.class);

    }

    @Override
    public TenantTo findTenant(int stageId) {

        StageModel model = verifyStage(stageId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    StageModel verifyStage(int stageId) {
        return stageRepository.findById(stageId)
            .orElseThrow(() -> new RuntimeException("Stage id " + stageId + " not found"));
    }

    @Override
    public WorkflowViewTypeTo findWorkflowViewType(int stageId) {

        StageModel model = verifyStage(stageId);

        if (model.getWorkflowViewType() == null)
            return null;
        
        return converter.convert(model.getWorkflowViewType(), WorkflowViewTypeTo.class);

    }

    StageModel verifyStageModel(int stageId) {
        return stageRepository.findById(stageId)
            .orElseThrow(() -> new RuntimeException("Stage id " + stageId + " not found"));
    }

    void setStageDependencies(StageModel model, StageInput input) {
        setStageParent(model, input.getParentId());
        setStageHtmlTag(model, input.getHtmlTagId());
        setStagePhase(model, input.getPhaseId());
        setStageWorkflowViewType(model, input.getWorkflowViewTypeId());
        setStageNodes(model, input);
        setStageTenant(model, input.getTenantId());
    }

    void setStageHtmlTag(StageModel model, Integer htmlTagId) {
        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
            model.setHtmlTag(htmlTag);
        }
    } 

    void setStageParent(StageModel model, Integer parentId) {
        if (parentId != null && parentId > 0) {
            StageModel stage = stageRepository.findById(parentId)
                .orElseThrow(() -> new RuntimeException("Parent stage id " + parentId + " not found"));
            model.setParent(stage);
        }
    } 

    void setStagePhase(StageModel model, Integer phaseId) {
        if (phaseId != null && phaseId > 0) {
            PhaseModel phase = phaseRepository.findById(phaseId)
                .orElseThrow(() -> new RuntimeException("Phase id " + phaseId + " not found"));
            model.setPhase(phase);
        }
    } 

    void setStageWorkflowViewType(StageModel model, Integer workflowViewTypeId) {
        if (workflowViewTypeId != null && workflowViewTypeId > 0) {
            WorkflowViewTypeModel workflowViewType = workflowViewTypeRepository.findById(workflowViewTypeId)
                .orElseThrow(() -> new RuntimeException("WorkflowViewType id " + workflowViewTypeId + " not found"));
            model.setWorkflowViewType(workflowViewType);
        }
    } 

    void setStageNodes(StageModel model, StageInput input) {
        if (input.getNodeIds() == null)
            return;

        input.getNodeIds().forEach(nodeId -> {
            NodeModel node = nodeRepository.findByIdAndDeletedIsFalse(nodeId)
                .orElseThrow(() -> new RuntimeException("Node not found"));
            node.setStages(new HashSet<>(Arrays.asList(model)));
            nodeRepository.save(node);
        });

        model.getNodes().forEach(node -> {
            if (!input.getNodeIds().contains(node.getId())) {
                node.getStages().remove(model);
                nodeRepository.save(node);
            }
        });
    }

    void setStageTenant(StageModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    } 
    
}
