package org.ebs.services;

public interface ReorderItemsService {
    public String reorderItems(Integer id, String entity);

}
