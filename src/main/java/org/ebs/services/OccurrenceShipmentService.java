package org.ebs.services;

import java.util.List;

import org.ebs.services.to.Contact;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.Input.OccurrenceShipmentInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;
public interface OccurrenceShipmentService {

    public OccurrenceShipmentTo findOccurrenceShipment(int id);

    public Page<OccurrenceShipmentTo> findOccurrenceShipments(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public OccurrenceShipmentTo createOccurrenceShipment(OccurrenceShipmentInput input);

    public OccurrenceShipmentTo modifyOccurrenceShipment(OccurrenceShipmentInput input);

    public int deleteOccurrenceShipment(int id);

    public ServiceTo findService(int occurrenceShipmentId);

    public Contact findRecipient(int occurrenceShipmentId);

    public List<ServiceItemTo> findItems(int occurrenceShipmentId);

    public int importOccurrenceShipments(MultipartFile file, int serviceId) throws Exception;
    
}