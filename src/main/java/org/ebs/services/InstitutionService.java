package org.ebs.services;

import java.util.List;

import org.ebs.services.to.Contact;
import org.ebs.services.to.CropTo;
import org.ebs.services.to.Institution;
import org.ebs.services.to.UnitTypeTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface InstitutionService {

    Institution findById(int institutionId);

    Page<Institution> findInstitutions(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    Contact findPrincipalContact(int institutionId);

    List<Institution> findInstitutionsByContact(int contactId);

    List<CropTo> findInstitutionCrops(int institutionId);

    UnitTypeTo findUnitType(int institutionId);
    
}
