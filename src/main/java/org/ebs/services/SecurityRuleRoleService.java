package org.ebs.services;

import org.ebs.services.to.SecurityRuleRoleTo;
import org.ebs.services.to.Input.SecurityRuleRoleInput;

public interface SecurityRuleRoleService {

    SecurityRuleRoleTo createSecurityRuleRole(SecurityRuleRoleInput securityRuleRole);
    SecurityRuleRoleTo modifySecurityRuleRole(SecurityRuleRoleInput securityRuleRole);
    int deleteSecurityRuleRole(Integer ruleId);
    int deleteSecurityRuleRoleByRoleId(Integer roleId);

}