package org.ebs.services;

import java.util.List;

import org.ebs.services.to.ProgramCB;
import org.ebs.services.to.UserCB;
import org.ebs.services.to.UserProgramCB;

public interface CoreBreedingService {
    /**
     * retrieves all users from Core Breeding domain
     */
    List<UserCB> findAllUsers();

     /**
     * retrieves all programs from Core Breeding domain
     */
    List<ProgramCB> findAllPrograms();

   /*
     * Retrives all programs belong to an user
     */
    List<UserProgramCB> findProgramsByUserId(int personId);

    /*
     * Retrieves a seed's attributes information
     */
    String findSeedAttributes(int seedId);

    /*
     * Retrieves a list's members information
     */
    String findListMembers(int listId);
}
