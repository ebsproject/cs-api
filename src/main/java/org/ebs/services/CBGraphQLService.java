package org.ebs.services;

import java.util.List;

import org.ebs.services.to.OrganizationCB;
import org.ebs.services.to.PersonSync;
import org.ebs.services.to.ProgramCB;

public interface CBGraphQLService {

    public List<OrganizationCB> findAllOrganizations();

    public List<ProgramCB> findAllPrograms();

    public List<PersonSync> findAllPersons();

    public List<PersonSync> findAllPersonsPaged();

}