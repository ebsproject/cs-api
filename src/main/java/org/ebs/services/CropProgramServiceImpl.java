package org.ebs.services;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.ebs.model.repos.CropProgramRepository;
import org.ebs.services.to.CropProgramTo;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service @Transactional(readOnly = true)
@RequiredArgsConstructor
class CropProgramServiceImpl implements CropProgramService {

	private final CropProgramRepository cropProgramRepository;
	private final ConversionService converter;

	public List<CropProgramTo> findByCrop(int cropId){
			return cropProgramRepository.findByCropIdAndDeletedIsFalse(cropId).stream()
				.map(c -> converter.convert(c, CropProgramTo.class))
				.collect(toList());
	}

}