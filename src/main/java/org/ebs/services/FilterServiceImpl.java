package org.ebs.services;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.ebs.model.DomainModel;
import org.ebs.model.FilterModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.DomainRepository;
import org.ebs.model.repos.FilterRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.FilterTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.FilterEntityInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class FilterServiceImpl implements FilterService {

    private final FilterRepository filterRepository;
    private final TenantRepository tenantRepository;
    private final ConversionService converter;
    private final DomainRepository domainRepository;
    
    @Override
    public Optional<FilterTo> findFilter(int id) {
        if (id < 1)
            return Optional.empty();

        return filterRepository.findById(id).map(r -> converter.convert(r, FilterTo.class));
    }

    @Override
    public Page<FilterTo> findFilters(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
                return filterRepository
                .findByCriteria(FilterModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, FilterTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public FilterTo createFilter(FilterEntityInput input) {
        
        FilterModel model = converter.convert(input, FilterModel.class);

        model.setId(0);

        setFilterDependencies(model, input);

        verifyDomains(input.getDomainIds(), model);

        model = filterRepository.save(model);

        return converter.convert(model, FilterTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public FilterTo modifyFilter(FilterEntityInput input) {

        FilterModel target = filterRepository.findById(input.getId())
            .orElseThrow(() -> new RuntimeException("Filter not found"));

        FilterModel source = converter.convert(input, FilterModel.class);

        Utils.copyNotNulls(source, target);

        setFilterDependencies(target, input);

        saveDomains(input.getDomainIds(), target);

        return converter.convert(filterRepository.save(target), FilterTo.class);
        
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteFilter(int id) {

        FilterModel model = filterRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Filter not found"));

        model.setDeleted(true);

        filterRepository.save(model);

        return id;

    }


    @Override
    public TenantTo findTenant(int filterId) {
        TenantModel tenant = tenantRepository
            .findByFilterId(filterId).orElse(null);
        return converter.convert(tenant, TenantTo.class);
    }

    @Override
    public List<DomainTo> findDomains(int filterId) {
        return domainRepository.findByFiltersIdAndDeletedIsFalse(filterId).stream().map(t -> converter.convert(t, DomainTo.class))
        .collect(toList());
    }

    @Override
    @Transactional(readOnly = false)
    public String createFilterDomain(int filterId, int domainId) {

        String message = new String();

        DomainModel domain = domainRepository.findById(domainId).orElseThrow(() -> new RuntimeException("Domain " + domainId + " not found"));

        FilterModel filter = filterRepository.findById(filterId).orElseThrow(() -> new RuntimeException("Filter " + filterId + " not found"));

        if (filter.getDomains() != null) {
            if (!filter.getDomains().contains(domain)) {
                filter.getDomains().add(domain);
                message = "Relation [Filter - Domain] Created";
            }
            else
                message = "Relation [Filter - Domain] Already Exists";
        } else {
            filter.getDomains().add(domain);
            message = "Relation [Filter - Domain] Created";
        }
        
        filterRepository.save(filter);
        
        return message;
    }

    @Override
    @Transactional(readOnly = false)
    public String deleteFilterDomain(int filterId, int domainId) {
        String message = new String();
        DomainModel domain = domainRepository.findById(domainId).orElseThrow(() -> new RuntimeException("Domain " + domainId + " not found"));
        FilterModel filter = filterRepository.findById(filterId).orElseThrow(() -> new RuntimeException("Filter " + filterId + " not found"));
/*         Optional.ofNullable(filter.getDomains()).ifPresent(currentDomains -> {
            if(currentDomains.contains(domain))
                filter.getDomains().remove(domain);
        }); */

        if (filter.getDomains() != null) {
            if (filter.getDomains().contains(domain)) {
                filter.getDomains().remove(domain);
                message = "Relation [Filter - Domain] Deleted";
            }
            else
                message = "Relation [Filter - Domain] Not Found";
        } else {
            message = "Relation [Filter - Domain] Not Found";
        }

        filterRepository.save(filter);

        return message;
    }

   void setFilterDependencies(FilterModel model, FilterEntityInput input) {

        setTenant(model, input.getTenantId());
   }


   void setTenant(FilterModel model, Integer tenantId) {
        if (tenantId != null) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant not found"));
            model.setTenant(tenant);
        }
   }

   void verifyDomains(List<Integer> domainIds, FilterModel filter) {
        Optional.ofNullable(domainIds).ifPresent(ids -> {
            List<DomainModel> domains = domainRepository.findByIds(ids);
            List<Integer> validDomainIds = domains.stream().map(t -> t.getId()).collect(toList());

            ids.forEach(id -> {
                if (!validDomainIds.contains(id))
                    throw new RuntimeException("Invalid domain: " + id);
            });
            filter.setDomains(domains);
        });
    }

   void saveDomains(List<Integer> domainIds, FilterModel filter) {
    Optional.ofNullable(domainIds).ifPresent(domain -> domain.forEach(domainId -> {
        filter.getDomains().stream().filter(current -> current.getId() == domainId)
                .findFirst().ifPresentOrElse(current -> {
                }, () -> filter.getDomains()
                        .add(domainRepository.findById(domainId)
                                .orElseThrow(() -> new RuntimeException(
                                        "Invalid domain: " + domainId))));
        }));

    }

}
