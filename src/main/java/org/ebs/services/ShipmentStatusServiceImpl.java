package org.ebs.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.ebs.model.EventModel;
import org.ebs.model.StageModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.repos.EventRepository;
import org.ebs.model.repos.StageRepository;
import org.ebs.model.repos.WorkflowInstanceRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class ShipmentStatusServiceImpl implements ShipmentStatusService {

    private final EventRepository eventRepository;
    private final StageRepository stageRepository;
    private final WorkflowRepository workflowRepository;
    private final WorkflowInstanceRepository workflowInstanceRepository;
    private JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();

    @Override
    public String changeShipmentStatus(Integer workflowId, Integer workflowInstanceId, String action) {

        String status = new String();

        List<String> validActions = new ArrayList<>(Arrays.asList("reset", "next", "back"));

        if (!validActions.contains(action)){
            System.out.println("Invalid action");
            jsonObjectBuilder.add("error", "400 Error: Invalid action [" + action + "]");
            JsonObject message = jsonObjectBuilder.build();
            return message.toString();
        }

        WorkflowModel workflow = workflowRepository.findById(workflowId).orElse(null);

        if (workflow == null) {
            jsonObjectBuilder.add("error", "400 Error: Workflow with id " + workflowId + " does not exist");
            return jsonObjectBuilder.build().toString();
        }

        WorkflowInstanceModel workflowInstance = workflowInstanceRepository.findById(workflowInstanceId).orElse(null);

        if (workflowInstance == null) {
            jsonObjectBuilder.add("error", "400 Error: Workflow instance with id " + workflowInstanceId + " does not exist");
            return jsonObjectBuilder.build().toString();
        }

        List<EventModel> events = eventRepository.findByWorkflowInstanceAndDeletedIsFalse(workflowInstance);

        if (events.isEmpty()) {
            jsonObjectBuilder.add("error", "400 Error: No shipment defined for workflow instance " + workflowInstanceId);
            return jsonObjectBuilder.build().toString();
        }

        if (action.equals("reset")) {
            events.forEach(event -> {
                event.setCompleted(null);
                event.setError(null);
                event.setDescription(null);
                eventRepository.save(event);
            });
            workflowInstance.setComplete(null);
            workflowInstanceRepository.save(workflowInstance);
            return jsonObjectBuilder.add("success", "Action executed successfully: The shipment's workflow was reset").build().toString();
        } else {
            if (action.equals("next")) {
                events = eventRepository
                        .findByWorkflowInstanceAndCompletedIsNullAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAsc(
                                workflowInstance);
                if (events.isEmpty())
                    return jsonObjectBuilder.add("success","This shipment's workflow has already been completed").build().toString();
                EventModel currentEvent = events.stream().findFirst().orElse(null);
                if (currentEvent.getStage() == null)
                    return jsonObjectBuilder.add("error","500 Error: There is no information about the current stage/status of the shipment").build().toString();
                currentEvent.setCompleted(new java.util.Date());
                eventRepository.save(currentEvent);
                StageModel nextStage = stageRepository.findBySequenceAndPhaseSequenceAndPhaseWorkflowAndDeletedIsFalse(currentEvent.getStage().getSequence() + 1, currentEvent.getStage().getPhase().getSequence(), workflow).orElse(null);
                if (nextStage == null) {
                    nextStage = stageRepository.findBySequenceAndPhaseSequenceAndPhaseWorkflowAndDeletedIsFalse(1, currentEvent.getStage().getPhase().getSequence() + 1, workflow).orElse(null);
                }
                StageModel newStage = nextStage;
                if (newStage != null) {
                    events.stream().filter(ev -> ev.getStage().equals(newStage)).findFirst().ifPresentOrElse(e -> {
                        if (e.getStage().getName().equals("Done")) {
                            e.setCompleted(new java.util.Date());
                            eventRepository.save(e);
                            workflowInstance.setComplete(new java.util.Date());
                            workflowInstanceRepository.save(workflowInstance);
                        }
                    }, () -> {
                        EventModel nextEvent = new EventModel();
                        nextEvent.setStage(newStage);
                        nextEvent.setWorkflowInstance(workflowInstance);
                        nextEvent.setRecordId(currentEvent.getRecordId());
                        //TODO nextEvent.setTenant(workflow.getTenant()) <- this method changed, workflow now has multiple tenants
                        // need to adjust it, workaround newStage.getTenant()
                        nextEvent.setTenant(newStage.getTenant());
                        if (nextEvent.getStage().getName().equals("Done")) {
                            nextEvent.setCompleted(new java.util.Date());
                            workflowInstance.setComplete(new java.util.Date());
                            workflowInstanceRepository.save(workflowInstance);
                        }
                        eventRepository.save(nextEvent);
                    });
                    status = newStage.getName();
                }
            } else if (action.equals("back")) {
                events = eventRepository
                        .findByWorkflowInstanceAndCompletedNotNullAndDeletedIsFalseOrderByStagePhaseSequenceDescStageSequenceDesc(
                                workflowInstance);
                if (events.isEmpty())
                    return jsonObjectBuilder.add("success","This shipment's status is in the first stage").build().toString();
                EventModel currentEvent = events.stream().findFirst().orElse(null);
                if (currentEvent.getStage() == null)
                    return jsonObjectBuilder.add("error","500 Error: There is no information about the current stage/status of the shipment").build().toString();
                currentEvent.setCompleted(null);
                currentEvent.setError(null);
                currentEvent.setDescription(null);
                if (currentEvent.getStage().getName().equals("Done")) {
                    workflowInstance.setComplete(null);
                    workflowInstanceRepository.save(workflowInstance);
                }
                eventRepository.save(currentEvent);
                status = currentEvent.getStage().getName();
            }
        }

        return jsonObjectBuilder.add("success","Action executed successfully, current status is now: " + status).build().toString();
    }

}
