package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.PrintoutTemplate;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.PrintoutTemplateInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface PrintoutTemplateService {

	public Optional<PrintoutTemplate> findPrintoutTemplate(int printoutTemplateId);

	public Page<PrintoutTemplate> findPrintoutTemplates(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);
	
	public PrintoutTemplate createPrintoutTemplate(PrintoutTemplateInput printoutTemplate);

	public PrintoutTemplate modifyPrintoutTemplate(PrintoutTemplateInput printoutTemplate);

	public int deletePrintoutTemplate(int printoutTemplateId);

	public PrintoutTemplate addPrintoutTemplateToPrograms(int printoutTemplateId, Set<Integer> programIds);

    public PrintoutTemplate removePrintoutTemplateFromPrograms(int printoutTemplateId, Set<Integer> programIds);

    public PrintoutTemplate addPrintoutTemplateToProducts(int printoutTemplateId, Set<Integer> productIds);
	
    public PrintoutTemplate removePrintoutTemplateFromProducts(int printoutTemplateId, Set<Integer> productIds);

	public List<ProgramTo> findPrograms(int printoutTemplateId);

	UserTo findCreatedBy(int printoutTemplateId);

    UserTo findUpdatedBy(int printoutTemplateId);
}