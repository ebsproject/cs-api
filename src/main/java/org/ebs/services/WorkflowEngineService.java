package org.ebs.services;

public interface WorkflowEngineService {
    
    public String moveNode(int wfInstanceId, int recordId, int targetNodeId);

    public String moveNode(int wfInstanceId, int recordId, String action);

    public String changeStatus(String payload);

}
