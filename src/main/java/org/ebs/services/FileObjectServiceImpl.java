package org.ebs.services;

import static java.util.Optional.ofNullable;

import org.ebs.model.FileObjectModel;
import org.ebs.model.repos.FileObjectRepository;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.Input.FileObjectInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class FileObjectServiceImpl implements FileObjectService {

    private final ConversionService converter;
    private final FileObjectRepository fileObjectRepository;


    @Override
    @Transactional(readOnly = false)
    public FileObjectTo modifyFileObject(FileObjectInput input) {
        FileObjectModel target = fileObjectRepository.findById(input.getId())
        .orElseThrow(
                () -> new RuntimeException("File Object relationship id " + input.getId() + " not found"));
                      
        ofNullable(input.getDescription()).ifPresent(d -> target.setDescription(d));
        ofNullable(input.getStatus()).ifPresent(s -> target.setStatus(s));
                
        return converter.convert(fileObjectRepository.save(target), FileObjectTo.class);

    }

}
