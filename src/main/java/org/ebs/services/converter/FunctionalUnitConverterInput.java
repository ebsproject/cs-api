package org.ebs.services.converter;

import org.ebs.model.FunctionalUnitModel;
import org.ebs.services.to.Input.FunctionalUnitInput;
import org.ebs.util.SimpleConverter;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class FunctionalUnitConverterInput implements Converter<FunctionalUnitInput, FunctionalUnitModel> {

    @Override
    public FunctionalUnitModel convert(FunctionalUnitInput source) {
        FunctionalUnitModel target = new FunctionalUnitModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
