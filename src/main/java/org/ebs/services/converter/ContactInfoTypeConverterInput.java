package org.ebs.services.converter;

import org.ebs.model.ContactInfoTypeModel;
import org.ebs.services.to.Input.ContactInfoTypeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactInfoTypeConverterInput implements Converter<ContactInfoTypeInput, ContactInfoTypeModel> {

    @Override
    public ContactInfoTypeModel convert(ContactInfoTypeInput source) {

        ContactInfoTypeModel target = new ContactInfoTypeModel();
        BeanUtils.copyProperties(source, target);
        return target;
        
    }
    
}
