package org.ebs.services.converter;
import org.ebs.model.SecurityRuleModel;
import org.ebs.services.to.SecurityRuleTo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;
import lombok.AllArgsConstructor;
@Component
@AllArgsConstructor
public class SecurityRuleConverter implements Converter<SecurityRuleModel, SecurityRuleTo> {
  @Override
  public SecurityRuleTo convert(SecurityRuleModel source) {
    SecurityRuleTo target = new SecurityRuleTo();
    BeanUtils.copyProperties(source, target);
    return target;
  }
}