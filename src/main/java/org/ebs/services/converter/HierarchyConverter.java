package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import org.ebs.model.HierarchyModel;
import org.ebs.services.to.HierarchyTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HierarchyConverter implements Converter<HierarchyModel, HierarchyTo> {

    @Override
    public HierarchyTo convert(HierarchyModel source) {
        HierarchyTo target = new HierarchyTo();
        BeanUtils.copyProperties(source, target);
        ofNullable(source.getContact())
            .ifPresent(o -> target.setContactId(o.getId()));
        ofNullable(source.getInstitution())
            .ifPresent(o -> target.setInstitutionId(o.getId()));
        return target;
    }
    
}