package org.ebs.services.converter;
import org.ebs.model.StatusModel;
import org.ebs.services.to.Input.StatusInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class StatusConverterInput implements Converter<StatusInput, StatusModel> {
    
    @Override
    public StatusModel convert(StatusInput source) {
        StatusModel target = new StatusModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
