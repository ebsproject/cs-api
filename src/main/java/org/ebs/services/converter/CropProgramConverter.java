package org.ebs.services.converter;

import org.ebs.model.CropProgramModel;
import org.ebs.services.to.CropProgramTo;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class CropProgramConverter extends SimpleConverter<CropProgramModel, CropProgramTo> {

}