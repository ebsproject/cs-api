package org.ebs.services.converter;

import org.ebs.model.OccurrenceShipmentModel;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class OccurrenceShipmentConverter implements Converter<OccurrenceShipmentModel, OccurrenceShipmentTo> {
    
    @Override
    public OccurrenceShipmentTo convert(OccurrenceShipmentModel source) {
        OccurrenceShipmentTo target = new OccurrenceShipmentTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
