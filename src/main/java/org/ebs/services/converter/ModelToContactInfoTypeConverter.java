package org.ebs.services.converter;

import org.ebs.model.ContactInfoTypeModel;
import org.ebs.services.to.ContactInfoType;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class ModelToContactInfoTypeConverter extends SimpleConverter<ContactInfoTypeModel, ContactInfoType> {

}
