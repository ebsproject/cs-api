package org.ebs.services.converter;
import org.ebs.model.PhaseModel;
import org.ebs.services.to.Input.PhaseInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class PhaseConverterInput implements Converter<PhaseInput, PhaseModel> {
    
    @Override
    public PhaseModel convert(PhaseInput source) {
        PhaseModel target = new PhaseModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
