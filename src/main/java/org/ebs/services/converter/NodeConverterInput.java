package org.ebs.services.converter;
import org.ebs.model.NodeModel;
import org.ebs.services.to.Input.NodeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class NodeConverterInput implements Converter<NodeInput, NodeModel> {
    
    @Override
    public NodeModel convert(NodeInput source) {
        NodeModel target = new NodeModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
