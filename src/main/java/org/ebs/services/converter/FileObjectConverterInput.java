package org.ebs.services.converter;

import org.ebs.model.FileObjectModel;
import org.ebs.services.to.Input.FileObjectInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class FileObjectConverterInput implements Converter<FileObjectInput, FileObjectModel> {

    @Override
    public FileObjectModel convert(FileObjectInput source) {

        FileObjectModel target = new FileObjectModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
