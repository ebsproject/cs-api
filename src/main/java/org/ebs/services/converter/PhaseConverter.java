package org.ebs.services.converter;

import org.ebs.model.PhaseModel;
import org.ebs.services.to.PhaseTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class PhaseConverter implements Converter<PhaseModel, PhaseTo> {

    @Override
    public PhaseTo convert(PhaseModel source) {
        PhaseTo target = new PhaseTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
