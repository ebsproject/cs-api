package org.ebs.services.converter;

import org.ebs.model.NodeCFModel;
import org.ebs.services.to.NodeCFTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class NodeCFConverter implements Converter<NodeCFModel, NodeCFTo> {
    
    @Override
    public NodeCFTo convert(NodeCFModel source) {
        NodeCFTo target = new NodeCFTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
