package org.ebs.services.converter;

import org.ebs.model.ServiceModel;
import org.ebs.services.to.ServiceTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ServiceConverter implements Converter<ServiceModel, ServiceTo> {
    
    @Override
    public ServiceTo convert(ServiceModel source) {
        ServiceTo target = new ServiceTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
