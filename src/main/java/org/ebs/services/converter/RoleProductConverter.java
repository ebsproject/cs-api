package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import org.ebs.model.RoleProductModel;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.RoleProductTo;
import org.ebs.services.to.RoleTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleProductConverter implements Converter<RoleProductModel, RoleProductTo> {

        @Override
        public RoleProductTo convert(RoleProductModel source) {
                source.getRole();
                RoleProductTo target = new RoleProductTo();
                BeanUtils.copyProperties(source, target);
                ofNullable(source.getRole())
                                .ifPresent(o -> target.setRoleId(o.getId()));
                ofNullable(source.getProductFunction())
                                .ifPresent(o -> target.setProductFunctionId(o.getId()));
                target.setRole(new RoleTo(source.getRole().getId(), source.getRole().getDescription(),
                                source.getRole().getSecurityGroup(), source.getRole().isSystem(),
                                source.getRole().getName(), null, null));
                target.setProductFunction(
                                new ProductFunctionTo(source.getProductFunction().getId(),
                                                source.getProductFunction().getDescription(),
                                                false, source.getProductFunction().getAction(), null, null, false));

                return target;
        }

}