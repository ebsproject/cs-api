package org.ebs.services.converter;

import org.ebs.model.ServiceFileModel;
import org.ebs.services.to.ServiceFileTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ServiceFileConverter implements Converter<ServiceFileModel, ServiceFileTo> {

    @Override
    public ServiceFileTo convert(ServiceFileModel source) {
        ServiceFileTo target = new ServiceFileTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
