package org.ebs.services.converter;

import org.ebs.model.PersonModel;
import org.ebs.services.to.Input.PersonInput;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class PersonInputToModelConverter extends SimpleConverter<PersonInput, PersonModel> {

}
