package org.ebs.services.converter.rule;

import java.util.ArrayList;
import java.util.List;

import org.ebs.model.rule.SegmentModel;
import org.ebs.model.rule.SequenceRuleSegmentModel;
import org.ebs.services.to.rule.ApiSegment;
import org.ebs.services.to.rule.ScaleSegment;
import org.ebs.services.to.rule.Segment;
import org.ebs.services.to.rule.SequenceSegment;
import org.ebs.services.to.rule.StaticSegment;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;

@Component
class ModelToSegmentConverter implements Converter<SequenceRuleSegmentModel, Segment>{

    @Override
    public Segment convert(SequenceRuleSegmentModel seqRuleSource) {
        Segment target = null;

        SegmentModel source = seqRuleSource.getSegment();

        if (source.getSequenceSegment() == null) {
            if (source.getApiSegment() == null) {
                if (source.getFormula() == null && source.getFormat() == null && source.getScale() == null) {
                    throw new RuntimeException("Segment [" + source.getName() + "] has incomplete or wrong definition, cannot resolve.");
                }else if (source.getFormula() != null || source.getFormat() != null){
                    target = new StaticSegment();
                    BeanUtils.copyProperties(source, target);

                }else if(source.getScale() != null) {
                    target = new ScaleSegment();
                    BeanUtils.copyProperties(source, target);
                }
            } else {
                target = new ApiSegment();
                BeanUtils.copyProperties(source.getApiSegment(), target);
                BeanUtils.copyProperties(source, target);
            }
        } else {
            target = new SequenceSegment();
            BeanUtils.copyProperties(source.getSequenceSegment(), target);
            BeanUtils.copyProperties(source, target);
        }
        target.setDisplayed(seqRuleSource.isDisplayed());
        target.setRequired(seqRuleSource.isRequired());
        target.setFamily(seqRuleSource.getFamily());
        target.setPosition(seqRuleSource.getPosition());
        target.setSkip(seqRuleSource.getSkip());
        target.setSkipInputSegmentId(seqRuleSource.getSkipInputSegmentId());
        target.setScale(jsonNodeToArray(seqRuleSource.getSegment().getScale()));
        return target;
    }

    List<String> jsonNodeToArray(JsonNode scale) {
        List<String> scales = null;
        if (scale != null && scale.isArray()) {
            scales = new ArrayList<>();
            for (JsonNode n : scale) {
                scales.add(n.textValue());
            }
        }
        return scales;
    }
    
}
