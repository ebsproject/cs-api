package org.ebs.services.converter.rule;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.ebs.model.rule.SequenceRuleModel;
import org.ebs.services.to.rule.Segment;
import org.ebs.services.to.rule.SequenceRule;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class ModelToSequenceRuleConverter implements Converter<SequenceRuleModel, SequenceRule>{

    private final ModelToSegmentConverter segmentConverter;

    @Override
    public SequenceRule convert(SequenceRuleModel source) {
        SequenceRule target = new SequenceRule();
        BeanUtils.copyProperties(source, target);
        
        List<Segment> segments = source.getRuleSegments().stream()
            .filter(rs -> !rs.isDeleted() && !rs.getSegment().isDeleted())
            .sorted((rs, rs2) -> rs.getPosition() < rs2.getPosition() ? -1 : 1)
            .map(rs -> segmentConverter.convert(rs))
            .collect(toList());
        target.setSegments(segments);
		return target;
    }
    
}
