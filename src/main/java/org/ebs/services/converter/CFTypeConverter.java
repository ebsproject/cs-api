package org.ebs.services.converter;

import org.ebs.model.CFTypeModel;
import org.ebs.services.to.CFTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class CFTypeConverter implements Converter<CFTypeModel, CFTypeTo> {
    
    @Override
    public CFTypeTo convert(CFTypeModel source) {
        CFTypeTo target = new CFTypeTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
