package org.ebs.services.converter;

import org.ebs.model.PrintoutTemplateModel;
import org.ebs.services.to.Input.PrintoutTemplateInput;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class PrintoutTemplateInputConverter extends SimpleConverter<PrintoutTemplateInput, PrintoutTemplateModel> {

}