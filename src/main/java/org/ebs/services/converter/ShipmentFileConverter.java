package org.ebs.services.converter;

import org.ebs.model.ShipmentFileModel;
import org.ebs.services.to.ShipmentFileTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ShipmentFileConverter implements Converter<ShipmentFileModel, ShipmentFileTo> {

    @Override
    public ShipmentFileTo convert(ShipmentFileModel source) {
        ShipmentFileTo target = new ShipmentFileTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
