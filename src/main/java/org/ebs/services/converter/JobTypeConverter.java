package org.ebs.services.converter;

import org.ebs.model.JobTypeModel;
import org.ebs.services.to.JobTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JobTypeConverter implements Converter<JobTypeModel, JobTypeTo> {

    @Override
    public JobTypeTo convert(JobTypeModel source) {
        
        JobTypeTo target = new JobTypeTo();

        BeanUtils.copyProperties(source, target);

        return target;
    }
    
}
