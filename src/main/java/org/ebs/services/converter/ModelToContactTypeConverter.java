package org.ebs.services.converter;
 

import org.springframework.core.convert.converter.Converter;
import org.ebs.model.CategoryModel;
import org.ebs.model.ContactTypeModel;
import org.ebs.model.repos.ContactTypeRepository;
import org.ebs.services.to.CategoryTo;
import org.ebs.services.to.ContactType;
import org.ebs.util.SimpleConverter;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import kotlin.reflect.jvm.internal.impl.descriptors.SourceElement;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Component
class ModelToContactTypeConverter implements Converter<ContactTypeModel, ContactType> {


	ContactTypeRepository contactTypeRepository;

	/**
	 * 
	 * @param source
	 */
	@Override
	public ContactType convert(ContactTypeModel source){
	
		//ContactTypeModel model1 = contactTypeRepository.findById(source.getId()).orElse(null);
		ContactType target = new  ContactType(); 
		BeanUtils.copyProperties(source, target); 
		
		//TODO: review the reason why the dont copy the attribute category to target
		CategoryModel categoryModel = source.getCategory();
        CategoryTo categoryTo = new CategoryTo();
        BeanUtils.copyProperties(categoryModel, categoryTo);
		target.setCategory(categoryTo);
		return target;
	}
}
