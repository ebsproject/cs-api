package org.ebs.services.converter;

import org.ebs.model.ServiceItemModel;
import org.ebs.services.to.Input.ServiceItemInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ServiceItemConverterInput implements Converter<ServiceItemInput, ServiceItemModel> {

    @Override
    public ServiceItemModel convert(ServiceItemInput source) {
        ServiceItemModel target = new ServiceItemModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
