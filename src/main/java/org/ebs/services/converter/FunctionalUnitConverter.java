package org.ebs.services.converter;

import org.ebs.model.FunctionalUnitModel;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class FuncUnitModelToFuncUnitToConverter extends SimpleConverter<FunctionalUnitModel, FunctionalUnitTo> {

    @Override
    public FunctionalUnitTo convert(FunctionalUnitModel source) {
        FunctionalUnitTo target = super.convert(source);
        return target;
    }

}