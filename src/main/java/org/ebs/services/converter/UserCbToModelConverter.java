package org.ebs.services.converter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.ebs.model.CategoryModel;
import org.ebs.model.ContactModel;
import org.ebs.model.PersonModel;
import org.ebs.model.PurposeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.UserCB;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

//TODO: Review second parameter (Category) Contact Model 
@Component
@RequiredArgsConstructor
class UserCbToModelConverter implements Converter<UserCB, UserModel> {

    private final TenantRepository tenantRepository;

    static final int DEFAULT_TENANT = 1;

    @Override
    public UserModel convert(UserCB source) {
        UserModel target = new UserModel();

        target.setExternalId(source.getPersonDbId());
        target.setUserName(source.getEmail());

        target.setActive(source.isActive());

        target.setUserName(source.getEmail());
        target.setActive(source.isActive());

        
        CategoryModel categoryM = new CategoryModel();
        categoryM.setId(1);
        ContactModel external_Id = new ContactModel();
        external_Id.setId(source.getPersonDbId());


        ContactModel contact = new ContactModel(0, categoryM, null, null, null, null, 
        null,source.getEmail(), null,null,null, null, 
        null, null, external_Id.getId(), null, null);
        PersonModel person = new PersonModel(0, source.getLastName(), source.getFirstName(), null, null,
                "NA", null, null, contact, null, null);
        contact.setPerson(person);
        target.setContact(contact);

        Set<TenantModel> tenants = new HashSet<>();
        tenants.add(tenantRepository.findById(DEFAULT_TENANT).get());
        target.setTenants(tenants);

       
        return target;
    }

}