package org.ebs.services.converter;

import org.ebs.model.JobLogModel;
import org.ebs.services.to.Input.JobLogInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
  class JobLogConverterInput implements Converter<JobLogInput,JobLogModel> {

	@Override
	public JobLogModel convert(JobLogInput source){
		JobLogModel target = new  JobLogModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}