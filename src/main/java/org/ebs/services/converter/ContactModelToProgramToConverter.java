package org.ebs.services.converter;

import java.util.Optional;

import org.ebs.model.ContactModel;
import org.ebs.services.to.ProgramTo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactModelToProgramToConverter implements Converter<ContactModel, ProgramTo> {
    
    @Override
    public ProgramTo convert(ContactModel source) {
        ProgramTo target = new ProgramTo();
        Optional.ofNullable(source.getInstitution())
            .ifPresent(i -> {
                target.setId(source.getId());
                target.setCode(i.getCommonName());
                target.setName(i.getLegalName());
                target.setExternalId(i.getExternalCode());
            });
        return target;
    }

}
