package org.ebs.services.converter;

import org.ebs.model.StageModel;
import org.ebs.services.to.StageTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class StageConverter implements Converter<StageModel, StageTo> {

    @Override
    public StageTo convert(StageModel source) {
        StageTo target = new StageTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
