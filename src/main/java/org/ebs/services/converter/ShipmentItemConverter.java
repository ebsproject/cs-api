package org.ebs.services.converter;

import org.ebs.model.ShipmentItemModel;
import org.ebs.services.to.ShipmentItemTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ShipmentItemConverter implements Converter<ShipmentItemModel, ShipmentItemTo> {

    @Override
    public ShipmentItemTo convert(ShipmentItemModel source) {
        ShipmentItemTo target = new ShipmentItemTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
