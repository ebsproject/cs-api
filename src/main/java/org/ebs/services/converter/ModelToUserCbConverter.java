package org.ebs.services.converter;

import org.ebs.model.UserModel;
import org.ebs.model.repos.RoleRepository;
import org.ebs.services.to.UserCB;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class ModelToUserCbConverter implements Converter<UserModel, UserCB> {
    private final RoleRepository roleRepository;

    @Override
    public UserCB convert(UserModel source) {
        UserCB target = new UserCB();
        target.setPersonDbId(source.getExternalId());
        target.setEmail(source.getUserName());
        target.setFirstName(source.getContact().getPerson().getGivenName());
        target.setLastName(source.getContact().getPerson().getFamilyName());
        target.setIsActive(source.isActive());

        return target;
    }

}