package org.ebs.services.converter;

import org.ebs.model.JobWorkflowModel;
import org.ebs.services.to.JobWorkflowTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JobWorkflowConverter implements Converter<JobWorkflowModel, JobWorkflowTo> {

    @Override
    public JobWorkflowTo convert(JobWorkflowModel source) {
        
        JobWorkflowTo target = new JobWorkflowTo();

        BeanUtils.copyProperties(source, target);

        return target;
    }
    
}
