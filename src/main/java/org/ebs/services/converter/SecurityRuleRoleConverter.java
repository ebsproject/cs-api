package org.ebs.services.converter;
import org.ebs.model.SecurityRuleRoleModel;
import org.ebs.services.to.SecurityRuleRoleTo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;
import lombok.AllArgsConstructor;
@Component
@AllArgsConstructor
public class SecurityRuleRoleConverter implements Converter<SecurityRuleRoleModel, SecurityRuleRoleTo> {
  @Override
  public SecurityRuleRoleTo convert(SecurityRuleRoleModel source) {
    SecurityRuleRoleTo target = new SecurityRuleRoleTo();
    BeanUtils.copyProperties(source, target);
    return target;
  }
}