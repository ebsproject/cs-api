package org.ebs.services.converter;

import org.ebs.model.ServiceItemModel;
import org.ebs.services.to.Input.ServItemInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ServItemConverterInput implements Converter<ServItemInput, ServiceItemModel> {

    @Override
    public ServiceItemModel convert(ServItemInput source) {
        ServiceItemModel target = new ServiceItemModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
