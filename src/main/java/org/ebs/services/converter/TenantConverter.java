package org.ebs.services.converter;

import org.ebs.model.TenantModel;
import org.ebs.services.to.TenantTo;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class TenantConverter extends SimpleConverter<TenantModel, TenantTo> {

}