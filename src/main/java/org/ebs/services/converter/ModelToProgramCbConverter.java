package org.ebs.services.converter;

import org.ebs.model.ProgramModel;
import org.ebs.services.to.ProgramCB;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class ModelToProgramCbConverter implements Converter<ProgramModel, ProgramCB> {

    @Override
    public ProgramCB convert(ProgramModel source) {
        ProgramCB target = new ProgramCB();

        target.setProgramCode(source.getCode());
        target.setProgramName(source.getName());
        target.setProgramDbId(source.getExternalId());

        return target;
    }

}