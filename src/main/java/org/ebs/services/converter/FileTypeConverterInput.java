package org.ebs.services.converter;
import org.ebs.model.FileTypeModel;
import org.ebs.services.to.Input.FileTypeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class FileTypeConverterInput implements Converter<FileTypeInput, FileTypeModel> {
    
    @Override
    public FileTypeModel convert(FileTypeInput source) {
        FileTypeModel target = new FileTypeModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
