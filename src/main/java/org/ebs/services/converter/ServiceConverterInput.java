package org.ebs.services.converter;
import org.ebs.model.ServiceModel;
import org.ebs.services.to.Input.ServiceInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ServiceConverterInput implements Converter<ServiceInput, ServiceModel> {
    
    @Override
    public ServiceModel convert(ServiceInput source) {
        ServiceModel target = new ServiceModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
