package org.ebs.services.converter;

import org.ebs.model.CFValueModel;
import org.ebs.services.to.CFValueTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class CFValueConverter implements Converter<CFValueModel, CFValueTo> {
    
    @Override
    public CFValueTo convert(CFValueModel source) {
        CFValueTo target = new CFValueTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
