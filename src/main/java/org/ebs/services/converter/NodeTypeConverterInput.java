package org.ebs.services.converter;
import org.ebs.model.NodeTypeModel;
import org.ebs.services.to.Input.NodeTypeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class NodeTypeConverterInput implements Converter<NodeTypeInput, NodeTypeModel> {
    
    @Override
    public NodeTypeModel convert(NodeTypeInput source) {
        NodeTypeModel target = new NodeTypeModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
