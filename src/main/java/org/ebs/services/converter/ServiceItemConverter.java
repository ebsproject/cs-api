package org.ebs.services.converter;

import org.ebs.model.ServiceItemModel;
import org.ebs.services.to.ServiceItemTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ServiceItemConverter implements Converter<ServiceItemModel, ServiceItemTo> {

    @Override
    public ServiceItemTo convert(ServiceItemModel source) {
        ServiceItemTo target = new ServiceItemTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
