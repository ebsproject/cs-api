package org.ebs.services.converter;

import org.ebs.model.AddressModel;
import org.ebs.services.to.Address;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class ModelToAddressConverter extends SimpleConverter<AddressModel, Address> {

}
