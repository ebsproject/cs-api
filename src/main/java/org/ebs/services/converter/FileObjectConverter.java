package org.ebs.services.converter;

import org.ebs.model.FileObjectModel;
import org.ebs.services.to.FileObjectTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class FileObjectConverter implements Converter<FileObjectModel, FileObjectTo> {
    
    @Override
    public FileObjectTo convert(FileObjectModel source) {
        FileObjectTo target = new FileObjectTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
