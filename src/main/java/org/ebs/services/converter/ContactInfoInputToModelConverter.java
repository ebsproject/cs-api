package org.ebs.services.converter;

import org.ebs.model.ContactInfoModel;
import org.ebs.model.repos.ContactInfoTypeRepository;
import org.ebs.services.to.Input.ContactInfoInput;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
class ContactInfoInputToModelConverter extends SimpleConverter<ContactInfoInput, ContactInfoModel> {

    private final ContactInfoTypeRepository infoTypeRepository;

    @Override
    protected void customize(ContactInfoInput source, ContactInfoModel target) {
        verifyInfoType(source.getContactInfoTypeId(), target);
    }

    void verifyInfoType(int infoTypeId, ContactInfoModel target) {
        infoTypeRepository.findById(infoTypeId).ifPresentOrElse(c -> target.setContactInfoType(c), () -> {
            throw new RuntimeException("InfoType not found: " + infoTypeId);
        });

    }
}
