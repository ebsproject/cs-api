package org.ebs.services.converter;

import org.ebs.model.ContactInfoModel;
import org.ebs.services.to.ContactInfo;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class ModelToContactInfoConverter extends SimpleConverter<ContactInfoModel, ContactInfo> {

}
