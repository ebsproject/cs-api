///////////////////////////////////////////////////////////
//  TypeConverterInput.java
//  Macromedia ActionScript Implementation of the Class TypeConverterInput
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:34 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.services.to.Input.TypeInput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.beans.BeanUtils;
import org.ebs.model.TypeModel;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:34 AM
 */
@Component
  class TypeConverterInput implements Converter<TypeInput,TypeModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public TypeModel convert(TypeInput source){
		TypeModel target = new  TypeModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}