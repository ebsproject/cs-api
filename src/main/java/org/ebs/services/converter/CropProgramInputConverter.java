package org.ebs.services.converter;

import org.ebs.model.CropProgramModel;
import org.ebs.services.to.Input.CropProgramInput;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class CropProgramInputConverter extends SimpleConverter<CropProgramInput, CropProgramModel> {

}