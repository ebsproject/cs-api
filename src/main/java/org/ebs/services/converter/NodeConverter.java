package org.ebs.services.converter;

import org.ebs.model.NodeModel;
import org.ebs.services.to.NodeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class NodeConverter implements Converter<NodeModel, NodeTo> {
    
    @Override
    public NodeTo convert(NodeModel source) {
        NodeTo target = new NodeTo();
        BeanUtils.copyProperties(source, target, "nodeCFs");
        return target;
    }

}
