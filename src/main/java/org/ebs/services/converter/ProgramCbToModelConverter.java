package org.ebs.services.converter;

import org.ebs.model.ProgramModel;
import org.ebs.services.to.ProgramCB;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class ProgramCbToModelConverter implements Converter<ProgramCB, ProgramModel> {

    @Override
    public ProgramModel convert(ProgramCB source) {
        ProgramModel target = new ProgramModel();
        target.setExternalId(source.getProgramDbId());
        target.setName(source.getProgramName());
        target.setCode(source.getProgramCode());

        return target;
    }

}