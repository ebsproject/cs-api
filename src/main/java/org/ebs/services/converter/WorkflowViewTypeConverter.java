package org.ebs.services.converter;

import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class WorkflowViewTypeConverter implements Converter<WorkflowViewTypeModel, WorkflowViewTypeTo> {
    
    @Override
    public WorkflowViewTypeTo convert(WorkflowViewTypeModel source) {
        WorkflowViewTypeTo target = new WorkflowViewTypeTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
