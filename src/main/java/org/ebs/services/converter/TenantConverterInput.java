package org.ebs.services.converter;

import org.ebs.model.TenantModel;
import org.ebs.services.to.Input.TenantInput;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class TenantConverterInput extends SimpleConverter<TenantInput, TenantModel> {

}