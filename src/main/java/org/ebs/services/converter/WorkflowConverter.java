package org.ebs.services.converter;

import org.ebs.model.WorkflowModel;
import org.ebs.services.to.WorkflowTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class WorkflowConverter implements Converter<WorkflowModel, WorkflowTo> {
    
    @Override
    public WorkflowTo convert(WorkflowModel source) {
        WorkflowTo target = new WorkflowTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
