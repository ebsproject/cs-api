package org.ebs.services.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.ebs.model.CategoryModel;
import org.ebs.services.to.Input.CategoryInput;
import org.springframework.beans.BeanUtils;

@Component
public class CategoryConverterInput implements Converter<CategoryInput, CategoryModel> {
    @Override
    public CategoryModel convert(CategoryInput source) {
        CategoryModel target = new CategoryModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
}
