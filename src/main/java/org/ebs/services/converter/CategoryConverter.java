package org.ebs.services.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.ebs.model.CategoryModel;
import org.ebs.services.to.CategoryTo;
import org.springframework.beans.BeanUtils;

@Component
public class CategoryConverter implements Converter<CategoryModel, CategoryTo> {

    @Override
    public CategoryTo convert(CategoryModel source) {
        
        CategoryTo target = new CategoryTo();

        BeanUtils.copyProperties(source, target);

        return target;
    }
    
}
