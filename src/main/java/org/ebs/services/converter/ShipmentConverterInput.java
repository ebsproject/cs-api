package org.ebs.services.converter;

import org.ebs.model.ShipmentModel;
import org.ebs.services.to.Input.ShipmentInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ShipmentConverterInput implements Converter<ShipmentInput, ShipmentModel> {
    
    @Override
    public ShipmentModel convert(ShipmentInput source) {
        ShipmentModel target = new ShipmentModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}