package org.ebs.services.converter;

import org.ebs.model.InstitutionModel;
import org.ebs.services.to.Institution;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class InstitutionConverter implements Converter<InstitutionModel, Institution> {
    
    @Override
    public Institution convert(@NonNull InstitutionModel source) {
        Institution target = new Institution();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
