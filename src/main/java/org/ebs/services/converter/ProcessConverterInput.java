///////////////////////////////////////////////////////////
//  ProcessConverterInput.java
//  Macromedia ActionScript Implementation of the Class ProcessConverterInput
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:17 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.beans.BeanUtils;
import org.ebs.model.ProcessModel;
import org.ebs.services.to.Input.ProcessInput;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:17 AM
 */
@Component
  class ProcessConverterInput implements Converter<ProcessInput,ProcessModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public ProcessModel convert(ProcessInput source){
		ProcessModel target = new  ProcessModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}