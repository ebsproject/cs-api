package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import org.ebs.model.RoleProductModel;
import org.ebs.services.to.Input.RoleProductInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleProductConverterInput implements Converter<RoleProductInput, RoleProductModel> {

    @Override
    public RoleProductModel convert(RoleProductInput source) {
        RoleProductModel target = new RoleProductModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}