package org.ebs.services.converter;

import org.ebs.model.PrintoutTemplateModel;
import org.ebs.services.to.PrintoutTemplate;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class PrintoutTemplateConverter extends SimpleConverter<PrintoutTemplateModel, PrintoutTemplate> {

}