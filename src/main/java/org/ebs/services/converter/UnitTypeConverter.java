package org.ebs.services.converter;

import org.ebs.model.UnitTypeModel;
import org.ebs.services.to.UnitTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
public class UnitTypeConverter implements Converter<UnitTypeModel, UnitTypeTo> {
    
    @Override
    public UnitTypeTo convert(@NonNull UnitTypeModel source) {
        UnitTypeTo target = new UnitTypeTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
