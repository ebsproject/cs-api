package org.ebs.services.converter;

import org.ebs.model.ShipmentModel;
import org.ebs.services.to.ShipmentTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ShipmentConverter implements Converter<ShipmentModel, ShipmentTo> {
    
    @Override
    public ShipmentTo convert(ShipmentModel source) {
        ShipmentTo target = new ShipmentTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
