package org.ebs.services.converter;

import org.ebs.model.JobLogModel;
import org.ebs.services.to.JobLogTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JobLogConverter implements Converter<JobLogModel, JobLogTo> {

    @Override
    public JobLogTo convert(JobLogModel source) {
        
        JobLogTo target = new JobLogTo();

        BeanUtils.copyProperties(source, target);

        return target;
    }
    
}
