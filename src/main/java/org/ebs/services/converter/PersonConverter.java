package org.ebs.services.converter;

import org.ebs.model.PersonModel;
import org.ebs.services.to.Person;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class PersonConverter implements Converter<PersonModel, Person> {
    
    @Override
    public Person convert(PersonModel source) {
        Person target = new Person();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
