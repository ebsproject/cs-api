package org.ebs.services.converter;
import org.ebs.model.StageModel;
import org.ebs.services.to.Input.StageInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class StageConverterInput implements Converter<StageInput, StageModel> {
    
    @Override
    public StageModel convert(StageInput source) {
        StageModel target = new StageModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
