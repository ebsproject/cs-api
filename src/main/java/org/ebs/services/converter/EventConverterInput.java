package org.ebs.services.converter;

import org.ebs.model.EventModel;
import org.ebs.services.to.Input.EventInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class EventConverterInput implements Converter<EventInput, EventModel> {

    @Override
    public EventModel convert(EventInput source) {
        EventModel target = new EventModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
