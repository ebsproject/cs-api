package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import org.ebs.model.ContactModel;
import org.ebs.services.to.CategoryTo;
import org.ebs.services.to.Contact;
import org.ebs.services.to.Institution;
import org.ebs.services.to.Person;
import org.ebs.services.to.PurposeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class ModelToContactConverter implements Converter<ContactModel, Contact> {

    @Override
    public Contact convert(ContactModel source) {
        //TODO: REVIEW A BETTER IMPLEMENTATION
        
        PurposeTo purposeTo = new PurposeTo(); 
        
        //ofNullable(source.getPurpose()).ifPresent(p -> BeanUtils.copyProperties(p, purposeTo));

        CategoryTo categoryTo = new CategoryTo();

        ofNullable(source.getCategory()).ifPresent(c -> BeanUtils.copyProperties(c, categoryTo));

        Contact target = new Contact(source.getId(), categoryTo, null, null, null, null, null,null, null, source.getEmail(),purposeTo,null, null, null, null);
        target.setCreatedOn(source.getCreatedOn());
        target.setUpdatedOn(source.getUpdatedOn());
        ofNullable(source.getPerson()).ifPresent(p -> {
            target.setPerson(new Person(p.getId(), p.getFamilyName(), p.getGivenName(), p.getAdditionalName(), p.getFullName(), p.getGender(),
                    p.getJobTitle(), p.getKnowsAbout(),target, p.getSalutation(), null));
        });

        ofNullable(source.getInstitution()).ifPresent(i -> {
            target.setInstitution(new Institution(i.getId(), i.getCommonName(), i.getLegalName(),target, i.isCgiar(), i.getExternalCode(), null, null));
        });

        return target;
    }

}
