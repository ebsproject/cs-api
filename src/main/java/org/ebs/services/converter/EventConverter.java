package org.ebs.services.converter;

import org.ebs.model.EventModel;
import org.ebs.services.to.EventTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class EventConverter implements Converter<EventModel, EventTo> {

    @Override
    public EventTo convert(EventModel source) {
        EventTo target = new EventTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
