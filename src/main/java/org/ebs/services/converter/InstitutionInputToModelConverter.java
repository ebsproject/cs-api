package org.ebs.services.converter;

import org.ebs.model.InstitutionModel;
import org.ebs.services.to.Input.InstitutionInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class InstitutionInputToModelConverter implements Converter<InstitutionInput, InstitutionModel> {

    @Override
    public InstitutionModel convert(InstitutionInput source) {
        InstitutionModel target = new InstitutionModel();
        BeanUtils.copyProperties(source, target);
        target.setCgiar(source.isCgiar());
        return target;
    }



}
