package org.ebs.services.converter;

import org.ebs.model.StatusModel;
import org.ebs.services.to.StatusTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class StatusConverter implements Converter<StatusModel, StatusTo> {
    
    @Override
    public StatusTo convert(StatusModel source) {
        StatusTo target = new StatusTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
