package org.ebs.services.converter;

import org.ebs.model.NodeTypeModel;
import org.ebs.services.to.NodeTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class NodeTypeConverter implements Converter<NodeTypeModel, NodeTypeTo> {
    
    @Override
    public NodeTypeTo convert(NodeTypeModel source) {
        NodeTypeTo target = new NodeTypeTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
