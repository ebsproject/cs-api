package org.ebs.services.converter;
import org.ebs.model.OccurrenceShipmentModel;
import org.ebs.services.to.Input.OccurrenceShipmentInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class OccurrenceShipmentConverterInput implements Converter<OccurrenceShipmentInput, OccurrenceShipmentModel> {
    
    @Override
    public OccurrenceShipmentModel convert(OccurrenceShipmentInput source) {
        OccurrenceShipmentModel target = new OccurrenceShipmentModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
