package org.ebs.services.converter;
import org.ebs.model.CFTypeModel;
import org.ebs.services.to.Input.CFTypeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class CFTypeConverterInput implements Converter<CFTypeInput, CFTypeModel> {
    
    @Override
    public CFTypeModel convert(CFTypeInput source) {
        CFTypeModel target = new CFTypeModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
