package org.ebs.services.converter;
import org.ebs.model.SecurityRuleModel;
import org.ebs.services.to.Input.SecurityRuleInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SecurityRuleConverterInput implements Converter<SecurityRuleInput, SecurityRuleModel> {

    @Override
    public SecurityRuleModel convert(SecurityRuleInput source) {
        SecurityRuleModel target = new SecurityRuleModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}