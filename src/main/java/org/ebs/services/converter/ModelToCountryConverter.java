package org.ebs.services.converter;

import org.ebs.model.CountryModel;
import org.ebs.services.to.Country;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

@Component
class ModelToCountryConverter extends SimpleConverter<CountryModel, Country> {

}
