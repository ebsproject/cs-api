package org.ebs.services.converter;

import org.ebs.model.ContactWorkflowModel;
import org.ebs.services.to.Input.ContactWorkflowInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactWorkflowConverterInput implements Converter<ContactWorkflowInput, ContactWorkflowModel> {

    @Override
    public ContactWorkflowModel convert(ContactWorkflowInput source) {
        ContactWorkflowModel target = new ContactWorkflowModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}