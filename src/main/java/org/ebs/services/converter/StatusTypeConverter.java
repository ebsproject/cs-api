package org.ebs.services.converter;

import org.ebs.model.StatusTypeModel;
import org.ebs.services.to.StatusTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class StatusTypeConverter implements Converter<StatusTypeModel, StatusTypeTo> {
    
    @Override
    public StatusTypeTo convert(StatusTypeModel source) {
        StatusTypeTo target = new StatusTypeTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
