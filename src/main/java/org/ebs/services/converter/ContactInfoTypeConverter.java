package org.ebs.services.converter;

import org.ebs.model.ContactInfoTypeModel;
import org.ebs.services.to.ContactInfoType;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactInfoTypeConverter implements Converter<ContactInfoTypeModel, ContactInfoType> {

    @Override
    public ContactInfoType convert(ContactInfoTypeModel source) {

        ContactInfoType target = new ContactInfoType();
        BeanUtils.copyProperties(source, target);
        return target;

    }
    
}
