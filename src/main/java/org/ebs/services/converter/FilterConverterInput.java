package org.ebs.services.converter;

import java.util.Optional;

import org.ebs.model.FilterModel;
import org.ebs.services.to.Input.FilterEntityInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class FilterConverterInput implements Converter<FilterEntityInput, FilterModel> {

    @Override
    public FilterModel convert(FilterEntityInput source) {
        FilterModel target = new FilterModel();
        BeanUtils.copyProperties(source, target, "graphService", "system");
        Optional.ofNullable(source.getGraphService()).ifPresent(o -> target.setIsGraphService(o));
        Optional.ofNullable(source.getSystem()).ifPresent(o -> target.setIsSystem(o));
        return target;
    }
    
}