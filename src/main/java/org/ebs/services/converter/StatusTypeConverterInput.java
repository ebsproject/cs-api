package org.ebs.services.converter;
import org.ebs.model.StatusTypeModel;
import org.ebs.services.to.Input.StatusTypeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class StatusTypeConverterInput implements Converter<StatusTypeInput, StatusTypeModel> {
    
    @Override
    public StatusTypeModel convert(StatusTypeInput source) {
        StatusTypeModel target = new StatusTypeModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
