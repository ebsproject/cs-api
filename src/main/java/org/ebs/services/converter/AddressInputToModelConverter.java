package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import java.util.HashSet;
import java.util.Set;

import org.ebs.model.AddressModel;
import org.ebs.model.PurposeModel;
import org.ebs.model.repos.CountryRepository;
import org.ebs.model.repos.PurposeRepository;
import org.ebs.services.to.Input.AddressInput;
import org.ebs.util.SimpleConverter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
class AddressInputToModelConverter extends SimpleConverter<AddressInput, AddressModel> {

    private final CountryRepository countryRepository;
    private final PurposeRepository purposeRepository;

    @Override
    protected void customize(AddressInput source, AddressModel target) {
        Set<PurposeModel> purposes = new HashSet<>();
        verifyCountry(source.getCountryId(), target);
        source.getPurposeIds().forEach(purposeId -> {
            verifyPurpose(purposeId, target, purposes);
        });
        target.setPurposes(purposes);
    }

    void verifyPurpose(Integer purposeId, AddressModel target, Set<PurposeModel> purposes) {

        PurposeModel purpose = purposeRepository.findByIdAndDeletedIsFalse(purposeId)
                .orElseThrow(() -> new RuntimeException("Purpose not found" + purposeId));
        purposes.add(purpose);
    }

    void verifyCountry(Integer countryId, AddressModel target) {
        ofNullable(countryId)
                .ifPresent(id -> countryRepository.findById(id).ifPresentOrElse(c -> target.setCountry(c), () -> {
                    throw new RuntimeException("Country not found: " + id);
                }));

    }
}
