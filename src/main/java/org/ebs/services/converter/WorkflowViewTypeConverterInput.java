package org.ebs.services.converter;
import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.services.to.Input.WorkflowViewTypeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class WorkflowViewTypeConverterInput implements Converter<WorkflowViewTypeInput, WorkflowViewTypeModel> {
    
    @Override
    public WorkflowViewTypeModel convert(WorkflowViewTypeInput source) {
        WorkflowViewTypeModel target = new WorkflowViewTypeModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
