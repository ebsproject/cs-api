package org.ebs.services.converter;

import org.ebs.model.ServiceFileModel;
import org.ebs.services.to.Input.ServiceFileInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ServiceFileConverterInput implements Converter<ServiceFileInput, ServiceFileModel> {

    @Override
    public ServiceFileModel convert(ServiceFileInput source) {
        ServiceFileModel target = new ServiceFileModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
