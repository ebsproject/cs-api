package org.ebs.services.converter;

import org.ebs.model.ShipmentFileModel;
import org.ebs.services.to.Input.ShipmentFileInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ShipmentFileConverterInput implements Converter<ShipmentFileInput, ShipmentFileModel> {

    @Override
    public ShipmentFileModel convert(ShipmentFileInput source) {
        ShipmentFileModel target = new ShipmentFileModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
