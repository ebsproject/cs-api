package org.ebs.services.converter;

import org.ebs.model.FilterModel;
import org.ebs.services.to.FilterTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class FilterConverter implements Converter<FilterModel, FilterTo> {

    @Override
    public FilterTo convert(FilterModel source) {
        FilterTo target = new FilterTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}