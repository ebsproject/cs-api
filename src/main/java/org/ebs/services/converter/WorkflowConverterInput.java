package org.ebs.services.converter;

import java.util.Optional;

import org.ebs.model.WorkflowModel;
import org.ebs.services.to.Input.WorkflowInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WorkflowConverterInput implements Converter<WorkflowInput, WorkflowModel> {

    @Override
    public WorkflowModel convert(WorkflowInput source) {
        WorkflowModel target = new WorkflowModel();
        BeanUtils.copyProperties(source, target, "system");
        Optional.ofNullable(source.getSystem()).ifPresent(o -> target.setIsSystem(o));
        return target;
    }
    
}
