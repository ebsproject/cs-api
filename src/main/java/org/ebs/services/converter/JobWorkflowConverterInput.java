package org.ebs.services.converter;

import org.ebs.model.JobWorkflowModel;
import org.ebs.services.to.Input.JobWorkflowInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
  class JobWorkflowConverterInput implements Converter<JobWorkflowInput,JobWorkflowModel> {

	@Override
	public JobWorkflowModel convert(JobWorkflowInput source){
		JobWorkflowModel target = new  JobWorkflowModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}