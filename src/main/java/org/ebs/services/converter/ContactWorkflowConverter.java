package org.ebs.services.converter;

import org.ebs.model.ContactWorkflowModel;
import org.ebs.services.to.ContactWorkflowsTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactWorkflowConverter implements Converter<ContactWorkflowModel, ContactWorkflowsTo> {

    @Override
    public ContactWorkflowsTo convert(ContactWorkflowModel source) {
        ContactWorkflowsTo target = new ContactWorkflowsTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}