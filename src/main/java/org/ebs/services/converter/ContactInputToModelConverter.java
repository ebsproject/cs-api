package org.ebs.services.converter;



import org.ebs.model.CategoryModel;
import org.ebs.model.ContactModel;
import org.ebs.model.repos.CategoryRepository;
import org.ebs.services.to.Input.ContactInput;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
class ContactInputToModelConverter implements Converter<ContactInput, ContactModel> {

    private final PersonInputToModelConverter personConverter;
    private final InstitutionInputToModelConverter institutionConverter;
    private final CategoryRepository categoryRepository;

    @Override
    public ContactModel convert(ContactInput source) {

        ContactModel target = new ContactModel();
        target.setId(source.getId());
    
        CategoryModel modelCategory = new CategoryModel(); 
        modelCategory = categoryRepository.findById(source.getCategory().getId()).orElse(null);
        String nameOfCategory = modelCategory.getName();
       // BeanUtils.copyProperties(source.getCategory(), modelCategory); 
        target.setCategory(modelCategory);

        target.setEmail(source.getEmail());
        
        if (nameOfCategory.equals("Institution") || nameOfCategory.equals("Internal Unit")) {
            if (source.getInstitution() == null)
                throw new RuntimeException("Institution data is missing");
            target.setInstitution(institutionConverter.convert(source.getInstitution()));
        } else {
            target.setPerson(personConverter.convert(source.getPerson()));
        }

        return target;
    }

}
