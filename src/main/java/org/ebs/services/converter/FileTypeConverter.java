package org.ebs.services.converter;

import org.ebs.model.FileTypeModel;
import org.ebs.services.to.FileTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class FileTypeConverter implements Converter<FileTypeModel, FileTypeTo> {
    
    @Override
    public FileTypeTo convert(FileTypeModel source) {
        FileTypeTo target = new FileTypeTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}