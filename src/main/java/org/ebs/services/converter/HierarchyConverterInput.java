package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import org.ebs.model.HierarchyModel;
import org.ebs.services.to.Input.HierarchyInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HierarchyConverterInput implements Converter<HierarchyInput, HierarchyModel> {

    @Override
    public HierarchyModel convert(HierarchyInput source) {
        HierarchyModel target = new HierarchyModel();
        BeanUtils.copyProperties(source, target);
        ofNullable(source.isPrincipalContact()).ifPresent(o -> target.setPrincipalContact(o));
        return target;
    }
    
}