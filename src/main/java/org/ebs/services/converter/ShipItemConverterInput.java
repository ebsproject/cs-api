package org.ebs.services.converter;

import org.ebs.model.ShipmentItemModel;
import org.ebs.services.to.Input.ShipItemInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ShipItemConverterInput implements Converter<ShipItemInput, ShipmentItemModel> {

    @Override
    public ShipmentItemModel convert(ShipItemInput source) {
        ShipmentItemModel target = new ShipmentItemModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
