package org.ebs.services.converter;
import org.ebs.model.CFValueModel;
import org.ebs.services.to.Input.CFValueInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class CFValueConverterInput implements Converter<CFValueInput, CFValueModel> {
    
    @Override
    public CFValueModel convert(CFValueInput source) {
        CFValueModel target = new CFValueModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
