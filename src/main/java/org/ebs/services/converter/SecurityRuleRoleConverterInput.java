package org.ebs.services.converter;
import org.ebs.model.SecurityRuleRoleModel;
import org.ebs.services.to.Input.SecurityRuleRoleInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SecurityRuleRoleConverterInput implements Converter<SecurityRuleRoleInput, SecurityRuleRoleModel> {

    @Override
    public SecurityRuleRoleModel convert(SecurityRuleRoleInput source) {
        SecurityRuleRoleModel target = new SecurityRuleRoleModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}