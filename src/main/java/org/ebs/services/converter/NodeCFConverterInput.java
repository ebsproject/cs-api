package org.ebs.services.converter;
import org.ebs.model.NodeCFModel;
import org.ebs.services.to.Input.NodeCFInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class NodeCFConverterInput implements Converter<NodeCFInput, NodeCFModel> {
    
    @Override
    public NodeCFModel convert(NodeCFInput source) {
        NodeCFModel target = new NodeCFModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }

}
