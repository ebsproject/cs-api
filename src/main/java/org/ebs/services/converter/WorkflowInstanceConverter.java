package org.ebs.services.converter;

import org.ebs.model.WorkflowInstanceModel;
import org.ebs.services.to.WorkflowInstanceTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class WorkflowInstanceConverter implements Converter<WorkflowInstanceModel, WorkflowInstanceTo> {

    @Override
    public WorkflowInstanceTo convert(WorkflowInstanceModel source) {
        WorkflowInstanceTo target = new WorkflowInstanceTo();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
