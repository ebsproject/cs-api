package org.ebs.services.converter;

import org.ebs.model.WorkflowInstanceModel;
import org.ebs.services.to.Input.WorkflowInstanceInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class WorkflowInstanceConverterInput implements Converter<WorkflowInstanceInput, WorkflowInstanceModel> {

    @Override
    public WorkflowInstanceModel convert(WorkflowInstanceInput source) {
        WorkflowInstanceModel target = new WorkflowInstanceModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
