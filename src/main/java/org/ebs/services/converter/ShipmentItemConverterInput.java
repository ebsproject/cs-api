package org.ebs.services.converter;

import org.ebs.model.ShipmentItemModel;
import org.ebs.services.to.Input.ShipmentItemInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ShipmentItemConverterInput implements Converter<ShipmentItemInput, ShipmentItemModel> {

    @Override
    public ShipmentItemModel convert(ShipmentItemInput source) {
        ShipmentItemModel target = new ShipmentItemModel();
        BeanUtils.copyProperties(source, target);
        return target;
    }
    
}
