package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.Input.StageInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface StageService {

    public Optional<StageTo> findStage(int id);

    public Page<StageTo> findStages(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public StageTo createStage(StageInput input);

    public StageTo modifyStage(StageInput input);

    public int deleteStage(int id);

    public HtmlTagTo findHtmlTag(int stageId);

    public StageTo findParent(int stageId);

    public PhaseTo findPhase(int stageId);

    public WorkflowViewTypeTo findWorkflowViewType(int stageId);

    public TenantTo findTenant(int stageId);
}
