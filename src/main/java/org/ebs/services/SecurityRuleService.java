package org.ebs.services;

import java.util.List;
import java.util.Optional;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
import java.util.Set;
import org.ebs.services.to.RoleTo;
import org.ebs.services.to.Input.SecurityRuleInput;
import org.ebs.services.to.ProductFunctionTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.SecurityRuleTo;

public interface SecurityRuleService {

    /**
     *
     * @param securityRule
     */
    SecurityRuleTo createSecurityRule(SecurityRuleInput securityRule);

    /**
     *
     * @param securityRuleId
     */
    int deleteSecurityRule(int securityRuleId);

    /**
     *
     * @param roleId
     */
    // Set<ProductFunctionTo> findProductFunctions(int roleId);

    /**
     *
     * @param securityRuleId
     */
    Optional<SecurityRuleTo> findSecurityRule(int securityRuleId);

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    Page<SecurityRuleTo> findSecurityRules(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    /**
     *
     * @param roleId
     */
    // Set<UserTo> findUsers(int roleId);

    /**
     *
     * @param securityRule
     */
    SecurityRuleTo modifySecurityRule(SecurityRuleInput securityRule);

    // RoleTo findDefaultByUserId(int userId);

    // List<RoleTo> findByUser(int userId);

    // List<RoleTo> findByRole(int roleId);

    // List<RoleTo> findByContactHierarchy(int contactHierarchyId);

    // List<SecurityRuleTo> findRuleByRole(int roleId);

    UserTo findCreatedBy(int userId);

    UserTo findUpdatedBy(int userId);

}