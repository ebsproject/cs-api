package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.AttributesModel;
import org.ebs.model.CFTypeModel;
import org.ebs.model.CFValueModel;
import org.ebs.model.HtmlTagModel;
import org.ebs.model.NodeCFModel;
import org.ebs.model.NodeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.AttributesRepository;
import org.ebs.model.repos.CFTypeRepository;
import org.ebs.model.repos.CFValueRepository;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.NodeCFRepository;
import org.ebs.model.repos.NodeRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.AttributesTo;
import org.ebs.services.to.CFTypeTo;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.NodeCFInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class NodeCFServiceImpl implements NodeCFService {
    
    private final ConversionService converter;
    private final NodeCFRepository nodeCFRepository;
    private final AttributesRepository attributesRepository;
    private final CFTypeRepository cfTypeRepository;
    private final HtmlTagRepository htmlTagRepository;
    private final NodeRepository nodeRepository;
    private final CFValueRepository cfValueRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<NodeCFTo> findNodeCF(int id) {
        if (id < 1)
            return Optional.empty();

        return nodeCFRepository.findById(id).map(r -> converter.convert(r, NodeCFTo.class));
    }

    @Override
    public Page<NodeCFTo> findNodeCFs(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return nodeCFRepository
                .findByCriteria(NodeCFModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, NodeCFTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public NodeCFTo createNodeCF(NodeCFInput input) {
        
        input.setId(0);

        NodeCFModel model = converter.convert(input, NodeCFModel.class);

        setNodeCFDependencies(model, input);

        model = nodeCFRepository.save(model);

        return converter.convert(model, NodeCFTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public NodeCFTo modifyNodeCF(NodeCFInput input) {
        
        NodeCFModel target = verifyNodeCFModel(input.getId());

        NodeCFModel source = converter.convert(input, NodeCFModel.class);

        Utils.copyNotNulls(source, target);

        setNodeCFDependencies(target, input);

        return converter.convert(nodeCFRepository.save(target), NodeCFTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteNodeCF(int id) {

        NodeCFModel model = verifyNodeCFModel(id);
        
        deleteCFValueByNodeId(id);

        model.setDeleted(true);

        nodeCFRepository.save(model);

        return id;

    }

    @Override
    public void deleteCFValueByNodeId(int nodeCFId) {

       List<CFValueModel> cfValue = cfValueRepository.findByNodeCFIdAndDeletedIsFalse(nodeCFId);
        if (cfValue.size() > 0){
            cfValue.forEach( cf -> {
                cf.setDeleted(true);
            });
            cfValueRepository.saveAll(cfValue);
        }   

    }

    @Override
    public AttributesTo findAttributes(int nodeCFId) {

        NodeCFModel model = verifyNodeCFModel(nodeCFId);

        if (model.getAttributes() == null)
            return null;
        
        return converter.convert(model.getAttributes(), AttributesTo.class);

    }

    @Override
    public CFTypeTo findCFType(int nodeCFId) {

        NodeCFModel model = verifyNodeCFModel(nodeCFId);

        if (model.getCfType() == null)
            return null;
        
        return converter.convert(model.getCfType(), CFTypeTo.class);

    }

    @Override
    public HtmlTagTo findHtmlTag(int nodeCFId) {

        NodeCFModel model = verifyNodeCFModel(nodeCFId);

        if (model.getHtmlTag() == null)
            return null;
        
        return converter.convert(model.getHtmlTag(), HtmlTagTo.class);

    }

    @Override
    public NodeTo findNode(int nodeCFId) {

        NodeCFModel model = verifyNodeCFModel(nodeCFId);

        if (model.getNode() == null)
            return null;
        
        return converter.convert(model.getNode(), NodeTo.class);
        
    } 

    @Override
    public CFValueTo findCFValue(int nodeCFId) {

        // CFValueModel cfValue = cfValueRepository.findByNodeCFId(nodeCFId)
        //     .orElse(null);
        
        // if (cfValue == null)
        //     return null;
        // else
        //     return converter.convert(cfValue, CFValueTo.class);
        return null;
    }

    @Override
    public TenantTo findTenant(int nodeCFId) {
        
        NodeCFModel model = verifyNodeCFModel(nodeCFId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    NodeCFModel verifyNodeCFModel(int nodeCFId) {
        return nodeCFRepository.findById(nodeCFId)
            .orElseThrow(() -> new RuntimeException("NodeCF id " + nodeCFId + " not found"));
    }

    void setNodeCFDependencies(NodeCFModel model, NodeCFInput input) {
        setNodeCFAttributes(model, input.getAttributesId());
        setNodeCFCFType(model, input.getCfTypeId());
        setNodeCFHtmlTag(model, input.getHtmlTagId());
        setNodeCFNode(model, input.getNodeId());
        setNodeCFTenant(model, input.getTenantId());
    }

    void setNodeCFAttributes(NodeCFModel model, Integer attributesId) {
    if (attributesId != null && attributesId > 0) {
        AttributesModel attributes = attributesRepository.findById(attributesId)
            .orElseThrow(() -> new RuntimeException("Attributes id " + attributesId + " not found"));
        model.setAttributes(attributes);
    }
    }

    void setNodeCFCFType(NodeCFModel model, Integer cfTypeId) {
        if (cfTypeId != null && cfTypeId > 0) {
            CFTypeModel cfType = cfTypeRepository.findById(cfTypeId)
                .orElseThrow(() -> new RuntimeException("CFType id " + cfTypeId + " not found"));
            model.setCfType(cfType);
        }
    }

    void setNodeCFHtmlTag(NodeCFModel model, Integer htmlTagId) {
        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
            model.setHtmlTag(htmlTag);
        }
    }

    void setNodeCFNode(NodeCFModel model, Integer nodeId) {
        if (nodeId != null && nodeId > 0) {
            NodeModel node = nodeRepository.findById(nodeId)
                .orElseThrow(() -> new RuntimeException("Node id " + nodeId + " not found"));
            model.setNode(node);
        }
    }

    void setNodeCFTenant(NodeCFModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

}
