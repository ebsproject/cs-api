package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilterTo implements Serializable {
    private int id;
    private String description;
    private Boolean isGraphService;
    private String apiUrl;
    private String paramPath;
    private String body;
    private String paramQuery;
    private String method;
    private Boolean isSystem;
    private String tooltip;
    private TenantTo tenant;
    private List<DomainTo> domains;
}