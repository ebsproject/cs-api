package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Contact extends AuditableTo implements Serializable {

    private static final long serialVersionUID = -263619826;
    private int id;
  
    private CategoryTo category;
    private List<ContactType> contactTypes;
    private List<Address> addresses;
    private List<ContactInfo> contactInfos;
    private List<HierarchyTo> parents;
    private List<HierarchyTo> members;
    /**
     * only when contact's category is 'Person' this attribute is set
     */
    private Person person;
    /**
     * only when contact's category is 'Institution' this attribute is set
     */
    private Institution institution;
    private String email;
    private PurposeTo purpose;
    private Set<TenantTo> tenants;
    private Set<JobLogTo> jobLogs;
    private Set<UserTo> users;
    private List<WorkflowTo> workflows;
}