package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ShipmentItemTo implements Serializable {

    private int id;

    private ShipmentTo shipment;

    private Integer germplasmId;

    private Integer seedId;

    private Integer packageId;

    private Integer number;

    private String code;

    private String status;

    private Integer weight;

    private String packageUnit;

    private Integer packageCount;

    private String testCode;

    private String mtaStatus;

    private String availability;

    private String use;

    private String smtaId;

    private String mlsAncestors;

    private String geneticStock;

    private String remarks;

    private String packageName;

    private String packageLabel;

    private String seedName;

    private String seedCode;

    private String designation;

    private String parentage;
    
    private String taxonomyName;

    private TenantTo tenant;

    private String origin;

}
