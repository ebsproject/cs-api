package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OccurrenceShipmentTo implements Serializable {

    private int id;

    private ServiceTo service;

    private int occurrenceDbId;

    private int occurrenceNumber;

    private String occurrenceName;

    private String experimentName;
   
    private String experimentCode;

    private int experimentYear;

    private String experimentSeason;

    private Contact recipient;

    private String coopkey;

    private boolean validated;

    private Integer weight;

    private String testCode;

    private String status;

    private String unit;

    private List<ServiceItemTo> items;

}