package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EventTo implements Serializable {

    private int id;

    private Date completed;

    private String description;

    private String error;

    private TenantTo tenant;

    private StageTo stage;

    private Integer recordId;

    private NodeTo node;

    private WorkflowInstanceTo workflowInstance;

    private Set<CFValueTo> cfValues;

}
