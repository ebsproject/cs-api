package org.ebs.services.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CropProgramCB {
    private String cropProgramCode;
	private String cropProgramName;
    private CropCB crop;
    private OrganizationCB organization;
}
