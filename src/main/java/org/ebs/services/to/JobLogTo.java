package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JobLogTo implements Serializable {
    private int id;
    private JobWorkflowTo jobWorkflow;
    private UUID jobTrackId;
    private TranslationTo translation;
    private JsonNode message;
    private Date startTime;
    private Date endTime;
    private String status;
    private TenantTo tenant;
    private Set<Contact> contacts;
    private Integer recordId;
}