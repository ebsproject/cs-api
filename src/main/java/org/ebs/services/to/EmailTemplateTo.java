///////////////////////////////////////////////////////////
//  EmailTemplateTo.java
//  Macromedia ActionScript Implementation of the Class EmailTemplateTo
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:56 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:56 AM
 */
@Getter
@Setter
@ToString
public class EmailTemplateTo implements Serializable {

    private static final long serialVersionUID = 176784348;
    private int id;
    private String name;
    private String subject;
    private String template;
    private Set<EntityReferenceTo> entityreferences;
    private Set<TenantTo> tenants;
    private Contact contact;

}