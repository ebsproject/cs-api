package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WorkflowInstanceTo implements Serializable {
    private int id;
    private Date complete;
    private Date initiated;
    private WorkflowTo workflow;
    private StatusTo status;
}
