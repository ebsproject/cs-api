package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Address implements Serializable {

    private int id;

    private String location;

    private String region;

    private String zipCode;

    private String streetAddress;

    private boolean Default;

    private List<Contact> contacts;

    private Country country;

    private List<PurposeTo> purposes;

    private Contact institution;

    private String fullAddress;

    private String additionalStreetAddress;

}