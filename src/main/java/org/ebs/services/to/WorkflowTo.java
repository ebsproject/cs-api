package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WorkflowTo implements Serializable {
    
    private int id;

    private String title;

    private String name;

    private String description;

    private String help;

    private Integer sortNo;

    private String icon;

    private Set<TenantTo> tenants;

    private HtmlTagTo htmlTag;

    private String api;

    private String navigationIcon;

    private String navigationName;

    private ProductTo product;

    private Integer sequence;

    private boolean showMenu;

    private Boolean isSystem;

    private JsonNode securityDefinition;

    private Set<RoleTo> roles;

    private Set<PhaseTo> phases;

    private List<StatusTypeTo> statusTypes;

    private List<ContactWorkflowsTo> workflowContacts;

}
