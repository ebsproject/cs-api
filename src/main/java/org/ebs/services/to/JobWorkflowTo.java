package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JobWorkflowTo implements Serializable {
    private int id;
    private JobTypeTo jobType;
    private ProductFunctionTo productFunction;
    private TranslationTo translation;
    private String name;
    private String description;
    private Integer requestCode;
    private String notes;
    private TenantTo tenant;
    private Set<JobLogTo> jobLogs;
    private EmailTemplateTo emailTemplate;
}