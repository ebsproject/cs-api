///////////////////////////////////////////////////////////
//  SegmentTo.java
//  Macromedia ActionScript Implementation of the Class SegmentTo
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:27 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:27 AM
 */
@Getter @Setter
public class SegmentTo implements Serializable {

	private static final long serialVersionUID = -368262840;
	private int id;
	private int tenant;
	private String name;
	private String tag;
	private boolean isAPI;
	private boolean isAutoNum;
	private boolean isStatic;
	private EntityReferenceTo entityreference;
	private Set<NumberSequenceRuleSegmentTo> numbersequencerulesegments;
	private FormulaTypeTo formulatype;

	@Override
	public String toString(){
		return "SegmentTo [isAPI=" + isAPI + ",isAutoNum=" + isAutoNum + ",isStatic=" + isStatic + ",]";
	}

}