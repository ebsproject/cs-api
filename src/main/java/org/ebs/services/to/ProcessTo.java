package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProcessTo implements Serializable {

    private static final long serialVersionUID = -90959865;
    private int id;
    private int tenant;
    private String name;
    private String description;
    private String code;
    private Boolean isBackground;
    private String dbFunction;
    private Boolean callReport;
    private String path;
}