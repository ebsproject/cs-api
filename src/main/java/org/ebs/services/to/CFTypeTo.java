package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CFTypeTo implements Serializable {

    private int id;
    
    private String name;

    private String description;

    private String help;

    private String type;

    private TenantTo tenant;

}
