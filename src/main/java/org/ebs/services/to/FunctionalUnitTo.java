package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
public class FunctionalUnitTo implements Serializable {

	private static final long serialVersionUID = 109370890;

	public FunctionalUnitTo( int id ) {
		this.id = id;
	}

	private int id;
	private String code;
	private String name;
	private String description;
	private String notes;
	private String type;
	private List<Contact> contacts;
}