package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CFValueTo implements Serializable {

    private int id;
    
    private Boolean flagValue;
    
    private String textValue;

    private Integer numValue;

    private Date dateValue;

    private Integer codeValue;

    private NodeCFTo nodeCF;

    private EventTo event;

    private TenantTo tenant;

}
