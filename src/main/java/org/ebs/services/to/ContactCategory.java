package org.ebs.services.to;

/**
 * Specifies the general classification of a contact: Person or Institution
 */
public enum ContactCategory {
    Institution, Person;
}
