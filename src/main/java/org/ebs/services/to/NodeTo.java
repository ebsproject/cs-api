package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NodeTo implements Serializable {

    private int id;
    
    private String name;

    private String description;

    private String help;

    private Integer sequence;

    private Boolean requireApproval;

    private HtmlTagTo htmlTag;

    private ProductTo product;
    
    private WorkflowTo workflow;

    private ProcessTo process;

    private JsonNode define;

    private JsonNode dependOn;

    private String icon;

    private JsonNode message;

    private JsonNode requireInput;

    private JsonNode securityDefinition;

    private String validationCode;

    private String validationType;

    private UUID designRef;

    private WorkflowViewTypeTo workflowViewType;

    private NodeTypeTo nodeType;

    private Set<NodeCFTo> nodeCFs;

    private TenantTo tenant;

}
