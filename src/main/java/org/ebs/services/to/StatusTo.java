package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StatusTo implements Serializable {

    private int id;
    
    private Date initiateDate;
    
    private Date completionDate;
    
    private Integer recordId;
    
    private StatusTypeTo statusType;
    
    private WorkflowInstanceTo workflowInstance;

    private TenantTo tenant;

}
