package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EntityReferenceTo implements Serializable {

    private static final long serialVersionUID = 320754037;
    private int id;
    private int tenant;
    private String entity;
    private String textfield;
    private String valuefield;
    private String storefield;
    private String entitySchema;
    private Set<AttributesTo> attributess;
    private Set<EmailTemplateTo> emailtemplates;
    private Set<DomainTo> domains;
    private Set<SegmentTo> segments;
    private Set<NumberSequenceRuleTo> numbersequencerules;
    private String query;
}