package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ServiceTo implements Serializable {

    private int id;

    private Integer recordId;

    private String description;

    private String requester;

    private String requestCode;

    private Date submitionDate;

    private String adminContact;

    private String chargeAccount;

    private Person person;

    private WorkflowInstanceTo workflowInstance;

    private TenantTo tenant;

    private Contact sender;

    private Contact requestor;

    private Contact recipient;

    private List<ServiceItemTo> items;

    private ProgramTo program;

    private Contact serviceProvider;

    private boolean byOccurrence;

}
