///////////////////////////////////////////////////////////
//  MessageTo.java
//  Macromedia ActionScript Implementation of the Class MessageTo
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:05 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:05 AM
 */
@Getter @Setter
public class MessageTo implements Serializable {

	private static final long serialVersionUID = -303628647;
	private int id;
	private String description;
	private String sql;
	private String filterClause;
	private HtmlTagTo htmltag;

	@Override
	public String toString(){
		return "MessageTo []";
	}

}