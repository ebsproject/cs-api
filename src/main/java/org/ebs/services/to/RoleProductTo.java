package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RoleProductTo  implements Serializable {
    private int id;
    private int roleId;
    private int productFunctionId;
    private RoleTo role;
    private ProductFunctionTo productFunction;
}
