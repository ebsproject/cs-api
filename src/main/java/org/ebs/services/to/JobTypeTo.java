package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JobTypeTo implements Serializable {
    private int id;
    private String name;
    private String description;
    //private TenantTo tenant;
}