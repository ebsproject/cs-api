package org.ebs.services.to;

import java.io.Serializable;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NodeCFTo implements Serializable {

    private int id;
    
    private String name;

    private String description;

    private String help;

    private Boolean required;

    private JsonNode fieldAttributes;

    private String apiAttributesName;

    private AttributesTo attributes;

    private CFTypeTo cfType;

    private HtmlTagTo htmlTag;

    private NodeTo node;

    private CFValueTo cfValue;

    private TenantTo tenant;

}
