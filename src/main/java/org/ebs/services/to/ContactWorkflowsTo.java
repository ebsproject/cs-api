package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContactWorkflowsTo  implements Serializable {
    private Integer id;
    private Contact contact;
    private WorkflowTo workflow;
    private boolean provide;
}
