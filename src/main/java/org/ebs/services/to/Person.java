package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Person implements Serializable {

    private int id;

    private String familyName;

    private String givenName;

    private String additionalName;

    private String fullName;

    private String gender = "NA";

    private String jobTitle;

    private String knowsAbout;

    private Contact contact;

    private String salutation;

    private String fullNameWithCoopkey;

}