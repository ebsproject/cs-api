package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContactInfo implements Serializable {

    private int id;

    private String value;

    private boolean Default;

    private ContactInfoType contactInfoType;

    private Contact contact;

}