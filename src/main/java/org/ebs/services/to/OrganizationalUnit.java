package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OrganizationalUnit implements Serializable {
    private static final long serialVersionUID = -445709000;
    private int id;
    private int unitId;
    private Integer unitRecordId;
    private String organization;
    private String unitName;
    private String unitAbbreviation;
    private String unitType;
    // private List<Team> teams;
    // private List<HierarchyTreeAttributesValuesTo> attributes;
}
