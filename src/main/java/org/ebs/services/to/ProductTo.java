package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductTo implements Serializable {

    private static final long serialVersionUID = 325189645;
    private int id;
    private int menuOrder;
    private String name;
    private String description;
    private String help;
    private String path;
    private String icon;
    private DomainTo domain;
    private HtmlTagTo htmltag;
    private Set<ProductFunctionTo> productfunctions;
    private String abbreviation;
    private EntityReferenceTo entityReference;
    private Boolean hasWorkflow;
    private Boolean isExternal;
    private Boolean isRedirect;
    private Integer externalId;

    public ProductTo(int id) {
        this.id = id;
    }
}