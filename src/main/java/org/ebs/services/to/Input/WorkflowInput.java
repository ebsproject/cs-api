package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowInput implements Serializable {

    private int id;

    private String title;

    private String name;

    private String description;

    private String help;

    private Integer sortNo;

    private String icon;

    private Set<Integer> tenantIds;

    private Integer htmlTagId;

    private String api;

    private String navigationIcon;

    private String navigationName;

    private Integer productId;

    private Integer sequence;

    private boolean showMenu;

    private Boolean system;

    private JsonNode securityDefinition;

    private Set<Integer> roleIds;

}
