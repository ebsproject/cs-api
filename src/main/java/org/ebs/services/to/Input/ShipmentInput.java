package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipmentInput implements Serializable {

    private int id;

    private Integer workflowInstanceId;

    private Integer programId;

    private Integer projectId;

    private Integer senderId;

    private Integer processorId;

    private Integer requesterId;

    private Integer recipientId;

    private String code;

    private String name;

    private String shuReferenceNumber;

    private String purpose;

    private String materialType;

    private String materialSubtype;

    private Date documentGeneratedDate;

    private Date documentSignedDate;

    private String authorizedSignatory;

    private Date shippedDate;

    private Date receivedDate;

    private String airwayBillNumber;

    private String remarks;

    private String recipientInstitution;

    private int tenantId;

    private Integer addressId;

    private Integer senderAddressId;

    private Integer requestorAddressId;

    private JsonNode institutionData;

}
