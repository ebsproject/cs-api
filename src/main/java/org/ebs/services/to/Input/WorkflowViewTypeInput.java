package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowViewTypeInput implements Serializable {

    private int id;
    
    private String name;

    private String notes;

    private int tenantId;

}
