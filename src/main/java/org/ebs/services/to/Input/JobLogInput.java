package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobLogInput implements Serializable {

	private static final long serialVersionUID = 71433201;
	private int id;
    private Integer jobWorkflowId;
    private UUID jobTrackId;
    private Integer translationId;
    private JsonNode message;
    private String startTime;
    private String endTime;
    private String status;
	private Integer tenantId;
    private Set<Integer> contactIds;
    private Integer recordId;
    
}