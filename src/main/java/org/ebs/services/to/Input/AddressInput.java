package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddressInput implements Serializable {

    private int id;

    private String location;

    private String region;

    private String zipCode;

    private String streetAddress;

    private boolean Default;

    private Integer countryId;

    private Set<Integer> purposeIds;

    private String additionalStreetAddress;

}