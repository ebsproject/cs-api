package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContactInfoInput implements Serializable {

    private int id;

    private String value;

    private boolean Default;

    private int contactInfoTypeId;

    private Integer contactId;

}