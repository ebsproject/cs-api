package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ShipmentFileInput implements Serializable {

    private int id;

    private Integer shipmentId;

    private UUID fileId;

    private String remarks;

    private Integer tenantId;
    
}
