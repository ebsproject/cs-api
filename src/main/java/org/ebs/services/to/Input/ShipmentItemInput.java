package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentItemInput implements Serializable {

    private int id;

    private Integer shipmentId;

    private Integer germplasmId;

    private Integer seedId;

    private Integer packageId;

    private Integer number;

    private String code;

    private String status;

    private Integer weight;

    private String packageUnit;

    private Integer packageCount;

    private String testCode;

    private String mtaStatus;

    private String availability;

    private String use;

    private String smtaId;

    private String mlsAncestors;

    private String geneticStock;

    private String remarks;

    private Integer tenantId;

    private String packageName;

    private String packageLabel;

    private String seedName;

    private String seedCode;

    private String designation;

    private String parentage;
    
    private String taxonomyName;

    private String origin;

}
