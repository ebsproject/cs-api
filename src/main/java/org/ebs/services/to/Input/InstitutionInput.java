package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InstitutionInput implements Serializable {

    private String legalName;

    private String commonName;

    private boolean cgiar;

    private Integer externalCode;

    private Set<Integer> cropIds;

    private int unitTypeId;
  
}