package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ContactInput implements Serializable {

    private static final long serialVersionUID = -263619826;
    private int id;  
    private CategoryInput category;
    private List<Integer> contactTypeIds;
    private List<AddressInput> addresses;
    private List<ContactInfoInput> contactInfos;
    /**
     * only when contact's category is 'Person' this attribute is required
     */
    private PersonInput person;
    /**
     * only when contact's category is 'Institution' this attribute is required
     */
    private InstitutionInput institution;

    private String email;

    private Set<Integer> purposeIds;

    private Set<Integer> tenantIds;
}