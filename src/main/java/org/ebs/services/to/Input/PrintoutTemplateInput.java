
package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;

@Data
public class PrintoutTemplateInput implements Serializable {

	private static final long serialVersionUID = 11253653;

	private int id;
	private String name;
	private String description;
	private String zpl;
	private String defaultFormat;
	private String label;
	private Set<Integer> tenantIds;
}
