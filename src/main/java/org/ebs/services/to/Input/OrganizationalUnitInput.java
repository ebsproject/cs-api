package org.ebs.services.to.Input;

import java.io.Serializable;

import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class OrganizationalUnitInput implements Serializable {
    private static final long serialVersionUID = 01433201;
    private ContactInput contact;
    private int parentOrganizationId;
    private Set<Integer> serviceIds;
    private Set<UnitMemberInput> unitMembers;
    private Set<Integer> workflowIds;
}
