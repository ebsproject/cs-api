///////////////////////////////////////////////////////////
//  NumberSequenceRuleSegmentInput.java
//  Macromedia ActionScript Implementation of the Class NumberSequenceRuleSegmentInput
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:10 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:10 AM
 */
@Getter @Setter
public class NumberSequenceRuleSegmentInput implements Serializable {

	private static final long serialVersionUID = 440631775;
	private int id;
	private int tenant;
	private int length;
	private String customField;
	private int position;
	private String formula;
	private SegmentInput segment;
	private NumberSequenceRuleInput numbersequencerule;

}