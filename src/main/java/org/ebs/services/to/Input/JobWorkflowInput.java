package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobWorkflowInput implements Serializable {

	private static final long serialVersionUID = 71433200;
	private int id;
    private Integer jobTypeId;
    private Integer productFunctionId;
    private Integer translationId;
    private String name;
    private String description;
    private Integer requestCode;
    private String notes;
	private Integer tenantId;
    private Integer emailTemplateId;
    
}