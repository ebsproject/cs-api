package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceInput implements Serializable {

    private int id;

    private Integer recordId;

    private String description;

    private String requester;

    private String requestCode;

    private Date submitionDate;

    private String adminContact;

    private String chargeAccount;

    private Integer personId;

    private Integer workflowInstanceId;

    private int tenantId;

    private Integer senderId;

    private Integer recipientId;

    private Integer requestorId;

    private Integer programId;

    private Integer serviceProviderId;

    private boolean byOccurrence;

}
