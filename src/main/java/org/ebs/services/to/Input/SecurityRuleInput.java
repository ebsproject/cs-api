
package org.ebs.services.to.Input;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SecurityRuleInput implements Serializable {

	private static final long serialVersionUID = -9247104;
	private int id;
	private String name;
    private String description;
    private Boolean usedByWorkflow;

}