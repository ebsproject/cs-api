package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PersonInput implements Serializable {

    private String familyName;

    private String givenName;

    private String additionalName;

    private String gender = "NA";

    private String jobTitle;

    private String knowsAbout;

    private String salutation;
    
}