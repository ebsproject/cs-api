package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilterEntityInput implements Serializable {
    private int id;
    private String description;
    private Boolean graphService;
    private String apiUrl;
    private String paramPath;
    private String body;
    private String paramQuery;
    private String method;
    private Boolean system;
    private String tooltip;
    private Integer tenantId;
    private List<Integer> domainIds;
}
