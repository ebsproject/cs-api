package org.ebs.services.to.Input;

import java.io.Serializable;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NodeCFInput implements Serializable {

    private int id;
    
    private String name;

    private String description;

    private String help;

    private Boolean required;

    private JsonNode fieldAttributes;

    private String apiAttributesName;

    private Integer attributesId;

    private Integer cfTypeId;

    private Integer htmlTagId;

    private Integer nodeId;

    private int tenantId;

}
