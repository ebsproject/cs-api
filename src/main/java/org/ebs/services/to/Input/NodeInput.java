package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NodeInput implements Serializable {

    private int id;
    
    private String name;

    private String description;

    private String help;

    private Integer sequence;

    private Boolean requireApproval;

    private Integer htmlTagId;

    private Integer productId;
    
    private Integer workflowId;

    private Integer processId;

    private JsonNode define;

    private JsonNode dependOn;

    private String icon;

    private JsonNode message;

    private JsonNode requireInput;

    private JsonNode securityDefinition;

    private String validationCode;

    private String validationType;

    private UUID designRef;

    private int workflowViewTypeId;

    private int nodeTypeId;

    private int tenantId;

}
