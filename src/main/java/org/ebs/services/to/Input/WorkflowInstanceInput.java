package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkflowInstanceInput implements Serializable {

    private int id;
    private Date complete;
    private Date initiated;
    private int workflowId;
    
}
