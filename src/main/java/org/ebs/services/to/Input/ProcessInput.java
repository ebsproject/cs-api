///////////////////////////////////////////////////////////
//  ProcessInput.java
//  Macromedia ActionScript Implementation of the Class ProcessInput
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:17 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:17 AM
 */
@Getter @Setter
public class ProcessInput implements Serializable {

	private static final long serialVersionUID = 153607325;
	private int id;
	private int tenant;
	private String name;
	private String description;
	private String code;
	private Boolean isBackground;
	private String dbFunction;
	private Boolean callReport;
	private String path;

}