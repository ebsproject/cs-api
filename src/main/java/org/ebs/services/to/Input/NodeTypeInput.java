package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NodeTypeInput implements Serializable {

    private int id;
    
    private String name;

    private String description;

}
