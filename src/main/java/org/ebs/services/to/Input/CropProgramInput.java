package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Data;

@Data
public class CropProgramInput implements Serializable {

	private static final long serialVersionUID = 46606863011L;
	private int id;
	private String code;
	private String name;
	private String description;
	private String notes;
	private int tenantId;
	private int cropId;
}