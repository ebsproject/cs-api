package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.ToString;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 14-Apr-2023 8:15:56 AM
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StageInput implements Serializable {

    private int id;

    private String name;

    private String description;

    private String help;

    private Integer sequence;

    private Integer parentId;

    private int tenantId;

    private Integer htmlTagId;

    private Integer phaseId;

    private JsonNode dependOn;

    private String icon;

    private UUID designRef;

    private Integer workflowViewTypeId;

    private Set<Integer> nodeIds;

}