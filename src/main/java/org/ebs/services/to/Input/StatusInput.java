package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusInput implements Serializable {

    private int id;
    
    private Date initiateDate;
    
    private Date completionDate;
    
    private Integer recordId;
    
    private Integer statusTypeId;
    
    private Integer workflowInstanceId;

    private int tenantId;

}
