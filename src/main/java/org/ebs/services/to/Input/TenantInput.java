///////////////////////////////////////////////////////////
//  TenantInput.java
//  Macromedia ActionScript Implementation of the Class TenantInput
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:31 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:31 AM
 */
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class TenantInput implements Serializable {

	private static final long serialVersionUID = 322741206;
	private int id;
	private String name;
	private Date expiration;
	private boolean expired;
	private int organizationId;
	private int customerId;
	private Integer cropId;
	private Boolean sync;
}