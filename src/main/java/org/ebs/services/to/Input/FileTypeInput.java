package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FileTypeInput implements Serializable {
    private static final long serialVersionUID = -479503200;
	private int id;
	private String description;
	private Integer workflowId;
}
