package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CFTypeInput implements Serializable {

    private int id;
    
    private String name;

    private String description;

    private String help;

    private String type;

    private int tenantId;

}
