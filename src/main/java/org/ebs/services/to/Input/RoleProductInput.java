package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RoleProductInput  implements Serializable {
    private RoleInput role;
    private Set<ProductFunctionInput> productFunction;
}
