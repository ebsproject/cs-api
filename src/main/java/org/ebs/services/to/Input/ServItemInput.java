package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ServItemInput implements Serializable {
    private int id;
    private Integer number;
    private String status;
    private String mtaStatus;
    private Integer weight;
    private String packageUnit;
    private Integer packageCount;
    private String availability;
    private String use;
    private String remarks;
    private String testCode;
    private String packageName;
    private String packageLabel;
    private String seedName;
    private String seedCode;
    private String code;
    private Boolean isDangerous;
    private Date receivedDate;
    private Integer listId;
    private String germplasmName;
    private String otherNames;
    private String sourceStudy;
    private String referenceNumber;
    private String parentage;    
    private String taxonomyName;
    private String origin;
    private String geneticStock;
    private String mlsAncestors;
}
