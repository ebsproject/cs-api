package org.ebs.services.to.Input;

import java.util.Set;
import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UnitMemberInput implements Serializable {
    private static final long serialVersionUID = 01433202;
    private int memberId;
    private Set<Integer> roleIds;
}
