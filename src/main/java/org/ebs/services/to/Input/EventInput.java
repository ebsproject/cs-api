package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventInput implements Serializable {
    
    private int id;

    private Date completed;

    private String description;

    private String error;

    private int tenantId;

    private Integer stageId;

    private Integer recordId;

    private Integer nodeId;

    private Integer workflowInstanceId;

}
