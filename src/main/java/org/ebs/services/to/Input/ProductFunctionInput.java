package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProductFunctionInput implements Serializable {

    private static final long serialVersionUID = 70147660;
    private int id;
    private String description;
    private boolean systemType;
    private String action;
    private Set<RoleInput> roles;
    private ProductInput product;
    private boolean dataAction;

}