package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class OccurrenceShipmentInput implements Serializable {

    private int id;

    private Integer serviceId;

    private int occurrenceDbId;

    private int occurrenceNumber;

    private String occurrenceName;

    private String experimentName;
   
    private String experimentCode;

    private int experimentYear;

    private String experimentSeason;

    private Integer recipientId;

    private String coopkey;

    private Boolean validated;

    private Integer weight;

    private String testCode;

    private String status;

    private String unit;

}
