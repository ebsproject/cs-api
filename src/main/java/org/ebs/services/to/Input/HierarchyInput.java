package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HierarchyInput  implements Serializable {
    private int contactId;
    private int institutionId;
    private boolean principalContact;
}
