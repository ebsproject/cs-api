///////////////////////////////////////////////////////////
//  SeasonInput.java
//  Macromedia ActionScript Implementation of the Class SeasonInput
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:25 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:25 AM
 */
@Getter @Setter
public class SeasonInput implements Serializable {

	private static final long serialVersionUID = -282341853;
	private int id;
	private int tenant;
	private String code;
	private String name;
	private String description;
	private String notes;

}