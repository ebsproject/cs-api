package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 14-Apr-2023 8:15:56 AM
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PhaseInput implements Serializable {

    private int id;

    private String name;

    private String description;

    private String help;

    private Integer sequence;

    private int tenantId;

    private Integer htmlTagId;

    private int workflowId;

    private JsonNode dependOn;

    private String icon;

    private UUID designRef;

    private Integer workflowViewTypeId;

}