package org.ebs.services.to.Input;

import java.io.Serializable;
import java.sql.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CFValueInput implements Serializable {

    private int id;
    
    private Boolean flagValue;
    
    private String textValue;

    private Integer numValue;

    private Date dateValue;

    private Integer codeValue;

    private Integer nodeCFId;

    private Integer eventId;

    private int tenantId;

}
