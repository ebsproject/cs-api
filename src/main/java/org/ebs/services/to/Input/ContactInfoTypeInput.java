package org.ebs.services.to.Input;

import org.ebs.services.to.ContactInfoType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactInfoTypeInput extends ContactInfoType{
    
}
