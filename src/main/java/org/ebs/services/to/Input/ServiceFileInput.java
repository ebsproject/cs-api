package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceFileInput implements Serializable {

    private int id;

    private Integer serviceId;

    private UUID fileId;

    private String remarks;

    private Integer tenantId;

    private Integer fileTypeId;

    private Boolean isLegalDocument;
    
}
