package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ShipItemInput implements Serializable {
    private int id;
    private Integer number;
    private String status;
    private Integer weight;
    private String packageUnit;
    private Integer packageCount;
    private String availability;
    private String use;
    private String remarks;
    private String testCode;
    private String packageName;
    private String packageLabel;
    private String seedName;
    private String seedCode;
    private String code;

}
