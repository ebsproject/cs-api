package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PersonSync implements Serializable {

	private String email;
	private String firstName;
	private int id;
	private Boolean isActive;
	private boolean deleted;
	private String lastName;
	private String personName;
	private String personType;
	private Set<ProgramPersonTo> programs;
	private String username;

}