package org.ebs.services.to;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AuditableTo {
    private UserTo createdBy;
    private UserTo updatedBy;
    private Date createdOn;
    private Date updatedOn;
}
