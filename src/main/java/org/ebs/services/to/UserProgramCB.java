package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a user from Core Breeding domain
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserProgramCB implements Serializable {

    private Integer programDbId;
    private String programCode;
    private String programName;
    private String programStatus;
    private String programType;
    private String personType;

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != UserProgramCB.class)
            return false;

            UserProgramCB u = (UserProgramCB) o;

        return programDbId.equals(u.getProgramDbId()) && programCode.equals(u.getProgramCode())
                && programName.equals(u.getProgramName()) && programStatus.equals(u.getProgramStatus())
                && programType.equals(u.getProgramType());

    }
}