package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IWINOccurrenceTo implements Serializable {
    private String sowingDate;
    private String coopkey;
    private String recipientName;
    private String recipientCountry;
    private String sendViaCoopkey;
    private String sendViaName;
    private String sendViaCountry;
    private String experimentName;
    private String experimentCode;
    private int experimentYear;
    private String experimentSeason;
    private String occurrenceName;
    private int occurrenceNumber;
    private int occurrenceId;
    private String programCode;
}
