package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a user from Core Breeding domain
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserCB implements Serializable {

    private Integer personDbId;
    private String email;
    private String username;
    private String firstName;
    private String lastName;
    private String personType;
    private Integer personRoleId;
    private boolean isActive;

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != UserCB.class)
            return false;

        UserCB u = (UserCB) o;

        return personDbId.equals(u.getPersonDbId()) && email.equals(u.getEmail())
                && firstName.equals(u.getFirstName()) && lastName.equals(u.getLastName())
                && personType.equals(u.getPersonType()) && isActive == u.isActive();

    }
    public void setIsActive(boolean isActive){
        this.isActive=isActive;
    }
}