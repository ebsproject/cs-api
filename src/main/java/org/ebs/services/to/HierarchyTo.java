package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class HierarchyTo implements Serializable {
    private int id;
    private int contactId;
    private int institutionId;
    private Contact contact;
    private Contact institution;
    private boolean isPrincipalContact;
    private Set<RoleTo> roles;
}
