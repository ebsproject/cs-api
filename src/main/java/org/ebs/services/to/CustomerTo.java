package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CustomerTo extends AuditableTo implements Serializable {

    private static final long serialVersionUID = 75375363;
    private int id;
    private String name;
    private String phone;
    private String officialEmail;
    private String alternateEmail;
    private String jobTitle;
    private String languagePreference;
    private Long phoneExtension;
    private boolean active;
    private String logo;
    private Integer owningOrganizationId;
}