package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LanguageTo implements Serializable {

    private static final long serialVersionUID = -212168519;
    private int id;
    private String name;
    private String codeIso;
    private String isBase;
    private Set<TranslationTo> translations;
    /**
     * this relationship should be to 'user'
     */
    private Set<Contact> contacts;

}