package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FileTypeTo implements Serializable {
    private static final long serialVersionUID = 201039000;
	private int id;
	private String description;
    private WorkflowTo workflow;
}
