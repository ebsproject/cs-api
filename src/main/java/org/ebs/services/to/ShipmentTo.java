package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ShipmentTo implements Serializable {

    private int id;

    private WorkflowInstanceTo workflowInstance;

    private ProgramTo program;

    private Integer projectId;

    private Contact sender;

    private Contact processor;

    private Contact requester;

    private Contact recipient;

    private String code;

    private String name;

    private String shuReferenceNumber;

    private String purpose;

    private String materialType;

    private String materialSubtype;

    private Date documentGeneratedDate;

    private Date documentSignedDate;

    private String authorizedSignatory;

    private Date shippedDate;

    private Date receivedDate;

    private String airwayBillNumber;

    private String remarks;

    private String recipientInstitution;

    private TenantTo tenant;

    private String status;

    private Address address;

    private Address senderAddress;

    private Address requestorAddress;

    private JsonNode institutionData;

    private List<ShipmentItemTo> items;
    
}
