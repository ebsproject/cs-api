package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TenantTo extends AuditableTo implements Serializable {

    private static final long serialVersionUID = -121994026;
    private int id;
    private String name;
    private Date expiration;
    private boolean expired;
    private Set<EmailTemplateTo> emailtemplates;
    private Set<ProgramTo> programs;
    private Set<UserTo> users;
    private Set<InstanceTo> instances;
    private OrganizationTo organization;
    private Set<NumberSequenceRuleTo> numbersequencerules;
    private CustomerTo customer;
    private Boolean sync;
}