
package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProductFunctionTo implements Serializable {

    private static final long serialVersionUID = -497452569;
    private int id;
    private String description;
    private boolean systemType;
    private String action;
    private Set<RoleTo> roles;
    private ProductTo product;
    private boolean isDataAction;

}