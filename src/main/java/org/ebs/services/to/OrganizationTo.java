package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OrganizationTo implements Serializable {

    private static final long serialVersionUID = -445709211;
    private int id;
    private String legalName;
    private String phone;
    private String webPage;
    private String slogan;
    private String name;
    private String taxId;
    private String logo;
    private boolean active;
    private Integer defaultAuthenticationId;
    private Integer defaultThemeId;
    private String code;

}