package org.ebs.services.to;

import java.io.Serializable;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FileObjectTo implements Serializable {
    
    private UUID id;

    private String name;

    private int ownerId;

    private long size;

    private String tags;

    private String description;

    private int status;

    private TenantTo tenant;

}
