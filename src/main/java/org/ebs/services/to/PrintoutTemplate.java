package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;

@Data
public class PrintoutTemplate extends AuditableTo implements Serializable {

	private static final long serialVersionUID = 324189145;
	private int id;
	private String name;
	private String description;
	private String zpl;
	private String defaultFormat;
	private String label;
	private Set<ProgramTo> programs;
	private Set<ProductTo> products;
	private Set<TenantTo> tenants;
}