package org.ebs.services.to;

import java.io.Serializable;

import lombok.Data;

@Data
public class CropProgramTo implements Serializable {

	private static final long serialVersionUID = 46606863011L;
	private int id;
	private String code;
	private String name;
	private String description;
	private String notes;
	private int tenantId;

}