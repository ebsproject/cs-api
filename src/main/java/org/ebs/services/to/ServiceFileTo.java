package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ServiceFileTo extends AuditableTo implements Serializable {
    
    private int id;

    private ServiceTo service;

    private FileObjectTo file;

    private String remarks;

    private TenantTo tenant;

    private FileTypeTo fileType;

    private Boolean isLegalDocument;

}
