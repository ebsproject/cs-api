package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HtmlTagTo implements Serializable {

    private static final long serialVersionUID = 45399455;
    private int id;
    private String tagName;
    private Set<AttributesTo> attributess;
    private Set<MessageTo> messages;
    private Set<DomainTo> domains;
    private Set<AlertTo> alerts;
    private Set<TranslationTo> translations;
    private Set<ProductTo> products;

}