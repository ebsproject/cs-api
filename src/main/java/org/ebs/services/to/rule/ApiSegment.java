package org.ebs.services.to.rule;

import org.ebs.model.rule.ApiSegmentModel.ApiType;
import org.springframework.http.HttpMethod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ApiSegment extends Segment {
    public enum Module {
        CS("cs", 2), 
        CB("cb", 1), 
        CBGRAPH("cbgraphql", 0), 
        SM("sm", 4), 
        MARKERDB("smmarkerdb", 4), 
        FILE("file", 0);
        
        private String prefix;
        private int domainId;
        
        private Module(String prefix, int domainId) {
            this.prefix = prefix;
            this.domainId = domainId;
        }

        public String prefix() {
            return prefix;
        }
        public int domainId() {
            return domainId;
        }

    }
    private ApiType type;
    private HttpMethod method;
    private String serverUrl;
    private String pathTemplate;
    private String bodyTemplate;
    private String responseMapping;

}
