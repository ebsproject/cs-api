package org.ebs.services.to.rule;

import java.util.List;

import org.ebs.model.rule.SegmentModel.DataType;

import lombok.Data;

@Data
public abstract class Segment {

    private Integer id;
    private String name;
    private String format;
    private String formula;
    private boolean requiresInput;
    private DataType dataType;
    private boolean displayed;
    private Integer family;
    private boolean required;
    private int position;
    private String skip;
    private Integer skipInputSegmentId;
    private List<String> scale;
}
