package org.ebs.services.to.rule;

import java.util.List;

import lombok.Data;

@Data
public class SequenceRule {
    
    private int id;
    private String name;
    private List<Segment> segments;
}

