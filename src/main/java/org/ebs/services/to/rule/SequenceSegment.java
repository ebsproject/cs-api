package org.ebs.services.to.rule;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * When first created, last = lowest - increment, so the first
 * time last is computed returns lowest as first value of the sequence
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor @NoArgsConstructor
public class SequenceSegment extends Segment {

    private int lowest;
    private int increment;
    private int last;
    private JsonNode resetCondition;
}
