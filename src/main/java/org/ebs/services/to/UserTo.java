package org.ebs.services.to;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserTo extends AuditableTo implements Serializable {

    private static final long serialVersionUID = 40425027;

    public UserTo(int id) {
        this.id = id;
    }

    private int id;
    private String userName;
    private Date lastAccess;
    private RoleTo defaultRole;
    private Integer isIs;
    private Set<DelegationTo> delegations;
    private Set<TenantTo> tenants;
    private Set<FunctionalUnitTo> functionalunits;
    private Contact contact;
    private Integer externalId;
    private boolean isActive;
    private JsonNode preference;
    private boolean isAdmin;
}