package org.ebs.services.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProgramPersonTo {
    
	private int programDbId;
	private String programCode;
	private String programName;
	private String programType;
	private String personRole;
}
