package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ShipmentFileTo implements Serializable {
    
    private int id;

    private ShipmentTo shipment;

    private FileObjectTo file;

    private String remarks;

    private TenantTo tenant;

}
