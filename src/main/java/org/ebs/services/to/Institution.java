package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Institution implements Serializable {

    private Integer id;

    private String commonName;

    private String legalName;
    
    private Contact contact;

    private boolean isCgiar;

    private Integer externalCode;

    private List<CropTo> crops;

    private UnitTypeTo unitType;
    
}