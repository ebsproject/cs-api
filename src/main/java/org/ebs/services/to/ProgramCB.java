package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a program from Core Breeding domain
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProgramCB implements Serializable {

    private Integer programDbId;
    private String programCode;
    private String programName;
    private String programType;
    private String programStatus;
    private String description;
    private CropProgramCB cropProgram;

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != ProgramCB.class)
            return false;

        ProgramCB p = (ProgramCB) o;

        return programDbId == p.getProgramDbId() && programCode.equals(p.getProgramCode())
                && ((programName == null && p.getProgramName() == null)
                        || programName.equals(p.getProgramName()))
                && ((description == null && p.getDescription() == null)
                        || description.equals(p.getDescription()))
                && programType.equals(p.getProgramType())
                && programStatus.equals(p.getProgramStatus());
    }

}