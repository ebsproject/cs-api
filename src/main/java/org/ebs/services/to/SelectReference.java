package org.ebs.services.to;

import java.io.Serializable;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@SqlResultSetMapping(name = "SelectReference", classes = @ConstructorResult(targetClass = SelectReference.class, columns = { @ColumnResult(name = "id", type = Integer.class),
        @ColumnResult(name = "description", type = String.class) }))
public class SelectReference implements Serializable{
  @Id  
  private int id;  
  private String description;
}
