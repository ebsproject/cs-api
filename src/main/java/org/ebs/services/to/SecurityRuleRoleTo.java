package org.ebs.services.to;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SecurityRuleRoleTo {
  private int id;
  private RoleTo role;
  private SecurityRuleTo rule;
}