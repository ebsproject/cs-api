///////////////////////////////////////////////////////////
//  DomainInstanceTo.java
//  Macromedia ActionScript Implementation of the Class DomainInstanceTo
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:54 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:54 AM
 */
@Getter @Setter @ToString
public class DomainInstanceTo implements Serializable {

	private static final long serialVersionUID = 495381780;
	private int id;
	private int tenantId;
	private int domainId;
	private String context;
	private String sgContext;
	private boolean mfe;
}