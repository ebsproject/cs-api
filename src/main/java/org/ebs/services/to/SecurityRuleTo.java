package org.ebs.services.to;
import java.io.Serializable;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SecurityRuleTo extends AuditableTo implements Serializable  {
  private static final long serialVersionUID = -464450627;
  private int id;
  private String name;
  private String description;
  private boolean usedByWorkflow;
}