package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StageTo implements Serializable {
    
    private int id;

    private String name;

    private String description;

    private String help;

    private Integer sequence;

    private StageTo parent;

    private TenantTo tenant;

    private HtmlTagTo htmlTag;

    private PhaseTo phase;

    private JsonNode dependOn;

    private String icon;

    private UUID designRef;

    private WorkflowViewTypeTo workflowViewType;

    private Set<NodeTo> nodes;

}
