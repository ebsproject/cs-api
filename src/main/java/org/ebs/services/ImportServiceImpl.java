package org.ebs.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.graphql.client.GraphQLClient;
import org.ebs.services.to.Input.CategoryInput;
import org.ebs.services.to.Input.ContactInput;
import org.ebs.services.to.Input.InstitutionInput;
import org.ebs.services.to.Input.PersonInput;
import org.ebs.services.to.Input.ServiceItemInput;
import org.ebs.services.to.Input.ShipmentItemInput;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class ImportServiceImpl implements ImportService {

    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;
    private final ShipmentItemService shipmentItemService;
    private final ServiceItemService serviceItemService;
    private final ContactService contactService;
    private final GraphQLClient graphQLClient;
    protected final TokenGenerator tokenGenerator;

    @Override
    public String importShipmentItems(MultipartFile file, String entity, int shipmentId) throws Exception {
        switch(entity) {
            case "shipment":
                List<ShipmentItemInput> shipmentItems = new ArrayList<>();
                new BufferedReader(new InputStreamReader(file.getInputStream()))
                .lines().forEach(line -> {
                    String[] fields = line.split(",");
                    ShipmentItemInput input = new ShipmentItemInput();
                    input.setShipmentId(shipmentId);
                    input.setGermplasmId(Integer.parseInt(fields[0]));
                    input.setSeedId(Integer.parseInt(fields[1]));
                    input.setNumber(Integer.parseInt(fields[2]));
                    input.setCode(fields[3]);
                    input.setStatus(fields[4]);
                    input.setWeight(Integer.parseInt(fields[5]));
                    input.setPackageUnit(fields[6]);
                    input.setPackageCount(Integer.parseInt(fields[7]));
                    input.setTenantId(1);
                    shipmentItems.add(input);
                });
                shipmentItemService.createShipmentItems(shipmentItems);
                break;
            case "workflow":
                List<ServiceItemInput> items = new ArrayList<>();
                new BufferedReader(new InputStreamReader(file.getInputStream()))
                .lines().forEach(line -> {
                    String[] fields = line.split(",");
                    ServiceItemInput input = new ServiceItemInput();
                    input.setServiceId(shipmentId);
                    input.setGermplasmId(Integer.parseInt(fields[0]));
                    input.setSeedId(Integer.parseInt(fields[1]));
                    input.setNumber(Integer.parseInt(fields[2]));
                    input.setCode(fields[3]);
                    input.setStatus(fields[4]);
                    input.setWeight(Integer.parseInt(fields[5]));
                    input.setPackageUnit(fields[6]);
                    input.setPackageCount(Integer.parseInt(fields[7]));
                    input.setTenantId(1);
                    items.add(input);
                });
                serviceItemService.createServiceItems(items);
                break;
        }
        return "success";
    }

    @Override
    public String importSeeds(MultipartFile file, String entity, int shipmentId) throws Exception {
        
        if (cbGraphQlEndpoint == null || cbGraphQlEndpoint.isBlank())
            cbGraphQlEndpoint = "https://cbgraphqlapi-dev.ebsproject.org/graphql";

        int count = 0;
        int createdItems = 0;

        Set<String> seedNames = new HashSet<>();
        String seedNamesString = new String();
        BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
        String line = new String();
        
        while ((line = br.readLine()) != null) {
            seedNames.add(line);
            seedNamesString += "\"" + line + "\",";
        }

        seedNamesString = seedNamesString.substring(0, seedNamesString.length()-1);

        String query = "query{findSeedsByName(seedNames:[" + seedNamesString + "]){id seedName germplasm{id}}}";

/*         GraphQLClient graphQLClient = new GraphQLClient(
        WebClient
            .builder()
            //.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
            .build(),tokenGenerator); */

        ObjectMapper om = new ObjectMapper();
        JsonNode response = om.readTree(graphQLClient.getQueryData(cbGraphQlEndpoint, query));
        if (response.get("data") != null && response.get("data").get("findSeedsByName") != null) {
            JsonNode existingSeeds = response.get("data").get("findSeedsByName");
            if (existingSeeds.isArray()) {
                switch (entity) {
                    case "shipment": {
                        List<ShipmentItemInput> items = new ArrayList<>();
                        for (JsonNode seed : existingSeeds) {
                            seedNames.remove(seed.get("seedName").asText());
                            ShipmentItemInput input = new ShipmentItemInput();
                            count++;
                            input.setShipmentId(shipmentId);
                            input.setGermplasmId(Integer.parseInt(seed.get("germplasm").get("id").asText()));
                            input.setSeedId(Integer.parseInt(seed.get("id").asText()));
                            input.setSeedName(seed.get("seedName").asText());
                            input.setNumber(count);
                            input.setCode(shipmentId + "-" + seed.get("id").asText());
                            input.setStatus("unverified");
                            input.setWeight(0);
                            input.setPackageUnit("grams");
                            input.setPackageCount(0);
                            input.setTenantId(1);
                            items.add(input);
                        }
                        for (String newSeed : seedNames) {
                            ShipmentItemInput input = new ShipmentItemInput();
                            count++;
                            input.setShipmentId(shipmentId);
                            input.setGermplasmId(0);
                            input.setSeedId(0);
                            input.setSeedName(newSeed);
                            input.setNumber(count);
                            input.setCode(shipmentId + "-" + count + "U");
                            input.setStatus("unknown");
                            input.setWeight(0);
                            input.setPackageUnit("grams");
                            input.setPackageCount(0);
                            input.setTenantId(1);
                            items.add(input);
                        }
                        createdItems = shipmentItemService.createShipmentItems(items);
                    }
                    break;
                    case "workflow": {
                        List<ServiceItemInput> items = new ArrayList<>();
                        for (JsonNode seed : existingSeeds) {
                            seedNames.remove(seed.get("seedName").asText());
                            ServiceItemInput input = new ServiceItemInput();
                            count++;
                            input.setServiceId(shipmentId);
                            input.setGermplasmId(Integer.parseInt(seed.get("germplasm").get("id").asText()));
                            input.setSeedId(Integer.parseInt(seed.get("id").asText()));
                            input.setSeedName(seed.get("seedName").asText());
                            input.setNumber(count);
                            input.setCode(shipmentId + "-" + seed.get("id").asText());
                            input.setStatus("unverified");
                            input.setWeight(0);
                            input.setPackageUnit("grams");
                            input.setPackageCount(0);
                            input.setTenantId(1);
                            items.add(input);
                        }
                        for (String newSeed : seedNames) {
                            ServiceItemInput input = new ServiceItemInput();
                            count++;
                            input.setServiceId(shipmentId);
                            input.setGermplasmId(0);
                            input.setSeedId(0);
                            input.setSeedName(newSeed);
                            input.setNumber(count);
                            input.setCode(shipmentId + "-" + count + "U");
                            input.setStatus("unknown");
                            input.setWeight(0);
                            input.setPackageUnit("grams");
                            input.setPackageCount(0);
                            input.setTenantId(1);
                            items.add(input);
                        }
                        createdItems = serviceItemService.createServiceItems(items);
                    }
                    break;
                }
            }
        }  

        return "success:items imported " + createdItems;

    }

    @Override
    public String importPersons(MultipartFile file) {
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
            List<String> csvLines = br.lines().collect(Collectors.toList());
            br.close();
            if (csvLines.size() == 0 || csvLines.size() == 1)
                throw new RuntimeException("Incompatible csv format");
            csvLines.remove(0);
            for (String line: csvLines) {
                String[] fields = line.split(",");
                ContactInput contactInput = new ContactInput();
                contactInput.setId(0);
                contactInput.setEmail(fields[0]);
                contactInput.setCategory(new CategoryInput(1, "", ""));
                PersonInput personInput = new PersonInput();
                personInput.setFamilyName(fields[1]);
                personInput.setGivenName(fields[2]);
                if (!fields[4].isBlank())
                    personInput.setAdditionalName(fields[3]);
                personInput.setGender(fields[4]);
                personInput.setJobTitle(fields[5]);
                personInput.setKnowsAbout(fields[6]);
                contactInput.setPerson(personInput);
                contactService.createContact(contactInput);
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }

        return "Successfully imported " + count + " contacts";
    }

    @Override
    public String importInstitutions(MultipartFile file) {
        int count = 0;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
            List<String> csvLines = br.lines().collect(Collectors.toList());
            br.close();
            if (csvLines.size() == 0 || csvLines.size() == 1)
                throw new RuntimeException("Incompatible csv format");
            csvLines.remove(0);
            for (String line: csvLines) {
                String[] fields = line.split(",");
                ContactInput contactInput = new ContactInput();
                contactInput.setId(0);
                contactInput.setEmail(fields[0]);
                contactInput.setCategory(new CategoryInput(2, "", ""));
                InstitutionInput institutionInput = new InstitutionInput();
                institutionInput.setCommonName(fields[1]);
                institutionInput.setLegalName(fields[2]);
                contactInput.setInstitution(institutionInput);
                contactService.createContact(contactInput);
                count++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Error: " + e.getMessage();
        }

        return "Successfully imported " + count + " contacts";
    }
    
}
