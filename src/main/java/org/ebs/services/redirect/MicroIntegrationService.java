package org.ebs.services.redirect;

import org.ebs.model.parameters.BasicResponse;
import org.ebs.model.parameters.NotificationParameter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

public class MicroIntegrationService {
    private WebClient webClient;

    public MicroIntegrationService(WebClient webClient) {
        this.webClient = webClient;
    }

    public String redirectRequestToMI(String uri, String payload) {
        return webClient
                .post()
                .uri(uri)
                .body(BodyInserters.fromValue(payload))
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public String doGet(String uri) {
        return webClient
                .get()
                .uri(uri)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public BasicResponse postNotification(NotificationParameter parameters) {
        return webClient
                .post()
                .uri("/notification/v2/send")
                .body(BodyInserters.fromValue(parameters))
                .retrieve()
                .bodyToMono(BasicResponse.class)
                .block();

    }

}
