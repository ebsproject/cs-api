package org.ebs.services;

import java.util.List;
import java.util.Set;
import org.ebs.services.to.RoleProductTo;
import org.ebs.services.to.Input.RoleProductInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface RoleProductService {

    RoleProductTo createRoleProduct(RoleProductInput roleProduct);
    String deleteRoleProduct(Integer roleId, Set<Integer> roleProductIds);

}