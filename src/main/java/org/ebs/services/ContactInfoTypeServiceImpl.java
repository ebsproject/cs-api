package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactInfoTypeModel;
import org.ebs.model.repos.ContactInfoRepository;
import org.ebs.model.repos.ContactInfoTypeRepository;
import org.ebs.services.to.ContactInfoType;
import org.ebs.services.to.Input.ContactInfoTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ContactInfoTypeServiceImpl implements ContactInfoTypeService {

    private final ContactInfoRepository contactInfoRepository;
    private final ContactInfoTypeRepository contactInfoTypeRepository;
    private final ConversionService converter;

    @Override
    public ContactInfoType findByContactInfo(int contactInfoId) {
        return contactInfoRepository.findById(contactInfoId).map(c -> c.getContactInfoType())
                .map(i -> converter.convert(i, ContactInfoType.class)).orElse(null);
    }

    @Override
    public Optional<ContactInfoType> findContactInfoType(int infoTypeId) {
        return contactInfoTypeRepository.findById(infoTypeId)
            .map(r -> converter.convert(r, ContactInfoType.class));
    }

    @Override
    public Page<ContactInfoType> findContactInfoTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
                return contactInfoTypeRepository.findByCriteria(ContactInfoTypeModel.class,filters,sort,page,disjunctionFilters)
                    .map(r -> converter.convert(r,ContactInfoType.class));
    }

    @Override
    @Transactional(readOnly = false)
    public ContactInfoType createContactInfoType(ContactInfoTypeInput infoTypeInput) {

        infoTypeInput.setName(infoTypeInput.getName().trim());

        if (contactInfoTypeRepository.findByNameIgnoreCase(infoTypeInput.getName()).size() > 0)
            throw new RuntimeException("Additional information with name " + infoTypeInput.getName() + " already exists");
        
        infoTypeInput.setId(0);

        ContactInfoTypeModel model = converter.convert(infoTypeInput, ContactInfoTypeModel.class);

        model = contactInfoTypeRepository.save(model);

        return converter.convert(model, ContactInfoType.class);

    }

    @Override
    @Transactional(readOnly = false)
    public ContactInfoType modifyContactInfoType(ContactInfoTypeInput infoTypeInput) {

        infoTypeInput.setName(infoTypeInput.getName().trim());

        List<ContactInfoTypeModel> infos = contactInfoTypeRepository.findByNameIgnoreCase(infoTypeInput.getName());

        if (infos.size() > 1)
            throw new RuntimeException("Additional information with name " + infoTypeInput.getName() + " already exists");
        else if (infos.size() == 1 && infos.get(0).getId() != infoTypeInput.getId())
            throw new RuntimeException("Additional information with name " + infoTypeInput.getName() + " already exists");

        ContactInfoTypeModel target = contactInfoTypeRepository.findById(infoTypeInput.getId())
            .orElseThrow(() -> new RuntimeException("Info type not found"));

        ContactInfoTypeModel source = converter.convert(infoTypeInput, ContactInfoTypeModel.class);

        Utils.copyNotNulls(source, target);

        return converter.convert(contactInfoTypeRepository.save(target), ContactInfoType.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteContactInfoType(int infoTypeId) {

        ContactInfoTypeModel infoType = contactInfoTypeRepository.findById(infoTypeId)
            .orElseThrow(() -> new RuntimeException("Info type not found"));
        
        infoType.setDeleted(true);

        contactInfoTypeRepository.save(infoType);

        return infoTypeId;
        
    }

}