package org.ebs.services;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.Locale.Category;

import org.ebs.model.ContactInfoTypeModel;
import org.ebs.model.ContactModel;
import org.ebs.model.ContactTypeModel;
import org.ebs.model.repos.CategoryRepository;
import org.ebs.model.repos.ContactTypeRepository;
import org.ebs.services.to.ContactType;
import org.ebs.services.to.CategoryTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class ContactTypeServiceImpl implements ContactTypeService {

    private final ContactTypeRepository contactTypeRepository;
    private final CategoryRepository categoryRepository;
    private final ConversionService converter;

    public List<ContactType> findByContact(int contactId) {
        return contactTypeRepository.findByContact(contactId).stream().map(t -> converter.convert(t, ContactType.class))
                .collect(toList());
    }

    @Override
    public ContactType findById(int ContactTypeId) {
        return contactTypeRepository.findById(ContactTypeId).map(c -> converter.convert(c, ContactType.class))
                .orElse(null);
    }

    @Override
    public Page<ContactType> findContactTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return contactTypeRepository.findByCriteria(ContactTypeModel.class, filters, sort, page, disjunctionFilters)
                .map(c -> converter.convert(c, ContactType.class));
    }
 
    @Override
	public CategoryTo findCategory(int contactTypeId){

        ContactTypeModel contactTypeModel = contactTypeRepository.findById(contactTypeId).orElse(null);
		return converter.convert(contactTypeModel.getCategory(),CategoryTo.class);  //contactTypeRepository.findById(contactTypeId).map(r -> converter.convert(r.getCategory(),CategoryTo.class));
	}



}