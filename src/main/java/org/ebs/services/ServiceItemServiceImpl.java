package org.ebs.services;

import static java.util.stream.Collectors.toList;
import static org.ebs.util.Utils.copyNotNulls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.ebs.model.OccurrenceShipmentModel;
import org.ebs.model.ServiceItemModel;
import org.ebs.model.ServiceModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.OccurrenceShipmentRepository;
import org.ebs.model.repos.ServiceItemRepository;
import org.ebs.model.repos.ServiceRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.ServItemInput;
import org.ebs.services.to.Input.ServiceItemInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ServiceItemServiceImpl implements ServiceItemService {

    private final ConversionService converter;
    private final ServiceRepository serviceRepository;
    private final ServiceItemRepository serviceItemRepository;
    private final TenantRepository tenantRepository;
    private final OccurrenceShipmentRepository occurrenceShipmentRepository;

    @Override
    public Optional<ServiceItemTo> findServiceItem(int id) {
        if (id < 1)
            return Optional.empty();

        return serviceItemRepository.findById(id).map(r -> converter.convert(r, ServiceItemTo.class));
    }

    @Override
    public Page<ServiceItemTo> findServiceItems(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return serviceItemRepository
                .findByCriteria(ServiceItemModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ServiceItemTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public ServiceItemTo createServiceItem(ServiceItemInput input) {

        input.setId(0);

        ServiceItemModel model = converter.convert(input, ServiceItemModel.class);

        setServiceItemDependencies(model, input);

        model = serviceItemRepository.save(model);

        return converter.convert(model, ServiceItemTo.class);

    }

    @Override
    @Transactional(readOnly = false , timeout = 60000)
    public int createServiceItems(List<ServiceItemInput> input) {
        List<ServiceItemModel> serviceItemModel = new ArrayList<>();
        input.forEach(item -> {
            item.setId(0);
            ServiceItemModel model = converter.convert(item, ServiceItemModel.class);
            setServiceItemDependencies(model, item);
            serviceItemModel.add(model);
        });
        serviceItemRepository.saveAll(serviceItemModel);
        return input.size();
    }

    @Override
    @Transactional(readOnly = false)
    public ServiceItemTo modifyServiceItem(ServiceItemInput input) {

        ServiceItemModel target = verifyServiceItem(input.getId());

        ServiceItemModel source = converter.convert(input, ServiceItemModel.class);

        Utils.copyNotNulls(source, target);

        setServiceItemDependencies(target, input);

        return converter.convert(serviceItemRepository.save(target), ServiceItemTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteServiceItem(int id) {

        ServiceItemModel model = verifyServiceItem(id);

        model.setDeleted(true);

        serviceItemRepository.save(model);

        return id;

    }

    @Override
    public ServiceTo findService(int serviceItemId) {

        ServiceItemModel model = verifyServiceItem(serviceItemId);

        if (model.getService() == null)
            return null;

        return converter.convert(model.getService(), ServiceTo.class);

    }

    @Override
    public TenantTo findTenant(int serviceItemId) {

        ServiceItemModel model = verifyServiceItem(serviceItemId);

        if (model.getTenant() == null)
            return null;

        return converter.convert(model.getTenant(), TenantTo.class);

    }

    @Override
    public OccurrenceShipmentTo findOccurrenceShipment(int serviceItemId) {

        ServiceItemModel model = verifyServiceItem(serviceItemId);

        if (model.getOccurrenceShipment() == null)
            return null;

        return converter.convert(model.getOccurrenceShipment(), OccurrenceShipmentTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int modifyServiceItems(Integer serviceId, List<ServItemInput> input) {
        final List<ServiceItemModel> target = new ArrayList<>();
        final Map<Integer,ServiceItemModel> targetMap = new HashMap<>();

        if (serviceId != null) {
            ServiceModel ship = verifyService(serviceId);
            target.addAll(ship.getItems().stream().filter(si -> !si.isDeleted()).collect(toList()));
            target.sort((a, b)-> a.getNumber() - b.getNumber());
            ServiceItemModel template = converter.convert(input.get(0), ServiceItemModel.class);
            template.setId(0);
            for (int i = 0; i < target.size(); i++) {
                int id = target.get(i).getId();
                copyNotNulls(template, target.get(i));
                target.get(i).setId(id);
                target.get(i).setNumber(i + 1);
            }
           
        } else {
            target.addAll(verifyServiceItems(input));
            target.forEach(t -> targetMap.put(t.getId(), t));
            input.forEach(
                    si -> {
                    ServiceItemModel sim = converter.convert(si, ServiceItemModel.class);
                    copyNotNulls(sim, targetMap.get(si.getId()));
                });
        }
        
        return target.size();

    }

    ServiceItemModel verifyServiceItem(int serviceItemId) {
        return serviceItemRepository.findById(serviceItemId)
                .orElseThrow(() -> new RuntimeException("Service item " + serviceItemId + " not found"));
    }

    private List<ServiceItemModel> verifyServiceItems(List<ServItemInput> input) {
            return verifyServiceItemsById(input.stream()
                .map(ServItemInput::getId)
                .collect(toList()));
    }

    private List<ServiceItemModel> verifyServiceItemsById(List<Integer> ids) {
        List<ServiceItemModel> result = serviceItemRepository
            .findAllById(ids);

            if (result.size() != ids.size()) {
                throw new RuntimeException( "Invalid Service Item Id(s)");
            }
            return result;
    }

    void setServiceItemDependencies(ServiceItemModel model, ServiceItemInput input) {
        setServiceItemService(model, input.getServiceId());
        setServiceItemTenant(model, input.getTenantId());
        setServiceItemOccurrenceShipment(model, input.getOccurrenceShipmentId());
    }

    void setServiceItemService(ServiceItemModel model, Integer serviceId) {
        if (serviceId != null) {
            model.setService(verifyService(serviceId));
        }
    }

    ServiceModel verifyService(Integer serviceId) {
        return serviceRepository.findById(serviceId).orElseThrow(
                () -> new RuntimeException("Service " + serviceId + " not found"));
    }

    void setServiceItemTenant(ServiceItemModel model, Integer tenantId) {
        if (tenantId != null) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

    void setServiceItemOccurrenceShipment(ServiceItemModel model, Integer occurrenceShipmentId) {
        if (occurrenceShipmentId != null) {
            OccurrenceShipmentModel occurrenceShipment = occurrenceShipmentRepository.findById(occurrenceShipmentId)
                    .orElseThrow(() -> new RuntimeException("Occurrence shipment " + occurrenceShipmentId + " not found"));
            model.setOccurrenceShipment(occurrenceShipment);
        }
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteServiceItems(Integer serviceId, List<Integer> input) {
        List<ServiceItemModel> items = null;        
        if (serviceId != null) {
            ServiceModel ship = verifyService(serviceId);
            items = ship.getItems().stream().filter(si -> !si.isDeleted()).collect(toList());
        } else {
            items = verifyServiceItemsById(input);
        }
        
        items.forEach(si -> si.setDeleted(true));
        return items.size();

    }
}
