package org.ebs.services.rule;

import static org.ebs.model.rule.ApiSegmentModel.ApiType.GRAPHQL;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.GET;

import java.text.ParseException;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Supplier;

import org.ebs.graphql.client.GraphQLRequestBody;
import org.ebs.model.rule.SegmentModel;
import org.ebs.services.to.rule.ApiSegment;
import org.ebs.services.to.rule.Segment;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@AllArgsConstructor
class ApiSegmentResolver implements SegmentResolver<ApiSegment> {

    private final TokenGenerator tokenGenerator;
    private final RuleScripts ruleScripts;
    private final UrlBuilder urlBuilder;

    @Override
    public String resolve(Segment seg, String input, String context) {
        ApiSegment segment = (ApiSegment) seg;
        
       return Optional.ofNullable(segment)
            .map( s -> doRequest(s, input))
            .map(r -> extractValue(segment, r, segment.getResponseMapping()))
            .map(v -> doFormat(segment.getFormat(), v, segment.getDataType()))
            .orElseThrow(exceptionFor(segment, input));
        }

    @Override
    public Class<ApiSegment> resolverFor() {
        return ApiSegment.class;
    }

    String doRequest(ApiSegment segment, String input) {
        return segment.getType().equals(GRAPHQL) ?
            graphqlRequest(segment, input) :
                restRequest(segment, input);
    }

    String graphqlRequest(ApiSegment segment, String input) {
        WebClient client = WebClient.builder()
            .defaultHeader(AUTHORIZATION, "Bearer " +  tokenGenerator.getToken())
            .build();
    
        return client
            .post()
            .uri(urlBuilder.getUrl(segment))
            .bodyValue( new GraphQLRequestBody(segment.getBodyTemplate().replace("{}", input)))
            .retrieve().bodyToMono(String.class)
            .blockOptional()
            .orElseThrow(exceptionFor(segment, input));
    }
    
    String restRequest(ApiSegment segment, String input) {
        WebClient client = WebClient.builder()
            .defaultHeader(AUTHORIZATION, "Bearer " +  tokenGenerator.getToken())
            .build();
    
        String uri = urlBuilder.getUrl(segment).replace("{}", input);

        if(segment.getMethod().equals(GET)) {
            return client.get()
                .uri(uri)
                .retrieve().bodyToMono(String.class).block();
        } else {
            log.info("calling uri: {}", uri);
            log.info("with body: {}", segment.getBodyTemplate().replace("{}", input));
            String response = client.post()
                .uri(uri)
                .bodyValue(segment.getBodyTemplate().replace("{}", input))
                .retrieve().bodyToMono(String.class)
                .blockOptional()
                    .orElseThrow(exceptionFor(segment, input));
            log.info("response: {}", response);
            return response;
        }
    }

    String extractValue(ApiSegment segment, String response, String extractor) {
        try {
            Object val = ruleScripts.invoke("extract", response, extractor);
            String extractedValue = (val == null && !segment.isRequired()) ? "" : val.toString();
            log.info("extracted value: {}", extractedValue);
            return extractedValue;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(
                    "Could not extract value for mandatory segment "+segment.getName()+" with data " + response + " and path " + extractor +
                    ". Reason: " + e.getMessage());
        }
    }
    
    

    /**
     * Applies format, if any
     * @param format
     * @param value
     * @param dataType
     * @return the formatted value
     */
    String doFormat(String format, String value, SegmentModel.DataType dataType) {
        Object v = value;
        try{
            switch (dataType) {
                case NUMBER:
                    v = Integer.valueOf(value);
                    break;
                case DATE:
                    v = parseDate(value);
                    break;
                case DATETIME:
                    v = parseDateTime(value);
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.warn(String.format("Cannot parse %s as %s: %s", value, dataType, e.getMessage() ));
        }
        return format(format, v, dataType);
    }

    private Supplier<RuntimeException> exceptionFor(ApiSegment segment, String input) {
        return () -> new RuntimeException(
                "Could not get a value with segment " + segment + "(input: " + input + ")");
    }

    Instant parseDate(String value) throws ParseException {
        return SYSTEM_DATE_FORMAT.parse(value).toInstant();
    }

    Instant parseDateTime(String value) throws ParseException {
        return SYSTEM_DATETIME_FORMAT.parse(value).toInstant();
    }
}
