package org.ebs.services.rule;

import static org.ebs.model.rule.ApiSegmentModel.ApiType.REST;
import static org.ebs.services.to.rule.ApiSegment.Module.CB;
import static org.ebs.services.to.rule.ApiSegment.Module.MARKERDB;
import static org.ebs.services.to.rule.ApiSegment.Module.SM;

import org.ebs.model.DomainInstanceModel;
import org.ebs.model.repos.DomainInstanceRepository;
import org.ebs.services.to.rule.ApiSegment;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;

@Component
@Profile({ "default", "prod" })
@RequiredArgsConstructor
class UrlBuilderImpl implements UrlBuilder {

    private final DomainInstanceRepository domainInstanceRepo;
    
    @Override
    public String getUrl(ApiSegment segment) {
        ApiSegment.Module module = ApiSegment.Module.valueOf(segment.getServerUrl());
        DomainInstanceModel instance = domainInstanceRepo.findByDomainIdAndDeletedIsFalse(module.domainId()).get(0);
        boolean isMarkerDB = module.equals(MARKERDB);
        boolean isSM = module.equals(SM);

        String url = instance.getSgContext();
        if (isMarkerDB || isSM) {
            if (url.contains("ebsproject") && isMarkerDB) {
                url = url.replaceFirst("sm", MARKERDB.prefix());
            } else {
                url = url.split(",")[isSM ? 0 : 1];
            }
        }

        url = url.replaceFirst("/$", "") + "/" + segment.getPathTemplate();
        return url;
    }
}
