package org.ebs.services.rule;

import org.ebs.services.to.rule.ApiSegment;

public interface UrlBuilder {
    String getUrl(ApiSegment segment);
}
