package org.ebs.services.rule;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.springframework.stereotype.Component;

@Component
public class RuleScripts {
    private final Invocable scripts;
    ScriptEngine engine;

    public RuleScripts() {
        ScriptEngineManager manager = new ScriptEngineManager();
        engine = manager.getEngineByName("JavaScript");
        String script;
        try {
            script = new String(getClass().getResourceAsStream("/js/extractor.js").readAllBytes(),
                    StandardCharsets.UTF_8);
            engine.eval(script);
        } catch (IOException | ScriptException e) {
            e.printStackTrace();
        }
        scripts = (Invocable) engine;
    }
    
    public Object invoke(String name, Object... args) throws NoSuchMethodException, ScriptException {
        return scripts.invokeFunction(name, args);
    }
    
    public Object execute(String code) throws ScriptException, NoSuchMethodException {
        engine.eval("function code(){ return " + code + "}");
        return ((Invocable)engine).invokeFunction("code", (Object[])null);
    }
}
