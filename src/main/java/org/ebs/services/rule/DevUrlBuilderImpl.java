package org.ebs.services.rule;

import org.ebs.services.to.rule.ApiSegment;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile({"dev", "test"})
class DevUrlBuilderImpl implements UrlBuilder{
    
    public String getUrl(ApiSegment segment) {
        ApiSegment.Module module = ApiSegment.Module.valueOf(segment.getServerUrl());
        String url = null;
        switch (module) {
            case CB:
                url = "http://localhost:8080/v3";
                break;
            case CS:
                url = "http://localhost:8081";
                break;
            case SM:
                url = "http://localhost:8082";
                break;
            case FILE:
                url = "http://localhost:8083";
                break;
            case CBGRAPH:
                url = "http://localhost:8084";
                break;
            case MARKERDB:
                url = "http://localhost:8085";
                break;
        }
        return url + "/" + segment.getPathTemplate();
    }
}
