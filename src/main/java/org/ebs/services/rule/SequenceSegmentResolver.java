package org.ebs.services.rule;

import java.util.Optional;

import javax.script.ScriptException;

import java.util.List;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;

import org.ebs.model.rule.SequenceSegmentModel;
import org.ebs.model.rule.repos.SegmentRepository;
import org.ebs.model.rule.repos.SequenceSegmentRepository;
import org.ebs.services.to.rule.Segment;
import org.ebs.services.to.rule.SequenceSegment;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
class SequenceSegmentResolver implements SegmentResolver<SequenceSegment> {

    private final SegmentRepository segmentRepo;
    private final SequenceSegmentRepository sequenceSegmentRepo;
    private final RuleScripts scripts;

    /*
     * This resolver ignores input
     */
    @Override
    public String resolve(Segment seg, String input, String context) {
        return resolveAll(seg, input, context, 1).get(0);
    }

    public List<String> resolveAll(Segment seg, String input, String context, int batch) {
        SequenceSegment segment = (SequenceSegment)seg;
        List<Integer> nextVals = getNext(segment, context, batch);

        Optional<String> formatString = ofNullable(seg.getFormat());

        return nextVals.stream()
            .map(n -> formatString.map(f -> format(f, n)).orElse(n.toString()))
            .collect(toList());

    }

    Integer getNext(SequenceSegment segment, String context) {
        
        return getNext(segment, context, 1).get(0);
    }

    List<Integer> getNext(SequenceSegment segment, String context, int batch) {
        SequenceSegmentModel model = segmentRepo.findById(segment.getId())
            .map(s -> s.getSequenceSegment())
            .orElseThrow(
                () -> new RuntimeException("No segment found with id " + segment.getId()));

        final SequenceSegmentModel tmp = model;
        if(!(context == null  || context.isBlank())) {
            model = sequenceSegmentRepo.findByParentIdAndFamilyKey(model.getId(), context)
                .orElseGet(() -> newFamilySequence(tmp, context));
        }

        List<Integer> sequenceList = new ArrayList<>(batch);
        for(int i =0; i < batch; i++) {
            if (needsReset(model)) {
                model.setLast(model.getLowest());
            } else {
                model.setLast(model.getLast() + model.getIncrement());
            }
            sequenceList.add(model.getLast());
        }

        model = sequenceSegmentRepo.save(model);
        log.info("last value: {}", model.getLast());
        return sequenceList;
    }

    boolean needsReset(SequenceSegmentModel segment) {
        boolean reset = false;
        if (segment.getResetCondition() == null)
            return reset;
        
        try{
            String resetCondition = segment.getResetCondition().get("reset-condition").asText();
            String resetValueFunction = segment.getResetCondition().get("reset-value-function")
                    .asText();
            String resetValue = segment.getResetCondition().get("reset-value").asText();
            log.debug("Reset condition: {}. Reset value function: {}. Reset value: {}", resetCondition, resetValueFunction, resetValue);
            reset = Boolean.parseBoolean(scripts.execute(resetCondition.replace("$input", resetValue)).toString());

            if (reset) {
                String newValue = scripts.execute(resetValueFunction).toString();
                log.debug("resetting to value: {}", newValue);
                ((ObjectNode) segment.getResetCondition()).put("reset-value", newValue);
                sequenceSegmentRepo.save(segment);
            }

        } catch (NoSuchMethodException | ScriptException e) {
            reset = false;
            log.error("cannot reset sequence({}) {}: {}", segment.getId(),
                    segment.getResetCondition().textValue(), e.getMessage());
        }

        return reset;
    }

    @Override
    public Class<SequenceSegment> resolverFor() {
        return SequenceSegment.class;
    }


    SequenceSegmentModel newFamilySequence(SequenceSegmentModel parent, String familyKey) {
        log.info("generating new sub sequence for sequence segment {} using key {}", parent.getId(), familyKey);
        
        SequenceSegmentModel seq = new SequenceSegmentModel(0, parent.getLowest(),
                parent.getIncrement(), 0, parent.getResetCondition(), familyKey, parent.getId());
        seq = sequenceSegmentRepo.save(seq);
        return seq;
    }
}
