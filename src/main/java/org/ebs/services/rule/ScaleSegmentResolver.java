package org.ebs.services.rule;

import java.util.Map;

import org.ebs.services.to.rule.ScaleSegment;
import org.ebs.services.to.rule.Segment;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class ScaleSegmentResolver implements SegmentResolver<ScaleSegment> {

    @Override
    public String resolve(Segment segment, String input, String context) {
        log.trace("scale value: {}", input);
        String formattedValue = format(segment.getFormat(), input, segment.getDataType());
        log.trace("formatted scale value: {}", formattedValue);
        return formattedValue;
    }

    @Override
    public boolean validate(Segment segment, Map<String, String> args) {
        
        if (SegmentResolver.super.validate(segment, args) &&
                segment.getScale().contains(args.get(String.valueOf(segment.getId())))) {
            return true;
        }
        throw new RuntimeException(String.format("scale segment %s has invalid value, only %s are allowed", segment.getName(), segment.getScale()));
    }

    @Override
    public Class<ScaleSegment> resolverFor() {
        return ScaleSegment.class;
    }
}
