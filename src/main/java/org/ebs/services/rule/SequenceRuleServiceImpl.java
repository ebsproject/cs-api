package org.ebs.services.rule;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.script.ScriptException;

import org.ebs.model.rule.SequenceRuleModel;
import org.ebs.model.rule.repos.SequenceRuleRepository;
import org.ebs.services.to.rule.Segment;
import org.ebs.services.to.rule.SequenceRule;
import org.ebs.services.to.rule.SequenceSegment;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = false)
public class SequenceRuleServiceImpl implements SequenceRuleService{

    private final SequenceRuleRepository ruleRepo;
    private final ConversionService converter;
    private final SegmentResolverFacade segmentResolverFacade;
    private final RuleScripts ruleScripts;

    @Override
    public Page<SequenceRule> findAll(Pageable page) {
        return ruleRepo.findByCriteria(SequenceRuleModel.class,null,null,page).map(s -> converter.convert(s, SequenceRule.class));
    }

    @Override
    public SequenceRule findById(int sequenceRuleId) {
        return ruleRepo.findById(sequenceRuleId).map(r -> converter.convert(r, SequenceRule.class))
                .orElseThrow(() -> new RuntimeException(
                        "Sequence rule " + sequenceRuleId + " does not exist."));
    }

    @Override
    public String next(int ruleId, Map<String, String> args) {
        log.info("requesting next for rule {}: {}", ruleId, args);

        return nextBatch(ruleId, 1, args).get(0);
    }

    @Override
    public List<String> nextBatch(int ruleId, int batchSize, Map<String, String> args) {
        log.info("requesting next batch for rule {} size {}: {}", ruleId, batchSize, args);

        SequenceRule rule = findById(ruleId);
        cleanArgs(args);
        List<Segment> segments = getNonSkipped(rule, args);
        if (validateInputs(segments, args)) {

            Map<Segment, String> segmentMap = new HashMap<>();
            String familyCode = generateSegmentMap(segments, args, segmentMap);

            // rules are assumed to have only one sequence segment
            List<String> sequenceCodes = generateSequenceCodes(segments, batchSize, familyCode);

            log.debug("segment map: {}", segmentMap);
            List<String> codes = new ArrayList<>();
            for (int i = 0; i < batchSize; i++) {
                final int ii = i;
                codes.add(segments.stream().filter(s -> s.isDisplayed())
                        .map(s -> s instanceof SequenceSegment ? sequenceCodes.get(ii)
                                : segmentMap.get(s))
                        .collect(joining()));
            }
            log.trace("rule codes: {}", codes);
            return codes;
        }
        return null;
    }
    
    List<Segment> getNonSkipped(SequenceRule rule, Map<String, String> args) {
        
        return rule.getSegments().stream()
            .filter(s -> {
                log.debug("evaluating skip for segment: {}", s);
                String input = args.get(String.valueOf(s.getSkipInputSegmentId()!= null ? s.getSkipInputSegmentId() : s.getId()));
                try {
                    boolean skip = Boolean.parseBoolean(String.valueOf(ruleScripts.invoke("isSkipped", input, s.getSkip())));
                    log.trace("skipping segment ({}): {}", s.getId(), skip);
                    return !skip;
                } catch (NoSuchMethodException | ScriptException e) {
                        log.warn(
                                "Could not evaluate Skip value for segment {} with input {}. Skipping by default. Reason: {}",
                                         s, input, e.getMessage());
                    return false;
                }
            })
            .collect(toList());
    }
    
    /**
     * removes duplicate values in each input arg
     * @param args to clean
     * @return an equivalent map where the values are clean of duplicates
     */
    void cleanArgs(Map<String, String> args) {
        args.forEach((k, v) -> {
            args.replace(k, new HashSet<>(asList(v.split(","))).stream().collect(joining(",")));
        });
    }

    /**
     * Creates a cache map where the key is a segment and the value is its resolved value. This method
     * does not resolves sequence segments, because they return a different value each time they are called, 
     * so cannot be cached
     * @param segments to be resolved
     * @param args used as input for rule segments 
     * @param segmentMap to be used as cache, where the resolved values of segments are stored
     * @return the familyCode of the rule, used to resolve sequence segments
     */
    String generateSegmentMap(List<Segment> segments, Map<String, String> args,
            Map<Segment, String> segmentMap) {
        final StringBuilder familyCode = new StringBuilder();
        segments.stream()
            .filter(s -> s.getFamily() > 0)
            .filter(s -> !(s instanceof SequenceSegment))
            .forEach(s -> {
                    String value = resolveSegment(s, args.get(s.getId().toString()), null);
                    segmentMap.put(s, value);
                    familyCode.append(value);
            });
        segments.stream()
            .filter(s -> s.getFamily() == 0)
            .filter(s -> !(s instanceof SequenceSegment))
            .forEach(s -> segmentMap.put(s,
                    resolveSegment(s, args.get(s.getId().toString()), familyCode.toString())));
        return familyCode.toString();
    }
    
    /**
     * generates an ordered list of sequence codes from a Sequence Segment
     * @param rule to provide a Sequence Segment
     * @param batchSize number of sequence codes to generate
     * @param familyCode to get the proper sub-sequencer, if any
     */
    List<String> generateSequenceCodes(List<Segment> segments, int batchSize, String familyCode) {
        SequenceSegment seqSegment = (SequenceSegment) segments.stream()
            .filter(s -> s.isDisplayed() && s instanceof SequenceSegment)
            .findFirst()
            .orElse(null);

        final List<String> sequenceCodes = new ArrayList<>();
        if (seqSegment != null) {
            SequenceSegmentResolver seqResolver = (SequenceSegmentResolver) segmentResolverFacade
                    .resolverFor(seqSegment);
            sequenceCodes.addAll(
                    seqResolver.resolveAll(seqSegment, null, familyCode.toString(), batchSize));
        }
        return sequenceCodes;
    }

    boolean validateInputs(List<Segment> segments, Map<String, String> args) {
        return segments.stream()
            .map(seg -> segmentResolverFacade.resolverFor(seg).validate(seg, args))
            .reduce(true, (a,b) -> a && b);
    }
    
    String resolveSegment(Segment segment, String value, String familyCode) {
        SegmentResolver<? extends Segment> resolver = segmentResolverFacade.resolverFor(segment);
        log.debug("resolving for {} with value {}", segment, value);
        if(value == null) {
            return resolver.resolve(segment, value, familyCode);
        }else{
            return Stream.of(value.split(","))
                    .map(s -> { 
                        try{
                            return s;
                        }catch(Exception e){
                            throw new RuntimeException("Segment "+segment.getName()+" with value '" + value + "' contains invalid token: " + s);
                        }})
                .map(i ->resolver.resolve(segment, i, familyCode))
                .collect(joining());
        }
    }
}
