package org.ebs.services.rule;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.ebs.model.rule.SegmentModel;
import org.ebs.services.to.rule.Segment;


interface SegmentResolver<T extends Segment> {
    public static final Locale loc = new Locale("en", "US");
    public static final SimpleDateFormat SYSTEM_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat SYSTEM_DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

    /**
     * Returns the effective value of a segment. That is, performing the required computations and/or operations defined in a segment
     * @param segment to resolve
     * @param input needed for some segments to compute the return value
     * @param context of a rule to modify the default behavior of a resolver. Optional
     * @return the (resolved) value of a segment
     */
    public String resolve(Segment segment, String input, String context);

    /**
     * Specifies which type of segments this resolver works for.
     * @return the type of the supported segment
     */
    public Class<T> resolverFor();

    /**
     * Default implementation to format Instant values
     * @param format
     * @param value
     * @return formatted date/time string
     */
    default String format(String format, Instant value) {
        DateFormat df = new SimpleDateFormat(format, loc);
        return df.format(Date.from(value));
    }

    /**
     * Default implementation to format numeric values
     * @param format
     * @param value
     * @return formatted number string
     */
    default String format(String format, Integer value) {
        NumberFormat df = new DecimalFormat(format);
        return df.format(value);
    }

    /**
     * Default implementation to format string values
     * @param format
     * @param value
     * @return formatted string
     */
    default String format(String format, String value) {
        return String.format(loc, format, value);
    }

    default String format(String format, Object value, SegmentModel.DataType dataType) {
        String v = value.toString();
        if (format != null) {

            switch (dataType) {
            case NUMBER:
            v = format(format, (Integer) value);
            break;
            case DATE:
            case DATETIME:
                v = format(format, (Instant) value);
                break;
            default:
                v = format(format, (String) value);
                break;
            }
        }
        return v;
    }
    
    default boolean validate(Segment segment, Map<String, String> args) {
        if (segment.isRequiresInput() &&
            !args.containsKey(String.valueOf(segment.getId())) &&
            segment.isRequired()) {
                throw new RuntimeException("Missing input value for segment " + segment.getName());
            } else {
                return true;
            }
        }
    }
