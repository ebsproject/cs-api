package org.ebs.services.rule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ebs.services.to.rule.Segment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class SegmentResolverFacade {
    
    private final Map<Class<?>, SegmentResolver<? extends Segment>> resolverMap = new HashMap<>();

    @Autowired
    public SegmentResolverFacade(List<SegmentResolver<?>> resolvers) {

        resolvers.stream().forEach(r -> {
            
            SegmentResolver<?> prevValue = resolverMap.putIfAbsent(r.resolverFor(), r);
            if (prevValue != null) {
                throw new RuntimeException(
                    "Only one segment resolver can be defined for " + r.resolverFor().getSimpleName());
            }
        });
        
        log.info("Segment Resolvers registered: {}", resolverMap.size());
    }

    public SegmentResolver<? extends Segment> resolverFor(Segment segment) {
        return resolverMap.get(segment.getClass());
    }
}
