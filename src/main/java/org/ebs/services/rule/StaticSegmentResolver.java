package org.ebs.services.rule;

import java.time.Instant;
import java.util.Optional;

import org.ebs.services.to.rule.Segment;
import org.ebs.services.to.rule.StaticSegment;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
class StaticSegmentResolver implements SegmentResolver<StaticSegment> {

    /*
     * This resolver ignores input
     */
    @Override
    public String resolve(Segment seg, String input, String context) {
        
        StaticSegment segment = (StaticSegment)seg;
        Object value = Optional.ofNullable(segment).map(s -> s.getFormula())
                .orElse("invoke JS function!!");
        switch (segment.getDataType()) {
            case DATE:
            case DATETIME:
                value = Instant.now();
                break;
            case NUMBER:
                value = Integer.valueOf(value.toString());
                break;
            default:
                break;
        }
        log.info("static value: {}", value);
        String formattedValue = format(segment.getFormat(), value, segment.getDataType());
        log.info("formatted value: {}", formattedValue);
        return formattedValue;
    }


    @Override
    public Class<StaticSegment> resolverFor() {
        return StaticSegment.class;
    }
}
