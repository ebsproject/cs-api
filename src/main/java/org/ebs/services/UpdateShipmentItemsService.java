package org.ebs.services;

import org.springframework.scheduling.annotation.Async;

public interface UpdateShipmentItemsService {
    
    public String updateSingleShipmentShipmentItemsCBGraphQL(int shipmentId);

    public String updateListItemsCB(int listId);

    @Async
    public void updateSingleShipmentItemsAllData(int shipmentId, int userId);

}
