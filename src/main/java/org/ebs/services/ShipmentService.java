package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.Address;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.ShipmentInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ShipmentService {
    
    public Optional<ShipmentTo> findShipment(int id);

    public Page<ShipmentTo> findShipments(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public ShipmentTo createShipment(ShipmentInput input);

    public ShipmentTo modifyShipment(ShipmentInput input);

    public int deleteShipment(int id);

    public WorkflowInstanceTo findWorkflowInstance(int shipmentId);

    public ProgramTo findProgram(int shipmentId);

    public Contact findSender(int shipmentId);

    public Contact findProcessor(int shipmentId);

    public Contact findRequester(int shipmentId);

    public Contact findRecipient(int shipmentId);

    public TenantTo findTenant(int shipmentId);

    public String getStatus(int shipmentId);

    public Address findAddress(int shipmentId);

    public Address findSenderAddress(int shipmentId);

    public Address findRequestorAddress(int shipmentId);

    public List<ShipmentItemTo> findItems(int shipmentId);

}
