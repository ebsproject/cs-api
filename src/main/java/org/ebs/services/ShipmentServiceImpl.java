package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ebs.model.AddressModel;
import org.ebs.model.ContactModel;
import org.ebs.model.EventModel;
import org.ebs.model.ProgramModel;
import org.ebs.model.ShipmentModel;
import org.ebs.model.TenantModel;
import org.ebs.model.UserModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.EventRepository;
import org.ebs.model.repos.ProgramRepository;
import org.ebs.model.repos.ShipmentItemRepository;
import org.ebs.model.repos.ShipmentRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.model.repos.WorkflowInstanceRepository;
import org.ebs.security.EbsUser;
import org.ebs.services.to.Address;
import org.ebs.services.to.Contact;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.ShipmentItemTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.ShipmentInput;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ShipmentServiceImpl implements ShipmentService {

    private final ConversionService converter;
    private final AddressRepository addressRepository;
    private final ContactRepository contactRepository;
    private final EventRepository eventRepository;
    private final ProgramRepository programRepository;
    private final ShipmentRepository shipmentRepository;
    private final ShipmentItemRepository shipmentItemRepository;
    private final TenantRepository tenantRepository;
    private final WorkflowInstanceRepository workflowInstanceRepository;
    private final UserRepository userRepository;

    @Override
    public Optional<ShipmentTo> findShipment(int id) {
        if (id < 1)
            return Optional.empty();

        return shipmentRepository.findById(id).map(r -> converter.convert(r, ShipmentTo.class));
    }

    @Override
    public Page<ShipmentTo> findShipments(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String programCode = new String();
        if (principal instanceof EbsUser) {
            UserModel u = userRepository.findById(((EbsUser)principal).getId()).orElse(null);
            if (u != null && u.getPreference() != null)
                programCode = getProgramCodeFilter(u.getPreference());
        }
        if (filters == null)
            filters = new ArrayList<FilterInput>();
        if (programCode != null && !programCode.isBlank()) {
            filters.add(new FilterInput("program.code", programCode, FilterMod.EQ));
        }
        return shipmentRepository
                .findByCriteria(ShipmentModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, ShipmentTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public ShipmentTo createShipment(ShipmentInput input) {

        input.setId(0);

        ShipmentModel model = converter.convert(input, ShipmentModel.class);

        setShipmentDependencies(model, input);

        model = shipmentRepository.save(model);

        return converter.convert(model, ShipmentTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public ShipmentTo modifyShipment(ShipmentInput input) {

        ShipmentModel target = verifyShipmentModel(input.getId());

        ShipmentModel source = converter.convert(input, ShipmentModel.class);

        Utils.copyNotNulls(source, target);

        setShipmentDependencies(target, input);

        return converter.convert(shipmentRepository.save(target), ShipmentTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteShipment(int id) {

        ShipmentModel model = verifyShipmentModel(id);

        model.setDeleted(true);

        shipmentRepository.save(model);

        return id;

    }

    @Override
    public WorkflowInstanceTo findWorkflowInstance(int shipmentId) {
        
        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getWorkflowInstance() == null)
            return null;

        return converter.convert(model.getWorkflowInstance(), WorkflowInstanceTo.class);

    }

    @Override
    public ProgramTo findProgram(int shipmentId) {

        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getProgram() == null)
            return null;

        return converter.convert(model.getProgram(), ProgramTo.class);

    }

    @Override
    public Contact findSender(int shipmentId) {
        
        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getSender() == null)
            return null;

        return converter.convert(model.getSender(), Contact.class);

    }

    @Override
    public Contact findProcessor(int shipmentId) {
        
        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getProcessor() == null)
            return null;

        return converter.convert(model.getProcessor(), Contact.class);

    }

    @Override
    public Contact findRequester(int shipmentId) {
        
        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getRequester() == null)
            return null;

        return converter.convert(model.getRequester(), Contact.class);

    }

    @Override
    public Contact findRecipient(int shipmentId) {
        
        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getRecipient() == null)
            return null;

        return converter.convert(model.getRecipient(), Contact.class);

    }

    @Override
    public TenantTo findTenant(int shipmentId) {
        
        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getTenant() == null)
            return null;

        return converter.convert(model.getTenant(), TenantTo.class);

    }

    @Override
    public String getStatus(int shipmentId) {

        ShipmentModel shipment = verifyShipmentModel(shipmentId);

        if (shipment.getWorkflowInstance().getComplete() != null)
            return "Done";

        List<EventModel> shipmentEvents = eventRepository.findByRecordIdAndCompletedIsNullAndDeletedIsFalseOrderByStagePhaseSequenceAscStageSequenceAsc(shipmentId);

        EventModel currentEvent = shipmentEvents.stream().findFirst().orElse(null);

        if (currentEvent == null)
            return null;

        return currentEvent.getStage().getName();

    }

    @Override
    public Address findAddress(int shipmentId) {

        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getAddress() == null)
            return null;

        return converter.convert(model.getAddress(), Address.class);

    }
    @Override
    public Address findSenderAddress(int shipmentId) {

        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getSenderAddress() == null)
            return null;

        return converter.convert(model.getSenderAddress(), Address.class);

    }

    @Override
    public Address findRequestorAddress(int shipmentId) {

        ShipmentModel model = verifyShipmentModel(shipmentId);

        if (model.getRequestorAddress() == null)
            return null;

        return converter.convert(model.getRequestorAddress(), Address.class);

    }

    @Override
    public List<ShipmentItemTo> findItems(int shipmentId) {

        return shipmentItemRepository.findByShipmentId(shipmentId).stream()
            .map(r -> converter.convert(r, ShipmentItemTo.class)).collect(Collectors.toList());
            
    }

    void setShipmentDependencies(ShipmentModel model, ShipmentInput input) {
        setShipmentWorkflowInstance(model, input.getWorkflowInstanceId());
        setShipmentProgram(model, input.getProgramId());
        setShipmentSender(model, input.getSenderId());
        setShipmentProcessor(model, input.getProcessorId());
        setShipmentRequester(model, input.getRequesterId());
        setShipmentRecipient(model, input.getRecipientId());
        setShipmentTenant(model, input.getTenantId());
        setShipmentAddress(model, input.getAddressId());
        setShipmentSenderAddress(model, input.getSenderAddressId());
        setShipmentRequestorAddress(model,input.getRequestorAddressId());
    
    }

    void setShipmentWorkflowInstance(ShipmentModel model, Integer workflowInstanceId) {
        if (workflowInstanceId != null) {
            WorkflowInstanceModel workflowInstance = workflowInstanceRepository.findById(workflowInstanceId)
                    .orElseThrow(() -> new RuntimeException("Workflow instance " + workflowInstanceId + " not found"));
            model.setWorkflowInstance(workflowInstance);
        }
    }

    void setShipmentProgram(ShipmentModel model, Integer programId) {
        if (programId != null) {
            ProgramModel program = programRepository.findById(programId)
                    .orElseThrow(() -> new RuntimeException("Program " + programId + " not found"));
            model.setProgram(program);
        }
    }

    void setShipmentSender(ShipmentModel model, Integer senderId) {
        if (senderId != null) {
            ContactModel contact = contactRepository.findById(senderId)
                    .orElseThrow(() -> new RuntimeException("Sender " + senderId + " not found"));
            model.setSender(contact);
        }
    }

    void setShipmentProcessor(ShipmentModel model, Integer processorId) {
        if (processorId != null) {
            ContactModel contact = contactRepository.findById(processorId)
                    .orElseThrow(() -> new RuntimeException("Processor " + processorId + " not found"));
            model.setProcessor(contact);
        }
    }

    void setShipmentRequester(ShipmentModel model, Integer requesterId) {
        if (requesterId != null) {
            ContactModel contact = contactRepository.findById(requesterId)
                    .orElseThrow(() -> new RuntimeException("Requester " + requesterId + " not found"));
            model.setRequester(contact);
        }
    }

    void setShipmentRecipient(ShipmentModel model, Integer recipientId) {
        if (recipientId != null) {
            ContactModel contact = contactRepository.findById(recipientId)
                    .orElseThrow(() -> new RuntimeException("Recipient " + recipientId + " not found"));
            model.setRecipient(contact);
        }
    }

    void setShipmentTenant(ShipmentModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                    .orElseThrow(() -> new RuntimeException("Tenant " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

    void setShipmentAddress(ShipmentModel model, Integer addressId) {
        if (addressId != null ) {
            AddressModel address = addressRepository.findById(addressId)
                    .orElseThrow(() -> new RuntimeException("Address " + addressId + " not found"));
            model.setAddress(address);
        }
    }

    void setShipmentSenderAddress(ShipmentModel model, Integer senderAddressId) {
        if (senderAddressId != null ) {
            AddressModel address = addressRepository.findById(senderAddressId)
                    .orElseThrow(() -> new RuntimeException("Address " + senderAddressId + " not found"));
            model.setSenderAddress(address);
        }
    }

    void setShipmentRequestorAddress(ShipmentModel model, Integer requestorAddressId) {
        if (requestorAddressId != null ) {
            AddressModel address = addressRepository.findById(requestorAddressId)
                    .orElseThrow(() -> new RuntimeException("Address " + requestorAddressId + " not found"));
            model.setRequestorAddress(address);
        }
    }

    ShipmentModel verifyShipmentModel(int shipmentModelId) {
        return shipmentRepository.findById(shipmentModelId)
                .orElseThrow(() -> new RuntimeException("Shipment id " + shipmentModelId + " not found"));
    }

    private String getProgramCodeFilter(JsonNode userPreferences) {
        JsonNode filterNodes = userPreferences.get("filters");
        if (filterNodes != null && filterNodes.isArray()) {
            for (JsonNode filter : filterNodes) {
                if (filter.get("default") != null && filter.get("default").asBoolean()) {
                    if (filter.get("filterValues") != null && filter.get("filterValues").isObject()) {
                        JsonNode programNodes = filter.get("filterValues").get("Program");
                        if (programNodes != null && programNodes.isArray()) {
                            for (JsonNode programFilter : programNodes) {
                                JsonNode programName = programFilter.get("name");
                                if (programName != null && programName.isTextual()) {
                                    Optional<ProgramModel> program = programRepository.findByName(programName.asText())
                                        .stream().findFirst();
                                    if (program.isPresent()) {
                                        return program.get().getCode();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

}
