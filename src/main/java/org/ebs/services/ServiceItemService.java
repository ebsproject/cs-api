package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.ServItemInput;
import org.ebs.services.to.Input.ServiceItemInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ServiceItemService {

    public Optional<ServiceItemTo> findServiceItem(int id);

    public Page<ServiceItemTo> findServiceItems(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    public ServiceItemTo createServiceItem(ServiceItemInput input);
    public int createServiceItems(List<ServiceItemInput> input);

    public ServiceItemTo modifyServiceItem(ServiceItemInput input);

    /**
     * Perfoms bulk update of shipment items in two modes: by shipmentItem and by Service
     * -By shipment item: modifies only the items passed as argument
     * -By shipment: updates all items in a shipment using the first item in input as template
     * @param shipmentId the shipment to have all of its items updated (optional)
     * @param input the list of items to update, or the template to use when modifying all items of a shipment
     * @return the number of elements updated
     * 
     */
    public int modifyServiceItems(Integer shipmentId, List<ServItemInput> input);

    public int deleteServiceItem(int id);

        /**
        * Perfoms bulk delete of shipment items in two modes: by shipmentItem and by Service
        * -By shipment item: deletes only the items with the ids passed as argument
        * -By shipment: deletes all items in a given shipment
        * @param shipmentId the shipment to have all of its items removed (optional)
        * @param input the list of item ids to remove (optional)
        * @return the number of elements deleted
        * 
        */
    public int deleteServiceItems(Integer shipmentId, List<Integer> input);

    public ServiceTo findService(int shipmentItemId);

    public TenantTo findTenant(int shipmentItemId);

    public OccurrenceShipmentTo findOccurrenceShipment(int serviceItemId);

}
