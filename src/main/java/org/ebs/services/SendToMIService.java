package org.ebs.services;

public interface SendToMIService {
    public String sendPayload(String payload);
}
