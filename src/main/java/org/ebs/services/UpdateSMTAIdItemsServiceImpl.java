package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import org.ebs.model.ServiceItemModel;
import org.ebs.model.ServiceModel;
import org.ebs.model.repos.ServiceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static java.util.stream.Collectors.toList;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class UpdateSMTAIdItemsServiceImpl implements UpdateSMTAIdItemsService {

    private final ServiceRepository serviceRepository;

    @Override
    @Transactional(readOnly = false)
    public String updateSmtaIdItems(Integer id, String smtaId, String mtaStatus) {
        List<String> mtaStatuses = new ArrayList<>();
        if (mtaStatus.equals("pud")) {
            mtaStatuses.add("pud");
            mtaStatuses.add("pud1");
        }
        if (mtaStatus.equals("pud2")) {
            mtaStatuses.add("pud2");
        }
        if (mtaStatus.equals("mls")) {
            mtaStatuses.add("fao");
            mtaStatuses.add("mls");
            mtaStatuses.add("rel1");
            mtaStatuses.add("smta");
        }

        final List<ServiceItemModel> target = new ArrayList<>();
        ServiceModel ship = verifyService(id);
        target.addAll(ship.getItems().stream().filter(si -> !si.isDeleted()).collect(toList()));
        for (int i = 0; i < target.size(); i++) {
            if (mtaStatus.equals("other"))
                target.get(i).setSmtaId(smtaId);
            else if (mtaStatuses.contains(target.get(i).getMtaStatus().toLowerCase()))
                target.get(i).setSmtaId(smtaId);
        }
        return "The items have been updated";

    }

    ServiceModel verifyService(Integer serviceId) {
        return serviceRepository.findById(serviceId).orElseThrow(
                () -> new RuntimeException("Service " + serviceId + " not found"));
    }

}
