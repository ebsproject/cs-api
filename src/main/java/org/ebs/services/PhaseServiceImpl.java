package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ebs.model.HtmlTagModel;
import org.ebs.model.PhaseModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.WorkflowViewTypeModel;
import org.ebs.model.repos.HtmlTagRepository;
import org.ebs.model.repos.PhaseRepository;
import org.ebs.model.repos.StageRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.ebs.model.repos.WorkflowViewTypeRepository;
import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.PhaseTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.Input.PhaseInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class PhaseServiceImpl implements PhaseService {

    private final ConversionService converter;
    private final PhaseRepository phaseRepository;
    private final HtmlTagRepository htmlTagRepository;
    private final WorkflowRepository workflowRepository;
    private final WorkflowViewTypeRepository workflowViewTypeRepository;
    private final StageRepository stageRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<PhaseTo> findPhase(int id) {
        if (id < 1)
            return Optional.empty();

        return phaseRepository.findById(id).map(r -> converter.convert(r, PhaseTo.class));
    }

    @Override
    public Page<PhaseTo> findPhases(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return phaseRepository
                .findByCriteria(PhaseModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, PhaseTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public PhaseTo createPhase(PhaseInput input) {

        // if (!phaseRepository.findByDesignRef(input.getDesignRef()).isEmpty())
        //     throw new RuntimeException("Design ref must be unique");
        
        input.setId(0);

        PhaseModel model = converter.convert(input, PhaseModel.class);

        setPhaseDependencies(model, input);

        model = phaseRepository.save(model);

        return converter.convert(model, PhaseTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public PhaseTo modifyPhase(PhaseInput input) {
        
        PhaseModel target = verifyPhaseModel(input.getId());

        // if (phaseRepository.findByDesignRef(input.getDesignRef())
        //     .stream()
        //     .filter(r -> !r.equals(target)).count() > 0)
        //         throw new RuntimeException("Design ref must be unique");

        Utils.copyNotNulls(input, target);

        setPhaseDependencies(target, input);

        return converter.convert(phaseRepository.save(target), PhaseTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deletePhase(int id) {

        PhaseModel model = verifyPhaseModel(id);

        model.setDeleted(true);

        phaseRepository.save(model);

        return id;

    }

    @Override
    public HtmlTagTo findHtmlTag(int phaseId) {

        PhaseModel model = verifyPhase(phaseId);

        if (model.getHtmlTag() == null)
            return null;
        
        return converter.convert(model.getHtmlTag(), HtmlTagTo.class);

    }

    @Override
    public WorkflowTo findWorkflow(int phaseId) {

        PhaseModel model = verifyPhase(phaseId);

        if (model.getWorkflow() == null)
            return null;
        
        return converter.convert(model.getWorkflow(), WorkflowTo.class);

    }

    @Override
    public TenantTo findTenant(int phaseId) {

        PhaseModel model = verifyPhase(phaseId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    PhaseModel verifyPhase(int phaseId) {
        return phaseRepository.findById(phaseId)
            .orElseThrow(() -> new RuntimeException("Phase id " + phaseId + " not found"));
    }

    @Override
    public WorkflowViewTypeTo findWorkflowViewType(int phaseId) {

        PhaseModel model = verifyPhase(phaseId);

        if (model.getWorkflowViewType() == null)
            return null;
        
        return converter.convert(model.getWorkflowViewType(), WorkflowViewTypeTo.class);

    }

    PhaseModel verifyPhaseModel(int phaseId) {
        return phaseRepository.findById(phaseId)
            .orElseThrow(() -> new RuntimeException("Phase id " + phaseId + " not found"));
    }

    void setPhaseDependencies(PhaseModel model, PhaseInput input) {
        setPhaseHtmlTag(model, input.getHtmlTagId());
        setPhaseWorkflow(model, input.getWorkflowId());
        setPhaseWorkflowViewType(model, input.getWorkflowViewTypeId());
        setPhaseTenant(model, input.getTenantId());
    }

    void setPhaseHtmlTag(PhaseModel model, Integer htmlTagId) {
        if (htmlTagId != null && htmlTagId > 0) {
            HtmlTagModel htmlTag = htmlTagRepository.findById(htmlTagId)
                .orElseThrow(() -> new RuntimeException("HtmlTag id " + htmlTagId + " not found"));
            model.setHtmlTag(htmlTag);
        }
    } 

    void setPhaseWorkflow(PhaseModel model, int workflowId) {
        if (workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
    } 

    void setPhaseWorkflowViewType(PhaseModel model, Integer workflowViewTypeId) {
        if (workflowViewTypeId != null && workflowViewTypeId > 0) {
            WorkflowViewTypeModel workflowViewType = workflowViewTypeRepository.findById(workflowViewTypeId)
                .orElseThrow(() -> new RuntimeException("WorkflowViewType id " + workflowViewTypeId + " not found"));
            model.setWorkflowViewType(workflowViewType);
        }
    } 

    void setPhaseTenant(PhaseModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    }

    @Override
    public List<StageTo> findStages(int phaseId) {
        return stageRepository.findByPhaseId(phaseId)
            .stream().map(r -> converter.convert(r, StageTo.class)).collect(Collectors.toList());
    } 
    
}
