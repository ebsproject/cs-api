package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.ShipmentFileTo;
import org.ebs.services.to.ShipmentTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.ShipmentFileInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ShipmentFileService {

    public Optional<ShipmentFileTo> findShipmentFile(int id);

    public Page<ShipmentFileTo> findShipmentFiles(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public ShipmentFileTo createShipmentFile(ShipmentFileInput input);

    public ShipmentFileTo modifyShipmentFile(ShipmentFileInput input);

    public int deleteShipmentFile(int id);

    public ShipmentTo findShipment(int shipmentFileId);

    public FileObjectTo findFileObject(int shipmentFileId);

    public TenantTo findTenant(int shipmentFileId);

}
