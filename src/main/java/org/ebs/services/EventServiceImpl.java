package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.ebs.model.EventModel;
import org.ebs.model.NodeModel;
import org.ebs.model.StageModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowInstanceModel;
import org.ebs.model.repos.EventRepository;
import org.ebs.model.repos.NodeRepository;
import org.ebs.model.repos.StageRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowInstanceRepository;
import org.ebs.services.to.CFValueTo;
import org.ebs.services.to.EventTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.StageTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowInstanceTo;
import org.ebs.services.to.Input.EventInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final ConversionService converter;
    private final EventRepository eventRepository;
    private final StageRepository stageRepository;
    private final TenantRepository tenantRepository;
    private final NodeRepository nodeRepository;
    private final WorkflowInstanceRepository workflowInstanceRepository;

    @Override
    public Optional<EventTo> findEvent(int id) {
        if (id < 1)
            return Optional.empty();

        return eventRepository.findById(id).map(r -> converter.convert(r, EventTo.class));
    }

    @Override
    public Page<EventTo> findEvents(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return eventRepository
                .findByCriteria(EventModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, EventTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public EventTo createEvent(EventInput input) {
        
        input.setId(0);

        EventModel model = converter.convert(input, EventModel.class);

        setEventDependencies(model, input);

        model = eventRepository.save(model);

        return converter.convert(model, EventTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public EventTo modifyEvent(EventInput input) {
        
        EventModel target = verifyEventModel(input.getId());

        EventModel source = converter.convert(input, EventModel.class);

        Utils.copyNotNulls(source, target);

        setEventDependencies(target, input);

        return converter.convert(eventRepository.save(target), EventTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteEvent(int id) {

        EventModel model = verifyEventModel(id);

        model.setDeleted(true);

        eventRepository.save(model);

        return id;

    }

    @Override
    public TenantTo findTenant(int eventId) {
        
        EventModel model = verifyEventModel(eventId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    @Override
    public StageTo findStage(int eventId) {

        EventModel model = verifyEventModel(eventId);

        if (model.getStage() == null)
            return null;
        
        return converter.convert(model.getStage(), StageTo.class);

    }

    @Override
    public WorkflowInstanceTo findWorkflowInstance(int eventId) {

        EventModel model = verifyEventModel(eventId);

        if (model.getWorkflowInstance() == null)
            return null;
        
        return converter.convert(model.getWorkflowInstance(), WorkflowInstanceTo.class);

    }

    @Override
    public List<CFValueTo> findCFValues(int eventId) {

        EventModel model = verifyEventModel(eventId);

        if (model.getCfValues() == null)
            return null;
        
        return model.getCfValues().stream().map(cfValue -> converter.convert(cfValue, CFValueTo.class)).collect(Collectors.toList());

    }

    @Override
    public NodeTo findNode(int eventId) {

        EventModel model = verifyEventModel(eventId);

        if (model.getNode() == null)
            return null;
        
        return converter.convert(model.getNode(), NodeTo.class);

    }

    EventModel verifyEventModel(int eventId) {
        return eventRepository.findById(eventId)
            .orElseThrow(() -> new RuntimeException("Event id " + eventId + " not found"));
    }

    void setEventDependencies(EventModel model, EventInput input) {
        setEventStage(model, input.getStageId());
        setEventTenant(model, input.getTenantId());
        setEventWorkflowInstance(model, input.getWorkflowInstanceId());
        setEventNode(model, input.getNodeId());
    }

    void setEventStage(EventModel model, Integer stageId) {
        if (stageId != null) {
            StageModel stage = stageRepository.findById(stageId)
                .orElseThrow(() -> new RuntimeException("Stage id " + stageId + " not found"));
            model.setStage(stage);
        }
    }

    void setEventTenant(EventModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    } 

    void setEventWorkflowInstance(EventModel model, Integer workflowInstanceId) {
        if (workflowInstanceId != null) {
            WorkflowInstanceModel workflowInstance = workflowInstanceRepository.findById(workflowInstanceId)
                .orElseThrow(() -> new RuntimeException("Workflow instance id " + workflowInstanceId + " not found"));
            model.setWorkflowInstance(workflowInstance);
        }
    }

    void setEventNode(EventModel model, Integer nodeId) {
        if (nodeId != null && nodeId > 0) {
            NodeModel node = nodeRepository.findById(nodeId)
                .orElseThrow(() -> new RuntimeException("Node id " + nodeId + " not found"));
            model.setNode(node);
        }
    } 
    
}
