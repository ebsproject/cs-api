package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.AddressModel;
import org.ebs.model.CustomerModel;
import org.ebs.model.OrganizationModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.CustomerRepository;
import org.ebs.model.repos.OrganizationRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.Address;
import org.ebs.services.to.CustomerTo;
import org.ebs.services.to.OrganizationTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.OrganizationInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service @Transactional(readOnly = true)
@RequiredArgsConstructor
  class OrganizationServiceImpl implements OrganizationService {

	private final OrganizationRepository organizationRepository;
	private final ConversionService converter;
	private final TenantRepository tenantRepository;
	private final CustomerRepository customerRepository;
	private final AddressRepository addressRepository;

	/**
	 *
	 * @param organization
	 */
	@Override @Transactional(readOnly = false)
	public OrganizationTo createOrganization(OrganizationInput organization){
		organization.setId(0);
		OrganizationModel model = converter.convert(organization,OrganizationModel.class);
		setOrganizationDependencies(model, organization);
		checkOrganizationCode(model);
		return converter.convert(organizationRepository.save(model), OrganizationTo.class);
	}

	void checkOrganizationCode(OrganizationModel organization) {
		Optional<OrganizationModel> organizationModel = organizationRepository.findByCodeAndDeletedIsFalse(organization.getCode());
		organizationModel.ifPresent(org -> {
			if(org.getId() != organization.getId())
				throw new RuntimeException("Given code already exists");
		});
	}

	void setOrganizationDependencies(OrganizationModel model, OrganizationInput input) {
		setOrganizationOwningCustomer(model, input);
		setOrganizationAddress(model, input);
	}

	void setOrganizationOwningCustomer(OrganizationModel model, OrganizationInput input) {
		if(input.getOwningCustomerId() == null) return;

		CustomerModel customerModel = customerRepository.findByIdAndDeletedIsFalse(input.getOwningCustomerId())
			.orElseThrow(() -> new RuntimeException("Customer not found"));
		model.setOwningCustomer(customerModel);
	}

	void setOrganizationAddress(OrganizationModel model, OrganizationInput input) {
		if (input.getAddress() == null)
			return;
		AddressModel address = new AddressModel();
		if (input.isGlobalAddress()) {
			//Assign an existing Global Address
			address = addressRepository.findById(input.getAddress().getId())
				.orElseThrow(() -> new RuntimeException("Global address " + input.getAddress().getId() + " not found"));
		} else if (model.getAddress() == null) {
			//Create a new address and assign it
			input.getAddress().setId(0);
			address = converter.convert(input.getAddress(), AddressModel.class);
			address = addressRepository.save(address);
		} else if (model.getAddress().getId() == input.getAddress().getId()) {
			//Modify the existing address
			address = addressRepository.findById(input.getAddress().getId())
				.orElseThrow(() -> new RuntimeException("Address " + input.getAddress().getId() + " not found"));
			Utils.copyNotNulls(converter.convert(input.getAddress(), AddressModel.class), address);
			address = addressRepository.save(address);
		} else {
			//Attempted to modify a different address
			throw new RuntimeException("Cannot modify a different address");
		}
		model.setAddress(address);
	}

	/**
	 *
	 * @param organizationId
	 */
	@Override @Transactional(readOnly = false)
	public int deleteOrganization(int organizationId){
		OrganizationModel organization = organizationRepository.findByIdAndDeletedIsFalse(organizationId)
			.orElseThrow(() -> new RuntimeException("Organization not found"));

		organization.setDeleted(true);
		organizationRepository.save(organization);
		return organizationId;
	}

	/**
	 *
	 * @param organizationId
	 */
	public Optional<CustomerTo> findOwningCustomer(int organizationId){
		return organizationRepository.findByIdAndDeletedIsFalse(organizationId).map(r -> converter.convert(r.getOwningCustomer(),CustomerTo.class));
	}

	/**
	 *
	 * @param organizationId
	 */
	public Set<CustomerTo> findOwnedCustomers(int organizationId){
		return customerRepository.findByOwningOrganizationId(organizationId).stream().map(e -> converter.convert(e,CustomerTo.class)).collect(Collectors.toSet());
	}

	/**
	 *
	 * @param organizationId
	 */
	@Override
	public Optional<OrganizationTo> findOrganization(int organizationId){
		if(organizationId <1)
		 {return Optional.empty();}
		 return organizationRepository.findByIdAndDeletedIsFalse(organizationId).map(r -> converter.convert(r,OrganizationTo.class));
	}

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	@Override
	public Page<OrganizationTo> findOrganizations(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters){
		return organizationRepository.findByCriteria(OrganizationModel.class,filters,sort,page,disjunctionFilters).map(r -> converter.convert(r,OrganizationTo.class));
	}

	/**
	 *
	 * @param organizationId
	 */
	public Set<TenantTo> findTenants(int organizationId){
		return tenantRepository.findByOrganizationId(organizationId).stream().map(e -> converter.convert(e,TenantTo.class)).collect(Collectors.toSet());
	}

	/**
	 *
	 * @param organization
	 */
	@Override @Transactional(readOnly = false)
	public OrganizationTo modifyOrganization(OrganizationInput organization){

		OrganizationModel target= organizationRepository.findByIdAndDeletedIsFalse(organization.getId())
			.orElseThrow(() -> new RuntimeException("Organization not found"));

		OrganizationModel source= converter.convert(organization,OrganizationModel.class);
		checkOrganizationCode(source);
		Utils.copyNotNulls(source,target);
		setOrganizationDependencies(target, organization);
		return converter.convert(organizationRepository.save(target), OrganizationTo.class);
	}

	@Override
	public Optional<Address> findAddress(int organizationId) {
		return addressRepository.findByOrganization(organizationId).map(r -> converter.convert(r, Address.class));
	}

}