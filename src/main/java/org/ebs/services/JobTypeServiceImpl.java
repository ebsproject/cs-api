package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.JobTypeModel;
import org.ebs.model.repos.JobTypeRepository;
import org.ebs.services.to.JobTypeTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class JobTypeServiceImpl implements JobTypeService {

    private final JobTypeRepository jobTypeRepository;
    private final ConversionService converter;

    @Override
    public Optional<JobTypeTo> findJobType(int jobLogId) {
        if (jobLogId < 1) {
            return Optional.empty();
        }
        return jobTypeRepository.findById(jobLogId).map(r -> converter.convert(r, JobTypeTo.class));
    }

    @Override
    public Page<JobTypeTo> findJobTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
        return jobTypeRepository.findByCriteria(JobTypeModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, JobTypeTo.class));
    }

}
