///////////////////////////////////////////////////////////
//  LanguageService.java
//  Macromedia ActionScript Implementation of the Interface LanguageService
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:04 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
import java.util.Set;
import org.ebs.services.to.LanguageTo;
import org.ebs.services.to.Input.LanguageInput;
import org.ebs.services.to.TranslationTo;
import org.ebs.services.to.Contact;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:04 AM
 */
public interface LanguageService {

    /**
     *
     * @param Language
     */
    public LanguageTo createLanguage(LanguageInput Language);

    /**
     *
     * @param languageId
     */
    public int deleteLanguage(int languageId);

    /**
     *
     * @param languageId
     */
    public Optional<LanguageTo> findLanguage(int languageId);

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    public Page<LanguageTo> findLanguages(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);

    /**
     *
     * @param languageId
     */
    public Set<Contact> findContacts(int languageId);

    /**
     *
     * @param languageId
     */
    public Set<TranslationTo> findTranslations(int languageId);

    /**
     *
     * @param language
     */
    public LanguageTo modifyLanguage(LanguageInput language);

}