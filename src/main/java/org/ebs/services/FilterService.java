package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.DomainTo;
import org.ebs.services.to.FilterTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.FilterEntityInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface FilterService {
    
    public Optional<FilterTo> findFilter(int id);

    public Page<FilterTo> findFilters(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public FilterTo createFilter(FilterEntityInput input);

    public FilterTo modifyFilter(FilterEntityInput input);

    public int deleteFilter(int id);

    public TenantTo findTenant(int filterId);

    public List<DomainTo> findDomains(int filterId);

    public String createFilterDomain(int filterId, int domainId);

    public String deleteFilterDomain(int filterId, int domainId);
}
