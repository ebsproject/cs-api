package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.NodeTypeModel;
import org.ebs.model.repos.NodeTypeRepository;
import org.ebs.services.to.NodeTypeTo;
import org.ebs.services.to.Input.NodeTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class NodeTypeServiceImpl implements NodeTypeService {
    
    private final ConversionService converter;
    private final NodeTypeRepository NodeTypeRepository;

    @Override
    public Optional<NodeTypeTo> findNodeType(int id) {
        if (id < 1)
            return Optional.empty();

        return NodeTypeRepository.findById(id).map(r -> converter.convert(r, NodeTypeTo.class));
    }

    @Override
    public Page<NodeTypeTo> findNodeTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return NodeTypeRepository
                .findByCriteria(NodeTypeModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, NodeTypeTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public NodeTypeTo createNodeType(NodeTypeInput input) {
        
        input.setId(0);

        NodeTypeModel model = converter.convert(input, NodeTypeModel.class);


        model = NodeTypeRepository.save(model);

        return converter.convert(model, NodeTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public NodeTypeTo modifyNodeType(NodeTypeInput input) {
        
        NodeTypeModel target = verifyNodeTypeModel(input.getId());

        NodeTypeModel source = converter.convert(input, NodeTypeModel.class);

        Utils.copyNotNulls(source, target);

        return converter.convert(NodeTypeRepository.save(target), NodeTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteNodeType(int id) {

        NodeTypeModel model = verifyNodeTypeModel(id);

        model.setDeleted(true);

        NodeTypeRepository.save(model);

        return id;

    }

    NodeTypeModel verifyNodeTypeModel(int NodeTypeId) {
        return NodeTypeRepository.findById(NodeTypeId)
            .orElseThrow(() -> new RuntimeException("NodeType id " + NodeTypeId + " not found"));
    }



}
