package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.ContactModel;
import org.ebs.model.HierarchyModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.HierarchyRepository;
import org.ebs.model.repos.RoleContactHierarchyRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.Input.HierarchyInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class HierarchyServiceImpl implements HierarchyService{

    private final HierarchyRepository hierarchyRepository;
    private final ContactRepository contactRepository;
    private final ConversionService converter;
    private final UserRepository userRepository;
    private final RoleContactHierarchyRepository roleContactHierarchyRepository;

    @Override
    @Transactional(readOnly = false)
    public HierarchyTo createHierarchy(HierarchyInput hierarchyInput) {

        HierarchyModel model = new HierarchyModel();

        verifyDependencies(model, hierarchyInput.getContactId(), hierarchyInput.getInstitutionId());

        boolean modelExists = hierarchyRepository.findByContactAndInstitution(model.getContact(), model.getInstitution()).isPresent();

        if (modelExists) {
            HierarchyModel existingHierarchy = hierarchyRepository.findByContactAndInstitution(model.getContact(), model.getInstitution()).get();
            if (!existingHierarchy.isDeleted()) {
                if (!existingHierarchy.isPrincipalContact()) {
                    verifyPrincipalContact(existingHierarchy, hierarchyInput);
                }
                return converter.convert(hierarchyRepository.save(existingHierarchy), HierarchyTo.class);
            }
            else {
                verifyPrincipalContact(existingHierarchy, hierarchyInput);
                existingHierarchy.setDeleted(false);
                hierarchyRepository.save(existingHierarchy);
                return converter.convert(existingHierarchy, HierarchyTo.class);
            }
        } else {
            model = converter.convert(hierarchyInput, HierarchyModel.class);

            verifyDependencies(model, hierarchyInput.getContactId(), hierarchyInput.getInstitutionId());

            verifyPrincipalContact(model, hierarchyInput);
        
            model = hierarchyRepository.save(model);

            return converter.convert(model, HierarchyTo.class);
        }
    }

    @Override
    @Transactional(readOnly = false)
    public HierarchyTo modifyHierarchy(HierarchyInput hierarchyInput, boolean verified) {
        HierarchyModel target = new HierarchyModel();

        HierarchyModel source = converter.convert(hierarchyInput, HierarchyModel.class);

        verifyDependencies(target, hierarchyInput.getContactId(), hierarchyInput.getInstitutionId());

        target = verifyHierarchy(target);

        if (!target.isPrincipalContact()) {
            verifyPrincipalContact(target, hierarchyInput);
            Utils.copyNotNulls(source, target);
        } else {
            Utils.copyNotNulls(source, target);
        }

        return converter.convert(hierarchyRepository.save(target), HierarchyTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public String deleteHierarchy(int contactId, int institutionId) {
        HierarchyModel target = new HierarchyModel();

        verifyDependencies(target, contactId, institutionId);

        target = verifyHierarchy(target);

        roleContactHierarchyRepository.deleteByHierarchyId(target.getId());

        target.setDeleted(true);

        hierarchyRepository.save(target);

        return "Hierarchy deleted";
    }

    @Override
    @Transactional(readOnly = false)
    public int deleteHierarchy(int hierarchyId) {
        HierarchyModel target = verifyHierarchy(hierarchyId);

        roleContactHierarchyRepository.deleteByHierarchyId(target.getId());

        target.setDeleted(true);

        hierarchyRepository.save(target);

        return hierarchyId;
    }

    HierarchyModel verifyHierarchy(int hierarchyId) {
        return hierarchyRepository.findById(hierarchyId)
            .orElseThrow(() -> new RuntimeException("Hierarchy not found"));
    }

    @Override
    public Optional<HierarchyTo> findHierarchy(int id) {

        if (id < 1)
            return Optional.empty();

        return hierarchyRepository.findById(id).map(r -> converter.convert(r, HierarchyTo.class));
    }

    HierarchyModel verifyHierarchy(HierarchyModel hierarchy) {
        return hierarchyRepository.findByContactAndInstitutionAndDeletedIsFalse(hierarchy.getContact(), hierarchy.getInstitution())
            .orElseThrow(() -> new RuntimeException("Hierarchy not found"));
    }

    void verifyDependencies(HierarchyModel model, int contactId, int institutionId) {
        ContactModel contact = contactRepository.findById(contactId).orElseThrow(() -> new RuntimeException("Contact not found"));
        ContactModel parent = contactRepository.findById(institutionId).orElseThrow(() -> new RuntimeException("Institution not found"));
        if (parent.getCategory().getName().equals("Person"))
            throw new RuntimeException("Invalid institution/parent");
        model.setContact(contact);
        model.setInstitution(parent);
    }

    void verifyPrincipalContact(HierarchyModel model, HierarchyInput hierarchyInput) {
        if (hierarchyInput.isPrincipalContact()) {
            hierarchyRepository
            .findByInstitutionAndIsPrincipalContactIsTrueAndDeletedIsFalse(model.getInstitution())
                .ifPresent(i -> {throw new RuntimeException("Institution " + hierarchyInput.getInstitutionId() + " already has a principal contact");});
        }
    }

    @Override
    public Contact findContact(int contactId) {
        return contactRepository.findByIdAndDeletedIsFalse(contactId)
            .map(r -> converter.convert(r, Contact.class)).orElseThrow(() -> new RuntimeException("Error while fetching hierarchy contact: " + contactId + " not found"));
    }

    @Override
    public Contact findInstitution(int institutionId) {
        return contactRepository.findByIdAndDeletedIsFalse(institutionId)
            .map(r -> converter.convert(r, Contact.class)).orElse(null);
    }

    @Override
    public Page<HierarchyTo> findHierarchyList(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
                return hierarchyRepository.findByCriteria(HierarchyModel.class,filters,sort,page,disjunctionFilters).map(r -> converter.convert(r,HierarchyTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public boolean deleteContactParentsRelation(int contactId) {

        ContactModel contact = contactRepository.findById(contactId)
            .orElse(null);
        
        if (contact == null)
            throw new RuntimeException("Could not remove parent associations, contact " + contactId + " not found");

        hierarchyRepository.findByContactAndDeletedIsFalse(contact)
            .forEach(r -> {
                r.setDeleted(true);
                hierarchyRepository.save(r);
            });

        return true;
    }

    @Override
    public HierarchyTo findHierarchy(int contactId, int institutionId) {
        if ( contactId <= 0 || institutionId <= 0)
            throw new RuntimeException("Hierarchy not found");
        return hierarchyRepository
            .findByContactIdAndInstitutionIdAndDeletedIsFalse(contactId, institutionId)
                .map(r -> converter.convert(r, HierarchyTo.class)).orElse(null);
    }

    @Override
    @Transactional(readOnly = false)
    public void cleanHierarchies() {
        System.out.println("cleaning hierarchies");
        contactRepository.findAll().forEach(contact -> {
            if (contact.isDeleted()) {
                contact.getParents().forEach(parent -> {
                    parent.setDeleted(true);
                    hierarchyRepository.save(parent);
                    roleContactHierarchyRepository.deleteByHierarchyId(parent.getId());
                });
                contact.getMembers().forEach(member -> {
                    member.setDeleted(true);
                    hierarchyRepository.save(member);
                    roleContactHierarchyRepository.deleteByHierarchyId(member.getId());
                });
                contact.getUsers().forEach(user -> {
                    user.setActive(false);
                    user.setDeleted(true);
                    userRepository.save(user);
                });
            }
        });
    }
}
