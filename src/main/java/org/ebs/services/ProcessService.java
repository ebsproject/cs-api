package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.Input.ProcessInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ProcessService {

    /**
     *
     * @param Process
     */
    public ProcessTo createProcess(ProcessInput Process);

    /**
     *
     * @param processId
     */
    public int deleteProcess(int processId);

    /**
     *
     * @param processId
     */
    public Optional<ProcessTo> findProcess(int processId);

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    public Page<ProcessTo> findProcesses(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters);

    /**
     *
     * @param process
     */
    public ProcessTo modifyProcess(ProcessInput process);

}