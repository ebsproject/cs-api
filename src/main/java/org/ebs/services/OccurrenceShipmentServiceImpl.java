package org.ebs.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.ebs.model.ContactModel;
import org.ebs.model.OccurrenceShipmentModel;
import org.ebs.model.ServiceModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.OccurrenceShipmentRepository;
import org.ebs.model.repos.ServiceItemRepository;
import org.ebs.model.repos.ServiceRepository;
import org.ebs.services.redirect.MicroIntegrationService;
import org.ebs.services.to.Contact;
import org.ebs.services.to.OccurrenceShipmentTo;
import org.ebs.services.to.ServiceItemTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.Input.OccurrenceShipmentInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class OccurrenceShipmentServiceImpl implements OccurrenceShipmentService {
    
    private final ConversionService converter;
    private final OccurrenceShipmentRepository occurrenceShipmentRepository;
    private final ServiceItemRepository serviceItemRepository;
    private final ServiceRepository serviceRepository;
    private final ContactRepository contactRepository;
    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;
    protected final TokenGenerator tokenGenerator;

    @Override
    public OccurrenceShipmentTo findOccurrenceShipment(int id) {
        if (id < 1)
            return null;

        return occurrenceShipmentRepository.findById(id).map(r -> converter.convert(r, OccurrenceShipmentTo.class)).orElse(null);
    }

    @Override
    public Page<OccurrenceShipmentTo> findOccurrenceShipments(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return occurrenceShipmentRepository
                .findByCriteria(OccurrenceShipmentModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, OccurrenceShipmentTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public OccurrenceShipmentTo createOccurrenceShipment(OccurrenceShipmentInput input) {
        
        input.setId(0);

        if (input.getValidated() == null)
            input.setValidated(false);

        OccurrenceShipmentModel model = converter.convert(input, OccurrenceShipmentModel.class);

        setOccurrenceShipmentDependencies(model, input);

        model = occurrenceShipmentRepository.save(model);

        return converter.convert(model, OccurrenceShipmentTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public OccurrenceShipmentTo modifyOccurrenceShipment(OccurrenceShipmentInput input) {
        
        OccurrenceShipmentModel target = verifyOccurrenceShipmentModel(input.getId());

        if (input.getValidated() == null)
            input.setValidated(target.isValidated());

        OccurrenceShipmentModel source = converter.convert(input, OccurrenceShipmentModel.class);

        Utils.copyNotNulls(source, target);

        setOccurrenceShipmentDependencies(target, input);

        return converter.convert(occurrenceShipmentRepository.save(target), OccurrenceShipmentTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteOccurrenceShipment(int id) {

        OccurrenceShipmentModel model = verifyOccurrenceShipmentModel(id);

        model.setDeleted(true);

        occurrenceShipmentRepository.save(model);

        return id;

    }

    OccurrenceShipmentModel verifyOccurrenceShipment(int occurrenceShipmentId) {
        return occurrenceShipmentRepository.findById(occurrenceShipmentId)
                .orElseThrow(() -> new RuntimeException("Occurrence shipment " + occurrenceShipmentId + " not found"));
    }

    @Override
    public ServiceTo findService(int occurrenceShipmentId) {

        OccurrenceShipmentModel model = verifyOccurrenceShipment(occurrenceShipmentId);

        if (model.getService() == null)
            return null;

        return converter.convert(model.getService(), ServiceTo.class);

    }

    @Override
    public Contact findRecipient(int occurrenceShipmentId) {

        OccurrenceShipmentModel model = verifyOccurrenceShipment(occurrenceShipmentId);

        if (model.getRecipient() == null)
            return null;

        return converter.convert(model.getRecipient(), Contact.class);

    }

    @Override
    public List<ServiceItemTo> findItems(int occurrenceShipmentId) {

        return serviceItemRepository.findByOccurrenceShipment(occurrenceShipmentId).stream()
                .map(r -> converter.convert(r, ServiceItemTo.class)).collect(Collectors.toList());

    }

    OccurrenceShipmentModel verifyOccurrenceShipmentModel(int occurrenceShipmentId) {
        return occurrenceShipmentRepository.findById(occurrenceShipmentId)
            .orElseThrow(() -> new RuntimeException("OccurrenceShipment id " + occurrenceShipmentId + " not found"));
    }

    void setOccurrenceShipmentDependencies(OccurrenceShipmentModel model, OccurrenceShipmentInput input) {
        setOccurrenceShipmentService(model, input.getServiceId());
        setOccurrenceShipmentRecipient(model, input.getRecipientId());
    }

    void setOccurrenceShipmentService(OccurrenceShipmentModel model, Integer serviceId) {
        if (serviceId != null) {
            model.setService(verifyService(serviceId));
        }
    }

    ServiceModel verifyService(Integer serviceId) {
        return serviceRepository.findById(serviceId).orElseThrow(
                () -> new RuntimeException("Service " + serviceId + " not found"));
    }

    void setOccurrenceShipmentRecipient(OccurrenceShipmentModel model, Integer recipientId) {
        if (recipientId != null && recipientId > 0) {
            ContactModel contact = contactRepository.findById(recipientId)
                    .orElseThrow(() -> new RuntimeException("Person id " + recipientId + " not found"));
            model.setRecipient(contact);
        }
    }

    @Override
    @Transactional(readOnly = false)
    public int importOccurrenceShipments(MultipartFile file, int serviceId) throws Exception {
        ServiceModel service = verifyService(serviceId);
        BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
        List<String> csvLines = br.lines().collect(Collectors.toList());
        br.close();
        if (csvLines.size() == 0 || csvLines.size() == 1)
            throw new RuntimeException("Incompatible csv format");
        csvLines.remove(0);
        List<OccurrenceShipmentModel> currentShipments = service.getOccurrenceShipments();
        List<OccurrenceShipmentModel> shipments = new ArrayList<>();
        List<Integer> occurrenceIdsInCb = getOccurrenceIdsByList(csvLines);
        for (String line: csvLines) {
            String[] fields = line.split(",");
            List<ContactModel> contactsWithCoopkey = contactRepository.findByContactInfosContactInfoTypeNameInIgnoreCaseAndContactInfosValueAndContactInfosDeletedIsFalseAndDeletedIsFalse(Arrays.asList("COOPKEY"), fields[1]);
            if (contactsWithCoopkey.isEmpty())
                throw new RuntimeException("No contact with associated coopkey " + fields[1] + " found");
            ContactModel recipient = contactsWithCoopkey.get(0);
            OccurrenceShipmentModel model = new OccurrenceShipmentModel();
            model.setId(0);
            model.setCoopkey(fields[1]);
            model.setRecipient(recipient);
            model.setService(service);
            model.setExperimentCode(fields[7]);
            model.setExperimentName(fields[8]);
            model.setExperimentYear(Integer.parseInt(fields[9]));
            model.setExperimentSeason(fields[10]);
            model.setOccurrenceName(fields[11]);
            model.setOccurrenceNumber(Integer.parseInt(fields[12]));
            model.setOccurrenceDbId(Integer.parseInt(fields[13]));
            model.setUnit("kg");
            if (fields.length > 14) {
                model.setWeight(Integer.parseInt(fields[14]));
                if (fields.length > 15) {
                    model.setTestCode(fields[15].isBlank() ? null : fields[15]);
                    if (fields.length > 16) {
                        model.setStatus(fields[16].isBlank() ? null : fields[16]);
                    }
                }
            }
            model.setValidated(occurrenceIdsInCb.contains(model.getOccurrenceDbId()));
            shipments.add(model);  
        }
        serviceItemRepository.deleteByOccurrenceShipmentList(currentShipments);
        occurrenceShipmentRepository.deleteByList(currentShipments);
        int newShipmentsImported = occurrenceShipmentRepository.saveAll(shipments).size();
        return newShipmentsImported;
    }

    private List<Integer> getOccurrenceIdsByList(List<String> csvLines) {
        List<Integer> occurrenceIdsFromCsv = new ArrayList<>();
        csvLines.forEach(line -> {
            occurrenceIdsFromCsv.add(Integer.parseInt(line.split(",")[13]));
        });
        try {
            ObjectMapper om = new ObjectMapper();
            MicroIntegrationService service = new MicroIntegrationService(WebClient
                .builder()
                .baseUrl(cbGraphQlEndpoint.replace("/graphql", "/"))
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
                .build());
            String cbGraphResponse = service.redirectRequestToMI("/occurrenceIdsByList", om.writeValueAsString(occurrenceIdsFromCsv));
            JsonNode data = om.readTree(cbGraphResponse);
            ObjectReader reader = om.readerFor(new TypeReference<List<Integer>>() {});
            return reader.readValue(data.get("content"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error validating occurrence data from CB");
        }
    }

}
