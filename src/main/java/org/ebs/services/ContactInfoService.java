package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.Contact;
import org.ebs.services.to.ContactInfo;
import org.ebs.services.to.Input.ContactInfoInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface ContactInfoService {

    List<ContactInfo> findByContact(int contactId);

    Contact findContactInfoContact(int contactInfoId);

    Page<ContactInfo> findContactInfos(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters);
    
    Optional<ContactInfo> findContactInfo(int contactInfoId);

    ContactInfo createContactInfo(ContactInfoInput input);

    ContactInfo modifyContactInfo(ContactInfoInput input);

    int deleteContactInfo(int contactInfoId);

    String getPersonCoopkey(int personId);
}