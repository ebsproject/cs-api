package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.CFTypeModel;
import org.ebs.model.repos.CFTypeRepository;
import org.ebs.services.to.CFTypeTo;
import org.ebs.services.to.Input.CFTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class CFTypeServiceImpl implements CFTypeService {
    
    private final ConversionService converter;
    private final CFTypeRepository cfTypeRepository;

    @Override
    public Optional<CFTypeTo> findCFType(int id) {
        if (id < 1)
            return Optional.empty();

        return cfTypeRepository.findById(id).map(r -> converter.convert(r, CFTypeTo.class));
    }

    @Override
    public Page<CFTypeTo> findCFTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return cfTypeRepository
                .findByCriteria(CFTypeModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, CFTypeTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public CFTypeTo createCFType(CFTypeInput input) {
        
        input.setId(0);

        CFTypeModel model = converter.convert(input, CFTypeModel.class);

        model = cfTypeRepository.save(model);

        return converter.convert(model, CFTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public CFTypeTo modifyCFType(CFTypeInput input) {
        
        CFTypeModel target = verifyCFTypeModel(input.getId());

        CFTypeModel source = converter.convert(input, CFTypeModel.class);

        Utils.copyNotNulls(source, target);

        return converter.convert(cfTypeRepository.save(target), CFTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteCFType(int id) {

        CFTypeModel model = verifyCFTypeModel(id);

        model.setDeleted(true);

        cfTypeRepository.save(model);

        return id;

    }

    CFTypeModel verifyCFTypeModel(int cfTypeId) {
        return cfTypeRepository.findById(cfTypeId)
            .orElseThrow(() -> new RuntimeException("CFType id " + cfTypeId + " not found"));
    }

}
