package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.HtmlTagTo;
import org.ebs.services.to.NodeCFTo;
import org.ebs.services.to.NodeTo;
import org.ebs.services.to.NodeTypeTo;
import org.ebs.services.to.ProcessTo;
import org.ebs.services.to.ProductTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.Input.NodeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface NodeService {

    public Optional<NodeTo> findNode(int id);

    public Page<NodeTo> findNodes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public NodeTo createNode(NodeInput input);

    public NodeTo modifyNode(NodeInput input);

    public int deleteNode(int id);

    public HtmlTagTo findHtmlTag(int nodeId);

    public ProductTo findProduct(int nodeId);

    public WorkflowTo findWorkflow(int nodeId);

    public ProcessTo findProcess(int nodeId);

    public WorkflowViewTypeTo findWorkflowViewType(int nodeId);

    public NodeTypeTo findNodeType(int nodeId);

    public List<NodeTo> findByStage(int stageId);

    public List<NodeCFTo> findNodeCFs(int nodeId);

    public TenantTo findTenant(int nodeId);
    
}