package org.ebs.services;

import static java.util.stream.Collectors.toSet;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.ContactModel;
import org.ebs.model.FunctionalUnitModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.FunctionalUnitRepository;
import org.ebs.services.to.FunctionalUnitTo;
import org.ebs.services.to.Input.FunctionalUnitInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class FunctionalUnitServiceImpl implements FunctionalUnitService {

    private final ConversionService converter;
    private final FunctionalUnitRepository funcUnitRepo;
    private final ContactRepository contactRepository;
    
    @Override
    public Optional<FunctionalUnitTo> findFunctionalUnit(int functionalUnitId) {
        if (functionalUnitId < 1) {
            return Optional.empty();
        }

        return funcUnitRepo.findByIdAndDeletedIsFalse(functionalUnitId)
                .map(fu -> converter.convert(fu, FunctionalUnitTo.class));
    }

    /**
     * Finds all root functional units (with no parent)
     */
    @Override
    public Page<FunctionalUnitTo> findFunctionalUnitList(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters) {
        /* if (filters == null) {
            filters = new ArrayList<>();
        }
        filters.add(new FilterInput("parent", null, FilterMod.NULL)); */

        return funcUnitRepo.findByCriteria(FunctionalUnitModel.class, filters, sort, page, disjunctionFilters)
                .map(e -> converter.convert(e, FunctionalUnitTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public FunctionalUnitTo createFunctionalUnit(FunctionalUnitInput functionalUnit) {
        functionalUnit.setId(0);
        FunctionalUnitModel functionalUnitModel = converter.convert(functionalUnit, FunctionalUnitModel.class);
        setFunctionalUnitDependencies(functionalUnitModel, functionalUnit);
        functionalUnitModel = funcUnitRepo.save(functionalUnitModel);

        return converter.convert(functionalUnitModel, FunctionalUnitTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public FunctionalUnitTo modifyFunctionalUnit(FunctionalUnitInput modifiedFunctionalUnit) {
        FunctionalUnitModel existingFunctionalUnitModel = verifyFuncUnitModel(modifiedFunctionalUnit.getId());
        Set<ContactModel> existingContacts = existingFunctionalUnitModel.getContacts();
        Utils.copyNotNulls(converter.convert(modifiedFunctionalUnit, FunctionalUnitModel.class), existingFunctionalUnitModel);
        existingFunctionalUnitModel.setContacts(existingContacts);
        setFunctionalUnitDependencies(existingFunctionalUnitModel, modifiedFunctionalUnit);
        existingFunctionalUnitModel = funcUnitRepo.save(existingFunctionalUnitModel);

        return converter.convert(existingFunctionalUnitModel, FunctionalUnitTo.class);
    }

    @Override
    @Transactional(readOnly = false)
    public int removeFunctionalUnit(int functionalUnitId) {
        FunctionalUnitModel functionalUnitModel = verifyFuncUnitModel(functionalUnitId);

        Optional<FunctionalUnitModel> child = functionalUnitModel.getChildren().stream()
            .filter(c -> !c.isDeleted())
            .findFirst();

        if(child.isPresent()) {
            throw new RuntimeException("Cannot remove because it has children. Remove children first.");
        }

        functionalUnitModel.setDeleted(true);
        funcUnitRepo.save(functionalUnitModel);

        return functionalUnitId;
    }

    @Override
    public Set<FunctionalUnitTo> findByParent(int parentFuncUnitId) {
        return funcUnitRepo.findByParentIdAndDeletedIsFalse(parentFuncUnitId).stream()
            .map(fu -> converter.convert(fu, FunctionalUnitTo.class))
            .collect(toSet());
    }

    /**
     * Assign a functional unit as child of an existing functional unit. This method
     * overrides the program and tenant of the FU to match the parent.
     *
     * @param fu
     * @param fuInput
     */
    void setFunctionalUnitDependencies(FunctionalUnitModel fu, FunctionalUnitInput fuInput) {

        if (fuInput.getParentFuncUnitId() != null) {
            setFunctionalUnitDependenciesFromParent(fu, fuInput);
        }
        if (fuInput.getContactIds() != null) {
            addContacts(fu, fuInput.getContactIds());
        }
    }

    void setFunctionalUnitDependenciesFromParent(FunctionalUnitModel fu, FunctionalUnitInput fuInput) {
        FunctionalUnitModel parent = funcUnitRepo.findByIdAndDeletedIsFalse(fuInput.getParentFuncUnitId())
                .orElseThrow(() -> new RuntimeException("Parent not found"));
        fu.setParent(parent);
    }

    /**
     * Validates a functional unit exists before trying to perform an action over it
     * @param functionalUnitId
     * @return a valid FunctionalUnit instance
     * @throws RuntimeException if the element does not exist
     */
    FunctionalUnitModel verifyFuncUnitModel(int functionalUnitId) {
        return funcUnitRepo.findByIdAndDeletedIsFalse(functionalUnitId)
            .orElseThrow(() -> new RuntimeException("FunctionalUnit not found"));
    }

    @Override
    @Transactional(readOnly = false)
    public FunctionalUnitTo addContacts(FunctionalUnitModel unit, List<Integer> contactIds) {
        Set<ContactModel> newContacts = contactRepository.findAllById(contactIds).stream()
        .filter(u -> !u.isDeleted())
        .collect(Collectors.toSet());

        defineContactsToAdd(unit, newContacts);

        return converter.convert(funcUnitRepo.save(unit), FunctionalUnitTo.class);
    }

    void defineContactsToAdd(FunctionalUnitModel unit, Set<ContactModel> newContacts) {
        FunctionalUnitModel parent = unit.getParent();

        if (parent == null || parent.getContacts().containsAll(newContacts)) {
            unit.setContacts(newContacts);
        } else {
            newContacts.removeAll(parent.getContacts());

            String contacts = Arrays.toString(newContacts.stream().map(u -> u.getEmail()).toArray());
            throw new RuntimeException(
                String.format("user(s) %s cannot be added because they don't belong to parent %s: %s", contacts, parent.getType(), parent.getName()));
        }
    }

    @Override
    @Transactional(readOnly = false)
    public FunctionalUnitTo removeContacts(Integer functionalUnitId, List<Integer> contactIds) {
        FunctionalUnitModel unit = verifyFuncUnitModel(functionalUnitId);
        Set<ContactModel> contactsToRemove = contactRepository.findAllById(contactIds).stream()
            .filter(u -> !u.isDeleted())
            .collect(toSet());

        return converter.convert(cascadeRemoveContacts(unit, contactsToRemove), FunctionalUnitTo.class);
    }

    FunctionalUnitModel cascadeRemoveContacts(FunctionalUnitModel fu, Set<ContactModel> contacts){
        fu.getContacts().removeAll(contacts);
        fu = funcUnitRepo.save(fu);
        fu.getChildren().forEach(c -> cascadeRemoveContacts(c, contacts));
        return fu;
    }

}