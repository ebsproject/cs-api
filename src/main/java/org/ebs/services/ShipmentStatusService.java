package org.ebs.services;

public interface ShipmentStatusService {
    
    public String changeShipmentStatus(Integer workflowId, Integer workflowInstanceId, String action);

}
