package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.NodeTypeTo;
import org.ebs.services.to.Input.NodeTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface NodeTypeService {

    public Optional<NodeTypeTo> findNodeType(int id);

    public Page<NodeTypeTo> findNodeTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public NodeTypeTo createNodeType(NodeTypeInput input);

    public NodeTypeTo modifyNodeType(NodeTypeInput input);

    public int deleteNodeType(int id);
    
}