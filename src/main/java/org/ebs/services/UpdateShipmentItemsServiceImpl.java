package org.ebs.services;

import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ebs.graphql.client.GraphQLClient;
import org.ebs.model.ServiceItemModel;
import org.ebs.model.ServiceModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.ServiceItemRepository;
import org.ebs.model.repos.ServiceRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.redirect.MicroIntegrationService;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class UpdateShipmentItemsServiceImpl implements UpdateShipmentItemsService {

    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;
    private final CoreBreedingService cbService;
    private final ServiceRepository shipmentRepository;
    private final ServiceItemRepository shipmentItemRepository;
    private final UserRepository userRepository;
    private final MessagingService messagingService;
    private final GraphQLClient graphQLClient;
    protected final TokenGenerator tokenGenerator;

    @Override
    public String updateSingleShipmentShipmentItemsCBGraphQL(int shipmentId) {

        if (cbGraphQlEndpoint == null || cbGraphQlEndpoint.isBlank())
            cbGraphQlEndpoint = "https://cbgraphqlapi-dev.ebsproject.org/graphql";
            cbGraphQlEndpoint = cbGraphQlEndpoint.replace("cbapi", "cbgraphqlapi");

        System.out.println("******************** Start Service " + shipmentId + " Items Update ********************");

        ServiceModel shipment = shipmentRepository.findById(shipmentId).orElse(null);

        if (shipment == null)
            return "404 Error shipment not found";

        System.out.println("Updating shipment " + shipmentId + " items...");

        int count = 0;

        for (ServiceItemModel item : shipmentItemRepository.findServiceItemsToUpdateByService(shipmentId)) {
            System.out.println("\tUpdating item " + item.getId());
            if (item.getSeedId() != null) {
                System.out.println("\t\tseedId present: " + item.getSeedId() + ", calling CB Graph for seed attributes...");
                String query = "query{findSeedAttributeList(filters:{col:\"seed.id\" mod:EQ val:\"" + item.getSeedId() + "\"}){content{id dataValue seed{id}}}}";
                System.out.println("\t\tBuilding graphql client");
                //SslContext context;
                try {
                    /* context = SslContextBuilder.forClient()
                        .trustManager(InsecureTrustManagerFactory.INSTANCE)
                        .build();
                    HttpClient httpClient = HttpClient.create().secure(t -> t.sslContext(context)); */
                    /* GraphQLClient graphQLClient = new GraphQLClient(
                    WebClient
                        .builder()
                        //.clientConnector(new ReactorClientHttpConnector(httpClient))
                        //.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " +  tokenGenerator.getToken())
                        .build(),
                        tokenGenerator); */
                    System.out.println("\t\tsending query to graphql server");
                    String queryData = graphQLClient.getQueryData(cbGraphQlEndpoint, query);
                    System.out.println("\t\t\tresponse received\n\t\t\t" + queryData);
                    Pattern attrPattern = Pattern.compile("\"dataValue\":\"(.*?)\"");
                    Matcher attrMatcher = attrPattern.matcher(queryData);
                    if (attrMatcher.find()) {
                        String attributeDataValue = attrMatcher.group(1);
                        System.out.println("\t\t\tFound dataValue attribute for seed, value is " + attributeDataValue);
                        item.setTestCode(attributeDataValue);
                        item.setStatus("liberated");
                        shipmentItemRepository.save(item);
                        count++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("******************** Finish Service " + shipmentId + " Items Update ********************");

        return "Finished updating shipment " + shipment.getId() + " items, items successfully updated: " + count
         + "\nNote: Some items might not have the RSHT value assigned yet in Core Breeding";
    }

    @Override
    public String updateListItemsCB(int listId) {

        String listMembersString = cbService.findListMembers(listId);
        
        ObjectMapper om = new ObjectMapper();

        JsonNode node;

        try {
            node = om.readTree(listMembersString);
            if (node.get("result") != null) {
                JsonNode listMemberData = node.get("result").get("data");
                if (listMemberData != null) {
                    if (listMemberData.isArray()) {
                        listMemberData = listMemberData.get(0);
                        if (listMemberData != null) {
                            if (listMemberData.get("entity").asText().equals("package")) {
                                JsonNode membersArray = listMemberData.get("members");
                                for (JsonNode member : membersArray) {
                                    if (member.hasNonNull("seedDbId") && member.hasNonNull("packageDbId")) {
                                        ServiceItemModel item = shipmentItemRepository
                                            .findBySeedIdAndPackageId(
                                                member.get("seedDbId").asInt(), member.get("packageDbId").asInt())
                                                    .stream().findFirst().orElse(null);
                                        if (item != null) {
                                            if (member.hasNonNull("rsht"))
                                                item.setTestCode(member.get("rsht").asText());
                                            if (member.hasNonNull("taxonomyName"))
                                                item.setTaxonomyName(member.get("taxonomyName").asText());
                                            if (member.hasNonNull("seedCode"))
                                                item.setSeedCode(member.get("seedCode").asText());
                                            if (member.hasNonNull("seedName"))
                                                item.setSeedName(member.get("seedName").asText());    
                                            if (member.hasNonNull("packageName"))
                                                item.setPackageName(member.get("packageName").asText());
                                            if (member.hasNonNull("label"))
                                                item.setPackageLabel(member.get("label").asText());
                                            if (member.hasNonNull("germplasmName"))
                                                item.setGermplasmId(member.get("germplasmDbId").asInt());
                                            shipmentItemRepository.save(item);
                                        }
                                    }
                                }
                            } else 
                                return "Associated list entity is not of type 'package'";
                        } else
                            return "No data available for list + " + listId;
                    } else
                        return "Incompatible response: expected array type in 'data' attribute";
                } else 
                    return "No data available for list + " + listId;
            } else 
                return "No data available for list + " + listId;
        } catch (Exception e) {
            e.printStackTrace();
            return "Internal Server Error";
        }

        return "Finished updating list " + listId + " items";

    }

    @Override
    public void updateSingleShipmentItemsAllData(int shipmentId, int userId) {

        System.out.println("Start - Update service (id " + shipmentId + ") all data");

        List<ServiceItemModel> items = shipmentItemRepository.findByServideIdAndPackageIsNotNull(shipmentId);

        if (items.isEmpty()) {
            System.out.println("Finish - Update service (id " + shipmentId + ") all data");
            sendSimpleNotification(userId, "No items found for shipment with id" + shipmentId);
            return;
        }

        MicroIntegrationService service = new MicroIntegrationService(WebClient
            .builder()
            .baseUrl(cbGraphQlEndpoint.replace("/graphql", "/"))
            .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
            .build());
        ObjectMapper om = new ObjectMapper();
        try {
            for (ServiceItemModel item : items) {
                String cbGraphResponse = service.doGet("/seed/" + item.getSeedId() + "/package/" + item.getPackageId() + "/item-attributes");
                JsonNode responseJson = om.readTree(cbGraphResponse);
                if (responseJson == null)
                    continue;
                JsonNode content = responseJson.get("content");
                if (content != null && content.isObject()) {
                    item.setSeedName(content.get("seedName").isNull() ? item.getSeedName() :content.get("seedName").asText());
                    item.setSeedCode(content.get("seedCode").isNull() ? item.getSeedCode() :content.get("seedCode").asText());
                    item.setTestCode(content.get("rsht").isNull() ? item.getTestCode() :content.get("rsht").asText());
                    item.setGermplasmId(content.get("germplasmId").asInt());
                    item.setGermplasmName(content.get("germplasmNormalizedName").isNull() ? item.getGermplasmName() :content.get("germplasmNormalizedName").asText());
                    item.setParentage(content.get("parentage").isNull() ? item.getParentage() :content.get("parentage").asText());
                    item.setDesignation(content.get("designation").isNull() ? item.getDesignation() :content.get("designation").asText());
                    item.setPackageId(content.get("packageId").asInt());
                    item.setPackageUnit(content.get("packageUnit").isNull() ? item.getPackageUnit() :content.get("packageUnit").asText());
                    item.setPackageName(content.get("packageCode").isNull() ? item.getPackageName() :content.get("packageCode").asText());
                    item.setPackageLabel(content.get("packageLabel").isNull() ? item.getPackageLabel() :content.get("packageLabel").asText());
                    item.setGeneticStock(content.get("geneticStock").isNull() ? item.getGeneticStock() :content.get("geneticStock").asText());
                    item.setOrigin(content.get("origin").isNull() ? item.getOrigin() :content.get("origin").asText());
                    item.setMtaStatus(content.get("mtaStatus").isNull() ? item.getMtaStatus() :content.get("mtaStatus").asText());
                    item.setMlsAncestors(content.get("mlsAncestors").isNull() ? item.getMlsAncestors() :content.get("mlsAncestors").asText());
                    item.setUse(content.get("use").isNull() ? item.getUse() :content.get("use").asText());
                    item.setTaxonomyName(content.get("taxonomyName").isNull() ? item.getTaxonomyName() :content.get("taxonomyName").asText());
                    shipmentItemRepository.save(item);
                }
            }
        } catch (Exception e) {
            System.out.println("Error updating service (id " + shipmentId + ") items");
            e.printStackTrace();
        }
        sendSimpleNotification(userId, "Service " + items.get(0).getService().getRequestCode() + " items update successfully completed");
        System.out.println("Finish - Update service (id " + shipmentId + ") "  + items.get(0).getService().getRequestCode() + " all data");
    }

    private void sendSimpleNotification(int userId, String description) {
        UserModel user = userRepository.findById(userId).orElse(null);
        if (user != null && user.getContact() != null) {
            UUID uuid = UUID.randomUUID();
            String notificationPayload = "{" + //
                            "\"recordId\" : 1," + //
                            "\"jobWorkflowId\" : 3," + //
                            "\"status\" : \"submitted\"," + //
                            "\"message\" : {\"message\": {\"status\": \"submitted\", \"title\": \"Items update complete\", \"description\": \"" + description + "\"}}," + //
                            "\"contacts\" : [" + user.getContact().getId() + "]," + //
                            "\"jobTrackId\": \"" + uuid + "\"" + //
                            "}";
            
            messagingService.createNotificationSimple(notificationPayload);
        }
    }
    
}
