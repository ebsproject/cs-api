///////////////////////////////////////////////////////////
//  AuditLogsService.java
//  Macromedia ActionScript Implementation of the Interface AuditLogsService
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:11:48 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
import org.ebs.services.to.AuditLogsTo;
import org.ebs.services.to.Input.AuditLogsInput;
import org.ebs.services.to.InstanceTo;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:11:48 AM
 */
public interface AuditLogsService {

	/**
	 *
	 * @param AuditLogs
	 */
	public AuditLogsTo createAuditLogs(AuditLogsInput AuditLogs);

	/**
	 *
	 * @param auditLogsId
	 */
	public int deleteAuditLogs(int auditLogsId);

	/**
	 *
	 * @param auditLogsId
	 */
	public Optional<AuditLogsTo> findAuditLogs(int auditLogsId);

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	public Page<AuditLogsTo> findAuditLogss(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

	/**
	 *
	 * @param auditlogsId
	 */
	public Optional<InstanceTo> findInstance(int auditlogsId);

	/**
	 *
	 * @param auditLogs
	 */
	public AuditLogsTo modifyAuditLogs(AuditLogsInput auditLogs);

}