package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.StatusTypeModel;
import org.ebs.model.TenantModel;
import org.ebs.model.WorkflowModel;
import org.ebs.model.repos.StatusTypeRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.WorkflowRepository;
import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.StatusTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class StatusTypeServiceImpl implements StatusTypeService {
    
    private final ConversionService converter;
    private final StatusTypeRepository statusTypeRepository;
    private final WorkflowRepository workflowRepository;
    private final TenantRepository tenantRepository;

    @Override
    public Optional<StatusTypeTo> findStatusType(int id) {
        if (id < 1)
            return Optional.empty();

        return statusTypeRepository.findById(id).map(r -> converter.convert(r, StatusTypeTo.class));
    }

    @Override
    public Page<StatusTypeTo> findStatusTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters,
            boolean disjunctionFilters) {
            return statusTypeRepository
                .findByCriteria(StatusTypeModel.class, filters, sort, page, disjunctionFilters)
                .map(r -> converter.convert(r, StatusTypeTo.class));
    }

    @Override
    @Transactional(readOnly = false)
    public StatusTypeTo createStatusType(StatusTypeInput input) {
        
        input.setId(0);

        StatusTypeModel model = converter.convert(input, StatusTypeModel.class);

        setStatusTypeDependencies(model, input);

        model = statusTypeRepository.save(model);

        return converter.convert(model, StatusTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public StatusTypeTo modifyStatusType(StatusTypeInput input) {
        
        StatusTypeModel target = verifyStatusTypeModel(input.getId());

        StatusTypeModel source = converter.convert(input, StatusTypeModel.class);

        Utils.copyNotNulls(source, target);

        setStatusTypeDependencies(target, input);

        return converter.convert(statusTypeRepository.save(target), StatusTypeTo.class);

    }

    @Override
    @Transactional(readOnly = false)
    public int deleteStatusType(int id) {

        StatusTypeModel model = verifyStatusTypeModel(id);

        model.setDeleted(true);

        statusTypeRepository.save(model);

        return id;

    }

    @Override
    public WorkflowTo findWorkflow(int statusTypeId) {
        
        StatusTypeModel model = verifyStatusTypeModel(statusTypeId);

        if (model.getWorkflow() == null)
            return null;
        
        return converter.convert(model.getWorkflow(), WorkflowTo.class);

    }

    @Override
    public TenantTo findTenant(int statusTypeId) {
        
        StatusTypeModel model = verifyStatusTypeModel(statusTypeId);

        if (model.getTenant() == null)
            return null;
        
        return converter.convert(model.getTenant(), TenantTo.class);

    }

    StatusTypeModel verifyStatusTypeModel(int statusTypeId) {
        return statusTypeRepository.findById(statusTypeId)
            .orElseThrow(() -> new RuntimeException("StatusType id " + statusTypeId + " not found"));
    }

    void setStatusTypeDependencies(StatusTypeModel model, StatusTypeInput input) {
        setStatusTypeWorkflow(model, input.getWorkflowId());
        setStatusTypeTenant(model, input.getTenantId());
    }

    void setStatusTypeWorkflow(StatusTypeModel model, Integer workflowId) {
        if (workflowId != null && workflowId > 0) {
            WorkflowModel workflow = workflowRepository.findById(workflowId)
                .orElseThrow(() -> new RuntimeException("Workflow id " + workflowId + " not found"));
            model.setWorkflow(workflow);
        }
    } 

    void setStatusTypeTenant(StatusTypeModel model, int tenantId) {
        if (tenantId > 0) {
            TenantModel tenant = tenantRepository.findById(tenantId)
                .orElseThrow(() -> new RuntimeException("Tenant id " + tenantId + " not found"));
            model.setTenant(tenant);
        }
    } 

}
