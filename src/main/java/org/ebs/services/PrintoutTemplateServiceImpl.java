package org.ebs.services;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.ContactModel;
import org.ebs.model.PrintoutTemplateModel;
import org.ebs.model.ProductModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.PrintoutTemplateRepository;
import org.ebs.model.repos.ProductRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.to.PrintoutTemplate;
import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.UserTo;
import org.ebs.services.to.Input.PrintoutTemplateInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service @Transactional(readOnly = true)
@RequiredArgsConstructor
class PrintoutTemplateServiceImpl implements PrintoutTemplateService {

	private final PrintoutTemplateRepository printoutTemplateRepository;
	private final ConversionService converter;
	private final ContactRepository contactRepository;
	private final ProductRepository productRepository;
	private final TenantRepository tenantRepository;
	private final UserRepository userRepository;

	@Override
	public Optional<PrintoutTemplate> findPrintoutTemplate(int printoutTemplateId) {
		return printoutTemplateRepository.findByIdAndDeletedIsFalse(printoutTemplateId)
			.map(p -> converter.convert(p, PrintoutTemplate.class));
	}

	@Override
	public Page<PrintoutTemplate> findPrintoutTemplates(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters) {

		if (filters != null) {
			filters.forEach(f -> {
				if (f.getCol().equals("programs.code"))
					f.setCol("programs.institution.commonName");
				else if (f.getCol().equals("programs.name"))
					f.setCol("programs.institution.legalName");
			});
		}

        return printoutTemplateRepository.findByCriteria(PrintoutTemplateModel.class, filters, sort, page, disjunctionFilters)
                .map(e -> converter.convert(e, PrintoutTemplate.class));
	}

	@Override
	@Transactional(readOnly= false)
	public PrintoutTemplate createPrintoutTemplate(PrintoutTemplateInput printoutTemplate) {
		printoutTemplate.setId(0);
		PrintoutTemplateModel model = converter.convert(printoutTemplate, PrintoutTemplateModel.class);

		model.setName(model.getName().trim().toLowerCase());

		model = printoutTemplateRepository.save(model);

		setPrintoutTemplateTenants(model, printoutTemplate);

		return converter.convert(model, PrintoutTemplate.class);
	}

	void setPrintoutTemplateTenants(PrintoutTemplateModel model, PrintoutTemplateInput input) {

		if (input.getTenantIds() == null)
			return;

		input.getTenantIds().forEach(tenantId -> {
			TenantModel tenant = tenantRepository.findByIdAndDeletedIsFalse(tenantId)
				.orElseThrow(() -> new RuntimeException("Tenant not found"));
			tenant.setPrintoutTemplates(new HashSet<>(Arrays.asList(model)));
			tenantRepository.save(tenant);
		});
		
		model.getTenants().forEach(tenant -> {
            if (!input.getTenantIds().contains(tenant.getId())) {
                tenant.getPrintoutTemplates().remove(model);
                tenantRepository.save(tenant);
            }
        });
	}

	@Override
	@Transactional(readOnly= false)
	public PrintoutTemplate modifyPrintoutTemplate(PrintoutTemplateInput printoutTemplate) {
		PrintoutTemplateModel existingModel= verifyPrintoutTemplate(printoutTemplate.getId());

		Utils.copyNotNulls(printoutTemplate, existingModel);

        setPrintoutTemplateTenants(existingModel, printoutTemplate);

		return converter.convert(printoutTemplateRepository.save(existingModel), PrintoutTemplate.class);
	}

	/**
	 * Verifies a Printout Template exists before trying to do an action over it
	 */
	PrintoutTemplateModel verifyPrintoutTemplate(int printoutTemplateId) {
		return printoutTemplateRepository.findByIdAndDeletedIsFalse(printoutTemplateId)
		.orElseThrow(() -> new RuntimeException("PrintoutTemplate not found"));
	}

	@Override
	@Transactional(readOnly= false)
	public int deletePrintoutTemplate(int printoutTemplateId) {
		PrintoutTemplateModel existingModel= verifyPrintoutTemplate(printoutTemplateId);
		existingModel.setDeleted(true);
		printoutTemplateRepository.save(existingModel);
		return printoutTemplateId;
	}

	@Override
	@Transactional(readOnly= false)
	public PrintoutTemplate addPrintoutTemplateToPrograms(int printoutTemplateId, Set<Integer> programIds) {
		PrintoutTemplateModel model = verifyPrintoutTemplate(printoutTemplateId);
		Set<Integer> currentIds = model.getPrograms().stream().map(p -> p.getId()).collect(Collectors.toSet());
		programIds.removeAll(currentIds);

		List<ContactModel> programs = contactRepository.findAllById(programIds);

		programs.removeIf(p -> p.getInstitution() == null || p.getInstitution().getUnitType() == null
			 || !p.getInstitution().getUnitType().getName().equals("Program"));

		model.getPrograms().addAll(programs);

		model.setUpdatedOn(new Date());

		return converter.convert(printoutTemplateRepository.save(model), PrintoutTemplate.class);
	}

	@Override
	@Transactional(readOnly= false)
	public PrintoutTemplate removePrintoutTemplateFromPrograms(int printoutTemplateId, Set<Integer> programIds) {
		PrintoutTemplateModel model = verifyPrintoutTemplate(printoutTemplateId);
		List<ContactModel> programsToRemove = contactRepository.findAllById(programIds);
		model.getPrograms().removeAll(programsToRemove);
		model.setUpdatedOn(new Date());
		return converter.convert(printoutTemplateRepository.save(model), PrintoutTemplate.class);
	}

	@Override
	@Transactional(readOnly= false)
	public PrintoutTemplate addPrintoutTemplateToProducts(int printoutTemplateId, Set<Integer> productIds) {
		PrintoutTemplateModel model = verifyPrintoutTemplate(printoutTemplateId);
		Set<Integer> currentIds = model.getProducts().stream().map(p -> p.getId()).collect(Collectors.toSet());
		productIds.removeAll(currentIds);
		model.getProducts().addAll(productRepository.findAllById(productIds));
		model.setUpdatedOn(new Date());
		return converter.convert(printoutTemplateRepository.save(model), PrintoutTemplate.class);
	}

	@Override
	@Transactional(readOnly= false)
	public PrintoutTemplate removePrintoutTemplateFromProducts(int printoutTemplateId, Set<Integer> productIds) {
		PrintoutTemplateModel model = verifyPrintoutTemplate(printoutTemplateId);
		List<ProductModel> productsToRemove = productRepository.findAllById(productIds);
		model.getProducts().removeAll(productsToRemove);
		model.setUpdatedOn(new Date());
		return converter.convert(printoutTemplateRepository.save(model), PrintoutTemplate.class);
	}

	@Override
	public List<ProgramTo> findPrograms(int printoutTemplateId) {

		PrintoutTemplateModel model = verifyPrintoutTemplate(printoutTemplateId);

		return model.getPrograms()
			.stream()
			.map(r -> converter.convert(r, ProgramTo.class))
			.collect(Collectors.toList());

	}

	@Override
    public UserTo findCreatedBy(int printoutTemplateId) {
        PrintoutTemplateModel po = verifyPrintoutTemplate(printoutTemplateId);
        return userRepository.findById(po.getCreatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }

    @Override
    public UserTo findUpdatedBy(int printoutTemplateId) {
        PrintoutTemplateModel po = verifyPrintoutTemplate(printoutTemplateId);
        if (po.getUpdatedBy() == null)
            return null;
        return userRepository.findById(po.getUpdatedBy())
            .map(r -> converter.convert(r, UserTo.class)).orElse(null);
    }
}