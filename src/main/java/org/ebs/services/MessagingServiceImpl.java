package org.ebs.services;

import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.ebs.model.EmailTemplateModel;
// import org.ebs.model.HierarchyTreeModel;
import org.ebs.model.JobWorkflowModel;
import org.ebs.model.TranslationModel;
import org.ebs.model.UserModel;
import org.ebs.model.repos.EmailTemplateRepository;
// import org.ebs.model.repos.HierarchyTreeRepository;
import org.ebs.model.repos.JobWorkflowRepository;
import org.ebs.model.repos.TranslationRepository;
import org.ebs.model.repos.UserRepository;
import org.ebs.services.redirect.MicroIntegrationService;
import org.ebs.services.to.JobLogTo;
import org.ebs.services.to.Input.JobLogInput;
import org.ebs.util.MultipartByteArrayResource;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Sinks.Many;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class MessagingServiceImpl implements MessagingService {

    private final UserRepository userRepository;
    private final JobWorkflowRepository jobWorkflowRepository;
    private final TranslationRepository translationRepository;
    private final JobLogService jobLogService;
    private final Many<JobLogTo> jobLogSink;
    private final EmailTemplateRepository emailTemplateRepository;
    private final RestTemplate restTemplate;
    protected final TokenGenerator tokenGenerator;
    @Value("${ebs.email.endpoint}")
    private String smtpServer;
    
    @Override
    @Transactional(readOnly = false)
    public String createJobLogFromPayload(String payload) {

        JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

        try {

            Set<Integer> contactIds = new HashSet<>();
            Integer submitterUserId = null;

            JsonReader reader = Json.createReader(new StringReader(payload));
            JsonObject json = reader.readObject();
    
            JsonObject sourceJsonObject = json.getJsonObject("source");
            JsonObject messageJsonObject = json.getJsonObject("message");
            JsonObject targetObject = json.getJsonObject("target");
    
            JobLogInput jobLogInput = new JobLogInput();
    
            if (json.get("tenant_id") != null && json.get("tenant_id").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0)
                    jobLogInput.setTenantId(Integer.parseInt(json.get("tenant_id").toString()));
            else 
                return jsonBuilder.add("error", "tenant_id is missing or has an incorrect data type").build().toString();

            if (sourceJsonObject != null) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode message = mapper.readTree(payload);
                JsonValue submitterId = sourceJsonObject.get("submitter_id");
                if (submitterId != null && submitterId.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                    UserModel submitter = verifyUserModel(Integer.parseInt(submitterId.toString()));
                    if (submitter == null)
                        return jsonBuilder.add("error", "submitter id " + submitterId + " not found").build().toString();
                    ((ObjectNode) message.path("target")).put("contacts", contactIds.toString());
                    submitterUserId = submitter.getId();
                } else {
                    return jsonBuilder.add("error", "submitter_id is missing or has an incorrect data type").build().toString();
                }
                jobLogInput.setMessage(message);

                JsonValue jobTrackId = json.getJsonObject("source").get("job_track_id");
                if (jobTrackId != null && jobTrackId.getValueType().compareTo(JsonValue.ValueType.STRING) == 0)
                    jobLogInput.setJobTrackId(UUID.fromString(json.getJsonObject("source").getString("job_track_id")));
                else
                    return jsonBuilder.add("error", "job_track_id is missing or has an incorrect data type").build().toString();
            } else {
                return jsonBuilder.add("error", "source object is missing").build().toString();
            }

            if (messageJsonObject != null) {
                JsonValue jobWorkflowId = messageJsonObject.get("job_workflow_id");
                JsonValue translationId = messageJsonObject.get("translation_id");
                JsonValue status = messageJsonObject.get("status");

                if (jobWorkflowId != null && jobWorkflowId.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                        JobWorkflowModel jobWorkflow = jobWorkflowRepository.findById(Integer.parseInt(jobWorkflowId.toString())).orElse(null);
                        if (jobWorkflow == null)
                            return jsonBuilder.add("error", "job workflow id " + jobWorkflowId + " not found").build().toString();
                        jobLogInput.setJobWorkflowId(Integer.parseInt(jobWorkflowId.toString()));
                } else
                    return jsonBuilder.add("error", "job_workflow_id is missing or has incorrect data type").build().toString();    
            
                if (translationId != null && translationId.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                    TranslationModel translation = translationRepository.findById(Integer.parseInt(translationId.toString())).orElse(null);
                    if (translation == null)
                        return jsonBuilder.add("error", "translation id " + translationId + " not found").build().toString();
                    jobLogInput.setTranslationId(Integer.parseInt(translationId.toString()));
                } else
                    return jsonBuilder.add("error", "translation_id is missing or has incorrect data type").build().toString();    
                
                if (status != null && status.getValueType().compareTo(JsonValue.ValueType.STRING) == 0)
                    jobLogInput.setStatus(status.toString().substring(1, status.toString().length() - 1));
                else
                    return jsonBuilder.add("error", "status is missing or has incorrect data type").build().toString();    
                
            } else {
                return jsonBuilder.add("error", "message object is missing").build().toString();
            }

            if (targetObject != null) {
                JsonValue usersArray = targetObject.get("users");
                JsonValue contactsArray = targetObject.get("contacts");
                if (usersArray != null) {
                    if (usersArray.getValueType().compareTo(JsonValue.ValueType.OBJECT) == 0) {
                        // Received nested users/users array
                        if (json.getJsonObject("target").getJsonObject("users").getJsonArray("users").getValueType().compareTo(JsonValue.ValueType.ARRAY) == 0)
                            contactIds = getContactIdsToNotify(getSetOfUserIds(json.getJsonObject("target").getJsonObject("users").getJsonArray("users"), submitterUserId));
                    } else if (usersArray.getValueType().compareTo(JsonValue.ValueType.ARRAY) == 0) {
                        // Received users array
                        contactIds = getContactIdsToNotify(getSetOfUserIds(json.getJsonObject("target").getJsonArray("users"), submitterUserId));
                    } else if (usersArray.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                        //Received an integer
                        UserModel submitter = verifyUserModel(Integer.parseInt(json.getJsonObject("target").get("users").toString()));
                        if (submitter != null)
                            contactIds.add(submitter.getContact().getId());
                    } else {
                        return jsonBuilder.add("error", "users array missing or has incorrect data type").build().toString();
                    }
                } else {
                    return jsonBuilder.add("error", "users array missing or has incorrect data type").build().toString();
                }
                if (contactsArray != null) {
                    if (contactsArray.getValueType().compareTo(JsonValue.ValueType.OBJECT) == 0) {
                        if (json.getJsonObject("target").getJsonObject("contacts").getJsonArray("contacts").getValueType().compareTo(JsonValue.ValueType.ARRAY) == 0) {
                            // Received contacts/contacts array
                            JsonArray contactsJsonArray = json.getJsonObject("target").getJsonObject("contacts").getJsonArray("contacts");
                            for (JsonValue contact : contactsJsonArray) {
                                if (contact != null) {
                                    if (contact.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                                        contactIds.add(Integer.parseInt(contact.toString()));
                                    }
                                }
                            }
                        }
                    } else if (contactsArray.getValueType().compareTo(JsonValue.ValueType.ARRAY) == 0) {
                        // Received contacts array
                        JsonArray contactsJsonArray = json.getJsonObject("target").getJsonArray("contacts");
                            for (JsonValue contact : contactsJsonArray) {
                                if (contact != null) {
                                    if (contact.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                                        contactIds.add(Integer.parseInt(contact.toString()));
                                    }
                                }
                            }
                    } else {
                        return jsonBuilder.add("error", "contacts array has incorrect data type").build().toString();
                    }
                }
            } else
                return jsonBuilder.add("error", "target object is missing").build().toString();
    
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //formatter.setTimeZone(TimeZone.getTimeZone("UTC-5:00"));
            
            Date currentTime = new Date();
    
            jobLogInput.setStartTime(formatter.format(currentTime));
    
            jobLogInput.setContactIds(contactIds);
    
            //jobLogInput.setJobTrackId(UUID.randomUUID());
            
            JobLogTo createdJobLog = jobLogService.createJobLog(jobLogInput);
    
            if (createdJobLog != null) {
                jobLogSink.tryEmitNext(createdJobLog);
                return jsonBuilder.add("success", "job created with track id " + createdJobLog.getJobTrackId()).build().toString();
            }
            else
                return jsonBuilder.add("error", "could not create job from given payload").build().toString();
        } catch (Exception e) {
            return jsonBuilder.add("error", e.getMessage()).build().toString();
        }

    }

    private Set<Integer> getSetOfUserIds(JsonArray usersJsonArray, Integer submitterId) {
        Set<Integer> userIds = new HashSet<>();
        usersJsonArray.stream().forEach(user -> {
            if (user != null) {
                if (user.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                    userIds.add(Integer.parseInt(user.toString()));
                }
            }
        });
/*         if (submitterId != null)
            userIds.add(submitterId); */
        return userIds;
    }

    public Set<Integer> getContactIdsToNotify(Set<Integer> userIds) {

        Set<Integer> contactIds = new HashSet<>();
        userIds.stream().forEach(userId -> {
            UserModel user = userRepository.findById(userId).orElse(null);
            if (user != null)
                contactIds.add(user.getContact().getId());
        });

        return contactIds;
    }

    UserModel verifyUserModel(int userId) {
        return userRepository.findByIdAndDeletedIsFalse(userId)
                .orElse(null);
    }

    @Override
    @Transactional(readOnly = false)
    public String createNotificationSimple(String payload) {
        
        JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();

        try {

            JsonReader reader = Json.createReader(new StringReader(payload));
            JsonObject json = reader.readObject();
    
            JobLogInput jobLogInput = new JobLogInput();

            JsonValue jobTrackIdJson = json.get("jobTrackId");
            String jobTrackIdString = new String();

            if (jobTrackIdJson != null && jobTrackIdJson.getValueType().compareTo(JsonValue.ValueType.STRING) == 0) {
                jobTrackIdString = jobTrackIdJson.toString().substring(1, jobTrackIdJson.toString().length() - 1);

                if (jobTrackIdString.length() > 36)
                    return jsonBuilder.add("error", "jobTrackId has an incorrect value").build().toString();
                jobLogInput.setJobTrackId(UUID.fromString(jobTrackIdString));
            } else
                jobLogInput.setJobTrackId(UUID.randomUUID());
    
            if (json.get("recordId") != null && json.get("recordId").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0)
                    jobLogInput.setRecordId(Integer.parseInt(json.get("recordId").toString()));
            else 
                return jsonBuilder.add("error", "recordId is missing or has an incorrect data type").build().toString();
            if (json.get("jobWorkflowId") != null && json.get("jobWorkflowId").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0)
                    jobLogInput.setJobWorkflowId(Integer.parseInt(json.get("jobWorkflowId").toString()));
            else 
                return jsonBuilder.add("error", "jobWorkflowId is missing or has an incorrect data type").build().toString(); 
            if (json.get("status") != null && json.get("status").getValueType().compareTo(JsonValue.ValueType.STRING) == 0)
                jobLogInput.setStatus(json.get("status").toString().substring(1, json.get("status").toString().length() - 1));   
            if (json.get("message") != null && json.get("message").getValueType().compareTo(JsonValue.ValueType.OBJECT) == 0) {
                ObjectMapper mapper = new ObjectMapper();
                jobLogInput.setMessage(mapper.readTree(json.get("message").toString()));
                if ((jobLogInput.getStatus() == null || jobLogInput.getStatus().isEmpty()) && json.getJsonObject("message").get("status") != null
                    && json.getJsonObject("message").get("status").getValueType().compareTo(JsonValue.ValueType.STRING) == 0) {
                        jobLogInput.setStatus(json.getJsonObject("message").get("status")
                            .toString().substring(1, json.getJsonObject("message").get("status").toString().length() - 1));
                    }
            } else
                return jsonBuilder.add("error", "message is missing or has incorrect data type").build().toString();    
            if (json.get("contacts") != null && json.get("contacts").getValueType().compareTo(JsonValue.ValueType.ARRAY) == 0) {
                JsonArray contactsJsonArray = json.getJsonArray("contacts");
                Set<Integer> contactIds = new HashSet<>();
                    for (JsonValue contact : contactsJsonArray) {
                        if (contact != null) {
                            if (contact.getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0) {
                                contactIds.add(Integer.parseInt(contact.toString()));
                            }
                        }
                    }
                jobLogInput.setContactIds(contactIds);
            } else {
                return jsonBuilder.add("error", "contacts array is missing or has incorrect data type").build().toString();
            }
    
            jobLogInput.setStartTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

            jobLogInput.setTenantId(1);

            JobLogTo createdJobLog = jobLogService.createJobLog(jobLogInput);
    
            if (createdJobLog != null) {
                jobLogSink.tryEmitNext(createdJobLog);
                return jsonBuilder.add("success", "job created with track id " + createdJobLog.getJobTrackId()).build().toString();
            }
            else
                return jsonBuilder.add("error", "could not create job from given payload").build().toString();
        } catch (Exception e) {
            return jsonBuilder.add("error", e.getMessage()).build().toString();
        }
    }

    @Override
    public String sendEmail(String payload, List<MultipartFile> fileAttachment) throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        JsonNode jsonPayload = mapper.readTree(payload);
        JsonNode templateObject = jsonPayload.get("template");

        String to = jsonPayload.get("to") != null ? 
            jsonPayload.get("to").asText() : null;
        String from = jsonPayload.get("from") != null ? 
            jsonPayload.get("from").asText() : null;

        if (to == null || from == null)
            return "Missing to parameter";

        LinkedMultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        FormHttpMessageConverter converter = new FormHttpMessageConverter();
        restTemplate.getMessageConverters().add(converter);
        if (templateObject != null && templateObject.isObject()) {
            try {
                ObjectMapper om = new ObjectMapper();
                JsonNode templateIdNode = templateObject.get("id");
                JsonNode templateDataNode = templateObject.get("data");
                int templateId = 0;
                String templateData = new String();
                if (templateIdNode != null && templateIdNode.isInt())
                    templateId = Integer.parseInt(templateIdNode.asText());
                else
                    return "template id missing or has incorrect data type";
                EmailTemplateModel template = emailTemplateRepository.findById(templateId)
                    .orElse(null);
                if (template == null)
                    return "template id not found";
                if (templateDataNode != null)
                    templateData = om.writeValueAsString(templateDataNode);
                Path fileDir = Paths.get("/home/wso2carbon/files");
                if (!Files.exists(fileDir)) {
                    Files.createDirectories(fileDir);
                }
                String emailContent = template.getTemplate();
                Path file = fileDir.resolve("email_file.txt");
                Files.writeString(file, emailContent);
                params.add("File", new FileSystemResource(file));
                params.add("Data", templateData);
                params.add("to", to);
                params.add("from", from);
                params.add("subject", template.getSubject());
            } catch (Exception e) {
                e.printStackTrace();
                return "could not send email";
            }
        } else {
            String subject = jsonPayload.get("subject") != null ? 
                jsonPayload.get("subject").asText() : null;
            String emailBody = jsonPayload.get("body") != null ? 
                jsonPayload.get("body").asText() : "";
            if (subject == null)
                return "Missing subject parameter";
            params.add("to", to);
            params.add("from", from);
            params.add("subject", subject);
            params.add("text", emailBody);
        }
        if(fileAttachment!=null && !fileAttachment.isEmpty()) {
            for (MultipartFile file : fileAttachment) {
                if (file != null) {
                    String fileName = file.getOriginalFilename().trim();
                    if (file.getBytes().length > 0) {
                        MultipartByteArrayResource resource = new MultipartByteArrayResource(file.getBytes());
                        resource.setFilename(file.getOriginalFilename());
                        params.add("attachments", resource);
                    }
                }
            }
        }
        String authString = "Bearer " + tokenGenerator.getToken();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("Authorization", authString);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity =
                new HttpEntity<>(params, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(smtpServer,
                HttpMethod.POST,
                requestEntity,
                String.class);

        return responseEntity.getBody();
    }

    @Override
    public String printReport(String payload, String authorization, int userId) {

        try {

            String printServer = smtpServer.replace("/email/send", "/report/print");
            JsonReader reader = Json.createReader(new StringReader(payload));
            JsonObject json = reader.readObject();
            int contactId = 0;

            if (json.get("name") == null ||
                json.get("name").getValueType().compareTo(JsonValue.ValueType.STRING) != 0)
                    return "400 - name parameter missing or has incorrect data type";
            
            if (json.get("printerName") == null ||
                json.get("printerName").getValueType().compareTo(JsonValue.ValueType.STRING) != 0)
                    return "400 - printerName parameter missing or has incorrect data type";
            
            if (json.get("contactId") != null) {
                if (json.get("contactId").getValueType().compareTo(JsonValue.ValueType.NUMBER) == 0)
                    contactId = json.getInt("contactId");
                else 
                    return "400 - contactId has incorrect data type";
            } else {
                UserModel user = userRepository.findById(userId).orElse(null);
                if (user != null && user.getContact() != null)
                    contactId = user.getContact().getId();
            }

            JsonObjectBuilder builder = Json.createObjectBuilder();
            JsonArrayBuilder paramsArray = Json.createArrayBuilder();

            if (json.get("params") != null 
                && json.get("params").getValueType().compareTo(JsonValue.ValueType.ARRAY) == 0) {
                    for (JsonValue param : json.getJsonArray("params")) {
                        JsonObjectBuilder formattedParam = Json.createObjectBuilder();
                        if (param.getValueType().compareTo(JsonValue.ValueType.OBJECT) == 0) {
                            if (param.asJsonObject().get("value") == null
                                || param.asJsonObject().get("value").getValueType().compareTo(JsonValue.ValueType.STRING) != 0) {
                                    return "400 - params array has incorrect format";
                            }
                            formattedParam.add("value", param.asJsonObject().getString("value") + "&");
                            paramsArray.add(formattedParam);
                        } else {
                            return "400 - params array has incorrect format";
                        }
                    }
            } else {
                return "400 - params parameter missing or has incorrect data type";
            }

            builder.add("name", json.getString("name") + "&");
            builder.add("printerName", json.getString("printerName") + "&");
            builder.add("params", paramsArray);
            builder.add("printServer", printServer);
            builder.add("contactId", contactId);

            MicroIntegrationService service = new MicroIntegrationService(WebClient
                .builder()
                .baseUrl("http://localhost:8290")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, authorization)
                .build());
            String miResponse = service.redirectRequestToMI("/printout/report", builder.build().toString());
            reader = Json.createReader(new StringReader(miResponse));
            json = reader.readObject();
            if (json.get("success") == null ||
                json.get("success").getValueType().compareTo(JsonValue.ValueType.STRING) != 0)
                    return "500 - Internal Server Error (MI)";
            
            return json.getString("success");

        } catch (Exception e) {
            e.printStackTrace();
            return "500 - Internal Server Error";
        }

    }

    @Override
    public String queueGigwaLoader(String payload, String authorization) {
        try {

            JsonReader reader = Json.createReader(new StringReader(payload));
            JsonObject json = reader.readObject();

            if (json.get("id") == null ||
                json.get("id").getValueType().compareTo(JsonValue.ValueType.STRING) != 0)
                    return "400 - id missing or has incorrect data type";
            
            JsonObjectBuilder builder = Json.createObjectBuilder();

            builder.add("id", json.getString("id"));

            MicroIntegrationService service = new MicroIntegrationService(WebClient
                .builder()
                .baseUrl("http://localhost:8290")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, authorization)
                .build());
            String miResponse = service.redirectRequestToMI("/queue/gigwa", builder.build().toString());
            reader = Json.createReader(new StringReader(miResponse));
            json = reader.readObject();
            if (json.get("message") == null ||
                json.get("message").getValueType().compareTo(JsonValue.ValueType.STRING) != 0)
                    return "500 - Internal Server Error (MI)";
            
            return json.getString("message");

        } catch (Exception e) {
            e.printStackTrace();
            return "500 - Internal Server Error";
        }
    }
    
}