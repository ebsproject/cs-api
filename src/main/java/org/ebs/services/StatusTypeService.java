package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.StatusTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.WorkflowTo;
import org.ebs.services.to.Input.StatusTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface StatusTypeService {

    public Optional<StatusTypeTo> findStatusType(int id);

    public Page<StatusTypeTo> findStatusTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public StatusTypeTo createStatusType(StatusTypeInput input);

    public StatusTypeTo modifyStatusType(StatusTypeInput input);

    public int deleteStatusType(int id);

    public WorkflowTo findWorkflow(int statusTypeId);

    public TenantTo findTenant(int statusTypeId);
    
}