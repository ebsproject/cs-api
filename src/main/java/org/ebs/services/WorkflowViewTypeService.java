package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.WorkflowViewTypeTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.WorkflowViewTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;
public interface WorkflowViewTypeService {

    public Optional<WorkflowViewTypeTo> findWorkflowViewType(int id);

    public Page<WorkflowViewTypeTo> findWorkflowViewTypes(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters);

    public WorkflowViewTypeTo createWorkflowViewType(WorkflowViewTypeInput input);

    public WorkflowViewTypeTo modifyWorkflowViewType(WorkflowViewTypeInput input);

    public int deleteWorkflowViewType(int id);

    public TenantTo findTenant(int workflowViewTypeId);
    
}