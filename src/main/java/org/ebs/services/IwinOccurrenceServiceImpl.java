package org.ebs.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.ebs.model.AddressModel;
import org.ebs.model.ContactModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.services.redirect.MicroIntegrationService;
import org.ebs.services.to.IWINOccurrenceTo;
import org.ebs.util.Connection;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class IwinOccurrenceServiceImpl implements IwinOccurrenceService {

    @Value("${ebs.cb.graphql.endpoint}")
    private String cbGraphQlEndpoint;
    protected final TokenGenerator tokenGenerator;
    private final ContactRepository contactRepository;

    @Override
    public Page<IWINOccurrenceTo> getIwinOccurrenceByCoopkey(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters) {
        if (filters == null || filters.isEmpty())
            throw new RuntimeException("coopkey or occurrenceDbIds must be provided");
        String coopkey = filters
                .stream()
                .filter(f -> f.getCol().toLowerCase().equals("coopkey"))
                .findFirst()
                .map(f -> f.getVal())
                .orElse(null);
        List<Integer> occurrenceDbIdsFilters = filters
                .stream().filter(f -> f.getCol().equals("occurrenceDbId"))
                .map(f -> Integer.parseInt(f.getVal()))
                .collect(Collectors.toList());
        if (coopkey == null && occurrenceDbIdsFilters.isEmpty())
            throw new RuntimeException("coopkey or occurrenceDbIds must be provided");
        List<IWINOccurrenceTo> list = new ArrayList<>();
        if (coopkey != null) {
            List<ContactModel> contactWithCoopkey = contactRepository.findByContactInfosContactInfoTypeNameInIgnoreCaseAndContactInfosValueAndContactInfosDeletedIsFalseAndDeletedIsFalse(Arrays.asList("COOPKEY"), coopkey);
            List<ContactModel> sendViaCoopkeyContacts = contactRepository.findByContactInfosContactInfoTypeNameInIgnoreCaseAndContactInfosValueAndContactInfosDeletedIsFalseAndDeletedIsFalse(Arrays.asList("Send Via Coopkey"), coopkey);
            if (contactWithCoopkey.isEmpty() && sendViaCoopkeyContacts.isEmpty() && occurrenceDbIdsFilters.isEmpty())
                return new Connection<IWINOccurrenceTo>(new ArrayList<IWINOccurrenceTo>(), createPage(page), 0);
            if (!contactWithCoopkey.isEmpty()) {
                List<IWINOccurrenceTo> byCoopkeyResult = retrieveOccurrenceData(coopkey, filters, contactWithCoopkey.get(0));   
                sendViaCoopkeyContacts.forEach(c -> {
                    String additionalCoopkey = c.getContactInfos()
                        .stream()
                        .filter(ci -> ci.getContactInfoType().getName().toLowerCase().equals("coopkey") && !ci.isDeleted())
                        .findAny()
                        .map(ci -> ci.getValue())
                        .orElse(null);
                    byCoopkeyResult.addAll(retrieveOccurrenceData(additionalCoopkey, filters, c));
                });
                list.addAll(byCoopkeyResult);
            }
        }
        if (!occurrenceDbIdsFilters.isEmpty()) {
            String occurrenceDbIdsString = occurrenceDbIdsFilters.toString().replace("[", "(").replace("]", ")");
            FilterInput occurrenceDbIdsFilter = new FilterInput("occurrenceDbIds", occurrenceDbIdsString, FilterMod.EQ);
            filters.add(occurrenceDbIdsFilter);
            List<IWINOccurrenceTo> byOccurrenceDbIdsResult = retrieveOccurrenceData("", filters, null);
            for (IWINOccurrenceTo e : byOccurrenceDbIdsResult) {
                IWINOccurrenceTo existing = list.stream().filter(ex -> {
                    if (ex.getOccurrenceId() != e.getOccurrenceId())
                        return false;
                    if (ex.getCoopkey() == null && e.getCoopkey() == null)
                        return true;
                    if (ex.getCoopkey().equals(e.getCoopkey()))
                        return true;
                    return false;
                })
                    .findFirst().orElse(null);
                if (existing == null) {
                    list.add(e);
                }
            }
        }
        if (list.isEmpty())
            return new Connection<IWINOccurrenceTo>(new ArrayList<IWINOccurrenceTo>(), createPage(page), 0);
        if (sort != null && !sort.isEmpty())
            applySort(list, sort);
        return buildPage(list,page);
    }

    private List<IWINOccurrenceTo> retrieveOccurrenceData(String coopkey, List<FilterInput> filters, ContactModel contact) {
        try {
            String jsonString = "{\"coopkey\":\"" + coopkey + "\",\"filters\":" + filters.toString() + "}";
            MicroIntegrationService service = new MicroIntegrationService(WebClient
                .builder()
                .baseUrl(cbGraphQlEndpoint.replace("/graphql", "/"))
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
                .build());
            String cbGraphResponse = service.redirectRequestToMI("/occurrenceByCoopkey", jsonString);
            ObjectMapper om = new ObjectMapper();
            JsonNode data = om.readTree(cbGraphResponse);
            ObjectReader reader = om.readerFor(new TypeReference<List<IWINOccurrenceTo>>() {});
            List<IWINOccurrenceTo> cbOccurrenceData = reader.readValue(data.get("content"));
            if (cbOccurrenceData == null || cbOccurrenceData.isEmpty())
                return new ArrayList<>();
            for(IWINOccurrenceTo e : cbOccurrenceData) {
                ContactModel currentContact = contact;
                if (contact == null && e.getCoopkey() != null) {
                    currentContact = contactRepository.findByContactInfosContactInfoTypeNameInIgnoreCaseAndContactInfosValueAndContactInfosDeletedIsFalseAndDeletedIsFalse(Arrays.asList("COOPKEY"), e.getCoopkey())
                        .stream().findFirst().orElse(null);
                }
                if (currentContact != null) {
                    String recipientName = currentContact.getPerson().getFullName();
                    AddressModel recipientAddress = currentContact
                            .getContactAddresses()
                            .stream()
                            .filter(ca -> ca.isDefault())
                            .findAny()
                            .map(ca -> ca.getAddress())
                            .orElse(null);
                    String sowingDate = currentContact
                        .getContactInfos()
                        .stream()
                        .filter(ci -> ci.getContactInfoType().getName().toLowerCase().equals("wheat sowing date") && !ci.isDeleted())
                        .findAny()
                        .map(ci -> ci.getValue())
                        .orElse(null);
                    String sendViaCoopKey = currentContact
                            .getContactInfos()
                            .stream()
                            .filter(ci -> ci.getContactInfoType().getName().toLowerCase().equals("send via coopkey") && !ci.isDeleted())
                            .findAny()
                            .map(ci -> ci.getValue())
                            .orElse(null);
                    String sendViaName = null;
                    String sendViaCountry = null;
                    if (sendViaCoopKey != null) {
                        ContactModel sendViaContact = contactRepository.findByContactInfosContactInfoTypeNameInIgnoreCaseAndContactInfosValueAndContactInfosDeletedIsFalseAndDeletedIsFalse(Arrays.asList("COOPKEY"), sendViaCoopKey).stream().findFirst().orElse(null);
                        if (sendViaContact != null) {
                            sendViaName = sendViaContact.getPerson().getFullName().replace(",", "");
                            sendViaCountry = sendViaContact
                                .getContactAddresses()
                                .stream()
                                .filter(ca -> ca.isDefault())
                                .findAny()
                                .map(ca -> ca.getAddress().getCountry().getName())
                                .orElse(null);
                        }
                    }
                    e.setCoopkey(currentContact
                        .getContactInfos()
                        .stream()
                        .filter(ci -> ci.getContactInfoType().getName().toLowerCase().equals("coopkey") && !ci.isDeleted())
                        .findAny()
                        .map(ci -> ci.getValue())
                        .orElse(null));
                    e.setRecipientName(recipientName.replace(",", ""));
                    e.setSowingDate(sowingDate);
                    e.setSendViaCoopkey(sendViaCoopKey);
                    e.setSendViaName(sendViaName);
                    e.setSendViaCountry(sendViaCountry);
                    if (recipientAddress != null) {
                        e.setRecipientCountry(recipientAddress.getCountry().getName());
                    }
                }
            }
            return cbOccurrenceData;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error retrieving occurrence data from CB");
        }
    }

    private void applySort(List<IWINOccurrenceTo> list, List<SortInput> sorts) {
        String fieldName = sorts.get(0).getCol();
        //String sortMode = sorts.get(0).getMod().name();
        switch (fieldName) {
            case "sowingDate": 
                list.sort(Comparator.nullsLast(new Comparator<IWINOccurrenceTo>() {
                    public int compare(IWINOccurrenceTo o1, IWINOccurrenceTo o2) {
                        if (o1.getSowingDate() == null && o2.getSowingDate() == null)
                            return 0;
                        if (o1.getSowingDate() == null)
                            return 1;
                        if (o2.getSowingDate() == null)
                            return -1;
                        return String.CASE_INSENSITIVE_ORDER.compare(o1.getSowingDate(), o2.getSowingDate());
                    }
                }));
            break;
            case "coopkey": 
                list.sort(Comparator.nullsLast(new Comparator<IWINOccurrenceTo>() {
                    public int compare(IWINOccurrenceTo o1, IWINOccurrenceTo o2) {
                        if (o1.getCoopkey() == null && o2.getCoopkey() == null)
                            return 0;
                        if (o1.getCoopkey() == null)
                            return 1;
                        if (o2.getCoopkey() == null)
                            return -1;
                        return String.CASE_INSENSITIVE_ORDER.compare(o1.getCoopkey(), o2.getCoopkey());
                    }
                }));
            break;
            case "recipientName": 
                list.sort(Comparator.nullsLast(new Comparator<IWINOccurrenceTo>() {
                    public int compare(IWINOccurrenceTo o1, IWINOccurrenceTo o2) {
                        if (o1.getRecipientName() == null && o2.getRecipientName() == null)
                            return 0;
                        if (o1.getRecipientName() == null)
                            return 1;
                        if (o2.getRecipientName() == null)
                            return -1;
                        return String.CASE_INSENSITIVE_ORDER.compare(o1.getRecipientName(), o2.getRecipientName());
                    }
                }));
            break;
            case "recipientCountry": 
                list.sort(Comparator.nullsLast(new Comparator<IWINOccurrenceTo>() {
                    public int compare(IWINOccurrenceTo o1, IWINOccurrenceTo o2) {
                        if (o1.getRecipientCountry() == null && o2.getRecipientCountry() == null)
                            return 0;
                        if (o1.getRecipientCountry() == null)
                            return 1;
                        if (o2.getRecipientCountry() == null)
                            return -1;
                        return String.CASE_INSENSITIVE_ORDER.compare(o1.getRecipientCountry(), o2.getRecipientCountry());
                    }
                }));
            break;
            case "sendViaCoopkey": 
                list.sort(Comparator.nullsLast(new Comparator<IWINOccurrenceTo>() {
                    public int compare(IWINOccurrenceTo o1, IWINOccurrenceTo o2) {
                        if (o1.getSendViaCoopkey() == null && o2.getSendViaCoopkey() == null)
                            return 0;
                        if (o1.getSendViaCoopkey() == null)
                            return 1;
                        if (o2.getSendViaCoopkey() == null)
                            return -1;
                        return String.CASE_INSENSITIVE_ORDER.compare(o1.getSendViaCoopkey(), o2.getSendViaCoopkey());
                    }
                }));
            break;
            case "sendViaName": 
                list.sort(Comparator.nullsLast(new Comparator<IWINOccurrenceTo>() {
                    public int compare(IWINOccurrenceTo o1, IWINOccurrenceTo o2) {
                        if (o1.getSendViaName() == null && o2.getSendViaName() == null)
                            return 0;
                        if (o1.getSendViaName() == null)
                            return 1;
                        if (o2.getSendViaName() == null)
                            return -1;
                        return String.CASE_INSENSITIVE_ORDER.compare(o1.getSendViaName(), o2.getSendViaName());
                    }
                }));
            break;
        }
    }

    private Page<IWINOccurrenceTo> buildPage(List<IWINOccurrenceTo> list, PageInput page) {

        Pageable p = createPage(page);

        int totalElements = list.size();

        int start = p.getPageNumber() * p.getPageSize();

        int end = (p.getPageNumber() + 1) * p.getPageSize();

        if (start < totalElements) {
            list = list.subList(start, Math.min(end, totalElements));
        } else {
            throw new RuntimeException("Incorrect start page");
        }

        return new Connection<IWINOccurrenceTo>(list, p, totalElements);
    }

    private Pageable createPage(PageInput pageInput) {
        Pageable defaultPage = PageRequest.of(0, 20);
        return pageInput == null ? defaultPage
                : PageRequest.of(Math.max(0, pageInput.getNumber() - 1),
                        Math.min(300, pageInput.getSize()));
    }
    
}
