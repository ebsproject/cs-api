package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.AttributesTo;
import org.ebs.services.to.DomainTo;
import org.ebs.services.to.EmailTemplateTo;
import org.ebs.services.to.EntityReferenceTo;
import org.ebs.services.to.NumberSequenceRuleTo;
import org.ebs.services.to.SelectReference;
import org.ebs.services.to.Input.EntityReferenceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface EntityReferenceService {

    /**
     *
     * @param EntityReference
     */
    public EntityReferenceTo createEntityReference(EntityReferenceInput EntityReference);

    /**
     *
     * @param entityReferenceId
     */
    public int deleteEntityReference(int entityReferenceId);

    /**
     *
     * @param entityreferenceId
     */
    public Set<AttributesTo> findAttributess(int entityreferenceId);

    /**
     *
     * @param entityreferenceId
     */
    public Set<DomainTo> findDomains(int entityreferenceId);

    /**
     *
     * @param entityreferenceId
     */
    public Set<EmailTemplateTo> findEmailTemplates(int entityreferenceId);

    /**
     *
     * @param entityReferenceId
     */
    public Optional<EntityReferenceTo> findEntityReference(int entityReferenceId);

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    public Page<EntityReferenceTo> findEntityReferences(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters);

    /**
     *
     * @param entityreferenceId
     */
    public Set<NumberSequenceRuleTo> findNumberSequenceRules(int entityreferenceId);

 
    /**
     *
     * @param entityReference
     */
    public EntityReferenceTo modifyEntityReference(EntityReferenceInput entityReference);


    public List<SelectReference> findReference(String schema,String table,String fieldValue,String fieldText);



}