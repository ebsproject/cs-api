package org.ebs.services;

import static java.util.Map.entry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ebs.services.to.ProgramCB;
import org.ebs.services.to.UserCB;
import org.ebs.services.to.UserProgramCB;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrPagination;
import org.ebs.util.brapi.BrapiClient;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
class CoreBreedingServiceImpl implements CoreBreedingService {

    private final BrapiClient brapiClientCB;
    private static final String userResourcePath = "/persons-search";
    private static final String programResourcePath = "/programs-search";
    private static final String userProgramResourcePath = "/persons/${id}/programs";
    private static final String seedAttributesResourcePath = "/seeds/${id}/attributes";
    private static final String listMembersResourcePath = "/lists/${id}/members";
    private static final Map.Entry<String, String>  filtersProgram = entry("programStatus", "active"), userFields = entry("fields", "person.id AS personDbId | person.email | person.username | person.first_name AS firstName | person.last_name AS lastName | person.person_name AS personName | person.person_type AS personType | person.person_role_id AS personRoleId | person.is_active AS isActive");

    @Override
    public List<UserCB> findAllUsers() {
        List<UserCB> userList = new ArrayList<>();

        boolean pendingRecords = true;
        int pageNum = 1;

        while (pendingRecords) {
            BrPagedResponse<UserCB> response = brapiClientCB
                    .post(userResourcePath + "?page=" + pageNum++, userFields, UserCB.class);
            BrPagination page = response.getMetadata().getPagination();

            userList.addAll(response.getResult().getData());
            pendingRecords = page.getCurrentPage() != page.getTotalPages();
        }

        return userList;
    }
    @Override
    public List<ProgramCB> findAllPrograms() {
        List<ProgramCB> programList = new ArrayList<>();

        boolean pendingRecords = true;
        int pageNum = 1;

        while (pendingRecords) {
            BrPagedResponse<ProgramCB> response = brapiClientCB
                    .post(programResourcePath + "?page=" + pageNum++, filtersProgram, ProgramCB.class);
            BrPagination page = response.getMetadata().getPagination();

            programList.addAll(response.getResult().getData());
            pendingRecords = page.getCurrentPage() != page.getTotalPages();
        }

        return programList;
    }
    @Override
    public List<UserProgramCB> findProgramsByUserId(int personId) {

        List<UserProgramCB> uprogramList = new ArrayList<>();

       
            BrPagedResponse<UserProgramCB> response = brapiClientCB
                    .getList(userProgramResourcePath.replace("${id}", ""+personId), UserProgramCB.class);
            uprogramList.addAll(response.getResult().getData());

        return uprogramList;
    }

    @Override
    public String findSeedAttributes(int seedId) {

        String seedAttribbutes = brapiClientCB.get(seedAttributesResourcePath.replace("${id}", ""+seedId));

        return seedAttribbutes;
    }

    @Override
    public String findListMembers(int listId) {

        String listMembers = brapiClientCB.get(listMembersResourcePath.replace("${id}", ""+listId));

        return listMembers;

    }

}
