///////////////////////////////////////////////////////////
//  ProgramService.java
//  Macromedia ActionScript Implementation of the Interface ProgramService
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:21 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.ProgramTo;
import org.ebs.services.to.Input.ProgramInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:21 AM
 */
public interface ProgramService {

    /**
     *
     * @param Program
     */
    public ProgramTo createProgram(ProgramInput Program);

    /**
     *
     * @param programId
     */
    public int deleteProgram(int programId);

    /**
     *
     * @param programId
     */
    public Optional<ProgramTo> findProgram(int programId);

    /**
     *
     * @param page
     * @param sort
     * @param filters
     */
    public Page<ProgramTo> findPrograms(PageInput page, List<SortInput> sort,
            List<FilterInput> filters, boolean disjunctionFilters);


    /**
     *
     * @param program
     */
    public ProgramTo modifyProgram(ProgramInput program);

    //public List<ProgramTo> findByPrintoutTemplate(int printoutTemplateId);

}