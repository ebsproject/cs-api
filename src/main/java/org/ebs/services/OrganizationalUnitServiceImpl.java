package org.ebs.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.ContactModel;
import org.ebs.model.HierarchyModel;
import org.ebs.model.InstitutionModel;
import org.ebs.model.repos.ContactRepository;
import org.ebs.model.repos.HierarchyRepository;
import org.ebs.model.repos.InstitutionRepository;
import org.ebs.services.to.Contact;
import org.ebs.services.to.HierarchyTo;
import org.ebs.services.to.ProgramSync;
import org.ebs.services.to.Input.ContactWorkflowInput;
import org.ebs.services.to.Input.HierarchyInput;
import org.ebs.services.to.Input.OrganizationalUnitInput;
import org.ebs.services.to.Input.UnitMemberInput;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class OrganizationalUnitServiceImpl implements OrganizationalUnitService {

    private final ContactService contactService;
    private final ContactRepository contactRepository;
    private final HierarchyService hierarchyService;
    private final RoleContactHierarchyService roleContactHierarchyService;
    private final HierarchyRepository hierarchyRepository;
    private final InstitutionRepository institutionRepository;
    private final ContactWorkflowService contactWorkflowService;
    private final SynchronizeProgramService synchronizeProgramService;

    @Override
    public Contact createOrganizationalUnit(OrganizationalUnitInput input) {

        if (input.getContact().getCategory().getId() != 4)
            throw new RuntimeException("Invalid category");
        
        Contact unit = contactService.createContact(input.getContact());

        setParentOrganization(false, unit.getId(), input.getParentOrganizationId());

        setUnitMembers(false, unit.getId(), input.getUnitMembers());

        setUnitWorkflows(false, unit.getId(), input.getWorkflowIds());

        syncProgramToCB(unit.getId());

        return unit;
    }

    @Override
    public Contact modifyOrganizationalUnit(OrganizationalUnitInput input) {

        if (input.getContact().getCategory().getId() != 4)
            throw new RuntimeException("Invalid category");
        
        Contact unit = contactService.modifyContact(input.getContact());

        setParentOrganization(true, unit.getId(), input.getParentOrganizationId());

        setUnitMembers(true, unit.getId(), input.getUnitMembers());

        setUnitWorkflows(true, unit.getId(), input.getWorkflowIds());

        syncProgramToCB(unit.getId());

        return unit;

    }

    private void setParentOrganization(boolean changeParent, int unitContactId, int parentContactId) {
        if (parentContactId <= 0)
            throw new RuntimeException("Invalid parent organization");
        
        if (changeParent) {
            hierarchyService.deleteContactParentsRelation(unitContactId);
        }

        hierarchyService.createHierarchy(new HierarchyInput(unitContactId, parentContactId, false));
    }

    private void setUnitMembers(boolean changeRoles, int unitContactId, Set<UnitMemberInput> memberList) {

        List<Integer> existingUnitMembers = hierarchyRepository.findByInstitutionIdAndDeletedIsFalse(unitContactId)
            .stream().map(r -> r.getId()).collect(Collectors.toList());

        memberList.forEach(member -> {
            if (member.getRoleIds() == null || member.getRoleIds().isEmpty())
                throw new RuntimeException("Missing member role information");
            HierarchyTo hierarchy = hierarchyService
                .createHierarchy(new HierarchyInput(member.getMemberId(), unitContactId, false));
            if (changeRoles) {
                roleContactHierarchyService.deleteContactHierarchyRolesRelation(hierarchy.getId());
                existingUnitMembers.removeAll(Arrays.asList(hierarchy.getId()));
            }

            member.getRoleIds().forEach(roleId -> {
                roleContactHierarchyService.createRoleContactHierarchy(roleId, hierarchy.getId());
            });
        });

        existingUnitMembers.forEach(m -> {
            roleContactHierarchyService.deleteContactHierarchyRolesRelation(m);
            hierarchyService.deleteHierarchy(m);
        });

    }

    private void setUnitWorkflows(boolean changeWorkflows, int unitContactId, Set<Integer> workflowIds) {

        if (changeWorkflows) {
            Set<Integer> currentWorkflowIds = 
                contactWorkflowService.findModelByContactId(unitContactId)
                .stream().map(w -> w.getWorkflow().getId())
                .collect(Collectors.toSet());
            Set<Integer> currentWorkflowIdsCopy = new HashSet<>(currentWorkflowIds);
            currentWorkflowIds.removeAll(workflowIds);
            workflowIds.removeAll(currentWorkflowIdsCopy);
            workflowIds.forEach(w -> {
                contactWorkflowService.createContactWorkflow(new ContactWorkflowInput(0, unitContactId, w, true));
            });
            currentWorkflowIds.forEach(w -> {
                contactWorkflowService.deleteContactWorkflow(unitContactId, w);
            });
        } else {
            workflowIds.forEach(w -> {
                contactWorkflowService.createContactWorkflow(new ContactWorkflowInput(0, unitContactId, w, true));
            });
        }
    }

    private void syncProgramToCB(int contactId) {
        ContactModel contact = contactRepository.findById(contactId).orElse(null);
        if (contact == null)
            return;
        List<HierarchyModel> contactParent = hierarchyRepository.findByContact(contact);
        InstitutionModel program = contact.getInstitution();
        if (program.getUnitType().getName().equals("Program")) {
            if (program.getCrops() != null && !program.getCrops().isEmpty()) {
                ProgramSync programSync = new ProgramSync();
                programSync.setProgramCode(program.getCommonName());
                programSync.setProgramName(program.getLegalName());
                programSync.setOrganizationCode(null);
                if (contactParent.isEmpty())
                    programSync.setOrganizationCode("EBS");
                else {
                    programSync.setOrganizationCode(contactParent.stream().filter(p -> p.getInstitution().getInstitution() != null
                        && p.getInstitution().getInstitution().isCgiar()).findFirst().map(pi -> pi.getInstitution().getInstitution().getCommonName())
                        .orElse("EBS"));
                }
                programSync.setCropCode(program.getCrops().get(0).getCode());
                programSync.setActive(!program.isDeleted() && !program.getContact().isDeleted());
                synchronizeProgramService.synchronizeProgramsToCB(Arrays.asList(programSync));
            } else {
                log.info("Program {} has no crops, cannot sync to CB...", program.getCommonName());
            }  
        }
    }

    @Override
    public boolean deleteOrganizationalUnit(int organizationalUnitId) {

        if (organizationalUnitId <= 0) 
            throw new RuntimeException("Organizational unit not found");

        ContactModel unit = verifyOrganizationalUnit(organizationalUnitId);
        InstitutionModel unitInstitution = unit.getInstitution();

        if (unit == null || unitInstitution == null)
            throw new RuntimeException("Organizational unit not found");

        int deleteContact = contactService.deleteContact(unit.getId());

        if (deleteContact == unit.getId()) {
            hierarchyRepository.findByInstitutionIdAndDeletedIsFalse(deleteContact)
                .forEach(h -> {
                    roleContactHierarchyService.deleteContactHierarchyRolesRelation(h.getId());
                    hierarchyService.deleteHierarchy(h.getId());
                });
            hierarchyRepository.findByContactIdAndDeletedIsFalse(deleteContact)
                .forEach(h -> hierarchyService.deleteHierarchy(h.getId()));
        }
        contactWorkflowService.deleteContactWorkflowsRelation(deleteContact);
        unitInstitution.setCrops(new ArrayList<>());
        unitInstitution.setDeleted(true);
        institutionRepository.save(unitInstitution);
        synchronizeProgramService.deleteCBProgram(unitInstitution.getCommonName());
        return true;
    }

    private ContactModel verifyOrganizationalUnit(int id) {
        return contactRepository.findByIdAndDeletedIsFalse(id).orElse(null);
    }
    
}