package org.ebs.services;

public interface RoleContactHierarchyService {

    boolean createRoleContactHierarchy(int roleId, int contactHierarchyId);
    boolean deleteRoleContactHierarchy(int roleId, int contactHierarchyId);
    boolean deleteContactHierarchyRolesRelation(int contactHierarchyId);
    
}
