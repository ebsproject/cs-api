///////////////////////////////////////////////////////////
//  InstanceServiceImpl.java
//  Macromedia ActionScript Implementation of the Class InstanceServiceImpl
//  Generated by Enterprise Architect
//  Created on:      18-Mar-2021 8:12:03 AM
//  Original author: EBRIONES
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.AddressModel;
import org.ebs.model.InstanceModel;
import org.ebs.model.TenantModel;
import org.ebs.model.repos.AddressRepository;
import org.ebs.model.repos.AuditLogsRepository;
import org.ebs.model.repos.DomainInstanceRepository;
import org.ebs.model.repos.InstanceRepository;
import org.ebs.model.repos.TenantRepository;
import org.ebs.services.to.Address;
import org.ebs.services.to.AuditLogsTo;
import org.ebs.services.to.DomainInstanceTo;
import org.ebs.services.to.InstanceTo;
import org.ebs.services.to.TenantTo;
import org.ebs.services.to.Input.InstanceInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author EBRIONES
 * @version 1.0
 * @created 18-Mar-2021 8:12:03 AM
 */
@Service @Transactional(readOnly = true)
@RequiredArgsConstructor
class InstanceServiceImpl implements InstanceService {

	private final InstanceRepository instanceRepository;
	private final ConversionService converter;
	private final AuditLogsRepository auditlogsRepository;
	private final TenantRepository tenantRepository;
	private final DomainInstanceRepository domaininstanceRepository;
	private final AddressRepository addressRepository;

	/**
	 *
	 * @param instanceInput
	 */
	@Override @Transactional(readOnly = false)
	public InstanceTo createInstance(InstanceInput instanceInput){
		instanceInput.setId(0);
		InstanceModel model = converter.convert(instanceInput,InstanceModel.class);

		setInstanceDependencies(model, instanceInput);
		model.setName(generateInstanceName(model.getTenant()));
		return converter.convert(instanceRepository.save(model), InstanceTo.class);
	}

    /**
	 * Assign a functional unit as child of an existing funcional unit. This method
	 * overrides the program and tenant of the FU to match the parent.
     *
	* @param fu
	 * @param fuInput
     */
	void setInstanceDependencies(InstanceModel model, InstanceInput input) {
		TenantModel tenantModel = tenantRepository.findByIdAndDeletedIsFalse(input.getTenantId())
			.orElseThrow(() -> new RuntimeException("Tenant not found"));
		model.setTenant(tenantModel);

		if (input.getAddressId() != null && input.getAddressId() > 0) {
			AddressModel addressModel = addressRepository.findById(input.getAddressId())
				.orElseThrow(() -> new RuntimeException("Address " + input.getAddressId() + " not found"));
			model.setAddress(addressModel);
		}
	}

	String generateInstanceName(TenantModel tenant) {
		return String.format("%s-%s-%02d"
			,tenant.getOrganization().getCode()
			,tenant.getCustomer().getName()
			,tenant.getInstances().size()+1).toUpperCase();
	}

	/**
	 *
	 * @param instanceId
	 */
	@Override @Transactional(readOnly = false)
	public int deleteInstance(int instanceId){
		InstanceModel instance = verifyInstance(instanceId);
		instance.setDeleted(true);
		instanceRepository.save(instance);
		return instanceId;
	}

	/**
	 *
	 * @param instanceId
	 */
	public Set<AuditLogsTo> findAuditLogss(int instanceId){
		return auditlogsRepository.findByInstanceId(instanceId).stream().map(e -> converter.convert(e,AuditLogsTo.class)).collect(Collectors.toSet());
	}

	/**
	 *
	 * @param instanceId
	 */
	public Set<DomainInstanceTo> findDomainInstances(int instanceId){
		return domaininstanceRepository.findByInstanceIdAndDeletedIsFalse(instanceId).stream().map(e -> converter.convert(e,DomainInstanceTo.class)).collect(Collectors.toSet());
	}

	/**
	 *
	 * @param instanceId
	 */
	@Override
	public Optional<InstanceTo> findInstance(int instanceId){
		if(instanceId <1)
		 {return Optional.empty();}
		 return instanceRepository.findByIdAndDeletedIsFalse(instanceId).filter(r -> !r.isDeleted()).map(r -> converter.convert(r,InstanceTo.class));
	}

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	@Override
	public Page<InstanceTo> findInstances(PageInput page, List<SortInput> sort, List<FilterInput> filters, boolean disjunctionFilters){
		return instanceRepository.findByCriteria(InstanceModel.class,filters,sort,page,disjunctionFilters).map(r -> converter.convert(r,InstanceTo.class));
	}

	/**
	 *
	 * @param instanceId
	 */
	public Optional<TenantTo> findTenant(int instanceId){
		return instanceRepository.findByIdAndDeletedIsFalse(instanceId).map(r -> converter.convert(r.getTenant(),TenantTo.class));
	}

	InstanceModel verifyInstance(int instanceId) {
		return instanceRepository.findByIdAndDeletedIsFalse(instanceId)
			.orElseThrow(() -> new RuntimeException("Instance not found"));
	}

	/**
	 *
	 * @param instanceInput
	 */
	@Override @Transactional(readOnly = false)
	public InstanceTo modifyInstance(InstanceInput instanceInput){
		InstanceModel originalModel= verifyInstance(instanceInput.getId());
		InstanceModel modifiedModel= converter.convert(instanceInput,InstanceModel.class);

		setInstanceDependencies(modifiedModel, instanceInput);
		Utils.copyNotNulls(modifiedModel,originalModel);

		return converter.convert(instanceRepository.save(originalModel), InstanceTo.class);
	}

	@Override
	public Address findAddress(int instanceId) {
		return instanceRepository.findByIdAndDeletedIsFalse(instanceId).map(r -> converter.convert(r.getAddress(), Address.class)).orElse(null);
	}

}