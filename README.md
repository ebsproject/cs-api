# Project Description

This project is the core back-end of a Domain-Driven Micro Services Architecture, and can be added into Service Gateway Platform.

For EBS, it is defined as an entire back-end model where the following capabilities are included under the Service Gateway Concept:

- Management of the EBS APIs and Third-party component APIs to have control of the data exchange mechanism. (Data Exchange flow concept).

- Expose an API Driven Data Access Layer.

- Implement the Identity Server to enable the Authentication Method based on Token.

# Pattern and Technologies

The project relies heavily on Spring Framework and uses the following packages to work:

## Spring Framework libraries

- Spring
- Spring Boot (2.6.6)
- Spring Web
- Spring Cache

## Non-Spring Framework libraries

- springdoc openapi
- spring boot kickstart playground
- GraphQL Java Tools
- GraphQL Spring Kickstart
- postgresql JDBC

# Project Structure

cs-api is a part of Micro-Services (Mis) architecture, which aims for fast development of isolated services that can coordinate with others to achieve complex tasks. 

## Layers

This project consists of layers that cover specific tasks, these are:

- **GraphQL and Rest endpoints**: Expose resources and actions over the corresponding service.
- **Validations**: All requests must be well formed and be coherent, in this layer the requests are checked for that.
- **Transformations**: In this layer request objects called Transfer Objects (TOs) are converted to Model Objects (MOs).
- **Services**: This is the most important layer, also known as Control Layer. After input data is validated and transformed, the service can process the request.
- **Repositories**: It is the Data Access Layer. Performs operations in the underlying database. Provides a common language for querying and persisting.
- **ORM Entities**: Define a database model and expose it as an Object Model.

In addition to these layers, which have explicit interactions, there are two ubiquitous layers:

- **Security**: It is in charge of receiving and validating a security token.
- **Auditing**: It is in charge of managing auditing information in the persistence layer, determining who and when the information was created or modified.

# How to setup the development environment

To get started developing or running the project, the following software is needed:

- IDE: VSCode (recommended)
- Java 11
- Git client
- Libraries or extensions for Spring Framework depending on the IDE chosen

For _VSCode_ the following Extensions are needed:

- Spring Boot Extension Pack
- Extension Pack for Java(TM) by Red Hat
- Lombok Annotations Support for VS Code

The following extensions are recommended but not required:

- Debugger for Java
- Test Runner for Java
- Git Graph

Optional software:

- Maven
- Gradle
- Docker (for building the image and deployment)

## Installation

1. Clone [cs-api repository](https://bitbucket.org/ebsproject/cs-api/src/develop/) into a folder
2. In VSCode (or IDE) open the folder where the repository was cloned.

# How to build or run the project
## Considerations
<!--- - IMPORTANT: Change the encoding of buildDockerImage.sh and entrypoint.sh with "dos2unix NameFile.sh"--->
 - IMPORTANT: Review the 8080 port for the service and the 8290 for dataflows artifact OR get into the docker network.
 - The micro service can connect to a DB, which is not managed in this project.
 - There should be an API manager component which will manage the microservice access.
 - The main configuration item is the data source, found in `` src/main/resources/application.yml `` under ``spring.datasource``. The default database configuration follows the standard naming: _ebs-{domain}-db:port/{domaindatabase}_. Example: ``jdbc:postgresql://ebs-ex-db:5432/exdb``. username and password default both to **postgres**
 - Endpoints in dataflows connecting with other microservices _must_ point to standard container names: ebs-sg-{domain}, protocol (http) and port (8080). Example: http://ebs-sg-ex:8080
 - Communication over HTTPS will be managed by the upper layer of API Manager

### Synchronizer

#### Project Description

This project can be conceptualized as a project within and inherent in the same CS-API project. Taking all its features. 

- CONTEXT: This artifact was designed for bring and store data in automatically way from a main source -- Core Breeding for this case -- in order to maintain the EBS System access with security and assure that all users in Core Breeding with specific permission has the same permission in Core System.
- CONCEPT: Scheduled task of ONE-WAY SYNCHRONIZATION -- Core Breeding being the main source won't be affected with any change commited inside Core System
- Synchronized Entities
    1.- programs: Necessary to usethem when a printout tamplate is created.
        1.1.- crop_program: Necessary to create the relationship between program and crop.
    2.- users: Necessary to usethem with the login process.
        2.1.- contact: Necessary to maintain the data integrity. All user must have a contact but not all contact must have a user.
        2.2.- person: Necessary to maintain the data integrity. All user has a contact and all contact has a person.
        2.3.- user_role: Necessary to create the relationship between user and role.
        3.4.- tenant_user: Necessary to create the relationship between user and tentant.

#### Pattern and Technologies

-This project uses the same patterns and technologies of CS-API project.

#### Project Structure

-This project has the same structure as CS-API project. Taking

#### How to setup the development environment

This project uses the same development configuration as CS-API project

#### Installation

This project uses the same installation configuration as CS-API
### Building tools

There are two building tools configured for the services component. Developers can use whichever they prefer: Gradle or Maven. If none of these is installed in the development environment, the wrappers for them can be used instead, which will take care of downloading and installing themselves locally.

|dev environment| chosen tool| wrapper
|--|--|--
|Windows|Gradle|gradlew.bat
|Windows|Maven |mvnw.cmd
|*nix   |Gradle|gradlew
|*nix   |Maven |mvnw

For the rest of the document, commands will be written as if a local installation of the build tool was available. If necessary, just replace the ``gradle`` or ``mvn`` command with the appropriate wrapper.

## Local Development

### Run from code

How to compile and run the services for local dev & testing
#### Maven

    #run with default values
    mvn spring-boot:run

    #run overriding values
    mvn spring-boot:run -D"spring-boot.run.arguments=--spring.datasource.url=jdbc:postgresql://my-new-host:5432/mydb --server.port=8081"

#### Gradle

	#run with default values
    gradle bootRun

	#run overriding values
	gradle bootRun --args="--spring.datasource.url=jdbc:postgresql://my-new-host:5432/mydb --server.port=8081"

### Arguments

#### Arguments to the Synchronizador

In orther to make the synchronizer works is necessary set the next environment variables passed as arguments.

ebs_cs_sync_enabled=true ``For the docker run command`` or ebs.cs.sync.enabled=true ``For the gradle run command`` this for all the environment variables.

ebs_cs_sync_delay_time_seconds=30 ``This set the laps time the sync will be executed``
ebs_cs_serverData=http://localhost:8080/graphql ``This is necessary to bring data from the own graphql service``
ebs_cb_serverData=cbapi-dev.ebsproject.org ``This is necessary to bring the data from Core Breeding``
ebs_cs_auth_client_id=some_client_id ``Necessary to make identification against Identity Service``
ebs_cs_auth_client_secret=some_client_secret ``This is the password used by the client id to generate token``
ebs_cs_auth_username=some_username ``This is the user name registered in the Identity server with the enough permissions to read and write the id client and secret and token``
ebs_cs_auth_password=some_password ``This is the password registered in the Identity server with the enough permissions to read and write the id client and secret and token``
ebs_cs_auth_endpoint=https://sg.SOME_SERVER:PORT/oauth2/token ``The identity Server endpoint``
ebs_cs_auth_grant_type=password ``Necessary configurations to read requests``
ebs_cs_auth_claims=username ``Necessary configurations to read requests``
ebs_cs_auth_scope=openid ``Necessary configurations to read requests``

<!--All this arguments such as environment variables are absolutly necessary-->

#### Profile

Two profiles must be provided to the application, one to define the environment (development, production) and other to determine where to store files (local directory or in the cloud)
To change the application profiles, use the following configuration

Example:
```
--spring.profiles.active = dev or prod to production profile

```
Currently two profiles are available for environment: dev and prod (default)
Two additional profiles are available for the storage: store-local and store-s3

During deployment is mandatory to provide two profiles, one for each aspect of the application

Example:
```
--spring.profiles.active = dev,store-s3
--spring.profiles.active = prod,store-local

```

##### store-local

store-local will create a directory in the same path as the project source code or the .jar file depending on the deployment mechanism. The name of the directory will be the name of the envirnoment variable **ebs.aws.s3.bucket**

##### store-s3
Profile store-s3 will try to create a connection to an AWS S3 instance. For that it will require Amazon IAM or STS credentials. They can be provided as environment variables or putting them in a file in /home/user/.was/credentials

Example of credentials file:
```
[default]
aws_access_key_id={key value}
aws_secret_access_key={secret key}
aws_session_token={optional session token, if using STS credentials}
```

Example for environment variables in Dockerfile
```
ENV AWS_PROFILE=default
ENV AWS_ACCESS_KEY_ID={key value}
ENV AWS_SECRET_ACCESS_KEY={secret key}
ENV AWS_SESSION_TOKEN={optional session token, if using STS credentials}
```

Additionally, two extra arguments are needed to specify the S3 region and the bucket name
ENV EBS_AWS_S3_BUCKET={some name}
ENV EBS_AWS_S3_REGION={some valid region}

#### Service client

When the log displays something like this it means the services are up and running:

    Tomcat started on port(s): 8080 (http) with context path ''
    Started Application in 6.718 seconds (JVM running for 7.074)

A Graphql client explorer will be deployed at: ``http://{host}:{port}/playground``

Graphql endpoint will be in ``http://localhost:{port}/graphql``

A Rest client will be available at ``http://{host}:{port}/api-doc.html``

Rest Services will be published as defined in the Resource classes: ``http://localhost:{port}/{resource-path}``

### Build
#### Maven

    mvn package
The artifact will be created in path ``{project-home}/target/ebs-sg-{domain}.jar``
#### Gradle

    gradle bootJar
The artifact will be created in path ``{project-home}/build/libs/ebs-sg-{domain}.jar``

#### After build

You can run the artifact as a regular java executable:

    java -jar {project-home}/{artifact-path}/ebs-sg-{domain}.jar

To override default configuration values:

    java -jar build/libs/ebs-sg-ex.jar --spring.datasource.url=jdbc:postgresql://172.17.61.4:5434/mydb --server.port=8083

### Testing

Unit tests are executed always during build phase. For the build to succeed 100% of tests must pass. Results of testing can be checked in:

- maven:
 - target/site/jacoco/index.html
- gradle:
 - build/reports/jacoco/test/html/index.html and
 - build/reports/tests/test/index.html

#### Runing tests (maven)
All tests

    mvn test
    mvn test jacoco:report (to generate coverage report)

Individual tests

    mvn test -Dtest=path/to/test/MyTest

Examples:

    mvn test -Dtest=org/ebs/graphql/QueryResolverTest
    mvn test -Dtest=org/ebs/model/repository/*

#### Running tests (gradle)
All tests

    gradle test

Individual tests

    gradle test --tests path.to.test.MyTest

Examples:

    gradle test --tests org.ebs.graphql.QueryResolverTest
    gradle test --tests org.ebs.model.repository.*

# Deployment Specification

# EBS Service Gateway - Deploy Micro Service

This method has been tested in Linux (CentOS) environment. Testing under windows is still pending.

## Build Docker image

Check _Dockerfile_ and modify ENV variables to access the appropriate database:

    ENV spring.datasource.url=jdbc:postgresql://{hostname}:{port}/{databasename}
    ENV spring.datasource.username=username
    ENV spring.datasource.password=password

Run the following script to generate a docker image for the project:

    ./buildDockerImage.sh

This will generate an image named _ebs-sg-{domain}:{version-tag}_. Example: _ebs-sg-ex:0.1_
The host running this script just needs docker to be installed, maven and java are not necessary.

## Deploy container
Run an image based on this generated image. Example:

    docker run --rm -dt -p 8080:8080 --name ebs-sg-ex-0.1 --network ebs-sg-net ebs-sg-ex:0.1

# Security configuration

## Authorization

This application supports a security configuration based on RBAC (Role Based Access Control), which is done at the request level by extracting the information provided by the Identity Server (JWT Token), and applying roles (privileges) to the user according to their User Profile.

The Authorization mechanism tells us if a user is allowed to perfom certain actions (Actions/Data Actions) in the application at the backend level, and to protect or restrict a certain action or resource, Spring Security is used.

We can achieve that by using the ```@PreAuthorize``` annotation and providing the roles that can access certain action.

Example 1: Using the annotation at class level, this will protect all methods (actions) available in the class

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public class AdminOperationsServiceImpl {
        // class body
    }

Example 2: Using the annotation at method level, this will allow us to protect a single method (action) in the class

    public class OperationsServiceImpl {
         @PreAuthorize("hasRole('ROLE_ADMIN')")
         public int deleteRecord(int recordId) {
             //method body
         }
    }

We can define multiple roles in the annotation like this

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_SYSADMIN')")

This way operations (queries/mutations/services) exposed through the GraphQL APIs will be protected by an Authorization mechanism which relies on Spring Security to grant Authorities to end users consuming these APIs.

# Diagram Component Dependency


![cs-api dependency diagram](/img/cs-api-dependency-diagram.png)